package apk.rpiplayer.org.practicabbdd;

/**
 * Created by juand on 18/12/2017.
 * POJO que almacena el nombre y apellidos de una persona
 */

public class Persona {

    private String nombre;
    private String apellidos;

    public Persona(String nombre, String apellidos){
        this.nombre=nombre;
        this.apellidos=apellidos;
    }

    public String getNombre() {
        return nombre;
    }

    public String getApellidos() {
        return apellidos;
    }
}
