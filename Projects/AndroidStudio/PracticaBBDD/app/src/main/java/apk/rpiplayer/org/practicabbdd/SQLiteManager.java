package apk.rpiplayer.org.practicabbdd;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by juand on 18/12/2017.
 * Permite acceder a las dos tablas de persona en la base de datos SQLite internaa
 */

public class SQLiteManager{


    private static SQLiteDatabase db;
    private static String TARGET_TABLE;

    //CONSTANTES PARA LAS TABLAS
    public static final String TABLA_PERSONA="persona";
    public static final String TABLA_COPIA_PERSONA="copia_persona";
    private static final String KEY="id";
    private static final String COLUMN_1="nombre";
    private static final String COLUMN_2="apellidos";

    private static final String[] COLUMNS ={COLUMN_1,COLUMN_2};

    //CONSULTAS CREACIONALES
    private static final String createTable =
            "CREATE TABLE persona (" +
                    KEY+" integer primary key autoincrement," +
                    COLUMN_1+" text not null," +
                    COLUMN_2+" text);";

    private static final String createTableNombre =
            "CREATE TABLE copia_persona (" +
                    KEY+" integer primary key autoincrement," +
                    COLUMN_1+" text not null," +
                    COLUMN_2+" text);";

    //Constructor
    public SQLiteManager(Context ctx){
        db = new PersonaOpenHelper(ctx).getWritableDatabase();
    }

    /**
     * Permite seleccionar la tabla desde la que se quieren extraer los datos. Debe llamarse si se
     * quiere cambiar la fuente de información antes de realizar las consultas.
     * @param nombre_tabla
     */
    public void selectTable(String nombre_tabla){
        TARGET_TABLE = nombre_tabla;
    }

    //Inserta las personas en la base de datos
    public void insertarPersona(Persona p){
        ContentValues cvalues = new ContentValues();
        cvalues.put("nombre",p.getNombre());
        cvalues.put("apellidos",p.getApellidos());

        db.insertOrThrow(TARGET_TABLE,null,cvalues);
    }

    //Llama al método insertarPersona(Persona p) en bucle para la introducción de colecciones
    public void insertarPersonas(List<Persona> personas){
        for(Persona p: personas){
            insertarPersona(p);
        }
    }

    //Obtiene todas las personas de la tabla objetivo
    public ArrayList<Persona> obtenerPersonas(){
        ArrayList<Persona> personas = new ArrayList<Persona>();
        Cursor c=db.query(TARGET_TABLE,COLUMNS,null,null,null,null,null);
        c.moveToFirst();

        while(!c.isAfterLast()){
            personas.add(new Persona(c.getString(0),c.getString(1)));
            c.moveToNext();
        }
        c.close();
        return personas;
    }

    /**
     * Permite obtener una base de datos SQLite interna
     */
    private class PersonaOpenHelper extends SQLiteOpenHelper{

        public PersonaOpenHelper(Context ctx){
            super(ctx, "db_pruebas", null, 1);
        }
        @Override
        public void onCreate(SQLiteDatabase sqLiteDatabase) {
            //Crea las dos tablas de persona
            sqLiteDatabase.execSQL(createTable);
            sqLiteDatabase.execSQL(createTableNombre);
        }

        @Override
        public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
            //Elimina las tablas existentes y llama al onCreate para volver a crearlas
            sqLiteDatabase.execSQL("DROP TABLE IF EXISTS "+TABLA_PERSONA);
            sqLiteDatabase.execSQL("DROP TABLE IF EXISTS "+TABLA_COPIA_PERSONA);
            onCreate(sqLiteDatabase);
        }
    }
}
