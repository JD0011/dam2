/**
 * Hacer bbdd que guarda nombre y apellidos. Mostrar en listview. Cuando el usuario haga
 * click sobre un item, añadirlo a la base de datos secundaria. Hacer un boton para alternar
 * entre las bases de datos.
 *
 * Modificar el adapter para que muestre nombre o apellido, según se decida.
 * Cambiar nombre y apellidos a clase Persona
 */
package apk.rpiplayer.org.practicabbdd;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private ListView lvNombres;
    private SQLiteManager bbdd;
    private PersonaArrayAdapter adapter;
    private ArrayList<Persona> personas;

    private Button btnActualizar;
    private boolean alternaTabla;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lvNombres=(ListView)findViewById(R.id.lv_nombres);
        btnActualizar=(Button)findViewById(R.id.btn_actualizar);
        alternaTabla=true;

        bbdd=new SQLiteManager(this);

        personas = new ArrayList<Persona>();
        adapter = new PersonaArrayAdapter(this, personas);
        lvNombres.setAdapter(adapter);

        //Obtiene las personas que existan ya en la base de datos persona
        bbdd.selectTable(SQLiteManager.TABLA_PERSONA);
        personas = bbdd.obtenerPersonas();

        //EJECUTAR UNA SOLA VEZ. SI NO, SE GRABARÁN LOS DATOS DOS VECES
        //INICIO
        adapter.add(new Persona("Juan de Dios","Delgado"));
        adapter.add(new Persona("David","Espinosa"));
        adapter.add(new Persona("Marcos","López"));
        adapter.add(new Persona("Manuel","Fernández"));
        adapter.add(new Persona("Victor","González"));
        adapter.add(new Persona("Germán","Morales"));

        //Guardar las personas creadas anteriormente
        bbdd.selectTable(SQLiteManager.TABLA_PERSONA);
        bbdd.insertarPersonas(personas);
        //FIN
        adapter.notifyDataSetChanged();

        //LISTENNER'S
        //Guarda el nombre de la persona pulsado en la tabla COPIA_PERSONA
        lvNombres.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if(alternaTabla==true){
                    bbdd.selectTable(SQLiteManager.TABLA_COPIA_PERSONA);
                    //Almacena solo nombre
                    bbdd.insertarPersona(new Persona(personas.get(i).getNombre(),""));
                }
            }
        });

        //Alterna entre las tablas desde donde extrae las personas
        btnActualizar.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                actualizaPersonas();
            }
        });
    }

    private void actualizaPersonas(){
        //Permite alternar entre las dos tablas
        alternaTabla=!alternaTabla;

        if(alternaTabla){
            bbdd.selectTable(SQLiteManager.TABLA_PERSONA);
            personas=bbdd.obtenerPersonas();
        }else{
            bbdd.selectTable(SQLiteManager.TABLA_COPIA_PERSONA);
            personas=bbdd.obtenerPersonas();
        }

        //Actualiza contenido
        adapter.clear();
        adapter.addAll(personas);
        adapter.notifyDataSetChanged();
    }
}
