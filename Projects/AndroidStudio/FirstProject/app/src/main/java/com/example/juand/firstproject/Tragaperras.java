/**
 * Controla el layout de Tragaperras, actualizando la información con respecto a cada evento que se
 * produce. Sirve de enlace entre el juego y el usuario.
 *
 * @author Juan de Dios Delgado Bermúdez
 * @since 06/10/2017
 */

package com.example.juand.firstproject;

import android.support.annotation.IdRes;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Layout;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.Random;

public class Tragaperras extends AppCompatActivity {

    //Elementos del layout
    private TextView[] ruleta=new TextView[3];
    private TextView num_jugadas, saldo, resultadoLabel;
    private Button jugar,reiniciar;
    //Niveles
    private RadioGroup niveles;

    // Juego de Tragaperras
    TPGame game;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tragaperras);

        //CONTROLES

        // Ruleta
        ruleta[0] = (TextView) findViewById(R.id.hueco1_value);
        ruleta[1] = (TextView) findViewById(R.id.hueco2_value);
        ruleta[2] = (TextView) findViewById(R.id.hueco3_value);
        resultadoLabel = (TextView) findViewById(R.id.resultado_label);

        //Info
        num_jugadas = (TextView) findViewById(R.id.num_jugadas_value);
        saldo = (TextView) findViewById(R.id.saldo_value);

        //RadioGroup de los niveles. Se establece por defecto el nivel fácil
        niveles = (RadioGroup) findViewById(R.id.niveles_rad_group);
        niveles.check(R.id.nivel1);

        //Botones
        jugar = (Button) findViewById(R.id.jugar_button);
        reiniciar = (Button) findViewById(R.id.reiniciar_button);

        //INSTANCIACION DE JUEGO
        game = new TPGame(extraerDificultad(),5);

        actualizarRuleta();
        actualizarEstadisticas();

        //LISTENER'S

        //Listener que detecta la dificultad elegida en el RadioGroup
        niveles.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {

                //Se reinician los valores del juego y se establece nueva dificultad
                game.reiniciar();
                game.setDificultad_actual(extraerDificultad());


                actualizarRuleta();
                actualizarEstadisticas(); //Saldo y  Nº Jugadas

                //Limpia la cadena de texto de resultado
                resultadoLabel.setText("");
            }
        });

        //Listener del botón Jugar que detecta cuando se debe realizar una jugada
        jugar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                game.generarResultado();

                //Comprueba la jugada, suma el saldo, y muestra el mensaje de resultado
                resultadoLabel.setText(game.comprobarJugada()? R.string.win_msg : R.string.lose_msg);

                actualizarRuleta();
                actualizarEstadisticas(); //Saldo y Nº Jugadas

            }
        });

        //Listener del botón reiniciar que reiniciará la partida
        reiniciar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Reinicia el juego, reseteando el valor de sus atributos por los iniciales
                game.reiniciar();

                actualizarRuleta();
                actualizarEstadisticas(); //Saldo y Nº Jugadas

                //Limpia la cadena de texto de resultado
                resultadoLabel.setText("");

            }
        });

    }

    /**
     * Extraera el valor de la dificultad en función del RadioButton que se le pase por parámetros
     * @return Dificultad elegida
     */
    public int extraerDificultad (){

        //RadioButton elegido
        int rb_id =niveles.getCheckedRadioButtonId();
        RadioButton rButtElegido = (RadioButton) findViewById(rb_id);

        switch (rButtElegido.getText().toString()){
            case "1-3":
                return 1;
            case "1-5":
                return 2;
            case "1-10":
                return 3;
        }
        //Nunca retorna 0
        return 0;
    }

    /**
     * Este método actualizará la ruleta mostrando los valores que contiene la variable resultado
     * del objeto TPGame
     */
    public void actualizarRuleta(){
        for(int i = 0;i<ruleta.length;i++){
            ruleta[i].setText(String.valueOf(game.getResults()[i]));
        }
    }

    public void actualizarEstadisticas(){
        //Número de jugadas
        num_jugadas.setText(game.getJugadas());
        //Saldo actual
        saldo.setText(game.getSaldo());

        if(game.tieneSaldo()){
            jugar.setEnabled(true);
        }else{
            jugar.setEnabled(false);
            resultadoLabel.setText(R.string.end_game);
        }
    }
}

