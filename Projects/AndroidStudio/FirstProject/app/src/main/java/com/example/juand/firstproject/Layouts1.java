package com.example.juand.firstproject;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

//Event manager
public class Layouts1 extends AppCompatActivity {


    //It's like main class of the layout
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Sets the view to show
        setContentView(R.layout.activity_layouts1);
    }
}
