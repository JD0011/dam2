package com.example.juand.firstproject;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class Layouts3 extends AppCompatActivity {

    private EditText num1, num2;
    private TextView result;
    private int num;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_layouts3);

        num1 = (EditText) findViewById(R.id.number1);
        num2 = (EditText) findViewById(R.id.number2);
        result = (TextView) findViewById(R.id.resultado);
    }

    protected void sumar(View e){
        int numero1 = Integer.parseInt(num1.getText().toString());

        num+=numero1;

        result.setText(String.valueOf(num));
    }
}
