/**
 * Layout about elevator. It will how the button clicked in a label and the orientation
 * (updwards or downwards)
 *
 * @author Juan de Dios Delgado Bermúdez
 * @since 03/10/2017
 */

package com.example.juand.firstproject;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

public class Elevator extends AppCompatActivity {

    private TextView floor_value;
    private TextView orientation_value;

    //Stores all detected buttons
    private ArrayList<Button> buttons = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_elevator);

        //Get the textview controls
        floor_value = (TextView) findViewById(R.id.floor_value);
        orientation_value = (TextView) findViewById(R.id.orientation_value);

        //Get all the buttons named as button_XX
        int counter = 0;
        do{
            counter++;

            //Concat button_+counter. This will get the same ID that R class has.
            int idNumber = getResources().getIdentifier("button_"+counter,"id",getPackageName());

            //Add the button to the arraylist
            buttons.add((Button) findViewById(idNumber));

        //While the last button is not null
        }while(buttons.get(counter-1)!=null);

        //Remove the last button because its null
        buttons.remove(counter-1);

        //Listener
        View.OnClickListener buttonClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //v.getId() will return the clicked button
                Button clicked =(Button) findViewById(v.getId());

                //Set the orientation
                if(Integer.parseInt(clicked.getText().toString()) > Integer.parseInt(floor_value.getText().toString())){

                    //Hacia arriba
                    orientation_value.setText("Upwards");

                }else if (Integer.parseInt(clicked.getText().toString()) == Integer.parseInt(floor_value.getText().toString())){

                    orientation_value.setText("Same floor");

                }else{

                    //Hacia abajo
                    orientation_value.setText("Downwards");
                }

                //Set the current floor
                floor_value.setText(clicked.getText().toString());
            }
        };

        //Set the listener
        for(Button a: buttons){
            a.setOnClickListener(buttonClick);
        }
    }

}
