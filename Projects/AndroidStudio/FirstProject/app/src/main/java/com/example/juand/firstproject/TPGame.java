/**
 * Consiste en un juego básico de tragaperras.
 * El jugador comienza con un saldo de 5€. Si pierde, se resta 1, si gana, se suman otros 5.
 * Al quedarse sin saldo, el juego termina y el jugador debe reiniciar partida.
 * Existen tres tipos de dificultad, basándose en los números que saldrán en la ruleta:
 *      · Nivel 1: rango del 1 al 3.
 *      · Nivel 2: rango del 1 al 5.
 *      · Nivel 3: rango del 1 al 10.
 *
 * @author Juan de Dios Delgado Bermúdez
 * @since 06/10/2017
 */

package com.example.juand.firstproject;

public class TPGame{

    //Precio de la jugada
    private final int precio=1;

    //Almacena los valores iniciales
    private final int SALDO_INICIAL;

    //Valores actuales
    private int dificultad_actual;
    private int saldo_actual;

    //Hueco de resultados
    private int[] resultado = new int[3];

    //Numero de jugadas
    private int jugadas;

    //Constructor
    public TPGame(int dificultad, int saldo){
        //Guarda los valores iniciales
        SALDO_INICIAL = saldo;

        //Establece los valores actuales
        dificultad_actual = dificultad;
        saldo_actual = saldo;
    }

    //GETTERS

    public int[] getResults(){
        return resultado;
    }
    public String getSaldo(){ return String.valueOf(saldo_actual); }
    public String getJugadas(){
        return String.valueOf(jugadas);
    }

    //SETTERS
    public void setDificultad_actual(int dificultad){
        dificultad_actual = dificultad;
    }

    /**
     * Este método asigna unos resultados en función de la dificultad elegida, y los almacena en
     * la variable {@link #resultado}
     */
    public void generarResultado(){
        //Aumenta jugadas
        jugadas++;
        switch (dificultad_actual) {
            //Fácil
            case 1:
                for (int i = 0; i < resultado.length; i++) {
                    resultado[i] = (int) (Math.random() * (1 + 3)-1);
                }
                break;
            //Normal
            case 2:
                for (int i = 0; i < resultado.length; i++) {
                    resultado[i] = (int) (Math.random() * (1 + 5)-1);
                }
                break;
            //Dificil
            case 3:
                for (int i = 0; i < resultado.length; i++) {
                    resultado[i] = (int) (Math.random() * (1 + 10)-1);
                }
                break;
        }

    }

    /**
     * Comprobará si los números generados aleatoriamente son los mismos. Si lo son, notificará
     * que la jugada ha sido victoriosa, y si no, que se ha perdido. Además, realizará los cambios
     * pertinentes en el saldo (si gana, se suma 5 euros pero si pierde, se resta 1)
     * @return Resultado de la jugada
     */
    public boolean comprobarJugada(){
        //Si todos los valores son iguales
        if( resultado[0] == resultado[1] && resultado[1] == resultado[2] ){
            //Suma el nuevo saldo
            saldo_actual+=5;
            //Ha ganado
            return true;
        }else{
            if(saldo_actual - precio <1){
                saldo_actual=0;
            }else{
                saldo_actual-=precio;
            }
            //Ha perdido
            return false;
        }
    }

    /**
     * Comprueba el estado del saldo del jugador
     * @return Saldo del jugador en valor booleano
     */
    public boolean tieneSaldo(){
        if(saldo_actual==0){
            return false;
        }
        return true;
    }

    /**
     * Restablecerá los valores de los atributos por los iniciales
     */
    public void reiniciar(){

        saldo_actual = SALDO_INICIAL;
        jugadas = 0;

        for(int i = 0;i<resultado.length;i++){
            resultado[i]=0;
        }
    }
}
