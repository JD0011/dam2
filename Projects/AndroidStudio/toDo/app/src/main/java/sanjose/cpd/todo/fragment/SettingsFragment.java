package sanjose.cpd.todo.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.support.annotation.Nullable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.joda.time.LocalTime;

import java.util.concurrent.atomic.LongAdder;

import sanjose.cpd.todo.R;
import sanjose.cpd.todo.pojo.Settings;
import sanjose.cpd.todo.util.FirebasePath;
import sanjose.cpd.todo.util.LocalTimeFormatter;

public class SettingsFragment extends Fragment {

    private Switch swNotificationsEnabled, swPeriodicNotificationEnabled;
    private TimePicker tpPeriodicTimeNotification;
    private EditText etTimeBeforeNotification;
    private Button btnSave;
    private String msgRangeTime;
    private Switch swAllowInvite;
    private ProgressDialog pdLoaging;
    private Activity activity;
    private LinearLayout llPeriodicNotifications, llTaskNotification;

    private Settings userSettings;
    private boolean badPeriodTime;

    //Firebase BBDD
    private FirebaseDatabase fbDatabase = FirebaseDatabase.getInstance();
    private DatabaseReference fbDatabaseRef = fbDatabase.getReference(FirebasePath.DB_NAME);
    private DatabaseReference fbNotificationSettings;
    private DatabaseReference fbUserRef;

    //Firebase Auth
    private FirebaseUser fbCurrentUser = FirebaseAuth.getInstance().getCurrentUser();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_settings, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        activity = getActivity();

        //Pantalla
        swAllowInvite = (Switch) view.findViewById(R.id.sw_allow_invite);
        swNotificationsEnabled = (Switch) view.findViewById(R.id.sw_notifications);
        swPeriodicNotificationEnabled = (Switch) view.findViewById(R.id.sw_periodic_notifications);
        tpPeriodicTimeNotification = (TimePicker) view.findViewById(R.id.tp_time);
        etTimeBeforeNotification = (EditText) view.findViewById(R.id.et_time_before_notification);
        llPeriodicNotifications = (LinearLayout) view.findViewById(R.id.ll_periodic_notification_settings);
        llTaskNotification = (LinearLayout) view.findViewById(R.id.ll_notification_settings);
        btnSave = (Button) view.findViewById(R.id.btn_save);

        pdLoaging = new ProgressDialog(activity);

        userSettings = new Settings();
        badPeriodTime = false;

        fbUserRef = fbDatabaseRef.child(FirebasePath.MAIN_USERS).child(fbCurrentUser.getUid());
        fbNotificationSettings = fbUserRef.child(FirebasePath.USER_SETTINGS);
        loadUserSettings();

        pdLoaging.setOnCancelListener(dialogInterface -> {
            //Al cancelar, vuelve al dialog de listado de proyectos
            getFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fl_content, new ProjectListingFragment())
                    .commit();
        });

        msgRangeTime =
                getString(R.string.error_time_must_be_in_range) +
                        " " +
                        Settings.MIN_TIME_BEFORE_NOTIFICATE_TASK +
                        " " +
                        getString(R.string.lbl_and) +
                        " " +
                        Settings.MAX_TIME_BEFORE_NOTIFICATE_TASK;


        setListeners();
    }

    private void setListeners() {
        etTimeBeforeNotification.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                int number;
                try {
                    number = Integer.parseInt(etTimeBeforeNotification.getText().toString());
                } catch (Exception e) {
                    number = Settings.DEFAULT_TIME_BEFORE_NOTIFICATE_TASK;
                    etTimeBeforeNotification.setError(getString(R.string.error_must_be_number));
                    badPeriodTime = true;
                }

                //Si el tiempo previo para mostrar la notificación está en el rango adecuado, registralo
                if (number >= Settings.MIN_TIME_BEFORE_NOTIFICATE_TASK && number <= Settings.MAX_TIME_BEFORE_NOTIFICATE_TASK) {
                    userSettings.setMinutsBeforeTaskTimeNotification(number);
                    badPeriodTime = false;
                } else {
                    etTimeBeforeNotification.setError(msgRangeTime);
                    badPeriodTime = true;
                }
            }
        });

        swAllowInvite.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean enabled) {
                //Establece permiso para ser invitado
                userSettings.setAllowInvite(enabled);
            }
        });

        swNotificationsEnabled.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean enabled) {
                //Activa las notifiaciones de tareas
                userSettings.setEnableTasksNotification(enabled);
                llTaskNotification.setVisibility(enabled ? View.VISIBLE : View.GONE);
            }
        });

        swPeriodicNotificationEnabled.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean enabled) {
                //Activa las notificaciones diarias
                userSettings.setEnablePeriodicNotification(enabled);
                llPeriodicNotifications.setVisibility(enabled ? View.VISIBLE : View.GONE);
            }
        });

        tpPeriodicTimeNotification.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
            @Override
            public void onTimeChanged(TimePicker timePicker, int hour, int minute) {
                LocalTime auxLocalTime = new LocalTime(hour, minute);
                userSettings.setDatetimeNotification(LocalTimeFormatter.fromLocalTimeToFirebaseFormat(auxLocalTime));
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (userSettings.isEnableTasksNotification() && badPeriodTime) {
                    etTimeBeforeNotification.setError(msgRangeTime);
                } else {
                    pdLoaging.setMessage(getString(R.string.lbl_saving_settings));
                    pdLoaging.show();

                    fbNotificationSettings.setValue(userSettings, (databaseError, databaseReference) -> {
                        pdLoaging.hide();
                        if (databaseError != null) {
                            Toast.makeText(activity, R.string.error_settings_unsaved, Toast.LENGTH_SHORT).show();
                            loadUserSettings();
                        } else {
                            Toast.makeText(activity, R.string.lbl_settings_saved, Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        });
    }

    private void loadUserSettings() {

        //Carga los ajustes del usuario
        fbNotificationSettings.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                pdLoaging.show();
                //No hagas nada si no existen datos almacenados
                if (dataSnapshot.getValue() == null) {
                    return;
                }

                Settings notSettings = dataSnapshot.getValue(Settings.class);

                swAllowInvite.setChecked(notSettings.isAllowInvite());
                swNotificationsEnabled.setChecked(notSettings.isEnableTasksNotification());
                swPeriodicNotificationEnabled.setChecked(notSettings.isEnablePeriodicNotification());

                LocalTime auxLocalTime = LocalTimeFormatter.fromFirebaseFormatToLocalTime(notSettings.getDatetimeNotification());
                tpPeriodicTimeNotification.setCurrentHour(auxLocalTime.getHourOfDay());
                tpPeriodicTimeNotification.setCurrentMinute(auxLocalTime.getMinuteOfHour());

                etTimeBeforeNotification.setText(String.valueOf(notSettings.getMinutsBeforeTaskTimeNotification()));

                pdLoaging.hide();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
