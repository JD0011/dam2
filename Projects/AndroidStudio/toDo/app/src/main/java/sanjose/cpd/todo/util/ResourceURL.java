package sanjose.cpd.todo.util;

import java.net.URI;

/**
 * Contiene todas las direcciones WEB que apuntan hacia algún recurso almacenado en la red
 */

public interface ResourceURL {

    //Manual de usuario
    String TUTORIAL = "https://drive.google.com/file/d/1SQPIt7w0dfEm-lUOJuRVglFXQxvqDKCF/view";
}
