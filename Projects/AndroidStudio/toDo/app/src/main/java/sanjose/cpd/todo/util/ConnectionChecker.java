package sanjose.cpd.todo.util;

import android.app.AlertDialog;
import android.arch.lifecycle.ViewModelProvider;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.Looper;

import sanjose.cpd.todo.R;

/**
 * Se encarga de comprobar si la conexión a internet está disponible. Esto es necesario ya que
 * la aplicación está en constante conexión con internet
 */

public class ConnectionChecker extends Thread {

    //Constantes
    private static final long TIMEOUT = 7000;

    private boolean exitCondition;
    private boolean dialogIsShowing;

    private ConnectivityManager connectivityManager;
    private AlertDialog.Builder adBuilder;
    private AlertDialog alertDialog;

    private Handler dialogHandler;

    public ConnectionChecker(Context ctx) {
        exitCondition = false;
        dialogIsShowing = false;

        adBuilder = new AlertDialog.Builder(ctx);
        connectivityManager = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);

        initialize();
    }

    private void initialize() {
    }

    /**
     * Permite detener el hilo cambiando el flag que lo mantiene activo
     */
    public void stopThread() {
        exitCondition = true;
    }

    @Override
    public void run() {
        while (!exitCondition) {
            if (connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() != NetworkInfo.State.CONNECTED &&
                    connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() != NetworkInfo.State.CONNECTED) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    public void run() {
                        if(!exitCondition){
                            alertDialog = adBuilder
                                    .setMessage(R.string.error_connection)
                                    .setPositiveButton(R.string.ok, (dialogInterface, i) -> {
                                        //Nada
                                    })
                                    .setCancelable(false)
                                    .show();
                        }
                        exitCondition = true;
                    }
                });
            }
        }
    }
}
