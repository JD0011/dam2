package sanjose.cpd.todo.adapter.recyclerview;

import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import sanjose.cpd.todo.R;
import sanjose.cpd.todo.listener.RecyclerViewItemClickListener;
import sanjose.cpd.todo.pojo.Project;
import sanjose.cpd.todo.util.FirebasePath;

/**
 * Adapter de proyectos para el RecyclerView
 */

public class ProjectAdapter extends RecyclerView.Adapter<ProjectAdapter.ProjectViewHolder> {

    //Conjunto de datos
    private ArrayList<Project> projectsList;
    private RecyclerViewItemClickListener rvClickListener;

    //Firebase BBDD
    private FirebaseDatabase fbDatabase = FirebaseDatabase.getInstance();
    private DatabaseReference fbDatabaseRef = fbDatabase.getReference(FirebasePath.DB_NAME);
    private DatabaseReference fbTasksRef = fbDatabaseRef.child(FirebasePath.MAIN_PROJECTS);


    //ViewHolder para cada uno de los elementos mostrados en el RecycleView
    public class ProjectViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        CardView cv;
        TextView tvName, tvDescription, tvTasksTodo, tvTasksLabel;
        int tasks, taskToDo;

        public ProjectViewHolder(View view) {
            super(view);
            cv = view.findViewById(R.id.cv_project);
            tvName = view.findViewById(R.id.tv_project_name);
            tvDescription = view.findViewById(R.id.tv_index);
            tvTasksTodo = view.findViewById(R.id.tv_tasks_value);
            tvTasksLabel = view.findViewById(R.id.tv_tasks);
            itemView.setOnClickListener(this);

            fbTasksRef = fbDatabaseRef.child(FirebasePath.MAIN_TASKS);
        }

        public void bind(Project pr) {
            tvName.setText(pr.getName());
            tvDescription.setText(pr.getDescription());

            //Obtén las tareas que tiene que realziar este usuario con respecto al proyecto
            fbTasksRef.child(pr.getName()).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    tasks = 0;
                    taskToDo = 0;
                    for (DataSnapshot dsStage : dataSnapshot.getChildren()) {
                        for (DataSnapshot dsTask : dsStage.getChildren()) {
                            //Suma solo las tareas suyas o comúnes
                            if (dsTask.child(FirebasePath.TASK_OWNER).getValue().equals(FirebaseAuth.getInstance().getCurrentUser().getEmail()) ||
                                    dsTask.child(FirebasePath.TASK_FOR_ALL_USERS).getValue(Boolean.class)) {
                                tasks++;
                                //Cuenta las no finalizadas
                                if (!dsTask.child(FirebasePath.TASK_FINISHED).getValue(Boolean.class)) {
                                    taskToDo++;
                                }
                            }
                        }
                    }

                    if(taskToDo==0){
                        tvTasksLabel.setVisibility(View.INVISIBLE);
                        tvTasksTodo.setVisibility(View.INVISIBLE);
                    }else{
                        String msgTasks = taskToDo + " / " + tasks;
                        tvTasksTodo.setText(msgTasks);
                        tvTasksLabel.setVisibility(View.VISIBLE);
                        tvTasksTodo.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }

        @Override
        public void onClick(View view) {
            rvClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    //Constructor preparado para recibir la fuente de datos y el listener obligatoriamente
    public ProjectAdapter(ArrayList<Project> projectsList, RecyclerViewItemClickListener rvClickListener) {
        this.projectsList = projectsList;
        this.rvClickListener = rvClickListener;
    }

    @NonNull
    @Override
    public ProjectViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_projects_template, parent, false);
        final ProjectViewHolder vh = new ProjectViewHolder(v);
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rvClickListener.onItemClick(view, vh.getAdapterPosition());
            }
        });

        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull ProjectViewHolder holder, int position) {
        holder.bind(projectsList.get(position));
        holder.tvName.setText(projectsList.get(position).getName());
        holder.tvDescription.setText(projectsList.get(position).getDescription());
    }

    @Override
    public int getItemCount() {
        return projectsList.size();
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }
}
