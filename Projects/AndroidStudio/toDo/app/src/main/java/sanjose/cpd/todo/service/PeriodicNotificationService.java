package sanjose.cpd.todo.service;

import android.app.IntentService;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.joda.time.LocalTime;

import java.util.ArrayList;
import java.util.concurrent.LinkedBlockingDeque;

import br.com.goncalves.pugnotification.notification.PugNotification;
import sanjose.cpd.todo.R;
import sanjose.cpd.todo.activity.MainActivity;
import sanjose.cpd.todo.activity.ProjectActivity;
import sanjose.cpd.todo.pojo.Settings;
import sanjose.cpd.todo.pojo.Task;
import sanjose.cpd.todo.util.FirebasePath;
import sanjose.cpd.todo.util.LocalDateTimeFormatter;
import sanjose.cpd.todo.util.LocalTimeFormatter;

/**
 * Servicio de notificaciones sobre tareas
 */

public class PeriodicNotificationService extends IntentService {

    private static final int NOTIFICATION_ID = 2;
    private static final String SERVICE_NAME = "PeriodicNotificationService";

    private ArrayList<Task> userTasks;
    private ArrayList<Task> periodicTasks;
    private ArrayList<String> userTasksNames;

    //Firebase BBDD
    private FirebaseDatabase fbDatabase = FirebaseDatabase.getInstance();
    private DatabaseReference fbDatabaseRef = fbDatabase.getReference(FirebasePath.DB_NAME);
    private DatabaseReference fbMembersRef;
    private DatabaseReference fbTasksRef;
    private DatabaseReference fbNotificationSettings;

    private Boolean hasShownNotification = false;

    private Settings userSettings;
    //Firebase User
    private FirebaseUser fbCurrentUser = FirebaseAuth.getInstance().getCurrentUser();

    public PeriodicNotificationService() {
        super(SERVICE_NAME);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        userTasks = new ArrayList<>();
        userTasksNames = new ArrayList<>();

        userSettings = new Settings();

        fbMembersRef = fbDatabaseRef.child(FirebasePath.MAIN_MEMBERS);
        fbTasksRef = fbDatabaseRef.child(FirebasePath.MAIN_TASKS);
        fbNotificationSettings = fbDatabaseRef.child(FirebasePath.MAIN_USERS).child(fbCurrentUser.getUid()).child(FirebasePath.USER_SETTINGS);

        //Carga las tareas comúnes y de usuario de los proyectos a los que pertenece
        fbMembersRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                //Obtención de rol en el proyecto
                DatabaseReference fbProyectMembREF = fbMembersRef.child(dataSnapshot.getKey());

                fbProyectMembREF.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        for (DataSnapshot dsStage : dataSnapshot.getChildren()) {
                            for (DataSnapshot dsMember : dataSnapshot.getChildren()) {

                                //Si el usuario pertenece al proyecto
                                if (dsMember.getKey().equalsIgnoreCase(fbCurrentUser.getUid())) {

                                    //Referencia a Tasks/<ProjectKey>
                                    DatabaseReference fbProjectTaskRef = fbTasksRef.child(fbProyectMembREF.getKey());
                                    fbProjectTaskRef.addValueEventListener(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(DataSnapshot dataSnapshot) {
                                            for (DataSnapshot dsStage : dataSnapshot.getChildren()) {
                                                for (DataSnapshot dsTask : dsStage.getChildren()) {
                                                    Task task = dsTask.getValue(Task.class);
                                                    //Si no se ha añadido ya la tarea y es común o pertenece al usuario actual, añádela
                                                    if (!userTasksNames.contains(task.getName()) && (task.getForAllUsers() || task.getOwner().equalsIgnoreCase(fbCurrentUser.getEmail()))) {
                                                        userTasksNames.add(task.getName());
                                                        userTasks.add(task);
                                                    }
                                                    /**
                                                     if (!userTasks.contains(task) && (task.getForAllUsers() || task.getOwner().equalsIgnoreCase(fbCurrentUser.getUid()))) {
                                                     userTasks.add(task);
                                                     }**/
                                                }
                                            }
                                        }

                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {

                                        }
                                    });
                                }
                            }
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }

                });
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        //Carga la configuración de las notificaciones
        fbNotificationSettings.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                //Procede solo si hay datos almacenados
                if (dataSnapshot.getValue() != null) {
                    userSettings = (Settings) dataSnapshot.getValue(Settings.class);
                    hasShownNotification = false;
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void onStart(@Nullable Intent intent, int startId) {
        super.onStart(intent, startId);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        periodicTasks = new ArrayList<>();

        while (true) {
            //Comprueba si la notificación periódica está activa y busca las tareas correspondientes del día
            if (userSettings.isEnablePeriodicNotification()) {
                LocalTime auxPeriodicNotificationTime = LocalTimeFormatter.fromFirebaseFormatToLocalTime(userSettings.getDatetimeNotification());
                //Si es la hora de lanzar la notificación periódica
                if (auxPeriodicNotificationTime.hourOfDay().equals(LocalTime.now().hourOfDay()) && auxPeriodicNotificationTime.minuteOfHour().equals(LocalTime.now().minuteOfHour())) {
                    for (Task t : userTasks) {
                        LocalDateTime auxLDT = LocalDateTimeFormatter.fromFirebaseFormatToLocalDate(t.getDateTime());
                        LocalDate auxLocalDate = auxLDT.toLocalDate();
                        LocalTime auxLocalTime = auxLDT.toLocalTime();
                        if (auxLocalDate.equals(LocalDate.now()) && auxLocalTime.isAfter(LocalTime.now())) {
                            periodicTasks.add(t);
                        }
                    }
                    if (!hasShownNotification) {
                        showAllTaskForDayNotification(periodicTasks);
                        periodicTasks.clear();
                        hasShownNotification = true;
                    }

                }

            }

            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }

    /**
     * Muestra una notificación correspondiente a las tareazs que debe realizar ese día
     *
     * @param tasks Tareas a realizar
     */
    private void showAllTaskForDayNotification(ArrayList<Task> tasks) {
        String msg, tasksDescription;
        if (tasks.size() == 0) {
            msg = getString(R.string.lbl_any_tasks_today);
        } else {
            msg =   getString(R.string.lbl_number_tasks) + " " + tasks.size() + " " + getString(R.string.lbl_tasks)+"."+
                    "\n"+getString(R.string.lbl_check_proyects)+" :\n";

            for (Task t : tasks) {
                if (!msg.contains(t.getProjectID())) {
                    msg += t.getProjectID() + "\n";
                }
            }
        }


        Intent showTaskIntent = new Intent(getApplicationContext(), MainActivity.class);
        showTaskIntent.setAction(Intent.ACTION_MAIN);
        showTaskIntent.addCategory(Intent.CATEGORY_LAUNCHER);
        showTaskIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        PendingIntent contentIntent = PendingIntent.getActivity(
                getApplicationContext(),
                0,
                showTaskIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        PugNotification.with(getApplicationContext())
                .load()
                .title(R.string.lbl_your_periodic_notification)
                .bigTextStyle(msg)
                .vibrate(new long[]{500})
                .smallIcon(R.drawable.ic_assignment)
                .largeIcon(R.drawable.ic_assignment)
                .flags(Notification.DEFAULT_ALL | Notification.DEFAULT_VIBRATE)
                .button(R.drawable.ic_assignment, getString(R.string.lbl_check_out), contentIntent)
                .simple()
                .build();
    }

}