package sanjose.cpd.todo.adapter.listview;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import sanjose.cpd.todo.R;
import sanjose.cpd.todo.pojo.UserRol;
import sanjose.cpd.todo.util.StringFormatter;

/**
 * Muestra una lista con información del usuario y el rol que le corresponde en un proyecto
 * determinado
 */
public class UserRolAdapter extends ArrayAdapter {

    private Activity context;
    private ArrayList<UserRol> users;

    public UserRolAdapter(Activity context, ArrayList<UserRol> users) {
        super(context, R.layout.listview_textview_template, users);
        this.context = context;
        this.users = users;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View item = inflater.inflate(R.layout.listview_user_rol_template, parent, false);

        TextView tvMail = (TextView)item.findViewById(R.id.tv_mail);
        tvMail.setText(users.get(position).getMail());

        TextView tvRol = (TextView) item.findViewById(R.id.tv_rol);
        tvRol.setText(StringFormatter.toCapitalCase(users.get(position).getRol()));

        return item;
    }
}
