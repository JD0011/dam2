package sanjose.cpd.todo.adapter.recyclerview.timeline;

import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

/**
 * Delegado de tipo de vista
 */

public interface ViewTypeDelegateAdapter {

    RecyclerView.ViewHolder onCreateViewHolder (ViewGroup parent);
    void onBindViewHolder (RecyclerView.ViewHolder holder, TimeLineType item);
}
