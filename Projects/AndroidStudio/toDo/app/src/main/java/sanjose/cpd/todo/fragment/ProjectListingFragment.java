package sanjose.cpd.todo.fragment;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Iterator;

import sanjose.cpd.todo.R;
import sanjose.cpd.todo.activity.ProjectActivity;
import sanjose.cpd.todo.activity.TutorialActivity;
import sanjose.cpd.todo.adapter.recyclerview.ProjectAdapter;
import sanjose.cpd.todo.listener.RecyclerViewItemClickListener;
import sanjose.cpd.todo.pojo.Project;
import sanjose.cpd.todo.util.FirebasePath;

/**
 * Fragment que permite visualizar los proyectos en formato de lista
 */

public class ProjectListingFragment extends Fragment {

    private ArrayList<Project> projectList;
    private Button btnCreateProject;

    private ProgressBar pbLoad;

    //RecyclerView
    private RecyclerView rvProjects;
    private ProjectAdapter prAdapter;
    private LinearLayoutManager lmForRecycler;

    //Firebase BBDD
    private FirebaseDatabase fbDatabase = FirebaseDatabase.getInstance();
    private DatabaseReference fbDatabaseRef = fbDatabase.getReference(FirebasePath.DB_NAME);
    private DatabaseReference fbProjectMembersRef = fbDatabaseRef.child(FirebasePath.MAIN_MEMBERS);
    private DatabaseReference fbProjectsRef = fbDatabaseRef.child(FirebasePath.MAIN_PROJECTS);

    //User
    private String userIdentifier = FirebaseAuth.getInstance().getCurrentUser().getUid();

    public ProjectListingFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_projects_listing, container, false);
        setHasOptionsMenu(true);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //Pantalla
        rvProjects = (RecyclerView) view.findViewById(R.id.rview_projects);
        pbLoad = (ProgressBar) view.findViewById(R.id.pb_loading);
        pbLoad.setIndeterminate(true);

        //Tamaño fijo
        rvProjects.setHasFixedSize(true);

        lmForRecycler = new LinearLayoutManager(view.getContext());
        rvProjects.setLayoutManager(lmForRecycler);

        projectList = new ArrayList<>();
        prAdapter = new ProjectAdapter(projectList, new RecyclerViewItemClickListener() {
            @Override
            public void onItemClick(View v, int position) {
                String projectKey = projectList.get(position).getName();
                Intent i = new Intent(view.getContext(), ProjectActivity.class);
                i.putExtra("project_key", projectKey);
                startActivity(i);
            }
        });

        rvProjects.setAdapter(prAdapter);

        setupListeners();

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_projects_listing, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.it_add_project:
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.fl_content, new CreateProjectFragment())
                        .addToBackStack(null)
                        .commit();

                break;
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void setupListeners() {
        ArrayList<String> projectsID = new ArrayList<>();

        //Carga los datos de los proyectos del usuario
        fbProjectMembersRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                //Refresca la lista de proyectos a los que el usuario pertenece
                projectsID.clear();
                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    if (ds.child(userIdentifier).exists()) {
                        //Evita duplicados
                        if (!projectsID.contains(ds.getKey())) {
                            projectsID.add(ds.getKey());
                        }
                    }
                }

                //Limpia los proyectos de los que el usuario ya no sea miembro
                Iterator<Project> projectIterator = projectList.iterator();
                while(projectIterator.hasNext()){
                    Project currProject = projectIterator.next();
                    if (!projectsID.contains(currProject.getName())) {
                        projectIterator.remove();
                    }
                }

                //Obtén los datos de los proyectos
                fbProjectsRef.addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                        boolean alreadyShown = false;
                        if (projectsID.contains(dataSnapshot.getKey())) {
                            pbLoad.setVisibility(View.VISIBLE);
                            Project auxProject = dataSnapshot.getValue(Project.class);

                            for (Project p : projectList) {
                                if (p.getName().equalsIgnoreCase(auxProject.getName())) {
                                    alreadyShown = true;
                                    break;
                                }
                            }

                            if (!alreadyShown) {
                                projectList.add(auxProject);
                            }

                            if (projectList.size() == projectsID.size()) {
                                prAdapter.notifyDataSetChanged();
                                pbLoad.setVisibility(View.GONE);
                            }
                        }
                        prAdapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                        if (projectsID.contains(dataSnapshot.getKey())) {
                            Project auxProject = dataSnapshot.getValue(Project.class);
                            //Se itera entre projectos. Nota: remove normal no funciona
                            for (Project p : projectList) {
                                if (p.getName().equalsIgnoreCase(auxProject.getName())) {
                                    projectList.remove(p);
                                    projectList.add(p);
                                    prAdapter.notifyDataSetChanged();
                                    break;
                                }
                            }

                        }
                        prAdapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {
                        Project auxProject = dataSnapshot.getValue(Project.class);
                        Iterator<Project> projectIterator = projectList.iterator();
                        while(projectIterator.hasNext()){
                            Project currProject = projectIterator.next();
                            if(currProject.getName().equalsIgnoreCase(auxProject.getName())){
                                projectIterator.remove();
                                projectsID.remove(currProject.getId());
                                break;
                            }
                        }

                        prAdapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}