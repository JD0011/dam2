package sanjose.cpd.todo.util;

import org.joda.time.DateTime;
import org.joda.time.LocalDateTime;
import org.joda.time.LocalTime;

import java.util.ArrayList;

/**
 * Convierte de LocalTime a formato en Firebase
 */

public class LocalTimeFormatter {

    public static ArrayList<Integer> fromLocalTimeToFirebaseFormat(LocalTime localTime) {
        ArrayList<Integer> time = new ArrayList<>();

        time.add(localTime.getHourOfDay());
        time.add(localTime.getMinuteOfHour());
        time.add(localTime.getSecondOfMinute());

        return time;
    }

    public static LocalTime fromFirebaseFormatToLocalTime(ArrayList<Integer> date) {
        LocalTime localTime = null;

        if (date.size() == 3) {
            localTime = new LocalTime(date.get(0), date.get(1));
        }

        return localTime;
    }
}
