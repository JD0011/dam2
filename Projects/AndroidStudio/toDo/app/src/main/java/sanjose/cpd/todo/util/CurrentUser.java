package sanjose.cpd.todo.util;

import sanjose.cpd.todo.pojo.User;

/**
 * Almacena los datos del usuario en una clase accesible desde cualquier parte del proyecto
 */

public abstract class CurrentUser {

    public static String name, mail, id;

    public void setDataFromUser(User user){
        name = user.getName();
        mail = user.getMail();
        id = user.getId();
    }
}
