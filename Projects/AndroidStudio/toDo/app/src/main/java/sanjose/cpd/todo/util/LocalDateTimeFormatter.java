package sanjose.cpd.todo.util;

import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;

import java.util.ArrayList;

/**
 * Se encarga de ofrecer distintos formatos de fecha, los cuales se han ido desarrollando en
 * función de las necesidades de interpretación de las fechas basandose en el sistema en el que
 * se utilizan
 */

public class LocalDateTimeFormatter {

    public static final String DATE_FORMAT = "yyyy/MM/dd";

    public static ArrayList<Integer> fromLocalDateToFirebaseFormat(LocalDateTime localDate) {
        ArrayList<Integer> date = new ArrayList<>();

        date.add(localDate.getYear());
        date.add(localDate.getMonthOfYear());
        date.add(localDate.getDayOfMonth());
        date.add(localDate.getHourOfDay());
        date.add(localDate.getMinuteOfHour());

        return date;
    }

    public static LocalDateTime fromFirebaseFormatToLocalDate(ArrayList<Integer> date) {
        LocalDateTime localDate = null;

        if (date.size() == 5) {
            localDate = new LocalDateTime(date.get(0), date.get(1), date.get(2),date.get(3), date.get(4));
        }

        return localDate;
    }
}
