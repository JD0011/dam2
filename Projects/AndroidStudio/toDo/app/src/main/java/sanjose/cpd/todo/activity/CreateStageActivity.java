package sanjose.cpd.todo.activity;

import android.app.DatePickerDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import org.joda.time.Days;
import org.joda.time.Hours;
import org.joda.time.LocalDate;

import java.util.Calendar;
import java.util.HashMap;

import sanjose.cpd.todo.R;
import sanjose.cpd.todo.pojo.Stage;
import sanjose.cpd.todo.util.DateFormatter;
import sanjose.cpd.todo.util.FirebasePath;
import sanjose.cpd.todo.util.StringFormatter;

/**
 * Permite crear una etapa nueva en el proyecto desde el que se invoque la instancia de esta clase
 */
public class CreateStageActivity extends AppCompatActivity implements View.OnClickListener {

    //Constantes
    private static final int DAYS_BETWEEN_START_AND_END = 2;

    private Calendar cNow;
    private DatePickerDialog dpdCalendar;
    private LocalDate ldStartDate, ldEndDate;

    private TextView tvDaysValue, tvHoursValue;
    private EditText etName, etDescription;
    private Button btnStartDate, btnEndDate, btnSave;
    private String projectName, stageName, stageDescription, nameError, descriptionError;
    private boolean badName = true;
    private boolean badDescription = true;
    private boolean dataOK = false;
    private boolean nameInUse = false;

    private String errorMsgDaysBetween;

    //Firebase BBDD
    private FirebaseDatabase fbDatabase = FirebaseDatabase.getInstance();
    private DatabaseReference fbDatabaseRef = fbDatabase.getReference(FirebasePath.DB_NAME);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_stage);

        //Pantalla
        etName = (EditText) findViewById(R.id.et_name);
        etDescription = (EditText) findViewById(R.id.et_description);
        btnStartDate = (Button) findViewById(R.id.btn_start_date);
        btnEndDate = (Button) findViewById(R.id.btn_end_date);
        btnSave = (Button) findViewById(R.id.btn_save);
        tvDaysValue = (TextView) findViewById(R.id.tv_days_value);
        tvHoursValue = (TextView) findViewById(R.id.tv_hours_value);

        errorMsgDaysBetween = getString(R.string.error_minimun_days_bt_dates) + " " + DAYS_BETWEEN_START_AND_END;

        btnSave.setOnClickListener(this);

        Bundle extras = getIntent().getExtras();

        projectName = extras.getString("project_key", "");

        cNow = Calendar.getInstance();
        Calendar cStartDate = Calendar.getInstance();
        Calendar cEndDate = Calendar.getInstance();

        nameError = StringFormatter.getLengthErrorMessageFrom(getApplicationContext(), R.string.error_name_must_be_between, Stage.STAGE_NAME_MIN_LENGTH, Stage.STAGE_NAME_MAX_LENGTH);
        descriptionError = StringFormatter.getLengthErrorMessageFrom(getApplicationContext(), R.string.error_description_must_be_between, Stage.STAGE_DESCRIPTION_MIN_LENGTH, Stage.STAGE_DESCRIPTION_MAX_LENGTH);

        setValidators();

        DatePickerDialog.OnDateSetListener startDateListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                LocalDate auxDate = new LocalDate(year, monthOfYear + 1, dayOfMonth);


                //Asigna la fecha final automáticamente si está vacía
                if (ldEndDate == null || auxDate.plusDays(DAYS_BETWEEN_START_AND_END).isAfter(ldEndDate)) {
                    ldStartDate = auxDate;
                    ldEndDate = auxDate.plusDays(DAYS_BETWEEN_START_AND_END);
                    btnEndDate.setText(ldEndDate.toString());
                    Toast.makeText(getApplicationContext(), errorMsgDaysBetween, Toast.LENGTH_SHORT).show();

                    cStartDate.set(Calendar.YEAR, year);
                    cStartDate.set(Calendar.MONTH, monthOfYear);
                    cStartDate.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                    btnStartDate.setText(ldStartDate.toString());

                    //Notifica la cntidad de días y horas seleccionadas
                    tvDaysValue.setText(String.valueOf(Days.daysBetween(ldStartDate, ldEndDate).getDays()));
                    tvHoursValue.setText(String.valueOf(Hours.hoursBetween(ldStartDate, ldEndDate).getHours()));
                }

                if (auxDate.isBefore(LocalDate.now())) {
                    Toast.makeText(getApplicationContext(), R.string.error_date_before_now, Toast.LENGTH_SHORT).show();
                } else if (ldEndDate != null && auxDate.isAfter(ldEndDate)) {
                    Toast.makeText(getApplicationContext(), R.string.error_startdate_after_enddate, Toast.LENGTH_SHORT).show();
                } else if (ldEndDate != null && !auxDate.plusDays(DAYS_BETWEEN_START_AND_END).isBefore(ldEndDate)) {
                    Toast.makeText(getApplicationContext(), errorMsgDaysBetween, Toast.LENGTH_SHORT).show();
                } else {
                    ldStartDate = auxDate;

                    cStartDate.set(Calendar.YEAR, year);
                    cStartDate.set(Calendar.MONTH, monthOfYear);
                    cStartDate.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                    btnStartDate.setText(ldStartDate.toString());

                    //Notifica la cntidad de días y horas seleccionadas
                    tvDaysValue.setText(String.valueOf(Days.daysBetween(ldStartDate, ldEndDate).getDays()));
                    tvHoursValue.setText(String.valueOf(Hours.hoursBetween(ldStartDate, ldEndDate).getHours()));
                }

            }
        };

        DatePickerDialog.OnDateSetListener endDateListener = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                LocalDate auxDate = new LocalDate(year, monthOfYear + 1, dayOfMonth);

                if (ldEndDate.isBefore(ldStartDate) || ldEndDate.isBefore(LocalDate.now())) {
                    Toast.makeText(getApplicationContext(), R.string.error_end_date, Toast.LENGTH_LONG).show();
                } else if (!ldEndDate.minusDays(DAYS_BETWEEN_START_AND_END).isAfter(ldStartDate)) {
                    Toast.makeText(getApplicationContext(), errorMsgDaysBetween, Toast.LENGTH_LONG).show();
                } else {
                    ldEndDate = auxDate;
                    cEndDate.set(Calendar.YEAR, year);
                    cEndDate.set(Calendar.MONTH, monthOfYear);
                    cEndDate.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                    btnEndDate.setText(ldEndDate.toString());

                    //Notifica la cntidad de días y horas seleccionadas
                    tvDaysValue.setText(String.valueOf(Days.daysBetween(ldStartDate, ldEndDate).getDays()));
                    tvHoursValue.setText(String.valueOf(Hours.hoursBetween(ldStartDate, ldEndDate).getHours()));
                }
            }
        };

        btnStartDate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dpdCalendar = new DatePickerDialog(CreateStageActivity.this, startDateListener,
                        cStartDate.get(Calendar.YEAR), cNow.get(Calendar.MONTH),
                        cStartDate.get(Calendar.DAY_OF_MONTH));
                dpdCalendar.show();
            }
        });

        btnEndDate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dpdCalendar = new DatePickerDialog(CreateStageActivity.this, endDateListener,
                        cStartDate.get(Calendar.YEAR), cNow.get(Calendar.MONTH),
                        cStartDate.get(Calendar.DAY_OF_MONTH));
                dpdCalendar.show();
            }
        });
    }

    /**
     * Establece reglas de validación para los campos de nombre y descripción
     */
    private void setValidators() {
        etName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                stageName = etName.getText().toString().trim();

                if (stageName.length() < Stage.STAGE_NAME_MIN_LENGTH || stageName.length() > Stage.STAGE_NAME_MAX_LENGTH) {
                    badName = true;
                    etName.setError(nameError);
                } else {
                    badName = false;

                    Query qNameAlreadyExists = fbDatabaseRef
                            .child(FirebasePath.MAIN_PROJECTS)
                            .child(projectName)
                            .child(FirebasePath.MAIN_STAGES)
                            .orderByChild(FirebasePath.COMMON_NAME)
                            .equalTo(stageName);

                    qNameAlreadyExists.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            if (dataSnapshot.getValue() != null) {
                                nameInUse = true;
                                etName.setError(getString(R.string.error_name_in_use));
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                }


            }
        });

        etDescription.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                stageDescription = etDescription.getText().toString().trim();

                if (stageDescription.length() < Stage.STAGE_DESCRIPTION_MIN_LENGTH || stageDescription.length() > Stage.STAGE_DESCRIPTION_MAX_LENGTH) {
                    badDescription = true;
                    etDescription.setError(descriptionError);
                } else {
                    badDescription = false;
                }
            }
        });
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.btn_save:
                if (validateData()) {
                    createStage();
                }
                break;
        }
    }

    /**
     * Se encarga de grabar la etapa con los datos establecidos en la base de datos de Firebase
     */
    private void createStage() {
        DatabaseReference fbStagesRef = fbDatabaseRef.child(FirebasePath.MAIN_PROJECTS).child(projectName).child(FirebasePath.MAIN_STAGES).child(stageName);

        String UniqueID = fbStagesRef.getKey();

        Stage stg = new Stage(
                UniqueID,
                projectName,
                stageName,
                stageDescription,
                new HashMap<String, String>(),
                DateFormatter.fromLocalDateFirebaseFormat(ldStartDate),
                DateFormatter.fromLocalDateFirebaseFormat(ldEndDate));

        fbStagesRef.setValue(stg, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                if (databaseError == null) {
                    Toast.makeText(getApplicationContext(), R.string.lbl_stage_added, Toast.LENGTH_SHORT).show();
                    finish();
                } else {
                    Toast.makeText(getApplicationContext(), R.string.error_something_wrong, Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    /**
     * Comprueba que los datos introducidos cumplan las reglas de validación establecidas y muestra
     * el error en el caso que corresponda
     * @return True si los datos son correctos, False de lo contrario
     */
    private boolean validateData() {
        dataOK = !badName && !badDescription && ldStartDate != null && ldEndDate != null && !nameInUse;
        if (!dataOK) {
            if (badName) {
                etName.setError(nameError);
            }

            if (badDescription) {
                etDescription.setError(descriptionError);
            }

            if (ldStartDate == null || ldEndDate == null) {
                Toast.makeText(getApplicationContext(), R.string.error_must_set_dates, Toast.LENGTH_SHORT).show();
            }

            if (nameInUse) {
                etName.setError(getString(R.string.error_name_in_use));
            }
        }

        return dataOK;
    }

}
