package sanjose.cpd.todo.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;

/**
 * Implementación de FragmentPagerAdapter que ofrece la posibilidad de pasar extras (Arguments, en
 * caso de los fragments)
 */

public class BundlePageAdapter extends FragmentPagerAdapter {

    private ArrayList<String> fragments;
    private Bundle extras;
    private Context ctx;

    public BundlePageAdapter(FragmentManager fm, Context ctx, ArrayList<String> fragments, Bundle extras) {
        super(fm);
        this.fragments = fragments;
        this.extras = extras;
        this.ctx = ctx;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment frg = Fragment.instantiate(ctx, fragments.get(position));
        frg.setArguments(extras);

        return frg;
    }

    @Override
    public int getCount() {
        return fragments.size();
    }
}
