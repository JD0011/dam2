package sanjose.cpd.todo.adapter.recyclerview.timeline;

import android.app.Activity;
import android.app.AlertDialog;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import sanjose.cpd.todo.R;
import sanjose.cpd.todo.adapter.recyclerview.timeline.delegate.DateItemDelegateAdapter;
import sanjose.cpd.todo.adapter.recyclerview.timeline.delegate.HeaderItemDelegateAdapter;
import sanjose.cpd.todo.adapter.recyclerview.timeline.delegate.ItemDelegateAdapter;
import sanjose.cpd.todo.adapter.recyclerview.timeline.type.DayView;
import sanjose.cpd.todo.comparator.TimeLineViewComparator;
import sanjose.cpd.todo.listener.TaskItemClickListener;
import sanjose.cpd.todo.pojo.Stage;
import sanjose.cpd.todo.pojo.Task;
import sanjose.cpd.todo.util.FirebasePath;
import sanjose.cpd.todo.util.LocalDateTimeFormatter;

/**
 * Adapter que permite crear un TimeLine a partir de Stages y Tareas, permitiendo crear una vista
 * para visualizar la información correspondiente de forma directa
 * Created by juand on 30/05/2018.
 */

public class TimeLineAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<TimeLineType> items;
    private ArrayList<DayView> days;
    private ArrayList<LocalDate> dates;
    private Activity activity;
    private ProgressBar pbLoading;
    private SparseArray<ViewTypeDelegateAdapter> delegateAdapters;

    //Firebase BBDD
    private FirebaseDatabase fbDatabase = FirebaseDatabase.getInstance();
    private DatabaseReference fbDatabaseRef = fbDatabase.getReference(FirebasePath.DB_NAME);
    private DatabaseReference fbTasksRefs = fbDatabaseRef.child(FirebasePath.MAIN_TASKS);

    //Firebase Authentication
    private FirebaseUser fbCurrentUser = FirebaseAuth.getInstance().getCurrentUser();

    public TimeLineAdapter(Activity activity, HashMap<String, String> usersData) {
        this.items = new ArrayList<>();
        this.days = new ArrayList<>();
        this.dates = new ArrayList<>();
        this.activity = activity;

        pbLoading = new ProgressBar(activity);

        delegateAdapters = new SparseArray<>();
        delegateAdapters.put(TimeLineType.HEADER, new HeaderItemDelegateAdapter());
        delegateAdapters.put(TimeLineType.DATE, new DateItemDelegateAdapter());

        delegateAdapters.put(TimeLineType.ITEM, new ItemDelegateAdapter(usersData, new TaskItemClickListener(){
            @Override
            public void onLongItemClick(View v, int position) {
                TimeLineType tlt = items.get(position);
                if(tlt instanceof Task){
                    Task auxTask = (Task) tlt;
                    AlertDialog.Builder adBuilder = new AlertDialog.Builder(activity);
                    AlertDialog adDeleteUser = adBuilder.create();

                    adDeleteUser.setTitle(R.string.lbl_delete_task);
                    adDeleteUser.setButton(AlertDialog.BUTTON_POSITIVE, activity.getString(R.string.yes), (dialogInterface, i1) -> {
                        pbLoading.setVisibility(View.VISIBLE);
                        deleteTask(auxTask);
                    });
                    adDeleteUser.setButton(AlertDialog.BUTTON_NEGATIVE, activity.getString(R.string.no), (dialogInterface, i1) -> {
                        //Nada
                    });
                    adDeleteUser.show();
                }
            }

            @Override
            public void onFinishedButtonClick(Task t, ToggleButton btn) {
                DatabaseReference fbTaskRef = fbTasksRefs.child(t.getProjectID()).child(t.getStageID()).child(t.getName());
                fbTaskRef.child(FirebasePath.TASK_FINISHED).setValue(btn.isChecked(), (databaseError, databaseReference) -> {
                    if(databaseError!=null){
                        Toast.makeText(activity, R.string.error_something_wrong, Toast.LENGTH_SHORT).show();
                    }else{
                        if(btn.isChecked()){
                            fbTaskRef.child(FirebasePath.TASK_OWNER).setValue(fbCurrentUser.getEmail(), (databaseError1, databaseReference1) -> {
                                if(databaseError1!=null){
                                    Toast.makeText(activity, R.string.error_something_wrong, Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                    }
                });
            }
        }));
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return delegateAdapters.get(viewType).onCreateViewHolder(parent);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        delegateAdapters.get(getItemViewType(position)).onBindViewHolder(holder, items.get(position));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public int getItemViewType(int position) {
        return items.get(position).getViewType();
    }

    /**
     * Añade el cabecero del timelineview. En este caso, una stage
     *
     * @param stage Stage a visualizar
     */
    public void addStageHeader(Stage stage) {
        items.add(stage);
        notifyDataSetChanged();
    }

    /**
     * Añade información sobre las fechas de las tareas y el tiempo asignado
     *
     * @param dv Información temporal de la tarea a visualizar
     */
    public void addDayItem(DayView dv) {
        days.add(dv);
        items.add(dv);
        organizeItemsByDate();
    }

    /**
     * Añade una tarea al timelineview, visualizandose como un elemento más, sin ningún tipo
     * de jerarquía
     *
     * @param task Task a visualizar
     */
    public void addTaskItem(Task task) {
        //Agregala a la lista
        LocalDate auxDate = LocalDateTimeFormatter.fromFirebaseFormatToLocalDate(task.getDateTime()).toLocalDate();
        if (!dates.contains(auxDate)) {
            dates.add(auxDate);
            addDayItem(new DayView(auxDate));
        }
        items.add(task);
        organizeItemsByDate();
    }

    /**
     * Elimina una tarea del TimelineView
     *
     * @param taskName Nombre de tarea a eliminar
     */
    public void removeTaskItem(String taskName) {
        for (TimeLineType t : items) {
            if (t instanceof Task) {
                Task auxTask = (Task) t;
                if (auxTask.getName().equalsIgnoreCase(taskName)) {
                    items.remove(t);
                    break;
                }
            }
        }

        deleteDaysWithoutTasks();
        organizeItemsByDate();
    }

    /**
     * Ordena aquellos elementos que estén desordenados mediante la fecha
     */
    private void organizeItemsByDate() {
        Collections.sort(items, new TimeLineViewComparator());
        notifyDataSetChanged();
    }

    /**
     * Comprueba si quedan tareas para un día determinado. Si no quedan, elimina la vista de ese
     * día del TimelineView, ya que no representaría a ninguna tarea
     */
    private void deleteDaysWithoutTasks() {
        boolean hasTaskForThatDay;

        //Por cada vista de Día
        for (DayView dv : days) {
            hasTaskForThatDay = false;
            //Busca si entre las tareas
            for (TimeLineType t : items) {
                if (t instanceof Task) {
                    Task auxTask = (Task) t;
                    LocalDateTime ldt = LocalDateTimeFormatter.fromFirebaseFormatToLocalDate(auxTask.getDateTime());

                    //Hay alguna para ese día
                    if (ldt.toLocalDate().equals(dv.getLocalDate())) {
                        hasTaskForThatDay = true;
                        break;
                    }
                }
            }

            //Si no hay tareas para ese día, eliminalo
            if (!hasTaskForThatDay) {
                items.remove(dv);
                dates.remove(dv.getLocalDate());
            }
        }
    }

    /**
     * Elimina la tarea especificada
     * @param task Tarea a eliminar
     */
    private void deleteTask(Task task) {
        fbTasksRefs.child(task.getProjectID()).child(task.getStageID()).child(task.getName()).removeValue(new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                pbLoading.setVisibility(View.GONE);
                if(databaseError == null){
                    Toast.makeText(activity.getApplicationContext(), R.string.lbl_success_task_delete, Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(activity.getApplicationContext(), R.string.error_something_wrong, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


    public ArrayList<TimeLineType> getItems() {
        return items;
    }
}
