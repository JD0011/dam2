package sanjose.cpd.todo.adapter.recyclerview.timeline.delegate;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;

import sanjose.cpd.todo.R;
import sanjose.cpd.todo.adapter.recyclerview.timeline.TimeLineType;
import sanjose.cpd.todo.adapter.recyclerview.timeline.ViewTypeDelegateAdapter;
import sanjose.cpd.todo.listener.RecyclerViewItemClickListener;
import sanjose.cpd.todo.listener.TaskItemClickListener;
import sanjose.cpd.todo.pojo.Task;
import sanjose.cpd.todo.util.FirebasePath;
import sanjose.cpd.todo.util.LocalDateTimeFormatter;

/**
 * Representa una tarea en el TimelineView, proporcionando los datos necesarios para completar
 * el layout de tarea del recycledview
 * Created by juand on 30/05/2018.
 */

public class ItemDelegateAdapter implements ViewTypeDelegateAdapter {

    private HashMap<String, String> proyectMembers;
    private TaskItemClickListener rvClickListener;

    public ItemDelegateAdapter(HashMap<String, String> proyectMembers, TaskItemClickListener rvClickListener) {
        this.proyectMembers = proyectMembers;
        this.rvClickListener = rvClickListener;
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder implements TaskItemClickListener {

        //Firebase BBDD
        FirebaseDatabase fbDatabase = FirebaseDatabase.getInstance();
        DatabaseReference fbDatabaseRef = fbDatabase.getReference(FirebasePath.DB_NAME);
        DatabaseReference fbTaskRef;

        TextView tvDescripcion, tvHour, tvOwner, tvOwnerString, tvDoneBy;
        CardView cvForAllUsers, cvOwner;
        Task task;
        ToggleButton btnDone;

        public ItemViewHolder(View itemView) {
            super(itemView);

            tvDescripcion = itemView.findViewById(R.id.tv_index);
            tvHour = itemView.findViewById(R.id.tv_hour_value);
            tvOwner = itemView.findViewById(R.id.tv_owner);
            tvOwnerString = itemView.findViewById(R.id.tv_owner_value);
            tvDoneBy = itemView.findViewById(R.id.tv_done_by);
            cvForAllUsers = (CardView) itemView.findViewById(R.id.cv_forallusers);
            cvOwner = (CardView) itemView.findViewById(R.id.cv_owner);
            btnDone = (ToggleButton) itemView.findViewById(R.id.btn_done);
        }

        public void bind(Task task) {
            this.task = task;
            fbTaskRef = fbDatabaseRef.child(FirebasePath.MAIN_TASKS).child(task.getProjectID()).child(task.getStageID()).child(task.getName());

            tvDescripcion.setText(task.getDescription());
            tvHour.setText(LocalDateTimeFormatter.fromFirebaseFormatToLocalDate(task.getDateTime()).toString(Task.TIME_PATTERN));

            if (task.getForAllUsers() == true) {
                cvForAllUsers.setVisibility(View.VISIBLE);
                if (!task.isFinished()) {
                    cvOwner.setVisibility(View.GONE);
                } else {
                    cvOwner.setVisibility(View.VISIBLE);
                }

            } else {
                cvForAllUsers.setVisibility(View.INVISIBLE);
            }

            if (!task.isFinished()) {
                tvOwner.setVisibility(View.VISIBLE);
                tvDoneBy.setVisibility(View.GONE);
            } else {
                tvOwner.setVisibility(View.GONE);
                tvDoneBy.setVisibility(View.VISIBLE);
            }

            String username = "";
            for (Map.Entry<String, String> e : proyectMembers.entrySet()) {
                if (e.getKey().equalsIgnoreCase(task.getOwner())) {
                    username = e.getValue();
                    break;
                }
            }

            fbTaskRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if(dataSnapshot.getValue()!=null){
                        Task task = dataSnapshot.getValue(Task.class);
                        btnDone.setChecked(task.isFinished());
                        tvOwnerString.setText(task.getOwner());
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

            tvOwnerString.setText(username);

        }

        @Override
        public void onLongItemClick(View v, int position) {
            rvClickListener.onLongItemClick(v, position);
        }

        @Override
        public void onFinishedButtonClick(Task t, ToggleButton button) {
            rvClickListener.onFinishedButtonClick(t, button);
        }
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.timeline_task_item, parent, false);
        final ItemViewHolder vh = new ItemViewHolder(v);

        v.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                rvClickListener.onLongItemClick(view, vh.getAdapterPosition());
                return true;
            }
        });

        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, TimeLineType item) {
        ((ItemViewHolder) holder).bind((Task) item);
        ((ItemViewHolder) holder).btnDone.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        rvClickListener.onFinishedButtonClick((Task)item, ((ItemViewHolder) holder).btnDone);
                    }
                }

        );
    }
}
