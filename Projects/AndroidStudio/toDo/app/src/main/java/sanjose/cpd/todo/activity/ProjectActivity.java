package sanjose.cpd.todo.activity;

import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.loicteillard.easytabs.EasyTabs;

import java.util.ArrayList;

import sanjose.cpd.todo.R;
import sanjose.cpd.todo.fragment.BundlePageAdapter;
import sanjose.cpd.todo.fragment.ProjectInfoFragment;
import sanjose.cpd.todo.fragment.ProjectMembersFragment;
import sanjose.cpd.todo.fragment.ProjectStagesFragment;
import sanjose.cpd.todo.fragment.StageTasksFragment;
import sanjose.cpd.todo.fragment.TaskVisualizationFragment;
import sanjose.cpd.todo.util.FirebasePath;
import sanjose.cpd.todo.util.StringFormatter;

/**
 * Activity que visualiza información de un proyecto determinado
 */
public class ProjectActivity extends AppCompatActivity {

    //Constantes
    public static final int TAB_PROJECT_INFO = 0;
    public static final int TAB_STAGES = 1;
    public static final int TAB_TASKS = 2;
    public static final int TAB_MEMBERS = 3;

    private String projectKey;
    private int tabNumber;
    private ArrayList<String> fragmentsList;
    private Menu menu;

    //Firebase DDBB
    private FirebaseDatabase fbDatabase = FirebaseDatabase.getInstance();
    private DatabaseReference fbDatabaseRef = fbDatabase.getReference(FirebasePath.DB_NAME);
    private DatabaseReference fbProjectRef, fbMembersRef;

    //Firebase Authentication
    private FirebaseUser fbCurrentUser = FirebaseAuth.getInstance().getCurrentUser();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project);

        projectKey = getIntent().getExtras().getString("project_key", "");
        tabNumber = getIntent().getExtras().getInt("tab_number", TAB_PROJECT_INFO);


        fbMembersRef = fbDatabaseRef.child(FirebasePath.MAIN_MEMBERS);
        fbProjectRef = fbDatabaseRef.child(FirebasePath.MAIN_PROJECTS).child(projectKey);

        setTitle(StringFormatter.toCapitalCase(projectKey));

        //OJO: El adapter debe tener la misma cantidad de fragments que la lista (Ver layout)
        fragmentsList = new ArrayList<>();
        fragmentsList.add(ProjectInfoFragment.class.getName());
        fragmentsList.add(ProjectStagesFragment.class.getName());
        fragmentsList.add(TaskVisualizationFragment.class.getName());
        fragmentsList.add(ProjectMembersFragment.class.getName());

        Bundle extras = new Bundle();
        extras.putString("project_key", projectKey);
        BundlePageAdapter pagerAdapter = new BundlePageAdapter(getSupportFragmentManager(), getApplicationContext(), fragmentsList, extras);

        EasyTabs easyTabs = findViewById(R.id.easytabs);
        ViewPager viewpager = findViewById(R.id.vp_content);

        viewpager.setAdapter(pagerAdapter);
        easyTabs.setViewPager(viewpager, tabNumber);
        easyTabs.setPagerListener(new EasyTabs.PagerListener() {
            @Override
            public void onTabSelected(int index) {
                if(index!=TAB_STAGES){
                    ProjectStagesFragment frStageFragment = new ProjectStagesFragment();
                    frStageFragment.setArguments(extras);

                    //Si el StageTaskFragment está activo y se cambia de tab, elimínalo de la cola
                    if(getSupportFragmentManager().findFragmentById(R.id.cl_root) instanceof StageTasksFragment){
                        getSupportFragmentManager().popBackStack();
                    }
                }
            }
        });

        setProjectListener();
    }

    /**
     * Sacará al usuario de la pantalla del proyecto en las casuísticas determinadas
     */
    private void setProjectListener() {
        //Si el proyecto es eliminado
        fbProjectRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.getValue()==null){
                    if(!isFinishing()){
                        finish();
                    }
                    Toast.makeText(ProjectActivity.this, R.string.error_project_has_been_deleted, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        //Si el usuario ya no es miembro
        fbMembersRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                boolean isMember = false;
                for(DataSnapshot dsProject: dataSnapshot.getChildren()){
                    if(dsProject.getKey().equalsIgnoreCase(projectKey)){
                        for(DataSnapshot dsMember: dsProject.getChildren()){
                            if(dsMember.getKey().equalsIgnoreCase(fbCurrentUser.getUid())){
                                isMember = true;
                            }
                        }
                    }
                }

                if(!isMember){
                    Toast.makeText(ProjectActivity.this, R.string.lbl_no_longer_member, Toast.LENGTH_SHORT).show();
                    if(!isFinishing()){
                        finish();
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        getMenuInflater().inflate(R.menu.menu_general, menu);
        //Oculta los botones inecesarios para esta activity
        for(int i=0;i<menu.size();i++){
            if(menu.getItem(i).getItemId()!=R.id.it_goback){
                menu.getItem(i).setVisible(false);
            }
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.it_goback:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
