package sanjose.cpd.todo.pojo;

import org.joda.time.LocalTime;

import java.util.ArrayList;

import sanjose.cpd.todo.util.LocalTimeFormatter;

/**
 * Define propiedades de notificaciones para el usuario
 * Created by juand on 09/06/2018.
 */

public class Settings {

    //Constantes
    public static final int DEFAULT_TIME_BEFORE_NOTIFICATE_TASK = 5;
    public static final int MIN_TIME_BEFORE_NOTIFICATE_TASK = 0;
    public static final int MAX_TIME_BEFORE_NOTIFICATE_TASK = 60;
    public static final LocalTime DEFAULT_TIME_NOTIFICATION = new LocalTime(10,0,0);

    //Ajustes generales
    private boolean allowInvite;

    //Ajustes de notificaciones
    private boolean enableTasksNotification;
    private int minutsBeforeTaskTimeNotification;
    private boolean enablePeriodicNotification;
    private ArrayList<Integer> datetimeNotification;


    public Settings(){
        this.allowInvite = true;
        this.enablePeriodicNotification = true;
        this.minutsBeforeTaskTimeNotification = DEFAULT_TIME_BEFORE_NOTIFICATE_TASK;
        this.enableTasksNotification = true;
        this.datetimeNotification = LocalTimeFormatter.fromLocalTimeToFirebaseFormat(DEFAULT_TIME_NOTIFICATION);
    }

    public Settings(boolean allowInvite, boolean enableTasksNotification, int minutsBeforeTaskTimeNotification, boolean enablePeriodicNotification, ArrayList<Integer> datetime_notification) {
        this.allowInvite = allowInvite;
        this.enableTasksNotification = enableTasksNotification;
        this.minutsBeforeTaskTimeNotification = minutsBeforeTaskTimeNotification;
        this.enablePeriodicNotification = enablePeriodicNotification;
        this.datetimeNotification = datetime_notification;
    }

    public boolean isAllowInvite() {
        return allowInvite;
    }

    public void setAllowInvite(boolean allowInvite) {
        this.allowInvite = allowInvite;
    }

    public boolean isEnableTasksNotification() {
        return enableTasksNotification;
    }

    public void setEnableTasksNotification(boolean enableTasksNotification) {
        this.enableTasksNotification = enableTasksNotification;
    }

    public int getMinutsBeforeTaskTimeNotification() {
        return minutsBeforeTaskTimeNotification;
    }

    public void setMinutsBeforeTaskTimeNotification(int minutsBeforeTaskTimeNotification) {
        this.minutsBeforeTaskTimeNotification = minutsBeforeTaskTimeNotification;
    }

    public boolean isEnablePeriodicNotification() {
        return enablePeriodicNotification;
    }

    public void setEnablePeriodicNotification(boolean enablePeriodicNotification) {
        this.enablePeriodicNotification = enablePeriodicNotification;
    }

    public ArrayList<Integer> getDatetimeNotification() {
        return datetimeNotification;
    }

    public void setDatetimeNotification(ArrayList<Integer> datetimeNotification) {
        this.datetimeNotification = datetimeNotification;
    }
}
