package sanjose.cpd.todo.fragment;

import android.graphics.Color;
import android.support.annotation.Nullable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.joda.time.LocalDate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import jp.wasabeef.blurry.Blurry;
import pl.hypeapp.materialtimelineview.MaterialTimelineView;
import sanjose.cpd.todo.R;
import sanjose.cpd.todo.adapter.recyclerview.timeline.TimeLineAdapter;
import sanjose.cpd.todo.adapter.recyclerview.timeline.TimeLineType;
import sanjose.cpd.todo.dialog.TaskCreationDialog;
import sanjose.cpd.todo.listener.RecyclerViewItemClickListener;
import sanjose.cpd.todo.pojo.Stage;
import sanjose.cpd.todo.pojo.Task;
import sanjose.cpd.todo.util.DateFormatter;
import sanjose.cpd.todo.util.FirebasePath;
import sanjose.cpd.todo.util.Rol;

/**
 * Muestra en formato de línea temporal la Stage y las tareas correspondientes a la pasada
 * mediante el bundle
 */
public class StageTasksFragment extends Fragment {

    private Stage stageRoot;
    private ArrayList<Task> tasksList;
    private ArrayList<LocalDate> daysList;

    private ProgressBar pbLoading;

    private HashMap<String, String> proyectMembers;

    private String currentUserID;
    private String currentUserRol;

    private AlertDialog.Builder adBuilder;

    private TimeLineAdapter tmAdapter;
    private RecyclerView rvTasks;

    //Firebase BBDD
    private FirebaseDatabase fbDatabase = FirebaseDatabase.getInstance();
    private DatabaseReference fbDatabaseRef = fbDatabase.getReference(FirebasePath.DB_NAME);
    private DatabaseReference fbStageTasksRef;
    private DatabaseReference fbTasksRefs;

    //Firebase Autentication
    private FirebaseAuth fbAuth = FirebaseAuth.getInstance();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_tasks_visualization, container, false);
        setHasOptionsMenu(true);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Bundle extras = getArguments();
        stageRoot = extras.getParcelable("stage");
        loadProyectMembers();

        pbLoading = (ProgressBar) view.findViewById(R.id.pb_loading);
        pbLoading.setIndeterminate(true);


        currentUserID = fbAuth.getCurrentUser().getUid();
        //Referencia a la stage de la base de datos
        fbTasksRefs = fbDatabaseRef.child(FirebasePath.MAIN_TASKS);
        fbStageTasksRef = fbTasksRefs.child(stageRoot.getProjectID()).child(stageRoot.getName());

        //Pantalla
        rvTasks = (RecyclerView) view.findViewById(R.id.rv_tasks);

        adBuilder = new AlertDialog.Builder(getActivity());

        LinearLayoutManager llManager = new LinearLayoutManager(getActivity().getApplicationContext());

        proyectMembers = new HashMap<>();

        tmAdapter = new TimeLineAdapter(getActivity(), proyectMembers);
        tasksList = new ArrayList<>();
        daysList = new ArrayList<>();

        rvTasks.setAdapter(tmAdapter);
        rvTasks.setLayoutManager(llManager);

        tmAdapter.addStageHeader(stageRoot);

        setStageListeners();

        view.setBackgroundColor(getResources().getColor(R.color.common_google_signin_btn_text_light_pressed));
    }

    /**
     * Carga los miembros del proyecto, necesarios para representar el TimelineView
     */
    private void loadProyectMembers() {
        DatabaseReference fbProyectMembers = fbDatabaseRef.child(FirebasePath.MAIN_MEMBERS).child(stageRoot.getProjectID());
        fbProyectMembers.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    proyectMembers.put(ds.getKey(), ds.child(FirebasePath.COMMON_MAIL).getValue().toString());

                    if (ds.getKey().equalsIgnoreCase(currentUserID)) {
                        currentUserRol = ds.child(FirebasePath.COMMON_ROL).getValue().toString();
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    /**
     * Se encarga de leer las tareas que pertenecen a la stage que se está visualizando
     */
    private void setStageListeners() {
        //Listener de movimiento de datos
        fbStageTasksRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                boolean alreadyShown = false;
                Task auxTask = dataSnapshot.getValue(Task.class);
                if (currentUserRol.equalsIgnoreCase(Rol.ADMIN)) {
                    for (Task t : tasksList) {
                        if (t.getName().equalsIgnoreCase(auxTask.getName())) {
                            alreadyShown = true;
                            break;
                        }
                    }
                    if (!alreadyShown) {
                        tasksList.add(auxTask);
                        tmAdapter.addTaskItem(auxTask);
                    }
                } else if (auxTask.getForAllUsers() == true || auxTask.getOwner().equalsIgnoreCase(fbAuth.getCurrentUser().getEmail())) {
                    for (Task t : tasksList) {
                        if (t.getName().equalsIgnoreCase(auxTask.getName())) {
                            alreadyShown = true;
                            break;
                        }
                    }
                    if (!alreadyShown) {
                        tasksList.add(auxTask);
                        tmAdapter.addTaskItem(auxTask);
                    }
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                Task task = dataSnapshot.getValue(Task.class);
                tmAdapter.removeTaskItem(task.getName());
                tmAdapter.addTaskItem(task);
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                Task task = dataSnapshot.getValue(Task.class);
                Iterator<Task> taskIterator = tasksList.iterator();
                //Nota: remove normal no funciona. Objetos no son iguales
                while(taskIterator.hasNext()){
                    Task currTask = taskIterator.next();
                    if(task.getName().equalsIgnoreCase(currTask.getName())){
                        taskIterator.remove();
                        tmAdapter.removeTaskItem(currTask.getName());
                    }
                }
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.menu_general, menu);
        for (int i = 0; i < menu.size(); i++) {
            //Oculta todos los botones menos los deseados
            if (menu.getItem(i).getItemId() == R.id.it_add_task || menu.getItem(i).getItemId() == R.id.it_goback) {
                menu.getItem(i).setVisible(true);
            } else {
                menu.getItem(i).setVisible(false);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            //Añade una nueva tarea
            case R.id.it_add_task:
                //Comprueba que crea la tarea en el antes del fin de la etapa
                LocalDate endDate = DateFormatter.fromFirebaseFormatToLocalDate(stageRoot.getEndDate());
                if (LocalDate.now().isAfter(endDate)) {
                    String msgDate = getString(R.string.lbl_you_trying_before_date) + " (" + endDate.toString() + ")";
                    adBuilder
                            .setMessage(msgDate)
                            .setPositiveButton(R.string.ok, (dialogInterface, i) -> {
                                //Nada
                            })
                            .show();
                } else {
                    //Muestra diálogo de creación de tarea
                    TaskCreationDialog tcd = new TaskCreationDialog(getActivity(), stageRoot);
                    tcd.show();
                    break;
                }
        }
        return super.onOptionsItemSelected(item);
    }
}
