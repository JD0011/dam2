package sanjose.cpd.todo.adapter.recyclerview.timeline.type;


import org.joda.time.LocalDate;

import sanjose.cpd.todo.adapter.recyclerview.timeline.TimeLineType;
import sanjose.cpd.todo.pojo.Task;

/**
 * Permite representar un divisor en el TimelineView representando un día concreto
 */
public class DayView  implements TimeLineType {

    private LocalDate dateTime;

    public DayView(LocalDate dateTime){
        this.dateTime = dateTime;
    }

    public LocalDate getLocalDate(){
        return dateTime;
    }

    @Override
    public int getViewType(){
        return TimeLineType.DATE;
    }
}