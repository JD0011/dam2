package sanjose.cpd.todo.pojo;

import android.os.Parcel;
import android.os.Parcelable;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import sanjose.cpd.todo.adapter.recyclerview.timeline.TimeLineType;

/**
 * Etapa
 */

public class Stage implements TimeLineType, Parcelable {

    //Stage constants
    public static final int STAGE_NAME_MAX_LENGTH = 15;
    public static final int STAGE_NAME_MIN_LENGTH = 4;
    public static final int STAGE_DESCRIPTION_MAX_LENGTH = 140;
    public static final int STAGE_DESCRIPTION_MIN_LENGTH = 10;

    private String id;
    private String projectID;
    private String name;
    private String description;
    private HashMap<String, String> notes;
    private List<String> tasks;
    private ArrayList<Integer> startDate;
    private ArrayList<Integer> endDate;

    public Stage() {

    }

    public Stage(String id, String projectID, String name, String description, HashMap<String, String> notes, ArrayList<Integer> startDate, ArrayList<Integer> endDate) {
        this.id = id;
        this.projectID = projectID;
        this.name = name;
        this.description = description;
        this.notes = notes;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public Stage(String id, String projectID, String name, String description, HashMap<String, String> notes, ArrayList<Integer> startDate, ArrayList<Integer> endDate, ArrayList<String> tasks) {
        this.id = id;
        this.projectID = projectID;
        this.name = name;
        this.description = description;
        this.startDate = startDate;
        this.endDate = endDate;

        this.tasks = tasks;
    }

    /**
     * Crea Stage a partir de un parcel. Este constructor se utiliza al pasar un objeto de esta
     * clase entre fragments
     * @param in Fuente de datos
     */
    public Stage (Parcel in){
        String[] stringProperties = new String[5];
        in.readStringArray(stringProperties);
        this.id = stringProperties[0];
        this.projectID = stringProperties[1];
        this.name = stringProperties[3];
        this.description = stringProperties[4];

        ArrayList<Integer> startDate = new ArrayList<>();
        in.readList(startDate, null);
        this.startDate = startDate;

        ArrayList<Integer> endDate= new ArrayList<>();
        in.readList(endDate, null);
        this.endDate = endDate;

        ArrayList<String> tasks= new ArrayList<>();
        in.readList(tasks, null);
        this.tasks = tasks;

        HashMap<String, String> notes = new HashMap<>();
        in.readMap(notes, null);
        this.notes = notes;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProjectID() {
        return projectID;
    }

    public void setProjectID(String projectID) {
        this.projectID = projectID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public HashMap<String, String> getNotes() {
        return notes;
    }

    public void setNotes(HashMap<String, String> notes) {
        this.notes = notes;
    }

    public List<String> getTasks() {
        return tasks;
    }

    public void setTasks(List<String> tasks) {
        this.tasks = tasks;
    }

    public ArrayList<Integer> getStartDate() {
        return startDate;
    }

    public void setStartDate(ArrayList<Integer> startDate) {
        this.startDate = startDate;
    }

    public ArrayList<Integer> getEndDate() {
        return endDate;
    }

    public void setEndDate(ArrayList<Integer> endDate) {
        this.endDate = endDate;
    }

    @Override
    public int getViewType() {
        return TimeLineType.HEADER;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeStringArray(new String[]{
                this.id,
                this.projectID,
                this.name,
                this.description
        });
        parcel.writeMap(notes);
        parcel.writeList(startDate);
        parcel.writeList(endDate);
    }

    /**
     * Permite leer la información de un parcelable
     */
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {

        @Override
        public Stage createFromParcel(Parcel parcel) {
            return new Stage(parcel);
        }

        @Override
        public Stage[] newArray(int i) {
            return new Stage[i];
        }
    };
}