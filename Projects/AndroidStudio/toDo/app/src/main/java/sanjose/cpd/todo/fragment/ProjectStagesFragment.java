package sanjose.cpd.todo.fragment;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.Iterator;

import sanjose.cpd.todo.R;
import sanjose.cpd.todo.activity.CreateStageActivity;
import sanjose.cpd.todo.adapter.listview.StageListAdapter;
import sanjose.cpd.todo.pojo.Stage;
import sanjose.cpd.todo.util.FirebasePath;

/**
 * Muestra las etapas de un proyecto determinado
 */

public class ProjectStagesFragment extends Fragment implements View.OnClickListener {//Objetos para listview: stages

    private ListView lvStages;
    private StageListAdapter stageAdapter;
    private ArrayList<Stage> stagesList;
    private ArrayList<Stage> stagesToDelete;
    private Menu deleteMenu;

    private AdapterView.OnItemClickListener stageSelectionListener, deleteListener;

    private boolean deleteModeEnabled;

    private String projectKey;

    private AlertDialog.Builder adBuilder;

    //Firebase BBDD
    private FirebaseDatabase fbDatabase = FirebaseDatabase.getInstance();
    private DatabaseReference fbDatabaseRef = fbDatabase.getReference(FirebasePath.DB_NAME);
    private DatabaseReference fbStagesRef;
    private DatabaseReference fbTasksRef;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_project_stages, container, false);
        setHasOptionsMenu(true);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //Obtención de Key de proyecto
        Bundle extras = getArguments();

        projectKey = extras.getString("project_key");
        fbStagesRef = fbDatabaseRef.child(FirebasePath.MAIN_PROJECTS).child(projectKey).child(FirebasePath.MAIN_STAGES);
        fbTasksRef = fbDatabaseRef.child(FirebasePath.MAIN_TASKS);
        lvStages = (ListView) view.findViewById(R.id.lv_stages);

        view.findViewById(R.id.btn_add).setOnClickListener(this);

        stagesToDelete = new ArrayList<>();
        stagesList = new ArrayList<>();
        stageAdapter = new StageListAdapter(getActivity(), stagesList);
        lvStages.setAdapter(stageAdapter);
        adBuilder = new AlertDialog.Builder(getContext());

        setListeners();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_add:
                //Lanza layout de creación de Stages
                Intent i = new Intent(getActivity(), CreateStageActivity.class);
                i.putExtra("project_key", projectKey);
                startActivity(i);
                break;
        }
    }

    private void setListeners() {

        lvStages.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                //Carga el fragment con la stage correspondiente para visualizar las tareas
                Bundle extras = new Bundle();
                extras.putParcelable("stage", stagesList.get(i));

                //Carga los miembros del proyecto
                StageTasksFragment tvfragment = new StageTasksFragment();
                tvfragment.setArguments(extras);

                FragmentManager fmanager = getFragmentManager();

                fmanager.beginTransaction()
                        .replace(R.id.cl_root, tvfragment)
                        .addToBackStack(null)
                        .commit();
            }
        });

        lvStages.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int index, long l) {
                adBuilder
                        .setMessage(R.string.lbl_are_sure_delete_stage)
                        .setNegativeButton(R.string.no, (dialogInterface, i1) -> {
                            //nada
                        })
                        .setPositiveButton(R.string.yes, (dialogInterface, i1) -> {
                            deleteStage(stagesList.get(index));
                        })
                        .show();

                return true;
            }
        });

        fbStagesRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                boolean alreadyShown=false;
                Stage stg = dataSnapshot.getValue(Stage.class);
                //Evita duplicidad
                for (Stage stage : stagesList) {
                    if (stage.getName().equalsIgnoreCase(stg.getName())) {
                        alreadyShown = true;
                        break;
                    }
                }
                if(!alreadyShown){
                    stageAdapter.add(stg);
                }
                stageAdapter.notifyDataSetChanged();
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                Stage stg = dataSnapshot.getValue(Stage.class);
                for (Stage stage : stagesList) {
                    if (stage.getName().equalsIgnoreCase(stg.getName())) {
                        stageAdapter.remove(stage);
                        stageAdapter.add(stage);
                        break;
                    }
                }
                stageAdapter.notifyDataSetChanged();
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                Stage stg = dataSnapshot.getValue(Stage.class);
                Iterator<Stage> stageIterator = stagesList.iterator();
                while(stageIterator.hasNext()){
                    Stage currStage = stageIterator.next();
                    if (currStage.getName().equalsIgnoreCase(stg.getName())) {
                        stageIterator.remove();
                        stageAdapter.remove(currStage);
                        break;
                    }
                }

                stageAdapter.notifyDataSetChanged();
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.menu_proyect_stages, menu);
        for (int i = 0; i < menu.size(); i++) {
            //Oculta todos los botones menos los deseados
            if (menu.getItem(i).getItemId() == R.id.it_goback) {
                menu.getItem(i).setVisible(true);
            } else {
                menu.getItem(i).setVisible(false);
            }

            deleteMenu = menu;
        }
    }

    /**
     * Elimina la tarea especificada
     *
     * @param stage Stage a eliminar
     */
    private void deleteStage(Stage stage) {
        fbTasksRef.child(stage.getProjectID()).child(stage.getId()).removeValue(new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                if (databaseError != null) {
                    Toast.makeText(getActivity().getApplicationContext(), R.string.error_something_wrong, Toast.LENGTH_SHORT);
                } else {
                    fbStagesRef.child(stage.getName()).removeValue((databaseError1, databaseReference1) -> {
                        if (databaseError != null) {
                            Toast.makeText(getActivity().getApplicationContext(), R.string.error_something_wrong, Toast.LENGTH_SHORT);
                        } else {
                            stageAdapter.remove(stage);
                            stagesToDelete.remove(stage);

                            if (stagesToDelete.size() == 0) {
                                Toast.makeText(getActivity().getApplicationContext(), R.string.lbl_stages_deleted, Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }
            }
        });
    }
}
