package sanjose.cpd.todo.activity;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import sanjose.cpd.todo.R;
import sanjose.cpd.todo.fragment.ProjectListingFragment;
import sanjose.cpd.todo.fragment.SettingsFragment;
import sanjose.cpd.todo.pojo.Settings;
import sanjose.cpd.todo.service.TaskNotificationService;
import sanjose.cpd.todo.service.PeriodicNotificationService;
import sanjose.cpd.todo.util.ConnectionChecker;
import sanjose.cpd.todo.util.FirebasePath;

/**
 * Actividad principal de la aplicación. Se encarga de cargar la vista inicial
 */
public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private Menu menu;
    private AlertDialog.Builder adBuilder;
    private Context ctx;

    private TextView tvUser, tvMail;
    private ProjectListingFragment plFragment;
    private SettingsFragment settingsFragment;

    //Firebase DDBB
    private FirebaseDatabase fbDatabase = FirebaseDatabase.getInstance();
    private DatabaseReference fbDatabaseRef = fbDatabase.getReference(FirebasePath.DB_NAME);
    private DatabaseReference fbNotificationSettings;

    //Autenticación
    private FirebaseAuth fbAuth;
    private FirebaseUser fbUser;
    private GoogleApiClient mGoogleApiClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle(R.string.lbl_taskk_manager);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        plFragment = new ProjectListingFragment();
        settingsFragment = new SettingsFragment();

       new ConnectionChecker(this).start();

        adBuilder = new AlertDialog.Builder(this);
        ctx = this;

        fbAuth = FirebaseAuth.getInstance();
        fbUser = fbAuth.getCurrentUser();
        fbNotificationSettings = fbDatabaseRef.child(FirebasePath.MAIN_USERS).child(fbUser.getUid()).child(FirebasePath.USER_SETTINGS);

        loadFragment(plFragment);

        startServices();
    }

    /**
     * Se encarga de inicializar los servicios de notificaciones en función de los ajustes del
     * usuario
     */
    private void startServices() {
        fbNotificationSettings.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                //Si el usuario no tiene ajustes, establece los predeterminados
                if(dataSnapshot.getValue()==null){
                    fbNotificationSettings.setValue(new Settings());
                    return;
                }
                Settings notSettings = dataSnapshot.getValue(Settings.class);

                Intent iPeriodicNotification = new Intent(MainActivity.this, PeriodicNotificationService.class);
                Intent iTaskNotificacion = new Intent(MainActivity.this, TaskNotificationService.class);

                //Activa notificaciones periódicas
                if(notSettings.isEnablePeriodicNotification()){
                    startService(iPeriodicNotification);
                }else{
                    stopService(iPeriodicNotification);
                }

                //Activa notificaciones sobre tareas
                if (notSettings.isEnableTasksNotification()) {
                    startService(iTaskNotificacion);
                } else {
                    stopService(iTaskNotificacion);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        mGoogleApiClient.connect();

        setNavigationBarData();
    }


    @Override
    public void onBackPressed() {
        adBuilder
                .setMessage(R.string.lbl_are_sure_close)
                .setPositiveButton(R.string.yes, (dialogInterface, i) -> {
                    DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                    if (drawer.isDrawerOpen(GravityCompat.START)) {
                        drawer.closeDrawer(GravityCompat.START);
                    } else {
                        moveTaskToBack(true);
                        super.onBackPressed();
                    }
                })
                .setNegativeButton(R.string.no, (dialogInterface, i) -> {
                    //Nada
                })
                .show();



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        //Infla el menu lateral
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id){
            case R.id.action_tutorial:
                startActivity(new Intent(MainActivity.this, TutorialActivity.class));
                break;
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        FragmentManager fmanager = getFragmentManager();
        switch (id) {
            case R.id.nav_projects:
                loadFragment(plFragment);
                break;
            case R.id.nav_settings:
                loadFragment(settingsFragment);
                break;
            case R.id.nav_sign_out:
                askForLogout();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    /**
     * Pregunta al usuario si quiere cerrar la sesión
     */
    private void askForLogout() {
        adBuilder.setMessage(R.string.lbl_are_sure_logout)
                .setPositiveButton(R.string.yes, (dialogInterface, i) -> goToLogin())
                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //No hagas nada
                    }
                })
                .show();
    }

    /**
     * Cierra la actividad actual y va a la pantalla de login
     */
    private void goToLogin() {
        signOut();
        finish();
        startActivity(new Intent(ctx, LoginActivity.class));
    }

    /**
     * Se desconecta de los proveedores de Firebase y Google, respectivamente
     */
    private void signOut() {
        // Firebase sign out
        fbAuth.signOut();

        //Google sign out
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        // ...
                        Toast.makeText(getApplicationContext(), R.string.lbl_logged_out, Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(ctx, LoginActivity.class));
                    }
                });
    }

    /**
     * Espera a que se pinte la sidebar para poder establecer los valores en la UI
     */
    private void setNavigationBarData() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                boolean exitCondition = false;
                while (!exitCondition) {
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    //Establece nombre
                    tvUser = (TextView) findViewById(R.id.lbl_username);
                    tvMail = (TextView) findViewById(R.id.lbl_mail);
                    ImageView profPic = (ImageView) findViewById(R.id.iv_profile_picture);
                    if (tvUser != null && tvMail != null && profPic != null) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                tvUser.setText(fbUser.getDisplayName());

                                //Establece  correo de usuario
                                tvMail.setText(fbUser.getEmail());

                                Uri picture;
                                if ((picture = fbUser.getPhotoUrl()) != null) {
                                    Glide.with(MainActivity.this).load(picture).into(profPic);
                                }
                            }
                        });
                        exitCondition = true;
                    }

                }
            }
        }).start();
    }

    /**
     * Carga el fragment especificado en la ventana principal
     * @param frg Fragment a cargar
     */
    private void loadFragment(Fragment frg) {
        FragmentManager fmanager = getFragmentManager();
        fmanager.beginTransaction()
                .replace(R.id.fl_content, frg)
                .commit();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (Integer.parseInt(android.os.Build.VERSION.SDK) > 5
                && keyCode == KeyEvent.KEYCODE_BACK
                && event.getRepeatCount() == 0) {
            onBackPressed();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}
