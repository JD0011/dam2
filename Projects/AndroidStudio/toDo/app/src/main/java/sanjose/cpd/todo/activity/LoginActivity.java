package sanjose.cpd.todo.activity;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.ActionCodeSettings;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import sanjose.cpd.todo.R;
import sanjose.cpd.todo.util.FirebasePath;

/**
 * Se encarga de proporcionar los medios para iniciar sesión en la aplicación
 */
public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText etMail, etPassword;
    private FirebaseAuth fbAuth;
    private GoogleSignInClient gSigninClient;
    private int RC_SIGN_IN = 1000;

    private String TAG = "LOGIN";

    //Firebase BBDD
    private FirebaseDatabase fbDatabase = FirebaseDatabase.getInstance();
    private DatabaseReference fbDatabaseRef = fbDatabase.getReference(FirebasePath.DB_NAME);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme_NoActionBar);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        fbAuth = FirebaseAuth.getInstance();
        if (fbAuth.getCurrentUser() != null) {
            finish();
            startActivity(new Intent(getApplicationContext(), MainActivity.class));
        }

        //CONTROLES
        etMail = (EditText) findViewById(R.id.et_mail);
        etPassword = (EditText) findViewById(R.id.et_password);
        findViewById(R.id.sign_in_button).setOnClickListener(this);
        findViewById(R.id.btn_login).setOnClickListener(this);
        findViewById(R.id.btn_register).setOnClickListener(this);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.webclient_id))
                .requestEmail()
                .build();

        gSigninClient = GoogleSignIn.getClient(this, gso);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.sign_in_button:
                signIn();
                break;
        }
    }

    /**
     * Lanza el cuadro de selección de usuario mediante cuenta de Google
     */
    private void signIn() {
        Intent signInIntent = gSigninClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Resultado devuelto del panel de logueo con cuentas existentes de Google
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if (result.isSuccess()) {
                // Login con Google fue correcto
                GoogleSignInAccount account = result.getSignInAccount();
                Toast.makeText(getApplicationContext(), R.string.lbl_google_success, Toast.LENGTH_SHORT).show();
                firebaseAuthWithGoogle(account);
            } else {
                Toast.makeText(this, R.string.error_google_login_failed, Toast.LENGTH_LONG).show();
            }
        }
    }

    /**
     * Autentifica al usuario en firebase mediante la cuenta de Google seleccionada
     * @param acct
     */
    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        fbAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            //Inicio de sesión en Firebase correcto
                            FirebaseUser fbUser = fbAuth.getCurrentUser();

                            registerUser(fbUser);
                            startActivity(new Intent(getApplicationContext(), MainActivity.class));
                        } else {
                            //Inicio de sesión en Firebase fallido
                            Toast.makeText(LoginActivity.this, R.string.error_auth_fail,
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    /**
     * Registra el usuario en la base de datos de firebase, guardando los datos necesarios para
     * interactuar con la aplicación
     */
    private void registerUser(FirebaseUser fbUser) {
        DatabaseReference fbUserRef = fbDatabaseRef.child(FirebasePath.MAIN_USERS).child(fbUser.getUid());
        fbUserRef.child(FirebasePath.COMMON_MAIL).setValue(fbAuth.getCurrentUser().getEmail());
        fbUserRef.child(FirebasePath.COMMON_NAME).setValue(fbAuth.getCurrentUser().getDisplayName());
    }
}
