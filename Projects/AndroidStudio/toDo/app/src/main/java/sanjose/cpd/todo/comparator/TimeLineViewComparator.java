package sanjose.cpd.todo.comparator;

import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;

import java.util.Comparator;

import sanjose.cpd.todo.adapter.recyclerview.timeline.TimeLineType;
import sanjose.cpd.todo.adapter.recyclerview.timeline.type.DayView;
import sanjose.cpd.todo.pojo.Stage;
import sanjose.cpd.todo.pojo.Task;
import sanjose.cpd.todo.util.LocalDateTimeFormatter;

/**
 * Compara los distintos tipos de View's representables en el TimelineView en función del valor
 * de la propiedad de fecha u hora
 * Created by juand on 05/06/2018.
 */

public class TimeLineViewComparator implements Comparator<TimeLineType> {
    @Override
    public int compare(TimeLineType t, TimeLineType other) {
        LocalDate thisDate, otherDate;

        //Ordena los items en función de la fecha
        if (t instanceof Stage) {
            return 0; //En el sitio
        } else if (t instanceof Task) {
            thisDate = LocalDateTimeFormatter.fromFirebaseFormatToLocalDate(((Task) t).getDateTime()).toLocalDate();
        } else if (t instanceof DayView) {
            thisDate = ((DayView) t).getLocalDate();
        } else {
            thisDate = null;
        }

        if (other instanceof Stage) {
            return 1; //Delante
        } else if (other instanceof Task) {
            otherDate = LocalDateTimeFormatter.fromFirebaseFormatToLocalDate(((Task) other).getDateTime()).toLocalDate();
        } else if (other instanceof DayView) {
            otherDate = ((DayView) other).getLocalDate();
        } else {
            otherDate = null;
        }

        //Si los dos son tareas, ordenalos en función de la hora
        if (t instanceof Task && other instanceof Task) {
            LocalDateTime thisTime = LocalDateTimeFormatter.fromFirebaseFormatToLocalDate(((Task) t).getDateTime());
            LocalDateTime otherTime = LocalDateTimeFormatter.fromFirebaseFormatToLocalDate(((Task) other).getDateTime());
            if (thisTime.isAfter(otherTime)) {
                return 1;
            } else if (thisTime.isBefore(otherTime)) {
                return -1;
            } else {
                return 0;
            }
        } else {
            if (thisDate.isAfter(otherDate)) {
                return 1;
            } else if (thisDate.isBefore(otherDate)) {
                return -1;
            } else {
                return 0;
            }
        }

    }
}
