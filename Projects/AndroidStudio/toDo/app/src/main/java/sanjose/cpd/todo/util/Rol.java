package sanjose.cpd.todo.util;

/**
 * Define los roles existentes en la aplicación
 */

public abstract class Rol {

    public static final String ADMIN = "admin";
    public static final String WORKER = "worker";
}
