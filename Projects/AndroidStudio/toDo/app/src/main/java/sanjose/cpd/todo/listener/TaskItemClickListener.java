package sanjose.cpd.todo.listener;

import android.view.View;
import android.widget.Button;
import android.widget.ToggleButton;

import sanjose.cpd.todo.pojo.Task;

/**
 * Interfaz que ofrece la implementación del evento onItemClick y el click al botón de finalizado
 * de tareas en la representación en formato TimelineView de las tareas.
 */

public interface TaskItemClickListener {
    void onLongItemClick(View v, int position);
    void onFinishedButtonClick(Task t, ToggleButton btn);
}