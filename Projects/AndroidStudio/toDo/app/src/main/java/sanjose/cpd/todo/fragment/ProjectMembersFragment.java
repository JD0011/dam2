package sanjose.cpd.todo.fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import sanjose.cpd.todo.R;
import sanjose.cpd.todo.adapter.listview.UserRolAdapter;
import sanjose.cpd.todo.dialog.RolsDialog;
import sanjose.cpd.todo.pojo.UserRol;
import sanjose.cpd.todo.util.FirebasePath;
import sanjose.cpd.todo.util.Rol;

/**
 * Muestra los miembros de un proyecto determinado
 */

public class ProjectMembersFragment extends Fragment implements View.OnClickListener {

    //Pantalla
    private EditText etUserMail;
    private TextView tvUsername, tvMail;
    private CardView cvUserInfo;
    private ProgressBar pbUserSearch;
    private ListView lvMembers;
    private ImageButton btnAdd;

    private boolean userMailExists, userIsAlreadyMember;
    private UserRolAdapter urAdapter;
    private UserRol searchedUser;
    private String projectKey, userName, userMail, userUID, choosenRol;
    private ArrayList<UserRol> projectMembers;
    private AlertDialog.Builder adBuilder;

    //Autenticacion
    private FirebaseAuth fbAuth = FirebaseAuth.getInstance();
    private final String currentUserMail = fbAuth.getCurrentUser().getEmail();

    //Firebase BBDD
    private FirebaseDatabase fbDatabase = FirebaseDatabase.getInstance();
    private DatabaseReference fbDatabaseRef = fbDatabase.getReference(FirebasePath.DB_NAME);
    private DatabaseReference fbProjectMembersRef, fbUsersRef;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_project_members, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //Pantalla
        etUserMail = (EditText) view.findViewById(R.id.et_email);
        lvMembers = (ListView) view.findViewById(R.id.lv_members);
        cvUserInfo = (CardView) view.findViewById(R.id.cv_user_info);
        pbUserSearch = (ProgressBar) view.findViewById(R.id.pb_user_search);
        tvUsername = (TextView) view.findViewById(R.id.tv_username);
        tvMail = (TextView) view.findViewById(R.id.tv_usermail);
        btnAdd = view.findViewById(R.id.btn_add_user);

        btnAdd.setOnClickListener(this);
        ((ImageButton) view.findViewById(R.id.btn_search_user)).setOnClickListener(this);

        Bundle extras = getArguments();
        projectKey = extras.getString("project_key", "");

        fbProjectMembersRef = fbDatabaseRef.child(FirebasePath.MAIN_MEMBERS).child(projectKey);
        fbUsersRef = fbDatabaseRef.child(FirebasePath.MAIN_USERS);

        pbUserSearch.setIndeterminate(true);
        projectMembers = new ArrayList<>();
        urAdapter = new UserRolAdapter(getActivity(), projectMembers);
        lvMembers.setAdapter(urAdapter);
        adBuilder = new AlertDialog.Builder(getActivity());

        setProjectMembersListener();
        updateUI();

        lvMembers.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                UserRol userToDelete = projectMembers.get(position);

                AlertDialog adDeleteUser = adBuilder.create();

                adDeleteUser.setTitle(R.string.lbl_delete_user);
                adDeleteUser.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.yes), (dialogInterface, i1) -> {
                    deleteMember(userToDelete);
                });
                adDeleteUser.setButton(AlertDialog.BUTTON_NEGATIVE, getString(R.string.no), (dialogInterface, i1) -> {
                    //Nada
                });

                adDeleteUser.show();

            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_add_user:
                checkIfUserIsMember();
                if (userMailExists && !userIsAlreadyMember) {
                    askForRol();
                }

                updateUI();

            case R.id.btn_search_user:
                if (!etUserMail.getText().toString().trim().equals("")) {
                    pbUserSearch.setVisibility(View.VISIBLE);
                    cvUserInfo.setVisibility(View.VISIBLE);
                    userMail = etUserMail.getText().toString().toLowerCase();
                    if (!userMail.equals(currentUserMail)) {
                        //Busca el mail especificado
                        checkIfUserExists();
                    }

                } else {
                    cvUserInfo.setVisibility(View.GONE);
                }

                break;
        }
    }

    /**
     * Comprueba si el mail introducido existe en la base de datos para poder ser agregado como miembro de proyecto
     */
    private void checkIfUserExists() {
        DatabaseReference fbUsersRef = fbDatabaseRef.child(FirebasePath.MAIN_USERS);
        fbUsersRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                pbUserSearch.setVisibility(View.VISIBLE);
                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    String mail = ds.child(FirebasePath.COMMON_MAIL).getValue().toString();
                    if (!mail.equals(currentUserMail) && mail.equals(userMail)) {
                        userMailExists = true;
                        userUID = ds.getKey().toString();
                        userName = ds.child(FirebasePath.COMMON_NAME).getValue().toString();
                        searchedUser = new UserRol(userUID, ds.child(FirebasePath.COMMON_MAIL).getValue(String.class), "");
                        break;
                    }
                }
                updateUI();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    /**
     * Comprueba si un usuario ya pertenece al proyecto especificado
     */
    private void checkIfUserIsMember() {
        userIsAlreadyMember = false;
        for (UserRol uRol : projectMembers) {
            if (userMail.equalsIgnoreCase(uRol.getMail())) {
                userIsAlreadyMember = true;
                break;
            }
        }
    }

    /**
     * Establece el listener que carga a los miembros del proyecto que se visualiza
     */
    private void setProjectMembersListener() {
        fbProjectMembersRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                boolean alreadyShown = false;
                UserRol auxUserRol = new UserRol(
                        dataSnapshot.getKey().toString(),
                        dataSnapshot.child(FirebasePath.COMMON_MAIL).getValue().toString(),
                        dataSnapshot.child(FirebasePath.COMMON_ROL).getValue().toString());

                for (UserRol urol : projectMembers) {
                    if (urol.getId().equalsIgnoreCase(auxUserRol.getId())) {
                        alreadyShown = true;
                    }
                }

                if (!alreadyShown) {
                    urAdapter.add(auxUserRol);
                }

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                UserRol auxUserRol = new UserRol(
                        dataSnapshot.getKey().toString(),
                        dataSnapshot.child(FirebasePath.COMMON_MAIL).getValue().toString(),
                        dataSnapshot.child(FirebasePath.COMMON_ROL).getValue().toString());

                urAdapter.remove(auxUserRol);
                urAdapter.add(auxUserRol);
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                UserRol auxUserRol = new UserRol(
                        dataSnapshot.getKey().toString(),
                        dataSnapshot.child(FirebasePath.COMMON_MAIL).getValue().toString(),
                        dataSnapshot.child(FirebasePath.COMMON_ROL).getValue().toString());
                urAdapter.remove(auxUserRol);
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    /**
     * Añade un usuario como miembro del proyecto
     */
    private void addUserAsMember(UserRol userRol) {
        fbUsersRef.child(userRol.getId()).child(FirebasePath.USER_SETTINGS).child(FirebasePath.USER_ALLOW_INVITE).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() == null) {
                    return;
                }

                boolean allowInvite = dataSnapshot.getValue(Boolean.class);
                if (!allowInvite) {
                    adBuilder
                            .setMessage(R.string.lbl_cant_invite_user)
                            .setPositiveButton(R.string.ok, (dialogInterface, i) -> {
                                //nada
                            })
                            .show();
                } else {
                    //Añade el usuario al proyecto
                    fbProjectMembersRef.child(userUID).setValue(userRol, (databaseError, databaseReference) -> {
                        if (databaseError != null) {
                            Toast.makeText(getActivity().getApplicationContext(), R.string.error_something_wrong, Toast.LENGTH_SHORT).show();
                        } else {
                            fbProjectMembersRef.child(userUID).child(FirebasePath.COMMON_ROL).setValue(choosenRol, (databaseError1, databaseReference1) -> {
                                if (databaseError != null) {
                                    Toast.makeText(getActivity().getApplicationContext(), R.string.error_something_wrong, Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(getActivity().getApplicationContext(), R.string.lbl_member_registered, Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                    });
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    /**
     * Elimina el miembros especificado del proyecto
     *
     * @param user Usuario a eliminar
     */
    private void deleteMember(UserRol user) {
        if (user.getMail().equalsIgnoreCase(fbAuth.getCurrentUser().getEmail())) {
            Toast.makeText(getActivity().getApplicationContext(), R.string.error_you_owner, Toast.LENGTH_LONG).show();
        } else {

            //Comprueba que el usuario a eliminar no sea el dueño del proyecto
            fbDatabaseRef.child(FirebasePath.MAIN_PROJECTS).child(projectKey).child(FirebasePath.PROJECT_OWNER_ID).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    String projectOwner = dataSnapshot.getValue(String.class);

                    if (user.getId().equalsIgnoreCase(projectOwner)) {
                        Toast.makeText(getActivity(), R.string.error_cant_delete_admin, Toast.LENGTH_SHORT).show();
                    } else {

                        //Comprueba que no elimine a un admin sin serlo
                        fbProjectMembersRef.child(fbAuth.getCurrentUser().getUid()).child(FirebasePath.COMMON_ROL).addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                String rol = dataSnapshot.getValue(String.class);

                                if (!rol.equalsIgnoreCase(Rol.ADMIN)) {
                                    Toast.makeText(getActivity(), R.string.error_no_permissions, Toast.LENGTH_SHORT).show();
                                } else {
                                    //Ordena por correo y busca el especificado
                                    fbProjectMembersRef.orderByChild(FirebasePath.COMMON_MAIL).addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(DataSnapshot dataSnapshot) {
                                            String tempMail;
                                            for (DataSnapshot ds : dataSnapshot.getChildren()) {
                                                tempMail = ds.child(FirebasePath.COMMON_MAIL).getValue().toString();
                                                if (tempMail.equalsIgnoreCase(user.getMail())) {
                                                    ds.getRef().removeValue(new DatabaseReference.CompletionListener() {
                                                        @Override
                                                        public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                                                            if (databaseError == null) {
                                                                //Sin errores
                                                                Toast.makeText(getActivity().getApplicationContext(), R.string.lbl_member_deleted, Toast.LENGTH_SHORT).show();
                                                                urAdapter.remove(user);
                                                            } else {
                                                                //Posible error de conexión
                                                                Toast.makeText(getActivity().getApplicationContext(), R.string.error_something_wrong, Toast.LENGTH_SHORT).show();
                                                            }
                                                        }
                                                    });

                                                    break;
                                                }
                                            }
                                        }

                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {

                                        }
                                    });
                                }
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
    }

    /**
     * Pregunta al creador del proyecto que ROL desea asignar al miembro que está agregando
     * y lo agrega a la base de datos
     *
     * @return True si elige un rol, False si cancela la operación
     */
    private void askForRol() {
        //Pregunta el rol a establecer al usuario a partir de un AlertDialog predefinido
        RolsDialog rDialog = new RolsDialog(getActivity());

        rDialog.setPositiveButton(R.string.ok, (dialogInterface, i) -> {
            if (rDialog.someIsChecked()) {
                choosenRol = rDialog.choosenRol();
                searchedUser.setRol(choosenRol);
                addUserAsMember(searchedUser);
            } else {
                choosenRol = null;

                Toast.makeText(getActivity().getApplicationContext(), R.string.error_must_choose_rol, Toast.LENGTH_SHORT).show();
            }
        });

        rDialog.show();
    }

    /**
     * Actualiza la interfaz en función de las posibles casuísticas que surgen al buscar un usuario
     */
    private void updateUI() {
        pbUserSearch.setVisibility(View.GONE);
        if (userMail != null && userMail.equals(currentUserMail)) {
            btnAdd.setVisibility(View.INVISIBLE);
            tvMail.setText(R.string.error_same_mail);
            tvUsername.setText("");
        } else if (userIsAlreadyMember) {
            btnAdd.setVisibility(View.INVISIBLE);
            tvMail.setText(R.string.error_already_member);
            tvUsername.setText("");
        } else if (userMailExists && !userIsAlreadyMember) {
            btnAdd.setVisibility(View.VISIBLE);
            tvMail.setText(userMail);
            tvUsername.setText(userName);
        } else {
            btnAdd.setVisibility(View.INVISIBLE);
            tvMail.setText(R.string.lbl_user_not_found);
            tvUsername.setText("");
        }

        // userMailExists = false;
    }
}
