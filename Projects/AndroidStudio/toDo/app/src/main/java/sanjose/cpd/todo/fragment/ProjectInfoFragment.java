package sanjose.cpd.todo.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.joda.time.LocalDate;

import java.lang.reflect.Array;

import sanjose.cpd.todo.R;
import sanjose.cpd.todo.pojo.Project;
import sanjose.cpd.todo.util.DateFormatter;
import sanjose.cpd.todo.util.FirebasePath;
import sanjose.cpd.todo.util.Rol;

/**
 * Created by user on 05/05/2018.
 */

public class ProjectInfoFragment extends Fragment implements View.OnClickListener {

    private TextView tvProjectCreationDate, tvProjectEndDate, tvDescription;
    private Button btnDeleteProject;
    private ProgressDialog pdDeleteProject;
    private boolean isBeingDeleted;

    //Firebase BBDD
    private FirebaseDatabase fbDatabase = FirebaseDatabase.getInstance();
    private DatabaseReference fbDatabaseRef = fbDatabase.getReference(FirebasePath.DB_NAME);
    private DatabaseReference fbProjectRef;
    private DatabaseReference fbMembersRef;
    private DatabaseReference fbTasksRef;

    //User
    private String userIdentifier = FirebaseAuth.getInstance().getCurrentUser().getUid();

    private Project p;
    private String projectKey;
    private String userRol;

    public ProjectInfoFragment() {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_project_info, container, false);
        setHasOptionsMenu(true);
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        for (int i = 0; i < menu.size(); i++) {
            if (menu.getItem(i).getItemId() != R.id.it_goback) {
                menu.getItem(i).setVisible(false);
            }
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //Pantalla
        tvProjectCreationDate = (TextView) view.findViewById(R.id.tv_project_creation_value);
        tvProjectEndDate = (TextView) view.findViewById(R.id.tv_project_end_value);
        tvDescription = (TextView) view.findViewById(R.id.tv_description_value);
        btnDeleteProject = (Button) view.findViewById(R.id.btn_delete_project);
        ((Button) view.findViewById(R.id.btn_leave_project)).setOnClickListener(this);

        pdDeleteProject = new ProgressDialog(getActivity());
        pdDeleteProject.setCancelable(false);

        projectKey = getArguments().getString("project_key", "");

        fbProjectRef = fbDatabaseRef.child(FirebasePath.MAIN_PROJECTS).child(projectKey);
        fbMembersRef = fbDatabaseRef.child(FirebasePath.MAIN_MEMBERS);
        fbTasksRef = fbDatabaseRef.child(FirebasePath.MAIN_TASKS);

        setListenerToProject();

    }

    @Override
    public void onClick(View view) {
        int id = view.getId();

        switch (id) {
            case R.id.btn_delete_project:

                if (!userRol.equalsIgnoreCase(Rol.ADMIN)) {
                    Toast.makeText(getActivity(), R.string.error_no_permissions, Toast.LENGTH_SHORT).show();
                } else {
                    AlertDialog.Builder adBuilder = new AlertDialog.Builder(getActivity());
                    adBuilder
                            .setTitle(R.string.lbl_delete_project)
                            .setMessage(R.string.lbl_are_sure_delete_project)
                            .setNegativeButton(R.string.no, (dialogInterface, i) -> {
                                //Nada
                            })
                            .setPositiveButton(R.string.yes, (dialogInterface, i) -> {
                                pdDeleteProject.setTitle(R.string.lbl_wait_a_moment);
                                pdDeleteProject.setMessage(getString(R.string.lbl_deleting_project));
                                pdDeleteProject.show();
                                deleteProject();
                            })
                            .show();
                }
                break;
            case R.id.btn_leave_project:
                pdDeleteProject.setTitle(R.string.lbl_wait_a_moment);
                pdDeleteProject.setMessage(getString(R.string.lbl_leaving_project));
                pdDeleteProject.show();
                leaveProject();
                break;
        }
    }

    /**
     * Elimina el proyecto mostrado
     */
    private void deleteProject() {
        fbTasksRef.child(projectKey).removeValue((databaseError, databaseReference) -> {
            if (databaseError != null) {
                pdDeleteProject.hide();
                Toast.makeText(getActivity(), R.string.error_something_wrong, Toast.LENGTH_SHORT).show();
            } else {
                isBeingDeleted = true;
                fbMembersRef.child(projectKey).removeValue((databaseError1, databaseReference1) -> {
                    if (databaseError1 != null) {
                        pdDeleteProject.hide();
                        Toast.makeText(getActivity(), R.string.error_something_wrong, Toast.LENGTH_SHORT).show();
                    } else {
                        fbProjectRef.removeValue((databaseError2, databaseReference2) -> {
                            pdDeleteProject.hide();
                            if (databaseError2 != null) {
                                Toast.makeText(getActivity(), R.string.error_something_wrong, Toast.LENGTH_SHORT).show();
                            }

                        });
                    }

                });
            }
        });
    }

    /**
     * Permite al usuario actual dejar el proyecto que está visualizando en ese momento
     */
    private void leaveProject() {
        //Comprueba que el usuario que quiere dejar el proyecto no sea el dueño
        fbProjectRef.child(FirebasePath.PROJECT_OWNER_ID).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue().toString().equalsIgnoreCase(userIdentifier)) {
                    pdDeleteProject.hide();
                    Toast.makeText(getActivity(), R.string.error_you_owner, Toast.LENGTH_LONG).show();
                } else {
                    fbTasksRef.child(projectKey).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            boolean userHasTasks = false;
                            for (DataSnapshot dsStage : dataSnapshot.getChildren()) {
                                for (DataSnapshot dsTask : dsStage.getChildren()) {
                                    if (dsTask.child(FirebasePath.TASK_OWNER).toString().equalsIgnoreCase(userIdentifier)) {
                                        if (!dsTask.child(FirebasePath.TASK_FINISHED).getValue(Boolean.class)) {
                                            userHasTasks = true;
                                            break;
                                        }
                                    }
                                }
                            }

                            if (userHasTasks) {
                                Toast.makeText(getActivity(), R.string.error_have_tasks, Toast.LENGTH_LONG).show();
                            } else {
                                fbMembersRef.child(projectKey).child(userIdentifier).removeValue(new DatabaseReference.CompletionListener() {
                                    @Override
                                    public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                                        pdDeleteProject.hide();
                                        if (databaseError != null) {
                                            Toast.makeText(getActivity(), R.string.error_something_wrong, Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                });
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    /**
     * Agrega el listener para obtener el proyecto cuya ID corresponde a la obtenida
     */
    private void setListenerToProject() {
        fbProjectRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (!isBeingDeleted) {
                    p = dataSnapshot.getValue(Project.class);
                    updateUI();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        fbMembersRef.child(projectKey).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot dsUser : dataSnapshot.getChildren()) {
                    if (dsUser.getKey().equalsIgnoreCase(FirebaseAuth.getInstance().getCurrentUser().getUid())) {
                        //Cuando tengas el rol, añade el listener de eliminar
                        userRol = dsUser.child(FirebasePath.COMMON_ROL).getValue().toString();
                        btnDeleteProject.setOnClickListener(ProjectInfoFragment.this);
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void updateUI() {
        if (p != null) {
            LocalDate creationDate = new LocalDate(
                    p.getCreationDate().get(0),
                    p.getCreationDate().get(1),
                    p.getCreationDate().get(2));
            String dateFormat = DateFormatter.DATE_FORMAT;
            tvProjectCreationDate.setText(creationDate.toString(dateFormat));
            tvDescription.setText(p.getDescription());
        }
    }
}
