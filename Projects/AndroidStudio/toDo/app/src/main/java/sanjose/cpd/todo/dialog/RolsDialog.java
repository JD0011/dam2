package sanjose.cpd.todo.dialog;

import android.app.AlertDialog;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.Toast;

import sanjose.cpd.todo.R;

/**
 * Este AlertDialog permite establecer un rol durante la adición de miembros a un proyecto
 * Created by juand on 22/05/2018.
 */

public class RolsDialog extends AlertDialog.Builder {

    //ID'S de RadioButtons
    private final int RB_ADMIN_ID = 1;
    private final int RB_WORKER_ID = 3;

    private RadioGroup rGroupRols;
    private RadioButton rbAdmin;
    private RadioButton rbWorker;

    public RolsDialog(Context context) {
        super(context);
    }

    @Override
    public AlertDialog show() {

        rGroupRols = new RadioGroup(getContext());

        //Rol Admin
        rbAdmin = new RadioButton(getContext());
        rbAdmin.setId(RB_ADMIN_ID);
        rbAdmin.setText(getContext().getString(R.string.lbl_rol_admin));
        rGroupRols.addView(rbAdmin);

        //Rol Trabajador
        rbWorker = new RadioButton(getContext());
        rbWorker.setId(RB_WORKER_ID);
        rbWorker.setText(getContext().getString(R.string.lbl_rol_worker));
        rGroupRols.addView(rbWorker);

        setTitle(R.string.lbl_choose_rol_title);
        setMessage(getContext().getString(R.string.lbl_set_user_rol));
        setView(rGroupRols);

        return super.show();
    }

    public boolean someIsChecked(){
        boolean someChecked = false;

        for(int x = 0; x< rGroupRols.getChildCount(); x++){
            if(rGroupRols.getChildAt(x) instanceof RadioButton){
                if(((RadioButton) rGroupRols.getChildAt(x)).isChecked()){
                    someChecked = true;
                    break;
                }
            }
        }

        return someChecked;
    }

    /**
     * Comprueba el radioButton elegido en el RadioGroup
     * @return Rol elegido
     */
    public String choosenRol() {
        int id = rGroupRols.getCheckedRadioButtonId();

        String value;
        switch (id) {
            case RB_ADMIN_ID:
                value = "admin";
                break;
            case RB_WORKER_ID:
                value = "worker";
                break;
            default:
                value = null;
        }

        return value;
    }
}
