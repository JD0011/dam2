package sanjose.cpd.todo.fragment;

/**
 * Created by juand on 31/05/2018.
 */

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.joda.time.LocalDate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import pl.hypeapp.materialtimelineview.MaterialTimelineView;
import sanjose.cpd.todo.R;
import sanjose.cpd.todo.adapter.recyclerview.timeline.TimeLineAdapter;
import sanjose.cpd.todo.adapter.recyclerview.timeline.TimeLineType;
import sanjose.cpd.todo.adapter.recyclerview.timeline.type.DayView;
import sanjose.cpd.todo.dialog.TaskCreationDialog;
import sanjose.cpd.todo.listener.RecyclerViewItemClickListener;
import sanjose.cpd.todo.listener.TaskItemClickListener;
import sanjose.cpd.todo.pojo.Stage;
import sanjose.cpd.todo.pojo.Task;
import sanjose.cpd.todo.util.FirebasePath;
import sanjose.cpd.todo.util.Rol;

/**
 * Muestra en formato de línea temporal las tareas asociadas con el usuario indicado, de forma que
 * las pueda visualizar y saber a donde pertenecen
 */
public class TaskVisualizationFragment extends Fragment {

    private ArrayList<Task> tasksList;
    private ArrayList<LocalDate> daysList;
    private int daysCounter;

    private ProgressBar pbLoading;

    private HashMap<String, String> proyectMembers;
    private AdapterView.OnItemClickListener stageSelectionListener, deleteListener;

    private String projectKey;
    private String currentUserID;
    private String currentUserRol;

    private AlertDialog.Builder adBuilder;

    private Task generatedTask;
    private TimeLineAdapter tmAdapter;
    private RecyclerView rvTasks;

    //Firebase BBDD
    private FirebaseDatabase fbDatabase = FirebaseDatabase.getInstance();
    private DatabaseReference fbDatabaseRef = fbDatabase.getReference(FirebasePath.DB_NAME);
    private DatabaseReference fbProjectRef;
    private DatabaseReference fbTasksRefs;

    //Firebase Autentication
    private FirebaseAuth fbAuth = FirebaseAuth.getInstance();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_tasks_visualization, container, false);
        setHasOptionsMenu(true);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        pbLoading = (ProgressBar) view.findViewById(R.id.pb_loading);
        pbLoading.setIndeterminate(true);

        projectKey = getArguments().getString("project_key");

        //Referencia a la stage de la base de datos
        fbTasksRefs = fbDatabaseRef.child(FirebasePath.MAIN_TASKS);
        fbProjectRef = fbDatabaseRef.child(FirebasePath.MAIN_PROJECTS);

        //Pantalla
        rvTasks = (RecyclerView) view.findViewById(R.id.rv_tasks);

        currentUserID = fbAuth.getCurrentUser().getUid();

        adBuilder = new AlertDialog.Builder(getActivity());

        LinearLayoutManager llManager = new LinearLayoutManager(getActivity().getApplicationContext());

        proyectMembers = new HashMap<>();
        loadProyectMembers();

        tmAdapter = new TimeLineAdapter(getActivity(), proyectMembers);
        tasksList = new ArrayList<>();
        daysList = new ArrayList<>();

        rvTasks.setAdapter(tmAdapter);
        rvTasks.setLayoutManager(llManager);
    }

    /**
     * Elimina la tarea especificada
     *
     * @param task Tarea a eliminar
     */
    private void deleteTask(Task task) {
        if (task.getOwner() != fbAuth.getCurrentUser().getUid()) {
            if (!currentUserRol.equals(Rol.ADMIN)) {
                Toast.makeText(getActivity().getApplicationContext(), R.string.error_no_permissions, Toast.LENGTH_SHORT).show();
            } else {
                fbTasksRefs.child(task.getProjectID()).child(task.getStageID()).child(task.getName()).removeValue(new DatabaseReference.CompletionListener() {
                    @Override
                    public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                        pbLoading.setVisibility(View.GONE);
                        if (databaseError == null) {
                            Toast.makeText(getActivity().getApplicationContext(), R.string.lbl_success_task_delete, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getActivity().getApplicationContext(), R.string.error_something_wrong, Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        } else {
            fbTasksRefs.child(task.getProjectID()).child(task.getStageID()).child(task.getName()).removeValue(new DatabaseReference.CompletionListener() {
                @Override
                public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                    pbLoading.setVisibility(View.GONE);
                    if (databaseError == null) {
                        Toast.makeText(getActivity().getApplicationContext(), R.string.lbl_success_task_delete, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getActivity().getApplicationContext(), R.string.error_something_wrong, Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }

    /**
     * Carga los miembros del proyecto, necesarios para representar el TimelineView
     */
    private void loadProyectMembers() {
        DatabaseReference fbProyectMembers = fbDatabaseRef.child(FirebasePath.MAIN_MEMBERS).child(projectKey);
        fbProyectMembers.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    proyectMembers.put(ds.getKey(), ds.child(FirebasePath.COMMON_MAIL).getValue().toString());

                    if (ds.getKey().equalsIgnoreCase(currentUserID)) {
                        currentUserRol = ds.child(FirebasePath.COMMON_ROL).getValue().toString();

                        setStageListeners();
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


    /**
     * Se encarga de leer las tareas que pertenecen a la stage que se está visualizando
     */
    private void setStageListeners() {
        fbProjectRef.child(projectKey).child(FirebasePath.MAIN_STAGES).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot dsStage : dataSnapshot.getChildren()) {
                    fbTasksRefs.child(projectKey).child(dsStage.getKey()).addChildEventListener(new ChildEventListener() {
                        @Override
                        public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                            boolean alreadyShown = false;
                            Task auxTask = dataSnapshot.getValue(Task.class);

                            if (currentUserRol.equalsIgnoreCase(Rol.ADMIN)) {
                                for(Task t: tasksList){
                                    if(t.getName().equalsIgnoreCase(auxTask.getName())){
                                        alreadyShown= true;
                                        break;
                                    }
                                }
                                if (!alreadyShown) {
                                    tasksList.add(auxTask);
                                    tmAdapter.addTaskItem(auxTask);
                                }
                            } else if (auxTask.getForAllUsers() == true || auxTask.getOwner().equalsIgnoreCase(fbAuth.getCurrentUser().getEmail())) {
                                for(Task t: tasksList){
                                    if(t.getName().equalsIgnoreCase(auxTask.getName())){
                                        alreadyShown= true;
                                        break;
                                    }
                                }
                                if (!alreadyShown) {
                                    tasksList.add(auxTask);
                                    tmAdapter.addTaskItem(auxTask);
                                }
                            }
                        }

                        @Override
                        public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                            Task auxTask = dataSnapshot.getValue(Task.class);
                                    tmAdapter.removeTaskItem(auxTask.getName());
                                    tmAdapter.addTaskItem(auxTask);
                        }

                        @Override
                        public void onChildRemoved(DataSnapshot dataSnapshot) {
                            Task auxTask = dataSnapshot.getValue(Task.class);
                            //Nota: remove normal no funciona
                            Iterator<Task> taskIterator = tasksList.iterator();
                            while(taskIterator.hasNext()){
                                Task currTask = taskIterator.next();
                                if(currTask.getName().equalsIgnoreCase(auxTask.getName())){
                                    taskIterator.remove();
                                    tmAdapter.removeTaskItem(currTask.getName());
                                }
                            }
                        }

                        @Override
                        public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }
}
