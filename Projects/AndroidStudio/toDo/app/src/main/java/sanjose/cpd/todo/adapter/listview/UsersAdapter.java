package sanjose.cpd.todo.adapter.listview;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import sanjose.cpd.todo.R;
import sanjose.cpd.todo.pojo.Task;
import sanjose.cpd.todo.pojo.User;

/**
 * Muestra a los usuarios en un listview. Se le pasa objetos de tipo User
 */

public class UsersAdapter extends ArrayAdapter{

    private Activity context;

    public UsersAdapter(Activity context, ArrayList<User> users) {
        super(context, R.layout.spinner_item_username, users);
        this.context = context;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        TextView tvUsername = (TextView) inflater.inflate(R.layout.spinner_item_username, parent, false);

        tvUsername.setText(
                ((User)getItem(position)).getName()
                +"("+
                ((User)getItem(position)).getMail()
                +")"
        );

        return tvUsername;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        if(convertView == null){
            convertView = inflater.inflate(R.layout.spinner_item_username,parent, false);
        }
        TextView tvUsername = (TextView) convertView;
        tvUsername.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        tvUsername.setText(
                ((User)getItem(position)).getName()
                        +"("+
                        ((User)getItem(position)).getMail()
                        +")"
        );

        return convertView;
    }
}
