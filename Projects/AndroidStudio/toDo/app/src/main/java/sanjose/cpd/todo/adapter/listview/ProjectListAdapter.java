package sanjose.cpd.todo.adapter.listview;

import android.app.Activity;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import sanjose.cpd.todo.R;

/**
 * Permite visualizar los proyectos en un ListView (en desuso)
 */

public class ProjectListAdapter extends ArrayAdapter {

    private Activity context;
    private ArrayList<String> projects;

    public ProjectListAdapter(Activity context, ArrayList<String> projects){
        super(context, R.layout.listview_projects_template,projects);
        this.context = context;
        this.projects = projects;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View item = inflater.inflate(R.layout.listview_projects_template, null);

        TextView tvProjectName = (TextView)item.findViewById(R.id.tv_project_name);
        tvProjectName.setText(projects.get(position));

        return item;
    }
}

