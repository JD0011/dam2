package sanjose.cpd.todo.util;

/**
 * Esta clase permite localizar las distintas ramas de las que se compone la base de datos Firebase.
 * De esta manera, no hay cabida a error, siempre que se utilicen las constantes aquí
 * especificadas
 */

public interface FirebasePath {

    //Nombre de la base de datos
    String DB_NAME = "taskmanager";

    //Ramas principales
    String MAIN_PROJECTS = "projects";
    String MAIN_STAGES = "stages";
    String MAIN_MEMBERS = "members";
    String MAIN_TASKS = "tasks";
    String MAIN_USERS = "users";

    //Atributos comunes
    String COMMON_ID = "id";
    String COMMON_NAME = "name";
    String COMMON_DESCRIPTION = "description";
    String COMMON_START_DATE = "startDate";
    String COMMON_END_DATE = "endDate";
    String COMMON_MAIL = "mail";
    String COMMON_VIEWTYPE = "name";

    //Instancia de miembros
    String COMMON_ROL = "rol";

    //Instancia de proyecto
    String PROJECT_CREATION_DATE = "creationDate";
    String PROJECT_OWNER_ID = "ownerID";

    //Instancia de etapa
    String STAGE_PROJECT_ID = "projectID";
    String STAGE_ID = "id";
    String STAGE_NAME = "name";

    //Instancia de tarea
    String TASK_DATE_TIME = "dateTime";
    String TASK_FINISHED = "finished";
    String TASK_FOR_ALL_USERS = "forAllUsers";
    String TASK_OWNER = "owner";
    String TASK_STAGE_ID = "stageID";

    //Instancia de User
    String USER_SETTINGS = "settings";
    String USER_ALLOW_INVITE= "allowInvite";
    String USER_NOTIFICATION_DATETIME = "datetimeNotification";
    String USER_NOTIFICATION_PERIODIC = "enablePeriodicNotification";
    String USER_NOTIFICATION_TASK= "enableTasksNotification";
    String USER_NOTIFICATION_NOTICE= "minutsBeforeTaskTimeNotification";

}
