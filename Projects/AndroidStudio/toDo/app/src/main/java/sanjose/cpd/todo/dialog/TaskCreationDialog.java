package sanjose.cpd.todo.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.tsongkha.spinnerdatepicker.SpinnerDatePickerDialogBuilder;

import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.joda.time.LocalTime;

import java.util.ArrayList;

import sanjose.cpd.todo.R;
import sanjose.cpd.todo.adapter.listview.UsersAdapter;
import sanjose.cpd.todo.pojo.Stage;
import sanjose.cpd.todo.pojo.Task;
import sanjose.cpd.todo.pojo.User;
import sanjose.cpd.todo.util.DateFormatter;
import sanjose.cpd.todo.util.FirebasePath;
import sanjose.cpd.todo.util.LocalDateTimeFormatter;
import sanjose.cpd.todo.util.Randomizer;
import sanjose.cpd.todo.util.Rol;
import sanjose.cpd.todo.util.StringFormatter;

/**
 * Dialogo que permite crear una tarea nueva de forma sencilla y allá donde se llame. Devuelve un
 * objeto de tarea nuevo
 * Created by juand on 01/06/2018.
 */

public class TaskCreationDialog extends Dialog implements View.OnClickListener {

    private Activity ownerActivity;

    private CardView cvOwner;
    private EditText etDescription;
    private TextView tvSelectedDate;
    private Switch swForAllUsers;
    private Spinner spMembersList;
    private ArrayList<User> membersList;
    private UsersAdapter uAdapter;
    private Button btnSetDate;

    private String userRol, owner;

    private ProgressBar pbRecording;

    private CalendarView cpvTaskDate;
    private TimePicker tpTaskTime;
    private LocalDate taskDateSelected;
    private LocalTime taskDateTimeSelected;

    private boolean descriptionOK;
    private String descriptionError;

    private Task generatedTask;
    private Stage stageRoot;

    //Firebase BBDD
    private FirebaseDatabase fbDatabase = FirebaseDatabase.getInstance();
    private DatabaseReference fbDatabaseRef = fbDatabase.getReference(FirebasePath.DB_NAME);
    private DatabaseReference fbStageTasksRef, fbMembersRef, fbUsersRef;

    //Firebase Authentication
    private FirebaseUser fbCurrentUser = FirebaseAuth.getInstance().getCurrentUser();

    public TaskCreationDialog(@NonNull Activity context, Stage stageRoot) {
        super(context);
        this.ownerActivity = context;
        this.stageRoot = stageRoot;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_new_task);

        //Pantalla
        cvOwner = (CardView) findViewById(R.id.cv_owner);
        etDescription = (EditText) findViewById(R.id.et_description);
        swForAllUsers = (Switch) findViewById(R.id.sw_forallusers);
        spMembersList = (Spinner) findViewById(R.id.sp_users);
        btnSetDate = (Button) findViewById(R.id.btn_set_date);
        cpvTaskDate = (CalendarView) findViewById(R.id.cpv_date);
        tpTaskTime = (TimePicker) findViewById(R.id.tp_time);
        pbRecording = (ProgressBar) findViewById(R.id.pb_recording);
        tvSelectedDate = (TextView) findViewById(R.id.tv_selected_date);

        pbRecording.setIndeterminate(true);
        btnSetDate.setOnClickListener(this);
        swForAllUsers.setOnClickListener(this);

        membersList = new ArrayList<>();

        //Fecha por defecto: actual
        taskDateTimeSelected = LocalTime.now();

        //Establece la fecha. Controla que sea posterior a la fecha de inicio
        LocalDate startDate = DateFormatter.fromFirebaseFormatToLocalDate(stageRoot.getStartDate());
        if(LocalDate.now().isBefore(startDate)){
            taskDateSelected = startDate;
        }else{
            taskDateSelected = LocalDate.now();
        }
        tvSelectedDate.setText(taskDateSelected.toString());

        findViewById(R.id.btn_ok).setOnClickListener(this);
        findViewById(R.id.btn_cancel).setOnClickListener(this);

        fbStageTasksRef = fbDatabaseRef.child(FirebasePath.MAIN_TASKS).child(stageRoot.getProjectID()).child(stageRoot.getName());
        fbMembersRef = fbDatabaseRef.child(FirebasePath.MAIN_MEMBERS);
        fbUsersRef = fbDatabaseRef.child(FirebasePath.MAIN_USERS);

        descriptionError = StringFormatter.getLengthErrorMessageFrom(
                getContext(),
                R.string.error_description_must_be_between,
                Task.TASK_DESCRIPTION_MIN_LENGTH,
                Task.TASK_DESCRIPTION_MAX_LENGTH);

        setMembersListener();
        setValidators();

        //Listener para establecer la hora
        tpTaskTime.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
            @Override
            public void onTimeChanged(TimePicker timePicker, int hour, int minute) {
                taskDateTimeSelected = new LocalTime(hour, minute);
            }
        });
    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        //Inicializa el adapter una vez que se pueda acceder a la activity dueña del diálogo
        uAdapter = new UsersAdapter(ownerActivity, membersList);
        spMembersList.setAdapter(uAdapter);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();

        switch (id) {
            case R.id.btn_ok:
                //if (nameOK && descriptionOK) {
                if (descriptionOK) {
                    pbRecording.setVisibility(View.VISIBLE);

                    //Concatena LocalDate y LocalDateTime en una sola fecha
                    LocalDateTime ldt = new LocalDateTime(
                            taskDateSelected.getYear(),
                            taskDateSelected.getMonthOfYear(),
                            taskDateSelected.getDayOfMonth(),
                            taskDateTimeSelected.getHourOfDay(),
                            taskDateTimeSelected.getMinuteOfHour());

                    if(userRol.equalsIgnoreCase(Rol.ADMIN)){
                        User selectedUser = (User) spMembersList.getSelectedItem();
                        owner = selectedUser.getMail();
                    }

                    generatedTask = new Task(
                            stageRoot.getProjectID(),
                            stageRoot.getName(),
                            Randomizer.generateID(),
                            etDescription.getText().toString(),
                            owner,
                            false,
                            swForAllUsers.isChecked(),
                            //etName.getText().toString(),
                            LocalDateTimeFormatter.fromLocalDateToFirebaseFormat(ldt));

                    //Pasa la tarea generada a la actividad padre mediante los extras
                    registerTask(generatedTask);

                } else {
                    if (!descriptionOK) {
                        etDescription.setError(descriptionError);
                    }
                }
                break;
            case R.id.btn_cancel:
                dismiss();
                break;
            case R.id.sw_forallusers:
                if (swForAllUsers.isChecked()) {
                    cvOwner.setVisibility(View.GONE);
                } else {
                    cvOwner.setVisibility(View.VISIBLE);
                }
                break;
            case R.id.btn_set_date:
                LocalDate startDate = DateFormatter.fromFirebaseFormatToLocalDate(stageRoot.getStartDate());
                LocalDate endDate = DateFormatter.fromFirebaseFormatToLocalDate(stageRoot.getEndDate());
                new SpinnerDatePickerDialogBuilder()
                        .context(ownerActivity)
                        .callback((view1, year, monthOfYear, dayOfMonth) -> {
                            taskDateSelected = new LocalDate(year, monthOfYear, dayOfMonth);
                            tvSelectedDate.setText(taskDateSelected.toString());
                        })
                        //.spinnerTheme()
                        .showTitle(true)
                        .showDaySpinner(true)
                        .defaultDate(
                                startDate.year().get(),
                                startDate.monthOfYear().get(),
                                startDate.dayOfMonth().get())
                        .maxDate(
                                endDate.year().get(),
                                endDate.monthOfYear().get(),
                                endDate.dayOfMonth().get())
                        .minDate(
                                startDate.year().get(),
                                startDate.monthOfYear().get(),
                                startDate.dayOfMonth().get())
                        .build()
                        .show();
                break;
        }
    }

    /**
     * Establece las reglas de validación de los EditText's presentes en el cuadro de diálogo
     */
    private void setValidators() {
        etDescription.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String desctription = etDescription.getText().toString();
                descriptionOK = desctription != null &&
                        desctription.length() > Task.TASK_DESCRIPTION_MIN_LENGTH &&
                        desctription.length() < Task.TASK_DESCRIPTION_MAX_LENGTH;

                if (!descriptionOK) {
                    etDescription.setError(descriptionError);
                }
            }
        });
    }

    /**
     * Carga los usuarios a los que se les puede asignar las tareas
     */
    private void setMembersListener() {
        fbMembersRef.child(stageRoot.getProjectID()).child(fbCurrentUser.getUid()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.getValue()==null){
                    return;
                }

                userRol = dataSnapshot.child(FirebasePath.COMMON_ROL).getValue(String.class);

                //Esconde la selección de dueño de tarea si el usuario es un worker
                if(userRol.equalsIgnoreCase(Rol.WORKER)){
                    cvOwner.setVisibility(View.GONE);
                    swForAllUsers.setVisibility(View.GONE);
                    owner = fbCurrentUser.getEmail();
                }else{
                    fbMembersRef.child(stageRoot.getProjectID()).addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            for (DataSnapshot dsUser : dataSnapshot.getChildren()) {
                                //Si el usuario actual no es administrador, no muestres administradores para asignarles tareas
                                if(!userRol.equalsIgnoreCase(Rol.ADMIN) && dsUser.child(FirebasePath.COMMON_ROL).getValue().toString().equalsIgnoreCase(Rol.ADMIN)){
                                    continue;
                                }
                                fbUsersRef.child(dsUser.getKey()).addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        User auxUser = new User(
                                                dataSnapshot.getKey(),
                                                dataSnapshot.child(FirebasePath.COMMON_MAIL).getValue().toString(),
                                                dataSnapshot.child(FirebasePath.COMMON_NAME).getValue().toString());

                                        //Comprueba que no esté añadido ya
                                        if (!membersList.contains(auxUser)) {
                                            uAdapter.add(auxUser);
                                        }
                                    }
                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {}
                                });
                            }
                        }
                        @Override
                        public void onCancelled(DatabaseError databaseError) {}
                    });
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    /**
     * Graba en la base de datos la tarea generada mediante el cuadro de diálogo
     *
     * @param generatedTask Tarea nueva
     */
    private void registerTask(Task generatedTask) {
        fbStageTasksRef.child(generatedTask.getName()).setValue(generatedTask, (databaseError, databaseReference) -> {
            pbRecording.setVisibility(View.GONE);
            if (databaseError == null) {
                Toast.makeText(getContext(), R.string.lbl_task_recorded, Toast.LENGTH_SHORT).show();
                dismiss();
            } else {
                Toast.makeText(getContext(), R.string.error_disconnected, Toast.LENGTH_SHORT).show();
            }
        });
    }
}
