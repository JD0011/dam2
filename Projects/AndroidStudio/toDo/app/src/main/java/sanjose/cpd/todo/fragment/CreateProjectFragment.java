package sanjose.cpd.todo.fragment;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import org.joda.time.LocalDate;

import java.util.ArrayList;

import sanjose.cpd.todo.R;
import sanjose.cpd.todo.activity.ProjectActivity;
import sanjose.cpd.todo.pojo.Project;
import sanjose.cpd.todo.util.FirebasePath;
import sanjose.cpd.todo.util.Rol;
import sanjose.cpd.todo.util.StringFormatter;

/**
 * Permite crear un proyecto desde cero.
 */

public class CreateProjectFragment extends Fragment {
    //Pantalla
    private EditText etProjectName, etProjectDescription;
    private boolean dataOK, nameInUse;
    private String projectName, projectKey, projectDescription;
    private String nameError, descriptionError;
    private ProgressBar pbProjectCreation;

    private AlertDialog.Builder adBuilder;

    //Autenticacion
    private FirebaseAuth fbAuth = FirebaseAuth.getInstance();

    //Firebase BBDD
    private FirebaseDatabase fbDatabase = FirebaseDatabase.getInstance();
    private DatabaseReference fbDatabaseRef = fbDatabase.getReference(FirebasePath.DB_NAME);

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_create_project, container, false);
        setHasOptionsMenu(true);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        etProjectName = (EditText) view.findViewById(R.id.et_project_name);
        etProjectDescription = (EditText) view.findViewById(R.id.et_description);

        pbProjectCreation = (ProgressBar) view.findViewById(R.id.pb_create_project_process);
        pbProjectCreation.setIndeterminate(true);

        adBuilder = new AlertDialog.Builder(getActivity().getApplicationContext());

        /*  Forma los mensajes de error de longitud en función de las constantes establecidas en
            las respectivas clases
         */

        nameError = StringFormatter.getLengthErrorMessageFrom(
                getActivity().getApplicationContext(),
                R.string.error_name_must_be_between,
                Project.PROJECT_NAME_MIN_LENGTH,
                Project.PROJECT_NAME_MAX_LENGTH);

        descriptionError = StringFormatter.getLengthErrorMessageFrom(
                getActivity().getApplicationContext(),
                R.string.error_description_must_be_between,
                Project.PROJECT_DESCRIPTION_MIN_LENGTH,
                Project.PROJECT_DESCRIPTION_MAX_LENGTH);

        setValidators();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_create_project, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.it_next:
                checkData();
                if (dataOK) {
                    generateProject();
                }
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void checkData() {
        dataOK = false;

        boolean validName = etProjectName.getText().toString().length() < Project.PROJECT_NAME_MAX_LENGTH && !TextUtils.isEmpty(etProjectName.getText());
        boolean validDescription = etProjectDescription.getText().toString().length() < Project.PROJECT_DESCRIPTION_MAX_LENGTH && !TextUtils.isEmpty(etProjectDescription.getText());

        if (validName && validDescription && !nameInUse) {
            dataOK = true;
        } else {

            dataOK = false;

            //Muestra mensajes de error correspondientes
            if (!validName) {
                etProjectName.setError(nameError);
            }

            if (!validDescription) {
                etProjectDescription.setError(descriptionError);
            }

            if (nameInUse) {
                etProjectName.setError(getString(R.string.error_name_in_use));
            }
        }
    }

    private void setValidators() {
        etProjectName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                projectName = etProjectName.getText().toString().trim();

                projectName = StringFormatter.toCapitalCase(projectName);

                if (projectName.length() < Project.PROJECT_NAME_MIN_LENGTH || projectName.length() > Project.PROJECT_NAME_MAX_LENGTH) {
                    etProjectName.setError(nameError);
                } else {
                    nameInUse = false;

                    Query qNameAlreadyExists = fbDatabaseRef.child(FirebasePath.MAIN_PROJECTS).orderByChild(FirebasePath.COMMON_NAME).equalTo(projectName);
                    qNameAlreadyExists.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            if (dataSnapshot.getValue() != null) {
                                nameInUse = true;
                                etProjectName.setError(getActivity().getApplicationContext().getString(R.string.error_name_in_use));
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                }
            }
        });

        etProjectDescription.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                projectDescription = etProjectDescription.getText().toString().trim();

                projectDescription = StringFormatter.toCapitalCase(projectDescription);

                if (projectDescription.length() > Project.PROJECT_DESCRIPTION_MAX_LENGTH) {
                    etProjectDescription.setError(descriptionError);
                }
            }
        });
    }

    /**
     * Se encarga de grabar el proyecto en la base de datos
     *
     * @return Key que representa al proyecto en la base de datos
     */
    private void generateProject() {

        pbProjectCreation.setVisibility(View.VISIBLE);

        DatabaseReference fbProjectRef = fbDatabaseRef.child(FirebasePath.MAIN_PROJECTS).child(projectName);
        DatabaseReference fbProjectMembers = fbDatabaseRef.child(FirebasePath.MAIN_MEMBERS);

        String uniqueID = fbProjectRef.getKey();

        projectKey = uniqueID;

        ArrayList<Integer> creationDate = new ArrayList<>();

        creationDate.add(LocalDate.now().year().get());
        creationDate.add(LocalDate.now().monthOfYear().get());
        creationDate.add(LocalDate.now().dayOfMonth().get());

        Project p = new Project(uniqueID, projectName, projectDescription, fbAuth.getCurrentUser().getUid(), creationDate);

        FirebaseUser user = fbAuth.getCurrentUser();
        String userIdentifier = user.getUid();

        DatabaseReference fbUserRef = fbDatabaseRef.child(FirebasePath.MAIN_USERS).child(userIdentifier);

        //Agrega al usuario como administrador, además del correo
        fbProjectMembers.child(projectName).child(user.getUid()).child(FirebasePath.COMMON_ROL).setValue(Rol.ADMIN, (databaseError, databaseReference) -> {
            if (databaseError != null) {
                Toast.makeText(getActivity(), R.string.error_something_wrong, Toast.LENGTH_SHORT).show();
            } else {
                fbProjectMembers.child(projectName).child(user.getUid()).child(FirebasePath.COMMON_MAIL).setValue(user.getEmail(), (databaseError1, databaseReference1) -> {
                    if (databaseError1 != null) {
                        Toast.makeText(getActivity(), R.string.error_something_wrong, Toast.LENGTH_SHORT).show();
                    } else {
                        //Agrega el proyecto a la lista
                        fbProjectRef.setValue(p, (databaseError2, databaseReference2) -> {
                            pbProjectCreation.setVisibility(View.GONE);
                            if (databaseError2 != null) {
                                Toast.makeText(getActivity().getApplicationContext(), R.string.error_something_wrong, Toast.LENGTH_SHORT).show();
                            } else {
                                Intent i = new Intent(getActivity().getApplicationContext(), ProjectActivity.class);
                                i.putExtra("project_key", projectKey);
                                i.putExtra("tab_number", ProjectActivity.TAB_MEMBERS);
                                startActivity(i);

                                //Cambia esta vista a la de listado de proyectos
                                getFragmentManager()
                                        .beginTransaction()
                                        .replace(R.id.fl_content, new ProjectListingFragment())
                                        .commit();
                            }
                        });
                    }
                });
            }
        });
    }
}
