package sanjose.cpd.todo.util;

import org.joda.time.DateTime;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.Random;

/**
 * Crea un ID aleatorio formado por el tiempo actual en milésimas más un conjunto de cuatro números.
 * Las posibilidades de que este identificador se repita son mínimas debido al pequeño volúmen de
 * datos que interactúa con dicho ID.
 */

public abstract class Randomizer {
    /**
     * Genera un ID aleatorio
     *
     * @return Timestamp + 4 números
     */
    public static String generateID() {
        String randomID = "" + DateTime.now().getMillis() + new Random().nextInt(9999);
        return randomID;
    }
}
