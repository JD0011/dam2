package sanjose.cpd.todo.pojo;
// Generated 30-abr-2018 17:58:32 by Hibernate Tools 5.2.10.Final

import com.google.firebase.database.Exclude;

import org.joda.time.LocalDate;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Proyecto
 */
public class Project {

	//Project constants
	public static final int PROJECT_NAME_MAX_LENGTH = 20;
	public static final int PROJECT_NAME_MIN_LENGTH = 4;
	public static final int PROJECT_DESCRIPTION_MAX_LENGTH = 140;
	public static final int PROJECT_DESCRIPTION_MIN_LENGTH = 50;

	private String ID;
	private String name;
	private String description;
	private String ownerID;
	private ArrayList<Integer> creationDateAsList;

	public Project(){

	}

	public Project(String ID, String name, String description) {
		this.ID = ID;
		this.name = name;
		this.description = description;
	}


	public Project(String ID, String name, String description, String ownerID,ArrayList<Integer> creationDate) {
		this.ID = ID;
		this.name = name;
		this.description = description;
		this.ownerID = ownerID;
		this.creationDateAsList = creationDate;
	}

	public String getId() {
		return ID;
	}

	public void setId(String ID) {
		this.ID = ID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return description;
	}

	public String getOwnerID() {
		return ownerID;
	}

	public void setOwnerID(String ownerID) {
		this.ownerID = ownerID;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public ArrayList<Integer> getCreationDate() {
		return creationDateAsList;
	}

	public void setCreationDate(ArrayList<Integer> creationDateAsList) {
		this.creationDateAsList = creationDateAsList;
	}

	@Exclude
	public Map<String,Object> toMap() {
		HashMap<String,Object> values = new HashMap<>();
		values.put("id", ID);
		values.put("name", name);
		values.put("description", description);
		values.put("creation_date", creationDateAsList);

		return values;
	}
}
