package sanjose.cpd.todo.pojo;

/**
 * Contiene únicamente información sobre el usuario y su rol
 */
public class UserRol {

    private String id, mail, rol;

    public UserRol(String id, String mail, String rol) {
        this.id = id;
        this.mail = mail;
        this.rol = rol;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }
}
