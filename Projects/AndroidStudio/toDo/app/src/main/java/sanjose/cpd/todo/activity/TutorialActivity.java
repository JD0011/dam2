package sanjose.cpd.todo.activity;

import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import sanjose.cpd.todo.R;
import sanjose.cpd.todo.util.ResourceURL;

/**
 * Muestra un tutorial al usuario accediendo al almacenamiento en la nube de firebase
 */
public class TutorialActivity extends AppCompatActivity {

    private WebView wvTutorial;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutorial);
        setTitle(R.string.lbl_manual);

        wvTutorial = (WebView) findViewById(R.id.wv_tutorial);
        loadTutorial();
    }

    /**
     * Carga el tutorial almacenado en la URL especificada
     */
    private void loadTutorial(){
        wvTutorial.setWebViewClient(new WebViewClient());
        wvTutorial.getSettings().setJavaScriptEnabled(true);
        wvTutorial.loadUrl(ResourceURL.TUTORIAL);
    }
}
