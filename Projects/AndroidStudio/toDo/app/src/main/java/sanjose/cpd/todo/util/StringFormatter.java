package sanjose.cpd.todo.util;

import android.content.Context;
import android.content.res.Resources;

import sanjose.cpd.todo.R;
import sanjose.cpd.todo.pojo.Stage;

/**
 * Esta clase proporciona útiles para formateo de Strings determinados
 */

public abstract class StringFormatter {

    /**
     * Primera letra en mayúsculas y demás en minúsculas
     * @param text
     * @return
     */
    public static String toCapitalCase(String text){
        if(text.length()!=0){
            return text.substring(0,1).toUpperCase() + text.substring(1).toLowerCase();
        }else{
            return "";
        }
    }

    public static String getLengthErrorMessageFrom (Context ctx, int stringResource, int minLength, int maxLength){
        String msg =
                ctx.getString(stringResource)
                + " " +
                        minLength
                + " " +
                ctx.getString(R.string.lbl_and)
                + " " +
                maxLength
                + " " +
                ctx.getString(R.string.lbl_characters);

        return msg;
    }
}
