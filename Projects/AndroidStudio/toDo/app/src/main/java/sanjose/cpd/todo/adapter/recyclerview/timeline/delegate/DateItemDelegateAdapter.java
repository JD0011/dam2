package sanjose.cpd.todo.adapter.recyclerview.timeline.delegate;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import sanjose.cpd.todo.R;
import sanjose.cpd.todo.adapter.recyclerview.timeline.TimeLineType;
import sanjose.cpd.todo.adapter.recyclerview.timeline.ViewTypeDelegateAdapter;
import sanjose.cpd.todo.adapter.recyclerview.timeline.type.DayView;

/**
 * Representa un separador conformado por una fecha determinada.
 */

public class DateItemDelegateAdapter implements ViewTypeDelegateAdapter {

    public class DateItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView date;
        public DateItemViewHolder(View itemView) {
            super(itemView);
            date = itemView.findViewById(R.id.tv_time_value);
        }

        public void bind(DayView dv){
            date.setText(dv.getLocalDate().toString());
        }

        @Override
        public void onClick(View view) {
            Toast.makeText(view.getContext(), "Click detectado en header", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public DateItemViewHolder onCreateViewHolder(ViewGroup parent) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.timeline_task_date, parent, false);
        final DateItemViewHolder vh = new DateItemViewHolder(v);

        //Consultar ProjectAdapter para el handle del click

        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, TimeLineType item) {
        ((DateItemViewHolder) holder).bind((DayView) item);
    }
}
