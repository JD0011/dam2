package sanjose.cpd.todo.pojo;

/**
 * Clase simple para representar el conjunto de datos de un usuario necesarios en algunos fragmentos
 * de la aplicación
 */

public class User {

    private String id, mail, name;

    public User(String id, String mail, String name) {
        this.id = id;
        this.mail = mail;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
