package sanjose.cpd.todo.pojo;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import sanjose.cpd.todo.adapter.recyclerview.timeline.TimeLineType;

/**
 * Tarea
 */

public class Task implements Parcelable, TimeLineType {

    //Stage constants
    public static final int TASK_DESCRIPTION_MAX_LENGTH = 90;
    public static final int TASK_DESCRIPTION_MIN_LENGTH = 10;
    public static final String TIME_PATTERN = "HH:mm";

    private String projectID;
    private String stageID;
    private String name;
    private String description;
    private String owner;
    private Boolean forAllUsers;
    private Boolean isFinished;
    private ArrayList<Integer> datetime;

    public Task() {

    }

    public Task(String projectID, String stageID, String name, String description, String owner, Boolean isFinished, Boolean forAllUsers, ArrayList<Integer> datetime) {
        this.projectID = projectID;
        this.stageID = stageID;
        this.name = name;
        this.description = description;
        this.owner = owner;
        this.isFinished = isFinished;
        this.forAllUsers = forAllUsers;
        //this.notes = notes;
        this.datetime = datetime;
    }

    /**
     * Crea Task a partir de un parcel. Este constructor se utiliza al pasar un objeto de esta
     * clase entre fragments
     *
     * @param in Fuente de datos
     */
    public Task(Parcel in) {
        String[] stringProperties = new String[4];
        in.readStringArray(stringProperties);
        this.projectID = stringProperties[0];
        this.stageID = stringProperties[1];
        this.name = stringProperties[2];
        this.description = stringProperties[3];
        ArrayList<Integer> datetime = new ArrayList<>();
        in.readList(datetime, null);
        this.datetime = datetime;
    }

    public String getProjectID() {
        return projectID;
    }

    public void setProjectID(String projectID) {
        this.projectID = projectID;
    }

    public String getStageID() {
        return stageID;
    }

    public void setStageID(String stageID) {
        this.stageID = stageID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public Boolean isFinished() {
        return isFinished;
    }

    public void setFinished(Boolean finished) {
        isFinished = finished;
    }

    public Boolean getForAllUsers() {
        return forAllUsers;
    }

    public void setForAllUsers(Boolean forAllUsers) {
        this.forAllUsers = forAllUsers;
    }

    public ArrayList<Integer> getDateTime() {
        return datetime;
    }

    public void setDateTime(ArrayList<Integer> datetime) {
        this.datetime = datetime;
    }

    @Override
    public int getViewType() {
        return TimeLineType.ITEM;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeStringArray(new String[]{
                this.projectID,
                this.stageID,
                this.name,
                this.description
        });
        //parcel.writeMap(notes);
        parcel.writeList(datetime);
    }

    /**
     * Permite leer la información de un parcelable
     */
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {

        @Override
        public Task createFromParcel(Parcel parcel) {
            return new Task(parcel);
        }

        @Override
        public Task[] newArray(int i) {
            return new Task[i];
        }
    };
}
