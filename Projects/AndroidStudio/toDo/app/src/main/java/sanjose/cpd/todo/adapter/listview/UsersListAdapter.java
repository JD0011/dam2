package sanjose.cpd.todo.adapter.listview;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import sanjose.cpd.todo.R;

/**
 * Muestra a los usuarios en un listview mediante un String que los representa
 */

public class UsersListAdapter extends ArrayAdapter{

    private Activity context;

    public UsersListAdapter(Activity context, ArrayList<String> users) {
        super(context, R.layout.listview_textview_template, users);
        this.context = context;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View item = inflater.inflate(R.layout.listview_textview_template, parent, false);

        TextView tvMail = (TextView)item.findViewById(R.id.tv_mail);
        tvMail.setText(getItem(position).toString());

        return item;
    }
}
