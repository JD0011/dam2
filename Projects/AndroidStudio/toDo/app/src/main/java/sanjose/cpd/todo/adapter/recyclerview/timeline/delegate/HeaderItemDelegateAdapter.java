package sanjose.cpd.todo.adapter.recyclerview.timeline.delegate;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import sanjose.cpd.todo.R;
import sanjose.cpd.todo.adapter.recyclerview.timeline.TimeLineType;
import sanjose.cpd.todo.adapter.recyclerview.timeline.ViewTypeDelegateAdapter;
import sanjose.cpd.todo.pojo.Stage;
import sanjose.cpd.todo.util.DateFormatter;

/**
 * Esta clase representaría las Stages en forma de header del timeline. ADAPTAR
 * Created by juand on 30/05/2018.
 */

public class HeaderItemDelegateAdapter implements ViewTypeDelegateAdapter {

    public class HeaderItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvName;
        TextView tvDate;

        public HeaderItemViewHolder(View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tv_name);
            tvDate = itemView.findViewById(R.id.tv_stage_date);
        }


        public void bind(Stage stage) {
            tvName.setText(stage.getName());
            tvDate.setText(
                    DateFormatter.fromFirebaseFormatToLocalDate(stage.getStartDate()).toString() +
                    " / " +
                    DateFormatter.fromFirebaseFormatToLocalDate(stage.getEndDate()));
        }


        @Override
        public void onClick(View view) {
            Toast.makeText(view.getContext(), "Click detectado en header", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public HeaderItemViewHolder onCreateViewHolder(ViewGroup parent) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.timeline_task_firstitem, parent, false);
        final HeaderItemViewHolder vh = new HeaderItemViewHolder(v);

        //Consultar ProjectAdapter para el handle del click

        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, TimeLineType item) {
        ((HeaderItemViewHolder) holder).bind((Stage) item);
    }
}
