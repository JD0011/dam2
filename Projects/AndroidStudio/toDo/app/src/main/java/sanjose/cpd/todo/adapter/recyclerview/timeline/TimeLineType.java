package sanjose.cpd.todo.adapter.recyclerview.timeline;

/**
 * Representa los tipos de vista definibles para el componente MaterialTimeLineView. Cada uno
 * representa a un layout diferente.
 * Created by juand on 30/05/2018.
 */

public interface TimeLineType {

    int HEADER = 0;
    int DATE = 1;
    int ITEM = 2;

    int getViewType();

}
