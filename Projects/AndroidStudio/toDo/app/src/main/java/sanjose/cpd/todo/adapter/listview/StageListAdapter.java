package sanjose.cpd.todo.adapter.listview;

import android.app.Activity;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import sanjose.cpd.todo.R;
import sanjose.cpd.todo.pojo.Stage;
import sanjose.cpd.todo.util.DateFormatter;

/**
 * Permite visualizar las etapas en un listview
 */
public class StageListAdapter extends ArrayAdapter {

    private Activity context;
    private ArrayList<Stage> stages;

    public StageListAdapter(Activity context, ArrayList<Stage> stages){
        super(context, R.layout.listview_stages_template,stages);
        this.context = context;
        this.stages = stages;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View item = inflater.inflate(R.layout.recyclerview_stage_template, parent, false);

        TextView tvStageName = (TextView)item.findViewById(R.id.tv_name);
        tvStageName.setText(stages.get(position).getName());

        TextView tvDescription = (TextView)item.findViewById(R.id.tv_description);
        tvDescription.setText(stages.get(position).getDescription());

        TextView startDate = (TextView)item.findViewById(R.id.tv_startdate_value);
        startDate.setText((DateFormatter.fromFirebaseFormatToLocalDate(stages.get(position).getStartDate())).toString());

        TextView endDate = (TextView)item.findViewById(R.id.tv_enddate_value);
        endDate.setText((DateFormatter.fromFirebaseFormatToLocalDate(stages.get(position).getEndDate())).toString());

        return item;
    }

    @Override
    public int getCount() {
        return stages.size();
    }
}