package sanjose.cpd.todo.listener;

import android.view.View;

/**
 * Interfaz que ofrece la implementación el evento onItemClick. Se utiliza dentro de un
 * recyclerView para permitir la asignación de listener a los items
 */

public interface RecyclerViewItemClickListener {
    void onItemClick(View v, int position);

}
