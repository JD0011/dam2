package sanjose.cpd.todo.util;

import android.app.DatePickerDialog;
import android.content.res.Resources;

import org.joda.time.LocalDate;

import java.util.ArrayList;

import sanjose.cpd.todo.R;

/**
 * Se encarga de ofrecer distintos formatos de fecha, los cuales se han ido desarrollando en
 * función de las necesidades de interpretación de las fechas basandose en el sistema en el que
 * se utilizan
 */

public abstract class DateFormatter {

    public static final String DATE_FORMAT = "yyyy/MM/dd";

    public static ArrayList<Integer> fromLocalDateFirebaseFormat(LocalDate localDate) {
        ArrayList<Integer> date = new ArrayList<>();

        date.add(localDate.getYear());
        date.add(localDate.getMonthOfYear());
        date.add(localDate.getDayOfMonth());

        return date;
    }

    public static LocalDate fromFirebaseFormatToLocalDate(ArrayList<Integer> date) {
        LocalDate localDate = null;

        if (date.size() == 3) {
            localDate = new LocalDate(date.get(0), date.get(1), date.get(2));
        }

        return localDate;
    }
}
