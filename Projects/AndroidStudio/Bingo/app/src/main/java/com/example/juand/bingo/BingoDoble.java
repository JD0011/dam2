/**
 * Bingo con dos cartones. Se implementan las clases necesarias para el correcto funcionamiento
 * del bingo. Esta vez se crean dos objetos Carton.
 *
 * @author Juan de Dios Delgado Bermúdez
 */
package com.example.juand.bingo;

import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TextView;

public class BingoDoble extends AppCompatActivity {

    private Button btnVolver;
    private ConstraintLayout clBombo;
    private TableLayout tlCarton1, tlCarton2;
    private TextView tvResultado;
    private RelativeLayout bolaResultado;
    private Bingo juego;
    private Carton carton1, carton2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bingo_doble);

        //Inicialización de variables
        clBombo = (ConstraintLayout) findViewById(R.id.bombo);
        tlCarton1 = (TableLayout) findViewById(R.id.carton1);
        tlCarton2 = (TableLayout) findViewById(R.id.carton2);
        bolaResultado = (RelativeLayout) findViewById(R.id.bolaResultado_layout);
        tvResultado = (TextView) findViewById(R.id.resultado_tv);
        btnVolver = (Button) findViewById(R.id.volver_btn);

        juego = new Bingo(bolaResultado,clBombo);
        carton1 = new Carton(tlCarton1, "CARTON 1");
        carton2 = new Carton(tlCarton2, "CARTON 2");

        //CLICK EN EL BOMBO
        clBombo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!juego.bomboMoviendose()){
                    //numDisponibles==0? Juego finalizado porque no quedan números
                    if (juego.hayNumerosDisponibles()) {
                        juego.moverBombo(BingoDoble.this);

                        if(carton1.tacharNumero(juego.getNumResultado())){
                            //Comprueba linea
                            if(carton1.compruebaLinea(juego)){
                                carton1.tacharLinea(carton1.getLinea());
                                estableceResultado(carton1.getNombre()+": "+getResources().getString(R.string.linea_hecha_en)+String.valueOf(carton1.getLinea()+1));
                            }
                        }

                        if(carton2.tacharNumero(juego.getNumResultado())){
                            //Comprueba linea
                            if(carton2.compruebaLinea(juego)){
                                carton2.tacharLinea(carton2.getLinea());
                                estableceResultado(carton2.getNombre()+": "+getResources().getString(R.string.linea_hecha_en)+String.valueOf(carton2.getLinea()+1));
                            }
                        }

                        //COMPRUEBA BINGO
                        if(carton1.tieneBingo()){
                            estableceResultado(carton1.getNombre()+": "+getResources().getString(R.string.bingo));
                        }

                        if(carton2.tieneBingo()){
                            tvResultado.setText(carton2.getNombre()+": "+getResources().getString(R.string.bingo));
                        }

                        if(carton1.tieneBingo() && carton2.tieneBingo()){
                            estableceResultado(carton1.getNombre()+", "+carton2.getNombre()+getResources().getString(R.string.bingo));
                            clBombo.setEnabled(false);
                        }
                    }
                }
            }
        });

        btnVolver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    /**
     * Sobreescribe el antiguo valor del textview resultado por el nuevo
     * @param message
     */
    public void estableceResultado(String message){
        tvResultado.setText("");
        tvResultado.setText(message);
    }
}