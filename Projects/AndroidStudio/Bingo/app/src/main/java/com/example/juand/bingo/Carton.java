/**
 * Clase Carton.
 * Representa un cartón del juego, con sus correspondientes números generados de forma aleatoria.
 * Se encarga de procesar el resultado producido por el {@link Bingo} y modificar las bolas,
 * cambiando su imagen. Detecta cuando se ha hecho línea y cuando bingo.
 *
 * @author Juan de Dios Delgado Bermúdez
 */
package com.example.juand.bingo;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.text.style.BackgroundColorSpan;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.ResourceBundle;

public class Carton {

    //Almacena RelativeLayout's que representan las bolas
    private ArrayList<RelativeLayout> rlBolasCarton;
    private String strNombre;
    private TableLayout tlCarton;
    //Linea premiada
    private int linea;
    private int numerosTachados;
    private boolean tieneLinea;

    public Carton(TableLayout carton, String nombre){
        tlCarton = carton;
        strNombre = nombre;

        Initialize();
    }

    /**
     * Inicializa algunas variables con valor por defecto necesario para el correcto
     * funcionamiento de la clase.
     */
    private void Initialize(){
        //Extraer bolas (RelativeLayout compuesto de imagen y textview) del TableLayout
        rlBolasCarton = extraeBolas(tlCarton);

        asignarNumeros(); //a las bolas
        //Linea por defecto
        tieneLinea = false;
    }

    /**
     * Establece el texto de las bolas de un carton en un número aleatorio (Bolas: 15)
     */
    private void asignarNumeros(){
        ArrayList<TextView> tvNumeros;
        ArrayList<Integer> numeros;

        //Crear números
        numeros = new ArrayList<Integer>();
        for(int i=1;i<100;i++){
            numeros.add(i);
        }
        //Mezclarlos
        Collections.shuffle(numeros);

        //Asignación de números
        tvNumeros = new ArrayList<TextView>();
        for(RelativeLayout bola : rlBolasCarton) {
            for (int i = 0; i < bola.getChildCount(); i++) {
                //Obtener posición 1 -> TextView
                ((TextView) bola.getChildAt(1)).setText(String.valueOf(numeros.get(0))); //Posición del TextView
                //Eliminar numero de lista
                numeros.remove(0);
                numeros.trimToSize();
            }
        }
    }

    /**
     * Su funcionamiento consiste en recoger las bolas (Relative Layout) a partir del tablelayout
     * especificado y almacenarlos en la lista pasada por parámetros
     * @param tableLayout
     */
    private ArrayList<RelativeLayout> extraeBolas(TableLayout tableLayout){
        ArrayList<RelativeLayout> aux;
        TableRow row;

        aux = new ArrayList<RelativeLayout>();
        //Inspección de elementos de la tabla
        for(int i = 0; i< tableLayout.getChildCount(); i++){
            if(tlCarton.getChildAt(i) instanceof TableRow){

                //Inspección de elementos de la fila
                row = (TableRow) tlCarton.getChildAt(i);
                for(int j=0;j<row.getChildCount();j++){
                    if(row.getChildAt(j) instanceof RelativeLayout){
                        aux.add((RelativeLayout) row.getChildAt(j));
                    }
                }
            }
        }

        return aux;
    }

    public boolean tacharNumero(int num){
        boolean numTachado;
        ImageView cruz;
        TextView aux;
        //Buscamos coincidencias con respecto al número contenido
        numTachado=false;
        for(int i = 0; i< rlBolasCarton.size(); i++){
            aux=(TextView) rlBolasCarton.get(i).getChildAt(1);
            //Si coinciden, tachamos el número
            if(Integer.parseInt(aux.getText().toString())==num){
                ((ImageView) rlBolasCarton.get(i).getChildAt(0)).setImageResource(R.drawable.ball_encontrada);
                numerosTachados++;
                numTachado=true;
            }
        }
        return numTachado;
    }

    /**
     * Cambia el color del fondo del cartón si se han encontrado todas las bolas
     * @return
     */
    public boolean tieneBingo(){
        boolean tieneBingo;
        if(tieneBingo=numerosTachados==rlBolasCarton.size()){
            tlCarton.setBackgroundColor(Color.parseColor("#E8EACD"));
        }
        return tieneBingo;
    }

    /**
     * Comprobará si se ha hecho linea en alguna de las 3 siempre que no se haya hecho linea ya.
     * Cuenta el número de bolas cuyo valor de TextView no se encuentra entre los números
     * disponibles (o lo que es lo mismo, ya ha salido). Si el nº de bolas es igual a 5, se ha
     * hecho línea, y se guarda el valor de i como indicador de línea.
     */
    public boolean compruebaLinea(Bingo juego){
        //Cuenta las bolas encontradas
        int counter;
        TableRow auxRow;
        RelativeLayout auxBola;

        //Mientras no se haya encontrado una linea
        if(!tieneLinea){
            //Fila a fila
            for(int i=0;i<tlCarton.getChildCount();i++){
                if(tlCarton.getChildAt(i) instanceof  TableRow){
                    auxRow = (TableRow) tlCarton.getChildAt(i);

                    //Inspecciona las bolas
                    counter = 0;
                    for(int j=0;j<auxRow.getChildCount();j++){
                        if(auxRow.getChildAt(j) instanceof RelativeLayout){
                            auxBola = (RelativeLayout) auxRow.getChildAt(j);
                            int num = Integer.parseInt(((TextView)(auxBola.getChildAt(1))).getText().toString());
                            //Si el número ya ha salido, añadelo al contador
                            if(!juego.getNumsDisponibles().contains(num)){
                                counter++;
                            }
                        }
                    }

                    if(counter==5){ tieneLinea = true; linea=i; } //linea -> premiada
                }
            }
        }

        return tieneLinea;
    }

    /**
     * Extrae la TableRow especificada por parámetros y establece como imagen para las bolas
     * la correspobdiente a linea hecha
     * @param numLinea Linea que tachar
     */
    public void tacharLinea(int numLinea){
        RelativeLayout bola;
        TableRow row;
        row = (TableRow) tlCarton.getChildAt(numLinea);
        //Cambiamos imagen de la bola por el de bola de linea
        for(int i = 0; i< row.getChildCount(); i++){
            if(row.getChildAt(i) instanceof  RelativeLayout){
                bola = (RelativeLayout) row.getChildAt(i);
                //Bola.getChildAt(0) -> ImageView
                ((ImageView) bola.getChildAt(0)).setImageResource(R.drawable.ball_linea);
            }
         }
    }

    public int getLinea(){
        return linea;
    }

    public String getNombre(){
        return strNombre;
    }
}

