/**
 * Clase que maneja el activity principal de la aplicación, desde el que puedes elegir el número
 * de cartones con el que jugar. Después de la elección, se deriva al layout correspondiente.
 *
 * @author Juan de Dios Delgado Bermúdez
 */
package com.example.juand.bingo;

import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Spinner;

public class Principal extends AppCompatActivity {

    private Toolbar menu;
    private Button btIniciar;
    private Spinner spCartones;
    private View.OnClickListener iniciarListenner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);

        btIniciar = (Button)findViewById(R.id.iniciar_btn);
        spCartones = (Spinner)findViewById(R.id.cartones_spinner);
        menu = (Toolbar)findViewById(R.id.menu_info);

        //Establecer toolbar

        setSupportActionBar(menu);
        menu.setOverflowIcon(ContextCompat.getDrawable(this, R.drawable.info_icon));

        iniciarListenner = new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //En función del index del valor elegido en el selector de num. de cartones...
                switch (spCartones.getSelectedItemPosition()){
                    case 0: launchActivity(BingoSimple.class); break;
                    case 1: launchActivity(BingoDoble.class); break;
                }
            }
        };

        btIniciar.setOnClickListener(iniciarListenner);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_info, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.info_item:
                FragmentManager fragmentManager = getFragmentManager();
                DialogoInfo dialogo = new DialogoInfo();
                dialogo.show(fragmentManager, "info");
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    //Lanza la clase especificada
    private void launchActivity(Class clazz){
        Intent i = new Intent(Principal.this,clazz);
        startActivity(i);
    }
}
