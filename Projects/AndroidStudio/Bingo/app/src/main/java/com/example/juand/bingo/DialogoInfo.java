/**
 * Dialogo de información.
 * Muestra información sobre como jugar, con respecto a los pasos que el jugador debe seguir
 * y cómo las bolas se van eliminando.
 */
package com.example.juand.bingo;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

/**
 * Created by juand on 19/11/2017.
 */

public class DialogoInfo extends DialogFragment{

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder
                //Contenido
                .setMessage(R.string.info_message)
                .setTitle(R.string.pasos)
                //Botón OK y listener de onClick
                .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //Salir
                        dialog.cancel();
                    }
                });
        return builder.create();
    }
}
