/**
 * Clase Bingo.
 * Representa el elemento bombo del juego. Ofrece un resultado de forma aleatoria para luego ser
 * procesado por los cartones y actuar en consecuencia.
 *
 * @author Juan de Dios Delgado Bermúdez
 */
package com.example.juand.bingo;

import android.app.Activity;
import android.support.constraint.ConstraintLayout;
import android.util.DisplayMetrics;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;

public class Bingo {

    private ArrayList<Integer> numerosDisponibles;
    private ConstraintLayout clBombo;

    private RelativeLayout rlBolaResultado;
    private int numResultado;
    //Control para evitar pulsar varias veces el botón jugar
    private boolean enMovimiento;

    public Bingo(RelativeLayout BolaResultado, ConstraintLayout bombo){
        rlBolaResultado = BolaResultado;
        clBombo = bombo;
        Initialize();
    }

    private void Initialize(){
        //LLENAR BOMBO
        numerosDisponibles = new ArrayList<Integer>();
        for(int i=1;i<100;i++){
            numerosDisponibles.add(i);
        }

        //Mezcla los números
        Collections.shuffle(numerosDisponibles);
    }

    /**
     * Moverá el bomdo en la dirección especificada
     *
     */
    public void moverBombo(Activity activity) {

        enMovimiento = true;

        final TranslateAnimation haciaDerecha, haciaCentro;
        DisplayMetrics medidas;
        int[] posicionOriginal;
        int fromX, toX;

        //Obtiene número nuevo
        numResultado=extraerNumero();

        //Medidas de la pantalla
        medidas = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(medidas);

        //Posición original de la bola
        posicionOriginal = new int[2];
        rlBolaResultado.getLocationOnScreen(posicionOriginal);

        //MOVER BOLA AL CENTRO
        fromX = -medidas.widthPixels; //Desde fuera de la pantalla
        haciaCentro = new TranslateAnimation(fromX,0,0,0);
        haciaCentro.setDuration(175);
        haciaCentro.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
            @Override
            public void onAnimationStart(Animation animation) {
                //Asigna un número disponible
                ((TextView)(rlBolaResultado.getChildAt(1))).setText(String.valueOf(numResultado));
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                enMovimiento = false;
            }
        });

        //MOVER BOLA A LA DERECHA
        toX = medidas.widthPixels; //Ancho total de la pantalla
        haciaDerecha = new TranslateAnimation(0,toX-posicionOriginal[0],0,0);
        haciaDerecha.setDuration(175);
        haciaDerecha.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationRepeat(Animation animation) {

            }

            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                //Ejecutar segunda animación al final de esta
                rlBolaResultado.startAnimation(haciaCentro);
            }
        });

        rlBolaResultado.startAnimation(haciaDerecha);
    }

    /**
     * Se encarga de devolver un número disponible a partir de una lista.
     * @return
     */
    public int extraerNumero(){
        int aux;
        //Guarda número
        aux = numerosDisponibles.get(0);
        //Borralo
        numerosDisponibles.remove(0);

        return aux;
    }

    public int getNumResultado(){
        return numResultado;
    }

    public ArrayList<Integer> getNumsDisponibles(){
        return numerosDisponibles;
    }

    public boolean bomboMoviendose(){
        return enMovimiento;
    }

    public boolean hayNumerosDisponibles(){
        return numerosDisponibles.size()>0;
    }
}
