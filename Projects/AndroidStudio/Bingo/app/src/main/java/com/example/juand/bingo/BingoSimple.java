/**
 * Bingo con un solo cartón. Se implementan las clases necesarias para el correcto funcionamiento
 * del bingo.
 *
 * @author Juan de Dios Delgado Bermúdez
 */
package com.example.juand.bingo;

import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TextView;

public class BingoSimple extends AppCompatActivity {

    private Button btnVolver;
    private ConstraintLayout clBombo;
    private TextView tvResultado;
    private TableLayout tlCarton;
    private RelativeLayout bolaResultado;
    private Bingo juego;
    private Carton carton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bingo_simple);

        //Inicialización de variables
        clBombo = (ConstraintLayout) findViewById(R.id.bombo);
        tlCarton = (TableLayout)findViewById(R.id.carton1);
        bolaResultado = (RelativeLayout)findViewById(R.id.bolaResultado_layout);
        tvResultado = (TextView)findViewById(R.id.resultado_tv);
        btnVolver = (Button)findViewById(R.id.volver_btn);

        juego = new Bingo(bolaResultado,clBombo);
        carton = new Carton(tlCarton,"CARTON 1");

        //CLICK EN EL BOMBO
        clBombo.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {
                if(!juego.bomboMoviendose()){
                    //numDisponibles==0? Juego finalizado porque no quedan números
                    if(juego.hayNumerosDisponibles()){
                        juego.moverBombo(BingoSimple.this);
                        //Si se tacha un numero
                        if(carton.tacharNumero(juego.getNumResultado())){
                            //Comprueba linea
                            if(carton.compruebaLinea(juego)){
                                carton.tacharLinea(carton.getLinea());
                                estableceResultado(carton.getNombre()+": "+getResources().getString(R.string.linea_hecha_en)+String.valueOf(carton.getLinea()+1));
                            }
                        }
                    }

                    //COMPRUEBA BINGO
                    if(carton.tieneBingo()){
                        estableceResultado(carton.getNombre()+": "+getResources().getString(R.string.bingo));
                        //Desactiva click
                        clBombo.setEnabled(false);
                    }
                }
            }
        });

        btnVolver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    /**
     * Sobreescribe el antiguo valor del textview resultado por el nuevo
     * @param message
     */
    public void estableceResultado(String message){
        tvResultado.setText("");
        tvResultado.setText(message);
    }
}
