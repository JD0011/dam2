/**
 * Buscaminas. NORMAS:
 * · Gana al encontrar la mina
 * · Hay dos intentos nada mas para encontrarla (puede establecerse dinámicamente)
 * · Conseguir que el código sea mantenible y reutilizable
 * · Se debe mostrar la mina si se encuentra
 *
 * Se ha almacenado únicamente los ID's de los botones, ya que es un requisito para el
 * constructor BMGame (recordando que esta clase trabaja con ID's en vez de controles).
 * Estos ID's se han extraido de los botones miembros del TableLayout tabla_minas.
 *
 * Posteriormente se ha inicializado una instancia de juego y se han inicializado los
 * listenner's del programa:
 *      · _oclResetListener: reinicia el juego
 *      · _oclMinaListener: procesa los clicks sobre los botones.
 *      · _oclSetIntentosListener: establece en el juego un nuevo Nº de intentos
 *
 * A su vez, se han declarado múltiples métodos para organizar el código que administra los
 * elementos visuales de la pantalla.
 *
 *  · Se pueden añadir botones de forma dinámica, siempre que sean hijos del TableLayout campo_minas
 *  · Se pueden utilizar cualquier tipo de control, siempre que disponga de un atributo ID entero
 *  · Se puede establecer el número de intentos
 *  ·
 */

package com.example.juand.buscaminas;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.ArrayList;

public class Buscaminas extends AppCompatActivity {

    //TableLayout que contiene todos los botones (huecos)
    private TableLayout _tlCampoMinas;
    private ArrayList<Integer> _arrlIDBotones;

    private TextView _tvIntentosLabel;
    private TextView _tvNumIntentosValue;
    private TextView _tvTextResultado;

    private Button _btnReiniciar;
    private Button _btnSetIntentos;

    //Juego
    private BMGame _bmGame;

    //Listener's
    View.OnClickListener _oclResetListener;
    View.OnClickListener _oclMinaListener;
    View.OnClickListener _oclSetIntentosListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buscaminas);

        //EXTRAER CONTROLES PRINCIPALES

        _tlCampoMinas = (TableLayout) findViewById(R.id.tabla_minas);
        _tvIntentosLabel = (TextView) findViewById(R.id.intentos_label);
        _tvNumIntentosValue = (TextView) findViewById(R.id.num_intentos_value);
        _tvTextResultado = (TextView) findViewById(R.id.resultado_label);

        _btnReiniciar = (Button) findViewById(R.id.reiniciar_button);
        _btnSetIntentos = (Button) findViewById(R.id.num_intentos_button);

        //EXTRAER TODOS LOS ID's DE LOS BOTONES
        _arrlIDBotones = new ArrayList<>();
        TableRow trCurrentRow;

        //Consigue, fila por fila, los botones del TableLayout _tlCampoMinas
        for(int i = 0; i< _tlCampoMinas.getChildCount(); i++){
            //Si es una fila
            if (_tlCampoMinas.getChildAt(i) instanceof TableRow){
                //Almacenala en trCurrentRow
                trCurrentRow = (TableRow) _tlCampoMinas.getChildAt(i);

                //Consigue los botones de la fila actual
                for(int j=0;j<trCurrentRow.getChildCount();j++){
                    //Si es un botón
                    if(trCurrentRow.getChildAt(i) instanceof Button){
                        //Guarda su ID
                        _arrlIDBotones.add(trCurrentRow.getChildAt(j).getId());
                    }
                }
            }
        }

        //INSTANCIA EL JUEGO PASANDO idBotones, numIntentos y numMinas
        _bmGame = new BMGame(_arrlIDBotones,2,1);
        _bmGame.generaMinas();

        actualizaIntentos();

        //LISTENER'S
        _oclMinaListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            Button botonElegido = (Button) findViewById(v.getId());

            //Si está minado
            if(_bmGame.estaMinado(botonElegido.getId())) {
                //Haz visible únicamente el botón minado y establece de texto "MINA"
                botonElegido.setText("MINA");
                setVisibilidadCampos(View.INVISIBLE);
                botonElegido.setVisibility(View.VISIBLE);

                _tvTextResultado.setText(R.string.win_msg);
            }
            //Si no está minado
            else{
                //Haz el botón invisible
                botonElegido.setVisibility(View.INVISIBLE);
                //Notifica jugada
                _tvTextResultado.setText(R.string.pierde_intento);

                //Si no quedan intentos
                if(!_bmGame.quedanIntentos()){
                    _tvTextResultado.setText(R.string.lose_msg);
                    //Desactiva todos los campos
                    setVisibilidadCampos(View.INVISIBLE);
                }
                actualizaIntentos();
            }
            }
        };

        _oclSetIntentosListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String strNumero;

                strNumero = _tvNumIntentosValue.getText().toString();
                procesaInputIntentos(strNumero);
            }
        };


        _oclResetListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _bmGame.reiniciar();
                reiniciarPantalla();
            }
        };

        //Establece el listener en todos los botones
        for(int i = 0; i< _arrlIDBotones.size(); i++){
            ((Button)(findViewById(_arrlIDBotones.get(i)))).setOnClickListener(_oclMinaListener);
        }

        _btnReiniciar.setOnClickListener(_oclResetListener);
        _btnSetIntentos.setOnClickListener(_oclSetIntentosListener);
    }

    //MÉTODOS EXTRA

    //Reinicia los elementos de la pantalla
    public void reiniciarPantalla(){
        _tvTextResultado.setText(R.string.new_game);
        limpiaTextoBotones();
        actualizaIntentos();
        setVisibilidadCampos(View.VISIBLE);
    }

    //Establece el texto del TextView al valor dado por el juego
    public void actualizaIntentos(){
        _tvIntentosLabel.setText(String.valueOf(_bmGame.getNumIntentos()));
    }

    //Establece la visibilidad de los botones cuyo ID está almacenado en _arrlIDBotones
    public void setVisibilidadCampos(int visibility){
        for(int i = 0; i< _arrlIDBotones.size(); i++){
            ((Button)(findViewById(_arrlIDBotones.get(i)))).setVisibility(visibility);
        }
    }

    public void limpiaTextoBotones(){
        for(int i = 0; i< _arrlIDBotones.size(); i++){
            ((Button)(findViewById(_arrlIDBotones.get(i)))).setText("");
        }
    }

    //Controla que se introduce un Nº de intentos válido y lo asigna
    public void procesaInputIntentos(String strNumero){
        Boolean isNumber = false;
        int intNumero;

        if(strNumero!=""){
            for(int i=0;i<strNumero.length();i++){
                if(Character.isDigit(strNumero.charAt(i))){
                    isNumber = true;
                }
            }

            if(isNumber){
                //Si se introduce un Nº de intentos superior a 10, notifica error
                if ((intNumero = Integer.parseInt(strNumero))> 9){
                    _tvTextResultado.setText(R.string.error_intentos);
                }else{
                    _bmGame.setIntentos(Integer.parseInt(strNumero));
                    _bmGame.reiniciar();

                    reiniciarPantalla();
                }
            }
        }

    }

}
