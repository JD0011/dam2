/**
 * Parte interna del juego de buscaminas. Se ha evitado el uso de botones en la parte interna, asi se
 * logra la portabilidad y reutilización del código en otros entornos.
 *
 * El objetivo del juego es encontrar la mina. El jugador tendrá un número concreto de intentos.
 * Pierde cuando se queda sin intentos.
 *
 * @author Juan de Dios Delgado Bermúdez
 * @since 10/10/2017
 */
package com.example.juand.buscaminas;

import android.widget.Button;

import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Random;

public class BMGame {

    //VARS

    private ArrayList<Integer> _arlIDs;
    private int[] _intIDsMinadas;
    //Cantidad de intentos establecidos inicialmente (necesario para reiniciar partida)
    private int _intNumIntentosInicial;
    //Intentos actuales
    private int _intNumIntentos;

    private int _intCantidadMinas;

    //CONSTRUCTOR

    //Se pasa por parámetros las ID's de los objetos, los intentos disponibles y las minas
    public BMGame(ArrayList<Integer> arlIDS, int intNumIntentos, int intCantidadMinas){

        //Almacena el valor inicial de los intentos establecidos
        _intNumIntentosInicial = intNumIntentos;
        _intNumIntentos = intNumIntentos;
        _intCantidadMinas = intCantidadMinas;
        _intIDsMinadas = new int[intCantidadMinas];

        _arlIDs = arlIDS;

    }

    //GETTERS

    public int getNumIntentos() {
        return _intNumIntentos;
    }

    //SETTERS
    public void setIntentos(int intNumIntentos){
        _intNumIntentosInicial = intNumIntentos;
        _intNumIntentos = intNumIntentos;
    }

    //METHODS

    //Genera la cantidad de minas especificadas
    public void generaMinas(){
        for(int i=0;i<_intIDsMinadas.length;i++){
            int index = new Random().nextInt(_arlIDs.size());
            _intIDsMinadas[i] = _arlIDs.get(index);
        }
    }

    public void eliminaMinas(){
        for(int i=0;i<_intIDsMinadas.length;i++){
            _intIDsMinadas[i] = 0;
        }
    }

    /*Se pasa el identificador de elemento por parámetros, de esta manera, se puede usar en otros
    elementos, ya sean botones, layouts, o incluso RadioButtons (siempre que posean un campo ID)
     */
    public boolean estaMinado(int ID){
        for(int i = 0; i < _intIDsMinadas.length; i++){
            if(_intIDsMinadas[i] == ID){
                return true;
            }
        }
        //Si no encuentra la mina, resta intento
        _intNumIntentos--;
        return false;
    }

    /**
     * Reinicia las minas y restablece el número de intentos
     */
    public void reiniciar(){
        eliminaMinas();
        generaMinas();
        _intNumIntentos = _intNumIntentosInicial;
    }



    public boolean quedanIntentos(){
        if(_intNumIntentos == 0){
            return false;
        }
        return true;
    }
}
