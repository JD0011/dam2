/**
 * Prueba: PRUEBA INICIAL
 * Clase: Dos
 * Mediante las correspondientes operaciones con caracteres, determina si una palabra es palindroma
 * o no, y lo notifica mediante un TextView.
 * Def. palindromo: se lee igual adelante que hacia atrás.
 *
 * @author Juan de Dios Delgado Bermúdez
 * @since 31/10/2017
 */

package com.example.pruebastring;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.juand.pruebainicial.R;

public class Dos extends Activity {

	TextView tv;
	String texto;
	Bundle extras;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dos);
		tv = (TextView) findViewById(R.id.textView1);

		extras = getIntent().getExtras();

		texto = extras.getString("texto");

		//Establece respuesta
		tv.setText(esPalindroma(texto)? R.string.si_palindroma : R.string.no_palindroma);

	}

	private boolean esPalindroma(String texto) {
		boolean palindroma;
		String invertida;

		palindroma = false;
		invertida="";
		for (int i = texto.length()-1; i >= 0; i--) {
			invertida+=texto.toCharArray()[i];
		}

		if(texto.equals(invertida)){
			palindroma=true;
		}
		
		return palindroma;
	}

	public void finalizar(View view) {
		finish();
	}
}
