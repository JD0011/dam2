/**
 * Prueba: PRUEBA INICIAL
 * Clase: Uno
 * Esta clase muestra la posición de la clave, la cuál es un fragmento de texto, dentro de texto.
 * En caso de que no existiera, se mostraría la correspondiente notificación al usuario.
 *
 * @author Juan de Dios Delgado Bermúdez
 * @since 31/10/2017
 */

package com.example.pruebastring;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.juand.pruebainicial.R;

public class Uno extends Activity {

	TextView tv;
	String texto, clave;
	int posicion;
	Bundle extras;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.uno);

		tv = (TextView) findViewById(R.id.textView1);

		extras = getIntent().getExtras();
		texto = extras.getString("texto");
		clave = extras.getString("clave");

		//Determina la posición de la clave en el texto (devuelve -1 si no existe)
		posicion = texto.indexOf(clave);

		if(posicion!=-1){
			tv.setText(R.string.esta_en_posicion+ String.valueOf(posicion));
		}else{
			tv.setText(R.string.no_encontrado);
		}
	}

	public void finalizar(View view) {
		finish();
	}

}
