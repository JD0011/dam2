/**
 * Prueba: PRUEBA INICIAL
 * Clase: Tres
 * Permite determinar si una dirección de correo electrónica es correcta en función de si posee
 * el símbolo de la arroba (@).
 *
 * @author Juan de Dios Delgado Bermúdez
 * @since 31/10/2017
 */

package com.example.pruebastring;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.juand.pruebainicial.R;

public class Tres extends Activity {

	Bundle b;
	String texto;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.tres);
		TextView tv = (TextView) findViewById(R.id.textView1);

		b = getIntent().getExtras();
		texto = b.getString("texto");

		if (comprobarArroba(texto)) {
			tv.setText(R.string.mail_correcto);
		} else {
			tv.setText(R.string.mail_incorrecto);
		}

	}

	private boolean comprobarArroba(String texto) {
		boolean tieneArroba;

		tieneArroba = false;
		for(char c: texto.toCharArray()){
			if(c=='@'){
				tieneArroba = true;
			}
		}
		return tieneArroba;
	}

	public void finalizar(View view) {
		finish();
	}
}
