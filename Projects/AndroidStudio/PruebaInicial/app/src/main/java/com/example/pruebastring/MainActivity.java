/**
 * Prueba: PRUEBA INICIAL
 * Clase: MainActivity
 * Gestiona la activity principal. Nos brinda un puente con las demás activities, además de
 * tranportar los valores de texto y clave mediante un Bundle (paquete de datos extras).
 *
 * @author Juan de Dios Delgado Bermúdez
 * @since 31/10/2017
 */

package com.example.pruebastring;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.text.Layout;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.juand.pruebainicial.R;

import java.util.ArrayList;

public class MainActivity extends Activity {

    Bundle extras;
	EditText texto, clave;

    Button.OnClickListener ltEjercicio1 = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            launchActivity(Uno.class);
        }
    };

    Button.OnClickListener ltEjercicio2 = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            launchActivity(Dos.class);
        }
    };

    Button.OnClickListener ltEjercicio3 = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            launchActivity(Tres.class);
        }
    };

    Button.OnClickListener ltEjercicio4 = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            launchActivity(Cuatro.class);
        }
    };

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		texto = (EditText) findViewById(R.id.textView1);
		clave = (EditText) findViewById(R.id.textView2);

        extras = new Bundle();

        extras.putString("texto",texto.getText().toString());
        extras.putString("clave", clave.getText().toString());

	}

	public void launchActivity(Class classActivity){
		Intent i = new Intent(this,classActivity);
		i.putExtras(extras);
		startActivity(i);
	}

}
