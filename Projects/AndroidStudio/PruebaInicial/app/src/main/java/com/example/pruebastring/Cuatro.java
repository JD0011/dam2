/**
 * Prueba: PRUEBA INICIAL
 * Clase: Cuatro
 * Muestra distintos resultados de operaciones con cadenas de texto que se pueden realizar sobre
 * el tipo String. Como operaciones, se encuentran las siguientes:
 * 	· Extraer la mitad de una cadena.
 * 	· Invertir una cadena de texto.
 * 	· Devolver el último carácter de una cadena.
 * 	· Separar con guiones la cadena dada.
 * 	· Contar las vocales existentes.
 *
 * @author Juan de Dios Delgado Bermúdez
 * @since 31/10/2017
 */

package com.example.pruebastring;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.juand.pruebainicial.R;

import java.util.ArrayList;

public class Cuatro extends Activity {

	ArrayList<TextView> tvOpciones = new ArrayList<>();
	Bundle bundle;
	String texto;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.cuatro);

		//Recoge TextView's nombrados mediante textViewXX
		TextView aux;
		int counter;
		boolean exitCondition;

		counter = 0;
		exitCondition = false;
		for(int i=0;i<5;i++){
			tvOpciones.add((TextView)findViewById(getResources().getIdentifier("textView"+counter,"id",getPackageName())));
			counter++;
		}

		bundle = getIntent().getExtras();
		texto = bundle.getString("texto");

		tvOpciones.get(0).setText("La primera mitad es: " + devuelveMitad(texto));
		tvOpciones.get(1).setText("El ultimo caracter es: " + ultimoCaracter(texto));
		tvOpciones.get(2).setText("Inversa es: " + invertirTexto(texto));
		tvOpciones.get(3).setText("Separado por guiones: " + separaConGuiones(texto));
		tvOpciones.get(4).setText("Cantidad de vocales: " + cuentaVocales(texto));
	}

	//Devuelve la mitad de un String
	private String devuelveMitad(String texto) {
		String mitad;

		//Comprueba si la longitud es par o impar
		if((texto.length()-1) % 2 ==0){
			mitad = texto.substring(0,texto.length()/2);
		//Suma +1 posición si es impar, para calcular la mitad relativa
		}else{
			mitad = texto.substring(0,(texto.length()+1)/2);
		}
		return mitad;
	}

	private char ultimoCaracter(String texto) {
		return texto.charAt(texto.length()-1);
	}

	private String invertirTexto(String texto) {
		String invertido;

		invertido="";
		for (int i = texto.length() - 1; i >= 0; i--) {
			invertido += texto.charAt(i);
		}
		return invertido;
	}

	private String separaConGuiones(String texto) {
		String separado;

		separado="";
		for (int i = 0; i < texto.length(); i++) {
			separado+= texto.charAt(i);
			//No imprimas guión si es el último caracter, si es un espacio o si el siguiente es un espacio
			if (i != texto.length() - 1 && texto.toCharArray()[i]!=' ' &&texto.toCharArray()[i+1]!=' ') {
				separado+="-";
			}
		}
		return separado;
	}

	private int cuentaVocales(String texto) {
		int vocales;

		texto = texto.toLowerCase();
		vocales = 0;
		for(char c: texto.toCharArray()){
			if(c=='a' || c=='e' || c=='i'|| c=='o' || c=='u'){
				vocales++;
			}
		}

		return vocales;
	}

	public void finalizar(View view) {
		finish();
	}
}
