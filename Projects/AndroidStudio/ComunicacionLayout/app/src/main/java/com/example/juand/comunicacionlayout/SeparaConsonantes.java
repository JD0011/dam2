package com.example.juand.comunicacionlayout;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.w3c.dom.Text;

/**
 * Separa las consonantes de una cadena de texto
 *
 * @author Juan de Dios Delgado Bermúdez
 * @since 24/10/2017
 */
public class SeparaConsonantes extends AppCompatActivity {

    private TextView tvCadena, tvConsonantes;
    private String _strSoloConsonantes;
    private Button btnSeparar, btnVolver;;

    private Bundle extras;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_separa_consonantes);

        //Extraer controles
        tvCadena = (TextView) findViewById(R.id.cadena_label);
        tvConsonantes = (TextView) findViewById(R.id.consonantes_label);

        btnSeparar = (Button) findViewById(R.id.separar_btn);
        btnVolver = (Button) findViewById(R.id.volver_btn);

        //Recoger el valor de la cadena
        extras = getIntent().getExtras();
        tvCadena.setText(extras.getString("cadena"));

        //LISTENER'S
        btnSeparar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                separarConsonantes(extras.getString("cadena"));
            }
        });

        btnVolver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToMainActivity();
            }
        });
    }

    /**
     * Separa las consonantes y las amlacena en un String
     * @param cadena
     */
    public void separarConsonantes(String cadena){

        _strSoloConsonantes="";

        for(char c: cadena.toLowerCase().toCharArray()){
            if(c!=' '){
                if(!(c=='a' || c=='e' || c=='i' || c=='o' || c=='u')){
                    _strSoloConsonantes+=c;
                }
            }
        }

        tvConsonantes.setText(_strSoloConsonantes);
    }

    /**
     * Devuelve el control al layout principal
     */
    public void goToMainActivity(){
        //Finaliza este layout
        this.finish();

        //Inicia el layout principal
        Intent i = new Intent(SeparaConsonantes.this,Principal.class);
        //Devuelve la cadena
        i.putExtras(extras);
        startActivity(i);
    }
}
