package com.example.juand.comunicacionlayout;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * Separa las vocales de una cadena de texto
 *
 * @author Juan de Dios Delgado Bermúdez
 * @since 24/10/2017
 */
public class SeparaVocales extends AppCompatActivity {

    private TextView tvCadena, tvVocales;
    private String _strSoloVocales;
    private Button btnSeparar, btnVolver;

    private Bundle extras;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_separa_vocales);

        //Extraer contoles
        tvCadena = (TextView) findViewById(R.id.cadena_label);
        tvVocales = (TextView) findViewById(R.id.vocales_label);

        btnSeparar = (Button) findViewById(R.id.separar_btn);
        btnVolver = (Button) findViewById(R.id.volver_btn);

        //Recoger el valor de la cadena
        extras = getIntent().getExtras();
        tvCadena.setText(extras.getString("cadena"));

        //LISTENER'S
        btnSeparar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                separarVocales(extras.getString("cadena"));
            }
        });

        btnVolver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToMainActivity();
            }
        });

    }

    /**
     * Separa las vocales de la cadena especificada por parámetros y devuelve un String
     * formado por ellas
     * @param cadena
     */
    public void separarVocales(String cadena){

        _strSoloVocales="";
        //Si son vocales, añadelas
        for (char c: cadena.toLowerCase().toCharArray()){
            if(c=='a' || c=='e' || c=='i' || c=='o' || c=='u'){
                _strSoloVocales+=c;
            }
        }

        tvVocales.setText(_strSoloVocales);
    }

    /**
     * Devuelve el control al layout principal
     */
    public void goToMainActivity(){
        //Finaliza este layout
        this.finish();

        //Inicia el layout principal
        Intent i = new Intent(SeparaVocales.this,Principal.class);
        //Devuelve la cadena
        i.putExtras(extras);
        startActivity(i);
    }
}
