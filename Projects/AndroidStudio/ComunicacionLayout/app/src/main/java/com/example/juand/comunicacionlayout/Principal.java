
package com.example.juand.comunicacionlayout;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
/**
 * Esta aplicación ofrece tres funciones principales sobre una cadena: separar las vocales, separar
 * las consonantes y contar la longitud, consonantes y vocales que la conforman.
 *
 * @author Juan de Dios Delgado Bermúdez
 * @since 24/10/2017
 */
public class Principal extends AppCompatActivity {

    private EditText etCadenaPrincipal;
    private Button btnVocales, btnConsonantes, btnContar;

    private Bundle extras;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);

        //Extraer elementos
        etCadenaPrincipal = (EditText) findViewById(R.id.cadena_value);

        btnVocales= (Button) findViewById(R.id.vocales_btn);
        btnConsonantes = (Button) findViewById(R.id.consonantes_btn);
        btnContar = (Button) findViewById(R.id.contar_btn);

        //Comprobar si existe cadena
        compruebaExtras();

        //LISTENER'S
        btnVocales.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Almacenar cadena en el Bundle
                extras = new Bundle();

                extras.putString("cadena", etCadenaPrincipal.getText().toString());
                //Especifica clase del activity
                Intent i = new Intent(Principal.this,SeparaVocales.class);

                i.putExtras(extras);

                //Finalizar esta activity e iniciar la nueva
                finish();
                startActivity(i);
            }
        });

        btnConsonantes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Idem al anterior
                extras = new Bundle();
                extras.putString("cadena", etCadenaPrincipal.getText().toString());

                //Especifica clase del activity
                Intent i = new Intent(Principal.this,SeparaConsonantes.class);

                i.putExtras(extras);

                //Finalizar esta activity e iniciar la nueva
                finish();
                startActivity(i);
            }
        });

        btnContar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                extras = new Bundle();
                extras.putString("cadena", etCadenaPrincipal.getText().toString());

                //Especifica clase del activity
                Intent i = new Intent(Principal.this,ContarLetras.class);

                i.putExtras(extras);

                //Finalizar esta activity e iniciar la nueva
                finish();
                startActivity(i);
            }
        });
    }

    /**
     * Comprobará si existe una cadena de texto en los Extras al volver desde otra activity
     * secundaria. Esto permite no perder el valor de la cadena con la que se está trabajando.
     */
    public void compruebaExtras(){
        String cadena="";
        //Si hay extras
        if(getIntent().getExtras()!=null){
            //Intenta encontrar cadena
            cadena = getIntent().getExtras().getString("cadena");
        }

        etCadenaPrincipal.setText(cadena);
    }
}
