package com.example.juand.comunicacionlayout;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * Cuenta la longitud, consonantes y vocales de una cadena de texto.
 *
 * @author Juan de Dios Delgado Bermúdez
 * @since 24/10/2017
 */
public class ContarLetras extends AppCompatActivity {

    private TextView tvCadena, tvLetras, tvConsonantes, tvVocales;
    private int  intVocales, intConsonantes;
    private Button btnContar, btnVolver;

    private Bundle extras;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contar_letras);

        //Extraer controles
        tvCadena = (TextView) findViewById(R.id.cadena_label);
        tvLetras = (TextView) findViewById(R.id.letras_label);
        tvConsonantes = (TextView) findViewById(R.id.consonantes_label);
        tvVocales = (TextView) findViewById(R.id.vocales_label);

        btnContar = (Button) findViewById(R.id.contar_btn);
        btnVolver = (Button) findViewById(R.id.volver_btn);

        //Recoger el valor de la cadena
        extras = getIntent().getExtras();
        tvCadena.setText(extras.getString("cadena"));

        //LISTENER'S
        btnContar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                contarLetras(extras.getString("cadena"));
            }
        });

        btnVolver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToMainActivity();
            }
        });
    }

    /**
     * Cuenta la longitud de la cadena, las consonantes y las vocales que la componen.
     * @param cadena
     */
    public void contarLetras(String cadena){
        //Tamaño de la cadena
        tvLetras.setText(String.valueOf(tvCadena.length()));

        //Contabiliza vocales y consonantes
        for(char c: cadena.toLowerCase().toCharArray()){
            if(c!=' '){
                if(c=='a' || c=='e' || c=='i' || c=='o' || c=='u'){
                    intVocales++;
                }else{
                    intConsonantes++;
                }
            }
        }

        tvVocales.setText(String.valueOf(intVocales));
        tvConsonantes.setText(String.valueOf(intConsonantes));
    }

    /**
     * Devuelve el control al layout principal
     */
    public void goToMainActivity(){
        //Finaliza este layout
        this.finish();

        //Inicia el layout principal
        Intent i = new Intent(ContarLetras.this,Principal.class);
        //Devuelve la cadena
        i.putExtras(extras);
        startActivity(i);
    }
}
