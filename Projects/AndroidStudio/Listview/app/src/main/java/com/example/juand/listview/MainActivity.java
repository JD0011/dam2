package com.example.juand.listview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private Button btnAgregar;
    private ListView lvPersonas;
    private EditText etNombre, etApellidos;

    private ArrayList<Persona> personas;
    private MyArrayAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnAgregar = (Button) findViewById(R.id.agregar_btn);
        etNombre = (EditText) findViewById(R.id.nombre_et);
        etApellidos = (EditText) findViewById(R.id.apellido_et);

        lvPersonas = (ListView) findViewById(R.id.lista_inicial);
        personas = new ArrayList<Persona>();
        adapter = new MyArrayAdapter(this,personas);
        lvPersonas.setAdapter(adapter);



        btnAgregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nombre, apellido;
                //Si se ha escrito nombre y apellido, añadelo
                if(!(nombre=etNombre.getText().toString()).equals("") && !(apellido=etApellidos.getText().toString()).equals("")){
                    Persona aux = new Persona(nombre, apellido);
                    agregar(aux);
                }
            }
        });
    }

    public void agregar(Persona per) {
        personas.add(per);
        adapter.notifyDataSetChanged();
        etNombre.setText("");
        etApellidos.setText("");
    }
}
