package com.example.juand.listview;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by juand on 28/11/2017.
 */

public class ProvinciasArrayAdapter extends ArrayAdapter {

    Activity context;
    ArrayList<Provincia> provincias;

    public ProvinciasArrayAdapter(Activity context, ArrayList<Provincia> provincias){
        super(context, R.layout.lista_items_provincia_cp,provincias);
        this.context = context;
        this.provincias = provincias;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View item = inflater.inflate(R.layout.lista_items_provincia_cp, null);

        TextView provincia = (TextView)item.findViewById(R.id.provincia_label);
        provincia.setText(provincias.get(position).getNombre());

        TextView codigo =(TextView)item.findViewById(R.id.cp_label);
        codigo.setText(String.valueOf(provincias.get(position).getCodigo()));

        return(item);
    }
}
