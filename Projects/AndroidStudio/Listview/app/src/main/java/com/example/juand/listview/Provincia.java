package com.example.juand.listview;

/**
 * Created by juand on 28/11/2017.
 */

public class Provincia {

    private String nombre;
    private int codigo;

    public Provincia (String nombre, int codigo){
        this.nombre = nombre;
        this.codigo = codigo;
    }

    public String getNombre(){
        return nombre;
    }

    public int getCodigo(){
        return codigo;
    }
}
