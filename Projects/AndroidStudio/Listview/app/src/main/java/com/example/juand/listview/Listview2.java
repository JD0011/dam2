package com.example.juand.listview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

public class Listview2 extends AppCompatActivity {

    private ArrayList<Provincia> provinciasInicial, provinciasSecundaria;

    private ListView listaInicial, listaSecundaria;
    private ProvinciasArrayAdapter adapterInicial, adapterSecundario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listview2);

        listaInicial = (ListView)findViewById(R.id.lista_inicial);
        listaSecundaria= (ListView)findViewById(R.id.lista_secundaria);

        provinciasInicial = new ArrayList<Provincia>();
        provinciasSecundaria = new ArrayList<Provincia>();


        adapterInicial = new ProvinciasArrayAdapter(this, provinciasInicial);

        provinciasInicial.add(new Provincia("Málaga",29000));
        provinciasInicial.add(new Provincia("Sevilla",28000));
        provinciasInicial.add(new Provincia("Córdoba",27000));
        provinciasInicial.add(new Provincia("Jaén",26000));
        provinciasInicial.add(new Provincia("Cádiz",25000));
        provinciasInicial.add(new Provincia("Granada",24000));
        provinciasInicial.add(new Provincia("Huelva",23000));

        adapterInicial.notifyDataSetChanged();

        adapterSecundario = new ProvinciasArrayAdapter(this, provinciasSecundaria);

        listaInicial.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //Agrega esta provincia a la lista secundaria
                agregarASecundaria(provinciasInicial.get(position));
            }
        });

        listaSecundaria.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                borraDeSecundaria(provinciasSecundaria.get(position));
            }
        });

        listaInicial.setAdapter(adapterInicial);
        listaSecundaria.setAdapter(adapterSecundario);

    }

    //Agrega provincia a la lista secundaria
    public void agregarASecundaria(Provincia prov){
        provinciasSecundaria.add(prov);
        adapterSecundario.notifyDataSetChanged();
    }

    //Borra la provincia de la lista secundaria
    public void borraDeSecundaria(Provincia prov){
        provinciasSecundaria.remove(prov);
        adapterSecundario.notifyDataSetChanged();
    }
}
