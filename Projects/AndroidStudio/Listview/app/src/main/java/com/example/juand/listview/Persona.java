package com.example.juand.listview;

/**
 * Created by juand on 28/11/2017.
 */

public class Persona {

    private String nombre, apellido;

    public Persona(String nombre, String apellido){
        this.nombre = nombre;
        this.apellido = apellido;
    }

    public String getNombre(){
        return nombre;
    }

    public String getApellido(){
        return apellido;
    }
}
