package com.example.juand.listview;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by juand on 28/11/2017.
 */

public class MyArrayAdapter extends ArrayAdapter {

    Activity context;
    ArrayList<Persona> personas;

    public MyArrayAdapter(Activity context, ArrayList<Persona> personas){
        super(context, R.layout.lista_items_nombre_apellido,personas);
        this.context = context;
        this.personas = personas;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View item = inflater.inflate(R.layout.lista_items_nombre_apellido, null);

        TextView nombre = (TextView)item.findViewById(R.id.nombre_label);
        nombre.setText(personas.get(position).getNombre());

        TextView apellido =(TextView)item.findViewById(R.id.apellido_label);
        apellido.setText(personas.get(position).getApellido());

        return(item);
    }
}
