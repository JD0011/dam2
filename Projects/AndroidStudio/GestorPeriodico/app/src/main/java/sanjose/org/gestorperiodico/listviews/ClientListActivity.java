package sanjose.org.gestorperiodico.listviews;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;

import sanjose.org.gestorperiodico.BBDD.SQLiteUserManager;
import sanjose.org.gestorperiodico.R;
import sanjose.org.gestorperiodico.activity.admin.ClientManagerActivity;
import sanjose.org.gestorperiodico.activity.user.UserMainActivity;
import sanjose.org.gestorperiodico.datamodels.User;

/**
 * Clase que muestra la lista de usuarios registrados en el sistema
 */
public class ClientListActivity extends AppCompatActivity {

    private ArrayList<User> users;
    private ListView lv_clients;
    private ClientListArrayAdapter clvAdapter;

    private Button btnReturn;

    //Herramientas de búsqueda
    private EditText etName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_client_list);

        //Lista todos los usuarios
        SQLiteUserManager sum = new SQLiteUserManager(this);
        users = sum.retrieveAllClients();

        //Inicializa herramientas de búsqueda
        etName = (EditText) findViewById(R.id.et_name);

        //Inicializa elementos de la lista
        clvAdapter = new ClientListArrayAdapter(this, users);
        lv_clients = (ListView) findViewById(R.id.lv_clients);

        //Botones
        btnReturn = (Button) findViewById(R.id.btn_return);

        lv_clients.setAdapter(clvAdapter);

        lv_clients.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                openClientManager(users.get(i));
            }
        });

        //Obtén todos los usuarios hasta el momento
        final ArrayList<User> allUsers = sum.retrieveAllClients();

        //Actualiza dinámicamente la lista de usuarios a mostrar
        etName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(!etName.getText().equals("")){
                    ArrayList<User> filteredUsers = new ArrayList<User>();
                    for(User u : allUsers){
                        String fullName = u.getName()+" "+u.getLastname();
                        System.out.println("FULLNAME: "+fullName);
                        System.out.println("CHARSEQUENCE: "+charSequence);
                        System.out.println(fullName.contains(charSequence));
                        if(fullName.contains(charSequence)){
                            filteredUsers.add(u);
                        }
                    }

                    //Actualiza
                    users.clear();
                    users.addAll(filteredUsers);
                    clvAdapter.notifyDataSetChanged();
                }else{
                    //Vuelve a mostrar todos los usuarios
                    users.clear();
                    users.addAll(allUsers);
                    clvAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        btnReturn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }

    /**
     * Actualiza lista de clientes, por si se eliminó alguno
     */
    @Override
    protected void onRestart() {
        super.onRestart();
        refreshClientList();
    }

    /**
     * Actualiza la lista de clientes
     */
    public void refreshClientList(){
        SQLiteUserManager sum = new SQLiteUserManager(this);
        users.clear();
        users.addAll(sum.retrieveAllClients());
        clvAdapter.notifyDataSetChanged();

    }

    /**
     * Inicializa el administrador de cliente, desde el que se puede consultar datos y eliminar
     * @param user Cliente a editar
     */
    public void openClientManager(User user){
        Intent i = new Intent(this, ClientManagerActivity.class);
        i.putExtra("user", user);
        startActivity(i);
    }
}
