package sanjose.org.gestorperiodico.datamodels;

import org.joda.time.LocalDate;

/**
 * Representa una Fee, la cual, el cliente debe pagar
 * Created by juand on 17/01/2018.
 */

public class Fee {

    /**
     * Identificador de la factura
     */
    private int feeID;

    /**
     * Usuario al que pertenece la factura
     */
    private int suscription_id;

    /**
     * Indicador de si está pagada la factura
     */
    private boolean isPayed;

    /**
     * Precio de la factura
     */
    private double feePrice;

    /**
     * Fecha de pago límite
     */
    private LocalDate paymentDate;

    public Fee(int feeID, int suscription_id, double feePrice,LocalDate paymentDate, boolean isPayed) {
        this.feeID = feeID;
        this.suscription_id = suscription_id;
        this.feePrice = feePrice;
        this.isPayed = isPayed;
        this.paymentDate = paymentDate;
    }

    public double getFeePrice() {
        return feePrice;
    }

    public int getFeeID() {
        return feeID;
    }

    public int getSuscription_id() {
        return suscription_id;
    }

    public boolean isPayed() {
        return isPayed;
    }

    public LocalDate getPaymentDate() {
        return paymentDate;
    }
}