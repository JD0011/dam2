package sanjose.org.gestorperiodico.activity.admin;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;

import sanjose.org.gestorperiodico.BBDD.SQLiteUserManager;
import sanjose.org.gestorperiodico.listviews.DefaulterListActivity;
import sanjose.org.gestorperiodico.activity.user.ModifyUserActivity;
import sanjose.org.gestorperiodico.R;
import sanjose.org.gestorperiodico.listviews.ClientListActivity;
import sanjose.org.gestorperiodico.activity.main.RegisterActivity;
import sanjose.org.gestorperiodico.datamodels.User;

/**
 * Ventana principal para el usuario Administrador. Muestra todos los controles necesarios
 * para la administración de los usuarios
 */

public class AdminMainActivity extends AppCompatActivity {

    private ArrayList<Button> buttons;
    private ArrayList<User> defaulters;
    private User adminUser;

    private View.OnClickListener buttonsListener;

    private Thread tRefresher;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_main);

        //Se encarga de actualizar la interfaz con los datos almacenados si se hacen cambios
        tRefresher = new Thread(new Runnable() {
            @Override
            public void run() {
                SQLiteUserManager sum = new SQLiteUserManager(AdminMainActivity.this);
                User refreshedUser;
                while(true){
                    try {
                        Thread.sleep(2500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    synchronized (sum){
                        refreshedUser = sum.getClientByUsername(adminUser.getUsername());
                    }

                    //Si no son iguales, actualizalo
                    if(!adminUser.equals(refreshedUser)){
                        adminUser = refreshedUser;
                    }
                }
            }
        });

        tRefresher.setName("ADMIN ACCOUNT REFRESHER");
        tRefresher.start();

        Bundle extras= getIntent().getExtras();

        adminUser = (User) extras.getSerializable("admin");

        buttons = new ArrayList<Button>();

        //Botones de cliente
        buttons.add((Button) findViewById(R.id.btn_register_user));
        buttons.add((Button) findViewById(R.id.btn_list_clients));

        //Botones de factura
        buttons.add((Button) findViewById(R.id.btn_fee_list));
        buttons.add((Button) findViewById(R.id.btn_defaulter_list));

        //Botón de modificación de cuenta
        buttons.add((Button) findViewById(R.id.btn_manage_admin_account));

        buttonsListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (view.getId()){
                    case R.id.btn_register_user:
                        launchActivity(RegisterActivity.class);
                        break;
                    case R.id.btn_list_clients:
                        launchActivity(ClientListActivity.class);
                        break;
                    case R.id.btn_manage_admin_account:
                        launchActivity(ModifyUserActivity.class, adminUser);
                        break;
                    case R.id.btn_defaulter_list:
                        launchActivity(DefaulterListActivity.class);
                        break;
                }
            }
        };

        for(Button b: buttons){
            b.setOnClickListener(buttonsListener);
        }
    }

    /**
     * Lanza Actividad especificada
     * @param clazz Actividad a lanzar
     */
    private void launchActivity(Class clazz){
        Intent i = new Intent(this,clazz);
        i.putExtra("isAdmin", true);
        startActivity(i);
    }

    /**
     * Lanza Actividad especificada
     * @param clazz Actividad a lanzar
     * @param user Usuario como extra
     */
    private void launchActivity(Class clazz, User user){
        Intent i = new Intent(this,clazz);
        i.putExtra("isAdmin", true);
        i.putExtra("user", user);
        startActivity(i);
    }

    //CONFIRMACIÓN PARA SALIR
    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setMessage(R.string.msg_are_you_sure_to_close)
                .setNegativeButton(R.string.lbl_no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                })
                .setPositiveButton(R.string.lbl_yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        System.exit(0);
                    }
                }).show();
    }
}
