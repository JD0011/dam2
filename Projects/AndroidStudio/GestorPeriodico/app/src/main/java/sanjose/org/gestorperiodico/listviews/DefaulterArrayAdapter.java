package sanjose.org.gestorperiodico.listviews;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import sanjose.org.gestorperiodico.R;
import sanjose.org.gestorperiodico.datamodels.User;

/**
 * ArrayAdapter para la lista de morosos
 */

public class DefaulterArrayAdapter extends ArrayAdapter{

    private ArrayList<User> defaulters;
    private Activity ctx;

    public DefaulterArrayAdapter(Activity ctx, ArrayList<User> defaulters){
        super(ctx, R.layout.listview_defaulter_template);
        this.ctx = ctx;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = ctx.getLayoutInflater();
        View item = inflater.inflate(R.layout.listview_defaulter_template, null);

        //Selected user
        User user = defaulters.get(position);
        TextView name = (TextView) item.findViewById(R.id.lbl_username);
        name.setText(user.getName()+" "+ user.getLastname());

        return item;
    }
}
