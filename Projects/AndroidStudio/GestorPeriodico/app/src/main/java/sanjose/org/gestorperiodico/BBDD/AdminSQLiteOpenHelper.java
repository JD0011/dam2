package sanjose.org.gestorperiodico.BBDD;

 import android.content.Context;
 import android.database.sqlite.SQLiteDatabase;
 import android.database.sqlite.SQLiteOpenHelper;

public class AdminSQLiteOpenHelper extends SQLiteOpenHelper {

    //Nombres de las tablas
    public static final String USER_TABLE_NAME="Users";
    public static final String FEE_TABLE_NAME="Fees";
    public static final String SUSCRIPTION_TABLE_NAME = "Suscriptions";
    public static final String SUSCRIPTION_PRICE_TABLE_NAME = "Suscriptions_type";

    public static final String[] USERS_COLUMNS=
            {"user_id","username","password","name","lastname","address","postal_code","type_user","user_suscription"};

    /**
     * Tabla de los usuarios
     */
    private final String CREATE_USERS_TABLE =
            "CREATE TABLE Users (" +
                    "user_id integer primary key," +
                    "username text not null ," +
                    "password text not null," +
                    "name text not null, " +
                    "lastname text not null, " +
                    "address text not null," +
                    "postal_code integer," +
                    "type_user text not null," +
                    "user_suscription integer, " +

                    "FOREIGN KEY(user_suscription) REFERENCES suscriptions(suscription_id));";

    /**
     * Columnas de suscripción
     */
    public static final String[] SUSCRIPTIONS_COLUMNS=
            {"suscription_id","choosen_days","payment_date","delivered_newspapers","antiquity"};

    /**
     * Tabla de las suscripciones
     */
    private final String CREATE_SUSCRIPTIONS_TABLE =
            "CREATE TABLE Suscriptions(" +
                    "suscription_id integer primary key autoincrement," +
                    "choosen_days text," +
                    "payment_date text," +
                    "delivered_newspapers integer default 0," +
                    "antiquity integer default 0);";

    public static final String[] SUSCRIPTIONS_PRICE_COLUMNS =
            {"price_type","price_per_day"};

    /**
     * Tabla del precio de la suscripción
     */
    private final String CREATE_SUSCRIPTION_PRICE =
            "CREATE TABLE Suscriptions_type(" +
                    "price_type integer primary key not null,"+
                    "price_per_day integer not null);";

    public static final String[] FEE_COLUMNS =
            {"fee_id","suscription_id","fee_price","payment_date","is_payed"};

    /**
     * Tabla de facturas
     */
    private final String CREATE_FEE_TABLE =
            "CREATE TABLE Fees(" +
                    "fee_id integer primary key autoincrement," +
                    "suscription_id text not null," +
                    "fee_price integer not null," +
                    "payment_date text not null," +
                    "is_payed integer default 0," +

                    "FOREIGN KEY(suscription_id) REFERENCES Suscriptions(suscription_id));";
    /**
     * Borra las tablas si existen
     */
    private final String DROP_TABLES_IF_EXISTS =
            "DROP IF EXISTS Suscriptions;"+
            "DROP IF EXISTS Users;" +
            "DROP IF EXISTS Suscription_price;" +
            "DROP IF EXISTS Fees";

    public AdminSQLiteOpenHelper(Context context, String nombre, SQLiteDatabase.CursorFactory factory, int version) {

        super(context, nombre, factory, version);

    }

    @Override

    public void onCreate(SQLiteDatabase db) {

        db.execSQL(CREATE_SUSCRIPTIONS_TABLE);
        db.execSQL(CREATE_USERS_TABLE);
        db.execSQL(CREATE_SUSCRIPTION_PRICE);
        db.execSQL(CREATE_FEE_TABLE);

    }

    @Override

    public void onUpgrade(SQLiteDatabase db, int version1, int version2) {
        db.execSQL(DROP_TABLES_IF_EXISTS);
        db.execSQL(CREATE_USERS_TABLE);
        onCreate(db);
    }

}