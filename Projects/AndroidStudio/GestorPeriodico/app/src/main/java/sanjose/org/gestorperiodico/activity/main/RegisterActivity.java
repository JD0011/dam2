package sanjose.org.gestorperiodico.activity.main;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.util.ArrayList;
import java.util.HashMap;

import sanjose.org.gestorperiodico.BBDD.SQLiteUserManager;
import sanjose.org.gestorperiodico.R;
import sanjose.org.gestorperiodico.datamodels.User;

/**
 * Esta clase permite registrar a un nuevo usuario, ya sea un administrador o un cliente. Además,
 * también se utiliza para modificar los datos de un usuario. Esto se consigue comprobando
 * si tiene un extra adjuntado con un objeto User.
 */
public class RegisterActivity extends AppCompatActivity {

    private ArrayList<Boolean> fieldsAreOK;
    private HashMap<String,Button> buttons;
    private View.OnClickListener buttonsListener;
    private HashMap<String,EditText> fields;

    private boolean checkAdminToggleButton = false;
    private LinearLayout llAdmin_Toggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        //Campos
        fieldsAreOK = new ArrayList<Boolean>();
        fields = new HashMap<String,EditText>();
        fields.put("name",(EditText)findViewById(R.id.et_name));
        fields.put("lastname",(EditText)findViewById(R.id.et_lastname));
        fields.put("address",(EditText)findViewById(R.id.et_address));
        fields.put("postal_code",(EditText)findViewById(R.id.et_postal_code));
        fields.put("username",(EditText)findViewById(R.id.et_name));
        fields.put("password",(EditText)findViewById(R.id.et_password));
        fields.put("password2",(EditText)findViewById(R.id.et_repeat_password));

        //Botones
        buttons = new HashMap<String, Button>();
        buttons.put("finish",(Button) findViewById(R.id.btn_finish));
        buttons.put("return",(Button) findViewById(R.id.btn_return));

        //Layout de botón creador de administradores
        llAdmin_Toggle = (LinearLayout) findViewById(R.id.ll_admin_toggle);

        //COMPRUEBA SI LO HA LLAMADO UN ADMINISTRADOR
        Bundle extras = getIntent().getExtras();

        if(extras!=null && extras.containsKey("isAdmin")){
            boolean isAdmin = getIntent().getExtras().getBoolean("isAdmin");
            //Activa botón para crear usuario administrador
            if(isAdmin){
                llAdmin_Toggle.setVisibility(View.VISIBLE);
                buttons.put("toggle_admin",(ToggleButton) findViewById(R.id.btn_set_admin));
                checkAdminToggleButton = true;
            }
        }

        //LISTENNER
        buttonsListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (view.getId()){
                    case R.id.btn_finish:
                        //Si los datos están OK, registra al usuario
                        if(!checkData()){
                            Toast.makeText(RegisterActivity.this, R.string.error_fields, Toast.LENGTH_SHORT).show();

                        }else{

                            SQLiteUserManager sum = new SQLiteUserManager(RegisterActivity.this);
                            if(sum.existsClientname(fields.get("username").getText().toString())){
                                Toast.makeText(RegisterActivity.this, R.string.error_already_registered, Toast.LENGTH_SHORT).show();

                            }else{

                                String typeUser = User.CLIENT;
                                //Comprueba si debe crear administrador.
                                if(checkAdminToggleButton){
                                    if(((ToggleButton)buttons.get("toggle_admin")).isChecked()){
                                        typeUser = User.ADMIN;
                                    }
                                }

                                boolean gotRegistered = sum.addUser(
                                        new User(
                                                sum.getNextFreeUserID(),
                                                fields.get("username").getText().toString(),
                                                fields.get("password").getText().toString(),
                                                fields.get("name").getText().toString(),
                                                fields.get("lastname").getText().toString(),
                                                fields.get("address").getText().toString(),
                                                Integer.parseInt(fields.get("postal_code").getText().toString()),
                                                typeUser
                                        ));

                                if(gotRegistered){
                                    Toast.makeText(RegisterActivity.this, R.string.msg_user_stored_ok, Toast.LENGTH_SHORT).show();
                                    //Cierra esta ventana
                                    finish();
                                }else{
                                    Toast.makeText(RegisterActivity.this, R.string.msg_something_went_wrong, Toast.LENGTH_SHORT).show();
                                }
                            }
                        }
                        break;

                    case R.id.btn_return:
                        //Cierra esta ventana
                        finish();
                        break;
                }
            }
        };

        for(Button b: buttons.values()){
            b.setOnClickListener(buttonsListener);
        }

        assignFocusListenerToFields();
    }

    /**
     * Comprueba si los datos introducidos son correctos
     * @return True si los datos del usuario son correcto, False de lo contrario
     */
    public boolean checkData(){
        return checkUsername() && checkName() && checkLastname() && checkAddress() && checkPostalCode() && checkPassword() && checkRepeatedPassword();
    }

    private boolean checkUsername(){
        boolean isOkey = false;
        String username = fields.get("username").getText().toString();
        if(!username.equals("")){
            if (username.length() < 3 || username.length() > 10) {
                fields.get("username").setError(getResources().getString(R.string.error_username_size));
            }else{
                fields.get("username").setError(null);
                isOkey = true;
            }
        }

        return isOkey;
    }

    private boolean checkName(){
        boolean isOkey = false;

        String name = fields.get("name").getText().toString();
        if(!name.equals("")){
            if (name.length() < 3 || name.length() > 20) {
                fields.get("name").setError(getResources().getString(R.string.error_name));
            }else{
                fields.get("name").setError(null);
                isOkey = true;
            }
        }

        return isOkey;
    }

    private boolean checkLastname(){
        boolean isOkey = false;

        String lastname = fields.get("lastname").getText().toString();
        if(!lastname.equals("")){
            if (lastname.length() < 5 || lastname.length() > 25) {
                fields.get("lastname").setError(getResources().getString(R.string.error_lastname));
            }else{
                fields.get("lastname").setError(null);
                isOkey = true;
            }
        }

        return isOkey;
    }

    private boolean checkAddress(){
        boolean isOkey = false;

        String address = fields.get("address").getText().toString();
        if(!address.equals("")){
            if (address.length() < 5) {
                fields.get("address").setError(getResources().getString(R.string.error_address));
            } else {
                fields.get("address").setError(null);
                isOkey = true;
            }
        }

        return isOkey;
    }

    private boolean checkPostalCode(){
        boolean isOkey = false;

        String strPostalCode = fields.get("postal_code").getText().toString();

        int intPostalCode;
        try{
            intPostalCode = Integer.parseInt(strPostalCode);

            if(intPostalCode>9999){
                isOkey = true;
            }

        }catch(Exception e){
            e.printStackTrace();
        }

        return isOkey;
    }

    private boolean checkPassword(){
        boolean isOkey = false;

        String password = fields.get("password").getText().toString();

        if(!password.equals("")){
            if (password.length() > 8 && password.length() < 15) {
                boolean hasLetters = false, hasNumbers = false;
                for (char c : password.toCharArray()) {
                    if (Character.isLetter(c)) {
                        hasLetters = true;
                    }

                    if (Character.isDigit(c)) {
                        hasNumbers = true;
                    }
                }

                if (!(hasLetters && hasNumbers)) {
                    fields.get("password").setError(getResources().getString(R.string.error_password_bad));
                }else{
                    fields.get("password").setError(null);
                    isOkey = true;
                }

            }else{
                fields.get("password").setError(getResources().getString(R.string.error_password_length));
                isOkey =true;
            }
        }

        return isOkey;
    }

    private boolean checkRepeatedPassword(){
        boolean isOkey = false;

        String password = fields.get("password").getText().toString();
        String password2 = fields.get("password2").getText().toString();

        fields.get("password2").setError(null);

        if (!password.equals(password2)) {
            fields.get("password2").setError(getResources().getString(R.string.error_password_not_equal));
            fieldsAreOK.add(false);
        }else{
            fields.get("password2").setError(null);
            fieldsAreOK.clear();
        }

        return isOkey;
    }


    /**
     * Establece las reglas de validación de cada uno de los campos de entrada
     */
    private void assignFocusListenerToFields(){
        fields.get("username").setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                checkUsername();
            }
        });

        fields.get("name").setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                checkName();
            }
        });

        fields.get("lastname").setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                checkLastname();
            }
        });

        fields.get("address").setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                checkAddress();
            }
        });

        fields.get("postal_code").setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                checkPostalCode();
            }
        });

        fields.get("password").setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                checkPassword();
            }
        });

        fields.get("password2").setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                checkRepeatedPassword();
            }
        });
    }
}