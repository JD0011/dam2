package sanjose.org.gestorperiodico.activity.user;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;

import sanjose.org.gestorperiodico.BBDD.SQLiteUserManager;
import sanjose.org.gestorperiodico.R;
import sanjose.org.gestorperiodico.datamodels.User;

/**
 * Permite modificar los datos del usuario pasado mediante extras. Muestra la información
 * antigua y se actualiza si se cambian los datos
 */
public class ModifyUserActivity extends AppCompatActivity {

    private ArrayList<Button> buttons;
    private View.OnClickListener buttonsListener;
    private HashMap<String,TextView> labels;

    private Boolean wrongPassword;
    private User userToModify;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modify_user);

        //Cadenas de texto
        labels = new HashMap<String, TextView>();
        labels.put("username",(TextView) findViewById(R.id.lbl_username_value));
        labels.put("name",(TextView) findViewById(R.id.lbl_name_value));
        labels.put("lastname",(TextView) findViewById(R.id.lbl_lastname_value));
        labels.put("address",(TextView) findViewById(R.id.lbl_address_value));
        labels.put("postal_code",(TextView) findViewById(R.id.lbl_postal_code_value));

        //Botones
        buttons = new ArrayList<Button>();
        buttons.add((Button) findViewById(R.id.btn_return));
        buttons.add((Button) findViewById(R.id.btn_change_password));
        buttons.add((Button) findViewById(R.id.btn_change_name));
        buttons.add((Button) findViewById(R.id.btn_change_lastname));
        buttons.add((Button) findViewById(R.id.btn_change_address));
        buttons.add((Button) findViewById(R.id.btn_change_postal_code));

        //COMPRUEBA SI LO HA LLAMADO UN ADMINISTRADOR
        Bundle extras = getIntent().getExtras();

        userToModify = (User) getIntent().getSerializableExtra("user");

        //LISTENER
        buttonsListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean changeSomething = false;
                Button btnPushed = null;
                switch (view.getId()){

                    case R.id.btn_change_name:
                        changeSomething = true;
                        btnPushed = (Button) findViewById(R.id.btn_change_name);
                        break;

                    case R.id.btn_change_lastname:
                        changeSomething = true;
                        btnPushed = (Button) findViewById(R.id.btn_change_lastname);
                        break;

                    case R.id.btn_change_address:
                        changeSomething = true;
                        btnPushed = (Button) findViewById(R.id.btn_change_address);
                        break;

                    case R.id.btn_change_postal_code:
                        changeSomething = true;
                        btnPushed = (Button) findViewById(R.id.btn_change_postal_code);
                        break;

                    case R.id.btn_change_password:
                        final Context ctx = ModifyUserActivity.this;
                        LinearLayout layout = new LinearLayout(ctx);
                        layout.setOrientation(LinearLayout.VERTICAL);

                        final EditText password = new EditText(ctx);
                        password.setHint(R.string.current_password);
                        password.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                        layout.addView(password);

                        final EditText newpassword = new EditText(ctx);
                        newpassword.setHint(R.string.lbl_new_password);
                        newpassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                        layout.addView(newpassword);

                        final EditText repeatpassword = new EditText(ctx);
                        repeatpassword.setHint(R.string.repeat_password);
                        repeatpassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                        layout.addView(repeatpassword);

                        new AlertDialog.Builder(ctx)
                                .setTitle(R.string.change_password)
                                .setView(layout)
                                .setPositiveButton(R.string.lbl_ok, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {

                                        //COMPROBACIONES DE CONTRASEÑA
                                        if(!userToModify.getPassword().equals(password.getText().toString())){

                                            Toast.makeText(ctx,R.string.error_current_password_wrong,Toast.LENGTH_SHORT).show();
                                            dialogInterface.cancel();
                                        }else{

                                            String newPassword = newpassword.getText().toString();
                                            if(newPassword.length()<5 || newPassword.length()>15){
                                                Toast.makeText(ctx,R.string.error_password_bad,Toast.LENGTH_SHORT).show();
                                                dialogInterface.cancel();
                                            }else{

                                                if(!newPassword.equals(repeatpassword.getText().toString())){
                                                    Toast.makeText(ctx,R.string.error_password_not_equal,Toast.LENGTH_SHORT).show();
                                                    dialogInterface.cancel();
                                                }else{

                                                    SQLiteUserManager sum = new SQLiteUserManager(ctx);
                                                    if(sum.updatePasswordForUser(userToModify,newPassword)){
                                                        Toast.makeText(ctx,R.string.msg_password_change_ok,Toast.LENGTH_SHORT).show();
                                                    }else{
                                                        Toast.makeText(ctx,R.string.msg_something_went_wrong,Toast.LENGTH_SHORT).show();
                                                    }
                                                    dialogInterface.cancel();
                                                }
                                            }
                                        }
                                    }
                                }).show();
                        break;

                    case R.id.btn_return:
                        //Cierra esta ventana
                        finish();
                        break;
                }

                //Comprueba si debe actualizar datos
                final Button innerBtnPushed = btnPushed;
                if(changeSomething){
                    //Cuadro de entrada de datos
                    final EditText newValue = new EditText(ModifyUserActivity.this);
                    newValue.setOnFocusChangeListener(listenerFor(btnPushed));

                    new AlertDialog.Builder(ModifyUserActivity.this)
                            .setTitle(R.string.lbl_set_new_value)
                            .setView(newValue)
                            .setPositiveButton(R.string.lbl_ok, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    //Si acepta, establece nuevo valor
                                    SQLiteUserManager sum = new SQLiteUserManager(ModifyUserActivity.this);
                                    switch (innerBtnPushed.getId()){
                                        case R.id.btn_change_name:
                                            sum.updateNameForUser(userToModify,newValue.getText().toString());
                                            break;
                                        case R.id.btn_change_lastname:
                                            sum.updateLastnameForUser(userToModify,newValue.getText().toString());
                                            break;
                                        case R.id.btn_change_address:
                                            sum.updateAddressForUser(userToModify,newValue.getText().toString());
                                            break;
                                        case R.id.btn_change_postal_code:
                                            sum.updatePostalCodeForUser(userToModify,newValue.getText().toString());
                                            break;
                                    }

                                    queryUserData();
                                }
                            })
                            .setNegativeButton(R.string.lbl_cancel, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.cancel();
                                }
                            }).show();
                }
            }
        };

        for(Button b: buttons){
            b.setOnClickListener(buttonsListener);
        }

        queryUserData();
    }

    private View.OnFocusChangeListener listenerFor(Button btn){

        View.OnFocusChangeListener listener = null;
        switch (btn.getId()){
            case R.id.btn_change_name:
                listener = new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View view, boolean b) {
                        EditText edittext = (EditText) view;
                        if(!view.hasFocus()) {
                            String name = edittext.getText().toString();
                            if (!name.equals("")) {
                                if (name.length() < 3 || name.length() > 20) {
                                    edittext.setError(getResources().getString(R.string.error_name));
                                } else {
                                    edittext.setError(null);
                                }
                            }
                        }
                    }
                };
                break;

            case R.id.btn_change_lastname:
                listener = new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View view, boolean b) {
                        EditText edittext = (EditText) view;
                        if(!view.hasFocus()) {
                            String lastname = edittext.getText().toString();
                            if (!lastname.equals("")) {
                                if (lastname.length() < 5 || lastname.length() > 25) {
                                    edittext.setError(getResources().getString(R.string.error_lastname));
                                } else {
                                    edittext.setError(null);
                                }
                            }
                        }
                    }
                };
                break;

            case R.id.btn_change_address:
                listener = new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View view, boolean b) {
                        EditText edittext = (EditText) view;
                        if(!view.hasFocus()) {
                            String address = edittext.getText().toString();
                            if (!address.equals("")) {
                                if (address.length() < 5) {
                                    edittext.setError(getResources().getString(R.string.error_address));
                                } else {
                                    edittext.setError(null);
                                }
                            }
                        }
                    }
                };
                break;

            case R.id.btn_change_postal_code:
                listener = new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View view, boolean b) {
                        EditText edittext = (EditText) view;
                        String postalCode = edittext.getText().toString();
                        if (postalCode.length() == 5) {
                            for (char c : postalCode.toCharArray()) {
                                if (!Character.isDigit(c)) {
                                    edittext.setError(getResources().getString(R.string.error_postal_code));
                                }
                            }
                        } else {
                            edittext.setError(getResources().getString(R.string.error_postal_code));
                        }
                    }
                };
                break;
        }

        return listener;
    }

    /**
     * Muestra los datos guardados en la base de datos del usuario a modificar
     */
    private void queryUserData(){
        SQLiteUserManager sum = new SQLiteUserManager(ModifyUserActivity.this);
        userToModify = sum.getClientByID(userToModify.getUser_id());

        labels.get("username").setText(userToModify.getUsername());
        labels.get("name").setText(userToModify.getName());
        labels.get("lastname").setText(userToModify.getLastname());
        labels.get("address").setText(userToModify.getAddress());
        labels.get("postal_code").setText(String.valueOf(userToModify.getPostalCode()));
    }
}
