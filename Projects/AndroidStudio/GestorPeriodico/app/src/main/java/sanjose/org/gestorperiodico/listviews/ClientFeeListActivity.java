package sanjose.org.gestorperiodico.listviews;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import sanjose.org.gestorperiodico.BBDD.SQLiteUserManager;
import sanjose.org.gestorperiodico.R;
import sanjose.org.gestorperiodico.datamodels.Fee;
import sanjose.org.gestorperiodico.datamodels.User;

/**
 * Muestra la lista de facturas de un cliente determinado
 */
public class ClientFeeListActivity extends AppCompatActivity {

    private ListView lvFeeList;
    private ClientFeeArrayAdapter feesAdapter;

    private ArrayList<Fee> feeList;


    //Cliente del que se muestran los datos
    private User client;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_client_fee_list);

        feeList = new ArrayList<>();
        feesAdapter = new ClientFeeArrayAdapter(this,feeList);

        //Carga controles
        lvFeeList = (ListView) findViewById(R.id.lv_fee_list);
        lvFeeList.setAdapter(feesAdapter);

        //Consulta facturas del cliente
        Bundle extras = getIntent().getExtras();

        if(extras.containsKey("user")){
            client = (User)extras.getSerializable("user");
        }

        //LISTENER DE CLICK SOBRE FACTURA
        lvFeeList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, final int position, long l) {
                if(!feeList.get(position).isPayed()){
                    payFeeDialog(feeList.get(position));
                }
            }
        });

        queryFees();
    }

    /**
     * Muestra dialogo para pagar la factura especificada
     * @param fee Factura a pagar
     */
    private void payFeeDialog(final Fee fee){
        //Diálogo de confirmación
        new AlertDialog.Builder(ClientFeeListActivity.this)
                .setTitle(R.string.pay_fee)
                .setMessage(R.string.lbl_sure_want_to_pay)
                .setNegativeButton(R.string.lbl_no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                })
                .setPositiveButton(R.string.lbl_yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        SQLiteUserManager sum = new SQLiteUserManager(ClientFeeListActivity.this);
                        //Paga la factura
                        if(sum.payFee(fee)){
                            Toast.makeText(ClientFeeListActivity.this,R.string.lbl_fee_been_paid_ok,Toast.LENGTH_SHORT).show();
                            queryFees();
                        }else{
                            Toast.makeText(ClientFeeListActivity.this,R.string.lbl_fee_been_paid_wrong,Toast.LENGTH_SHORT).show();
                        }
                    }
                }).show();
    }

    /**
     * Carga las facturas desde la base de datos. Si el cliente no tiene facturas, se le notifica.
     * De lo contrario, se muestran todas las que tiene.
     */
    private void queryFees(){
        SQLiteUserManager sum = new SQLiteUserManager(this);

        ArrayList<Fee> temporalFeeList = sum.getFeesBySuscriptionID(client.getUserSuscriptionID());

        //Controla que haya facturas
        if(temporalFeeList.size()==0){
            feeList.clear();
            new AlertDialog.Builder(this)
                    .setTitle(R.string.lbl_without_fees)
                    .setMessage(R.string.lbl_no_fees_yet)
                    .setPositiveButton(R.string.lbl_ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            finish();
                        }
                    }).show();
        }else{
            //Fix para el error de que no actualiza el ListView
            feeList.clear();
            feeList.addAll(temporalFeeList);
            feesAdapter.notifyDataSetChanged();
        }
    }
}
