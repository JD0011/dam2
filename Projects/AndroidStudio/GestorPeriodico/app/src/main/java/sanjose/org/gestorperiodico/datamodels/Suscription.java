package sanjose.org.gestorperiodico.datamodels;

import android.content.Context;

import org.joda.time.LocalDate;

import java.util.ArrayList;

/**
 * Created by juand on 09/01/2018.
 */

public class Suscription {

    /**
     * Días elegidos por el cliente
     */
    private final ArrayList<Integer> choosenDays;

    /**
     * ID representativa de la suscripción
     */
    private int suscriptionId;

    /**
     * Número de periódicos recibidos
     */
    private int deliveredNewspapers;

    /**
     * Almacena el día de pago para las facturas
     */
    private LocalDate paymentDate;

    /**
     * Antiguedad de la suscripción
     */
    private int antiquity;

    /**
     * Contexto de la factura
     */
    private Context ctx;

    public Suscription(Context ctx, int suscriptionId, ArrayList<Integer> choosenDays, LocalDate paymentDate, int deliveredNewspapers, int antiquity) {
        this.ctx = ctx;
        this.suscriptionId = suscriptionId;
        this.choosenDays = choosenDays;
        this.paymentDate = paymentDate;
        this.deliveredNewspapers = deliveredNewspapers;
        this.antiquity = antiquity;
    }

    public int getSuscriptionID() {
        return suscriptionId;
    }

    public int getDeliveredNewspapers() {
        return deliveredNewspapers;
    }

    public ArrayList<Integer> getChoosenDays() {
        return choosenDays;
    }

    public int getAntiquity() {
        return antiquity;
    }

    public LocalDate getPaymentDate() {
        return paymentDate;
    }
}
