package sanjose.org.gestorperiodico.listviews;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import sanjose.org.gestorperiodico.BBDD.SQLiteUserManager;
import sanjose.org.gestorperiodico.R;
import sanjose.org.gestorperiodico.activity.user.UserMainActivity;
import sanjose.org.gestorperiodico.datamodels.User;
import sanjose.org.gestorperiodico.listviews.DefaulterArrayAdapter;

/**
 * Presenta a los usuarios morosos
 */
public class DefaulterListActivity extends AppCompatActivity {

    private ArrayList<User> defaulters;
    private ListView lvDefaulters;
    private DefaulterArrayAdapter daAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_defaulter_list);

        Bundle extras = getIntent().getExtras();

        defaulters  = new ArrayList<>();

        daAdapter = new DefaulterArrayAdapter(this, defaulters);
        lvDefaulters = (ListView) findViewById(R.id.lv_defaulters);
        lvDefaulters.setAdapter(daAdapter);

        checkDefaulters();
    }

    public void checkDefaulters(){
        SQLiteUserManager sum = new SQLiteUserManager(this);
        ArrayList<User> temporalDefaulters = sum.getDefaulters();

        if(temporalDefaulters.size()==0){
            new AlertDialog.Builder(DefaulterListActivity.this)
                    .setTitle(R.string.lbl_no_defaulters)
                    .setMessage(R.string.lbl_no_defaulters_long)
                    .setPositiveButton(R.string.lbl_ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            finish();
                        }
                    }).show();
        }else{
            //Fix para el error de que no actualiza el ListView
            defaulters.clear();
            defaulters.addAll(temporalDefaulters);
            daAdapter.notifyDataSetChanged();
        }
    }

    /**
     * Inicializa el editor de clientes
     * @param user Cliente a editar
     */
    public void openClientEditor(User user){
        Intent i = new Intent(this, UserMainActivity.class);
        i.putExtra("user", user);
        //Da permisos de administrador
        i.putExtra("asAdmin", true);
        startActivity(i);
    }
}
