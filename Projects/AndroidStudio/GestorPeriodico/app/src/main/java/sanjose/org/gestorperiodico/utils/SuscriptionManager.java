package sanjose.org.gestorperiodico.utils;

/**
 * Se encarga de actualizar los valores correspondientes a la suscripción del usuario
 */

import android.content.Context;

import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.joda.time.Months;

import sanjose.org.gestorperiodico.BBDD.SQLiteUserManager;
import sanjose.org.gestorperiodico.datamodels.Fee;
import sanjose.org.gestorperiodico.datamodels.Suscription;
import sanjose.org.gestorperiodico.datamodels.User;

/**
 * Se encarga de administrar los datos de la base de datos. Eso incluye la creación de las facturas,
 * la actualización de aquellas que se hayan pagado, y la actualización de las suscripciones
 */
public class SuscriptionManager {

    private User user;
    private Suscription userSuscription;
    private SQLiteUserManager sum;

    /**
     * Días de los que dispone el cliente para realizar el pago
     */
    private final int MAX_DAYS_FOR_PAYMENT = 5;

    public SuscriptionManager(Context ctx, User user, Suscription userSuscription){
        this.user = user;
        this.userSuscription = userSuscription;
        sum = new SQLiteUserManager(ctx);
    }

    /**
     * Actualiza el número de periódicos repartidos en función del día actual. Cuenta los periódicos
     * desde el inicio de la suscripción hasta el día actual, comprobando si existen los últimos
     * días del mes
     */
    public void recalculateDeliveredNewspapers(){
        //Obtener el tiempo transcurrido desde el comienzo de la suscripción
        LocalDate initialDate = userSuscription.getPaymentDate();

        int initialDay = userSuscription.getPaymentDate().getDayOfMonth();
        int monthIndex = initialDate.getMonthOfYear();
        int monthsBetwStartAndNow = Months.monthsBetween(initialDate, LocalDate.now()).getMonths();
        int deliveredNewspapers = 0;

        for(int i = 0; i <= monthsBetwStartAndNow; i++){
            //Suma el primer mes desde el día de la suscripción
            if(i==0){
                LocalDate monthDate = new LocalDate(userSuscription.getPaymentDate().year().get(),Months.months(monthIndex).getMonths(),1);
                int lastMonthDay = monthDate.dayOfMonth().getMaximumValue();

                //Calcula que el mes contenga ese día comparandolo con el último día
                for(Integer j: userSuscription.getChoosenDays()){
                    if(j>initialDay && j<=lastMonthDay){
                        deliveredNewspapers++;
                    }
                }
            }

            //Suma los días completos de los demás meses
            else if(i<monthsBetwStartAndNow){

                LocalDate monthDate = new LocalDate(userSuscription.getPaymentDate().year().get(),Months.months(monthIndex).getMonths(),1);
                int lastMonthDay = monthDate.dayOfMonth().getMaximumValue();

                //Calcula que el mes contenga ese día comparandolo con el último día
                for(Integer j: userSuscription.getChoosenDays()){
                    if(j<=lastMonthDay){
                        deliveredNewspapers++;
                    }
                }

            //Suma los días del último mes hasta la fecha actual
            }else if (i==monthsBetwStartAndNow){
                int currentMonthDay = LocalDate.now().getDayOfMonth();
                for(Integer y: userSuscription.getChoosenDays()){
                    if(y<=currentMonthDay){
                        deliveredNewspapers++;
                    }else{
                        break;
                    }
                }
            }

            //Mueve al siguiente mes
            if(monthIndex!=12){
                monthIndex++;
            }else{
                //En caso de diciembre, cambia a enero
                monthIndex=1;
            }

        }

        sum.updateDeliveredNewspapersForSuscription(userSuscription,deliveredNewspapers);
    }

    /**
     * Comprobará si el usuario está en plazo de pago para la factura actual. Este plazo se calcula
     * a partir del día del mes en el que se creo la suscripción y el número de días máximos
     * establecidos durante el que se permite pagar la factura
     * @return True, si debe de pagar una factura, False de lo contrario
     */
    public boolean timeForPayment(){

        LocalDate paymentDate = userSuscription.getPaymentDate();

        LocalDate lastPaymentDay = new LocalDate(paymentDate.getYear(),LocalDate.now().getMonthOfYear(),paymentDate.getDayOfMonth()+MAX_DAYS_FOR_PAYMENT);

        boolean beforeStartPaymentDay = false;
        if((LocalDate.now().equals(paymentDate) || LocalDate.now().isBefore(paymentDate))){
            beforeStartPaymentDay = true;
        }

        boolean afterLastPaymentDay = false;
        if(LocalDate.now().isAfter(lastPaymentDay) || LocalDate.now().equals(lastPaymentDay)){
            afterLastPaymentDay = true;
        }
        return beforeStartPaymentDay && afterLastPaymentDay;
    }

    /**
     * Actualiza la suscripción a partir del día actual
     */
    public void updateAntiquity(){
        int antiquity = Days.daysBetween(userSuscription.getPaymentDate(),LocalDate.now()).getDays();
        sum.updateAntiquityForSuscription(userSuscription,antiquity);
    }

    /**
     * Comprueba si se debe crear una nueva factura en función de factores determinantes, como son
     * el hecho de si la suscripción no tiene 1 mes de antiguedad o si ya se ha creado la factura
     * de este mes
     * @return True en caso de que deba crearla, False de lo contrario
     */
    public boolean shouldCreateFee(){
        boolean should;

        boolean sameYear = userSuscription.getPaymentDate().getYear() == LocalDate.now().getYear();
        boolean sameMonth = userSuscription.getPaymentDate().getMonthOfYear() == LocalDate.now().getMonthOfYear();

        /**
         * Si este es el primer mes desde que se realizo la suscripción, no se comprueba puesto que
         * no hay mes previo a este.
         */
        //Calcula la fecha de pago del mes pasado
        LocalDate pastMonthPaymentDate = new LocalDate(
                LocalDate.now().getYear(),                          //Año
                LocalDate.now().minusMonths(1).getMonthOfYear(),    //Mes
                userSuscription.getPaymentDate().getDayOfMonth());  //Día

        //¿Existe factura? Sí, no crees. No, creala
        System.out.println("shouldCreateFee: "+pastMonthPaymentDate.toString());
        if((should = sum.getFeeByPaymentDate(pastMonthPaymentDate)==null)) {
            //¿Es el primer mes de la suscripción? Si, no la crees
            if (sameMonth && sameYear) {
                should = false;
            }
        }

        return should;
    }

    /**
     * Crea la factura del mes pasado. Debe llamarse únicamente cuando el día actual sea el día de
     * pago
     */
    public void createFeeForPastMonth(){
        LocalDate monthToPay = LocalDate.now().minusMonths(1);
        int newspapersToPay = 0;

        //Comprueba si los días más altos existen
        for(Integer i: userSuscription.getChoosenDays()){
            if(i<=monthToPay.dayOfMonth().getMaximumValue()){
                newspapersToPay++;
            }
        }

        Fee userFee = new Fee(
                sum.getNextFreeFeeID(),
                userSuscription.getSuscriptionID(),
                newspapersToPay*sum.getSuscriptionPricePerDay(),
                monthToPay.plusMonths(1), //Guarda la fecha de la factura (no el mes pagado)
                false);

        sum.addFeeForUser(user, userFee);
    }

    /**
     * Calcula los días restantes para que el cliente pague la factura actual
     * @return Días restantes para realizar el pago
     */
    public int remainingDaysForPayCurrentFee() {
        LocalDate paymentDate = userSuscription.getPaymentDate();

        LocalDate lastDayForPayment = new LocalDate(paymentDate.getYear(),LocalDate.now().getMonthOfYear(),paymentDate.getDayOfMonth()+MAX_DAYS_FOR_PAYMENT);

        return Days.daysBetween(LocalDate.now(),lastDayForPayment).getDays();
    }

    /**
     * Devuelve la factura que corresponde al mes actual
     * @return Factura del mes actual
     */
    public Fee getCurrentFee(){
        Fee aux = null;
        for(Fee f: sum.getFeesBySuscriptionID(userSuscription.getSuscriptionID())){
            if(f.getPaymentDate().equals(LocalDate.now())){
                aux = f;
            }
        }

        return aux;
    }
}
