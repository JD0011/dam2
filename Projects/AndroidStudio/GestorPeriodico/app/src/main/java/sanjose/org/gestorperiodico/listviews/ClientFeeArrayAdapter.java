package sanjose.org.gestorperiodico.listviews;

import android.app.Activity;
import android.graphics.Color;
import android.provider.CalendarContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Currency;

import sanjose.org.gestorperiodico.BBDD.SQLiteUserManager;
import sanjose.org.gestorperiodico.R;
import sanjose.org.gestorperiodico.datamodels.Fee;
import sanjose.org.gestorperiodico.datamodels.Suscription;
import sanjose.org.gestorperiodico.datamodels.User;

/**
 * Adapter para la lista de facturas
 */
public class ClientFeeArrayAdapter extends ArrayAdapter{

    private ArrayList<Fee> fees;
    private Activity ctx;

    public ClientFeeArrayAdapter(Activity ctx, ArrayList<Fee> fees) {
        super(ctx, R.layout.listview_feelist_template, fees);
        this.fees = fees;
        this.ctx = ctx;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = ctx.getLayoutInflater();
        View item = inflater.inflate(R.layout.listview_feelist_template, null);

        ConstraintLayout clFeeItem = (ConstraintLayout) item.findViewById(R.id.cl_fee_item);

        //Colorea las facturas en función de si están pagadas
        if(fees.get(position).isPayed()){
            clFeeItem.setBackgroundColor(Color.GRAY);
        }

        Fee choosenFee = fees.get(position);

        TextView paymentDay = (TextView) item.findViewById(R.id.lbl_payment_day_value);
        paymentDay.setText(choosenFee.getPaymentDate().toString(SQLiteUserManager.DATE_PATTERN));

        TextView feePrice = (TextView) item.findViewById(R.id.lbl_fee_price_value);
        feePrice.setText(String.valueOf(choosenFee.getFeePrice()+"€"));

        return item;
    }
}
