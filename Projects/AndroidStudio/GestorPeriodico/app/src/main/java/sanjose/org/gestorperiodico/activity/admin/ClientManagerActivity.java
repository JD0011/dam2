package sanjose.org.gestorperiodico.activity.admin;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;

import sanjose.org.gestorperiodico.BBDD.SQLiteUserManager;
import sanjose.org.gestorperiodico.R;
import sanjose.org.gestorperiodico.datamodels.User;

/**
 * Esta clase muestra información sobre el cliente y la suscripción, si es que posee
 */
public class ClientManagerActivity extends AppCompatActivity {

    private User user;
    private ArrayList<Button> buttons;
    private View.OnClickListener buttonsListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_client_manager);

        //Extrae controles
        buttons = new ArrayList<Button>();
        buttons.add((Button) findViewById(R.id.btn_delete_user));
        buttons.add((Button) findViewById(R.id.btn_return));

        Bundle extras = getIntent().getExtras();

        user = (User) extras.getSerializable("user");

        ((TextView)findViewById(R.id.lbl_username_value)).setText(user.getUsername());
        ((TextView)findViewById(R.id.lbl_name_value)).setText(user.getName());
        ((TextView)findViewById(R.id.lbl_lastname_value)).setText(user.getLastname());
        ((TextView)findViewById(R.id.lbl_address_value)).setText(user.getAddress());
        ((TextView)findViewById(R.id.lbl_postal_code_value)).setText(String.valueOf(user.getPostalCode()));

        buttonsListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Context ctx = ClientManagerActivity.this;
                switch (view.getId()){
                    case R.id.btn_delete_user:
                        new AlertDialog.Builder(ctx)
                                .setTitle(R.string.lbl_are_you_sure)
                                .setMessage(R.string.lbl_delete_this_user)
                                .setPositiveButton(R.string.lbl_yes, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        SQLiteUserManager sum = new SQLiteUserManager(ctx);
                                        if(sum.deleteUserByID(user.getUser_id())){
                                            Toast.makeText(ctx, R.string.msg_user_deleted_ok,Toast.LENGTH_SHORT).show();
                                            finish();
                                        }else{
                                            Toast.makeText(ctx, R.string.error_cant_delete_user,Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                })
                                .setNegativeButton(R.string.lbl_no, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.cancel();
                                    }
                                }).show();

                        break;
                    case R.id.btn_return:
                        finish();
                        break;
                }
            }
        };

        for(Button b: buttons){
            b.setOnClickListener(buttonsListener);
        }

    }
}
