package sanjose.org.gestorperiodico.activity.suscription;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import sanjose.org.gestorperiodico.BBDD.SQLiteUserManager;
import sanjose.org.gestorperiodico.R;
import sanjose.org.gestorperiodico.activity.user.UserMainActivity;
import sanjose.org.gestorperiodico.datamodels.User;
import sanjose.org.gestorperiodico.datamodels.Fee;
import sanjose.org.gestorperiodico.datamodels.Suscription;

public class CreateSuscriptionActivity extends AppCompatActivity {

    private ArrayList<Button> buttons;
    private View.OnClickListener buttonsListener;
    private View.OnClickListener daysListener;

    private Context ctx;
    private ArrayList<Integer> choosenDays;

    private static final int MINIMUN_SUSCRIPTION_DAYS = 3;

    private TableLayout tlCalendar;

    private User user;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_suscription);

        ctx = CreateSuscriptionActivity.this;

        //Extrae controles
        tlCalendar = (TableLayout) findViewById(R.id.tl_calendar);

        buttons = new ArrayList<>();
        buttons.add((Button) findViewById(R.id.btn_accept_suscription));
        buttons.add((Button) findViewById(R.id.btn_cancel));

        //Obtiene el usuario a establecer la suscripción
        Bundle extras = getIntent().getExtras();

        user = (User)extras.getSerializable("user");

        //LISTENER'S
        choosenDays = new ArrayList<Integer>();
        daysListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TextView pushedTV = (TextView) view;
                int day = Integer.parseInt(pushedTV.getText().toString());

                if(pushedTV.getBackground()==null){
                    choosenDays.add(day);
                    pushedTV.setBackgroundColor(Color.GREEN);
                    pushedTV.setTextColor(Color.BLACK);
                }else{
                    choosenDays.remove(new Integer(day));
                    pushedTV.setBackground(null);
                    pushedTV.setTextColor(Color.GRAY);
                }

            }
        };

        //Asigna el listener a los textview

        for(int i = 0;i< tlCalendar.getChildCount();i++){
            if(tlCalendar.getChildAt(i) instanceof TableRow){

                TableRow currentRow = (TableRow) tlCalendar.getChildAt(i);
                for(int j=0;j <currentRow.getChildCount();j++){
                    if(currentRow.getChildAt(j) instanceof TextView){
                        currentRow.getChildAt(j).setOnClickListener(daysListener);
                    }
                }
            }
        }

        buttonsListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (view.getId()){
                    case R.id.btn_accept_suscription:
                        //Comprueba que se cumple la cantidad mínima de días para la suscripción
                        if(choosenDays.size()<=MINIMUN_SUSCRIPTION_DAYS){
                            new AlertDialog.Builder(ctx)
                                    .setMessage(R.string.error_min_days_suscription_is+" "+MINIMUN_SUSCRIPTION_DAYS)
                                    .setPositiveButton(R.string.lbl_ok, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            dialogInterface.cancel();
                                        }
                                    }).show();
                        }else{
                            SQLiteUserManager sum = new SQLiteUserManager(ctx);

                            Suscription userSuscription = new Suscription(ctx, sum.getNextFreeSuscriptionID(), choosenDays, LocalDate.now(),0,0);

                            if(sum.setUserSuscription(user,userSuscription)){
                                Toast.makeText(ctx,R.string.msg_suscription_registered,Toast.LENGTH_SHORT).show();
                                finish();
                            }
                        }
                        break;
                    case R.id.btn_cancel:
                        finish();
                        break;
                }
            }
        };

        for(Button b: buttons){
            b.setOnClickListener(buttonsListener);
        }
    }

}
