package sanjose.org.gestorperiodico.BBDD;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Collections;

import sanjose.org.gestorperiodico.datamodels.Fee;
import sanjose.org.gestorperiodico.datamodels.Suscription;
import sanjose.org.gestorperiodico.datamodels.User;

/**
 * Clase que gestiona los usuarios del sistema conectandose a la base de datos. Como este es un
 * proyecto simulado, esta base de datos se encuentra en local. En condiciones normales, estaría
 * hosteada en internet.
 */

public class SQLiteUserManager {

    private static String DAY_SEPARATOR="-";
    //Patrón de fecha
    public static final String DATE_PATTERN = "yyyy-MM-dd";
    //Formatter para el patrón de fecha
    private DateTimeFormatter dtf = DateTimeFormat.forPattern(DATE_PATTERN);
    private AdminSQLiteOpenHelper aoh;
    private Context ctx;

    public SQLiteUserManager(Context ctx){
        this.ctx = ctx;

        aoh = new AdminSQLiteOpenHelper(ctx,"gestion_periodicos", null, 1);
    }

    /**
     * Añade el usuario a la base de datos
     * @param user Usuario a registrar
     * @return True si el proceso ha sido satisfactorio, False de lo contrario
     */
    public boolean addUser(User user){
        SQLiteDatabase ddbb = aoh.getWritableDatabase();

        ContentValues cv = new ContentValues();
        cv.put("user_id", user.getUser_id());
        cv.put("username", user.getUsername());
        cv.put("name", user.getName());
        cv.put("password", user.getPassword());
        cv.put("lastname", user.getLastname());
        cv.put("address", user.getAddress());
        cv.put("postal_code", user.getPostalCode());
        cv.put("type_user", user.getTypeUser());
        cv.put("user_suscription", user.getUserSuscriptionID());

        long result = ddbb.insert("Users",null, cv);
        ddbb.close();

        return result!=-1;
    }

    /**
     * Tramita el pago de la factura del usuario sobre los dias especificados, aumentando
     * el valor de los periódicos pagados
     * @param fee Usuario que ha pagado
     * @return True si se ha tramitado correctamente, False de lo contrario
     */
    public boolean payFee(Fee fee){
        SQLiteDatabase ddbb = aoh.getWritableDatabase();

        ContentValues cv = new ContentValues();
        cv.put("is_payed",1);

        long rowsUpdated = ddbb.update(AdminSQLiteOpenHelper.FEE_TABLE_NAME, cv, "fee_id=?", new String[]{String.valueOf(fee.getFeeID())});
        ddbb.close();

        return rowsUpdated==1;
    }

    /**
     * Elimina los datos de un usuario, entendiendose como tales, su registro en la tabla usuarios,
     * suscripciones y las facturas que le pertenecen.
     * @param user_id Key para buscar el usuario a eliminar.
     * @return True si ha eliminado uno, False si no econtró coincidencias
     */
    public boolean deleteUserByID(int user_id){
        boolean userDeleted;

        deleteFeesByUserID(user_id);
        deleteSuscriptionByID(getClientByID(user_id).getUserSuscriptionID());

        SQLiteDatabase ddbb = aoh.getWritableDatabase();

        userDeleted = ddbb.delete("Users","user_id=?",new String[]{String.valueOf(user_id)}) !=0;
        ddbb.close();

        return userDeleted;
    }

    public boolean deleteSuscriptionByID(int suscriptionID){
        boolean suscriptionDeleted;

        SQLiteDatabase ddbb= aoh.getWritableDatabase();

        suscriptionDeleted = ddbb.delete("Suscriptions","suscription_ID=?",new String[]{String.valueOf(suscriptionID)}) !=0;

        ddbb.close();

        return  suscriptionDeleted;
    }

    public boolean deleteFeesByUserID(int user_id){
        boolean feesDeleted;

        SQLiteDatabase ddbb = aoh.getWritableDatabase();

        feesDeleted = ddbb.delete(AdminSQLiteOpenHelper.FEE_TABLE_NAME, "user_id=?", new String[]{String.valueOf(user_id)}) !=0;

        return feesDeleted;
    }

    /**
     * Comprueba si ya está registrado el correo electrónico.
     * @param username Correo electrónico a comprobar
     * @return True si el correo está registrado, False de lo contrario
     */
    public boolean existsClientname(String username){
        boolean exists;
        Cursor c;

        SQLiteDatabase ddbb = aoh.getWritableDatabase();

        c = ddbb.query(
                AdminSQLiteOpenHelper.USER_TABLE_NAME,
                new String[]{"Username"},
                "username=?",
                new String[]{username},
                null, null, null);

        exists = c.getCount()!=0;

        c.close();
        ddbb.close();

        return exists;
    }

    /**
     * Inserta datos de muestra para el testeo de la aplicación
     */
    public void inserSampleData(){
        ContentValues cv = new ContentValues();

        SQLiteDatabase ddbb = aoh.getWritableDatabase();
        cv.put("fee_id",0);
        cv.put("suscription_id",3);
        cv.put("fee_price","7.5");
        cv.put("payment_date","2018-03-3");
        cv.put("is_payed",0);

        ddbb.insert(AdminSQLiteOpenHelper.FEE_TABLE_NAME,null,cv);
        cv.clear();

        cv.put("fee_id",1);
        cv.put("suscription_id",4);
        cv.put("fee_price","7.5");
        cv.put("payment_date","2018-03-2");
        cv.put("is_payed",0);

        ddbb.insert(AdminSQLiteOpenHelper.FEE_TABLE_NAME,null,cv);
        cv.clear();

        cv.put("fee_id",2);
        cv.put("suscription_id",5);
        cv.put("fee_price","7.5");
        cv.put("payment_date","2018-03-5");
        cv.put("is_payed",0);

        ddbb.insert(AdminSQLiteOpenHelper.FEE_TABLE_NAME,null,cv);
        cv.clear();

        cv.put("fee_id",3);
        cv.put("suscription_id",6);
        cv.put("fee_price","7.5");
        cv.put("payment_date","2018-02-24");
        cv.put("is_payed",0);

        //FACTURAS NO PAGADAS FUERA DE FECHA
        ddbb.insert(AdminSQLiteOpenHelper.FEE_TABLE_NAME,null,cv);
        cv.clear();

        cv.put("fee_id",4);
        cv.put("suscription_id",6);
        cv.put("fee_price","7.5");
        cv.put("payment_date","2018-01-20");
        cv.put("is_payed",0);

        ddbb.insert(AdminSQLiteOpenHelper.FEE_TABLE_NAME,null,cv);
        cv.clear();

        cv.put("fee_id",5);
        cv.put("suscription_id",6);
        cv.put("fee_price","7.5");
        cv.put("payment_date","2018-01-19");
        cv.put("is_payed",0);
        //FIN FACTURAS NO PAGADAS FUERA DE FECHA

        ddbb.insert(AdminSQLiteOpenHelper.FEE_TABLE_NAME,null,cv);
        cv.clear();

        //SUSCRIPCIONES
        cv.put("suscription_id",0);
        cv.put("choosen_days","1-11-18-21-3");
        cv.put("payment_date","2018-01-28");
        cv.put("delivered_newspapers",0);
        cv.put("antiquity",0);

        ddbb.insert(AdminSQLiteOpenHelper.SUSCRIPTION_TABLE_NAME,null,cv);
        cv.clear();

        cv.put("suscription_id",1);
        cv.put("choosen_days","1-17-12-21-3");
        cv.put("payment_date","2018-01-2");
        cv.put("delivered_newspapers",0);
        cv.put("antiquity",0);

        ddbb.insert(AdminSQLiteOpenHelper.SUSCRIPTION_TABLE_NAME,null,cv);
        cv.clear();

        cv.put("suscription_id",2);
        cv.put("choosen_days","1-11-19-15-10");
        cv.put("payment_date","2017-12-28");
        cv.put("delivered_newspapers",0);
        cv.put("antiquity",0);

        ddbb.insert(AdminSQLiteOpenHelper.SUSCRIPTION_TABLE_NAME,null,cv);
        cv.clear();

        cv.put("suscription_id",3);
        cv.put("choosen_days","13-29-16-13-9");
        cv.put("payment_date","2018-01-21");
        cv.put("delivered_newspapers",0);
        cv.put("antiquity",0);

        ddbb.insert(AdminSQLiteOpenHelper.SUSCRIPTION_TABLE_NAME,null,cv);
        cv.clear();

        //FIN SUSCRIPCIONES

        //USUARIOS
        cv.put("user_id",1);
        cv.put("username","admin");
        cv.put("password","admin");
        cv.put("name","Administrador");
        cv.put("lastname","Superusuario");
        cv.put("address","Base de datos");
        cv.put("postal_code",29010);
        cv.put("type_user", User.ADMIN);
        cv.put("user_suscription",-1);

        ddbb.insert(AdminSQLiteOpenHelper.USER_TABLE_NAME,null,cv);
        cv.clear();

        cv.put("user_id",2);
        cv.put("username","user");
        cv.put("password","user");
        cv.put("name","Cliente");
        cv.put("lastname","Cliente");
        cv.put("address","av/ Huelin 35, Málaga");
        cv.put("postal_code",29010);
        cv.put("type_user", User.CLIENT);
        cv.put("user_suscription",-1);

        ddbb.insert(AdminSQLiteOpenHelper.USER_TABLE_NAME,null,cv);
        cv.clear();

        cv.put("user_id",3);
        cv.put("username","pepe01");
        cv.put("password","pepe01");
        cv.put("name","Pepe");
        cv.put("lastname","Romero");
        cv.put("address","c/ Mármoles 12, Málaga");
        cv.put("postal_code",29010);
        cv.put("type_user", User.CLIENT);
        cv.put("user_suscription",0);

        ddbb.insert(AdminSQLiteOpenHelper.USER_TABLE_NAME,null,cv);
        cv.clear();

        cv.put("user_id",4);
        cv.put("username","joser");
        cv.put("password","joser01");
        cv.put("name","José");
        cv.put("lastname","Romero");
        cv.put("address","c/ Mármoles 13, Málaga");
        cv.put("postal_code",29010);
        cv.put("type_user", User.CLIENT);
        cv.put("user_suscription",1);

        ddbb.insert(AdminSQLiteOpenHelper.USER_TABLE_NAME,null,cv);
        cv.clear();

        cv.put("user_id",5);
        cv.put("username","anton");
        cv.put("password","anton01");
        cv.put("name","Antonio");
        cv.put("lastname","García");
        cv.put("address","c/ Alameda 4, Granada");
        cv.put("postal_code",29010);
        cv.put("type_user", User.CLIENT);
        cv.put("user_suscription",2);

        ddbb.insert(AdminSQLiteOpenHelper.USER_TABLE_NAME,null,cv);
        cv.clear();

        cv.put("user_id",6);
        cv.put("username","rosa");
        cv.put("password","rosa01");
        cv.put("name","Rosa");
        cv.put("lastname", "Martínez");
        cv.put("address", "Av / Alameda 12, Málaga");
        cv.put("postal_code",29010);
        cv.put("type_user", User.CLIENT);
        cv.put("user_suscription",3);

        ddbb.insert(AdminSQLiteOpenHelper.USER_TABLE_NAME,null,cv);
        cv.clear();
        ddbb.close();
    }

    /**
     * Comprueba si la contraseña suministrada con el nombre de usuario corresponde al mismo
     * @param user Usuario que se loguea
     * @return True en caso de que la contraseña este bien, False de lo contrario
     */
    public boolean checkPassword(User user){
        boolean passwordMatchs;

        SQLiteDatabase ddbb = aoh.getWritableDatabase();

        Cursor c = ddbb.query(
                "Users",
                new String[]{"password"},
                "username=?",
                new String[]{user.getUsername()},
                null ,null, null);

        if(c.moveToFirst()){
            passwordMatchs = c.getString(0).equals(user.getPassword());
        }else{
            passwordMatchs = false;
        }

        c.close();
        ddbb.close();

        return passwordMatchs;
    }

    /**
     * Devuelve el usuario en función de su correo
     * @param username Key para buscar el usuario deseado
     * @return
     */
    public User getClientByUsername(String username){
        User aux = null;

        SQLiteDatabase ddbb = aoh.getWritableDatabase();
        Cursor c;

        c = ddbb.query(
                //De los usuarios
                "Users",
                //Dame todas las columnas
                AdminSQLiteOpenHelper.USERS_COLUMNS,
                //De aquel cuyo correo
                "username=?",
                //Coincida con este
                new String[]{username},
                null,null,null);

        c.moveToNext();
        //Comprueba si tiene suscripción.
        if(c.getInt(5)!=-1){
            aux = new User(c.getInt(0),c.getString(1),c.getString(2),c.getString(3),c.getString(4),c.getString(5),c.getInt(6), c.getString(7),c.getInt(8));
        }else{
            aux = new User(c.getInt(0),c.getString(1),c.getString(2),c.getString(3),c.getString(4),c.getString(5),c.getInt(6), c.getString(7));
        }

        c.close();
        ddbb.close();

        return aux;
    }

    /**
     * Devuelve el usuario en función de su ID
     * @param ID Identificador del usuario
     * @return
     */
    public User getClientByID(int ID){
        User aux = null;

        SQLiteDatabase ddbb = aoh.getWritableDatabase();
        Cursor c;

        c = ddbb.query(
                //De los usuarios
                "Users",
                //Dame todas las columnas
                AdminSQLiteOpenHelper.USERS_COLUMNS,
                //De aquel cuyo correo
                "user_id=?",
                //Coincida con este
                new String[]{String.valueOf(ID)},
                null,null,null);

        c.moveToNext();
        //Comprueba si tiene suscripción.
        if(c.getInt(5)!=0){
            aux = new User(c.getInt(0),c.getString(1),c.getString(2),c.getString(3),c.getString(4),c.getString(5),c.getInt(6), c.getString(7),c.getInt(8));
        }else{
            aux = new User(c.getInt(0),c.getString(1),c.getString(2),c.getString(3),c.getString(4),c.getString(5),c.getInt(6), c.getString(7));
        }

        c.close();
        ddbb.close();

        return aux;
    }

    /**
     * Establece una suscripción al usuario especificado
     * @param user Usuario dueño de la suscripción
     * @param suscription Suscripción a establecer
     * @return True si se graba correctamente, False de lo contrario
     */
    public boolean setUserSuscription(User user, Suscription suscription){

        SQLiteDatabase ddbb = aoh.getWritableDatabase();

        ContentValues cvSuscription = new ContentValues();
        cvSuscription.put("suscription_id",suscription.getSuscriptionID());
        cvSuscription.put("choosen_days",daysFromListToString(suscription.getChoosenDays()));
        cvSuscription.put("payment_date",suscription.getPaymentDate().toString());
        cvSuscription.put("delivered_newspapers",suscription.getDeliveredNewspapers());
        cvSuscription.put("antiquity",suscription.getAntiquity());

        long result = ddbb.insert(AdminSQLiteOpenHelper.SUSCRIPTION_TABLE_NAME, null, cvSuscription);

        //Si se agregó bien, establece al usuario la suscripción
        int updatedRows=0;
        if(result!=-1){
            //Añadir suscripción al usuario
            ContentValues cvUser = new ContentValues();
            cvUser.put("user_suscription",suscription.getSuscriptionID());
            updatedRows = ddbb.update(AdminSQLiteOpenHelper.USER_TABLE_NAME, cvUser,"user_id=?",new String[]{String.valueOf(user.getUser_id())});
        }

        ddbb.close();

        // result!=-1 Código de error
        return result!=-1 && updatedRows!=0;
    }

    public Suscription getSuscriptionByID(int suscription_id){
        Suscription aux = null;

        SQLiteDatabase ddbb = aoh.getWritableDatabase();

        Cursor c = ddbb.query(
                "suscriptions",
                AdminSQLiteOpenHelper.SUSCRIPTIONS_COLUMNS,
                "suscription_id=?",
                new String[]{String.valueOf(suscription_id)},
                null,null,null);

        if(c.moveToFirst()){
            String dateFormat="";
            for(char ch: c.getString(2).toCharArray()){
                if(ch!='/'){
                    dateFormat+=ch;
                }else{
                    dateFormat+='-';
                }
            }
            System.out.println("getSuscriptionByID as String: "+c.getString(2));
            System.out.println("getSuscriptionByID as String: "+dateFormat);
            System.out.println("getSuscriptionByID as LocalDate: "+dtf.parseLocalDate(dateFormat));
            aux = new Suscription(ctx, c.getInt(0),daysFromStringToList(c.getString(1)), dtf.parseLocalDate(dateFormat), c.getInt(3), c.getInt(4));
        }

        c.close();
        ddbb.close();

        return aux;
    }

    /**
     * Actualiza la suscripción con los nuevos valores
     * @param suscription Suscripción a actualizar
     * @return True si se ha actualizado satisfactoriamente, False de lo contrario
     */
    public boolean updateSuscription(Suscription suscription){
        SQLiteDatabase ddbb = aoh.getWritableDatabase();

        ContentValues cvSuscription = new ContentValues();
        cvSuscription.put("suscription_id",suscription.getSuscriptionID());
        cvSuscription.put("payment_date",suscription.getPaymentDate().toString());
        cvSuscription.put("choosen_days",daysFromListToString(suscription.getChoosenDays()));
        cvSuscription.put("delivered_newspapers",suscription.getDeliveredNewspapers());
        cvSuscription.put("antiquity",suscription.getAntiquity());

        int rowsUpdated = ddbb.update(AdminSQLiteOpenHelper.SUSCRIPTION_TABLE_NAME,cvSuscription,"suscription_id=?", new String[]{String.valueOf(suscription.getSuscriptionID())});

        return rowsUpdated==1;
    }

    /**
     * Devuelve la siguiente ID asignable para suscripcion que no esté ocupada
     */
    public int getNextFreeSuscriptionID(){

        SQLiteDatabase ddbb = aoh.getWritableDatabase();

        Cursor c = ddbb.query(
                AdminSQLiteOpenHelper.SUSCRIPTION_TABLE_NAME,
                new String[]{"suscription_id"},
                null,null,null,null,null);

        int freeID = 0;
        if(c.moveToNext()){
            while(!c.isAfterLast()){
                if(c.getInt(0)>freeID){
                    freeID = c.getInt(0);
                }

                c.moveToNext();
            }
        }

        c.close();
        ddbb.close();

        freeID++;

        return freeID;
    }

    /**
     * Devuelve el siguiente ID libre para usuario
     * @return Siguiente ID libre
     */
    public int getNextFreeUserID(){

        SQLiteDatabase ddbb = aoh.getWritableDatabase();

        Cursor c = ddbb.query(
                AdminSQLiteOpenHelper.USER_TABLE_NAME,
                new String[]{"user_id"},
                null,null,null,null,null);

        int freeID = 0;
        if(c.moveToNext()){
            while(!c.isAfterLast()){
                if(c.getInt(0)>freeID){
                    freeID = c.getInt(0);
                }

                c.moveToNext();
            }
        }

        c.close();
        ddbb.close();

        freeID++;

        return freeID;
    }

    /**
     * Devuelve el siguiente ID libre para factura
     * @return Siguiente ID libre
     */
    public int getNextFreeFeeID(){

        SQLiteDatabase ddbb = aoh.getWritableDatabase();

        Cursor c = ddbb.query(
                AdminSQLiteOpenHelper.FEE_TABLE_NAME,
                new String[]{"fee_id"},
                null,null,null,null,null);

        int freeID = 0;
        if(c.moveToNext()){
            while(!c.isAfterLast()){
                if(c.getInt(0)>freeID){
                    freeID = c.getInt(0);
                }

                c.moveToNext();
            }
        }

        c.close();
        ddbb.close();

        freeID++;

        return freeID;
    }

    public boolean setSuscriptionPricePerDay(Double price){
        SQLiteDatabase ddbb = aoh.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put("price_per_day",price);
        cv.put("price_type",0);

        long result = ddbb.insert(AdminSQLiteOpenHelper.SUSCRIPTION_PRICE_TABLE_NAME, null, cv);
        ddbb.close();

        // result!=-1 Código de error
        return result!=-1;
    }

    public double getSuscriptionPricePerDay(){
        double price;
        SQLiteDatabase ddbb = aoh.getWritableDatabase();

        Cursor c = ddbb.query(
                AdminSQLiteOpenHelper.SUSCRIPTION_PRICE_TABLE_NAME,
                new String[]{"price_per_day"},
                "price_type=?", new String[]{String.valueOf(0)},
                null, null, null);

        if(c.moveToFirst()){
            price = c.getDouble(0);
        }else{
            price = 0;
        }

        c.close();
        ddbb.close();

        return price;
    }

    /**
     * Devuelve la lista de morosos
     * @return Lista de Usuarios morosos
     */
    public ArrayList<User> getDefaulters(){
        ArrayList<Integer> userides = getAllClientsIDs();
        ArrayList<Integer> defaultersUserID = new ArrayList<>();
        ArrayList<User> defaulters = new ArrayList<>();
        SQLiteDatabase ddbb = aoh.getWritableDatabase();

        //Itera entre los nombres de usuarios
        for(int id: userides){

            Cursor c = ddbb.query(
                    AdminSQLiteOpenHelper.FEE_TABLE_NAME,
                    AdminSQLiteOpenHelper.FEE_COLUMNS,
                    "user_id=?",
                    new String[]{String.valueOf(id)},
                    null, null, null);

            //Comprueba sus facturas
            if(c.moveToFirst()){
                while(!c.isAfterLast()){
                    //Si no esta pagada
                    if(c.getInt(4)==0){
                        LocalDate paymentDate = dtf.parseLocalDate(c.getString(3));
                        //y ha superado la fecha límite
                        if(paymentDate.isBefore(LocalDate.now())){
                            //es moroso
                            defaultersUserID.add(id);
                        }
                    }

                    c.moveToNext();
                }
            }
            c.close();
        }

        ddbb.close();

        //Extrae los usuarios
        for(int defID: defaultersUserID){
            defaulters.add(getClientByID(defID));
        }

        return defaulters;
    }

    public Fee getFeeByPaymentDate(LocalDate paymentDate){
        Fee aux = null;

        SQLiteDatabase ddbb = aoh.getWritableDatabase();

        Cursor c = ddbb.query(
                AdminSQLiteOpenHelper.FEE_TABLE_NAME,
                AdminSQLiteOpenHelper.FEE_COLUMNS,
                null,  null,null, null, null);

        if(c.moveToNext()){
            while(!c.isAfterLast()){
                //Convierte a formato para fecha
                LocalDate feePaymentDate = dtf.parseLocalDate(c.getString(3));
                if(feePaymentDate.equals(LocalDate.now())){
                    aux = new Fee(c.getInt(0),c.getInt(1),c.getDouble(2),feePaymentDate,c.getInt(4)==1);
                }

                c.moveToNext();
            }
        }

        return aux;
    }

    /**
     * Devuelve la factura a aprtir del ID  del cliente
     * @param user Cliente que figura en la factura
     * @return
     */
    public ArrayList<Fee> getFeesBySuscriptionID(int suscriptionID){
        ArrayList<Fee> fees = new ArrayList<Fee>();
        SQLiteDatabase ddbb = aoh.getWritableDatabase();

        Cursor c = ddbb.query(
                AdminSQLiteOpenHelper.FEE_TABLE_NAME,
                AdminSQLiteOpenHelper.FEE_COLUMNS,
                "suscription_id=?",
                new String[]{String.valueOf(suscriptionID)},
                null, null, null);

        if(c.moveToFirst()){
            while(!c.isAfterLast()){

                LocalDate paymentDay = dtf.parseLocalDate(c.getString(3));

                fees.add( new Fee(c.getInt(0),c.getInt(1),c.getDouble(2),paymentDay, c.getInt(4)==1));
                c.moveToNext();
            }
        }

        c.close();
        ddbb.close();

        return fees;
    }

    /**
     * Comprueba si el cliente tiene facturas pentiendes
     * @param client Cliente a consultar
     * @return True si las tiene, False de lo contrario
     */
    public boolean clientHasFees(User client){
        boolean hasFees;
        SQLiteDatabase ddbb = aoh.getWritableDatabase();

        Cursor c = ddbb.query(
                AdminSQLiteOpenHelper.FEE_TABLE_NAME,
                new String[]{"fee_id"},"user_id=?",
                new String[]{String.valueOf(client.getUser_id())},
                null, null ,null
        );

        hasFees = c.moveToFirst();
        ddbb.close();
        c.close();

        return hasFees;
    }

    /**
     * Registra una factura a nombre del usuario especificado
     * @param user Cliente al que pertenece la factura
     * @param userFee Factura del cliente
     * @return True si se registra correctamente, False de lo contrario
     */
    public boolean addFeeForUser(User user, Fee userFee){
        SQLiteDatabase ddbb = aoh.getWritableDatabase();
        System.out.println("EIEIEI: "+userFee.getPaymentDate().toString(dtf));
        System.out.println(userFee.getPaymentDate().toString());
        ContentValues cv = new ContentValues();
        cv.put("fee_id",userFee.getFeeID());
        cv.put("suscription_id", user.getUser_id());
        cv.put("fee_price",userFee.getFeePrice());
        cv.put("payment_date", userFee.getPaymentDate().toString(dtf));
        cv.put("is_payed",userFee.isPayed());

        long result = ddbb.insert(AdminSQLiteOpenHelper.FEE_TABLE_NAME,null,cv);
        ddbb.close();

        // result!=-1 Código de error
        return result!=-1;
    }

    /**
     * Devuelve todos los correos de los usuarios en forma de lista
     * @return Lista de todos los correos utilizados por los usuarios
     */
    public ArrayList<String> getAllClientsNames(){
        ArrayList<String> usernames = new ArrayList<String>();

        SQLiteDatabase ddbb = aoh.getWritableDatabase();

        Cursor c;

        c = ddbb.query(
                //De los usuarios
                "Users",
                //Dame todas las columnas
                new String[]{"username"},
                null,null,null,null,null);

        if(c.moveToFirst()){
            while(!c.isAfterLast()){
                usernames.add(c.getString(0));
                c.moveToNext();
            }
        }

        ddbb.close();

        return usernames;
    }

    /**
     * Devuelve todos los correos de los usuarios en forma de lista
     * @return Lista de todos los correos utilizados por los usuarios
     */
    public ArrayList<Integer> getAllClientsIDs(){
        ArrayList<Integer> ides = new ArrayList<Integer>();

        SQLiteDatabase ddbb = aoh.getWritableDatabase();

        Cursor c;

        c = ddbb.query(
                //De los usuarios
                "Users",
                //Dame todas las columnas
                new String[]{"user_id"},
                null,null,null,null,null);

        if(c.moveToFirst()){
            while(!c.isAfterLast()){
                ides.add(c.getInt(0));
                c.moveToNext();
            }
        }

        ddbb.close();

        return ides;
    }

    public ArrayList<User> retrieveAllClients() {
        ArrayList<User> users = new ArrayList<>();

        SQLiteDatabase ddbb = aoh.getWritableDatabase();

        Cursor c = ddbb.query(AdminSQLiteOpenHelper.USER_TABLE_NAME,AdminSQLiteOpenHelper.USERS_COLUMNS, null, null, null, null, null);

        if(c.moveToFirst()){
            while(!c.isAfterLast()){

                //Si tiene suscripcion
                if(c.getInt(6)!=-1){
                    users.add(new User(c.getInt(0),c.getString(1),c.getString(2),c.getString(3),c.getString(4),c.getString(5),c.getInt(6),c.getString(7), c.getInt(8)));
                }else{
                    users.add(new User(c.getInt(0),c.getString(1),c.getString(2),c.getString(3),c.getString(4),c.getString(5),c.getInt(6),c.getString(7)));
                }

                c.moveToNext();
            }
        }

        c.close();
        ddbb.close();

        return users;
    }

    public boolean updateNameForUser(User userToModify, String value) {
        ContentValues cv = new ContentValues();
        cv.put("name",value);

        SQLiteDatabase ddbb = aoh.getWritableDatabase();

        int rowsUpdated = ddbb.update(AdminSQLiteOpenHelper.USER_TABLE_NAME,cv, "user_id=?", new String[]{String.valueOf(userToModify.getUser_id())});
        ddbb.close();

        return rowsUpdated==1;
    }

    public boolean updateLastnameForUser(User userToModify, String value) {
        ContentValues cv = new ContentValues();
        cv.put("lastname",value);

        SQLiteDatabase ddbb = aoh.getWritableDatabase();

        int rowsUpdated = ddbb.update(AdminSQLiteOpenHelper.USER_TABLE_NAME,cv, "user_id=?", new String[]{String.valueOf(userToModify.getUser_id())});
        ddbb.close();

        return rowsUpdated==1;
    }

    public boolean updateAddressForUser(User userToModify, String value) {
        ContentValues cv = new ContentValues();
        cv.put("address",value);

        SQLiteDatabase ddbb = aoh.getWritableDatabase();

        int rowsUpdated = ddbb.update(AdminSQLiteOpenHelper.USER_TABLE_NAME,cv, "user_id=?", new String[]{String.valueOf(userToModify.getUser_id())});
        ddbb.close();

        return rowsUpdated==1;
    }

    public boolean updatePostalCodeForUser(User userToModify, String value) {
        ContentValues cv = new ContentValues();
        cv.put("postal_code",value);

        SQLiteDatabase ddbb = aoh.getWritableDatabase();

        int rowsUpdated = ddbb.update(AdminSQLiteOpenHelper.USER_TABLE_NAME,cv, "user_id=?", new String[]{String.valueOf(userToModify.getUser_id())});
        ddbb.close();

        return rowsUpdated==1;
    }

    public boolean updatePasswordForUser(User userToModify, String value) {
        ContentValues cv = new ContentValues();
        cv.put("password",value);

        SQLiteDatabase ddbb = aoh.getWritableDatabase();

        int rowsUpdated = ddbb.update(AdminSQLiteOpenHelper.USER_TABLE_NAME,cv, "user_id=?", new String[]{String.valueOf(userToModify.getUser_id())});
        ddbb.close();

        return rowsUpdated==1;
    }

    /**
     * Actualiza el número de periódicos repartidos hasta el momento
     * @param suscription Suscripción a la que está suscrito el usuario
     * @param deliveredNewspapers Valor de periódicos repartidos
     * @return True si el registro se actualiza correctamente, False de lo contrario
     */
    public boolean updateDeliveredNewspapersForSuscription(Suscription suscription, int deliveredNewspapers){
        SQLiteDatabase ddbb = aoh.getWritableDatabase();

        //Esablece la nueva cantidad de periódicos repartidos
        ContentValues cv = new ContentValues();
        cv.put("delivered_newspapers", deliveredNewspapers);

        int rowsUpdated = ddbb.update(AdminSQLiteOpenHelper.SUSCRIPTION_TABLE_NAME, cv, "suscription_id=?", new String[]{String.valueOf(suscription.getSuscriptionID())});
        ddbb.close();

        return rowsUpdated==1;
    }

    /**
     * Actualiza la antiguedad de la sucripción por el valor especificado por parámetros
     * @param suscription Suscripción a la que está suscrito el usuarip
     * @param antiquity Valor de antiguedad de la suscripción
     * @return True si el registro se actualiza correctamente, False de lo contrario
     */
    public boolean updateAntiquityForSuscription(Suscription suscription, int antiquity){
        ContentValues cv = new ContentValues();

        cv.put("antiquity", antiquity);

        SQLiteDatabase ddbb = aoh.getWritableDatabase();

        int rowsUpdated = ddbb.update(AdminSQLiteOpenHelper.SUSCRIPTION_TABLE_NAME, cv, "suscription_id=?", new String[]{String.valueOf(suscription.getSuscriptionID())});

        return rowsUpdated==1;
    }

    /**
     * Convierte la lista de enteros de los días elegidos por el usuario a un String separado
     * por un carácter
     * @param days Lista de días elegidos en la suscripción
     * @return
     */
    private static String daysFromListToString(ArrayList<Integer> days){
        //Primero ordena los números
        Collections.sort(days);

        //Genera el string
        String output="";
        for(int i=0;i<days.size();i++){
            output+=days.get(i);

            if(i<days.size()-1){
                output+=DAY_SEPARATOR;
            }
        }

        return output;
    }

    /**
     * Devuelve una lista de días elegidos para la suscripción a partir del string pasado por
     * parámetros
     * @param daysString String que contiene los días
     * @return
     */
    private static ArrayList<Integer> daysFromStringToList(String daysString){
        ArrayList<Integer> choosenDays = new ArrayList<>();

        /**
         * Separa el String utilizando la constante de separación y parsea cada parte a entero,
         * guardándolo en el arraylist choosenDays
         */
        for(String s: daysString.split(DAY_SEPARATOR)){
            choosenDays.add(Integer.parseInt(s));
        }

        return choosenDays;
    }
}
