package sanjose.org.gestorperiodico.datamodels;

import java.io.Serializable;

/**
 * Created by juand on 09/01/2018.
 */

public class User implements Serializable{

    //Tipos de usuario
    public static final String CLIENT = "client";
    public static final String ADMIN = "admin";

    private final String password;

    private int user_id;
    private String typeUser;
    private String name, lastname;
    private String username;
    private String address;
    private int postalCode;
    private int userSuscriptionID;

    public User(int user_id, String username, String password, String name, String lastname, String address, int postalCode, String typeUser, int userSuscription) {
        this.user_id = user_id;
        this.name = name;
        this.lastname = lastname;
        this.username = username;
        this.password = password;
        this.address = address;
        this.postalCode = postalCode;
        this.typeUser = typeUser;
        this.userSuscriptionID = userSuscription;
    }

    public User(int user_id, String username, String password, String name, String lastname, String address, int postalCode, String typeUser) {
        this.user_id = user_id;
        this.name = name;
        this.lastname = lastname;
        this.username = username;
        this.password = password;
        this.address = address;
        this.postalCode = postalCode;
        this.typeUser = typeUser;
        //Valor por defecto para usuarios sin suscripción
        userSuscriptionID = -1;
    }

    public User(String username, String password){
        this.username = username;
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword(){ return password; }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(int postalCode) {
        this.postalCode = postalCode;
    }

    public String getTypeUser() {
        return typeUser;
    }

    public void setTypeUser(String typeUser) {
        this.typeUser = typeUser;
    }

    public int getUserSuscriptionID() {
        return userSuscriptionID;
    }

    public int getUser_id() {
        return user_id;
    }

    public boolean equals(User that) {
        return this.name.equals(that.name) &&
                this.lastname.equals(that.lastname) &&
                this.username.equals(that.username) &&
                this.password.equals(that.password) &&
                this.address.equals(that.address) &&
                this.postalCode==that.postalCode &&
                this.typeUser.equals(that.typeUser) &&
                this.userSuscriptionID==that.userSuscriptionID;
    }
}
