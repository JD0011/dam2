package sanjose.org.gestorperiodico.listviews;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import sanjose.org.gestorperiodico.BBDD.SQLiteUserManager;
import sanjose.org.gestorperiodico.R;
import sanjose.org.gestorperiodico.datamodels.Suscription;
import sanjose.org.gestorperiodico.datamodels.User;

/**
 * Created by juand on 17/01/2018.
 */

public class ClientListArrayAdapter extends ArrayAdapter {

    private ArrayList<User> users;
    private Activity ctx;

    public ClientListArrayAdapter(Activity ctx, ArrayList<User> users) {
        super(ctx, R.layout.listview_client_template, users);
        this.users = users;
        this.ctx = ctx;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = ctx.getLayoutInflater();
        View item = inflater.inflate(R.layout.listview_client_template, null);
        User choosenUser = users.get(position);

        TextView clientName = (TextView) item.findViewById(R.id.lbl_username);
        clientName.setText(choosenUser.getName() + " " + choosenUser.getLastname());

        TextView clientAddress = (TextView) item.findViewById(R.id.lbl_address);
        clientAddress.setText(choosenUser.getAddress());

        //Comprueba periódicos recibidos
        int numReceivedNewspapers;
        if(choosenUser.getUserSuscriptionID()==-1){
            //No se ha suscrito
            numReceivedNewspapers=0;
        }else{
            SQLiteUserManager sum = new SQLiteUserManager(ctx);
            Suscription userSuscription = sum.getSuscriptionByID(choosenUser.getUserSuscriptionID());
            numReceivedNewspapers = userSuscription.getDeliveredNewspapers();
        }

        TextView receivedNewspapers = (TextView) item.findViewById(R.id.lbl_received_newspapers_value);
        receivedNewspapers.setText(String.valueOf(numReceivedNewspapers));

        return item;
    }
}
