package sanjose.org.gestorperiodico.activity.main;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import org.joda.time.LocalDate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import sanjose.org.gestorperiodico.BBDD.SQLiteUserManager;
import sanjose.org.gestorperiodico.R;
import sanjose.org.gestorperiodico.activity.admin.AdminMainActivity;
import sanjose.org.gestorperiodico.activity.user.UserMainActivity;
import sanjose.org.gestorperiodico.datamodels.User;
import sanjose.org.gestorperiodico.utils.SuscriptionManager;

/**
 * Esta actividad se encarga del logeo del usuario en el sistema, proporcionando además, un acceso
 * al formulario de registro para nuevos usuarios.
 */
public class LoginActivity extends AppCompatActivity {

    private ArrayList<Button> buttons;
    private View.OnClickListener buttonsListener;
    private HashMap <String,EditText> edittexts;

    private ImageView ivBackground;
    private Thread tBackgroundChanger;

    private Context ctx;

    private int backgroundSelector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        ivBackground = (ImageView) findViewById(R.id.iv_background);
        ctx = LoginActivity.this;

        //Hilo que cambia el fondo de pantalla
        tBackgroundChanger = new Thread(new Runnable() {
            @Override
            public void run() {
                while(true){
                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    //Gnera color de forma aleatoria
                    Random rnd = new Random();
                    int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));

                    final int innerColor = color;
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            switch (backgroundSelector){
                                case 0:
                                    ivBackground.setBackground(getResources().getDrawable(R.drawable.newspaper_black_whie));
                                    backgroundSelector++;
                                    break;
                                case 1:
                                    ivBackground.setBackground(getResources().getDrawable(R.drawable.newspaper1_words));
                                    backgroundSelector++;
                                    break;
                                case 2:
                                    ivBackground.setBackground(getResources().getDrawable(R.drawable.newspaper2_page));
                                    backgroundSelector=0;
                                    break;
                            }
                        }
                    });

                }
            }
        });

        tBackgroundChanger.setName("BACKGROUND CHANGER THREAD");
        tBackgroundChanger.start();

        buttons = new ArrayList<Button>();
        edittexts = new HashMap<String, EditText>();

        buttons.add((Button) findViewById(R.id.btn_login));
        buttons.add((Button) findViewById(R.id.btn_register));

        edittexts.put("username",(EditText) findViewById(R.id.plaintext_username));
        edittexts.put("password",(EditText) findViewById(R.id.plaintext_password));

        SQLiteUserManager sum = new SQLiteUserManager(this);

        //SAMPLE DATA
        if(!sum.existsClientname("admin")){
            sum.setSuscriptionPricePerDay(2.5);
            //DATOS DE PRUEBA
            sum.inserSampleData();
        }

        //LISTENNER
        buttonsListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (view.getId()){
                    case R.id.btn_login:

                        //Comprueba que haya datos introducidos
                        if(!edittexts.get("username").getText().equals("") && !edittexts.get("password").getText().equals("")){
                            //Comprueba longitud del nombre de usuario
                            if(edittexts.get("username").length()>4){
                                User user = new User(
                                        edittexts.get("username").getText().toString(),
                                        edittexts.get("password").getText().toString()
                                );

                                SQLiteUserManager sum = new SQLiteUserManager(ctx);

                                //Comprobación de usuario
                                if(!sum.existsClientname(user.getUsername())){
                                    Toast.makeText(ctx, R.string.error_user_doesnt_exists, Toast.LENGTH_SHORT).show();
                                }else{
                                    if(!sum.checkPassword(user)){
                                        Toast.makeText(ctx, R.string.error_password_wrong, Toast.LENGTH_SHORT).show();
                                    }else{
                                        ProgressDialog loadingDialog = ProgressDialog.show(ctx,"",getResources().getString(R.string.msg_loading_data),true);

                                        //Carga los datos del usuario desde la base de datos
                                        User loggedUser = sum.getClientByUsername(user.getUsername());

                                        if(loggedUser ==null){
                                            Toast.makeText(ctx, R.string.error_user_couldnt_be_loaded, Toast.LENGTH_SHORT).show();
                                        }else{

                                            String msgWelcome = getResources().getString(R.string.msg_welcome) +" "+ loggedUser.getName();
                                            Toast.makeText(ctx, msgWelcome, Toast.LENGTH_SHORT).show();

                                            //Comprueba si es administrador
                                            Intent i;
                                            switch (loggedUser.getTypeUser()){
                                                case User.ADMIN:
                                                    i = new Intent(ctx, AdminMainActivity.class);
                                                    i.putExtra("admin", loggedUser);
                                                    startActivity(i);
                                                    break;
                                                case User.CLIENT:
                                                    i = new Intent(ctx, UserMainActivity.class);
                                                    i.putExtra("user", loggedUser);
                                                    startActivity(i);
                                                    break;
                                            }

                                            //Limpia los campos
                                            edittexts.get("username").setText("");
                                            edittexts.get("password").setText("");
                                        }
                                    }
                                }
                            }
                        }
                        break;

                    case R.id.btn_register:
                        //Limpia los campos
                        edittexts.get("username").setText("");
                        edittexts.get("password").setText("");

                        launchActivity(RegisterActivity.class);
                        break;
                }
            }
        };

        for(Button b: buttons){
            b.setOnClickListener(buttonsListener);
        }
    }

    private void launchActivity(Class clazz){
        Intent i = new Intent(this,clazz);
        startActivity(i);
    }
}
