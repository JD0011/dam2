package sanjose.org.gestorperiodico.activity.user;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import org.joda.time.LocalDate;

import java.util.ArrayList;
import java.util.HashMap;

import sanjose.org.gestorperiodico.BBDD.SQLiteUserManager;
import sanjose.org.gestorperiodico.activity.suscription.CreateSuscriptionActivity;
import sanjose.org.gestorperiodico.R;
import sanjose.org.gestorperiodico.datamodels.Fee;
import sanjose.org.gestorperiodico.datamodels.User;
import sanjose.org.gestorperiodico.datamodels.Suscription;
import sanjose.org.gestorperiodico.listviews.ClientFeeListActivity;
import sanjose.org.gestorperiodico.utils.SuscriptionManager;

/**
 * Ventana principal del cliente al conectarse
 */
public class UserMainActivity extends AppCompatActivity {

    private User user;
    private Suscription userSuscription;
    private TableLayout tlSuscriptionCalendar;
    private HashMap<String,TextView> textviews;
    private HashMap<String, Button> buttons;
    private View.OnClickListener buttonsListener;

    private Context ctx;
    private LinearLayout llSuscriptionInfo;

    //Hilo que se encarga de actualizar la interfaz cada 5 segundos
    private Thread tRefresher;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_main);

        ctx = this;

        textviews = new HashMap<String, TextView>();
        buttons = new HashMap<String, Button>();

        //Información de suscripción
        llSuscriptionInfo = (LinearLayout) findViewById(R.id.ll_suscription_info);

        //Botones
        buttons.put("manage_account",(Button) findViewById(R.id.btn_modify_account));
        buttons.put("change_suscription",(Button) findViewById(R.id.btn_change_suscription));
        buttons.put("subscribe_now", (Button) findViewById(R.id.btn_suscribe_now));
        buttons.put("check_fees", (Button) findViewById(R.id.btn_check_fees));

        //Datos de cliente
        textviews.put("username",(TextView)findViewById(R.id.lbl_username));
        textviews.put("name",(TextView)findViewById(R.id.lbl_name_value));
        textviews.put("lastname",(TextView)findViewById(R.id.lbl_lastname_value));
        textviews.put("address",(TextView)findViewById(R.id.lbl_address_value));
        textviews.put("postal_code",(TextView)findViewById(R.id.lbl_postal_code_value));
        textviews.put("bought_newspapers",(TextView)findViewById(R.id.lbl_bought_newspapers_value));
        textviews.put("antiquity",(TextView)findViewById(R.id.lbl_antiquity_value));

        //Días de suscripción
        tlSuscriptionCalendar = (TableLayout) findViewById(R.id.tl_calendar);

        //Factura mensual
        textviews.put("date_of_suscription",(TextView)findViewById(R.id.lbl_date_of_suscription_value));
        textviews.put("payment_day",(TextView)findViewById(R.id.lbl_payment_day_value));
        textviews.put("price_per_day",(TextView)findViewById(R.id.lbl_price_per_day_value));

        tRefresher = new Thread(new Runnable() {
            @Override
            public void run() {
                SQLiteUserManager sum = new SQLiteUserManager(ctx);
                User refreshedUser;
                while(true){
                    try {
                        Thread.sleep(5000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    synchronized (sum){
                        refreshedUser = sum.getClientByUsername(user.getUsername());
                    }

                    //Si no son iguales, actualizalo
                    if(!user.equals(refreshedUser)){
                        user = refreshedUser;
                    }
                }
            }
        });

        tRefresher.setName("CLIENT ACCOUNT REFRESHER");

        //LISTENER DE BOTONES
        buttonsListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (view.getId()){
                    case R.id.btn_suscribe_now:
                        launchActivity(CreateSuscriptionActivity.class);
                        break;
                    case R.id.btn_modify_account:
                        //Modifica cuenta
                        launchActivity(ModifyUserActivity.class);
                        break;
                    case R.id.btn_change_suscription:
                        launchActivity(CreateSuscriptionActivity.class);
                        break;
                    case R.id.btn_check_fees:
                        launchActivity(ClientFeeListActivity.class);
                        break;
                }
            }
        };

        for(Button b: buttons.values()){
            b.setOnClickListener(buttonsListener);
        }

        Bundle extras = getIntent().getExtras();

        if(extras !=null){
            if(extras.containsKey("user")){
                user = (User) extras.getSerializable("user");
            }
        }

        //Arranca el hilo actualizador
        tRefresher.start();

        SQLiteUserManager sum = new SQLiteUserManager(ctx);
        //Comprueba si tiene suscripción
        if(user.getUserSuscriptionID()==-1){
            buttons.get("subscribe_now").setVisibility(View.VISIBLE);
            llSuscriptionInfo.setVisibility(View.GONE);
        }else{
            buttons.get("subscribe_now").setVisibility(View.GONE);
            llSuscriptionInfo.setVisibility(View.VISIBLE);

            //Carga la suscripción
            userSuscription = sum.getSuscriptionByID(user.getUserSuscriptionID());
            loadClientStats();
            loadSuscriptionInfo();

            //ACTUALIZA DATOS DE LA SUSCRIPCIÓN
            SuscriptionManager smanager = new SuscriptionManager(ctx,user, userSuscription);
            smanager.recalculateDeliveredNewspapers();
            smanager.updateAntiquity();
            //Si es día de pago
            int a = LocalDate.now().getDayOfMonth();
            int b = userSuscription.getPaymentDate().getDayOfMonth();
            //Si no se ha creado ya la factura
            if(smanager.shouldCreateFee()){
                //Si es dia de pago
                if(LocalDate.now().getDayOfMonth()==userSuscription.getPaymentDate().getDayOfMonth()){
                    //Creala
                    smanager.createFeeForPastMonth();
                    Toast.makeText(ctx, R.string.msg_new_fee_created, Toast.LENGTH_LONG).show();
                }
            }

        }

        refreshClientData();
    }

    /**
     * Cierra la actividad principal y abre la especificada por parámetros
     * @param clazz
     */
    public void launchActivity(Class clazz){
        Intent i = new Intent(ctx, clazz);
        i.putExtra("user", user);
        startActivity(i);
    }

    /**
     * Rellena los textview's con la información del usuario
     */
    public void refreshClientData(){
        textviews.get("username").setText(getResources().getString(R.string.username)+": "+user.getUsername());
        textviews.get("name").setText(user.getName());
        textviews.get("lastname").setText(user.getLastname());
        textviews.get("address").setText(user.getAddress());
        textviews.get("postal_code").setText(String.valueOf(user.getPostalCode()));
    }

    /**
     * Obtiene la suscripción, si existe, y rellena los datos correspondientes
     */
    public void loadClientStats(){

        SQLiteUserManager sum = new SQLiteUserManager(this);

        textviews.get("bought_newspapers").setText(String.valueOf(userSuscription.getDeliveredNewspapers()));

        int antiquity = userSuscription.getAntiquity();

        //Si todavía no ha comenzado la suscripción (antiguedad en negativo), establece antiguedad en 0
        if(antiquity<0){
            antiquity = 0;
        }

        String antiquityMSG = String.valueOf(antiquity)+" "+ getResources().getString(R.string.day);

        textviews.get("antiquity").setText(antiquityMSG);
    }

    /**
     * Carga el calendario con las fechas elegidas según la suscripción del usuario
     */
    private void loadSuscriptionInfo(){
        ArrayList<Integer> choosenDays = userSuscription.getChoosenDays();

        ArrayList<TextView> dayBoxes = new ArrayList<>();
        for(int i = 0; i < tlSuscriptionCalendar.getChildCount();i++){
            if(tlSuscriptionCalendar.getChildAt(i) instanceof TableRow){

                TableRow currentRow = (TableRow) tlSuscriptionCalendar.getChildAt(i);
                for(int j = 0;j < currentRow.getChildCount(); j++){
                    if(currentRow.getChildAt(j) instanceof TextView){
                        dayBoxes.add((TextView)currentRow.getChildAt(j));
                    }
                }

            }
        }

        for(Integer d: choosenDays){
            dayBoxes.get(d).setBackgroundColor(Color.GREEN);
            dayBoxes.get(d).setTextColor(Color.BLACK);
        }
        SQLiteUserManager sum = new SQLiteUserManager(ctx);

        textviews.get("date_of_suscription").setText(userSuscription.getPaymentDate().toString(SQLiteUserManager.DATE_PATTERN));
        textviews.get("payment_day").setText(String.valueOf(userSuscription.getPaymentDate().getDayOfMonth()));
        textviews.get("price_per_day").setText(String.valueOf(sum.getSuscriptionPricePerDay())+" €");
    }

    //CONFIRMACIÓN PARA SALIR
    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setMessage(R.string.msg_are_you_sure_to_close)
                .setNegativeButton(R.string.lbl_no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                })
                .setPositiveButton(R.string.lbl_yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        System.exit(0);
                    }
                }).show();
    }
}
