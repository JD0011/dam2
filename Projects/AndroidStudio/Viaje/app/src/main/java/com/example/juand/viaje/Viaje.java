/**
 * Viaje: conjunto de radiobutton's y edittext's que deben actualizar el precio de forma dinámica.
 *
 * A tener en cuenta, los radiobuttons deben llamarse tal que así: transporte_propio. De esta manera, s
 * se reconocerá la categoria a la que pertenece.
 *
 * Se han instanciado dos Listener:
 *  · OnCheckedChangeListener para los radiogroup's de categoria.
 *  · TextWatcher para los edittext's de persona
 *
 * @author Juan de Dios Delgado Bermúdez
 * @since 19/10/2017
 */

package com.example.juand.viaje;

import android.support.annotation.IdRes;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

public class Viaje extends AppCompatActivity {

    private RadioGroup rgTransportes, rgHotel, rgComida, rgOcio;
    //Cantidad de personas
    private EditText etPersTransporte, etPersHotel, etPersComida, etPersOcio;
    //Precio del RadioButton elegido
    private double precioTransporte, precioHotel, precioComida, precioOcio;
    //Precio final
    private TextView tvPrecioFinal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_viaje);


        //Extrae RadioGroups
        rgTransportes = (RadioGroup) findViewById(R.id.transporte);
        rgHotel = (RadioGroup) findViewById(R.id.hotel);
        rgComida = (RadioGroup) findViewById(R.id.comida);
        rgOcio = (RadioGroup) findViewById(R.id.ocio_rgroup);
        tvPrecioFinal = (TextView) findViewById(R.id.precio_label);

        //EditText(inputType:Number) para número de personas
        etPersTransporte = (EditText) findViewById(R.id.transporte_personas);
        etPersOcio = (EditText) findViewById(R.id.ocio_personas);
        etPersComida = (EditText) findViewById(R.id.comida_personas);
        etPersHotel = (EditText) findViewById(R.id.hotel_personas);

        //LISTENER PARA LOS RADIOGROUP'S
        RadioGroup.OnCheckedChangeListener rgListener = new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {

                RadioButton rButton = (RadioButton) findViewById(checkedId);

                //Nombre del botón.Convención a tomar: <Categoría>_<NombreButton>
                String NombreIDButton = getResources().getResourceEntryName(checkedId);

                //Texto del RadioButton
                String contenido = rButton.getText().toString();

                //Extrae <categoría>
                switch (NombreIDButton.substring(0, NombreIDButton.indexOf('_'))) {
                    case "transporte":
                        precioTransporte = Double.parseDouble(contenido.substring(contenido.indexOf('-') + 2, contenido.length()));
                        break;
                    case "hotel":
                        precioHotel = Double.parseDouble(contenido.substring(contenido.indexOf('-') + 2, contenido.length()));
                        break;
                    case "comida":
                        precioComida = Double.parseDouble(contenido.substring(contenido.indexOf('-') + 2, contenido.length()));
                        break;
                    case "ocio":
                        precioOcio = Double.parseDouble(contenido.substring(contenido.indexOf('-') + 2, contenido.length()));
                        break;
                }

                actualizaPrecioFinal();
            }
        };

        //VIGILA EDITTEXT PERSONA
        TextWatcher personaWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            //En cada modificación del texto, actualiza el precio final
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                actualizaPrecioFinal();
            }

            @Override
            public void afterTextChanged(Editable s) {
            }

        };

        //Asigna TextWatcher a los EditText
        etPersHotel.addTextChangedListener(personaWatcher);
        etPersTransporte.addTextChangedListener(personaWatcher);
        etPersComida.addTextChangedListener(personaWatcher);
        etPersOcio.addTextChangedListener(personaWatcher);

        //Asigna OnCheckedChangeListener a los RadioGroup
        rgTransportes.setOnCheckedChangeListener(rgListener);
        rgHotel.setOnCheckedChangeListener(rgListener);
        rgComida.setOnCheckedChangeListener(rgListener);
        rgOcio.setOnCheckedChangeListener(rgListener);
    }

    public void actualizaPrecioFinal() {
        //Almacena totales
        double precioFinal, totalTransporte, totalHotel, totalComida, totalOcio;

        //VALOR POR DEFECTO (Sin personas, precioFinal = 0)
        totalTransporte = 0;
        totalHotel = 0;
        totalComida = 0;
        totalOcio = 0;

        /*
        Comprueba que los EditText tengan contenido antes de hacer el casting a int y multiplicar
        el precio por el número de personas
         */
        if (!etPersTransporte.getText().toString().equals("")) {
            totalTransporte = precioTransporte * Integer.parseInt(etPersTransporte.getText().toString());
        }
        if (!etPersHotel.getText().toString().equals("")) {
            totalHotel = precioHotel * Integer.parseInt(etPersHotel.getText().toString());
        }
        if (!etPersComida.getText().toString().equals("")) {
            totalComida = precioComida * Integer.parseInt(etPersComida.getText().toString());
        }
        if (!etPersOcio.getText().toString().equals("")) {
            totalOcio = precioOcio * Integer.parseInt(etPersOcio.getText().toString());
        }

        //Establece el precio final
        precioFinal = totalTransporte + totalHotel + totalComida + totalOcio;
        tvPrecioFinal.setText(String.valueOf(precioFinal));
    }
}