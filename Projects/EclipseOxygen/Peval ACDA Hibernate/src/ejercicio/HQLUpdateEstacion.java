package ejercicio;

import java.util.Iterator;

import javax.persistence.TypedQuery;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.mapping.Table;
import org.hibernate.query.Query;

import datamodels.TAccesos;
import datamodels.TEstaciones;

/**
 * EJERCICIO 5: Actualiza las estaciones por HQL. No se actualiza ninguna en contreto, sino todas
 * @author Juan de Dios Delgado Berm�dez
 *
 */
public class HQLUpdateEstacion {
	
	SessionFactory factory;

	public HQLUpdateEstacion() {
		factory = HibernateUtil.getSessionFactory();
	}
	
	/**
	 * Obtiene las estaciones de la tabla TEstaciones y actualiza sus campos en funci�n de las tablas
	 * relacionadas
	 */
	public void updateEstaciones() {
		Session session = factory.openSession();
		
		String hql = "FROM TEstaciones";
		TypedQuery<TEstaciones> tquery = session.createQuery(hql);
		
		Iterator<TEstaciones> itEstaciones = tquery.getResultList().iterator();
		while(itEstaciones.hasNext()) {
			Transaction tx = session.beginTransaction();
			TEstaciones estacion = itEstaciones.next();
			System.out.println("ID ESTACION "+estacion.getCodEstacion());
			estacion.setNumaccesos(getNumAccesosByEstacionID(estacion.getCodEstacion()));
			estacion.setNumaccesos(getNumLineasByEstacionID(estacion.getCodEstacion()));
			estacion.setNumaccesos(getNumViajesDestinoByEstacionID(estacion.getCodEstacion()));
			estacion.setNumaccesos(getNumViajeOrigenByEstacionID(estacion.getCodEstacion()));
			
			String hqlUpdate = 
					"UPDATE TEstaciones SET numaccesos=:naccesos, numlineas=:nlineas, numviajesprocedencia=:nvorigen, numviajesdestino=:nvdestino WHERE codEstacion=:cestacion";
			Query queryUpdate = session.createQuery(hqlUpdate);
			queryUpdate.setParameter("naccesos", estacion.getNumaccesos());
			queryUpdate.setParameter("nlineas", estacion.getNumlineas());
			queryUpdate.setParameter("nvorigen", estacion.getNumviajesprocedencia());
			queryUpdate.setParameter("nvdestino", estacion.getNumviajesdestino());
			queryUpdate.setParameter("cestacion", estacion.getCodEstacion());
			queryUpdate.executeUpdate();
			tx.commit();
			
			System.out.println("Estaci�n con c�digo "+estacion.getCodEstacion()+" actualizada correctamente");
		}
		
		session.close();
		
	}
	
	/**
	 * Devuelve el n�mero de accesos de la estaci�n especificada
	 * @param estacionID ID de la estaci�n a consultar
	 * @return N�mero de accesos
	 */
	public int getNumAccesosByEstacionID(int estacionID) {
		int numAccesos;
		Session session = factory.openSession();
		
		String hql ="SELECT COUNT(*) FROM TAccesos WHERE TEstaciones.codEstacion=:cod";
		TypedQuery<Long> tquery = session.createQuery(hql);
		tquery.setParameter("cod", estacionID);
		
		numAccesos = Integer.parseInt(tquery.getResultList().get(0).toString());
		session.close();
		System.out.println("N�mero de accesos: "+numAccesos);
		return numAccesos;
	}
	
	/**
	 * Devuelve el n�mero de l�neas que pasan por una estaci�n determinada
	 * @param estacionID ID de la estaci�n a consultar
	 * @return N�mero de l�neas
	 */
	public int getNumLineasByEstacionID(int estacionID) {
		int numLineas;
		Session session = factory.openSession();
		
		String hql ="SELECT COUNT(*) FROM TLineaEstacion WHERE TEstaciones.codEstacion=:cod";
		TypedQuery<Long> tquery = session.createQuery(hql);
		tquery.setParameter("cod", estacionID);
		
		numLineas = Integer.parseInt(tquery.getResultList().get(0).toString());
		session.close();
		System.out.println("N�mero de l�neas: "+numLineas);
		return numLineas;
	}
	
	/**
	 * Devuelve el n�mero de viajes de destino de la estaci�n especificada
	 * @param estacionID ID de la estaci�n a consultar
	 * @return N�mero de viajes de destino
	 */
	public int getNumViajesDestinoByEstacionID(int estacionID) {
		int numViajesDestino;
		Session session = factory.openSession();
		
		String hql ="SELECT COUNT(*) FROM TViajes WHERE TEstacionesByEstaciondestino.codEstacion=:cod";
		TypedQuery<Long> tquery = session.createQuery(hql);
		tquery.setParameter("cod", estacionID);
		numViajesDestino = Integer.parseInt(tquery.getResultList().get(0).toString());
		session.close();
		System.out.println("N�mero de viajes de destino: "+numViajesDestino);
		return numViajesDestino;
	}
	
	/**
	 * Devuelve el n�mero de viajes de procedencia de la estaci�n especificada
	 * @param estacionID ID de la estaci�n a consultar
	 * @return N�mero de viajes de procedencia
	 */
	public int getNumViajeOrigenByEstacionID(int estacionID) {
		int numViajesOrigen;
		Session session = factory.openSession();
		
		String hql ="SELECT COUNT(*) FROM TViajes WHERE TEstacionesByEstacionorigen.codEstacion=:cod";
		TypedQuery<Long> tquery = session.createQuery(hql);
		tquery.setParameter("cod", estacionID);
		
		numViajesOrigen = Integer.parseInt(tquery.getResultList().get(0).toString());
		session.close();
		System.out.println("N�mero de viajes de origen: "+numViajesOrigen);
		return numViajesOrigen;
	}
	
	public static void main(String[] args) {
		HQLUpdateEstacion u = new HQLUpdateEstacion();
		u.updateEstaciones();
	}
}
