package ejercicio;

import java.util.ArrayList;
import java.util.Iterator;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import datamodels.TEstaciones;
import datamodels.TViajes;

/**
 * EJERCICIO 06: Visualiza las estaciones.
 * @author Juan de Dios Delgado Berm�dez
 *
 */
public class VisualizadorEstaciones {
	
	private SessionFactory factory;
	
	public VisualizadorEstaciones() {
		factory = HibernateUtil.getSessionFactory();
	}
	
	/**
	 * Confecciona un String con la informaci�n de la estaci�n
	 * @return String con la informaci�n de la estaci�n
	 */
	public StringBuilder printEstaciones() {
		Session session = factory.openSession();
		Criteria cr = session.createCriteria(TEstaciones.class);
		
		Iterator<TEstaciones> itEstaciones = cr.list().iterator();
		StringBuilder sbuilder = new StringBuilder();
		while(itEstaciones.hasNext()) {
			TEstaciones estacion = itEstaciones.next();
			
			System.out.println("Recogiendo informaci�n de la estaci�n con c�digo: "+estacion.getCodEstacion());
			
			sbuilder.append(
					String.format("\n%s%5s%20s%10s", 
							"COD ESTACI�N",
							estacion.getCodEstacion(),
							"NOMBRE ESTACI�N",
							estacion.getNombre())
			);
			sbuilder.append("\nN�meros de l�neas que pasan: "+estacion.getNumlineas());
			sbuilder.append("\nN�mero de accesos que tiene: "+estacion.getTAccesoses().size());

			
			Criteria viajesDestino = session.createCriteria(TViajes.class);
			viajesDestino.add(Restrictions.like("TEstacionesByEstacionorigen.codEstacion", estacion.getCodEstacion()));
			
			int numViajesDestino = viajesDestino.list().size();
			sbuilder.append("\nNUM VIAJES DESTINO: "+numViajesDestino);
			
			if(numViajesDestino!=0) {
				sbuilder.append(String.format("\n%s%30s", "COD-VIAJE","NOMBRE VIAJE DESTINO"));
				Iterator<TViajes> itViajesDestino = viajesDestino.list().iterator();
				while(itViajesDestino.hasNext()) {
					TViajes viaje = itViajesDestino.next();
					sbuilder.append(String.format("\n%7s%25s", viaje.getCodViaje(), viaje.getNombre()));
				}
			}
			
			Criteria viajesProcedencia = session.createCriteria(TViajes.class);
			viajesProcedencia.add(Restrictions.like("TEstacionesByEstacionorigen.codEstacion", estacion.getCodEstacion()));
			
			int numViajesProcedencia = viajesProcedencia.list().size();
			sbuilder.append("\nNUM VIAJES PROCEDENCIA: "+numViajesProcedencia);
			
			if(numViajesProcedencia!=0) {
				sbuilder.append(String.format("\n%s%30s", "COD-VIAJE","NOMBRE VIAJE PROCEDENCIA"));
				Iterator<TViajes> itViajesProcedencia = viajesProcedencia.list().iterator();
				while(itViajesProcedencia.hasNext()) {
					TViajes viaje = itViajesProcedencia.next();
					sbuilder.append(String.format("\n%7s%25s", viaje.getCodViaje(), viaje.getNombre()));
				}
			}
			
			sbuilder.append("\n\n--------------------------------------------------------------\n");
		}
		
		session.close();
		
		return sbuilder;
	}
	
	public static void main(String[] args) {
		VisualizadorEstaciones visualizador = new VisualizadorEstaciones();
		System.out.println(visualizador.printEstaciones());
	}

}
