package ejercicio;


import java.util.Scanner;

import javax.persistence.TypedQuery;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.jboss.logging.LoggerProvider;

import datamodels.TEstaciones;
import datamodels.TLineaEstacion;
import datamodels.TLineaEstacionId;
import datamodels.TLineas;

/**
 * Engloba todos los ejercicios requeridos por la pr�ctica evaluable. Dentro del m�todo main
 * vienen los m�todos utilizados para cada ejercicio presentados en un men�
 * @author Juan de Dios Delgado Berm�dez
 *
 */
public class InsertLineaEstacion {
	
	SessionFactory factory;
	
	public InsertLineaEstacion() {
		factory = HibernateUtil.getSessionFactory();
	}
	
	/**
	 * Inserta un nuevo registro en la tabla linea estaci�n
	 * @param lineaEstacion Registro a grabar en la tabla
	 * @return True si se ha grabado el registro, False de lo contrario
	 */
	public void insertLineaEstacion(TLineaEstacion lineaEstacion) {
		Session session = factory.openSession();
		Transaction tx = session.beginTransaction();
		
		session.save(lineaEstacion);
		tx.commit();
		session.close();
	}
	
	/**
	 * Comprueba si existe la l�nea a partir del ID especificado
	 * @param idLinea ID de la l�nea a comprobar
	 * @return True si existe, False de lo contrario
	 */
	public TLineas getLinea(int idLinea) {
		TLineas linea;
		Session session = factory.openSession();
		
		linea = session.get(TLineas.class, (int)idLinea);
		session.close();
		
		return linea;
	}
	
	/**
	 * Comprueba si existe la estaci�n a partir del ID especificado
	 * @param idEstacion ID de la estaci�n a comprobar
	 * @return True si existe, False de lo contrario
	 */
	public TEstaciones getEstacion(int idEstacion) {
		TEstaciones estacion;
		Session session = factory.openSession();
		
		estacion = session.get(TEstaciones.class, (int)idEstacion);
		session.close();
		
		return estacion;
	}
	
	/**
	 * Comprueba si existe la compinaci�n de codLinea y codEstacion
	 * @return El �ltimo orden registrado en la tabla linea estaci�n
	 */
	public boolean existsComposedKey(int codLinea, int codEstacion) {
		TLineaEstacion lineaEstacion;
		boolean exists;
		Session session = factory.openSession();
		
		Criteria cr = session.createCriteria(TLineaEstacion.class);
		cr.add(Restrictions.like("id.codLinea", codLinea));
		cr.add(Restrictions.like("id.codEstacion", codEstacion));
		
		//Si existe, no se puede a�adir un nuevo registro con este conjunto de claves
		exists = cr.list().size()==1;
		session.close();
		
		return exists;
	}
	
	/**
	 * Devuelve el �ltimo orden disponible para la l�nea especificada
	 * @param numLinea
	 * @return �ltimo orden registrado
	 */
	public int lastOrden(int numLinea) {
		TLineaEstacion lineaEstacion;
		int last;
		Session session = factory.openSession();
		
		Criteria cr = session.createCriteria(TLineaEstacion.class);
		cr.add(Restrictions.like("id.codLinea", numLinea));
		cr.addOrder(Order.desc("orden"));
		
		//Si existe, no se puede a�adir un nuevo registro con este conjunto de claves
		
		if(cr.list().size()==0) {
			last = 0;
		}else {
			lineaEstacion =(TLineaEstacion) cr.list().get(0);
			last = lineaEstacion.getOrden();
		}
		
		session.close();
		
		return last;
	}

	/**
	 * Muestra el men� que gestiona la creaci�n de un registro para la tabla LineaEstacion
	 */
	public void showMenu() {
		Scanner sc;
		TLineas linea;
		TEstaciones estacion;
		int codLinea, codEstacion, numOrden;
		
		sc = new Scanner(System.in);
		
		System.out.print("Introduce el c�digo de l�nea -> ");
		try {
			codLinea = sc.nextInt();
		}catch (Exception e) {
			System.out.println("Error. Debe introducir un tipo num�rico");
			return;
		}
		
		if((linea = getLinea(codLinea))==null) {
			System.out.println("La l�nea introducida no existe");
			return;
		}
		
		System.out.print("Introduce el c�digo de estaci�n -> ");
		try {
			codEstacion = sc.nextInt();
		}catch (Exception e) {
			System.out.println("Error. Debe introducir un tipo num�rico");
			return;
		}
		
		if((estacion = getEstacion(codEstacion))==null) {
			System.out.println("La estaci�n introducida no existe");
			return;
		}
		
		//Comprueba conjunto de claves
		if(existsComposedKey(codLinea, codEstacion)) {
			System.out.println("Error. Ya existe un registro con este conjunto de claves");
			return;
		}else {
			int lastOrden = lastOrden(codLinea);
			
			System.out.print("Introduce el N� de orden(Orden que corresponde: "+ (lastOrden+1) +") ->");
			try {
				numOrden = sc.nextInt();
			}catch (Exception e) {
				System.out.println("Error. Debe introducir un tipo num�rico");
				return;
			}
			
			//Comprueba si el orden introducido es correcto
			if(lastOrden+1!=numOrden) {
				System.out.println(
						"Error. El siguiente orden debe ser : "+ (lastOrden+1)
				);
			}else {
				
				System.out.println("Los datos introducidos son correctos. Grabando registro en LineaEstacion");
				
				TLineaEstacion lineaEstacion = new TLineaEstacion();
				lineaEstacion.setId(new TLineaEstacionId(codLinea, codEstacion));
				lineaEstacion.setTLineas(linea);
				lineaEstacion.setTEstaciones(estacion);
				lineaEstacion.setOrden(numOrden);
				insertLineaEstacion(lineaEstacion);
			}
		}
	}
}
