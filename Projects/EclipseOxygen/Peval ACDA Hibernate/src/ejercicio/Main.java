package ejercicio;

import java.util.Scanner;

import datamodels.TEstaciones;
import datamodels.TLineaEstacion;
import datamodels.TLineaEstacionId;
import datamodels.TLineas;

public class Main {
	
	public static void main(String[] args) {
		InsertLineaEstacion gestacion = new InsertLineaEstacion();
		while(true) {
			
			System.out.println("Bienvenido a la aplicaci�n para la PEVAL de ACDA");
			
			System.out.println("Elije una opci�n");
			System.out.println(
					"1. EJ02: Introducir un registro en LineaEstacion. \n"
					+ "2. EJ03: Actualizar estaciones. \n"
					+ "3. EJ04: Visualizador de estaciones. \n"
					+ "4. EJ05: Actualizar estaciones (HQL). \n"
					+ "5. EJ06: Visualizador de estaciones (HQL). \n"
					+ "6. Salir.");
			Scanner sc = new Scanner(System.in);
			
			int answer;
			try {
				answer = sc.nextInt();
			}catch (Exception e) {
				System.out.println("Debe introducir un n�mero entre 1 y 6 (inclusive)");
				answer = 0;
			}
			switch (answer) {
			case 1:
				new InsertLineaEstacion().showMenu();
				break;
				
			case 2:
				sc = new Scanner(System.in);
				System.out.println("Todas las estaciones se est�n actualizando");
				new UpdaterEstacion().updateEstaciones();
				break;
				
			case 3:
				System.out.println("Visualizando las estaciones");
				System.out.println(new VisualizadorEstaciones().printEstaciones());
				break;

			case 4:
				System.out.println("Todas las estaciones se est�n actualizando mediante HQL");
				new HQLUpdateEstacion().updateEstaciones();
				break;
				
			case 5:
				System.out.println("Visualizando las estaciones mediante HQL");
				System.out.println(new HQLVisualizadorEstaciones().printEstaciones());
				break;
				
			case 6:
				System.exit(0);
				break;
				
			default:
				break;
			}
		}
	}
}
