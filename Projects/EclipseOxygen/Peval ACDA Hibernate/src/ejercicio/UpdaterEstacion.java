package ejercicio;


import java.util.Iterator;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projection;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import datamodels.TAccesos;
import datamodels.TEstaciones;
import datamodels.TLineaEstacion;
import datamodels.TViajes;

/**
 * EJERCICIO 5: Actualiza las estaciones. No se actualiza ninguna en contreto, sino todas
 * @author Juan de Dios Delgado Berm�dez
 *
 */
public class UpdaterEstacion {
	
	private SessionFactory factory;
	
	public UpdaterEstacion() {
		factory = HibernateUtil.getSessionFactory();
	}
	
	/**
	 * Obtiene las estaciones de la tabla TEstaciones y actualiza sus campos en funci�n de las tablas
	 * relacionadas
	 */
	public void updateEstaciones() {
		Session session = factory.openSession();
		
		Criteria cr = session.createCriteria(TEstaciones.class);
		Iterator<TEstaciones> itEstaciones = cr.list().iterator();
		
		while(itEstaciones.hasNext()) {
			TEstaciones estacion = itEstaciones.next();
			int estacionID = estacion.getCodEstacion();
			
			System.out.println("Actualizando campos de estaci�n con ID: "+estacion.getCodEstacion());
			
			estacion.setNumaccesos(getNumElement(TAccesos.class, "TEstaciones.codEstacion", estacionID));
			estacion.setNumlineas(getNumElement(TLineaEstacion.class, "TEstaciones.codEstacion", estacionID));
			estacion.setNumviajesdestino(getNumElement(TViajes.class, "TEstacionesByEstaciondestino.codEstacion", estacionID));
			estacion.setNumviajesprocedencia(getNumElement(TViajes.class, "TEstacionesByEstacionorigen.codEstacion", estacionID));
			
			Transaction tx = session.beginTransaction();
			session.update(estacion);
			tx.commit();
			
			System.out.println("Estaci�n con c�digo "+estacion.getCodEstacion()+" actualizada correctamente");
		}
		
		session.close();
	}

	/**
	 * Obtiene el n�mero de registros que contienen la clave especificada
	 * @param source Tabla desde la que se consultan los datos (Se usa la clase generada)
	 * @param keyField Campo que se utiliza para cribar los registros
	 * @param estacionID Valor que se busca en el campo elegido
	 * @return N�mero de registros de la tabla especificada
	 */
	public int getNumElement(Class source, String keyField, int estacionID) {
		int numElements;
		Session session = factory.openSession();
		
		Criteria cr = session.createCriteria(source)
						.add(Restrictions.like(keyField, estacionID))
						.setProjection(Projections.rowCount());
		
		numElements = Integer.parseInt(cr.uniqueResult().toString());
		session.close();
		
		System.out.println("N�mero de elementos para "+source.getSimpleName()+": "+numElements);
		return numElements;
	}
	
	public static void main(String[] args) {
		UpdaterEstacion u = new UpdaterEstacion();
		u.updateEstaciones();
	}

}
