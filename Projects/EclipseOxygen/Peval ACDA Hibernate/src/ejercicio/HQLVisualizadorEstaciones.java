package ejercicio;

import java.util.Iterator;

import javax.persistence.TypedQuery;

import org.hibernate.Criteria;
import org.hibernate.query.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import datamodels.TEstaciones;
import datamodels.TViajes;

/**
 * EJERCICIO 06: Visualiza las estaciones usando HQL
 * @author user
 *
 */
public class HQLVisualizadorEstaciones {
	
	SessionFactory factory;

	public HQLVisualizadorEstaciones() {
		factory = HibernateUtil.getSessionFactory();
	}
	
	/**
	 * Confecciona un String con la informaci�n de la estaci�n
	 * @return String con la informaci�n de la estaci�n
	 */
	public StringBuilder printEstaciones() {
		Session session = factory.openSession();
		String hqlSelect = "FROM TEstaciones";
		TypedQuery<TEstaciones> query = session.createQuery(hqlSelect);
		
		Iterator<TEstaciones> itEstaciones = query.getResultList().iterator();
		StringBuilder sbuilder = new StringBuilder();
		while(itEstaciones.hasNext()) {
			TEstaciones estacion = itEstaciones.next();
			
			System.out.println("Recogiendo informaci�n de la estaci�n con c�digo: "+estacion.getCodEstacion());
			
			sbuilder.append(
					String.format("\n%s%5s%20s%10s", 
							"COD ESTACI�N",
							estacion.getCodEstacion(),
							"NOMBRE ESTACI�N",
							estacion.getNombre())
			);
			sbuilder.append("\nN�meros de l�neas que pasan: "+estacion.getNumlineas());
			sbuilder.append("\nN�mero de accesos que tiene: "+estacion.getTAccesoses().size());

			
			String hqlViajesDestino = "FROM TViajes WHERE TEstacionesByEstaciondestino.codEstacion=:cestacion";
			Query queryViajesDestino = session.createQuery(hqlViajesDestino);
			queryViajesDestino.setParameter("cestacion", estacion.getCodEstacion());
			
			sbuilder.append("\nNUM VIAJES DESTINO: "+queryViajesDestino.getResultList().size());
			sbuilder.append(String.format("\n%s%30s", "COD-VIAJE","NOMBRE VIAJE DESTINO"));
			Iterator<TViajes> itViajesDestino = queryViajesDestino.getResultList().iterator();
			while(itViajesDestino.hasNext()) {
				TViajes viaje = itViajesDestino.next();
				sbuilder.append(String.format("\n%7s%25s", viaje.getCodViaje(), viaje.getNombre()));
			}
			
			
			String hqlViajesOrigen = "FROM TViajes WHERE TEstacionesByEstacionorigen.codEstacion=:cestacion";
			Query queryViajesOrigen = session.createQuery(hqlViajesOrigen);
			queryViajesOrigen.setParameter("cestacion", estacion.getCodEstacion());
			
			sbuilder.append("\nNUM VIAJES PROCEDENCIA: "+queryViajesOrigen.getResultList().size());
			sbuilder.append(String.format("\n%s%30s", "COD-VIAJE","NOMBRE VIAJE PROCEDENCIA"));
			Iterator<TViajes> itViajesProcedencia = queryViajesOrigen.getResultList().iterator();
			while(itViajesProcedencia.hasNext()) {
				TViajes viaje = itViajesProcedencia.next();
				sbuilder.append(String.format("\n%7s%25s", viaje.getCodViaje(), viaje.getNombre()));
			}
			
			sbuilder.append("\n\n--------------------------------------------------------------\n");
		}
		
		session.close();
		
		return sbuilder;
	}
	
	public static void main(String[] args) {
		System.out.println(new HQLVisualizadorEstaciones().printEstaciones());
	}
}
