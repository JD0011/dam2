package datamodels;
// Generated 31-ene-2018 12:09:24 by Hibernate Tools 5.2.8.Final

/**
 * TViajes generated by hbm2java
 */
public class TViajes implements java.io.Serializable {

	private Integer codViaje;
	private TEstaciones TEstacionesByEstacionorigen;
	private TEstaciones TEstacionesByEstaciondestino;
	private String nombre;

	public TViajes() {
	}

	public TViajes(TEstaciones TEstacionesByEstacionorigen, TEstaciones TEstacionesByEstaciondestino, String nombre) {
		this.TEstacionesByEstacionorigen = TEstacionesByEstacionorigen;
		this.TEstacionesByEstaciondestino = TEstacionesByEstaciondestino;
		this.nombre = nombre;
	}

	public Integer getCodViaje() {
		return this.codViaje;
	}

	public void setCodViaje(Integer codViaje) {
		this.codViaje = codViaje;
	}

	public TEstaciones getTEstacionesByEstacionorigen() {
		return this.TEstacionesByEstacionorigen;
	}

	public void setTEstacionesByEstacionorigen(TEstaciones TEstacionesByEstacionorigen) {
		this.TEstacionesByEstacionorigen = TEstacionesByEstacionorigen;
	}

	public TEstaciones getTEstacionesByEstaciondestino() {
		return this.TEstacionesByEstaciondestino;
	}

	public void setTEstacionesByEstaciondestino(TEstaciones TEstacionesByEstaciondestino) {
		this.TEstacionesByEstaciondestino = TEstacionesByEstaciondestino;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

}
