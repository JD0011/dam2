
package practica1;
/**
 * Equivale a la clase Tuber�a, en la que se vuelcan los datos obtenidos.
 * 
 * @author Juan de Dios Delgado Berm�dez
 */
public class Tablero {
	
	private int _intMaxTiradas, _intNumObjetivo, _intTama�oDado, _intNumJugadores;
	

	public Tablero( int intMaxTiradas, int intNumObjetivo, int intTama�oDado) {
		_intMaxTiradas = intMaxTiradas;
		_intNumObjetivo = intNumObjetivo;
		_intTama�oDado = intTama�oDado;
	}
	
	public int getMaxTiradas() {
		return _intMaxTiradas;
	}

	public int getNumObjetivo() {
		return _intNumObjetivo;
	}

	public int getTama�oDado() {
		return _intTama�oDado;
	}
	
	public void addJugador() {
		_intNumJugadores++;
	}

	public int getNumJugadores() {
		return _intNumJugadores;
	}
	
	

}
