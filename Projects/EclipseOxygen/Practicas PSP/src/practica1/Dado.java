package practica1;

import java.util.Random;

/**
 * Como tal, es el instrumento que utilizar�n los jugadores.
 * Almacena el rango de n�meros que contiene el dado y posee m�todos para tirarlo.
 * 
 * @author Juan de Dios Delgado Berm�dez
 *
 */
public class Dado {
	
	//VARS
	
	//Tama�o del dado
	private int _intTamDado;
	private int _intResultado; 
	
	//CONSTRUCTOR

	public Dado(int intTamDado) {
		_intTamDado = intTamDado;
	}
	
	//GETTERS

	public int getRange() {
		return _intTamDado;
	}
	
	public int getResultado() {
		return _intResultado;
	}
	
	//METHODS
	
	/**
	 * Se encarga de generar un resultado aleatorio que devuelve un n�mero
	 * en funci�n del tama�o del dado establecido
	 */
	public void lanzar() {		
		caidaDado();
		//Genera un n�mero aleatorio entre 1 y el tama�o del dado
		_intResultado = (int)(Math.random()*(_intTamDado+1 - 1) + 1);
	}
	
	/**
	 * Pausa para esperar a que el dado termine de caer
	 */
	private void caidaDado() {
		try {
			Thread.sleep((long) (Math.random()*(3000L-1000L)+1000L));
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
