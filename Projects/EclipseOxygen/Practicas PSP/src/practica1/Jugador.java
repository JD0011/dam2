package practica1;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.chrono.ChronoPeriod;

/**
 * Representa al jugador que se encargar� de tirar el dado.
 * 
 * Almacena informaci�n con respecto al nombre del jugador, la partida en la que participa,
 * el dado que utiliza e informaci�n relacionada con la tirada.
 * 
 * @author Juan de Dios Delgado Berm�dez
 * 
 */
public class Jugador extends Thread implements Comparable{
	
	//VARS
	
	private Partida _ptPartidaActual;
	private String _strNombre;
	private Dado _ddDado;
	//Tirada actual del jugador
	private int _intNumTirada;
	//N�mero objetivo del dado
	private int _intNumObjetivo;
	//M�ximas tiradas
	private int _intMaxTiradas;
	//Almacenamiento de resultados
	private int[] _intResultados;
	//Tiempo tardado en jugar
	private Duration _dtTiempoJugadas;
	
	//CONSTRUCTOR
	
	public Jugador(String strID, Tablero tblJuego, Partida ptPartida) {
		
		_strNombre = strID;
		
		//A�ade un jugador al tablero
		tblJuego.addJugador();
		
		//Obten informaci�n del tablero y guardala
		_ptPartidaActual = ptPartida;
		_intMaxTiradas = tblJuego.getMaxTiradas();
		_intNumObjetivo = tblJuego.getNumObjetivo();
		
		//Establece el array de resultados del tama�o de m�ximas tiradas
		_intResultados = new int[tblJuego.getMaxTiradas()];
		
		//Inicializa el dado con el tama�o proporcionado por el tablero		
		_ddDado = new Dado(tblJuego.getTama�oDado());
		
	}
	
	//GETTERS
	
	public int[] getResultados() {
		return _intResultados;
	}
	
	public int getResCorrecto() {
		int intCounter = 0;
		for(int result : _intResultados) {
			if(result == _intNumObjetivo) {
				intCounter++;
			}
		}
		return intCounter;
	}

	public String getNombre() {
		return _strNombre;
	}
	
	public Duration getTiempoJugadas() {
		return _dtTiempoJugadas;
	}
	
	//METHODS	

	/**
	 * Se encarga de tirar el dado para que se genere un resultado
	 * y contabilizar la tirada.
	 */
	public void tirarDado() {
		//Aumenta tirada
		_intNumTirada++;
		_ddDado.lanzar();		
	}
	
	/**
	 * Almacena el resultado en el array de resultados en la posici�n que est� libre
	 * @param intResultado Resultado obtenido
	 */
	private void grabarResultado(int intResultado) {
		for(int i = 0;i<_intResultados.length;i++) {
			
			if (_intResultados[i] == 0) {
				_intResultados[i] = intResultado;
				
				//Una vez a�adido, sal
				return;
			}
		}
	}
	
	/**
	 * Pausa aleatoria entre 1 y 3 segundos para que el jugador pueda recoger el dado 
	 */
	private void recogeDado() {
		try {
			sleep((long) (Math.random()*(3000L-1000L)+1000L));
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Muestra el resultado obtenido en una tirada determinada
	 */
	public void mostrarResultado() {
		System.out.println(
				"Jugador: "+ _strNombre + 
				" Tirada: " + _intNumTirada + 
				" Resultado: "+ _intResultados[_intNumTirada-1]+
				" N�mero de 6's: "+getResCorrecto());
	}
	
	@Override
	public void run() {
		//Controla tiempo de juego
		LocalDateTime startPlay;
		
		//Inicializa el n�mero de tirada actual
		_intNumTirada = 0;
		
		startPlay = LocalDateTime.now();
		//Tira el N� de veces especificado
		do {	
			tirarDado();
			
			grabarResultado(_ddDado.getResultado());
			
			mostrarResultado();
			
			recogeDado();
		}while(_intNumTirada < _intMaxTiradas);
		
		//Contabiliza las jugadas
		_dtTiempoJugadas = Duration.between(startPlay, LocalDateTime.now());
		
		//Notifica que acaba
		System.out.println("�El jugador "+_strNombre+" ha terminado de tirar el dado!");
		//Una vez acabado, notifica a la tuberia
		_ptPartidaActual.notificarFinJugadas(this);
		
	}

	@Override
	public int compareTo(Object o) {
		// TODO Auto-generated method stub
		return 0;
	}

}
