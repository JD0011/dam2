/**
 * Funcionamiento del programa
 *  1. Se define un tablero con unas normas establecidas donde los jugadores van a participar.
 *  2. Se instancia una nueva partida (tuber�a) que decidir� quien es el ganador mediante ciertas comprobaciones
 *  3. Se instancian los jugadores y se les dice el tablero que utilizaran y la partida en la que participan, 
 *  	pasandolas por par�metros al constructor.
 *  4. Se inicia la partida
 *  5. Se muestran los resultados
 */
package practica1;

public class MainClass {
	
	public static void main(String[] args) {
		
		//Se crea el tablero con las normas correspondientes al juego de dados
		Tablero tableroDados = new Tablero(20, 6, 6); //20 tiradas / N�m. objetivo: 6 / Tama�o dado: 6
		
		//Se instancia una nueva partida
		Partida primeraPartida = new Partida(tableroDados);
		
		//Se crean los participantes, a los que se les pasa por par�metros la partida y el tablero donde jugaran
		Jugador[] jdrJugadores= {
				new Jugador("Juan", tableroDados, primeraPartida),
				new Jugador("Pepe",tableroDados,primeraPartida),
				new Jugador("Ana",tableroDados,primeraPartida),
				new Jugador("Rosa", tableroDados, primeraPartida)
		};
		
		//Los jugadores comienzan a jugar
		
		for(Jugador pj: jdrJugadores) {
			pj.start();
		}
		
		
	}

}
