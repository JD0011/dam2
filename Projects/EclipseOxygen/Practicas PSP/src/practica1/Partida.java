package practica1;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.concurrent.TimeUnit;

/**
 * Representa la tuber�a que intercomunica a los jugadores y utiliza el tablero que contiene las normas
 * del juego (tama�o dado, m�ximo tuber�as)
 * 
 * @author juand
 *
 */
public class Partida {
	
	private ArrayList<Jugador> _jdrFinalizados;
	private ArrayList<Jugador> _jdrOrdenados;
	private Tablero _tblGame;
	private Jugador winner;
	
	
	public Partida(Tablero tblGame) {
		_tblGame = tblGame;
		_jdrFinalizados= new ArrayList<>();
	}
	
	/**
	 * A�ade el jugador que ha terminado al ArrayLit de jugadores que han terminado,
	 * y si todos ellos han finalizado, comprueba quien ha ganado
	 * 
	 * @param jdr Jugador que ha terminado
	 */
	public void notificarFinJugadas(Jugador jdr) {
		
		//A�adelo a la lista de jugadores que han acabado
		_jdrFinalizados.add(jdr);
		
		//Si ya han terminado todos los jugadores, comprueba quien ha ganado
		if(_jdrFinalizados.size() == _tblGame.getNumJugadores()) {
			ordenaJugadores();
			
			imprimeGanador();
			
			imprimirTablaResultados();
		}
	}
	
	/**
	 * Se encarga de ordenar los jugadores.
	 */
	public void ordenaJugadores() {
		
		_jdrOrdenados = _jdrFinalizados;
		
		/*Ordena a los jugadores en funci�n de los resultados correctos. Si hay empate, entonces
		 * ordenalos seg�n el timpo que hayan tardado en finalizar
		 */
		Collections.sort(_jdrOrdenados, 
					Comparator
					.comparing(Jugador::getResCorrecto).reversed()
					.thenComparing(Jugador::getTiempoJugadas));	
		
		winner = _jdrOrdenados.get(0);
		
	}
	
	/**
	 * Imprime el ganador de la partida
	 */
	public void imprimeGanador() {
		
		System.out.println("\n"+
				"El ganador es "+winner.getNombre()+
				", con "+winner.getResCorrecto()+
				" resultados correctos");
	}
	
	/**
	 * Imprime una tabla con los resultados de todos los jugadores
	 */
	public void imprimirTablaResultados() {
		
		int intPosition;
		long lngSeconds;
		
		System.out.println("\nTabla de resultados");
		System.out.println(String.format("%1$5s%2$9s%3$15s%4$17s","Puesto", "Nombre","N�mero de 6's","Tiempo empleado"));
		
		intPosition= 0;
		for(Jugador a: _jdrFinalizados) {
			
			lngSeconds = a.getTiempoJugadas().toMillis();
			
			System.out.println(
					//Formato de tabla
					String.format("%1$3d%2$11s%3$10d%4$12s.%5$s seg",
							++intPosition,
							a.getNombre(),
							a.getResCorrecto(),
							a.getTiempoJugadas().getSeconds(),
							(lngSeconds%1000)/10));
		}
	}
}
