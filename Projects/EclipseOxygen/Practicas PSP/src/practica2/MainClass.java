package practica2;

/**
 * Simular� el siguiente escenario:
 * 	� Lavadero con capacidad para 10 coches.
 * 	� 20 coches se disponen a utilizar los servicios del lavadero.
 * 
 * @author Juan de Dios Delgado Berm�dez
 * @since 25/10/2017
 *
 */
public class MainClass {
	
	public static void main(String[] args) {
		Lavadero lavaderoSanJose;
		Coche[] coches;
		
		lavaderoSanJose = new Lavadero(10); //Capacidad -> 10 coches
		
		//Instancia 20 coches
		coches = new Coche[20];
		
		for(int i=0;i<coches.length;i++) {
			coches[i]=new Coche(lavaderoSanJose,i+1);
			coches[i].start();
		}		
	}
	
}
