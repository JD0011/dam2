package practica2;

/**
 * Representa el veh�culo que se lavar� en el objeto Lavadero. 
 * Se sobreentiende que el due�o del Coche lo aparca en el lavadero, y cuando se termina 
 * de limpiar / encerar lo recoge, por lo que obviamos la creaci�n de la clase Cliente.
 * 
 * @author Juan de Dios Delgado Berm�dez
 * @since 25/10/2017
 *
 */
public class Coche extends Thread{
	
	//VARS
	
	private Lavadero lavadero;
	
	//CONSTRUCTOR
	public Coche(Lavadero lavaderoSanJose, int ID) {
		this.lavadero = lavaderoSanJose;
		setName("Coche "+String.valueOf(ID));
	}
	
	//M�TODOS
	
	@Override
	public void run() {
		//Llegan al lavadero
		try {
			sleep((long)((Math.random()*(20-2)+2)*1000));
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(lavadero.entrarParking(this)) {
			
			lavadero.aparcarCoche(this);
			
			lavadero.procesarCoche(this);
			
			lavadero.sacarCoche(this);
		}
		
		//Me voy...
		
	}
}
