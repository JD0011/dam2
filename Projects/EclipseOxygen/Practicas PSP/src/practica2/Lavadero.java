package practica2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.concurrent.atomic.AtomicIntegerArray;

/** TUBER�A
 * Ofrece servicios como lavado y encerado del veh�culo. Posee una capacidad
 * determinada, y se encarga de administrar las salidas / entradas de veh�culos. 
 * 
 * @author Juan de Dios Delgado Berm�dez
 * @since 25/10/2017
 *
 */
public class Lavadero {

	//VARS
	
	// Lista de coches a limpiar
	private ArrayList<Coche> listaTrabajo;
	//Contador
	private int cochesEnLavadero;
	private Coche[] plazas;
	
	
	
	//CONSTRUCTOR
	
	public Lavadero(int capacidad) {
		plazas = new Coche[capacidad];
		
		listaTrabajo= new ArrayList<Coche>();
		
		cochesEnLavadero=0;
	}
	
	//M�TODOS
	
	/**
	 * Comprueba qe hay plazas disponibles antes de que el coche entre al parking (Cartel indicando plazas libres).
	 * @param coche veh�culo que quiere entrar
	 * @return permiso para entrar o no al lavadero
	 */
	public boolean entrarParking(Coche coche) {
		
		boolean entra;
		
		System.out.println(coche.getName()+" ha llegado al lavadero.");
		
		if(cochesEnLavadero==10) {
			entra = false;
			System.out.println("El lavadero esta lleno," +coche.getName()+ " vuelva en otro momento");
			
		}else {
			entra = true;
		}
		
		return entra;
	}
	
	/**
	 * Asigna una plaza a un coche determinado. Pregunta al usuario en qu� plaza desea aparcar. Si esta est� ocupada,
	 * vuelve a preguntarle hasta que encuentre la que est� libre.
	 * @param coche
	 */
	public void aparcarCoche(Coche coche) {
		int plazaElegida;
		
		plazaElegida = elegirPlaza();
		
		synchronized (plazas) {
			plazas[plazaElegida] = coche;
			System.out.println(
					coche.getName()+" ha aparcado en la plaza "+plazaElegida+"."
					+ " Plazas disponibles: "+plazasDisponibles());
		}
		
		synchronized (listaTrabajo) {
			listaTrabajo.add(coche);				
		}
	}
	
	/**
	 * Elegira un n�mero que identifica a una plaza determinada del lavadero.
	 * @return plaza elegida por el usuario.
	 */
	public int elegirPlaza() {
		boolean exitCondition;
		int aux;
		
		synchronized(plazas) {	
			cochesEnLavadero++;
			
			aux=0;
			exitCondition=false;
			while(!exitCondition) {
				
				//Retardo para otorgar algo m�s de realismo a la simulaci�n.
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				//Genera un n�mero dentro del rango de plazas existentes
				aux=(int)(Math.random()*(plazas.length-0));

				
				//Si est� libre, finaliza bucle
				if(plazas[aux]==null) {
					exitCondition=true;
				}
			}
		}
		return aux;
	}
	
	
	/**
	 * Se encarga de lavar y encerar el coche, y notificar al lavadero que se ha finalizado el trabajo
	 * @param coche Veh�clo a procesar
	 */
	public synchronized void procesarCoche(Coche coche){
		
		while(listaTrabajo.get(0)!=coche) {
			try {
				wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		System.out.println("El operario ha comenzado a lavar el "+coche.getName());
		
		
		try {
			//Lavando
			Thread.sleep((long) (Math.random()*(4000-1000)+1000));
			//Encerando
			Thread.sleep((long) (Math.random()*(4000-1000)+1000));
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("El operario ha terminado con el "+coche.getName()+" .Ya se puede recoger");
		
		borraVehiculoDeLista();
		
		notifyAll();
	}
	
	/**
	 * Se encarga de eliminar de forma sincronizada el coche que ha sido limpiado por �ltima vez y reordenar el array
	 * que contiene los coches a limpiar
	 */
	private void borraVehiculoDeLista() {
		//Eliminamos el coche reci�n limpiado
		synchronized(listaTrabajo) {
			listaTrabajo.remove(0);
			
			/* Este fragmento de c�digo elimina los objetos de listaTrabajo que son iguales a null.
			 */
			listaTrabajo.removeAll(Collections.singleton(null));
			
			listaTrabajo.trimToSize();
		}

	}

	/**
	 * Permitir� sacar el coche del lavadero cuando este haya sido eliminado
	 * de la lista de ordenLlegada, o l
	 * @param coche
	 */
	public void sacarCoche(Coche coche) {
		synchronized (plazas) {
			cochesEnLavadero--;
			
			//Libera la plaza
			for(int i=0;i<plazas.length;i++) {
				if(plazas[i]==coche) {
					plazas[i]=null;
				}
			}
			System.out.println(coche.getName()+" ha salido del lavadero. Plazas disponibles: "+plazasDisponibles());
		}
	}
	
	/**
	 * Calcula el n�mero de plazas disponibles en funci�n de los huecos vac�os del array plazas
	 * @return plazas disponibles
	 */
	public int plazasDisponibles() {
		
		int counter= 0;
		synchronized(plazas) {
			for(int i=0;i<plazas.length;i++) {
					if(plazas[i]==null) {
						counter++;
					}
			}
		}
		return counter;
	}
}
