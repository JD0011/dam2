
/**
 * Escenario donde se producir� una simulaci�n ficticia rudimentaria del proceso que ocurre en un Restaurante.
 * <ol>
 * 	<li>Cliente llega al restaurante.</li>
 * 	<li>Cliente elige un plato de los disponibles en la barra del restaurante.</li>
 * 	<li>Notifica al camarero</li>
 * 	<li>El camarero le sirve el plato</li>
 * 	<li>Se le cobra al cliente y este se marcha.</li>
 * </ol>
 * 
 * @author juand
 * @since 07/11/2017
 *
 */
public class Main {
	
	public static void main(String[] args) {
		
		//Declaraci�n de objetos
		Carta carta;
		Barra barra;
		Cliente[] clientes;
		
		carta = new Carta(15); //Tama�o carta
		
		barra = new Barra(carta);
		
		
		clientes = new Cliente[20];
		for(int i= 0;i<clientes.length;i++) {
			clientes[i] = new Cliente(i, barra);
			clientes[i].start();
		}
		
		//Join para mostrar TotalContabilizado despu�s de atender a todos los clientes
		for(int i= 0;i<clientes.length;i++) {
			try {
				clientes[i].join();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		barra.mostrarTotalContabilizado();
	}

}
