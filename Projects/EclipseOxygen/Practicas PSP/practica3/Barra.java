import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

/**
 * <h1>Tuber�a</h1>
 * 
 * Representa la clase que manipular� toda la informaci�n de forma sincronizada, ya que en este planteamiento se accede
 * desde m�ltiples hilos a una instancia de esta clase de forma concurrente.<br>
 * 
 * <ol>
 * 	<li>El camarero notifica de los platos disponibles para elegir de la barra del restaurante.</li>
 * 	<li>Una vez que el cliente ha elegido uno disponible, el camarero se lo asigna, retir�ndolo de la barra.</li>
 * 	<li>Se le cobra al cliente el precio especificado seg�n el restaurante.</li>
 * </ol>
 * 
 * @author juand
 * @since 07/11/2017
 *
 */
public class Barra {
	
	private Carta carta;
	//Lista de personas esperando para pedir
	private ArrayList<Cliente> ordenPreferencia;
	//Contabilidad de clientes atendidos por este Camarero
	private ArrayList<Cliente> totalClientes;
	
	private LinkedHashMap<Integer, Cliente> platosReservados;
	
	/**
	 * Instancia una barra con los platos indicados por la carta<br>
	 * @see Carta
	 * @param carta Contiene los platos que presenta la barra
	 */
	public Barra(Carta carta) {
		this.carta = carta;
		
		ordenPreferencia=new ArrayList<>();
		totalClientes=new ArrayList<>();
		platosReservados=new LinkedHashMap<Integer, Cliente>();
	}
	
	/**
	 * <h1>Comprueba que quedan platos</h1>
	 * Compara la cantidad de clientes dentro con respecto a la cantidad de platos existentes
	 * @param cliente
	 * @return el resultado de la evaluaci�n
	 */
	public boolean quedanPlatos(Cliente cliente) {
		/**
		boolean disponible;
		
		synchronized (carta) {
			disponible = totalClientes.size() < carta.getPlatosCarta().size();
		}
		
		if(disponible) {
			synchronized (totalClientes) {
				totalClientes.add(cliente);
			}
			
			System.out.println(
					cliente.getName()+" - LLEGAR - "
					+ "Ha llegado al restaurante");
		}else {
			System.out.println(cliente.getName()+" - SALIR - "
					+ "No hay mas platos disponibles.");
		}
		**/
		return true;
	}
	
	/**
	 * <h1>Elecci�n de plato</h1>
	 * El cliente pregunta por la disponibilidad de un plato y el camarero responde seg�n las existencias.<br>
	 * Esto se logra gracias a que la clase Restaurante posee la informaci�n sobre la disponibilidad del plato.
	 * @param cliente Solicitante del plato
	 */
	public boolean elegirPlato(Cliente cliente) {
		boolean disponible;
		int idPlato;
			
		idPlato= 0;
		
		//Tiempo para elegir
		try {
			Thread.sleep((long)(Math.random()*(5000-1000)+1000));
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		idPlato = (int)(Math.random()*(carta.getSizeCarta()-0)+0);

		synchronized (platosReservados) {
			if(disponible=!platosReservados.containsKey(idPlato)) {
				
				platosReservados.put(carta.getPlatosCarta().get(idPlato), cliente);
				cliente.setPlato(idPlato);
				ordenPreferencia.add(cliente);
				System.out.println(
						cliente.getName()+" - ELEGIR - "
						+ "Ha elegido el plato "+idPlato);
			}else {
				System.out.println(cliente.getName() +" - YA ELEGIDO - "
						+ "El plato "+idPlato+" ya ha sido elegido por "+platosReservados.get(idPlato).getName());
			}
		}
		
		return disponible;
	}
	
	/**
	 * <h1>Asignaci�n de plato</h1>
	 * Se encarga de retirar el plato elegido por el cliente cuando sea su turno, el cu�l se determina
	 * mediante el orden de llegada.<br>
	 * @param cliente Cliente que pide el plato disponible
	 */
	public synchronized void servirPlato(Cliente cliente) {
		//Mientras no sea el primero en la lista
		while(cliente!=ordenPreferencia.get(0)) {
			try {
				wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		//Tiempo para servir
	
		try {
			Thread.sleep((long)(Math.random()*(3000-1000)+1000));
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
		System.out.println(cliente.getName()+" - SERVIR - El camarero le ha servido el plato "+cliente.getPlato());
	
		//Elimina el cliente de la lista de preferencia
		ordenPreferencia.remove(0);
		ordenPreferencia.trimToSize();
		
		notifyAll();
		
	}
	
	/**
	 * <h1>Pregunta al camarero el precio del plato.</h1>
	 * Para ello, el camarero consulta la carta disponible en el restaurante y le notifica el precio.<br>
	 * Tambi�n contabiliza la consumici�n en sus clientes atendidos.<br>
	 * @param cliente Solicitante de la cuenta
	 */
	public void pedirCuenta(Cliente cliente) {
		System.out.println(
				cliente.getName()+" - CUENTA - "
				+ "Paga "+carta.precioPlato(cliente.getPlato())+"� por el plato "+cliente.getPlato());		
	
		totalClientes.add(cliente);
	}
	
	/**
	 * <h1>Muestra los clientes atendidos</h1>
	 * Construye una tabla formateada con los datos de los clientes atendidos.<br>
	 * Entre esos datos, podemos destacar el nombre del cliente, plato consumido y precio del mismo.
	 */
	public void mostrarTotalContabilizado() {
		System.out.println("\n\nTotal de clientes atendidos.");
		
		System.out.println(
				String.format(
						"%-13s%-6s%-8s%-3s%-8s%s","CLIENTE","|","PLATO","|","PRECIO","|"));
		              
		for(Cliente cl: totalClientes) {
			System.out.println("-------------------------------------");
			System.out.println(
					String.format(
							"%-15s%-15s%-9s%s",
							cl.getName(),
							"N� "+cl.getPlato(),
							carta.precioPlato(cl.getPlato())+"�",
							"|"));
		}
	}
	
}
