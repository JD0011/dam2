
/**
 * <h1>Hilo</h1>
 * Representa cada uno de los objetos que van a acceder a nuestro espacio de datos en com�n.<br>
 * Realiza las diferentes acciones que le brinda el camarero a modo de interacci�n con la barra.
 * @author juand
 * @since 07/11/2017
 *
 */
public class Cliente extends Thread{
	
	private Barra cam;
	private int plato;

	public Cliente(int id, Barra cam) {
		this.cam = cam;
		setName("Cliente "+String.valueOf(id));
	}
	
	public void setPlato(int idPlato) {
		plato = idPlato;
	}
	
	public int getPlato() {
		return plato;
	}

	@Override
	public void run() {
				
		if(cam.quedanPlatos(this)) {
			
			//Si nadie ha elegido el plato anteriormente
			if(cam.elegirPlato(this)) {
				//Tiempo para comer
				try {
					sleep((long)(Math.random()*(8000-1000)+1000));
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				cam.servirPlato(this);
				cam.pedirCuenta(this);
			}
			
			
			
			
			
		}		
		
	}
	
}
