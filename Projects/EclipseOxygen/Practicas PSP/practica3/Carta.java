import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

/**
 * Clase auxiliar
 * Contiene la información correspondiente al precio del plato.
 * <ul>
 * 	<li>Platos de la carta</li>
 * 	<li>Precio de los platos</li>
 * </ul>
 * 
 * @author juand
 * @since 07/11/2017
 *
 */
public class Carta {
	

	//Platos de la barra y cantidad disponible
	//Plato y precio
	private LinkedHashMap<Integer, Double> platosPrecio;
	private ArrayList<Integer> platos;
	
	public Carta(int sizeCarta) {
		
		platosPrecio = new LinkedHashMap<Integer, Double>();
		platos = new ArrayList<>();
		
		for(int i=0;i<sizeCarta;i++) {
			//Plato y precio
			platosPrecio.put(i, Math.floor(Math.random()*(10-1)+1));
			platos.add(i);
		}
	}
	
	/**
	 * Precio establecido
	 * @param idPlato Plato del que buscar el precio
	 * @return precio en tipo Double
	 */
	public double precioPlato(int idPlato) {
		return platosPrecio.get(idPlato).doubleValue();
	}
	
	/**
	 * <h1>Muestra los platos</h1>
	 * Imprime la carta del restaurante con los platos y las existencias de cada uno
	 */
	public void mostrarCartaGenerada() {
		System.out.println("Contenido de la carta");
		System.out.println(String.format("%-10s%s", "PLATO", "PRECIO"));
		
		for (Entry<Integer, Double> e : platosPrecio.entrySet()) {
			System.out.println(String.format("%-25s%s",e.getKey(),e.getValue()));
		}
		
		System.out.println(String.format("%-18s%s","Total platos",platosPrecio.size()));
	}

	public ArrayList<Integer> getPlatosCarta() {
		return platos;
	}

	public int getSizeCarta() {
		return platos.size();
	}
	
}
