/*
 * En este ejemplo vamos a insertar un registro sin depedencia alguna de otras 
 * tablas de nuestra Base de datos.
 * En concreto se va a introducir un usuario.
 */

package paqueteProyectoHibernate3;

import java.util.Scanner;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import reversedclasses.Usuarios;

public class Ej02IntroRegistroSimple {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		// Obtener la sesi�n actual
		SessionFactory sesion = Ej00HibernateUtil.getSessionFactory();
		// crear la sesi�n
		Session session = sesion.openSession();
		// crear una transacci�n de la sesi�n
		Transaction tx = session.beginTransaction();
		try {
			System.out.println("Inserto una fila en la tabla USUARIO.");
			// Creo el objeto que contendr� un usuario
			Usuarios miUsuario = new Usuarios();
			System.out.println("Introduzca DNI");
			String entrada = sc.nextLine();
			miUsuario.setDni(entrada);
			System.out.println("Introduzca direcci�n");
			entrada = sc.nextLine();
			miUsuario.setDireccion(entrada);
			System.out.println("Introduzca fecha de Nacimiento");
			entrada = sc.nextLine();
			miUsuario.setFechaNac(entrada);
			System.out.println("Introduzca Nombre");
			entrada = sc.nextLine();
			miUsuario.setNombre(entrada);
			System.out.println("Introduzca N�mero de Carnet");
			entrada = sc.nextLine();
			miUsuario.setNumCarnet(entrada);

			session.save(miUsuario);
			tx.commit();
		}
		catch (Exception e) {
			System.out.println("ERROR DE BASE DE DATOS");
		}
		session.close();

		System.exit(0);
	}
}
