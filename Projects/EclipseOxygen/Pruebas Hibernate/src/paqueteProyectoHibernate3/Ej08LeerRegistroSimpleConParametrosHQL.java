package paqueteProyectoHibernate3;

import java.util.Iterator;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import reversedclasses.Usuarios;


public class Ej08LeerRegistroSimpleConParametrosHQL {

	public static void main(String[] args) {
		SessionFactory sesion= Ej00HibernateUtil.getSessionFactory();
		Session session = sesion.openSession();
		
		//consulta con par�metros
		String consulta="from Usuarios u where u.dni=:dniP";
		Query q = session.createQuery(consulta);
		q.setParameter("dniP", "33333333C");
				
		List <Usuarios> lista = q.list();
		
		//Obtenemos un Iterador y recorremos la lista.
		Iterator <Usuarios> iter=lista.iterator();
		System.out.println("N�mero de usuarios: "+lista.size());
		while (iter.hasNext()){
			//extraer el objeto
			Usuarios users = (Usuarios) iter.next();
			System.out.println(users.getDireccion()+" "+users.getDni()+" "+users.getFechaNac()+" "+users.getNombre()+" "+users.getNumCarnet());
		}
		session.close();
		System.exit(0);
	}

}