package paqueteProyectoHibernate3;




import java.util.Iterator;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import reversedclasses.Usuarios;


public class Ej07LeerRegistroSimpleHQL {

	public static void main(String[] args) {
		SessionFactory sesion= Ej00HibernateUtil.getSessionFactory();
		Session session = sesion.openSession();
		
		//consulta simple
		Query q = session.createQuery("from Usuarios");
		//consulta con where
		//Query q = session.createQuery("from Usuarios where dni='11111111A'");
		
		List <Usuarios> lista = q.list();
		
		//Obtenemos un Iterador y recorremos la lista.
		Iterator <Usuarios> iter=lista.iterator();
		System.out.println("N�mero de usuarios: "+lista.size());
		while (iter.hasNext()){
			//extraer el objeto
			Usuarios users = (Usuarios) iter.next();
			System.out.println(users.getDireccion()+" "+users.getDni()+" "+users.getFechaNac()+" "+users.getNombre()+" "+users.getNumCarnet());
		}
		session.close();
		System.exit(0);
	}

}