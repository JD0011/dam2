/*
 * En este ejemplo vamos a insertar un registro con depedencia de otras 
 * tablas de nuestra Base de datos.
 * En concreto se va a introducir un coche que debe tener un usuario en 
 * su tabla correspondiente.
 */

package paqueteProyectoHibernate3;

import java.util.Scanner;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import reversedclasses.Coches;
import reversedclasses.Usuarios;

public class Ej03IntroRegistroDependiente {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		// Obtener la sesi�n actual
		SessionFactory sesion = Ej00HibernateUtil.getSessionFactory();
		// crear la sesi�n
		Session session = sesion.openSession();
		// crear una transacci�n de la sesi�n
		Transaction tx = session.beginTransaction();
		System.out.println("Inserto una fila en la tabla COCHE (que debe tener un usuario como propietario)");
		// Creo el objeto que contendr� un usuario
		Coches miCoche = new Coches();
		System.out.println("Introduzca MATR�CULA");
		String entrada = sc.nextLine();
		miCoche.setMatricula(entrada);
		System.out.println("Introduzca MARCA");
		entrada = sc.nextLine();
		miCoche.setMarca(entrada);
		System.out.println("Introduzca MODELO");
		entrada = sc.nextLine();
		miCoche.setModelo(entrada);
		System.out.println("Introduzca COLOR");
		entrada = sc.nextLine();
		miCoche.setColor(entrada);
		System.out.println("Introduzca FECHA DE MATRICULACI�N");
		entrada = sc.nextLine();
		miCoche.setFechaMat(entrada);
		/*
		 * Vamos a introducir el DNI del propietario del veh�culo que debe
		 * existir en nuestra BD
		 */
		System.out.println("Introduzca DNI DEL PROPIETARIO");
		entrada = sc.nextLine();
		// creo un objeto accediendo al get de la sesi�n
		Usuarios miUsuario = (Usuarios) session.get(Usuarios.class, entrada);
		if (miUsuario == null) {
			System.out.println("El usuario no existe");
		} else {
			miCoche.setUsuarios(miUsuario);
		}
		session.save(miCoche);
		tx.commit();
		session.close();

		System.exit(0);
	}
}
