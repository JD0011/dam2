package paqueteProyectoHibernate3;

import java.util.Iterator;
import java.util.Scanner;
import java.util.Set;

import org.hibernate.ObjectNotFoundException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import reversedclasses.Coches;
import reversedclasses.Usuarios;


public class Ej01LeerRegistroSimpleYCompuesto {

	public static void main(String[] args) {
		//Obtener la sesi�n actual
		SessionFactory sesion=Ej00HibernateUtil.getSessionFactory();
		//crear la sesi�n
		Session session=sesion.openSession();
		
		Scanner sc=new Scanner(System.in);
		System.out.println("Introduzca DNI:");
		String entrada = sc.nextLine();
		Usuarios user=new Usuarios();
		try{
			user=(Usuarios) session.load(Usuarios.class, entrada);
			System.out.printf("Nombre Usuario: %s%n", user.getNombre());
			System.out.printf("Num�ro de Carnet: %s%n", user.getNumCarnet());
			System.out.printf("Fecha de Nacimiento: %s%n", user.getFechaNac());
			System.out.printf("DNI: %s%n", user.getDni());
			System.out.printf("Direcci�n: %s%n", user.getDireccion());
			//obtenemos coches en propiedad
			Set<Coches> cochesEnPropiedad = user.getCocheses();
			Iterator<Coches> it= cochesEnPropiedad.iterator();
			System.out.printf("N�mero de coches en propiedad: %d%n",cochesEnPropiedad.size());
			while (it.hasNext()){
				Coches coches = it.next();
				System.out.println("Matr�cula: "+coches.getMatricula());
			}				
		}catch(ObjectNotFoundException o){
			System.out.println("Usuario NO EXISTE");
		}
		
		session.close();
		System.exit(0);
	}
}
