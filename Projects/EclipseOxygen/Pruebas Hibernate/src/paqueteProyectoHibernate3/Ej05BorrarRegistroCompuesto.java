package paqueteProyectoHibernate3;

import java.util.Scanner;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import reversedclasses.Usuarios;

public class Ej05BorrarRegistroCompuesto {

	public static void main(String[] args) {
		//Obtener la sesi�n actual
		SessionFactory sesion=Ej00HibernateUtil.getSessionFactory();
		//crear la sesi�n
		Session session=sesion.openSession();
		//crear una transacci�n de la sesi�n
		Transaction tx = session.beginTransaction();
		
		System.out.println("Introduzca usuario a borrar:");
		Scanner sc=new Scanner(System.in);
		String usuario=sc.nextLine();
		
		Usuarios miUsuario=new Usuarios();
		miUsuario=(Usuarios) session.load(Usuarios.class, usuario);
			
		session.delete(miUsuario); //elimina el objeto
		tx.commit();
		System.out.println("Usuario eliminado Y TODAS SUS DATOS RELACIONADOS");
				
		session.close();
		System.exit(0);
	}
}
