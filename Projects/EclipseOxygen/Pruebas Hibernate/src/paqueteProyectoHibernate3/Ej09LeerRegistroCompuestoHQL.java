package paqueteProyectoHibernate3;

import java.util.Iterator;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import reversedclasses.Multas;


public class Ej09LeerRegistroCompuestoHQL {

	public static void main(String[] args) {
		SessionFactory sesion= Ej00HibernateUtil.getSessionFactory();
		Session session = sesion.openSession();
		
		//consulta compuesta con par�metros 

		String consulta="from Multas m where m.coches.usuarios.dni=:dniP";
		Query q = session.createQuery(consulta);
		q.setParameter("dniP", "55555555E");
		
		List <Multas> lista = q.list();
		
		//Obtenemos un Iterador y recorremos la lista.
		Iterator <Multas> iter=lista.iterator();
		System.out.println("N�mero de multas del usuario: "+lista.size());
		while (iter.hasNext()){
			//extraer el objeto
			Multas multas = (Multas) iter.next();
			System.out.println(multas.getDenuncia()+" "+multas.getCoches().getMatricula()+" "+multas.getFechaDenuncia()+" "+multas.getImporte());
		}
		session.close();
		System.exit(0);
	}

}