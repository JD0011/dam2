package paqueteProyectoHibernate3;

import java.util.Scanner;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import reversedclasses.Multas;

public class Ej04BorrarRegistroSimple {

	public static void main(String[] args) {
		//Obtener la sesi�n actual
		SessionFactory sesion=Ej00HibernateUtil.getSessionFactory();
		//crear la sesi�n
		Session session=sesion.openSession();
		//crear una transacci�n de la sesi�n
		Transaction tx = session.beginTransaction();
		
		System.out.println("Introduzca una multa a borrar:");
		Scanner sc=new Scanner(System.in);
		String idMulta=sc.nextLine();
		
		Multas miMulta=new Multas();
		miMulta=(Multas) session.load(Multas.class, (byte) Integer.parseInt(idMulta));
			
		session.delete(miMulta); //elimina el objeto
		tx.commit();
		System.out.println("Multa eliminada");
				
		session.close();
		System.exit(0);
	}
}
