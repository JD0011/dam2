package db4o.ejemplo1;

public class Main {

	final static String BDpath = "./Ficheros/db4o/Persona.yap";
	
	public static void main(String[] args) {
		
		

		//ESCRITURA
		/**
		 * 
		ObjectContainer db = Db4oEmbedded.openFile(Db4oEmbedded.newConfiguration(),BDpath);
		
		Persona[] pers= {
				new Persona("Juan", "M�laga"),
				new Persona("Rosa","Sevilla"),
				new Persona("Alberto","Madrid"),
				new Persona("Roc�o","Granada")
		};
		
		for(Persona p: pers) {
			db.store(p);
		}
		
		db.close();
		**/
		
		//LEER
		/**
		ObjectContainer db = Db4oEmbedded.openFile(BDpath);
		
		db.query(Persona.class);
		
		//Equivalente a SELECT * FROM..., al no especificar los valores para los atributo
		Persona per = new Persona(null,null);
		
		ObjectSet<Persona> result = db.queryByExample(per);
		
		
		if(result.size()==0) {
			System.out.println("No hay registros");
		}else {
			System.out.println("Imprime todos los resultados");
			for(Persona p: result) {
				System.out.println(p.toString());
			}
			System.out.println("Imprime (si existe) a Juan");
			
			//SELECT * FROM Persona WHERE nombre='Juan'
			per = new Persona("Juan", null);
			
			result = db.queryByExample(per);
			
			System.out.println(result.toString());
			
		}**/
		
	}
}
