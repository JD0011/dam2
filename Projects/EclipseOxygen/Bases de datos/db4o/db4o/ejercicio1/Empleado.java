package db4o.ejercicio1;

public class Empleado {

	private int id;
	private String nombre;
	private double sueldo;
	private int dep_id;
	
	
	public Empleado() {
		
	}
	
	public Empleado(int id) {
		this.id = id;
	}
	
	public Empleado(int id, String nombre, double sueldo, int dep) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.sueldo = sueldo;
		this.dep_id = dep;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public double getSueldo() {
		return sueldo;
	}

	public void setSueldo(double sueldo) {
		this.sueldo = sueldo;
	}

	public int getDep() {
		return dep_id;
	}

	public void setDep(int dep_id) {
		this.dep_id = dep_id;
	}

	@Override
	public String toString() {
		return "Empleado [id=" + id + ", nombre=" + nombre + ", sueldo=" + sueldo + ", dep=" + dep_id + "]";
	}
	
	
}
