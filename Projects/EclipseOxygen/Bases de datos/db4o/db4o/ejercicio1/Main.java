package db4o.ejercicio1;

import java.io.IOException;
import java.util.Scanner;

public class Main {
	
	private static String BDPath = "./Ficheros/db4o/Empleados.yap";

	public static void main(String[] args) {
		
		RegistroManager rmanager = new RegistroManager("./Ficheros/db4o/Empleados.yap");
		
		Scanner sc = new Scanner(System.in);
		boolean exitCondition = false;
		do {
			
			System.out.println("Bienvenido a la aplicaci�n gestora de Empleados.yap");
			System.out.println("Elija una opci�n");
			System.out.println("1.Insertar Empleado \n2.Eliminar Empleado \n3.Modificar empleado \n4.Mostrar todos \n5.Salir");
			
			int answer = sc.nextInt();
			
			sc = new Scanner(System.in);
			switch(answer) {
			case 1:
				Empleado aux = new Empleado();
				System.out.println("Introduzca la siguiente informaci�n con respecto al empleado");
				
				System.out.print("Nombre: ");
				aux.setNombre(sc.nextLine());
				
				System.out.print("Sueldo: ");
				aux.setSueldo(sc.nextDouble());
				
				System.out.print("Departamento (ID): ");
				aux.setDep(sc.nextInt());
				
				rmanager.insertarRegistro(aux);
				System.out.println("Empleado guardado correctamente");
				break;
			case 2:
				System.out.println("Introduzca el ID del empleado a eliminar: ");
				rmanager.eliminarRegistro(sc.nextInt());
				break;
			case 3:
				System.out.println("Introduce el ID del empleado a modificar: ");
				rmanager.modificarRegistro(sc.nextInt());
			case 4:
				System.out.println("Mostrando empleados regitrados");
				rmanager.mostrarRegistros();
				break;
			case 5:
				exitCondition = true;
				break;
				
				default:
					System.out.println("Opci�n no v�lida");
					break;
				
			}
			
		
		}while(!exitCondition);
		
	}
	
}