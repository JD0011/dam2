package db4o.ejercicio1;

import java.io.File;
import java.util.Scanner;

import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;

public class RegistroManager {
	
	//Database manager
	private ObjectContainer container;
	
	public RegistroManager(String DBPath) {
		if(!new File(DBPath).exists()) {
			container = Db4oEmbedded.openFile(Db4oEmbedded.newConfiguration(), DBPath);
		}else {
			container = Db4oEmbedded.openFile(DBPath);
		}
	}
	
	public void mostrarRegistros() {
		Empleado example = new Empleado();
		
		ObjectSet<Empleado> results = container.queryByExample(null);
		
		for(Empleado e: results) {
			System.out.println(e.toString());
		}
	}
	
	//Insert record
	public void insertarRegistro(Empleado emp) {
		container.store(emp);
	}
	
	//Delete record by example. If unknown, set to null, so it will show all of them.
	public void eliminarRegistro(int id) {
		
		Empleado example = new Empleado(id);

		ObjectSet<Empleado> result = container.queryByExample(example);
	}
	
	public void modificarRegistro(int emp_id) {
		Empleado example = new Empleado(emp_id);
		
		ObjectSet<Empleado> result = container.queryByExample(example);
		
		if(result.size()==0) {
			System.out.println("No existe ning�n empleado como el que ha especificado");
		}else{
			System.out.println("Datos del empleado");
			System.out.println(result.next().toString());
			
			Scanner sc = new Scanner(System.in);
			
			boolean exitCondition = false;
			do {
				System.out.println("Indique lo que quiere modificar");
				System.out.println("1.Nombre \n2.Sueldo \n3.Departamento \n4.Salir.");
				
				int answer;
				try {
					answer = sc.nextInt();
				}catch(Exception e) {
					System.out.println("Debe introducir una cantidad num�rica");
					continue;
				}
				
				switch(answer) {
					case 1:
						System.out.println("Nuevo nombre");
						result.next().setNombre(sc.nextLine());
						break;
					case 2:
						System.out.println("Nuevo sueldo");
						try {
							result.next().setSueldo(sc.nextDouble());
						}catch (Exception e) {
							System.out.println("Debe introducir una cantidad num�rica");
							continue;
						}
						break;
					case 3:
						System.out.println("Nuevo departamento (Especifique su ID)");
						try {
							result.next().setSueldo(sc.nextInt());
						}catch (Exception e) {
							System.out.println("Debe introducir una cantidad num�rica");
							continue;
						}
						break;
					case 4:
						System.out.println("Guardando la informaci�n");
						container.store(result.next());
						break;
					default:
						System.out.println("Opci�n no contemplada");
						break;
						
				}
				
			}while(!exitCondition);
				
				
			
		}
	}

}
