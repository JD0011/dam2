package db4o.ejercicio1;

public class Departamento {
	
	private String nombre;
	private String loc;
	
	public Departamento(String nombre, String loc) {
		super();
		this.nombre = nombre;
		this.loc = loc;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getLoc() {
		return loc;
	}

	public void setLoc(String loc) {
		this.loc = loc;
	}
	
	
}
