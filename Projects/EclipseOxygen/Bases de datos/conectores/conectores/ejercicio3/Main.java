package conectores.ejercicio3;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

public class Main {
	
	public static void main(String[] args) {
		
		Empleado[] emps= {
				new Empleado(5, 1, "Romero", "Salcedo", "Programador", 1500, 23),
				new Empleado(6, 2, "Gonzalez", "Armando", "Programador", 1500, 23),
				new Empleado(7, 4, "Carrero", "Molino", "Programador", 1500, 23)
		};
		
		//LOADING CLASS
				
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Properties props = new Properties();
		
		props.setProperty("user", "root");
		
		//INITIALIZE CONECCTION
		
		Connection connect = null;
		
		try {
			connect = DriverManager.getConnection("jdbc:mysql://localhost:3306/bbddej3",props);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Statement stmt = null;
		try {
			stmt = connect.createStatement();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Boolean seInserta;
		
		seInserta = true;
		for(Empleado emp: emps) {
			
			//CHECK DEPARTMENT
			try {
				stmt.execute("SELECT * FROM departamento WHERE dep_id="+emp.getDept_no()+";");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
			try {
				if(!stmt.getResultSet().first()) {
					System.out.println("The specified department doesn't exists");
					seInserta = false;
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			//CHECK EMPLOYE ID
			try {
				stmt.execute("SELECT * FROM empleado WHERE emp_no="+emp.getEmp_no()+";");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
				
			try {
				if(stmt.getResultSet().first()) {
					System.out.println("The employe ID already exists!");
					seInserta = false;
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			//CHECK SALARY
			if(emp.getSalario()<0) {
				System.out.println("The salary must be positive!");
				seInserta = false;
			}

			if(seInserta) {
				try {
					stmt.executeUpdate("INSERT INTO empleado "
									+ "VALUES ("
										+emp.getEmp_no()+",'"
										+emp.getApellido()+"','"
										+emp.getProfesion()+"','"
										+emp.getDirector()+"',"
										+emp.getSalario()+","
										+emp.getComision()+","
										+emp.getDept_no()+");");
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
		}
		
		
	}

}
