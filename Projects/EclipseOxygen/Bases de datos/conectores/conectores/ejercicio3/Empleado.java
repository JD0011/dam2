package conectores.ejercicio3;

import java.io.Serializable;
import java.util.Date;

public class Empleado implements Serializable{
	
	private int emp_no, dept_no;
	private String apellido, director, profesion;
	private double salario, comision;
	
	
	
	public Empleado(int emp_no, int dept_no, String apellido, String director, String profesion, double salario, double comision) {
		super();
		this.emp_no = emp_no;
		this.dept_no = dept_no;
		this.apellido = apellido;
		this.director = director;
		this.salario = salario;
		this.comision = comision;
		this.profesion = profesion;
	}
	
	public int getEmp_no() {
		return emp_no;
	}
	public int getDept_no() {
		return dept_no;
	}
	public String getApellido() {
		return apellido;
	}
	public String getDirector() {
		return director;
	}
	public String getProfesion() {
		return profesion;
	}
	public double getSalario() {
		return salario;
	}
	public double getComision() {
		return comision;
	}

}
