package Utils;

import java.io.IOException;
import java.util.Properties;

import javax.mail.AuthenticationFailedException;
import javax.mail.Authenticator;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.NoSuchProviderException;
import javax.mail.Part;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import datamodels.MailUser;
import javafx.application.Platform;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;

/**
 * Clase que administra las operaciones realizables sobre un correo electr�nico. Permite obtener las sesiones SMTP / POP3 de forma an�loga
 * al proveedor de correo del usuario conectado.
 * @author juand
 *
 */
public class MailManager{
	
	private Logger l = LoggerFactory.getLogger(MailManager.class);
	
	/**
	 * Usuario logeado en la aplicaci�n
	 */
	private MailUser mUser;
	
	/**
	 * Sesi�n SMTP del usuario
	 */
	private Session smtpSession;
	private Properties smtpProps;
	/**
	 * Sesi�n POP3 del usuario
	 */
	private Session pop3Session;
	private Properties pop3Props;
	
	public MailManager(MailUser mUser) {
		this.mUser = mUser;
		
		initialize();
	}
	
	/**
	 * Se encarga de establecer un estado inicial a las variables que lo necesitan
	 */
	private void initialize() {
		l.info("Estableciendo propiedades SMTP/POP3...");
		smtpProps = new Properties();
		smtpProps.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		smtpProps.put("mail.smtp.socketFactory.port", mUser.getSMTPPort());
		smtpProps.put("mail.smtp.auth", "true");
		smtpProps.put("mail.smtp.starttls.enable", "true");
		smtpProps.put("mail.smtp.host", mUser.getSMTPHostname());
		smtpProps.put("mail.smtp.port", mUser.getSMTPPort());
		
		pop3Props = new Properties();
		pop3Props.setProperty("mail.pop3.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		pop3Props.put("mail.pop3.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		pop3Props.put("mail.pop3.socketFactory.fallback", "false");
		pop3Props.put("mail.store.protocol", "pop3");
		pop3Props.put("mail.pop3.starttls.enable", false);
		pop3Props.put("mail.pop3.socketFactory.port", mUser.getPOP3Port());
		pop3Props.put("mail.pop3.port", mUser.getPOP3Port());
		pop3Props.put("mail.pop3.host", mUser.getPOP3Hostname());
		pop3Props.put("mail.pop3.user", mUser.getUsername());
		pop3Props.put("mail.pop3.ssl.trust", mUser.getPOP3Hostname());
		
		//Inicia las sesiones SMTP / POP3
		setupSessions();
	}
	
	/**Extrae las direcciones de los servidores IMAP y SMTP a trav�s del correo 
	 * introducido
	 */
	private void setupSessions() {
		
		Authenticator auth = new Authenticator() {
			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				// TODO Auto-generated method stub
				return new PasswordAuthentication(mUser.getUsername(), mUser.getPassword());
			}
		};
		
		smtpSession = Session.getInstance(smtpProps,auth);
		pop3Session = Session.getInstance(pop3Props,auth);
		
		l.info("Sesiones SMTP/POP3 iniciadas.");	
	}

	/**
	 * Obtiene los mails del buz�n de correo utilizando la sesi�n creada a partir de los datos del usuario.
	 * 
	 */
	public Message[] listMails(){
		l.info("Obteniendo correos de la bandeja de entrada...");
		Message[] messages = null;
		Store store = null;
		try {
			store = pop3Session.getStore("pop3");
			store.connect(mUser.getUsername(), mUser.getPassword());
			
			Folder inbox = store.getFolder("INBOX");
			
			inbox.open(Folder.READ_ONLY);
			
			messages = inbox.getMessages();
			l.info("Bandeja de entrada: mensajes obtenidos satisfactoriamente.");
		} catch (NoSuchProviderException e) {
			l.error("Error en lectura de mensajes. �Ha llamado al m�todo setupSessions()?");
			e.printStackTrace();
		} catch (MessagingException e) {
			l.error("Error en el mensaje. �Ha llamado al m�todo setupSessions()?");
			Platform.runLater(new Runnable() {
				
				@Override
				public void run() {
					Alert error = new Alert(
							AlertType.ERROR, 
							"No se ha podido listar el correo. �Ha realizado los pasos descritos en la "
							+ "gu�a sobre la configuraci�n de la cuenta?",
							ButtonType.OK);
					error.showAndWait();
				}
			});
			
			e.printStackTrace();
		}
		
		return messages;
	}
	
	/**
     * Lee el contenido del mensaje pasado por pa�metros y lo procesa, en funci�n de si es un mensaje sencillo o multiparte.</br>
     * <i>Se ejecutar� de forma recursiva para leer todas las partes de un mensaje.</i>
     * @param part Parte del mensaje a analizar
     * @throws MessagingException 
     * @throws IOException 
     */
    public void readContent(Part part, StringBuilder content) throws MessagingException, IOException {
    	//Control texto plano
    	/**
		if(part.isMimeType("text/plain")){
			content.append("<p>"+part.getContent().toString()+"</p>");
		}**/
		
		if(part.isMimeType("text/html")) {
			//Eliminaci�n de las etiquetas HTML
			StringBuilder htmlContent = new StringBuilder(part.getContent().toString());
			content.append(htmlContent.toString());
		}
				
		if(part.isMimeType("multipart/*")) {
			Multipart multipart = (Multipart) part.getContent();
    		
    		//Procesamiento de las partes de un mensaje Multipart
    		for(int i = 0; i<multipart.getCount();i++) {
    			Part p = multipart.getBodyPart(i);
    			readContent(p, content);
    		}
		}
    }
	
	 /**
     * Se encarga de enviar el mensaje pasado por par�metros al destinatario especificado.
     * Si hay m�ltiples destinatarios, eviar� el mensaje uno por uno
     * @return True si se env�a correctamente, False de lo contrario.
     */
	public boolean sendMessage(String[] destinataries, String subject, String content) {
		l.info("Enviando nuevo correo");
		Thread tMessageSender = new Thread(new Runnable() {
					
			@Override
			public void run() {
				//Crea el mensaje
				MimeMessage msg = new MimeMessage(smtpSession);
				
				try {
					//Establece remitente
					msg.setFrom(new InternetAddress(mUser.getUsername()));
					
					//A�ade a todos los destinatarios
					for(String dest: destinataries) {
						msg.addRecipient(Message.RecipientType.TO, new InternetAddress(dest));
					}
					l.info("Total de destinatarios del mensaje: "+destinataries.length+" usuario/s");
					
					//Establece asunto
					msg.setSubject(subject);
					//A�ade contenido
					msg.setContent(content, "text/plain");
					
					//Env�a el mensajes
					Transport smtpTransport = smtpSession.getTransport("smtp");
					smtpTransport.connect(mUser.getSMTPHostname(), mUser.getUsername(), mUser.getPassword());
					Transport.send(msg, msg.getAllRecipients());
					l.info("Mensajes enviados correctamente");
					smtpTransport.close();
				} catch (MessagingException e) {
					l.error("Error en el mensaje, compruebe que los datos son correctos. Correo no enviado");
					e.printStackTrace();
				}
				
			}
		});
    	
    	tMessageSender.setName("MESSAGE SENDER");
    	tMessageSender.start();
    	
    	return true;
	}
	
}
