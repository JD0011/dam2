package Utils;

/**
 * Posee reglas de validaci�n para la informaci�n requerida por el servidor FTP
 * @author juand
 *
 */
public class FTPValidator {
	
	/**
	 * Valida el usuario. Criterio: campo no est� vacio
	 * @param uname Nombre de usuario a evaluar
	 * @return True si es correcto, False de lo contrario
	 */
	public boolean validateUsername(String uname) {
		boolean isRight;
		//VERIFICACI�N CONTRASE�A
		isRight = true;
    	if(uname.equals("")) {
    		isRight = false;
		}
    	
    	return isRight;
	}
	
	/**
	 * Valida la contrase�a. Criterio: longitud superior a 2 caracteres
	 * @param pwd Contrase�a a evaluar
	 * @return True si es correcto, False de lo contrario
	 */
	public boolean validatePassword(String pwd) {
		boolean isRight;
		
		isRight = true;
		if(pwd.length()<2) {
			isRight = false;
		}
		
		return isRight;
	}
	
	/**
	 * Valida la direcci�n IP. Criterio: comprueba si es una IP o un dominio. En caso de IP, contabiliza el n�mero de puntos.
	 * En caso de dominio,
	 * @param ip Direcci�n IP o dominio a evaluar
	 * @returnTrue si es correcto, False de lo contrario
	 */
	public boolean validateIPAddress(String ip) {
		boolean isRight;
		
		isRight = true;
		if(ip.equals("")) {
    		isRight=false;
    		
    	}else {
    		boolean isDomain = false;
    		//Comprueba si es una direcci�n IP o hostname
    		for(char c: ip.toCharArray()) {
    			if(Character.isLetter(c)) {
    				isDomain = true;
    			}
    		}
    		
    		if(!isDomain) {
    			int dots = 0;
    			for(char c: ip.toCharArray()) {
    				if(c=='.') {
    					dots++;
    				}
    			}
    			//Comprueba que tenga tres puntos
    			if(dots!=3) {
    				isRight=false;
    			}else {
    				//Cortes�a de StackOverflow (https://stackoverflow.com/a/15875500)
    		    	final String ip_regex = "^(([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.){3}([01]?\\d\\d?|2[0-4]\\d|25[0-5])$";
    		    	
    				if(!ip.matches(ip_regex)) {
    	        		isRight=false;
    	    		}
    			}
    		}
    	}
		
		return isRight;
	}
	
	/**
	 * Valida el puerto. Criterio: debe estar entre 0 y 65535
	 * @param port Puerto a evaluar
	 * @return True si es correcto, False de lo contrario
	 */
	public boolean validatePort(String port) {
		boolean isRight;
		
		isRight = true;
    	if(port.equals("")) {
    		isRight = false;
		}else {
			int num=-1;
			try {
				num=Integer.parseInt(port);
			}catch(NumberFormatException e) {
	    		isRight = false;
			}	
			
			if(!(num >=0 && num<=65535)) {
	    		isRight = false;
			}
		}
    	
    	return isRight;
	}
}
