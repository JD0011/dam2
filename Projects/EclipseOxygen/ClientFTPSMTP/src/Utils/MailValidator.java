package Utils;

/**
 * Se encarga de validar el correo electr�nico mediante el m�todo especificado.
 * @author juand
 *
 */
public class MailValidator {

	public boolean validateMail(String mail) {
		boolean isRight;
		
		isRight=false;
		//Tama�o m�nimo
		if(mail.length()>5) {
			
			//Busca arrobas
			int atNumber = 0;
			for(char c: mail.toCharArray()) {
				if(c=='@') {
					atNumber++;
				}
			}
			
			if(atNumber==1) {
				String afterAT= mail.substring(mail.indexOf("@"), mail.length());
				//Comprueba que hay punto despu�s de la arroba
				if( afterAT.contains(".")) {
					isRight=true;
				}
			}
		}
		
		return isRight;
	}
}
