package Utils;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.Transport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.eclipsesource.json.Json;
import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonObject;
import com.eclipsesource.json.JsonValue;

import datamodels.MailUser;
import javafx.util.Pair;

/**
 * Helper para extraer la direcci�n POP3 y SMTP. Esta clase recurre al dominio del correo, el cual,
 * posteriormente es utilizado como key para extraer de un fichero de texto las direcciones
 * SMTP / POP3 correspondiente a dicho dominio. Dicha informaci�n es almacenada en un fichero para poder
 * actualizarala en un futuro ante posibles cambios.
 * @author juand
 *
 */
public class MailDomains {
	
	private static Logger l = LoggerFactory.getLogger(MailDomains.class);
	
	private final File configFile = new File("./mail_domains.json");
	private JsonObject domains;
	private JsonArray providers;
	
	//Properties
	private Properties pop3Props, smtpProps;
	
	public MailDomains() {
		createConfigIfNotExists();
		
		initialize();
	}
	
	/**
	 * Se encarga de dar un estado inicial a las variables que lo necesiten
	 */
	private void initialize() {
		//Propiedades POP3 independientes al usuario
		pop3Props = new Properties();
		pop3Props.put("mail.pop3.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
	    pop3Props.put("mail.pop3.socketFactory.fallback", "false"); 
		
		//Propiedades SMTP independientes al usuario
		smtpProps = new Properties();
		smtpProps.put("mail.smtp.auth", "true");
		smtpProps.put("mail.smtp.starttls.enable", "true");
	}
	
	/**
	 * Realiza un proceso semejante al de hacer ping a la direcci�n de servidor suministrado por el usuario para 
	 * comprobar que es correcta
	 * @param smtp_address Direcci�n SMTP
	 * @param pop3_address Direcci�n POP3
	 * @return Estado de la conexi�n SMTP / POP3
	 */
	public HashMap<String,Boolean> pingServer(MailUser mUser) {
		//Valores por defecto -> Conexi�n fallida. Si conecta -> True
		HashMap<String, Boolean> areConnected = new HashMap<String, Boolean>();
		areConnected.put("pop3", false);
		areConnected.put("smtp", false);
		Thread tConnection = new Thread(new Runnable() {
			
			@Override
			public void run() {
				
				Authenticator auth = new Authenticator() {
					@Override
					protected PasswordAuthentication getPasswordAuthentication() {
						// TODO Auto-generated method stub
						return new PasswordAuthentication(mUser.getUsername(), mUser.getPassword());
					}
				};
				
				//COMPROBACI�N CONEXI�N SMTP
				try {
					smtpProps.put("mail.smtp.host", mUser.getSMTPHostname());
					smtpProps.put("mail.smtp.port", mUser.getSMTPPort());
					
					Session smtpSession = Session.getInstance(smtpProps,auth);
					Transport smtpTransport = smtpSession.getTransport("smtp");
					smtpTransport.connect(mUser.getSMTPHostname(), mUser.getUsername(), mUser.getPassword());
					
					if(smtpTransport.isConnected()) {
						areConnected.put("smtp", true);
						l.info("Conexi�n SMTP: Conectado satisfactoriamente");
					}
					
					smtpTransport.close();
					
			     } catch(MessagingException e) {
			    	 e.printStackTrace();
			    	 l.error("Conexi�n SMTP: Error en la conexi�n");
			     }
				
				//COMPROBACI�N CONEXI�N POP3
				pop3Props.setProperty("mail.pop3.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
				pop3Props.put("mail.pop3.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
				pop3Props.put("mail.pop3.socketFactory.fallback", "false");
				pop3Props.put("mail.store.protocol", "pop3");
				pop3Props.put("mail.pop3.starttls.enable", false);
				pop3Props.put("mail.pop3.socketFactory.port", mUser.getPOP3Port());
				pop3Props.put("mail.pop3.port", mUser.getPOP3Port());
				pop3Props.put("mail.pop3.host", mUser.getPOP3Hostname());
				pop3Props.put("mail.pop3.user", mUser.getUsername());
				pop3Props.put("mail.pop3.ssl.trust", mUser.getPOP3Hostname());
	         
				Session pop3Session = Session.getInstance(pop3Props,auth);

				try {	
					Store store = pop3Session.getStore("pop3");
					store.connect(mUser.getUsername(), mUser.getPassword());
					
					if(store.getFolder("INBOX").exists()) {
						areConnected.put("pop3", true);
						l.info("Conexi�n POP3: Conectado satisfactoriamente");
					}
					store.close();
				} catch (NoSuchProviderException e) {
					l.error("Conexi�n POP3: Error en la conexi�n, no existe el proveedor");
					e.printStackTrace();
				} catch (MessagingException e) {
					l.error("Conexi�n POP3: Error en la conexi�n");
					e.printStackTrace();
				}
			}
		});
		
		tConnection.setName("CONNECTION THREAD");
		tConnection.start();
		
		//Espera a que se haya realizado la comprobaci�n de conexi�n
		try {
			tConnection.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return areConnected;
	}
	
	/**
	 * Crea la plantilla para el archivo de configuraci�n de servidores SMTP / POP3 para el
	 * correspondiente proveedor. A partir de ah�, el usuario va rellenando manualmente servidores
	 * adicionales
	 */
	public void createConfigIfNotExists() {
		if(!configFile.exists()) {
			l.info(configFile.getName() +" no existe, creando archivo con valores por defecto");
			domains = new JsonObject();
			providers = new JsonArray();
			
			JsonObject provider = new JsonObject();
			
			provider.add("provider_name", "gmail.com");
			provider.add("smtp_address", "smtp.gmail.com:587");
			provider.add("pop3_address", "pop.gmail.com:995");
			
			providers.add(provider);
			l.info("Proveedor a�adido: gmail");
			
			provider = new JsonObject();
			provider.add("provider_name", "hotmail.com");
			provider.add("smtp_address", "smtp-mail.outlook.com:587");
			provider.add("pop3_address", "pop-mail.outlook.com:995");
			
			providers.add(provider);
			l.info("Proveedor a�adido: hotmail");
			
			domains.add("providers", providers);
			flush();
		}
	}
	
	/**
	 * Lee el archivo de registro de los servidores SMTP / IMAP de los distintos proveedores
	 * @return
	 */
	private boolean readConfig() {
		l.info("Leyendo archivo de configuraci�n: "+configFile.getAbsolutePath());
		boolean readOK;
		
		if(!configFile.exists()) {
			l.error(configFile.getName() +" no existe");
			readOK = false;
		}else {

			FileReader freader;
			try {
				freader = new FileReader(configFile);
				domains = Json.parse(freader).asObject();
				providers = domains.get("providers").asArray();
				l.info("Archivo leido satisfactoriamente: "+configFile.getName());
			} catch (IOException e) {
				l.error("No se pudo leer el archivo de configuraci�n: "+configFile.getName());
			}
			
			if(domains!=null) {
				l.info("Dominios cargados correctamente");
				readOK = true;
			}else {
				l.error("Los dominios no han podido cargarse. Quiz�s el archivo de configuraci�n est� corrupto");
				l.error("Pruebe a eliminar el archivo: "+configFile.getAbsolutePath());
				readOK = false;
			}
		}
		
		return readOK;
	}
	
	/**
	 * A�ade un nuevo nodo hijo al elemento root del xml {@link #configFile} con los datos
	 * especificados
	 * @param provider_name Nombre del proveedor de correo
	 * @param smtp_address Direcci�n del servidor SMTP
	 * @param pop3_address_address DIrecci�n del servidor IMAP
	 */
	public void saveServer(String provider_name, String smtp_address,int smtp_port, String pop3_address, int pop3_port) {
		readConfig();
		
		l.info("Guardando nuevo servidor SMTP / IMAP: "+provider_name+", "+smtp_address+":"+smtp_port+", "+pop3_address+":"+pop3_port);
		
		JsonObject provider = new JsonObject();
		provider.add("provider_name", provider_name);
		provider.add("smtp_address", smtp_address + ":" + smtp_port);
		provider.add("pop3_address", pop3_address + ":" + pop3_port);
		
		providers.add(provider);
		domains.add("providers", providers);
		
		flush();
	}
	
	/**
	 * Graba la informaci�n contenida en domains en el fichero json {@link #configFile}
	 */
	private void flush() {
		PrintWriter pwriter = null;
		try {
			pwriter = new PrintWriter(configFile);
			pwriter.write(domains.toString());
			l.info("Archivo grabado satisfactoriamente: "+configFile.getAbsolutePath());
		} catch (IOException e) {
			l.error("No se pudo grabar el archivo de configuraci�n: "+configFile.getName());
		}finally {
			pwriter.close();
		}
	}
	
	/**
	 * Obtiene las direcciones SMTP / IMAP a partir de la direcci�n de correo suministrada
	 * @param mail_provider Correo electr�nico del que se quiere obtener las direcciones
	 * @return HashMap con las palabras clave smtp e imap y las direcciones correspondientes
	 */
	public HashMap<String,String> getServerFromProvider(String mail_provider){
		readConfig();
		HashMap<String,String> dirServers = null;
		
		for(JsonValue value : providers) {
			if(value.asObject().get("provider_name").asString().equals(mail_provider)) {
				l.info("Proveedor encontrado, leyendo direcciones SMTP / IMAP");
				dirServers = new HashMap<String,String>();
				dirServers.put("smtp",value.asObject().get("smtp_address").asString());
				dirServers.put("pop3",value.asObject().get("pop3_address").asString());
			}
		}
		
		if(dirServers==null) {
			l.error("No se ha encontrado las direcciones SMTP / IMAP correspondientes al proveedor de correo");
		}
		
		return dirServers;
	}
	
	/**
	 * Comprueba si existe el servidor correspondiente a la compa�ia de correo, determinada
	 * a partir de la direcci�n de correo
	 * @param fromATtoEND
	 * @return True en caso de que exista el servidor, False de lo contrario
	 */
	public boolean existsServer(String mail_provider) {
		readConfig();
		
		boolean exists;
		
		exists = false;
		for(JsonValue prov : providers) {
			if(prov.asObject().get("provider_name").asString().equals(mail_provider)){
				exists = true;
			}
		}
		
		if(exists) {
			l.info("Informaci�n relativa al correo suministrado encontrada");
		}else {
			l.error("No se encontr� ninguna coincidencia con respecto al correo suministrado");
		}
		
		return exists;
	}
	
	/**
	 * Convierte a los proveedores en una lista. Esta lista contiene informaci�n del proveedor y servidores SMTP / IMAP
	 * @return
	 */
	public HashMap<String,Pair<String,String>> providersToList(){
		l.info("Convirtiendo proveedores en lista...");
		HashMap<String,Pair<String,String>> providersList = new HashMap<String,Pair<String,String>>();
		
		for(JsonValue value : providers) {
			JsonObject provider = value.asObject();
			providersList.put(
					provider.get("provider_name").asString(), 
					new Pair<>(
							provider.get("smtp_address").asString(), 
							provider.get("pop3_address").asString()));
		}
		
		return providersList;
	}
}
