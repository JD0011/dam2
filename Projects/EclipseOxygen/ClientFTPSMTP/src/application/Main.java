package application;
	
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Clase principal de la aplicación. Carga la ventana principal.
 * @author juand
 *
 */
public class Main extends Application {
	
	private static Logger l = LoggerFactory.getLogger(Main.class);
	
	private static Stage primaryStage;
	@Override
	public void start(Stage stage) {
		primaryStage = stage;
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("/FXML/MainView.fxml"));
			Parent root = loader.load();

			Scene scene = new Scene(root);
			scene.getStylesheets().add(getClass().getResource("/CSS/application.css").toExternalForm());
			
			primaryStage.setScene(scene);
			primaryStage.show();
			
			l.info("Ventana principal cargada satisfactoriamente");
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	public static Stage getStage() {
		return primaryStage;
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
