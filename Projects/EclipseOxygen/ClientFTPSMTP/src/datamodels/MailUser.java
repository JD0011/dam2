package datamodels;

/**
 * Esta clase es un modelo de datos para almacenar la informaci�n correspondiente al <b>usuario
 * del servicio de correo</b>
 * @author juand
 *
 */
public class MailUser {

	/**
	 * Direcci�n de correo electr�nico del usuario
	 */
	private final String username;
	
	/**
	 * Contrase�a del correo electr�nico del usuario
	 */
	private final String password;
	
	/**
	 * Proveedor de correo extraido a trav�s de la direcci�n de correo electr�nico
	 */
	private final String mail_provider;
	
	/**
	 * Direcci�n del servidor SMTP
	 */
	private String smtp_hostname;
	
	/**
	 * Puerto de conexi�n para direcci�n SMTP
	 */
	private int smtp_port;
	
	/**
	 * Direcci�n del servidor pop3
	 */
	private String pop3_hostname;
	
	/**
	 * Puerto de conexi�n para direcci�n POP3
	 */
	private int pop3_port;

	/**
	 * 
	 * @param username Correo del usuario
	 * @param password Contrase�a del correo
	 */
	public MailUser(String username, String password) {
		this.username = username.toLowerCase();
		this.password = password;
		
		mail_provider = username.substring(username.indexOf('@')+1, username.length());
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	public String getMailProvider() {
		return mail_provider;
	}

	/**
	 * Establece la direcci�n SMTP y extrae el puerto
	 * @param smtp_address_with_port Direcci�n SMTP con puerto
	 */
	public void setSMTPAddress(String smtp_address_with_port) {
		try {
			smtp_hostname = smtp_address_with_port.substring(0, smtp_address_with_port.lastIndexOf(':'));
			smtp_port = Integer.parseInt(smtp_address_with_port.substring(smtp_address_with_port.lastIndexOf(':')+1,smtp_address_with_port.length()));
		}catch(StringIndexOutOfBoundsException e) {
			smtp_hostname = "";
			smtp_port = 0;
		}
	}
	
	/**
	 * Establece la direcci�n POP3 y extrae el puerto
	 * @param pop3_address_with_port Direcci�n POP3 con puerto
	 */
	public void setPOP3Address(String pop3_address_with_port) {
		try {
			pop3_hostname = pop3_address_with_port.substring(0, pop3_address_with_port.lastIndexOf(':'));
			pop3_port = Integer.parseInt(pop3_address_with_port.substring(pop3_address_with_port.lastIndexOf(':')+1,pop3_address_with_port.length()));
		}catch(StringIndexOutOfBoundsException e) {
			pop3_hostname = "";
			pop3_port = 0;
		}
	}

	public String getSMTPHostname() {
		return smtp_hostname;
	}

	public String getPOP3Hostname() {
		return pop3_hostname;
	}
	
	public int getSMTPPort() {
		return smtp_port;
	}

	public int getPOP3Port() {
		return pop3_port;
	}
	
	
	
}
