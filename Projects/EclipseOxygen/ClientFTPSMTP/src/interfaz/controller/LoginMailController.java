package interfaz.controller;


import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.ResourceBundle;

import org.apache.commons.net.imap.IMAPClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import Utils.MailDomains;
import Utils.MailValidator;
import datamodels.MailUser;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

public class LoginMailController implements Initializable{
	
	private static Logger l = LoggerFactory.getLogger(LoginMailController.class);
	
	//Mensajes de errores
	private final String BAD_MAIL_OR_SERVER_ISSUE= 
			"La direcci�n de correo electr�nica no es v�lida, "
			+ "o el servidor SMTP / IMAP no est� registrado. �Desea agregarlo?";
	
	//Interruptores de mail y password
	private boolean mailOK, passwordOK;
	
	//Cliente IMAP
	private IMAPClient imapClient;

    @FXML
    private TextField tf_username;

    @FXML
    private PasswordField pf_userpassword;

    @FXML
    private Button btn_login;
    
    @FXML
    private Button btn_exit;
    
    /**
     * Cierra la aplicaci�n. Es utilizado en el bot�n de salida
     * @param event
     */
    @FXML
    void exitApplication(MouseEvent event) {
    	l.info("Cerrando ventana de aplicaci�n");
    	Stage stage = (Stage) btn_exit.getScene().getWindow();
    	stage.close();
    }
    
    /**
     * Realiza la comprobaci�n de dominio y conecta el programa al servidor SMTP / IMAP.
     * Si la direcci�n del servidor no existe, se pregunta al usuario si quiere agregar
     * una nueva direcci�n.
     * @param event
     */
    @FXML
    void login(MouseEvent event) {
    	Thread tLogin = new Thread(new Runnable() {
			
			@Override
			public void run() {
				l.info("Iniciando proceso de login...");
				Platform.runLater(new Runnable() {
					
					@Override
					public void run() {
						btn_login.setText("Iniciando...");
						btn_login.setDisable(true);
					}
				});
				
				
		    	Alert errorAlert;
		    	MailDomains domainChecker = new MailDomains();
		    	
		    	MailUser mUser = new MailUser(tf_username.getText().toLowerCase(), pf_userpassword.getText());
		    	
		    	//Comprobaci�n servidor de correo
		    	if(!domainChecker.existsServer(mUser.getMailProvider())) {
		    		l.info("El proveedor del correo suministrado no est� registrado");
		    		errorAlert = new Alert(
		    				AlertType.WARNING, 
		    				BAD_MAIL_OR_SERVER_ISSUE, 
		    				ButtonType.YES, ButtonType.NO);
		    		
		    		errorAlert.showAndWait();
		    		
		    		//Si OK, lanza asistente para addici�n de servidor
		    		if(ButtonType.YES.equals(errorAlert.getResult())){
		    			l.info("Iniciando proceso de adici�n de servidor SMTP / IMAP");
		    			
		    			FXMLLoader loader = new FXMLLoader(getClass().getResource("/FXML/AddServerMailView.fxml"));
		    			try {
							
							Parent root = loader.load();
							Scene scene = new Scene(root);
							Stage stage = new Stage();
							
							//Pasa el proveedor de correo al controlador de AddServerMailView.fxml
							AddServerMailController asmController = loader.<AddServerMailController>getController();
							asmController.setMailUser(mUser);
							
							stage.setScene(scene);
							stage.show();
		    			} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
		    			
		    		}
		    	}else {
		    		//a�ade la direcci�n de los servidors SMTP / POP3 almacenados en el fichero al objeto MailUser
		    		HashMap<String,String> addresses = domainChecker.getServerFromProvider(mUser.getMailProvider());
		    		mUser.setPOP3Address(addresses.get("pop3"));
		    		mUser.setSMTPAddress(addresses.get("smtp"));
		    		
		    		HashMap<String, Boolean> canConnect =domainChecker.pingServer(mUser);
		    		if(!canConnect.containsValue(false)) {
		    			
		    			//Abre la ventana del gestor de correo   
		        		Platform.runLater(new Runnable() {
							
							@Override
							public void run() {
				        		FXMLLoader loader = new FXMLLoader(getClass().getResource("/FXML/MailView.fxml"));
				        		
				        		try {
				        			Parent root = loader.load();
				    				Scene scene = new Scene(root);
				    				Stage stage = new Stage();
				    				
				    				MailController mailController = loader.<MailController>getController();
				    				
				    				//Para por par�metros el usuario a conectarse
				    				mailController.setMailUser(mUser);
				    				
				    				stage.setScene(scene);
				    				stage.show();
				    				
				    			} catch (IOException e) {
				    				// TODO Auto-generated catch block
				    				e.printStackTrace();
				    			}
								//Cierra esta ventana
				        		Stage stage = (Stage)btn_login.getScene().getWindow();
				        		stage.close();
							}
						});
		        		
		    		}else {
		    			Platform.runLater(new Runnable() {
							
							@Override
							public void run() {
								Alert wrongUser = new Alert(
				    					AlertType.ERROR,
				    					"El usuario o la contrase�a son incorrectos", 
				    					ButtonType.OK);
				    			wrongUser.showAndWait();
				    			btn_login.setText("Iniciar sesi�n");
				    			btn_login.setDisable(false);
							}
						});
		    			
		    		}
		    	}
			}
		});
    	
    	tLogin.setName("LOGIN THREAD");
    	tLogin.start();
    	
    }
    
    @Override
	public void initialize(URL location, ResourceBundle resources) {
    	l.info("A�adiendo sistema de validaci�n din�mico sobre el usuario y longitud de contrase�a");
		tf_username.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue,
					String newValue) {
				MailValidator mvalidator = new MailValidator();
				
				if(mvalidator.validateMail(tf_username.getText())) {
					mailOK = true;
				}else {
					mailOK = false;
				}
				
				enableLoginIfOK();
			}
		});
		
		pf_userpassword.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if(pf_userpassword.getText().length()>4) {
					passwordOK = true;
				}else {
					passwordOK = false;
				}
				
				enableLoginIfOK();
			}
		});
		
		imapClient = new IMAPClient();
	}
    
    private void enableLoginIfOK() {
    	if(mailOK && passwordOK) {
    		btn_login.setDisable(false);
    	}else{
    		btn_login.setDisable(true);
    	}
    }

}

