package interfaz.controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.Timer;
import java.util.TimerTask;

import javax.mail.Message;
import javax.mail.MessagingException;

import Utils.MailManager;
import datamodels.MailUser;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import javafx.util.Callback;

public class MailController implements Initializable{

	private final ObservableList<Message> listMessages = FXCollections.observableArrayList();
	
	//Tiempo en el que debe refrescarse el buz�n (20 segundos a milisegundos)
	private final long INBOX_REFRESH_INTERVAL = 20*1000;
	
	/**
	 * Interruptor
	 */
	private boolean isRefreshing;
	
	/**
	 * Administrador de correo
	 */
	private MailManager mManager;
	
    @FXML
    private ListView<Message> lv_maillist;

    @FXML
    private Button btn_newmail;

    @FXML
    private Button btn_refresh_inbox;

    @FXML
    private Label lbl_user;

    @FXML
    private Pane pn_workarea;

    @FXML
    private Pane pn_mailwriter;

    @FXML
    private TextField tf_sender;

    @FXML
    private TextField tf_subject;

    @FXML
    private WebView wv_content;

    /**
     * Establece el administrador de correo del usuario logeado
     * @param mUser
     */
    public void setMailUser(MailUser mUser) {
    	mManager= new MailManager(mUser);
    	//Una vez cargado el usuario e inicializado el administrador, realizar operaciones que requieran del usuario
    	refreshInbox(null);
    	
    	String username = mUser.getUsername();
    	lbl_user.setText(username.substring(0, username.indexOf('@')));
    }
    
    //Establecer� valores iniciales en aquellas variables que lo requieran
    @Override
    public void initialize(URL location, ResourceBundle resources) {
    	//Refresca inbox autom�ticamente
    	Timer refreshCaller = new Timer(true); //Ejecutar como servicio
    	TimerTask task = new TimerTask() {
			
			@Override
			public void run() {
				//Llamada a refresh inbox	
				refreshInbox(null);
			}
		};
		
		//Ejecuta la tarea transcurrido x tiempo y cada x tiempo
    	refreshCaller.scheduleAtFixedRate(task, INBOX_REFRESH_INTERVAL, INBOX_REFRESH_INTERVAL);
    	
    	//Listener que detecta el cambio de selecci�n en la lista de correos
    	lv_maillist.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Message>() {

			@Override
			public void changed(ObservableValue<? extends Message> observable, Message oldValue, Message newValue) {
				//Evita actualizar la lista cuando se refresca el buzon de entrada
				if(isRefreshing) {
					return;
				}
				
				try {
					//Actualiza el remitente
					tf_sender.setText(newValue.getFrom()[0].toString());
					
					//Lee el asunto
					tf_subject.setText(newValue.getSubject());

					try {
						StringBuilder content = new StringBuilder();
						
						mManager.readContent(newValue, content);
						wv_content.getEngine().loadContent(content.toString());
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				} catch (MessagingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
    	
    	//Configuraci�n de la lista de correos recibidos
    	lv_maillist.setCellFactory(new Callback<ListView<Message>, ListCell<Message>>() {

			@Override
			public ListCell<Message> call(ListView<Message> param) {
				ListCell<Message> cell = new ListCell<Message>() {
					@Override
					protected void updateItem(Message item, boolean empty) {
						super.updateItem(item, empty);
                        if (item != null) {
                            try {
								setText(item.getSubject());
							} catch (MessagingException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
                        }
					}
				};
				return cell;
			}
		});
    	
    	//Establece el origen de datos del listview de correos recibidos
    	lv_maillist.setItems(listMessages);
    }
    
    @FXML
    void openNewMessageWindow(MouseEvent event) {
    	FXMLLoader loader = new FXMLLoader(getClass().getResource("/FXML/NewMailView.fxml"));
		try {
			Parent root = loader.load();
			Scene scene = new Scene(root);
	    	Stage stage = new Stage();
	    	stage.setScene(scene);
	    	stage.show();
	    	
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		NewMailController nmController = loader.<NewMailController>getController();
		nmController.setMailManager(mManager);
    }

    @FXML
    void refreshInbox(MouseEvent event) {
    	if(!isRefreshing) {
    		isRefreshing = true;
    		Thread tRefreher = new Thread(new Runnable() {
    			
    			@Override
    			public void run() {
					Platform.runLater(new Runnable() {
						
						@Override
						public void run() {
							btn_refresh_inbox.setDisable(true);
						}
					});
    	    		
    	    		
    		    	Message[] messages = mManager.listMails();
    		    	
    		    	//Actualiza la interfaz desde el hilo principal
    		    	Platform.runLater(new Runnable() {
    					@Override
    					public void run() {
    						listMessages.clear();
    						lv_maillist.refresh();
    				    	for(Message m: messages) {
    				    		listMessages.add(m);
    				    	}
    						
    						isRefreshing = false;
    						//Reactiva bot�n
    						btn_refresh_inbox.setDisable(false);
    					}
    				});
    		    	
    			}
    		});
    		
    		tRefreher.setName("INBOX REFRESHER");
        	tRefreher.start();
    	}
    	
    }
    
    /**
	 * Cierra la ventana del gestor de correo
	 * @param event
	 */
	@FXML
    void closeWindow(MouseEvent event) {
		Stage stage = (Stage) btn_refresh_inbox.getScene().getWindow();
		stage.close();
    }

}
