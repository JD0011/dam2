package interfaz.controller;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import Utils.MailDomains;
import datamodels.MailUser;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

public class AddServerMailController {
	
	private static Logger l = LoggerFactory.getLogger(AddServerMailController.class);
	
	/**
	 * Usuario a registrar las direcciones SMTP / POP3
	 */
	private MailUser mUser;

    @FXML
    private TextField tf_smtp_dir;

    @FXML
    private TextField tf_imap_dir;

    @FXML
    private Button btn_accept;

    @FXML
    private Button btn_cancel;

    /**
     * Establece el proveedor de correo.
     * @param mail_provider
     */
    void setMailUser(MailUser mUser) {
    	this.mUser = mUser;
    }
    
    /**
     * Cierra la ventana actual. Se utiliza en el bot�n de cancelar
     * @param event
     */
    @FXML
    void cancelClick(MouseEvent event) {
    	Stage stage = (Stage)btn_cancel.getScene().getWindow();
    	stage.close();
    	
    	l.info("Ventana cerrada");
    }

    /**
     * Registra la direcci�n suministrada por el usuraio en el archivo de direcciones SMTP / IMAP administrado por {@link MailDomains}
     * @param event
     */
    @FXML
    void registerAddress(MouseEvent event) {
    	l.info("Procesando servidores SMTP / IMAP suministrados...");

    	if(!tf_smtp_dir.getText().equals("") && !tf_imap_dir.getText().equals("")) {
    		mUser.setSMTPAddress(tf_smtp_dir.getText());
        	mUser.setPOP3Address(tf_imap_dir.getText());
    	}
    	
    	Alert notifier = new Alert(AlertType.INFORMATION,"Comprobando servidores...");
    	
    	notifier.show();
    	
    	MailDomains mdManager = new MailDomains();
    	HashMap<String, Boolean> areConnected = mdManager.pingServer(mUser);
    	
    	notifier.hide();
    	
    	//Notificaci�n al usuario de la correctitud de los datos
    	String errorMSG="";
    	if(!areConnected.get("smtp")) {
    		tf_smtp_dir.getStyleClass().add("wrong_input");
    		
    		errorMSG = "No se ha podido conectar al servidor SMTP. Compruebe la direcci�n o la contrase�a de usuario";
    	}
    	
    	if(!areConnected.get("pop3")) {
    		tf_imap_dir.getStyleClass().add("wrong_input");
    		errorMSG = "No se ha podido conectar al servidor POP3. Compruebe la direcci�n o la contrase�a de usuario";
    	}
    	
    	if(!areConnected.get("pop3") && !areConnected.get("smtp")) {
    		tf_smtp_dir.getStyleClass().add("wrong_input");
    		tf_imap_dir.getStyleClass().add("wrong_input");
    		
    		errorMSG = "No se ha podido conectar a ning�n servidor. Compruebe las direcciones o la contrase�a de usuario";
    	}
    	
    	if(!errorMSG.equals("")) {
    		
    		//Notifica del error al usuario
    		Alert errorAlert = new Alert(AlertType.ERROR,errorMSG, ButtonType.OK);
    		errorAlert.showAndWait();
    		
    	}else {
    		
    		//Agrega el servidor
    		mdManager.saveServer(
    				mUser.getMailProvider(), 
    				mUser.getSMTPHostname(), 
    				mUser.getSMTPPort(), 
    				mUser.getPOP3Hostname(), 
    				mUser.getPOP3Port());
    		
    		//Cierra la ventana
    		Stage stage = (Stage) btn_accept.getScene().getWindow();
    		stage.close();
    	}
    	
    }

}
