package interfaz.controller;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.ResourceBundle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sun.xml.internal.bind.v2.schemagen.xmlschema.List;
import com.sun.xml.internal.ws.policy.privateutil.PolicyUtils.Collections;

import Utils.MailManager;
import Utils.MailValidator;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

/**
 * Controlador de la vista NewMailController.</br>
 * Este controlador se encarga de <b>enviar el mensaje introducido</b> por el usuario al destinatario 
 * especificado por el mismo
 * @author juand
 *
 */
public class NewMailController implements Initializable{
	
	private static Logger l = LoggerFactory.getLogger(NewMailController.class);
    
	private final String CONFIRM_MSG = "Mensaje enviado correctamente a su destinatario";
	private final String BAD_MSG = "El mensaje no pudo ser enviado. Compruebe que la direcci�n es v�lida";
    private ChangeListener<String> changeTextListener;
	
	/**
	* Se utilizar� para interactura con el correo electr�nico del usuario
	*/
	private MailManager mManager;

	@FXML
    private Pane pn_workarea;

    @FXML
    private Pane pn_mailwriter;

    @FXML
    private TextField tf_destinatary;

    @FXML
    private TextField tf_subject;

    @FXML
    private TextArea ta_content;

    @FXML
    private Button btn_send_message;
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
    	changeTextListener = new ChangeListener<String>() {
			
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if(tf_destinatary.getText().length()>5 && ta_content.getText().length()>1) {
					btn_send_message.setDisable(false);
				}else {
					btn_send_message.setDisable(true);
				}
			}
		};
    	
    	tf_destinatary.textProperty().addListener(changeTextListener);
    	ta_content.textProperty().addListener(changeTextListener);
    }
    
    /**
     * Env�a el mensaje escrito con el asunto establecido a los destinatarios especificados
     * @param event
     */
    @FXML
    void sendMessage(MouseEvent event) {
    	l.info("Procesando mensaje a enviar...");
    	ArrayList<String> mails = new ArrayList<String>();
    	ArrayList<String> wrongMails = new ArrayList<String>();
    	if(!tf_destinatary.getText().equals("") && !ta_content.getText().equals("")) {
    		String mail = tf_destinatary.getText();
    		//Comprobaci�n de correo
    		if(mail.length()> 10 && mail.contains(".")) {
    			
    			MailValidator mValidator = new MailValidator();
    			
    			//Si hay m�ltiples destinatarios
    			if(mail.contains(";")) {
    				
    				//A�adelos, comprob�ndolos previamente
    				for(String m: mail.split(";")) {
    					if(mValidator.validateMail(m)) {
    						mails.add(m);
    					}else {
    						wrongMails.add(m);
    					}
    				}
    			}else {
    				if(mValidator.validateMail(mail)) {
    					mails.add(tf_destinatary.getText());
    				}else {
    					wrongMails.add(mail);
    				}
    				
    			}
    		}else {
    			wrongMails.add(tf_destinatary.getText());
    		}
    	}
    	
    	//Alerta en caso de que haya email's incorrectos
		if(wrongMails.size()!=0) {
			l.error("Algunas direcciones de correo son err�neas. Cancelando el env�o...");
			String wrongMailsChain = "";
			for(int i = 0; i<wrongMails.size(); i++) {
				if(i%2!=0 && i!=wrongMails.size()-1) {
					wrongMailsChain+=", ";
				}
				wrongMailsChain+=wrongMails.get(i);
			}
			Alert errorAlert = new Alert(AlertType.ERROR,"El/los siguiente/s correo/s no es/som v�lido/s: "+wrongMailsChain);
			errorAlert.showAndWait();
		}else {
			
			Alert resultAlert;
			//Env�a el mensaje
			if(mManager.sendMessage(mails.toArray(new String[mails.size()]), tf_subject.getText(), ta_content.getText())) {
				l.info("Los datos suministrados para el mensaje son correctos. Cerrando ventana...");
				
				Stage stage = (Stage) btn_send_message.getScene().getWindow();
				stage.close();
				resultAlert = new Alert(AlertType.CONFIRMATION, CONFIRM_MSG, ButtonType.OK);
				resultAlert.showAndWait();
			}else {
				l.error("Algo fall� al enviar el mensaje. Compruebe su conexi�n a internet");
				resultAlert = new Alert(AlertType.ERROR, BAD_MSG, ButtonType.OK);
			}
			
			
		}
    }

    /**
     * Establece el administrador de correo necesario para poder enviar nuevos mensajes
     * @param mManager
     */
	public void setMailManager(MailManager	mManager) {
		this.mManager = mManager;
	}
}

