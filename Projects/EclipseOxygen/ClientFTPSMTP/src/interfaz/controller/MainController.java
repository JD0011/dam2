package interfaz.controller;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

/**
 * Abrir� el panel principal, el cu�l permite elegir abrir entre un cliente FTP o un cliente 
 * de correo. Instanciar� la ventana correspondiente a la opci�n disponible
 * mostrada en los botones.
 * @author juand
 *
 */
public class MainController {
	
	private static Logger l = LoggerFactory.getLogger(MainController.class);

    @FXML
    private AnchorPane apMainPane;
    
    @FXML
    private Button btnFTP;

    @FXML
    private Button btnMail;
    
    //Stage principal de la ventana
    private Stage main;
    
    /**
     * Cierra la ventana actual y abre la interf�z del cliente FTP.
     * @param event
     */
    @FXML
    void openFTPUI(MouseEvent event) {
    	l.info("Abriendo interfaz FTP...");
    	//Cierra ventana
    	main = (Stage) apMainPane.getScene().getWindow();
    	main.close();
    	 
    	try {
			Parent root = FXMLLoader.load(getClass().getResource("/FXML/FTPView.fxml"));
			Scene scene = new Scene(root);
			
			Stage stage = new Stage();
			stage.setScene(scene);
			stage.show();
			
			l.info("Interfaz FTP abierta satisfactoriamente");
		} catch (IOException e) {
			l.error("Error al cargar los recursos");
			e.printStackTrace();
		}
    }

    /**
     * Cierra la ventana actual y abre la interf�z del cliente de correo
     * @param event
     */
    @FXML
    void openMailUI(MouseEvent event) {
    	l.info("Abriendo interfaz MAIL...");
    	//Cierra ventana
    	main = (Stage) apMainPane.getScene().getWindow();
    	main.close();
    	
    	try {
			Parent root = FXMLLoader.load(getClass().getResource("/FXML/LoginMailView.fxml"));
			Scene scene = new Scene(root);
			
			Stage stage = new Stage();
			stage.setScene(scene);
			stage.show();
			
			l.info("Interfaz MAIL abierta satisfactoriamente");
		} catch (IOException e) {
			l.error("Error al cargar los recursos");
			e.printStackTrace();
		}
		
    }

}
