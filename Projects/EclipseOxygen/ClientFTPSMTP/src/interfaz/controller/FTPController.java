package interfaz.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.SocketException;
import java.net.URL;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.ArrayList;
import java.util.ResourceBundle;

import javax.swing.JFileChooser;

import org.apache.commons.net.ProtocolCommandEvent;
import org.apache.commons.net.ProtocolCommandListener;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import Utils.FTPValidator;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;

public class FTPController implements Initializable{
	
	private static Logger l = LoggerFactory.getLogger(FTPController.class);
	
	//Datos de conexi�n
	private String hostname;
	private String username;
	private String password;
	private int port;
	
	private FTPClient ftpClient;
	private ObservableList<FileData> remotefiles_data = FXCollections.observableArrayList();
	private ObservableList<FileData> record_data = FXCollections.observableArrayList();
	
	//Thread de descarga de carpeta
	private Thread ftpFolderDownloadThread;
	//Archivo a enviar
	private File toSendFile;
	//FileData elegido de la lista de ficheros remotos
	private FileData selectedItemFromRemote;
	
	//Carpeta de descarga por defecto
	private File downloadFolder = new File("./Descargas");
	
	//Interruptor para actualizar ficheros
	private boolean interruptor = true;

	@FXML
    private Pane pnMain;

    @FXML
    private TextField tf_ipaddress;

    @FXML
    private TextField tf_port;

    @FXML
    private TextField tf_user;

    @FXML
    private PasswordField pf_password;
    
    @FXML
    private Button btn_sendfile;

    @FXML
    private ToggleButton btn_delete;
    
    @FXML
    private TextField tf_downloadfolder;

    @FXML
    private Button btn_selectfolder;

    @FXML
    private Label tf_status;

    @FXML
    private TableView<FileData> tv_record;

    @FXML
    private TableColumn<FileData, String> record_columnStream;
    
    @FXML
    private TableColumn<FileData, String> record_columnDate;

    @FXML
    private TableColumn<FileData, String> record_columnType;

    @FXML
    private TableColumn<FileData, String> record_columnName;

    @FXML
    private TableColumn<FileData, String> record_columnPath;

    @FXML
    private TitledPane tp_transfer_status;

    @FXML
    private TextField lbl_filename;

    @FXML
    private TextField tf_tosendpath;

    @FXML
    private TextField lbl_size;

    @FXML
    private Pane pnRemoteExplorer;

    @FXML
    private TableView<FileData> tv_remoteFiles;

    @FXML
    private TableColumn<FileData, String> remotefiles_columnType;

    @FXML
    private TableColumn<FileData, String> remotefiles_columnName;

    @FXML
    private Button btn_requestfile;

    @FXML
    private ToggleButton btn_delete_selected_file;
    
    @FXML
    private Button btn_delete_current_folder;
    
    @FXML
    private Button btn_connect;
    
    @FXML
    private Button btn_disconnect;

    @FXML
    private TextField tf_directorypath;

    /**
     * Elimina el fichero seleccionado en el explorador de archivos
     * @param event
     */
    @FXML
    void deleteSelectedFile(MouseEvent event) {
    	FileData toDelete = tv_remoteFiles.getSelectionModel().getSelectedItem();

    	l.info("Eliminando el fichero: "+toDelete.getName());
    	Thread ftpThread = new Thread(new Runnable() {
			
			@Override
			public void run() {
				try {
					ftpClient.dele("/"+toDelete.getName());
					l.info("Archivo eliminado satisfactoriamente: "+toDelete.getName());
				} catch (IOException e) {
					showLostConnectionError();
				}
			}
		});
    	
    	ftpThread.setName("FTP DELETE");
    	ftpThread.start();
    }
    
    //Inicializa aquellas variables que lo requieran
    @Override
	public void initialize(URL location, ResourceBundle resources) {
    	l.info("Inicializando algunos componentes de la interfaz...");
    	//Inicializa el tipo de dato de las columnas
		remotefiles_columnType.setCellValueFactory(new PropertyValueFactory<FileData,String>("type"));
		remotefiles_columnName.setCellValueFactory(new PropertyValueFactory<FileData,String>("name"));
		
		record_columnDate.setCellValueFactory(new PropertyValueFactory<FileData,String>("date"));
		record_columnName.setCellValueFactory(new PropertyValueFactory<FileData,String>("name"));
		record_columnPath.setCellValueFactory(new PropertyValueFactory<FileData,String>("path"));
		record_columnType.setCellValueFactory(new PropertyValueFactory<FileData,String>("type"));
		record_columnStream.setCellValueFactory(new PropertyValueFactory<FileData,String>("action"));
		
		if(!downloadFolder.exists()) {
			l.info("Creando carpeta de descargas en "+downloadFolder.getAbsolutePath());
			downloadFolder.mkdir();
		}
		
		tv_record.setItems(record_data);
		
		tf_downloadfolder.setText(downloadFolder.getAbsolutePath());
		
		//Establece la l�gica al hacer click sobre un item en la tabla de ficheros
    	tv_remoteFiles.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<FileData>() {

			@Override
			public void changed(ObservableValue<? extends FileData> observable, FileData oldValue, FileData newValue) {
				//Desactiva el bot�n de eliminar fichero
				btn_delete_selected_file.setDisable(true);
				
				//Evita que se ejecute al limpiar la lista y si el valor elegido es un fichero
				if(interruptor) {
					if(newValue==null || oldValue==newValue || newValue.getType()=="F") {
						btn_delete_selected_file.setDisable(false);
						return; 
					}
					interruptor=false;
					
				}else {
					interruptor=true;
					return;
				}
				
				l.info("Nuevo directorio seleccionado");
				
				FTPFile[] ftpFiles = null;
				ftpClient.enterLocalPassiveMode();
				
				selectedItemFromRemote = newValue;
				
				if(newValue.getType()=="D") {
					l.info("Accediendo a la carpeta seleccionada...");
					try {
						ftpClient.changeWorkingDirectory("./"+newValue.getName()+"/");
						ftpFiles = ftpClient.listFiles();
					} catch (IOException e) {
						showLostConnectionError();
						e.printStackTrace();
					}
				}
				
				try {
					tf_directorypath.setText(ftpClient.printWorkingDirectory());
					l.info("Directorio remoto actual actualizado: "+tf_directorypath.getText());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				//Limpia la tabla despu�s de la manipulaci�n de los datos
		    	final FTPFile[] files= ftpFiles;
		    	Platform.runLater(new Runnable() {
		    	    @Override 
		    	    public void run() {
				    	remotefiles_data.clear();
				    	//En caso de directorio, volcado de informaci�n de ficheros en la tabla 
						if(newValue.getType()=="D") {
					    	
					    	for(FTPFile f: files) {
					    		String type;
					    		if(f.isDirectory()) {
					    			type="D";
					    		}else {
					    			type="F";
					    		}
								remotefiles_data.add(new FileData(type, f.getName(), f.getSize()));
					    	}
					    	tv_remoteFiles.setItems(remotefiles_data);
					    	l.info("Explorador de archivos actualizado con nuevos ficheros");
						}
					}
	    	    });
			}
		});
	}
    
    /**
     * Conectar� el cliente con el servidor, despu�s de haber realizado las correspondientes comprobaciones
     * de datos. Si estos no son correctos, se mostrar� un mensaje notificando el error al usuario.
     * @param event
     */
    @FXML
    void connectToServer(MouseEvent event) {
    	l.info("Conectando al servidor FTP...");
    	ArrayList<Boolean> correcto = new ArrayList<Boolean>();
    	Alert error;    	

    	FTPValidator validator = new FTPValidator();
    	
    	//VERIFICACI�N DE DATOS
    	correcto.add(validator.validateIPAddress(tf_ipaddress.getText()));
    	correcto.add(validator.validatePort(tf_port.getText()));
    	correcto.add(validator.validateUsername(tf_user.getText()));
    	correcto.add(validator.validatePassword(pf_password.getText()));
    	
    	if(correcto.contains(false)) {
    		l.error("Algunos de los datos suministrados no son correctos");
    		error = new Alert(
    				AlertType.ERROR, 
    				"Algunos de los datos suministrados son incorrectos. Revise los par�metros", 
    				ButtonType.OK);
    		error.show();
    		
    	}else {
    		ftpClient = new FTPClient();
    		
    		//Imprime los comandos ejecutados
        	ftpClient.addProtocolCommandListener(new ProtocolCommandListener() {
    			
    			@Override
    			public void protocolReplyReceived(ProtocolCommandEvent arg0) {
    				System.out.println(arg0.getMessage());
    				//Modificaciones en la interfaz han de realizarse en el hilo principal
    				Platform.runLater(new Runnable() {
						
						@Override
						public void run() {
							tf_status.setText("Recibido: "+arg0.getMessage());
						}
					});
    			}
    			
    			@Override
    			public void protocolCommandSent(ProtocolCommandEvent arg0) {
    				System.out.println(arg0.getMessage());
    				//Modificaciones en la interfaz han de realizarse en el hilo principal
    				Platform.runLater(new Runnable() {
						
						@Override
						public void run() {
							tf_status.setText("Enviado: "+arg0.getMessage());
						}
					});
    			}
    		});
        	
        	//Conecta al servidor
    		try {
    			hostname = tf_ipaddress.getText();
    			port = Integer.parseInt(tf_port.getText());
    			username = tf_user.getText();
    			password = pf_password.getText();
    			
				ftpClient.connect(hostname, port);
				
				boolean isLogged = ftpClient.login(username, password);
				if(!isLogged){
					Alert wrongLogin = new Alert(
							AlertType.WARNING,
							"Los datos de usuario son incorrectos",
							ButtonType.OK);
					wrongLogin.showAndWait();
				}else {
					l.info("Conexi�n y logeo realizados correctamente sobre la direcci�n "+tf_ipaddress.getText());
		    		//Activa panel de FTP
		    		pnMain.setDisable(false);
		    		
		    		//Alterna botones
					btn_connect.setDisable(true);
					btn_disconnect.setDisable(false);
					
		    		fillTableView();
				}
			} catch (NumberFormatException | IOException e) {
				showLostConnectionError();
			}
    	}
    	
    }
    
    /**
     * Se encarga de cerrar la conexi�n con el servidor ftp
     * @param event
     */
    @FXML
    void disconnect(MouseEvent event) {
    	if(ftpClient.isConnected()) {
    		try {
        		//Aborta, si se est� ejecutando, cualquier operaci�n con el servidor FTP
    			ftpClient.abor();
    			//Desconecta
    	    	ftpClient.disconnect();
    	    	pnMain.setDisable(true);
    	    	
    	    	record_data.clear();
    	    	remotefiles_data.clear();
    	    	
    	    	//Alterna botones
    			btn_connect.setDisable(false);
    			btn_disconnect.setDisable(true);
    		} catch (IOException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
    	}
    }

    /**
     * Abre el FileChooser para establecer la carpeta de descargas
     * @param event
     */
    @FXML
    void openDirectoryChooser(MouseEvent event) {
    	l.info("Abriendo JFileChooser para establecer la carpeta de descargas");
    	JFileChooser fc = new JFileChooser();
    	fc.setDialogTitle("Selecciona la carpeta de descargas");
    	fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
    	int answer = fc.showOpenDialog(null);
    	
    	File aux=null;
    	
    	if(JFileChooser.APPROVE_OPTION == answer) {
    		aux=fc.getSelectedFile();
    		if(aux!=null) {
    			downloadFolder = aux;
    			
    			l.info("Nueva carpeta de descargas: "+ downloadFolder.getAbsolutePath());
    			tf_downloadfolder.setText(downloadFolder.getAbsolutePath());
			}
    	}
    }

    /**
     * Descarga el archivo seleccionado en la lista desplegable en la ubicaci�n especificada
     * @param event
     */
    @FXML
    void requestFile(MouseEvent event) {
		l.info("Descargando archivo...");
    	//Comprueba que hay un fichero elegido antes de proceder. Si no, sale del m�todo
    	FileData selectedFile=tv_remoteFiles.getSelectionModel().getSelectedItem();
    	if(selectedFile==null) {
    		l.error("No se ha seleccionado ning�n archivo en el explorador. Cancelando...");
    		return;
    	}
    	
    	//Archivo donde se almacena el fichero descargado
    	File downloadedFile= new File(downloadFolder.toPath()+"/"+selectedFile.name.getValue()) ;
    	
    	Thread ftpThread = new Thread(new Runnable() {
			
			@Override
			public void run() {

				//Cambia el texto del bot�n
				Platform.runLater(new Runnable() {
					@Override
					public void run() {
						btn_requestfile.setDisable(true);
					}
				});
				
				FileOutputStream fos = null;
				try {
					fos = new FileOutputStream(downloadedFile);
					ftpClient.retrieveFile(selectedFile.getName(), fos);
					l.info("Archivo descargado correctamente: "+selectedFile.getName());
					
					//A�ade el fichero al historial
					Platform.runLater(new Runnable() {
						
						@Override
						public void run() {
							fillRecordWith(downloadedFile, "Descarga");
							btn_requestfile.setDisable(false);
						}
					});
					
				} catch (IOException e) {
					showLostConnectionError();
					l.error("No se pudo descargar el archivo "+selectedFile.getName());
					l.info("Compruebe su conexi�n de internet y/o el estado del servidor");
				}finally {
					try {
						fos.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
    	});
    	
    	ftpThread.setName("FTP DOWNLOADER");
    	ftpThread.start();
    }

    /**
     * Comprueba que el archivo especificado en el textField filepath existe. Si existe, lo env�a con el mismo
     * nombre al servidor conectado.
     * @param event
     */
    @FXML
    void sendFile(MouseEvent event) {
    	l.info("Enviando archivo al servidor remoto...");
    	
    	JFileChooser fc = new JFileChooser();
    	fc.setDialogTitle("Elija el archivo a enviar");
    	fc.setFileSelectionMode(JFileChooser.FILES_ONLY); 
    	
    	int answer=fc.showOpenDialog(fc);
    	
    	if(JFileChooser.APPROVE_OPTION == answer) {
    		toSendFile = fc.getSelectedFile();
    		
    		l.info("Archivo a enviar: "+toSendFile.getAbsolutePath());
    		
    		lbl_filename.setText(toSendFile.getName());
    		tf_tosendpath.setText(toSendFile.getAbsolutePath());
    		lbl_size.setText(String.valueOf((toSendFile.length()/1048576)/1024)+ "mb");
    	}else {
    		l.info("Se ha cancelado el proceso de env�o");
    		return;
    	}
    	
    	if(toSendFile ==null || !toSendFile.exists()) {
    		l.error("El archivo seleccionado est� corrupto. Cancelando...");
    		return;
    		}
    	
    	//Proceso de env�o del fichero		
		ftpClient.enterLocalPassiveMode();
		try {
			ftpClient.setFileType(FTPClient.BINARY_FILE_TYPE);
			
    		//Asilamiento del proceso en un hilo
			Thread ftpThread = new Thread(new Runnable() {
				@Override
				public void run() {
					FileInputStream fis = null;
					try {
						fis = new FileInputStream(toSendFile);
						ftpClient.storeFile(toSendFile.getName(), fis);
						
						//Comprueba el c�digo de respuesta
						if(ftpClient.getReplyCode()==226) {
							//A�ade fichero al historial
							fillRecordWith(toSendFile, "Subida");
						}else {
							//Ejecutar en hilo de la interfaz
							Platform.runLater(new Runnable() {
								
								@Override
								public void run() {
									Alert warning = new Alert(
											AlertType.WARNING, 
											"No se ha podido enviar el archivo. �Tiene permisos de escriutra?",
											ButtonType.OK);
									warning.showAndWait();
								}
							});
						}
						
					} catch (IOException e) {
						showLostConnectionError();
					}finally {
						try {
							fis.close();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
			});
			
			ftpThread.setName("FTP UPLOADER");
			ftpThread.start();

    		l.info("Enviando archivo seleccionado...");
			
		} catch (FileNotFoundException e) {
			l.error("Archivo no encontrado. Quiz�s ha sido eliminado durante el proceso de env�o ("+toSendFile.getAbsolutePath()+")");
		} catch (IOException e) {
			l.error("IO Exception: �est� siendo utilizado el archivo por otro proceso?");
			showLostConnectionError();
		}finally {
		}
    }
    
    /**
     * Retrocede una carpeta en el servidor
     * @param event
     */
    @FXML
    void gobackOnServer(MouseEvent event) {
		l.info("Retrocediendo a la carpeta anterior");
		FTPFile[] ftpFiles= null;
		try {
			ftpClient.changeWorkingDirectory("../");
			ftpFiles = ftpClient.listFiles();
		} catch (IOException e) {
			showLostConnectionError();
			e.printStackTrace();
		}
		
		try {
			tf_directorypath.setText(ftpClient.printWorkingDirectory());
			l.info("Directorio remoto actual actualizado: "+tf_directorypath.getText());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//Limpia la tabla despu�s de la manipulaci�n de los datos
    	final FTPFile[] files= ftpFiles;
    	Platform.runLater(new Runnable() {
    	    @Override 
    	    public void run() {
		    	remotefiles_data.clear();
		    	//Volcado de informaci�n de ficheros en la tabla 
			    	
			    	for(FTPFile f: files) {
			    		String type;
			    		if(f.isDirectory()) {
			    			type="D";
			    		}else {
			    			type="F";
			    		}
						remotefiles_data.add(new FileData(type, f.getName(), f.getSize()));
			    	}
			    	tv_remoteFiles.setItems(remotefiles_data);
			    	l.info("Explorador de archivos actualizado con nuevos ficheros");
				
			}
	    });
    }
    
    /**
     * Rellena la tabla con los datos del directorio remoto
     */
    private void fillTableView() {
    	l.info("Visualizando los archivos remotos en el explorador...");
    	
    	FTPFile[] ftpFiles=null;
    	
    	try {
    		ftpClient.enterLocalPassiveMode();
    		tf_directorypath.setText(ftpClient.printWorkingDirectory());
			ftpFiles=ftpClient.listFiles();
			
			if(ftpClient.getReplyCode()==451) { // 451 Can't open directory "/".
				Alert warning = new Alert(AlertType.WARNING,"No se puede listar el directorio", ButtonType.OK);
				warning.showAndWait();
			}
			
			l.info("Lista de archivos obtenida");
		} catch (IOException e1) {
			l.error("IO Exception: � problemas de conexi�n?");
			e1.printStackTrace();
		}
    	
    	//Carga una primera vez
    	for(FTPFile f: ftpFiles) {    		
    		String type;
    		if(f.isDirectory()) {
    			type="D";
    		}else {
    			type="F";
    		}
			remotefiles_data.add(new FileData(type, f.getName(), f.getSize()));
    	}
    	
    	tv_remoteFiles.setItems(remotefiles_data);
    	
    } 
	
    /**
     * Rellena el historial de archivos enviados con el �ltimo fichero enviado o recibido
     * @param target Archivo que se ha enviado o recibido
     * @param direction Direcci�n del fichero, es decir, si se sube o se descarga
     */
	private void fillRecordWith(File target, String direction) {

		record_data.add(new FileData(
				target.isDirectory()? "D" : "F", 
				target.getName(), 
				target.length(), 
				target.getPath(), 
				LocalDateTime.now(),
				direction)
				);
		
		l.info("Nuevo fichero a�adido al historial");
	}
	
	/**
	 * Muestra el error correspondiente a la perdida de conexi�n con el servidor
	 */
	private void showLostConnectionError() {
		Platform.runLater(new Runnable() {
			
			@Override
			public void run() {
				tf_status.setText("Conexi�n perdida");
				Alert connectionError = new Alert(
						AlertType.ERROR, 
						"La conexi�n con el servidor ha fallado. �El servidor est� en l�nea?", 
						ButtonType.OK);
				connectionError.show();
				l.info("Conexi�n fallida. Revise la informaci�n suministrada o el estado del servidor");
				
				//Desactiva panel de FTP
	    		pnMain.setDisable(true);
			}
		});
		
		
		
	}
	
	/**
	 * Esta clase proporciona informaci�n relevante sobre los ficheros remotos. Es utilizada como mecanismo de representaci�n de los ficheros
	 * en el explorador de archivos y en el historial de transferencia, adem�s de en el indicador de propiedades del fichero a enviar.
	 * @author juand
	 *
	 */
    public class FileData{
    	
    	private final SimpleStringProperty type;
    	private final SimpleStringProperty name;
    	private final SimpleStringProperty size;
    	private final SimpleStringProperty path;
    	private final SimpleStringProperty date;
    	private final SimpleStringProperty action;
    	
    	public FileData(String type, String name, long sizeOnBytes) {
    		this.type = new SimpleStringProperty(type);
    		this.name = new SimpleStringProperty(name);
    		this.size = new SimpleStringProperty(String.valueOf(sizeOnBytes/1204 + "kb"));
    		
        	this.path = null;
        	this.date = null;
        	this.action = null;
		}
    	
    	public FileData(String type, String name, long sizeOnBytes, String path) {
    		this.type = new SimpleStringProperty(type);
    		this.name = new SimpleStringProperty(name);
    		this.size = new SimpleStringProperty(String.valueOf(sizeOnBytes/1204 + "kb"));
        	this.path = new SimpleStringProperty(path);
        	
        	this.date = null;
        	this.action = null;
		}
    	
    	public FileData(String type, String name, long sizeOnBytes, String path, LocalDateTime date, String action) {
    		this.type = new SimpleStringProperty(type);
    		this.name = new SimpleStringProperty(name);
    		this.size = new SimpleStringProperty(String.valueOf(sizeOnBytes/1204 + "kb"));
    		this.path = new SimpleStringProperty(path);
    		DateTimeFormatter dtf = DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM);
    		this.date = new SimpleStringProperty(dtf.format(date));
    		this.action = new SimpleStringProperty(action);
		}
        
        public String getType() {
            return type.get();
        }
            
        public String getName() {
            return name.get();
        }
        
    	public String getSize() {
            return size.get();
        }
    	
    	public String getPath() {
    		return path.get();
    	}
    	
    	public String getDate() {
    		return date.get();
    	}
    	
    	public String getAction() {
    		return action.get();
    	}
    }	
}