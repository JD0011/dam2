

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

public class EnviarFicheroSockets {
	
	private static Socket cs;
	private static ServerSocket server;
	private static Thread tServer;

	
	public static void main(String[] args) {

		tServer = new Thread(new Runnable() {
			
			@Override
			public void run() {
				try {
					server = new ServerSocket(29530);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				while(true) {
					try {
						cs = server.accept();
						System.out.println("Conexi�n recibida desde: "+cs.getInetAddress());
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					
					DataInputStream dis = null;
					try {
						dis = new DataInputStream(cs.getInputStream());
						System.out.println("Flujo de lectura abierto");
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					System.out.println("Recibiendo datos ");
					FileOutputStream fos = null;
					try {
						File output = new File("C:/Users/user/Desktop/resumen_recibido.txt");
						fos = new FileOutputStream(output);
						System.out.println("Recibiendo resumen");
						
						while(true) {
							byte b;
							if((b = dis.readByte())!=-1) {
								System.out.print(". ");
								fos.write(b);
							}else {
								break;
							}
						}
						
						fos.close();
						
						output = new File("C:/Users/user/Desktop/fichero_recibido.txt");
						fos = new FileOutputStream(output);
						System.out.println("Recibiendo fichero");
						
						while(true) {
							byte b;
							if((b = dis.readByte())!=-1) {
								System.out.print(". ");
								fos.write(b);
							}else {
								break;
							}
						}
						
						fos.close();
					}catch(Exception e){
						
					}
				}
			}
		});
		
		Scanner sc = new Scanner(System.in);
		
		while(true) {
			System.out.println("1. Activa servidor 2. Cierra servidor 3. Envia resumen");
			
			switch (sc.nextInt()) {
			case 1:
				tServer.start();
				break;
			case 2:
				tServer.interrupt();
			case 3:
				//Env�a fichero
				Socket sSender; 
				try {
					sSender = new Socket("192.168.1.69",77);
					OutputStream serverOut = sSender.getOutputStream();
					FileInputStream fis;
					byte b;
					
					File miResumen = new File("C:/Users/user/Desktop/resumen_cifrado.txt");
					fis = new FileInputStream(miResumen);
					//Resumen
					while((b = (byte)fis.read())!=-1) {
						serverOut.write(b);
					}
					
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					fis.close();
					//Fichero no cifrado
					File miFichero= new File("C:/Users/user/Desktop/ficheroJD.txt");
					fis = new FileInputStream(miFichero);
					//Resumen
					while((b = (byte)fis.read())!=-1) {
						serverOut.write(b);
					}
					fis.close();
					serverOut.close();
				} catch (UnknownHostException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				break;
			default:
				break;
			}
		}
	}
}
