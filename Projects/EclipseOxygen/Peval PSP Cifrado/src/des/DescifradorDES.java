package des;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;

/**
 * Esta clase se encarga de descifrar aquellos ficheros cifrados mediante DES
 * @author juand
 *
 */
public class DescifradorDES {

	private static final String fichero_clave_DES = "D:/SECUR/clave.txt";
	private static final String fichero_DES_codificado = "D:/SECUR/fichero_codificado.txt";
	private static final String fichero_DES_descodificado = "D:/SECUR/fichero_codificado_descodificado.txt";

	public static void main(String[] args) {
		try {
			System.out.println("Leyendo clave");
			File cinf = new File(fichero_clave_DES);
			FileInputStream cis = new FileInputStream(cinf);
			byte[] clave = new byte[(int) cinf.length()];
			cis.read(clave);

			System.out.println("Generando key a partir de espectro");
			DESKeySpec keyspec = new DESKeySpec(clave);
			SecretKeyFactory keyfac = SecretKeyFactory.getInstance("DES");
			SecretKey key = keyfac.generateSecret(keyspec);
			
			System.out.println("Inicializando descifrador");
			Cipher desCipher = Cipher.getInstance("DES");
			desCipher.init(Cipher.DECRYPT_MODE, key);
			
			File inf = new File(fichero_DES_codificado);
			FileInputStream is = new FileInputStream(inf);
			FileOutputStream os = new FileOutputStream(fichero_DES_descodificado);
			byte[] buffer = new byte[16];
			int bytes_leidos = is.read(buffer);
			
			System.out.println("Descodificando fichero");
			while (bytes_leidos != -1) {
				os.write(desCipher.doFinal(buffer, 0, bytes_leidos));
				bytes_leidos = is.read(buffer);
			}
			
			System.out.println("Cerrando flujos");
			os.close();
			cis.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}