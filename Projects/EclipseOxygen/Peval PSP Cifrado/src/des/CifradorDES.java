package des;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;

/**
 * Esta clase se encarga de cifrar un fichero utilizando el algoritmo DES. Adem�s, almacena la clave en el fichero clave.txt
 * @author juand
 *
 */
public class CifradorDES {
	
	private static final String fichero_original = "D:/SECUR/fichero_original.txt";
	private static final String fichero_DES_codificado = "D:/SECUR/fichero_codificado.txt";
	private static final String fichero_clave_DES = "D:/SECUR/clave.txt";
	
	public static void main(String[] args) {
		try {
			System.out.println("Obteniendo generador de claves con cifrado DES");
			KeyGenerator keygen = KeyGenerator.getInstance("DES");
			System.out.println("Generando clave");
			SecretKey key = keygen.generateKey();
			
			System.out.println("Obteniendo objeto Cipher con cifrado DES");
			Cipher desCipher = Cipher.getInstance("DES");
			
			System.out.println("Configurando Cipher para encriptar");
			desCipher.init(Cipher.ENCRYPT_MODE, key);
			
			System.out.println("Abriendo el fichero");
			File inf = new File(fichero_original);
			FileInputStream is = new FileInputStream(inf);
			
			System.out.println("Abriendo el fichero cifrado");
			FileOutputStream os = new FileOutputStream(fichero_DES_codificado);
			
			System.out.println("Cifrando el fichero...");
			byte[] buffer = new byte[8];
			int bytes_leidos = is.read(buffer);
			while (bytes_leidos != -1) {
				os.write(desCipher.doFinal(buffer, 0, bytes_leidos));
				bytes_leidos = is.read(buffer);
			}
			os.close();
			
			System.out.println("Obteniendo factor�a de claves con cifrado DES");
			SecretKeyFactory keyfac = SecretKeyFactory.getInstance("DES");
			
			System.out.println("Generando keyspec");
			DESKeySpec keyspec = (DESKeySpec) keyfac.getKeySpec(key, DESKeySpec.class);
			
			System.out.println("Salvando la clave en un fichero");
			FileOutputStream cos = new FileOutputStream(fichero_clave_DES);
			cos.write(keyspec.getKey());
			cos.close();
			is.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}