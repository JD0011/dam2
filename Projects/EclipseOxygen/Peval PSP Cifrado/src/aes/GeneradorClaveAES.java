package aes;

import java.io.File;
import java.io.FileOutputStream;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;

public class GeneradorClaveAES {

	private static final String clave_AES = "./clave_AES.txt";

	public static void main(String[] args) {

		try {
			// PASO 1: Crear claves AES
			System.out.println("Obteniendo generador de claves con cifrado AES");
			KeyGenerator keygen = KeyGenerator.getInstance("AES");
			System.out.println("Generando clave");
			SecretKey key = keygen.generateKey();
			
			FileOutputStream fos = new FileOutputStream(new File(clave_AES));
			fos.write(key.getEncoded());
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
