package aes;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

public class DecifradorAES {
	
	private static final String AES_key = "./clave_AES.txt";
	private static final String fichero_AES_cofidicado = "./fichero_codificado_AES.txt";
	private static final String fichero_AES_descofidicado = "./fichero_descodificado_AES.txt";
	
	public static void main(String[] args) {
		try {
			
			//Lectura de la clave desde fichero
			File keyFile = new File(AES_key);
			FileInputStream fis = new FileInputStream(keyFile);
			
			byte[] key = new byte[(int)keyFile.length()];
			fis.read(key);
			fis.close();
			
			//Obtenci�n de la clave
			SecretKeySpec keySpec = new SecretKeySpec(key, "AES");			
			Cipher desCipher = Cipher.getInstance("AES");
			// PASO 3a: Poner cifrador en modo CIFRADO
			System.out.println("Configurando Cipher para desencriptar");
			desCipher.init(Cipher.DECRYPT_MODE, keySpec);
			System.out.println("Descifrando mensaje");
			
			File ficheroCodificado = new File(fichero_AES_cofidicado);
			fis = new FileInputStream(ficheroCodificado);

			FileOutputStream fos = new FileOutputStream(fichero_AES_descofidicado);
			byte[] buffer = new byte[16];
			int bytes_leidos= fis.read(buffer);
			while (bytes_leidos != -1) {
				fos.write(desCipher.doFinal(buffer,  0 , bytes_leidos));
				bytes_leidos = fis.read(buffer);
			}

			fis.close();
			fos.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
