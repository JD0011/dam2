package rsa;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.math.BigInteger;
import java.security.Key;
import java.security.KeyFactory;
import java.security.spec.RSAPublicKeySpec;

import javax.crypto.Cipher;

/**
 * Se encarga de cifrar un fichero determinado con la clave p�blica suministrada
 * @author juand
 *
 */
public class CifradorRSAClavePublica {
	
	private static final String public_RSA_key = "D:/SECUR/clave_PUB_moyano.txt";
	private static final String fichero_clave_DES = "D:/SECUR/clave.txt";
	private static final String fichero_clave_DES_codificada_con_RSA = "D:/SECUR/fichero_clave_codificado.txt";
		
	public static void main(String[] args) {
		try {			
			//Lectura de clave privada
			File clavePublica = new File(public_RSA_key);
			BufferedReader br = new BufferedReader(new FileReader(clavePublica));
			BigInteger modulus = new BigInteger(br.readLine());
			BigInteger exponente = new BigInteger(br.readLine());
			
			System.out.println("Espectro de clave leido");
			RSAPublicKeySpec keyspec = new RSAPublicKeySpec(modulus, exponente);
			KeyFactory keyfac = KeyFactory.getInstance("RSA");
			Key public_key = keyfac.generatePublic(keyspec);
			
			System.out.println("Key generada desde el espectro");
			Cipher cipher = Cipher.getInstance("RSA");
			cipher.init(Cipher.ENCRYPT_MODE, public_key);
			
			System.out.println("Cipher inicializado correctamente");
			File ficheroNoCifrado = new File(fichero_clave_DES);
			FileInputStream is = new FileInputStream(ficheroNoCifrado);
			FileOutputStream os = new FileOutputStream(fichero_clave_DES_codificada_con_RSA);
			
			System.out.println("Escribiendo fichero cifrado con clave RSA p�blica");
			//Cambiar buffer a 16 en el cifrador
			byte[] buffer = new byte[16];
			int bytes_leidos = is.read(buffer);
			while (bytes_leidos != -1) {
				os.write(cipher.doFinal(buffer, 0, bytes_leidos));
				bytes_leidos = is.read(buffer);
			}
			
			System.out.println("Fichero cifrado. Cerrando flujos");
			os.close();
			br.close();
			is.close();
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		
	}
}
