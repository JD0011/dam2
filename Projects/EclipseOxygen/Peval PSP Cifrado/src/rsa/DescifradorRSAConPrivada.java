package rsa;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.math.BigInteger;
import java.security.Key;
import java.security.KeyFactory;
import java.security.spec.RSAPrivateKeySpec;

import javax.crypto.Cipher;

/**
 * Descifra el fichero esprecificado con la clave privad pasada por par�metros
 * @author juand
 *
 */
public class DescifradorRSAConPrivada {
	
	private static final String private_RSA_key = "D:/SECUR/clave_PRI_delgado.txt";
	private static final String fichero_clave_codificado = "D:/SECUR/calificacionDELGADO.txt";
	private static final String fichero_clave = "D:/SECUR/calificacionDELGADO_desc.txt";

	public static void main(String[] args) {
		try {
	    	//Lectura de clave p�blica
			BufferedReader br = new BufferedReader(new FileReader(private_RSA_key));
			BigInteger modulus = new BigInteger(br.readLine());
			BigInteger exponente = new BigInteger(br.readLine());
			
			RSAPrivateKeySpec keyspec = new RSAPrivateKeySpec(modulus, exponente);
			KeyFactory keyfac = KeyFactory.getInstance("RSA");
			Key private_key = keyfac.generatePrivate(keyspec);
			
			Cipher desCipher = Cipher.getInstance("RSA");
			desCipher.init(Cipher.DECRYPT_MODE, private_key);
			
			File inf = new File(fichero_clave_codificado);
			FileInputStream is = new FileInputStream(inf);
			FileOutputStream os = new FileOutputStream(fichero_clave);
			byte[] buffer = new byte[64];
			int bytes_leidos = is.read(buffer);
			while (bytes_leidos != -1) {
				os.write(desCipher.doFinal(buffer, 0, bytes_leidos));
				bytes_leidos = is.read(buffer);
			}
			os.close();
			is.close();
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
