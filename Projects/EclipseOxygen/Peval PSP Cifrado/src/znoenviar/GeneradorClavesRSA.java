package znoenviar;


import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPrivateKeySpec;
import java.security.spec.RSAPublicKeySpec;

public class GeneradorClavesRSA {
	
	private static final String public_RSA_key = "clave_PUB_delgado.txt";
	private static final String private_RSA_key = "clave_PRI_delgado.txt";

	public static void main(String[] args) throws NoSuchAlgorithmException, InvalidKeySpecException, FileNotFoundException {
		//Crear esta variable en el generador de claves
		
		SecureRandom random = new SecureRandom();
		
		KeyPairGenerator keygen = KeyPairGenerator.getInstance("RSA");
		//Iniciar el keygen con la variable creada y un tama�o de 512
		
	     keygen.initialize(512, random);
	     
	     
		System.out.println("Generando par de claves");
		KeyPair keypair = keygen.generateKeyPair();
		System.out.println("Obteniendo factor�a de claves con cifrado RSA");
		KeyFactory keyfac = KeyFactory.getInstance("RSA");
		System.out.println("Generando keyspec");

		System.out.println("Salvando la clave p�blica en un fichero");
		
		RSAPublicKeySpec publicKeySpec = keyfac.getKeySpec(keypair.getPublic(),
	                                                         RSAPublicKeySpec.class);
		
		FileOutputStream cos = new FileOutputStream(public_RSA_key);
		PrintWriter publicWriter = new PrintWriter(cos);
		publicWriter.println(publicKeySpec.getModulus());
		publicWriter.println(publicKeySpec.getPublicExponent());
		publicWriter.close();

		System.out.println("Salvando la clave privada en un fichero");
		
		RSAPrivateKeySpec privateKeySpec = keyfac.getKeySpec(keypair.getPrivate(),
				RSAPrivateKeySpec.class);
		
		FileOutputStream priv = new FileOutputStream(private_RSA_key);
		PrintWriter privateWriter = new PrintWriter(priv);
		privateWriter.println(privateKeySpec.getModulus());
		privateWriter.println(privateKeySpec.getPrivateExponent());
		privateWriter.close();
	}
}
