package znoenviar;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.math.BigInteger;
import java.security.Key;
import java.security.KeyFactory;
import java.security.spec.RSAPrivateKeySpec;

import javax.crypto.Cipher;

public class CifradorRSAClavePrivada {
	
	private static final String private_RSA_key = "D:/SECUR/clave_PRI_delgado.txt";
	private static final String fichero_RSA_codificado = "D:/SECUR/fichero_codificado.txt";
	private static final String fichero_original = "";
	
	public static void main(String[] args) {
		try {			
			
			//Lectura de clave privada
			System.out.println("Leyendo clave privada");
			BufferedReader br = new BufferedReader(new FileReader(private_RSA_key));
			BigInteger modulus = new BigInteger(br.readLine());
			BigInteger exponente = new BigInteger(br.readLine());
			
			System.out.println("Generando clave a partir de espectro");
			RSAPrivateKeySpec keyspec = new RSAPrivateKeySpec(modulus, exponente);
			KeyFactory keyfac = KeyFactory.getInstance("RSA");
			Key private_key = keyfac.generatePrivate(keyspec);
			
			System.out.println("Inicializando cifrador");
			Cipher cipher = Cipher.getInstance("RSA");
			cipher.init(Cipher.ENCRYPT_MODE, private_key);
			
			File ficheroNoCifrado = new File(fichero_original);
			FileInputStream is = new FileInputStream(ficheroNoCifrado);
			FileOutputStream os = new FileOutputStream(fichero_RSA_codificado);
			
			//Cambiar buffer a 16 en el cifrador
			byte[] buffer = new byte[16];
			int bytes_leidos = is.read(buffer);
			
			System.out.println("Cifrando fichero mediante clave RSA privada");
			while (bytes_leidos != -1) {
				os.write(cipher.doFinal(buffer, 0, bytes_leidos));
				bytes_leidos = is.read(buffer);
			}
			
			System.out.println("Cerrando flujos...");
			os.close();
			br.close();
			is.close();
			
		}catch (Exception e) {
			
		}
		
	}
}
