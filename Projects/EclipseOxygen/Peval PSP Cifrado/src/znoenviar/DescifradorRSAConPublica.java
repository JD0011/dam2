package znoenviar;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.math.BigInteger;
import java.security.Key;
import java.security.KeyFactory;
import java.security.spec.RSAPublicKeySpec;

import javax.crypto.Cipher;

public class DescifradorRSAConPublica {
	
	private static final String public_RSA_key = "";
	private static final String fichero_RSA_codificado = "";
	private static final String fichero_RSA_descodificado = "";
	
	public static void main(String[] args) {
		try {
	    	//Lectura de clave p�blica
			BufferedReader br = new BufferedReader(new FileReader(public_RSA_key));
			BigInteger modulus = new BigInteger(br.readLine());
			BigInteger exponente = new BigInteger(br.readLine());
			
			RSAPublicKeySpec keyspec = new RSAPublicKeySpec(modulus, exponente);
			KeyFactory keyfac = KeyFactory.getInstance("RSA");
			Key public_key = keyfac.generatePublic(keyspec);
			
			Cipher desCipher = Cipher.getInstance("RSA");
			desCipher.init(Cipher.DECRYPT_MODE, public_key);
			
			File inf = new File(fichero_RSA_codificado);
			FileInputStream is = new FileInputStream(inf);
			FileOutputStream os = new FileOutputStream(fichero_RSA_descodificado);
			byte[] buffer = new byte[64];
			int bytes_leidos = is.read(buffer);
			while (bytes_leidos != -1) {
				os.write(desCipher.doFinal(buffer, 0, bytes_leidos));
				bytes_leidos = is.read(buffer);
			}
			
			os.close();
			is.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
