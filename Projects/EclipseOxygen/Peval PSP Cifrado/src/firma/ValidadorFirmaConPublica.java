package firma;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.math.BigInteger;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.Signature;
import java.security.spec.DSAPublicKeySpec;

/**
 * Valida el fichero especificado a partir de la clave p�blica suministrada
 * @author juand
 *
 */
public class ValidadorFirmaConPublica {
	
	private static final String fichero_sin_firmar = "D:/SECUR/nota_obtenida.txt";
	private static final String fichero_firmado = "D:/SECUR/nota_firmada.txt";
	private static final String public_DSA_key = "D:/SECUR/clave_DSA_PUB_delgado.txt";
	
	public static void main(String[] args) {
		try {
			System.out.println("Leyendo espectro de clave DSA p�blica");
			BufferedReader br = new BufferedReader(new FileReader(public_DSA_key));
			BigInteger Y = new BigInteger(br.readLine());
			BigInteger P = new BigInteger(br.readLine());
			BigInteger Q = new BigInteger(br.readLine());
			BigInteger G = new BigInteger(br.readLine());
			DSAPublicKeySpec keyspec = new DSAPublicKeySpec(Y, P, Q, G);
			
			System.out.println("Generando clave a partir de espectro");
			KeyFactory keyfac = KeyFactory.getInstance("DSA");
			PublicKey public_key = keyfac.generatePublic(keyspec);
			Signature signature = Signature.getInstance("DSA");
			signature.initVerify(public_key);
			File inf = new File(fichero_sin_firmar);
			FileInputStream is = new FileInputStream(inf);
			byte[] buffer = new byte[64];
			int bytes_leidos = is.read(buffer);
			
			System.out.println("Comprobando fichero con clave DSA p�blica");
			while (bytes_leidos != -1) {
				signature.update(buffer, 0, bytes_leidos);
				bytes_leidos = is.read(buffer);
			}
			File insf = new File(fichero_firmado);
			FileInputStream isf = new FileInputStream(insf);
			byte[] firma = new byte[(int) insf.length()];
			isf.read(firma);
			
			System.out.println("Verificando firma del fichero");
			if (signature.verify(firma)) {
				System.out.println("OK: Fichero no alterado");
			}else {
				System.out.println("BAD: El fichero ha sido modificado");
			}
			
			br.close();
			isf.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}