package firma;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.math.BigInteger;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.spec.DSAPrivateKeySpec;

/**
 * Se encarga de firmar un fichero mediante el algoritmo DSA.
 * @author juand
 *
 */
public class FirmadorConPrivada {
	
	private static final String fichero_a_firmar = "D:/SECUR/nota_obtenida.txt";
	private static final String fichero_firmado = "D:/SECUR/nota_firmada.txt";
	private static final String private_DSA_key = "D:/SECUR/clave_DSA_PRI_delgado.txt";
	
	public static void main(String[] args) {
		try {
			System.out.println("Leyendo espectro de clave DSA privada");
			BufferedReader br = new BufferedReader(new FileReader(private_DSA_key));
			BigInteger X = new BigInteger(br.readLine());
			BigInteger P = new BigInteger(br.readLine());
			BigInteger Q = new BigInteger(br.readLine());
			BigInteger G = new BigInteger(br.readLine());
			DSAPrivateKeySpec keyspec = new DSAPrivateKeySpec(X, P, Q, G);
			
			System.out.println("Construyendo clave privada");
			
			System.out.println("Generando clave a partir de espectro");
			KeyFactory keyfac = KeyFactory.getInstance("DSA");
			PrivateKey private_key = keyfac.generatePrivate(keyspec);			
			Signature signature = Signature.getInstance("DSA");
			
			System.out.println("Firmando mensaje");
			signature.initSign(private_key);
			
			System.out.println("Abriendo el fichero");
			File inf = new File(fichero_a_firmar);
			FileInputStream is = new FileInputStream(inf);
			
			System.out.println("Firmando el fichero...");
			byte[] buffer = new byte[64];
			int bytes_leidos = is.read(buffer);
			while (bytes_leidos != -1) {
				signature.update(buffer, 0, bytes_leidos);
				bytes_leidos = is.read(buffer);
			}
			byte[] firma = signature.sign();
			
			System.out.println("Abriendo el fichero de firma");
			FileOutputStream os = new FileOutputStream(fichero_firmado);
			
			os.write(firma);
			os.close();		
			br.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}