package firma;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.spec.DSAPrivateKeySpec;
import java.security.spec.DSAPublicKeySpec;

/**
 * Genera las claves asim�tricas del algoritmo DSA
 * @author juand
 *
 */
public class GeneradorFirmaDSA {
	
	private static final String public_DSA_key = "D:/SECUR/clave_DSA_PUB_delgado.txt";
	private static final String private_DSA_key = "D:/SECUR/clave_DSA_PRI_delgado.txt";
	
	public static void main(String[] args) {
		try {
			KeyPairGenerator keygen = KeyPairGenerator.getInstance("DSA");
			
			System.out.println("Generando par de claves");
			KeyPair keypair = keygen.generateKeyPair();
			System.out.println("Obteniendo factor�a de claves con cifrado DSA");
			KeyFactory keyfac = KeyFactory.getInstance("DSA");
			
			System.out.println("Generando keyspec p�blico");
			DSAPublicKeySpec publicKeySpec = keyfac.getKeySpec(keypair.getPublic(), DSAPublicKeySpec.class);
			
			System.out.println("Salvando la clave p�blica en un fichero");
			FileOutputStream cosPublic = new FileOutputStream(public_DSA_key);
			PrintWriter cpwPublic = new PrintWriter(cosPublic);
			cpwPublic.println(publicKeySpec.getY());
			cpwPublic.println(publicKeySpec.getP());
			cpwPublic.println(publicKeySpec.getQ());
			cpwPublic.println(publicKeySpec.getG());
			cpwPublic.close();
			
			System.out.println("Generando keyspec privado");
			DSAPrivateKeySpec privateKeySpec = keyfac.getKeySpec(keypair.getPrivate(), DSAPrivateKeySpec.class);
			
			System.out.println("Salvando la clave privada en un fichero");
			FileOutputStream cosPrivate = new FileOutputStream(private_DSA_key);
			PrintWriter cpwPrivate = new PrintWriter(cosPrivate);
			cpwPrivate.println(privateKeySpec.getX());
			cpwPrivate.println(privateKeySpec.getP());
			cpwPrivate.println(privateKeySpec.getQ());
			cpwPrivate.println(privateKeySpec.getG());
			cpwPrivate.close();
		}catch(Exception e) {
			e.printStackTrace();
		}
		
	}
}
