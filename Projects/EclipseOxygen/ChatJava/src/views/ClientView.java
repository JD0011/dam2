package views;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.Socket;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;

import handler.ClientIO;

/**
 * Representa al cliente que se conecta al chat. A parte de la interf�z, posee la l�gica
 * necesaria para trabajar con el servidor.
 * @author juand
 *
 */
public class ClientView extends JFrame {

	private static final long serialVersionUID = 1L;
	

	//Area de mensajes
	private static JTextField clientMessage;
	private static JTextArea serverMessage;
	//Botones
	private JButton sendButton = new JButton("Enviar");
	private JButton btnDisconnect = new JButton("Salir");
	
	private String nick;
	//Socket de conexi�n
	private Socket cs;
	//Manejador de Input / Output streams (l�gica de cliente)
	private ClientIO clientIO;
	//Representa el tiempo restante
	private JLabel timeRemaining;

	public ClientView() {
		Initialize();
	}
	
	/**
	 * Inicializa la interf�z de la pesta�a del cliente y algunas variables
	 */
	private void Initialize() {
		setLayout(null);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		
		clientMessage = new JTextField();
		clientMessage.setBounds(10, 40, 400, 30);
		add(clientMessage);
		
		serverMessage = new JTextArea();
		serverMessage.setBounds(10, 80, 200, 30);
		serverMessage.setEditable(false);
		serverMessage.setLineWrap(true);
		serverMessage.setWrapStyleWord(true);
		
		JScrollPane serverScrollPane = new JScrollPane(serverMessage);
		//No mostrar scroll horizontal
		serverScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		serverScrollPane.setBounds(10, 70, 400, 300);
		add(serverScrollPane);
		
		sendButton.setBounds(420, 10, 100, 30);
		add(sendButton);
		
		btnDisconnect.setBounds(420, 50, 100, 30);
		add(btnDisconnect);
		
		timeRemaining = new JLabel();
		timeRemaining.setBounds(420,100,100,50);
		add(timeRemaining);
		
		
		//LISTENERS
		
		sendButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String text;
				//Si hay mensaje, env�alo
				if(!(text=clientMessage.getText().trim()).equals("")) {
					//Limite de mensaje a 150 caracteres
					if(text.length()<150) {
						//Borra el mensaje de la casilla
						clientMessage.setText("");
						//Env�alo al servidor
						clientIO.sendMessage(text);
						//A�adelo a la ventana de chat
						serverMessage.append(nick+"-> "+text+"\n");
					}else {
						clientMessage.setText("");
						serverMessage.append("!Mensaje demasiado largo� M�ximo: 150 caracteres");
					}
				}				
			}
		});
		
		btnDisconnect.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				//Env�a se�al de desconexi�n al servidor
				clientIO.sendDisconnectSignal();	
				clientIO.kill();
				System.exit(0);
				
			}
		});
	}
	
	/**
	 * Procura un nick v�lido para el cliente. Establece la longitud del nick en un rango de
	 * 4 a 16 caracteres. Si no se cumple esa regla, se vuelve a preguntar el nick o se finaliza
	 * el programa
	 */
	public void askNick() {
		boolean exitCondition = false;
		int respuesta;
		
		respuesta = 0;
		nick = "";
		while(!exitCondition) {
			//Pregunta por los datos necesarios
			nick = JOptionPane.showInputDialog("Nick (4-10 caracteres)");
			
			//Si cancela
			if(nick==null) {
				System.exit(0);
			}
			nick = nick.trim();
			//Si el nick est� vac�o notifica error
			if(nick.equals("")) {
				respuesta = JOptionPane.showConfirmDialog(
						null,
						"Debe introducir un nick con letras",
						"Error en el nick",
						JOptionPane.OK_CANCEL_OPTION);
			}else {
				if(nick.length()<4 || nick.length()>11) {
					respuesta = JOptionPane.showConfirmDialog(
							null,
							"El nick debe estar formado por entre 4 y 10 caracteres",
							"Error en el nick",
							JOptionPane.OK_CANCEL_OPTION);
				}else {
					//Todo correcto
					exitCondition = true;
				}
			}
			
			//Si cancela en advertencia
			if(respuesta==2) {
				System.exit(0);
			}
			
		}
		
		//Establece el nombre a la ventana
		setTitle("Bienvenido, "+nick);
	}
	
	/**
	 * Inicializar� el socket necesario para realizar la conexi�n y el handler
	 * para manipular la I/O con respecto al server
	 */
	public void configureServerHandler() {
		//Inicializa los sockets
		try {
			cs = new Socket("localhost", 44444);
		} catch (IOException e) {
			JOptionPane.showConfirmDialog(
					this,
					"La direcci�n ip o puerto no existe, o el servidor no est� inicializado",
					"Error en el servidor", 
					JOptionPane.OK_CANCEL_OPTION);
			System.exit(0);
		}
		
		//Si el socket se ha iniciado bien
		if(cs!=null) {
			clientIO = new ClientIO(cs, this);
			clientIO.openStreams();
			clientIO.start();
			
			//Petici�n de conexi�n
			clientIO.requestConnection();
			
		}
	}
	
	/**
	 * A�ade el texto especificado por par�metros y hace un salto de carro
	 * @param text
	 */
	public void showServerMessage(String text) {
		serverMessage.append(text+"\n");
	}
	
	/**
	 * Muestra en el Label del tiempo restante, el tiempo pasado por par�metros
	 * @param seconds
	 */
	public void showRemainingTime(int seconds) {
		timeRemaining.setText("<html>Tiempo<br> restante<br> "+seconds+" segs</html>");
	}
	
	/**
	 * Esconde la interfaz del cliente
	 */
	public void hideUI() {
		setVisible(false);
		dispose();
	}
	
	public String getNick() {
		return nick;
	}

	public static void main(String[] args) {
		
		ClientView cview = new ClientView();
		
		cview.askNick();
		cview.configureServerHandler();
		
		
	}
}
