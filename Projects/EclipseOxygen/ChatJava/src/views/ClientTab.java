package views;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.Socket;
import java.net.SocketException;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;

import handler.ServerIO;

/**
 * Tab generado mediante WindowBuilder el cual crear� una ventana para la comunicaci�n 
 * con un cliente determinado.
 * @author juand
 *
 */
public class ClientTab extends JPanel {
	
	private static final long serialVersionUID = 1L;
	//Manejador de Input / Output streams (l�gica de servidor)
	private ServerIO serverIO;
	//Interfaz del servidor
	private ServerView sview;
	//Areas para el chat
	private JTextArea clientMessage;
	private JTextArea serverMessage;
	
	//Enviar & Abandonar
	private JButton sendButton;
	private JButton exitButton;

	//Tiempo restante
	private JLabel timeRemaining;
	//Temporizador
	private Timer timeCounter;
	//Segundos restantes
	private int seconds;
	//Ayuda a parar la TimerTask en ejecuci�n
	private boolean pararCuenta;

	public ClientTab(ServerView sview, ServerIO serverIO) {
		this.sview = sview;
		this.serverIO = serverIO;
		//serverIO usar� cTab como pantalla para mostrar los mensajes
		serverIO.assignClient(this);
	}
	
	/**
	 * Inicializa la interf�z de la pesta�a del cliente y algunas variables
	 */
	public void Initialize() {
		
		//AUTOGENERADO
		JScrollPane clientScroll = new JScrollPane();
		clientScroll.setBounds(10, 11, 376, 201);
		clientScroll.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		
		clientMessage = new JTextArea();
		clientMessage.setEditable(false);
		clientMessage.setLineWrap(true);
		clientMessage.setWrapStyleWord(true);
		clientScroll.setViewportView(clientMessage);
		
		JScrollPane serverScroll = new JScrollPane();
		serverScroll.setBounds(10, 223, 279, 46);
		serverScroll.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		setLayout(null);
		add(clientScroll);
		add(serverScroll);
		
		serverMessage = new JTextArea();
		serverMessage.setLineWrap(true);
		serverMessage.setWrapStyleWord(true);
		serverScroll.setViewportView(serverMessage);
		
		//Implementaci�n propia
		sendButton = new JButton("Enviar");
		sendButton.setBounds(299, 223, 89, 46);
		add(sendButton);
		
		exitButton = new JButton("<html><p align=\"center\">Abandonar</p></html>");
		exitButton.setBounds(392,11,100,25);
		add(exitButton);
		
		timeRemaining = new JLabel();
		timeRemaining.setBounds(400,60,100,50);
		add(timeRemaining);
		
		//LISTENERS
		
		sendButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String text;
				//Control de mensaje
				if(!(text=serverMessage.getText().trim()).equals("")) {
					//Limite de mensaje a 150 caracteres
					if(text.length()<150) {
						//Borra el mensaje de la casilla
						serverMessage.setText("");
						//Env�alo al servidor
						serverIO.sendMessage(text);
						//A�adelo a la ventana de chat
						clientMessage.append("Psic�logo -> "+text+"\n");
					}else {
						serverMessage.setText("");
						clientMessage.append("!Mensaje demasiado largo� M�ximo: 150 caracteres");
					}
				}
			}
		});
		
		exitButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				timeRemaining.setText("<html>Conexi�n<br> finalizada!</html>");
				showDisconnectMessage("La conexi�n con"+serverIO.getNick()+" ha finalizado!");
				serverIO.kick("El gabinete ha cerrado esta conversaci�n");
				pararCuenta = true;
			}
		});
		
		//Abre los flujos
		serverIO.openStreams();
		//Arranca el handler
		serverIO.start();
		
		//Empieza la cuenta atr�s
		startTimer();
	}
	
	/**
	 * Realiza una cuenta regresiva cuya duraci�n se especifica en  {@link ServerView#MAX_TIME}.<br><br>
	 * Cuando se alcanza <b>0 segundos restantes</b>, se realizan las modificaciones oportunas a la 
	 * pesta�a del cliente y se hace un {@link ServerIO#kick(String)}, que <b>expulsa al cliente
	 * exponi�ndole el mensaje pasado por par�metros</b>
	 */
	public void startTimer() {
		pararCuenta = false;
		seconds = ServerView.MAX_TIME;
		TimerTask task = new TimerTask() {
			
			@Override
			public void run() {
				//De segundo en segundo
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				seconds--;
				
				//Si se agot� el tiempo
				if(seconds==0) {
					//Notifica al psic�logo
					timeRemaining.setText("<html>Tiempo<br> m�ximo<br> superado!</html>");
					showDisconnectMessage("La conexi�n con"+serverIO.getNick()+" ha finalizado!");
					serverIO.kick("El l�mite de tiempo por sesi�n ha sido superado");
					pararCuenta = true;
				}
				//Env�a tiempo restante
				serverIO.sendRemainingTime(seconds);
				//Actualiza el Label
				timeRemaining.setText("<html>Tiempo<br> restante<br> "+seconds+" segs</html>");
				
				//Mecanismo para parar el timer
				if(pararCuenta) {
					timeRemaining.setText("<html>Conexi�n<br> finalizada!</html>");
					cancel();
				}
			}
		};
		
		timeCounter = new Timer();
		timeCounter.scheduleAtFixedRate(task, 1000, ServerView.MAX_TIME);
	}

	/**
	 * A�ade el texto especificado por par�metros y hace un salto de carro
	 * @param text
	 */
	public void showClientMessage(String text) {
		clientMessage.append(text+"\n");
	}
	
	/**
	 * Notifica que el cliente ha abandonado la sesi�n de chat. Tambi�n cambia el bot�n
	 * de enviar por el de salir
	 * @param nick
	 */
	public void showDisconnectMessage(String message) {
		clientMessage.append(message+"\n");
		//Eliminamos el handler de la lista de clientes
		sview.removeClientIO(serverIO);
		
		//Modificaci�n de bot�n
		sendButton.setText("Cerrar");
		sendButton.setBackground(Color.GRAY);
		sendButton.setForeground(Color.BLACK);
		
		//Parar contador
		pararCuenta = true;
		sview.colorizeTabClient(this, Color.GRAY);
		
		//Ahora el bot�n elimina la pesta�a en vez de enviar mensajes
		sendButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				sview.removeTabClient(ClientTab.this);
			}
		});
		
	}
	
	public ServerIO getServerIO() {
		return serverIO;
	}

	public void showWelcome(String nick) {
		clientMessage.append("El cliente "+nick+" ha iniciado una sesi�n de chat\n");
	}
}
