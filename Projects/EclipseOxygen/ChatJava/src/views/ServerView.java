package views;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTabbedPane;
import javax.swing.UIManager;

import handler.ServerIO;

public class ServerView extends JFrame{
	
	private static final long serialVersionUID = 1L;
	
	private int port = 44444;
	//Cliente
	private Socket cs;
	//Server
	private ServerSocket ss;
	//Tab manager
	private JTabbedPane clientsPane;
	
	//Clientes conectados
	private static ArrayList<ServerIO> clients;
	
	//Constantes
	public static final int MAX_TIME=60; //Por sesi�n
	public final int MAX_CONNECTIONS=5;
	
	private JButton exitButton;
	private JLabel connectedClients;
	private JLabel portLabel;

	/**
	 * Crea un nuevo servidor y su vista correspondiente
	 */
	public ServerView() {
		super(" VENTANA DEL SERVIDOR DE CHAT ");
		clients = new ArrayList<ServerIO>();
		Initialize();
	}
	
	/**
	 * Inicializa la interf�z del servidor
	 */
	private void Initialize() {
		
		setLayout(null);
		setResizable(false);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);

		exitButton = new JButton("Salir");
		exitButton.setBounds(420, 10, 100, 30);
		add(exitButton);
		
		//Tabs para clientes
		clientsPane = new JTabbedPane();
		clientsPane.setBounds(10, 50, 500, 300);
		add(clientsPane);
		
		connectedClients = new JLabel("Clientes conectados: 0 de "+MAX_CONNECTIONS);
		connectedClients.setBounds(10, 10, 180, 20);
		add(connectedClients);

		portLabel = new JLabel("Puerto: "+port);
		portLabel.setBounds(220, 10, 180, 20);
		add(portLabel);
		
		//LISTENER'S
		exitButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				//Si hay clientes, exp�lsalos
				if(clients.size()>0) {
					kickAllClients();
				}
				//Salir
				System.exit(0);
			}
		});
	}	
	
	/**
	 * Inicializa el socket del servidor
	 */
	public boolean initSocket() {
		boolean isCorrect;
		
		try {
			ss = new ServerSocket(port);
			isCorrect = true;
		} catch (IOException e) {
			JOptionPane.showConfirmDialog(
					null,
					"El puerto "+port+" est� en uso",
					"Error en el servidor",
					JOptionPane.OK_CANCEL_OPTION);
			
			isCorrect = false;
		}
		
		return isCorrect;
	}
	
	/**
	 * Empieza a escuchar conexiones entrantes al socket del servidor.
	 * Si una conexi�n es establecida, se crea la pesta�a correspondiente para el cliente
	 * y se inicializa.
	 */
	public void listenClients() {
		ServerIO ioHandler;
		while(true) {
			//Espera a la conexi�n de alg�n cliente
			try {
				cs=ss.accept();
				//Handler para los flujos de entrada y salida
				ioHandler = new ServerIO(cs, this);
				//Pesta�a del cliente
				ClientTab cTab = new ClientTab(this, ioHandler);
				cTab.Initialize();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Responde al socket si puede o no unirse en funci�n del n�mero
	 * m�ximo de conexiones permitidas
	 * @return
	 */
	public boolean canJoin() {
		boolean join;
		if(clientsPane.getTabCount()<MAX_CONNECTIONS) {
			join = true;
		}else {
			join = false;
		}
		
		return join;
	}
	
	/**
	 * Comprueba si el nombre pasado por par�metros ya pertenece a alg�n
	 * usuario conectado
	 * @param name
	 * @return
	 */
	public boolean nameBeingUsed(String name) {
		boolean exists;
		
		exists = false;
		for(ServerIO client : clients) {
			if(client.getNick().equalsIgnoreCase(name)) {
				exists=true;
				break;
			}
		}
		return exists;
	}
	
	/**
	 * Actualiza el JLabel que indica la cantidad de clientes conectados
	 */
	public void refreshConnectedClients() {
		connectedClients.setText("Clientes conectados: "+clients.size()+" de "+MAX_CONNECTIONS);
	}
	
	/**
	 * A�ade al cliente y muestra la nueva pesta�a
	 * @param clientTab
	 */
	public void addTabClient(ClientTab clientTab) {
		clientsPane.add(clientTab);
		clientsPane.setSelectedIndex(clientsPane.indexOfComponent(clientTab));
	}
	
	/**
	 * Establece el color del clientTab pasado por par�metros consultando los componentes
	 * de clientsPane
	 * @param clientTab
	 * @param c
	 */
	public void colorizeTabClient(ClientTab clientTab, Color c) {
		clientsPane.setBackgroundAt(clientsPane.indexOfComponent(clientTab), c);
	}

	/**
	 * Elimina del TabbedPane la pesta�a especificada por par�metros
	 * @param clientTabAuto
	 */
	public void removeTabClient(ClientTab clientTab) {
		clientsPane.remove(clientTab);
	}
	
	/**
	 * A�ade el handler especificado a la lista de clientes
	 * @param serverIO
	 */
	public void addClientIO(ServerIO serverIO) {
		clients.add(serverIO);	
		//Actualiza JLabel
		refreshConnectedClients();
	}
	
	/**
	 * Elimina el handler especificado de la lista de clientes
	 * @param serverIO
	 */
	public void removeClientIO(ServerIO serverIO) {
		clients.remove(serverIO);
		//Actualiza JLabel
		refreshConnectedClients();
	}

	/**
	 * Expulsa a todos los clientes conectados al servidor, mostrando el
	 * motivo a partir de un mensaje
	 */
	private void kickAllClients() {
		System.out.println("INFO: Expulsando los clientes conectados...");
		for(ServerIO client : clients) {
			client.kick("El servidor ha cerrado la conexi�n");
		}
	}
	
	/**
	 * Obtiene el total de clientes conectados a partir de {@link ArrayList#size()}
	 * @return clientes conectados
	 */
	public int getClientsCount(){
		return clients.size();
	}
	
	public static void main(String[] args) {
		ServerView sView = new ServerView();
		
		//Si se inicia bien, continua
		if(sView.initSocket()) {
			sView.setBounds(0, 0, 540, 400);
			sView.setVisible(true);
			sView.listenClients();
		}
		
	}
	
}
