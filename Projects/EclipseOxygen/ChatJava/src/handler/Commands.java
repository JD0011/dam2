package handler;

/**
 * Contiene los comandos ejecutables y/o interpretables por el servidor y el 
 * cliente.
 * @author juand
 *
 */
public interface Commands {

	// Petición de conexión del cliente al servidor
	static final String  CONNECTION_REQUEST = "connrequest.";
	// Notificación de desconexión para cliente / servidor
	static final String  CONNECTION_FINISHED = "connfinished.";
	// Conexión rechazada por parte del servidor
	static final String  CONNECTION_REJECTED = "connrejected.";
	// Conexión permitida para el cliente
	static final String  CONNECTION_GRANTED = "conngranted.";
	// Tiempo de la sesión del cliente
	static final String  SESSION_TIME = "sessiontime.";
	// Mensaje que pueden enviar el cliente / servidor
	static final String  MESSAGE = "message.";
}
