package handler;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

import javax.swing.JOptionPane;

import views.ClientView;

/**
 * <b>Controla el flujo de entrada y salida del cliente</b> comunic�ndose con un servidor a partir de 
 * los comandos almacenados en {@link Commands}<br>
 * Para cumplir la funci�n de handler de entrada y salida se ha:<br>
 * 	� Heredado de Thread, para que el hilo compruebe el InputStream constantemente<br>
 *  � Declarado m�todos que manejan el OutputStream para el env�o de los comandos al servidor<br>
 *  
 *  La interpretaci�n de los comandos es la propia <b><i>l�gica del cliente</i></b>
 * @author juand
 *
 */
public class ClientIO extends Thread implements Commands{
	
	private Socket cs;
	private DataOutputStream dos;
	private DataInputStream dis;
	private ClientView cview;
	
	private boolean keepAlive;
		
	public ClientIO(Socket cs, ClientView cview) {
		this.cview = cview;
		this.cs = cs;
		
		keepAlive = true;
	}
	
	/**
	 * Env�a al servidor una petici�n de conexi�n. Si hay espacio disponible,
	 * mantendr� la conexi�n abierta, y si no, la cerrar�
	 */
	public void requestConnection() {
		System.out.println("INFO: Petici�n de conexi�n enviada al servidor : "+cs.getInetAddress());
		try {
			dos.writeUTF(CONNECTION_REQUEST+cview.getNick());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Envia un mensaje al servidor notificando la desconexi�n
	 */
	public void sendDisconnectSignal() {
		System.out.println("INFO: El cliente ha finalizado la conexi�n (notificando al servidor...)");
		try {
			dos.writeUTF(CONNECTION_FINISHED);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void sendMessage(String text) {
		System.out.println("INFO: Enviando mensaje al servidor");
		try {
			dos.writeUTF(MESSAGE+text);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Procesa el comando recibido desde el servidor
	 * @param msg
	 */
	private void processCommand(String msg) {
		//Comando recibido
		String cmd;
		//Contenido del mensaje
		String data;
		
		cmd = msg.substring(0, msg.indexOf('.')+1);
		data = msg.substring(msg.indexOf('.')+1,msg.length());
		
		switch (cmd) {
			//Conexi�n concebida
			case CONNECTION_GRANTED:
				System.out.println("INFO: Conexi�n aceptada por el servidor");
				//Abre la interfaz en caso de conexi�n aceptada
				cview.setBounds(0, 0, 540, 400);
				cview.setVisible(true);
				break;
			
				//Conexi�n rechazada
			case CONNECTION_REJECTED:
				//Notifica cliente
				System.out.println("INFO: Conexi�n con el servidor rechazada: "+data);
				keepAlive = false;
				
				//Cerrar interfaz
				cview.hideUI();
				
				JOptionPane.showConfirmDialog(
						null, 
						data, 
						"Conexi�n rechazada", 
						JOptionPane.OK_CANCEL_OPTION);
				break;
				
			//Conexi�n finalizada
			case CONNECTION_FINISHED:
				System.out.println("INFO: Conexi�n con el servidor finalizada: "+data);
				keepAlive = false;
				
				cview.setVisible(false);
				cview.dispose();
				
				JOptionPane.showConfirmDialog(
						null, 
						data, 
						cview.getNick()+" - Conexi�n finalizada", 
						JOptionPane.OK_CANCEL_OPTION);
				break;
			
			//Actualizaci�n de tiempo de sesi�n
			case SESSION_TIME:
				cview.showRemainingTime(Integer.parseInt(data));
				break;
			//Mensaje de cliente
			case MESSAGE:
				System.out.println("INFO: Mensaje del servidor recibido");
				//Extrae mensaje
				cview.showServerMessage("Psic�logo ->  "+data);
				break;
	
			default:
				break;
		}
	}
	
	public void openStreams() {
		try {
			//Inicializa streams
			dis = new DataInputStream(cs.getInputStream());
			dos = new DataOutputStream(cs.getOutputStream());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void run() {
		try {
			String msg;
			while(keepAlive) {
				//Si hay alg�n mensaje, procesalo
				msg = dis.readUTF();
				if(msg!=null) {
					processCommand(msg);
				}
			}
		} catch (IOException e) {
			JOptionPane.showConfirmDialog(
					null, 
					"Se ha perdido la conexi�n con el servidor", 
					cview.getNick()+" - Conexi�n finalizada", 
					JOptionPane.OK_CANCEL_OPTION);
		}
		//Cierra los flujos y socket
		close();
	}
	
	/**
	 * Mata el bucle del hilo
	 */
	public void kill() {
		keepAlive = false;
	}

	/**
	 * Cierra los flujos y el socket del cliente
	 */
	private void close() {
		
		try {
			dis.close();
			dos.close();
			
			cs.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
