package handler;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.net.Socket;
import java.net.SocketException;

import views.ClientTab;
import views.ServerView;

/**
 * <b>Controla el flujo de entrada y salida del servidor</b> comunic�ndose con un cliente a partir de 
 * los comandos almacenados en {@link Commands}<br>
 * Para cumplir la funci�n de handler de entrada y salida se ha:<br>
 * 	� Heredado de Thread, para que el hilo compruebe el InputStream constantemente<br>
 *  � Declarado m�todos que manejan el OutputStream para el env�o de los comandos al cliente<br>
 *  
 *  La interpretaci�n de los comandos es la propia <b><i>l�gica del server</i></b>
 * @author juand
 *
 */
public class ServerIO extends Thread implements Commands{
	
	private String nick;
	private Socket cs;
	private ClientTab cTab;
	private DataInputStream dis;
	private DataOutputStream dos;
	private ServerView sview;
	
	private boolean keepAlive;
	
	public ServerIO(Socket cs, ServerView sview) {
		this.cs = cs;
		this.sview = sview;
		keepAlive = true;
		
		System.out.println("INFO: Nuevo handler inicializado");
	}
	
	public void sendMessage(String text) {
		try {
			dos.writeUTF(MESSAGE+text);
		} catch (IOException e) {
			cTab.showDisconnectMessage("Se ha perdido la conexi�n con "+nick+" de forma inesperada!\n");
		}
	}
	
	/**
	 * Env�a el tiempo de sesi�n de conexi�n al cliente
	 */
	public void sendRemainingTime(int seconds) {
		try {
			dos.writeUTF(SESSION_TIME+seconds);
		} catch (IOException e) {
			cTab.showDisconnectMessage("Se ha perdido la conexi�n con "+nick+" de forma inesperada!\n");
		}
	}
	
	/**
	 * Procesa el comando recibido desde el cliente
	 * @param msg
	 */
	private void processCommand(String msg) {
		//Comando recibido
		String cmd;
		//Contenido del mensaje
		String data;
		
		cmd = msg.substring(0, msg.indexOf('.')+1);
		data = msg.substring(msg.indexOf('.')+1,msg.length());
		
		switch (cmd) {

			case CONNECTION_REQUEST: 
				//Guardar nick
				nick = data;
				System.out.println("INFO: Petici�n de conexi�n recibida por parte de: "+data);
				if(sview.getClientsCount()<sview.MAX_CONNECTIONS) {
					if(!sview.nameBeingUsed(data)) {
						//Notifica al cliente que puede unirse
						try {
							dos.writeUTF(CONNECTION_GRANTED);
						} catch (IOException e) {
							cTab.showDisconnectMessage("Se ha perdido la conexi�n con "+nick+" de forma inesperada!\n");
						}
						System.out.println("INFO: Conexi�n aceptada: capacidad -> "+(sview.getClientsCount()+1)+"/"+sview.MAX_CONNECTIONS+" clientes");
						
						//Adici�n del handler a la lista de clientes
						sview.addClientIO(this);
						
						//Adici�n del cliente
						cTab.setName(data);
						sview.addTabClient(cTab);
						System.out.println("INFO: Panel desplegado para el cliente "+data);
						cTab.showWelcome(data);
						
						//Env�a biendenida al cliente
						sendMessage("�Bienvenido al gabinete de psic�logos!�En qu� podemos ayudarle?");
					}else {
						System.out.println("INFO: Conexi�n rechazada para "+data+" : nombre repetido");
						//Si no, notifica al cliente y cierra
						kick("El nick elegido no est� disponible");
					}
					
					
				}else {
					System.out.println("INFO: Conexi�n rechazada para "+data+" : servidor lleno");
					//Si no, notifica al cliente y cierra
					kick("El servidor est� lleno");
				}
				break;


			case CONNECTION_FINISHED:
				System.out.println("INFO: El cliente "+cTab.getName()+" se ha desconectado");
				//Mata hilo
				keepAlive = false;
				System.out.println("INFO: Flujos y socket cerrados, finalizando hilo");
				cTab.showDisconnectMessage("El cliente "+nick+" ha finalizado la conexi�n!");
				//Handler eliminado
				sview.removeClientIO(this);
				
				break;


			case MESSAGE:
				System.out.println("INFO: Mensaje recibido de "+cTab.getName());
				cTab.showClientMessage(nick+" ->  "+ data);
				break;
	
			default:
				break;
		}
	}
	
	public void openStreams() {
		try {
			//Inicializa streams
			dis = new DataInputStream(cs.getInputStream());
			dos = new DataOutputStream(cs.getOutputStream());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * M�todo que expulsa el cliente del servidor, enviando una se�al de
	 * finalizaci�n de conexi�n
	 * @param kickMessage Motivo del kick
	 */
	public void kick(String kickMessage) {
		try {
			dos.writeUTF(CONNECTION_FINISHED+kickMessage);
		} catch (IOException e) {
			cTab.showDisconnectMessage("Se ha perdido la conexi�n con "+nick+" de forma inesperada!\n");
		}
		
		//Se mata el handler de
		keepAlive = false;
	}
	
	/**
	 * Mata el bucle del hilo
	 */
	public void kill() {
		System.out.println("INFO: Handler detenido para: "+nick);
		keepAlive = false;
	}
	
	/**
	 * Cierra los flujos y el socket del cliente
	 */
	public void close() {
		try {
			dis.close();
			dos.close();
			
			cs.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public String getNick() {
		return nick;
	}

	@Override
	public void run(){
		try {
			String msg;
			msg = null;
			while(keepAlive) {
				//Si hay alg�n mensaje, procesalo
				try {
					msg = dis.readUTF();
				}catch (EOFException e) {
					cTab.showDisconnectMessage("Se ha perdido la conexi�n con "+nick+" de forma inesperada!\n");
				}
				if(msg!=null) {
					processCommand(msg);
				}
			}
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Asigna la pesta�a en la que mostrar� los mensajes
	 * @param cTab
	 */
	public void assignClient(ClientTab cTab) {
		this.cTab = cTab;
	}
}
