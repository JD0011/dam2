package test;

import hibernate.HibernateUtil;
import hibernate.dao.Users;
import hibernate.dao.UsersHome;

public class Test1 {

	public static void main(String[] args) {
		UsersHome uh = new UsersHome();
		
		uh.attachDirty(new Users("pepe","pepe","pe@pe.pe","654321234","cool","worker", null, null, null));
		HibernateUtil.getInstance().getSessionFactory().getCurrentSession().getTransaction().commit();
		HibernateUtil.getInstance().getSessionFactory().getCurrentSession().close();
	}
}