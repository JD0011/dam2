package hibernate.dao;
// Generated 30-abr-2018 17:58:39 by Hibernate Tools 5.2.10.Final

import java.util.List;
import javax.naming.InitialContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.SessionFactory;
import static org.hibernate.criterion.Example.create;

/**
 * Home object for domain model class UsersHasTasks.
 * @see hibernate.dao.UsersHasTasks
 * @author Hibernate Tools
 */
public class UsersHasTasksHome {

	private static final Log log = LogFactory.getLog(UsersHasTasksHome.class);

	private final SessionFactory sessionFactory = getSessionFactory();

	protected SessionFactory getSessionFactory() {
		try {
			return (SessionFactory) new InitialContext().lookup("SessionFactory");
		} catch (Exception e) {
			log.error("Could not locate SessionFactory in JNDI", e);
			throw new IllegalStateException("Could not locate SessionFactory in JNDI");
		}
	}

	public void persist(UsersHasTasks transientInstance) {
		log.debug("persisting UsersHasTasks instance");
		try {
			sessionFactory.getCurrentSession().persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void attachDirty(UsersHasTasks instance) {
		log.debug("attaching dirty UsersHasTasks instance");
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(UsersHasTasks instance) {
		log.debug("attaching clean UsersHasTasks instance");
		try {
			sessionFactory.getCurrentSession().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void delete(UsersHasTasks persistentInstance) {
		log.debug("deleting UsersHasTasks instance");
		try {
			sessionFactory.getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public UsersHasTasks merge(UsersHasTasks detachedInstance) {
		log.debug("merging UsersHasTasks instance");
		try {
			UsersHasTasks result = (UsersHasTasks) sessionFactory.getCurrentSession().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public UsersHasTasks findById(hibernate.dao.UsersHasTasksId id) {
		log.debug("getting UsersHasTasks instance with id: " + id);
		try {
			UsersHasTasks instance = (UsersHasTasks) sessionFactory.getCurrentSession()
					.get("hibernate.dao.UsersHasTasks", id);
			if (instance == null) {
				log.debug("get successful, no instance found");
			} else {
				log.debug("get successful, instance found");
			}
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List<UsersHasTasks> findByExample(UsersHasTasks instance) {
		log.debug("finding UsersHasTasks instance by example");
		try {
			List<UsersHasTasks> results = (List<UsersHasTasks>) sessionFactory.getCurrentSession()
					.createCriteria("hibernate.dao.UsersHasTasks").add(create(instance)).list();
			log.debug("find by example successful, result size: " + results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}
}
