package src;
// Generated 27-abr-2018 20:48:15 by Hibernate Tools 5.2.8.Final

/**
 * SessionsId generated by hbm2java
 */
public class SessionsId implements java.io.Serializable {

	private int idSession;
	private int idProjectOwner;

	public SessionsId() {
	}

	public SessionsId(int idSession, int idProjectOwner) {
		this.idSession = idSession;
		this.idProjectOwner = idProjectOwner;
	}

	public int getIdSession() {
		return this.idSession;
	}

	public void setIdSession(int idSession) {
		this.idSession = idSession;
	}

	public int getIdProjectOwner() {
		return this.idProjectOwner;
	}

	public void setIdProjectOwner(int idProjectOwner) {
		this.idProjectOwner = idProjectOwner;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof SessionsId))
			return false;
		SessionsId castOther = (SessionsId) other;

		return (this.getIdSession() == castOther.getIdSession())
				&& (this.getIdProjectOwner() == castOther.getIdProjectOwner());
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result + this.getIdSession();
		result = 37 * result + this.getIdProjectOwner();
		return result;
	}

}
