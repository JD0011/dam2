package src;
// Generated 27-abr-2018 20:48:15 by Hibernate Tools 5.2.8.Final

/**
 * ProjectKeysId generated by hbm2java
 */
public class ProjectKeysId implements java.io.Serializable {

	private String accessKey;
	private int projectsIdProject;

	public ProjectKeysId() {
	}

	public ProjectKeysId(String accessKey, int projectsIdProject) {
		this.accessKey = accessKey;
		this.projectsIdProject = projectsIdProject;
	}

	public String getAccessKey() {
		return this.accessKey;
	}

	public void setAccessKey(String accessKey) {
		this.accessKey = accessKey;
	}

	public int getProjectsIdProject() {
		return this.projectsIdProject;
	}

	public void setProjectsIdProject(int projectsIdProject) {
		this.projectsIdProject = projectsIdProject;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof ProjectKeysId))
			return false;
		ProjectKeysId castOther = (ProjectKeysId) other;

		return ((this.getAccessKey() == castOther.getAccessKey()) || (this.getAccessKey() != null
				&& castOther.getAccessKey() != null && this.getAccessKey().equals(castOther.getAccessKey())))
				&& (this.getProjectsIdProject() == castOther.getProjectsIdProject());
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result + (getAccessKey() == null ? 0 : this.getAccessKey().hashCode());
		result = 37 * result + this.getProjectsIdProject();
		return result;
	}

}
