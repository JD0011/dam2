package src;
// Generated 27-abr-2018 20:48:20 by Hibernate Tools 5.2.8.Final

import java.util.List;
import javax.naming.InitialContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.SessionFactory;
import static org.hibernate.criterion.Example.create;

/**
 * Home object for domain model class ProjectKeys.
 * @see src.ProjectKeys
 * @author Hibernate Tools
 */
public class ProjectKeysHome {

	private static final Log log = LogFactory.getLog(ProjectKeysHome.class);

	private final SessionFactory sessionFactory = getSessionFactory();

	protected SessionFactory getSessionFactory() {
		try {
			return (SessionFactory) new InitialContext().lookup("SessionFactory");
		} catch (Exception e) {
			log.error("Could not locate SessionFactory in JNDI", e);
			throw new IllegalStateException("Could not locate SessionFactory in JNDI");
		}
	}

	public void persist(ProjectKeys transientInstance) {
		log.debug("persisting ProjectKeys instance");
		try {
			sessionFactory.getCurrentSession().persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void attachDirty(ProjectKeys instance) {
		log.debug("attaching dirty ProjectKeys instance");
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(ProjectKeys instance) {
		log.debug("attaching clean ProjectKeys instance");
		try {
			sessionFactory.getCurrentSession().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void delete(ProjectKeys persistentInstance) {
		log.debug("deleting ProjectKeys instance");
		try {
			sessionFactory.getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public ProjectKeys merge(ProjectKeys detachedInstance) {
		log.debug("merging ProjectKeys instance");
		try {
			ProjectKeys result = (ProjectKeys) sessionFactory.getCurrentSession().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public ProjectKeys findById(src.ProjectKeysId id) {
		log.debug("getting ProjectKeys instance with id: " + id);
		try {
			ProjectKeys instance = (ProjectKeys) sessionFactory.getCurrentSession().get("src.ProjectKeys", id);
			if (instance == null) {
				log.debug("get successful, no instance found");
			} else {
				log.debug("get successful, instance found");
			}
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List<ProjectKeys> findByExample(ProjectKeys instance) {
		log.debug("finding ProjectKeys instance by example");
		try {
			List<ProjectKeys> results = (List<ProjectKeys>) sessionFactory.getCurrentSession()
					.createCriteria("src.ProjectKeys").add(create(instance)).list();
			log.debug("find by example successful, result size: " + results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}
}
