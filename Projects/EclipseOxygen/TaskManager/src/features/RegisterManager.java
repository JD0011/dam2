package features;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;

import dao.MySQL.DatabaseConnection;
import datamodel.User;

/**
 * Clase encargada de registrar un usuario nuevo en la base de datos
 * @author juand
 *
 */
public class RegisterManager {
	
	public RegisterManager() {
		
	}
	
	/**
	 * Comprueba si el nombre de usuario est� en uso o no
	 * @param username Nombre a comprobar
	 * @return True si est� disponible, False de lo contrario
	 */
	public boolean usernameAvailable(String username) {
		boolean available = false;
		
		Connection ddbb = DatabaseConnection.getConnection();
		
		CallableStatement csUsernameAvailable;
		try {
			csUsernameAvailable = ddbb.prepareCall("{ CALL username_available(?,?)}");
			
			csUsernameAvailable.setString(1, username);
			csUsernameAvailable.registerOutParameter(2, Types.BOOLEAN);
			
			csUsernameAvailable.execute();
			available = csUsernameAvailable.getBoolean(2);
			
			csUsernameAvailable.close();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return available;
	}
	
	/**
	 * Registra el nombre de usuario almacenado en la variable de clase con la contrase�a pasada por par�metros.
	 * @param u Usuario a registrarse. Debe tener definidos nombre, contrase�a, correo y n�mero de tel�fono.
	 * @return True si se ha registrado correctamente, False de lo contrario.
	 */
	public boolean registerUser(User u) {
		boolean isRegistered = false;;
		Connection ddbb = DatabaseConnection.getConnection();
		
		try {
			CallableStatement csRegisterUser = ddbb.prepareCall("{ CALL register_user(?,?,?,?,?)}");
			
			csRegisterUser.setString(1, u.getUsername());
			csRegisterUser.setString(2, u.getPassword());
			csRegisterUser.setString(3, u.getMail());
			csRegisterUser.setInt(4, u.getPhonenumber());
			csRegisterUser.registerOutParameter(5, Types.BOOLEAN);
			
			csRegisterUser.execute();
			isRegistered  = csRegisterUser.getBoolean(5);
			
			csRegisterUser.close();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return isRegistered;
	}
	
	public static void main(String[] args) {
		RegisterManager rm = new RegisterManager();
		
		if(rm.usernameAvailable("pepaso")) {
			System.out.println(rm.registerUser(new User("pepe", "123456", "pep@ep.ep", 123456789)));
		}
	}
}
