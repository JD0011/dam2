package features;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;

import dao.MySQL.DatabaseConnection;
import datamodel.User;

/**
 * Clase que se encarga de la parte del login del usuario
 * @author juand
 *
 */
public class Login {
	
	/**
	 * Almacena el �ltimo usuario que intent� logearse
	 */
	private static User lastUser;
	
	/**
	 * Contadore que cuenta las veces que un usuario intenta conectarse
	 */
	private static int counter;
	
	/**
	 * N�mero m�ximos de intentos de logeo por parte del usuario
	 */
	private static final int MAX_ATTEMPTS = 5;
	
	/**
	 * Statement que permite realizar las consultas
	 */
	private CallableStatement csCanLogin;
	
	public Login() {
		InitializeCallableStatement();
	}
	
	private void InitializeCallableStatement() {
		Connection ddbb = DatabaseConnection.getConnection();
		
		try {
			csCanLogin = ddbb.prepareCall("{ CALL user_password_ok(?,?,?)}");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Llama al procedimiento almacenado encargado de comprobar si la contrase�a corresponde
	 * al usuario que intenta logearse
	 * @param u Usuario a logearse
	 * @return True si puede logearse, False de lo contrario.
	 */
	public boolean userCanLogin(User u) {
		
		boolean canLogin = false;
		
		try {
			
			csCanLogin.setString(1, u.getUsername());
			csCanLogin.setString(2, u.getPassword());
			csCanLogin.registerOutParameter(3, Types.BOOLEAN);
			
			csCanLogin.execute();
			canLogin = csCanLogin.getBoolean(3);
			
			if(!canLogin) {
				if(u.equals(lastUser)) {
					counter++;
				}else {
					counter = 0;
				}
			}else {
				counter = 0;
			}
			
			lastUser = u;
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return canLogin;
	}
	
	/**
	 * Comprueba si se ha alcanzado el n�mero m�ximo de intentos de logeo
	 * @return True si se ha alcanzado el l�mite de intentos, False de lo contrario.
	 */
	public boolean isMaxAttempsLoginReached() {
		return counter == MAX_ATTEMPTS;
	}
	
	/**
	 * Reinicia el contador de intentos de logeo
	 */
	public void resetAttemptsCounter() {
		counter = 0;
	}
	
	/**
	 * Cierra el Statement
	 */
	public void closeCallableStatement() {
		try {
			if(!csCanLogin.isClosed()) {
				csCanLogin.close();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
