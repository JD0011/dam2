package dao;

import datamodel.Group;
import datamodel.User;

/**
 * Interfaz que implementa todos los m�todos necesarios para manipular los registros de la tabla de equipos
 * desde una base de datos determinada por la clase donde se implemente.
 * @author Juan de Dios Delgado Berm�dez
 * @since 14/03/2018
 *
 */
public interface TeamDAO {
	
	public void addTeam(Group u);
	public void deleteTeamByID(int teamID);
	public User getTeamByID(int teamID);
	public boolean teamNameExists(String teamName);
}
