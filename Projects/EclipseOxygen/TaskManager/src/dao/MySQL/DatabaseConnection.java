package dao.MySQL;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Clase que devuelve una �nica instancia de la conexi�n de la base de datos MySQL
 * @author juand
 *
 */
public class DatabaseConnection {

	private static String hostname="localhost";
	private static String database = "/todo";
	private static int port=3306;
	private static String username="root";
	private static String password="";
	
	private static Connection conn;
	
	public static Connection getConnection() {
		
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		if(conn == null) {
			try {
				conn =  DriverManager.getConnection("jdbc:mysql://"+ hostname + database, username, password);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		
		return conn;
	}
}
