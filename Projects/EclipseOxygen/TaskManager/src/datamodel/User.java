package datamodel;

public class User {
	
	private String username;
	private String password;
	private String mail;
	private int phonenumber;
	private String profileDescription;
	
	public User(String username, String password) {
		this.username = username;
		this.password = password;
	}
	
	public User(String username, String password, String mail, int phonenumber) {
		this.username = username;
		this.password = password;
		this.mail = mail;
		this.phonenumber = phonenumber;
	}
	
	public User(String username, String password, String mail, int phonenumber, String profileDescription) {
		this.username = username;
		this.password = password;
		this.mail = mail;
		this.phonenumber = phonenumber;
		this.profileDescription = profileDescription;
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	public String getMail() {
		return mail;
	}

	public int getPhonenumber() {
		return phonenumber;
	}

	public String getProfileDescription() {
		return profileDescription;
	}

}
