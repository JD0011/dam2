package datamodel;

import java.time.LocalDateTime;

public class Task {

	/**
	 * Nombre de la tarea
	 */
	private String name;
	
	/**
	 * Contenido: se describe el procedimiento a realizar
	 */
	private String content;

	/**
	 * Fecha de inicio de la tarea
	 */
	private LocalDateTime startDate;
	
	/**
	 * Fecha final de la tarea
	 */
	private LocalDateTime endDate;

	public Task(String name, String content, LocalDateTime startDate, LocalDateTime endDate) {
		super();
		this.name = name;
		this.content = content;
		this.startDate = startDate;
		this.endDate = endDate;
	}

	public String getName() {
		return name;
	}

	public String getContent() {
		return content;
	}

	public LocalDateTime getStartDate() {
		return startDate;
	}

	public LocalDateTime getEndDate() {
		return endDate;
	}	
}
