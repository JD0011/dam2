package datamodel;

/**
 * Representa a una unidad de trabajo dentro de la aplicaci�n. Esta unidad est� formada por un administrador y sus miembros
 * @author juand
 *
 */
public class Group {

	/**
	 * Nombre del grupo
	 */
	private String name;
	/**
	 * Texto descriptivo del grupo
	 */
	private String description;
	/**
	 * Cantidad de miembros, sin incluir el administrador
	 */
	private int members;
	
	public Group(String name, String description, int members) {
		super();
		this.name = name;
		this.description = description;
		this.members = members;
	}
	
	public String getName() {
		return name;
	}
	
	public String getDescription() {
		return description;
	}
	
	public int getMembers() {
		return members;
	}
	
	
}
