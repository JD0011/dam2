package rsa;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.math.BigInteger;
import java.security.Key;
import java.security.KeyFactory;
import java.security.spec.RSAPrivateKeySpec;

import javax.crypto.Cipher;

public class DescifradorRSAConPrivada {

	private final static String PrivateKeyPath = "C:/Users/user/Documents/DAM2/Projects/EclipseOxygen/clave_PRIV_delgado.txt";

	public static void main(String[] args) {
		try {
	    	//Lectura de clave p�blica
			BufferedReader br = new BufferedReader(new FileReader(PrivateKeyPath));
			BigInteger modulus = new BigInteger(br.readLine());
			BigInteger exponente = new BigInteger(br.readLine());
			
			RSAPrivateKeySpec keyspec = new RSAPrivateKeySpec(modulus, exponente);
			KeyFactory keyfac = KeyFactory.getInstance("RSA");
			Key private_key = keyfac.generatePrivate(keyspec);
			
			Cipher desCipher = Cipher.getInstance("RSA");
			desCipher.init(Cipher.DECRYPT_MODE, private_key);
			
			File inf = new File("C:/Users/user/Desktop/fichero_enrique_cifrado.txt");
			FileInputStream is = new FileInputStream(inf);
			FileOutputStream os = new FileOutputStream("C:/Users/user/Desktop/fichero_enrique_descifrado.txt");
			byte[] buffer = new byte[64];
			int bytes_leidos = is.read(buffer);
			while (bytes_leidos != -1) {
				os.write(desCipher.doFinal(buffer, 0, bytes_leidos));
				bytes_leidos = is.read(buffer);
			}
			os.close();
			is.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
