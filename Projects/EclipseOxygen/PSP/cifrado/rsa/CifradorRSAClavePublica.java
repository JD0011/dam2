package rsa;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.math.BigInteger;
import java.security.Key;
import java.security.KeyFactory;
import java.security.spec.RSAPublicKeySpec;

import javax.crypto.Cipher;

public class CifradorRSAClavePublica {
	
	private final static String PublicKeyPath = "C:/Users/user/Documents/DAM2/Projects/EclipseOxygen/clave_PRIV_delgado.txt";
	
	public static void main(String[] args) {
		try {			
			//Lectura de clave privada
			BufferedReader br = new BufferedReader(new FileReader(PublicKeyPath));
			BigInteger modulus = new BigInteger(br.readLine());
			BigInteger exponente = new BigInteger(br.readLine());
			
			RSAPublicKeySpec keyspec = new RSAPublicKeySpec(modulus, exponente);
			KeyFactory keyfac = KeyFactory.getInstance("RSA");
			Key public_key = keyfac.generatePublic(keyspec);
			
			Cipher cipher = Cipher.getInstance("RSA");
			cipher.init(Cipher.ENCRYPT_MODE, public_key);
			
			File ficheroNoCifrado = new File("C:/Users/user/Desktop/resumenFichero.txt");
			FileInputStream is = new FileInputStream(ficheroNoCifrado);
			FileOutputStream os = new FileOutputStream("C:/Users/user/Desktop/resumen_cifrado.txt");
			
			//Cambiar buffer a 16 en el cifrador
			byte[] buffer = new byte[16];
			int bytes_leidos = is.read(buffer);
			while (bytes_leidos != -1) {
				os.write(cipher.doFinal(buffer, 0, bytes_leidos));
				bytes_leidos = is.read(buffer);
			}
			
			os.close();
			br.close();
			is.close();
			
		}catch (Exception e) {
			
		}
		
	}
}
