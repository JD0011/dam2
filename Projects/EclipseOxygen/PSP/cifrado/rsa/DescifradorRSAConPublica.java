package rsa;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.math.BigInteger;
import java.security.Key;
import java.security.KeyFactory;
import java.security.spec.RSAPublicKeySpec;

import javax.crypto.Cipher;

public class DescifradorConPublica {
	public static void main(String[] args) {
		try {
	    	//Lectura de clave p�blica
			BufferedReader br = new BufferedReader(new FileReader("C:/Users/user/Desktop/clave_publica_enrique.txt"));
			BigInteger modulus = new BigInteger(br.readLine());
			BigInteger exponente = new BigInteger(br.readLine());
			
			RSAPublicKeySpec keyspec = new RSAPublicKeySpec(modulus, exponente);
			KeyFactory keyfac = KeyFactory.getInstance("RSA");
			Key public_key = keyfac.generatePublic(keyspec);
			
			Cipher desCipher = Cipher.getInstance("RSA");
			desCipher.init(Cipher.DECRYPT_MODE, public_key);
			
			File inf = new File("C:/Users/user/Desktop/fichero_enrique_cifrado.txt");
			FileInputStream is = new FileInputStream(inf);
			FileOutputStream os = new FileOutputStream("C:/Users/user/Desktop/fichero_enrique_descifrado.txt");
			byte[] buffer = new byte[64];
			int bytes_leidos = is.read(buffer);
			while (bytes_leidos != -1) {
				os.write(desCipher.doFinal(buffer, 0, bytes_leidos));
				bytes_leidos = is.read(buffer);
			}
			os.close();
			is.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
