package rsa;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.math.BigInteger;
import java.security.Key;
import java.security.KeyFactory;
import java.security.SecureRandom;
import java.security.spec.RSAPrivateKeySpec;

import javax.crypto.Cipher;

public class CifradorClavePrivada {
	public static void main(String[] args) {
		try {			
			//Lectura de clave privada
			BufferedReader br = new BufferedReader(new FileReader("C:/Users/user/Documents/DAM2/Projects/EclipseOxygen/clave_PRIV_delgado.txt"));
			BigInteger modulus = new BigInteger(br.readLine());
			BigInteger exponente = new BigInteger(br.readLine());
			
			RSAPrivateKeySpec keyspec = new RSAPrivateKeySpec(modulus, exponente);
			KeyFactory keyfac = KeyFactory.getInstance("RSA");
			Key private_key = keyfac.generatePrivate(keyspec);
			
			Cipher cipher = Cipher.getInstance("RSA");
			cipher.init(Cipher.ENCRYPT_MODE, private_key);
			
			File ficheroNoCifrado = new File("C:/Users/user/Desktop/resumenFichero.txt");
			FileInputStream is = new FileInputStream(ficheroNoCifrado);
			FileOutputStream os = new FileOutputStream("C:/Users/user/Desktop/resumen_cifrado.txt");
			
			//Cambiar buffer a 16 en el cifrador
			byte[] buffer = new byte[16];
			int bytes_leidos = is.read(buffer);
			while (bytes_leidos != -1) {
				os.write(cipher.doFinal(buffer, 0, bytes_leidos));
				bytes_leidos = is.read(buffer);
			}
			
			os.close();
			br.close();
			is.close();
			
		}catch (Exception e) {
			
		}
		
	}
}
