package threading;

public class ThreadsTest1 {
	
	public static void main(String[] args) {
		
		Thread t1 = new Thread() {
			
			public void run() {
				int seconds = 0;
				int time = 0;
				while(time<10) {
					time++;
					System.out.println(seconds++ + " Segundo/s del hilo 1");
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		};
		
		Thread t2 = new Thread() {
			
			public void run() {
				int seconds = 0;
				int time2 = 0;
				while(time2<9) {
					time2++;
					System.out.println(seconds++ + " Segundo/s del hilo 2");
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		};
		
		t1.start();
		t2.start();
	}

}
