package threadsLessons1;

/**
 * Implementation Thread class test for starting to work with threads.
 * </br>
 * <b>Reminder</b>: its better to implement Runnable to allow to extend from other class
 * 
 * @author juand
 *
 */
public class Hilo extends Thread{
	
	Hilo(int n) {
		// TODO Auto-generated constructor stub
		this.setName("Hilo "+n);
		System.out.println("Creando el primer hilo: "+ this.getName());
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		
		for(int i=0;i<50;i++) {
			System.out.println("Estoy en " +this.getName());
		}
		
		System.out.println("Salgo del hilo "+this.getName());
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
