package ejercicio4;

public class EncrypterPipe {
	
	private String text;
	
	public EncrypterPipe() {

	}
	
	
	public void encript() {
		String aux = text;
		text = "";
		for(char c: aux.toCharArray()) {
			text+=(char)(c+3);
		}

	}
	
	public void setText(String text) {
		this.text=text;
	}
	
	public String getEncryptedText() {
		return text;
	}

}
