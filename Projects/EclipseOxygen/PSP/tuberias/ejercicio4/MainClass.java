package ejercicio4;

public class MainClass {
	
	public static void main(String[] args) {
		
		EncrypterPipe p = new EncrypterPipe();
		
		StringSender sender = new StringSender(p);
		StringReceiver receiver = new StringReceiver(p);
		
		sender.start();
		receiver.start();
	}

}
