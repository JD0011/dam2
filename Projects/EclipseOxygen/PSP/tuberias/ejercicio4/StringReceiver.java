package ejercicio4;

import java.util.Scanner;

public class StringReceiver extends Thread{
	
	private String text;
	private EncrypterPipe p;
	
	public StringReceiver(EncrypterPipe p) {
		this.p = p;
	}
	
	@Override
	public void run() {
		boolean exitCondition = false;
		while(!exitCondition) {
			try {
				sleep(20);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			text = p.getEncryptedText();
			
			if(text !=null) {
				System.out.println("(Receiver) Texto encriptado: "+text);
				exitCondition = true;
			}
		}
	}

}
