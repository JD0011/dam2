package ejercicio4;

import java.util.Scanner;

public class StringSender extends Thread{
	
	private String text, type;
	private EncrypterPipe p;
	
	public StringSender(EncrypterPipe p) {
		this.p = p;
	}
	
	@Override
	public void run() {
		//Ask for some text
		Scanner sc = new Scanner(System.in);
		System.out.println("(Sender) Introduce una cadena de texto: ");
		text = sc.nextLine();
		
		//Send to the pipe and encrypt it
		p.setText(text);
		p.encript();
			
	}

}
