package tubs.ejemploClase;

public class Main {
	
	public static void main(String[] args) {
		
		Cuenta cuenta = new Cuenta();
		Movimiento movimiento = new Movimiento();
		UsuarioAhorrador ua = new UsuarioAhorrador(movimiento, cuenta);
		UsuarioDerrochador ud = new UsuarioDerrochador(movimiento, cuenta);
		
		ua.start();
		ud.start();
	}

}
