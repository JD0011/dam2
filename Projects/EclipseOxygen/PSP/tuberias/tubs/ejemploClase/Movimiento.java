package tubs.ejemploClase;

public class Movimiento {

	public void ingreso (Cuenta cuenta, int importe) {
		 synchronized(cuenta){
			System.out.println("Voy a ingresar dinero");
			cuenta.Saldo+=importe;
			System.out.println("El saldo despu�s del ingreso es "+cuenta.Saldo);
		}
	}
	
	public void reintegro(Cuenta cuenta, int importe) {
		 synchronized(cuenta){
			System.out.println("Voy a sacar dinero");
			cuenta.Saldo-=importe;
			System.out.println("El saldo despu�s del reintegro es "+cuenta.Saldo);
		}
	}
}
