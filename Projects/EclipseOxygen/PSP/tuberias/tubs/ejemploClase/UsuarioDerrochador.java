package tubs.ejemploClase;

public class UsuarioDerrochador extends Thread {

	Movimiento movimiento;
	Cuenta cuenta;
	
	public UsuarioDerrochador(Movimiento movimiento, Cuenta cuenta) {
		this.movimiento = movimiento;
		this.cuenta = cuenta;
	}
	
	@Override
	public void run() {
		for(int i=0;i<10;i++) {
			
			
			movimiento.reintegro(cuenta, 500);
		}
	}
}