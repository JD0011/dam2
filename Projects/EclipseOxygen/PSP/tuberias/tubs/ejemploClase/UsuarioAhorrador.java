package tubs.ejemploClase;

public class UsuarioAhorrador extends Thread{

	Movimiento movimiento;
	Cuenta cuenta;
	
	public UsuarioAhorrador(Movimiento movimiento, Cuenta cuenta) {
		this.movimiento = movimiento;
		this.cuenta = cuenta;
	}
	
	@Override
	public void run() {
		for(int i=0;i<10;i++) {
			
			
			movimiento.ingreso(cuenta, 500);
		}
	}
	
	
}
