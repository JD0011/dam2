package ejemplo;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;

public class Client extends Thread{
	
	DataInputStream dis;
	DataOutputStream dos;
	
	public Client(PipedInputStream pIn) {
		dis = new DataInputStream(pIn);
		//dos = new DataOutputStream(pOut);
	}
	
	@Override
	public void run() {
		while(true) {
			try {
				int counter = dis.readInt();
				System.out.print("Client (Answer "+counter+"): ");
				double in = dis.readDouble();
				System.out.println(in*2);
				//dos.writeDouble(in*2);
				//dos.flush();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}
