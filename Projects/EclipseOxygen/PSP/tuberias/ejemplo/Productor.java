package ejemplo;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.PipedOutputStream;

public class Productor extends Thread{
	
	private DataOutputStream out;
	private double number;
	private int counter;
	
	public Productor(PipedOutputStream pOut) {
		out = new DataOutputStream(pOut);
	}
	
	@Override
	public void run() {
		while(true) {
			number = Math.random()*(10);
			
			try {
				Thread.sleep(500);
				System.out.println("Producer (Number "+counter+": "+number);
				
				out.writeInt(counter++);
				out.flush();
				
				out.writeDouble(number);
				out.flush();
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}
