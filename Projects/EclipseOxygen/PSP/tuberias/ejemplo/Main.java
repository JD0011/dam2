package ejemplo;
import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;

public class Main {
	
	public static void main(String[] args) {
		
		PipedOutputStream pOut = new PipedOutputStream();
		PipedInputStream pIn = null;
		try {
			pIn = new PipedInputStream(pOut);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Productor p1= new Productor(pOut);
		
		Client c1 = new Client(pIn);
		
		p1.start();
		c1.start();

	}
	
	
}
