package ejercicio1;

/**<h1>Tuber�a para intercomunicaci�n entre hilos</h1>
 * Clase tuber�a que posee una variable de clase que ser� modificada de forma externa mediante el m�todo
 * correspondiente
 * 
 * @author Juan De Dios Delgado Berm�dez
 * @since 4 oct. 2017
 */
public class Pipe {
	
	// Variable de clase
	private double number;
	
	//GETTER
	public double getNumber() {
		return number;
	}

	/**
	 * Incrementa la variable de clase {@link #number} por lo especificado en el paso
	 * de par�metros
	 * @param a Incremento a sumar
	 */
	public void incrementar(Double a) {
		number+=a;
	}
	
	

}
