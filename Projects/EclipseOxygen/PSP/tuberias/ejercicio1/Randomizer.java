package ejercicio1;

/**
 * <h1>Incrementa n�mero aleatorio en tuber�a</h1>
 * Esta clase incrementar� mediante un n�mero aleatorio el valor de la variable number
 * de la clase Tuberia. </br>
 * Adem�s, mostrar� el nombre del hilo que realiza dicha acci�n, la
 * cantidad que incrementa y el resultado por pantalla
 * 
 * @author Juan de Dios Delgado Berm�dez
 * @@since 4 oct. 2017
 */
public class Randomizer extends Thread{
	
	//Variables de clase
	Pipe t;
	String name;
	
	//Constructor
	public Randomizer(Pipe t, String name) {
		this.t= t;
		this.name = name;
	}
	
	
	@Override
	public void run() {
		while(true) {
			
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			//Genera un n�mero aleatorio del 0 al 10
			double rand = Math.random()*(10);
			
			t.incrementar(rand);
			
			//Imprime informaci�n
			System.out.println("Hilo "+name+", Genera: "+rand+", Resultado: "+t.getNumber());
			
		}
	}
	

}
