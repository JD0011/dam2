package ejercicio1;

/**<h1>Ejemplo de intercomunicaci�n de hilos</h1>
 * Identificar lo que sale y el resultado, adem�s del hilo que se ejecuta, el numero aleatorio de incremento
 * y el resultado que se ha obtenido
 * 
 * @author Juan de Dios Delgado Berm�dez
 * @since 4 oct. 2017
 */
public class Main {

	public static void main(String[] args) {
		
		// Instancia una �nica tuber�a
		Pipe t = new Pipe();

		// Hilos que modificar�n la variable de la tuber�a
		Randomizer h1 = new Randomizer(t, "h1");
		Randomizer h2 = new  Randomizer (t, "h2");
		
		h1.start();
		h2.start();

	}

}
