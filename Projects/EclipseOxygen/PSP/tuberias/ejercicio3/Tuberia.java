package ejercicio3;

public class Tuberia {

	private int num;
	
	public void recibirNum(int num) {
		this.num = num;
	}
	
	public int devolverNum() {
		return num;
	}
}
