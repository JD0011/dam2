package ejercicio3;

public class MainClass {
	
	public static void main(String[] args) throws InterruptedException {
		Tuberia t = new Tuberia();
		
		Thread ng = new Thread(new NumberGenerator(t));
		Thread fp = new Thread(new FactorialPrinter(t));
		
		ng.start();
		fp.start();
	}
	
	
	
	
}
