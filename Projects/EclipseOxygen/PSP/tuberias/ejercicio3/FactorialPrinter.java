package ejercicio3;

public class FactorialPrinter  implements Runnable{
	
	private int result=1;
	private Tuberia t;
	
	public FactorialPrinter(Tuberia t) {
		this.t = t;
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		for(int i= t.devolverNum();i>0;i--) {
			result *=i;
			
		}
		
		System.out.println("Factorial del n�mero "+t.devolverNum()+": "+result);
	}

}
