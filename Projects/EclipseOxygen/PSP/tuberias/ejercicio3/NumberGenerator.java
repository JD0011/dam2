package ejercicio3;

public class NumberGenerator implements Runnable{
	
	private Tuberia t;
	
	public NumberGenerator(Tuberia t) {
		this.t=t;
	}

	@Override
	public void run() {
		t.recibirNum((int)(Math.round(Math.random() * (30 - 5) + 5)));
	}

}
