package ejercicio2;

/**<h1>Tuber�a para intercomunicaci�n entre hilos</h1>
 * Clase tuber�a que posee una variable de clase que ser� modificada de forma externa mediante el m�todo
 * correspondiente. En este caso tenemos diferentes operaciones matem�ticas aplicables a la variable.
 * 
 * @author Juan De Dios Delgado Berm�dez
 * @since 4 oct. 2017
 */
public class Pipe {
	
	private double num;
	private int counter;
	
	public Pipe() {
		
	}
	
	public void multiplica(double a) {
		num*=a;
	}
	
	public void divide(double a) {
		num/=a;
	}
	
	public void resta(double a) {
		num-=a;
	}
	
	public void suma(double a) {
		num+=a;
	}
	
	public double getNum() {
		return num;
	}
	
	public int getCounter() {
		return counter;
	}

}
