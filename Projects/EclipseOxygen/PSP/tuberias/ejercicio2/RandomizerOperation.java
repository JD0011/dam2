package ejercicio2;

/**
 * <h1>Aplica una operaci�n matem�tica cuyo operando es aleatorio</h1>
 * Esta clase instanciar� diferentes hilo cuya operaci�n matem�tica ser� especificada en el 
 * constructor del hilo mediante el tipo enum {@link #op} </br>
 * Adem�s, mostrar� la operaci�n realizada, el n�mero generado y el resultado.
 * 
 * @author Juan de Dios Delgado Berm�dez
 * @@since 4 oct. 2017
 */
public class RandomizerOperation extends Thread{
	
	//Tipo enum con las posibles operaciones matem�ticas
	public static enum operation{
		
		MULTIPLICA(1),DIVIDE(2),SUMA(3),RESTA(4);
		
		private int opNumber;
		
		operation(int a){
			opNumber=a;
		}
		
		public int value() {
			return opNumber;
		}
	};
	
	//Variables de clase
	private operation op;
	private Pipe t;
	
	//Constructor
	public RandomizerOperation(operation a, Pipe t) {
		op=a;
		this.t = t;
	}
	
	@Override
	public void run() {
		while(true) {
			try {
				sleep(500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			double rand = Math.random()*(1 + 10)-1;
			
			switch (op.value()) {
				//Multiplica
				case 1:
						t.multiplica(rand);
					break;
				//Divide
				case 2:
						t.divide(rand);
					break;
				//Suma
				case 3:
						t.suma(rand);
					break;
				//Resta
				case 4:
						t.resta(rand);
					break;
			}
			
			System.out.println(op.name().toString()+". N�mero generado: "+rand+" Resultado: "+t.getNum());
		}
	}

}
