package ejercicio2;

import ejercicio2.RandomizerOperation.operation;

public class Main {
	
	public static void main(String[] args) {
		
		Pipe t = new Pipe();
		
		RandomizerOperation op1 = new RandomizerOperation(operation.MULTIPLICA, t);
		RandomizerOperation op2 = new RandomizerOperation(operation.DIVIDE, t);
		RandomizerOperation op3 = new RandomizerOperation(operation.SUMA, t);
		RandomizerOperation op4 = new RandomizerOperation(operation.RESTA, t);
		
		op1.start();
		op2.start();
		op3.start();
		op4.start();
		
		/*
		 * Se puede observar en algunas operaciones que, debido a problemas de sincronización,
		 * las operaciones matemáticas no terminan de realizarse sobre el número al que se aplica.
		 */
	}

}
