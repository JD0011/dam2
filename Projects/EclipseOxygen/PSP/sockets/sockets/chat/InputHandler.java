package sockets.chat;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.Socket;

public class InputHandler extends Thread{
	
	private Socket cs;
	
	public InputHandler(Socket cs) {
		this.cs=cs;
	}
	
	public Socket getSocket() {
		return cs;
	}

	@Override
	public void run() {
		DataInputStream dis=null;
		
		try {
			
			dis = new DataInputStream(cs.getInputStream());
			
			while(true) {
				if(dis.available()>0) {
					System.out.println(dis.readUTF());
				}
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			try {
				cs.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
	}
}
