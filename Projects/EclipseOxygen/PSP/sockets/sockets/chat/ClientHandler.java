package sockets.chat;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;

public class ClientHandler extends Thread{
	
	private ArrayList<Socket> clients;
	private Socket cs;
	
	public ClientHandler(Socket cs, ArrayList<Socket> clients) {
		this.clients=clients;
		this.cs=cs;
	}

	@Override
	public void run() {
		DataInputStream dis = null;
		DataOutputStream dos = null;
		String line;
		try {
			
			while(true) {
				dis = new DataInputStream(cs.getInputStream());
				if(dis.available()>0) {
					line = dis.readUTF();
					for(Socket other: clients) {
						if(other!=cs) {
							dos = new DataOutputStream(other.getOutputStream());
							dos.writeUTF("CLIENTE "+clients.indexOf(cs)+"-> "+line);
							dos.flush();
						}
					}
				}
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			try {
				dis.close();
				dos.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
	}
}
