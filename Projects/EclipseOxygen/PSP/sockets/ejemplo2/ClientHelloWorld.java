package ejemplo2;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

public class ClientHelloWorld extends Thread{
	
	private String host;
	private int port;
	private Socket cs;
	
	public ClientHelloWorld(String address, int port) {
		super();
		this.host = address;
		this.port = port;
	}
	
	@Override
	public void run() {
		try {
			cs = new Socket(host, port);
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		new OutputHandler(cs).start();
		new InputHandler(cs).start();
	}
	
	public static void main(String[] args) {
		new ClientHelloWorld("localhost", 1234).run();
	}

}
