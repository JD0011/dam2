package ejemplo2;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class ServerHelloWorld{

	private int port;
	
	private ServerSocket ss;
	private Socket cs;
	
	private ArrayList<Socket> clients;
	
	public ServerHelloWorld(int port) {
		super();
		this.port = port;
		
		clients = new ArrayList<Socket>();
	}


	public void run() {
		
		try {
			ss = new ServerSocket(port);
			while(true) {

				cs=ss.accept();
				
				new InputHandler(cs).start();
				new OutputHandler(cs).start();
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		new ServerHelloWorld(1234).run();
	}
}
