package ejemplo2;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;

public class OutputHandler extends Thread{

	private Socket cs;
	
	public OutputHandler(Socket cs) {
		this.cs=cs;
	}

	@Override
	public void run() {
		DataOutputStream dos = null;
		Scanner sc = null;
		
		try {
			
			dos = new DataOutputStream(cs.getOutputStream());
			
			boolean exitCondition = false;
			
			sc = new Scanner(System.in);
			
			String line;
			while(!exitCondition) {
				line = sc.nextLine();
				
				if(!line.equals("quit")) {
					dos.writeUTF(line);
					dos.flush();
				}else {
					exitCondition=true;
				}
			}
			
			dos.flush();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			try {
				dos.close();
				cs.close();
				sc.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
	}
}
