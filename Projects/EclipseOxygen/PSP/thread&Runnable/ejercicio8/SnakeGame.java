package ejercicio8;

public class SnakeGame extends Thread {
	
	private Dado dado1 = new Dado(), dado2 = new Dado();
	private int result;

	public int getResult() {
		return result;
	}

	public SnakeGame() {
	}
	
	@Override
	public void run() {
		dado1.start();
		dado2.start();
		
		try {
			dado1.join();
			dado2.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		result = dado1.getValue() + dado2.getValue();
	}
}
