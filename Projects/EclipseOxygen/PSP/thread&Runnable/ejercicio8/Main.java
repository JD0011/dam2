package ejercicio8;

public class Main {

	public static void main(String[] args) throws InterruptedException {
		
		SnakeGame game1 = new SnakeGame();
		
		game1.start();
		
		game1.join();
		
		System.out.println("Resultado: "+game1.getResult());

	}

}
