package ejercicio3;

public class Main {
	
	public static void main(String[] args) {
		
		NumberPrinterThread npThread1 = new NumberPrinterThread("Hilo 1"), 
				npThread2 = new NumberPrinterThread("Hilo 2");

		npThread1.start();
		npThread2.start();
	}
}
