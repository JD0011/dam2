package ejercicio3;

public class NumberPrinterThread extends Thread{
	
	private String name;
	private int num = 0;
	
	public NumberPrinterThread(String name) {
		this.name = name;
	}
	
	@Override
	public void run() {
		while(true) {
			System.out.println("("+name+")"+ num++);
			
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			if(num == 499) {
				continue;
			}
		}
		
	}
	
}
