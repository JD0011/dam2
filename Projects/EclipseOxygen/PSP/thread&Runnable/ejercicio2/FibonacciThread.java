package ejercicio2;

/**
 * This class prints the Fibonacci serie using the Thread class extend. 
 * 
 * @author juand
 *
 */
public class FibonacciThread extends Thread{
	
	/**
	 * Saves the number of numbers to show of the Fibonacci serie
	 */
	private int cant;
	
	/**
	 * With this constructor we speccify the quantity of numbers
	 * @param x Number of numbers of the Fibonacci serie
	 */
	public FibonacciThread(int x) {
		cant = x;
	}
	
	
	@Override
	public void run() {
		
		System.out.println("Cantidad de n�meros a mostrar: "+cant);
		
		
		//Saves the previous number to the current number in the serie
		int prevNum=0;
		
		//Represents the current number that we are printing in the console
		int currentNum=1;
		
		//Store the result of the sum between prevNum and currentNum
		int result;
		
		//We print the first number (0)
		System.out.print(prevNum);
		
		
		//Bucle that iterates over the serie Fibonacci and prints the specified number
		for(int i = cant-1; i>0;i--) {
			
			System.out.print(", ");
			System.out.print(currentNum);
			
			//Gets the next number in the serie
			result=prevNum+currentNum;
			
			//Sets the currentNum as the prevNumber
			prevNum=currentNum;
			
			//Sets the current number as the result for the next print
			currentNum = result;
			
		}
		
	}
	
	public static void main(String[] args) {
		FibonacciThread ft = new FibonacciThread(5);
		
		ft.start();
	}

}
