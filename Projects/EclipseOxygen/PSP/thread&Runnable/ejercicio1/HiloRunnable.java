package ejercicio1;

public class HiloRunnable implements Runnable{
	
	private int number, result=1;
	
	public HiloRunnable(int n) {
		// TODO Auto-generated constructor stub
		number= n;
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		for(int i= number;i>0;i--) {
			result *=i;
		}
	}
	
	public int getNumber() {
		return number;
	}

	public int getResult() {
		return result;
	}

}
