package ejercicio1;

public class MainClass {

	public static void main(String[] args) {
		
		//Execution of runnable class
		HiloRunnable h = new HiloRunnable(8);
		Thread t = new Thread(h);
		
		t.start();
		
		boolean exitCondition = false;
		while(!exitCondition) {
			if(!t.isAlive()) {
				System.out.println("(implements Runnable) Factorial del n�mero "+h.getNumber()+": "+h.getResult());
				exitCondition = true;
			}
		}
		
		//Execution of class extending Thread
		
		HiloThread ht= new HiloThread(6);
		
		ht.start();
		
		exitCondition = false;
		while(!exitCondition) {
			if(!ht.isAlive()) {
				System.out.println("(extend Thread) Factorial del n�mero "+ht.getNum()+": "+ht.getResult());
				exitCondition=true;
			}
		}
		
		
	}
	
}
