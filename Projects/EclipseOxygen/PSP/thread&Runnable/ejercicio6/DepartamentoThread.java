package ejercicio6;

public class DepartamentoThread extends Thread{
	
	private String dep;
	private String[] content;
	
	public DepartamentoThread(String dep, String[] content) {
		this.dep=dep;
		this.content=content;
	}
	
	@Override
	public void run() {
		for(String a: content) {
			System.out.println(dep+" : "+a);
		}
		
	}

}
