package ejercicio6;

public class Main {
	
	public static void main(String[] args) {
		
		String[] arrDeportes= {
				"Espa�a sigue adelante en el campeonato",
				"El M�laga C.F. Campe�n de Liga"
		};
		
		String[] arrEconomia= {
				"El IBEX35 baja medio punto",
				"La bolsa de NY se desploma",
				"Baja la prima de riesgo griega",
		};
		
		String[] arrPolitica= {
				"Espa�a sigue sin acuerdo de gobierno"
		};
		
		DepartamentoThread deportes = new DepartamentoThread("Deportes", arrDeportes);
		DepartamentoThread economia = new DepartamentoThread("Econom�a", arrEconomia);
		DepartamentoThread politica = new DepartamentoThread("Pol�tica", arrPolitica);
		
		deportes.start();
		economia.start();
		politica.start();
	}

}
