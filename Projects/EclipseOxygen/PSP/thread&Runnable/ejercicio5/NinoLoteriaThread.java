package ejercicio5;

public class NinoLoteriaThread extends Thread{
	
	private double numbers;
	private String position;
	
	public NinoLoteriaThread(String position) {
		this.position = position;
	}
	
	@Override
	public void run() {
		numbers = Math.round((Math.random() * (9 - 0) + 0));
		
		System.out.println(position+" "+numbers);
	}

}
