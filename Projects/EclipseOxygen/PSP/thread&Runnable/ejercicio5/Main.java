package ejercicio5;

public class Main {
	
	public static void main(String[] args) throws InterruptedException {
		
		NinoLoteriaThread n1 = new NinoLoteriaThread("Decenas de millar");
		NinoLoteriaThread n2 = new NinoLoteriaThread("Ud. de millar");
		NinoLoteriaThread n3 = new NinoLoteriaThread("Centenas");
		NinoLoteriaThread n4 = new NinoLoteriaThread("Decenas");
		NinoLoteriaThread n5 = new NinoLoteriaThread("Unidades");
		
		n1.start();
		n1.join();
		
		n2.start();
		n2.join();
		
		n3.start();
		n3.join();
		
		n4.start();
		n4.join();
		
		n5.start();
		n5.join();
	}

}
