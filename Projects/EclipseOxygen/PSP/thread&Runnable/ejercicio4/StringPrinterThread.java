package ejercicio4;

public class StringPrinterThread extends Thread{
	
	private String color;
	
	public void setColor(String c) {
		color = c;
	}
	
	public StringPrinterThread() {
	}
	
	@Override
	public void run() {
		System.out.println("Hola, el mundo es de color "+color);
	}

}
