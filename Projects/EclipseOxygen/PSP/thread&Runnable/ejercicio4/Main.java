package ejercicio4;

import java.util.Scanner;

public class Main {
	
	public static void main(String[] args) throws InterruptedException {
		StringPrinterThread spt1 = new StringPrinterThread(),
				spt2 = new StringPrinterThread(),
				spt3 = new StringPrinterThread();

		Scanner sc = new Scanner(System.in);
		
		System.out.println("Introduce el color 1: ");
		
		spt1.setColor(sc.nextLine());
		spt1.start();
		
		//Thread.join(); pauses the current thread until it finish
		spt1.join();
		
		System.out.println("Introduce el color 2: ");
		
		spt2.setColor(sc.nextLine());
		spt2.start();
		
		spt2.join();
		
		System.out.println("Introduce el color 3: ");
		
		spt3.setColor(sc.nextLine());
		spt3.start();
	}
	
	
	
	

}
