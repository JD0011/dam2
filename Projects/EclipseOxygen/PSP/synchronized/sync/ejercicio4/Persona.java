package sync.ejercicio4;

public class Persona extends Thread{
	
	private Perol _perol;
	private String _strNombre;

	public Persona(String strNombre,Perol perol) {
		_strNombre = strNombre;
		_perol = perol;
	}

	public String getNombre() {
		return _strNombre;
	}
	
	@Override
	public void run() {
		while(true) {
			_perol.tomarCucharada(this);
		}
		
	}

}
