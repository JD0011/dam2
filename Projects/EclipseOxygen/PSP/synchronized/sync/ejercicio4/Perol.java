package sync.ejercicio4;

public class Perol {

	String _strPersonaComiendo;
	
	
	public synchronized void tomarCucharada(Persona per) {
		if(_strPersonaComiendo==null) {
			_strPersonaComiendo=per.getNombre();
		}else {
			while(_strPersonaComiendo==per.getNombre()) {
				try {
					wait();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		System.out.println(per.getNombre()+" est� tomando una cucharada");
		
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		_strPersonaComiendo=null;
		
	}

}
