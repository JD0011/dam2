package sync.ejercicio1;

public class Persona  extends Thread{
	
	String _strCargo;
	Palacio _palacio;
	
	public Persona(String cargo,Palacio t) {
		_strCargo = cargo;
		_palacio = t;
	}

	public String getCargo() {
		return _strCargo;
	}

	@Override
	public void run() {
		_palacio.saludo(_strCargo);
	}

}
