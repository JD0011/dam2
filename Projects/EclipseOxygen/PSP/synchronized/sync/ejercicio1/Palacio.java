package sync.ejercicio1;

public class Palacio {
	
	private boolean estaElRey = false;
	
	/**
	 * While the king hasnt arrived and was a knight who called this method, make then wait
	 * until the king arrives. 
	 * @param per
	 */
	public synchronized void saludo(String cargo) {
			while(!estaElRey && cargo!="rey") {
				try {
					wait();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			estaElRey = true;
			
			if(cargo=="rey") {
				System.out.println("Bienvenidos caballeros");
			}else {
				System.out.println("Hola su majestad");
			}
		
		notifyAll();
	}

}
