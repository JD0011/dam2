package sync.ejercicio1;

import javax.swing.plaf.SliderUI;

public class Main {
	
	public static void main(String[] args) {
		
		Palacio PalacioReal = new Palacio();
		
		Persona rey = new Persona("rey",PalacioReal);
		Persona cab1 = new Persona("caballero",PalacioReal);
		Persona cab2 = new Persona("caballero",PalacioReal);
		Persona cab3 = new Persona("caballero",PalacioReal);
		
		cab1.start();
		cab2.start();
		cab3.start();
		
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		rey.start();
	}
	
	
}
