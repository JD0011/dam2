package sync.ejercicio2;

/**
 * Represents each of the people that is going to drink from a fountain
 * @author juand
 *
 */
public class Persona extends Thread{
	
	private Fuente _fntObjetivo;

	private String _strNombre;
	
	
	public Persona(Fuente fuenteObjetivo, String strNombre) {
		_fntObjetivo = fuenteObjetivo;
		_strNombre = strNombre;
	}

	public String getNombre() {
		return _strNombre;
	}

	@Override
	public void run() {
		while(true) {
			_fntObjetivo.bebe(this);
		}
		
	}
	
}
