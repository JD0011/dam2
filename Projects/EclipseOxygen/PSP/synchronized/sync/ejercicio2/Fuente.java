package sync.ejercicio2;

public class Fuente {
	
	String _strPersonaBebiendo;
	
	public synchronized void bebe(Persona per) {
		if(_strPersonaBebiendo==null) {
			_strPersonaBebiendo = per.getNombre();
		}else {
			while(_strPersonaBebiendo==per.getNombre()) {
				try {
					wait();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		System.out.println(per.getNombre() +" est� bebiendo de la fuente");
		
		//Espera 3 segundos
		try {
			per.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		_strPersonaBebiendo = null;
		
		notifyAll();
		
	}

}
