package sync.ejercicio2;

public class Main {

	public static void main(String[] args) {
		
		Fuente fuente = new Fuente();
		
		Persona per1 = new Persona(fuente, "Juan");
		Persona per2 = new Persona(fuente, "Rosa");
		
		per1.start();
		per2.start();

	}

}
