package sync.ejercicio3;

public class Main {
	
	public static void main(String[] args) {
		Contador t = new Contador();
		ManipuladorContador mc1 =new ManipuladorContador("Hilo1", t);
		ManipuladorContador mc2 =new ManipuladorContador("Hilo2", t);
		
		mc1.start();
		mc2.start();
		
	}
}
