package sync.ejercicio3;

public class ManipuladorContador extends Thread{
	
	//Tuber�a
	private Contador t;
	private String strNombre;

	public ManipuladorContador(String strNombre, Contador t) {
		this.strNombre=strNombre;
		this.t = t;
	}	
	
	public String getNombre() {
		return strNombre;
	}

	@Override
	public void run() {
		while(t.getNum()<500) {
			t.aumentar(this);
			
			try {
				sleep(1);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}
