package sync.ejercicio3;

public class Contador {
	
	private	int num;
	
	public int getNum() {
		return num;
	}
	
	public synchronized void aumentar(ManipuladorContador mc) {
		System.out.println(mc.getNombre()+" Contador: "+num++);
	}

}
