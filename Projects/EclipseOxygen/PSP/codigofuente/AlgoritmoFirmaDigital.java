package Ejemplos;
import java.security.*;

public class AlgoritmoFirmaDigital
{
  public static void main(String[] args)
  {
    try
    {
      // PASO 1, FIRMAR
      // Creamos el par de claves usando el algoritmo DSA
      KeyPairGenerator keygen = KeyPairGenerator.getInstance("DSA");
      KeyPair keypair = keygen.generateKeyPair();
      // Creamos la firma digital
      Signature signature = Signature.getInstance("DSA");
      // Inicializo la firma con la clave privada
      signature.initSign(keypair.getPrivate());
      String mensaje = "Mensaje a firmar";
      // Actualizo con el mensaje y firmo
      signature.update(mensaje.getBytes());
      byte[] firma = signature.sign();
      // PASO 2, VERIFICACIÓN DE LA FIRMA
      // Inicializo la verificación de la firma con la clave pública y actualizo
      // con el mensaje
      signature.initVerify(keypair.getPublic());
      signature.update(mensaje.getBytes());
      // Compruebo la veracidad de la firma
      if (signature.verify(firma)) System.out.println("El mensaje es auténtico :-)");
      else System.out.println("Intento de falsificación");
    } catch (Exception e)
    {
      e.printStackTrace();
    }
  }
}
