package programacionSegura;

import java.security.spec.KeySpec;
import javax.crypto.SecretKeyFactory; //Clase para operar con objetos SecretKey
import javax.crypto.spec.DESKeySpec; //usaremos el algoritmo DES
import javax.crypto.spec.SecretKeySpec;
import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey; //Para implementar la interfaz Key



public class E1EjemploCipher {

		public static void main(String[] args){
			try{
				System.out.println("Obteniendo generador de claves con cifrado DES");
				KeyGenerator keygen=KeyGenerator.getInstance("DES");
				
				System.out.println("Generando clave");
				SecretKey key=keygen.generateKey();				

				System.out.println("Obteniendo objeto Cipher con cifrado DES");
				Cipher desCipher=Cipher.getInstance("DES");
				System.out.println("Configurando Cipher para encriptar");
				System.out.println("La clave es: ");
				desCipher.init(Cipher.ENCRYPT_MODE, key);
				System.out.println("Preparando Mensaje");
				String mensajeoriginal="Programa arreglado";
				System.out.println("/////////////////////Mensaje original: "+mensajeoriginal);
				System.out.println("Cifrando mensaje");
				byte[] mensaje = desCipher.doFinal(mensajeoriginal.getBytes());
				System.out.println("/////////////////////Mensaje Cifrado: "+mensaje);
								
				desCipher.init(Cipher.DECRYPT_MODE, key);
				System.out.println("Preparando marcha atr�s");
				System.out.println("Descifrando");
				String mensajeDescifrado=new String (desCipher.doFinal(mensaje));
				System.out.println("/////////////////////Mensaje Descifrado: "+mensajeDescifrado);
				System.out.println("Obteniendo factor�a de claves con cifrado DES");
				SecretKeyFactory keyfac=SecretKeyFactory.getInstance("DES");
				System.out.println("Generando keyspec");
				KeySpec keyspec=keyfac.getKeySpec(key, DESKeySpec.class);
				System.out.println("clave: "+keyspec);
			}catch (Exception e){
				e.printStackTrace();
			}
		}
}
