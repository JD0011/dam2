package clienteServidorFirmado;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.math.BigInteger;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.Signature;
import java.security.spec.DSAPublicKeySpec;

public class Usuario {
  public static void main(String[] args) {
    try {
      BufferedReader br = new BufferedReader(
                                             new FileReader(
                                                            "d:\\clave_publica_firma.txt"));
      BigInteger Y = new BigInteger(br.readLine());
      BigInteger P = new BigInteger(br.readLine());
      BigInteger Q = new BigInteger(br.readLine());
      BigInteger G = new BigInteger(br.readLine());
      DSAPublicKeySpec keyspec = new DSAPublicKeySpec(Y, P, Q, G);
      KeyFactory keyfac = KeyFactory.getInstance("DSA");
      PublicKey public_key = keyfac.generatePublic(keyspec);
      Signature signature = Signature.getInstance("DSA");
      signature.initVerify(public_key);
      File inf = new File("d:\\fichero_prueba.txt");
      FileInputStream is = new FileInputStream(inf);
      byte[] buffer = new byte[64];
      int bytes_leidos = is.read(buffer);
      while (bytes_leidos != -1) {
        signature.update(buffer, 0, bytes_leidos);
        bytes_leidos = is.read(buffer);
      }
      File insf = new File("d:\\fichero_de_firma.txt");
      FileInputStream isf = new FileInputStream(insf);
      byte[] firma = new byte[(int) insf.length()];
      isf.read(firma);
      if (signature.verify(firma)) System.out.println("El fichero no ha sido alterado :-)");
      else System.out.println("�ATENCI�N! La firma no es v�lida. Quiz�s el fichero haya sido alterado.");
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}