package clienteServidorFirmado;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.Signature;
import java.security.spec.DSAPublicKeySpec;

public class ServidorFirmadoDigital {
  public static void main(String[] args) {
    try {
      KeyPairGenerator keygen = KeyPairGenerator.getInstance("DSA");
      System.out.println("Generando par de claves");
      KeyPair keypair = keygen.generateKeyPair();
      Signature signature = Signature.getInstance("DSA");
      System.out.println("Firmando mensaje");
      signature.initSign(keypair.getPrivate());
      System.out.println("Abriendo el fichero");
      File inf = new File("d:\\fichero_prueba.txt");
      FileInputStream is = new FileInputStream(inf);
      System.out.println("Firmando el fichero...");
      byte[] buffer = new byte[64];
      int bytes_leidos = is.read(buffer);
      while (bytes_leidos != -1) {
        signature.update(buffer, 0, bytes_leidos);
        bytes_leidos = is.read(buffer);
      }
      byte[] firma = signature.sign();
      System.out.println("Abriendo el fichero de firma");
      FileOutputStream os = new FileOutputStream("d:\\fichero_de_firma.txt");
      os.write(firma);
      os.close();
      System.out.println("Obteniendo factor�a de claves con cifrado DSA");
      KeyFactory keyfac = KeyFactory.getInstance("DSA");
      System.out.println("Generando keyspec");
      DSAPublicKeySpec publicKeySpec = keyfac.getKeySpec(keypair.getPublic(),
                                                         DSAPublicKeySpec.class);
      System.out.println("Salvando la clave en un fichero");
      FileOutputStream cos = new FileOutputStream("d:\\clave_publica_firma.txt");
      PrintWriter cpw = new PrintWriter(cos);
      cpw.println(publicKeySpec.getY());
      cpw.println(publicKeySpec.getP());
      cpw.println(publicKeySpec.getQ());
      cpw.println(publicKeySpec.getG());
      cpw.close();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}