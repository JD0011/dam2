package Ejemplos;
import java.security.*;
import java.util.Scanner;
import javax.crypto.*;
import java.io.*;

public class AlgoritmoRSA
{
  public static void main(String[] args) throws Exception
  {

    System.out.println("1. Creando claves publica y privada");

    // PASO 1: Crear e inicializar el par de claves RSA DE 512 bits
    KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
    keyGen.initialize(512); // tamano clave 512 bits
    KeyPair clavesRSA = keyGen.generateKeyPair();
    PrivateKey clavePrivada = clavesRSA.getPrivate();
    PublicKey clavePublica = clavesRSA.getPublic();

    System.out.print("2. Introducir Texto Plano (max. 64 caracteres): ");
    byte[] bufferPlano = leerLinea();

    // PASO 2: Crear cifrador RSA
    Cipher cifrador = Cipher.getInstance("RSA");

    // PASO 3a: Poner cifrador en modo CIFRADO
    cifrador.init(Cipher.ENCRYPT_MODE, clavePublica); // Cifra con la clave
                                                      // publica

    System.out.println("3a. Cifrar con clave publica");
    byte[] bufferCifrado = cifrador.doFinal(bufferPlano);
    System.out.println("TEXTO CIFRADO");
    mostrarBytes(bufferCifrado);
    System.out.println("\n-------------------------------");

    // PASO 3b: Poner cifrador en modo DESCIFRADO
    cifrador.init(Cipher.DECRYPT_MODE, clavePrivada); // Descrifra con la clave
                                                      // privada

    System.out.println("3b. Descifrar con clave privada");
    byte[] bufferPlano2 = cifrador.doFinal(bufferCifrado);
    System.out.println("TEXTO DESCIFRADO");
    mostrarBytes(bufferPlano2);
    System.out.println("\n-------------------------------");

    // PASO 3a: Poner cifrador en modo CIFRADO
    cifrador.init(Cipher.ENCRYPT_MODE, clavePrivada); // Cifra con la clave
                                                      // publica

    System.out.println("4a. Cifrar con clave privada");
    bufferCifrado = cifrador.doFinal(bufferPlano);
    System.out.println("TEXTO CIFRADO");
    mostrarBytes(bufferCifrado);
    System.out.println("\n-------------------------------");

    // PASO 3b: Poner cifrador en modo DESCIFRADO
    cifrador.init(Cipher.DECRYPT_MODE, clavePublica); // Descrifra con la clave
                                                      // privada

    System.out.println("4b. Descifrar con clave publica");
    bufferPlano2 = cifrador.doFinal(bufferCifrado);
    System.out.println("TEXTO DESCIFRADO");
    mostrarBytes(bufferPlano2);
    System.out.println("\n-------------------------------");
  } // Fin main

  public static byte[] leerLinea() throws IOException
  {
    System.out.println("Introduce el mensaje a cifrar");
    Scanner sc = new Scanner(System.in);
    String linea = sc.nextLine();
    return linea.getBytes();
  }

  public static void mostrarBytes(byte[] buffer)
  {
    System.out.write(buffer, 0, buffer.length);
  }
} // Fin clase