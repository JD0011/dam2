import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.security.MessageDigest;
import java.util.Scanner;

public class AlgoritmoFuncionHASH
{
  public static void main(String[] args)
  {
    try
    {
      // PASO 1: Obtener instancia del algoritmo MD5
      System.out.println("Obteniendo instancia");
      MessageDigest md = MessageDigest.getInstance("MD5");
      
      //Almacenamiento del fichero en buffer
      File fichero = new File("C:/Users/user/Desktop/ficheroJD.txt");
      FileInputStream fis = new FileInputStream(fichero);
      byte[] buffer = new byte[(int)fichero.length()];
      fis.read(buffer, 0, buffer.length);
      fis.close();
      
      md.update(buffer);
      System.out.println("calculando resumen");
      byte[] resumen = md.digest();
      System.out.println("Resumen de fichero : " + new String(resumen));
      
      resumen = md.digest();
      // Anotación para el usuario
      System.out.println("PODEMOS COMPROBAR COMO SIEMPRE SALE EL MISMO RESULTADO TANTAS VECES LO EJECUTE");
      
      //Guardado del resumen
      File output = new File("C:/Users/user/Desktop/resumenFichero.txt");
      FileOutputStream fos = new FileOutputStream(output);
      fos.write(resumen);
      fos.close();
      
    } catch (Exception e)
    {
      e.printStackTrace();
    }
  }
}
