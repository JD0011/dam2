package Ejemplos;
import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;

public class AlgoritmoAES
{

  public static void main(String[] args)
  {
    try
    {
      // PASO 1: Crear claves AES
      System.out.println("Obteniendo generador de claves con cifrado AES");
      KeyGenerator keygen = KeyGenerator.getInstance("AES");
      System.out.println("Generando clave");
      SecretKey key = keygen.generateKey();
      // PASO 2: Crear cifrador AES
      System.out.println("Obteniendo objeto Cipher con cifraddo AES");
      Cipher desCipher = Cipher.getInstance("AES");
      // PASO 3a: Poner cifrador en modo CIFRADO
      System.out.println("Configurando Cipher para encriptar");
      desCipher.init(Cipher.ENCRYPT_MODE, key);
      System.out.println("Preparando Mensaje");
      String mensaje = "Preparando terreno para encriptar EN AES";
      System.out.println("Mensaje original: " + mensaje);
      System.out.println("Cifrando mensaje");
      // CIFRADO
      String mensajeCifrado = new String(desCipher.doFinal(mensaje.getBytes()));
      System.out.println("Mensaje Cifrado: " + mensajeCifrado);
      // PASO 3b: Poner cifrador en modo DESCIFRADO
      System.out.println("Configurando Cipher para desencriptar");
      desCipher.init(Cipher.DECRYPT_MODE, key);
      System.out.println("Descifrando mensaje");
      // DESCIFRADO
      String mensajeDescifrado = new String(
                                            desCipher.doFinal(mensajeCifrado.getBytes()));
      System.out.println("Mensaje Descifrado: " + mensajeDescifrado);
    } catch (Exception e)
    {
      e.printStackTrace();
    }
  }
}
