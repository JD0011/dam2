package ejercicio1;

import java.io.File;
import java.util.ArrayList;

public class Main {
	
	public static void main(String[] args) {
		
		File dir = new File ("./");
		
		//Saving the files of the current folder into files var
		File[] files = dir.listFiles();
		
		//Printing the name of each file in the console
		for(File a: files) {
			System.out.println(a.getName());
		}
		
	}

}
