package ejercicio8;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class ReadFromFile {
	
	public static void readBytesFrom(String path) {
		
		FileInputStream fis = null;
		BufferedInputStream bis = null;
		
		try {
			fis = new FileInputStream(new File(path));
			bis = new BufferedInputStream(fis);
			
			
			//Reading and printing
			int c;
			while((c=bis.read())!=-1) {
				System.out.print((char)c);
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
public static void readCharsFrom(String path) {
		
		FileReader reader = null;
		
		try {
			reader = new FileReader(new File(path));
			
			//Buffer size
			char[] buffer= new char[1];
			
			//Reading and printing
			while(reader.read(buffer)!=-1) {
				System.out.print(buffer);
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Indique la ruta del fichero a leer: ");
		
		String path = sc.nextLine();
		
		path = "C:/Users/juand/Desktop/test lectura.txt";
		
		readBytesFrom(path);
		
	}

}
