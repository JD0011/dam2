package ejercicio3;

import java.io.File;
import java.util.Scanner;

public class Main {
	
	public static void deleteFile(File file) {
		for(File a: file.listFiles()) {
			if (a.isDirectory()){
				deleteFile(a);
			}else {
				a.delete();
			}
		}
	}
	
	public static void deleteFolder(File file) {
		//Delete folders inside file
		for(File a: file.listFiles()) {
			a.delete();
		}
	}
	
	private void deleteAllContent(File file) {
		// Primero se elimina todo el contenido de la carpeta con el metodo deleteFile() si hay contenido
		if(file.listFiles().length>0) {
			deleteFile(file);
		}
		
		//Ahora comprobamos las carpetas
		for(File directorio: file.listFiles()) {
			if(directorio.listFiles().length>0 && directorio.isDirectory()) {
				directorio.delete();
			}else {
				deleteAllContent(directorio);
			}
		}
		
		
	}
	
	public static void main(String[] args) {
		
		//C:\Users\juand\Desktop\nada.txt
		
		//Asking for the file to delete
		System.out.print("Introduce el fichero a eliminar: ");
		Scanner sc = new Scanner(System.in);
		
		File toDelete = new File(sc.nextLine());
		deleteFile(toDelete);
		//Notifying by console
		System.out.println("Fichero eliminado");
		deleteFolder(toDelete);
		
	}

}
