package ejercicio4;

import java.io.File;
import java.util.Scanner;

public class Main {
	
	public static void main(String[] args) {
		
		//Asking for directory
		System.out.println("Introduce un directorio: ");
		Scanner sc = new Scanner(System.in);
		String path = sc.nextLine();
		
		File a = new File (path);
		
		//Controlling possible errors reading the folder
		if(!a.exists()) {
			System.out.println("El fichero "+path+" no exite");
		}else if(!a.isDirectory()){
			System.out.println("El fichero "+path+" no es un directorio");
		}else {
			
			//If proceed, showing the folder content
			System.out.println("Contenido de la carpeta");
			for(File b: a.listFiles()) {
				System.out.println("  "+b.getName());
			}
		}
	}

}
