
package ejercicio6;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.Logger;

import ejercicio5.Departamento;

public class DepartamentModifierCharacter {
	
	private Logger l = Logger.getLogger(this.getClass().getSimpleName());
	
	private File source, resultFile;
	
	// Departament to work with
	private Departamento dep;
	
	public DepartamentModifierCharacter(String pathSource) {
		
		//Create the file objects
		source = new File(pathSource);
		resultFile = new File(source.getParent(),source.getName()+".temp");
		
		try {
			resultFile.createNewFile();
			
			l.info("The new file with updated info has been created");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	
		l.info("Modifier instantiated for file: "+pathSource);
		
	}
	
	/**
	 * This method reads from the specified path the Departaments written in binary and modifies the specified
	 * departament by <b>ID</b>
	 * @param pathSource
	 */
	public void editDepartamentById(int id) throws IOException {
		
		l.info("Request to modify the departament with id "+id);
		
		//Input & Output
		FileReader reader = null;
		FileWriter writer = null;
		
		BufferedReader buffReader = null;
		BufferedWriter buffWriter = null;
		
		try {
			
			reader = new FileReader(source);
			buffReader = new BufferedReader(reader);

			writer = new FileWriter(resultFile);
			buffWriter = new BufferedWriter(writer);

			
			int numDep;
			String nom, loc;
						
			//Current line
			String line = null;
			//Read, modify and write
			while((line = buffReader.readLine())!= null) {
				
				//dRead data using '/' as separator
				numDep = Integer.parseInt(line.substring(0, line.indexOf('/')));
				nom = line.substring(line.indexOf('/')+1,line.lastIndexOf('/'));
				loc = line.substring(line.lastIndexOf('/')+1, line.length());
				
				// If they aren't the departaments we are looking for, save them in the new file
				if(numDep!=id) {
					buffWriter.write(line);
					buffWriter.newLine();
				
				//Isolate the target
				}else {			
					
					l.info("Departament found!");
					
					dep = new Departamento(numDep, nom, loc);
					modifyDepartament();
					
					//Once modified, save it into the temp file
					buffWriter.write(dep.ToLine());
					buffWriter.newLine();
					
					l.info("The dep. has been written succesfully");
				}
				
			}
			
			//First of all, close the input & output streams, so the source file gets free
			buffReader.close();	
			reader.close();
		
			
			buffWriter.close();
			writer.close();
			
			
			//Replace the old file with the new file
			replaceFile();
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Will help to modify the fields of the Departamente from the console, using the console as the output. It will ask to the user
	 * which of the fields will be modified and to insert the new data
	 */
	private void modifyDepartament() {
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Informaci�n actual del departamento");
		System.out.println(
							"N�mero de departamento: "+dep.getNumDep()+"\n"
							+"Nombre: "+dep.getNombre()+"\n"
							+"Localidad: "+dep.getLocalidad()+"\n");
		
		System.out.println("�Qu� datos quiere modificar? ");
		System.out.println("1.Nombre \n2.Localidad");
		
		//Menu options processing
		switch (sc.nextInt()) {
		case 1:
				//Clear the scanner
				sc = new Scanner(System.in);
				
				System.out.println("Introduce el nuevo nombre: ");
				//Set the new name
				dep.setNombre(sc.nextLine());
				break;
		case 2:
				//Clear the scanner
				sc = new Scanner(System.in);
				
				System.out.println("Introduce la nueva localidad");
				//Set the new location
				dep.setLocalidad(sc.nextLine());
				break;
		default:
			break;
		}
	}
	
	private void replaceFile() {
		l.info(source.delete() ? "Source file has been deleted successfully":"The file wasn't deleted");
		l.info(resultFile.renameTo(source) ? "File has been renamed successfully" : "The file wasn't renamed");
	}

}