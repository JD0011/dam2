package ejercicio6;

import java.io.IOException;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {
	
	static boolean  exitCondition = false;
	
	public static void main(String[] args) {
		
		Scanner sc;
		
		DepartamentModifierBinary depModifierB = new DepartamentModifierBinary("C:\\Users\\juand\\Desktop\\DAM2\\Projects\\EclipseOxygen\\ACDA\\Ficheros\\Departamentos.dat");
		//DepartamentModifierCharacter depModifierC = new DepartamentModifierCharacter("C:\\Users\\juand\\Desktop\\DAM2\\Projects\\EclipseOxygen\\ACDA\\Ficheros\\Departamentos.txt");
		
		
		System.out.print("�Qu� departamento quieres modificar?Introduce ID:");
		
		
		int id;
		//Control that user insert only numbers
		while(!exitCondition) {
			//Clear the last value inserted
			sc = new Scanner(System.in);
				try {
					//Ask for the number
					id = sc.nextInt();
					exitCondition = true;
					
					//Modify the specified departament (Binary way)
					depModifierB.editDepartamentById(id);
					
				}catch(InputMismatchException e){
					System.out.println("�Error! Debes introducir un n�mero: ");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
	
	}

}
