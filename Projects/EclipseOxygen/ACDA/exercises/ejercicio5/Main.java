package ejercicio5;

import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;

public class Main {
	
	private static File fileBinary = new File("./Ficheros/Departamentos.dat"),
						fileText = new File("./Ficheros/Departamentos.txt");
	
	public static void recordDepBynaryWay(Departamento a) throws IOException {
		FileOutputStream fos = null;
		DataOutputStream dos = null;
		
		try {
			
			if(!fileBinary.exists()) {
				fileBinary.createNewFile();
			}
			
			//Binary access to the file
			fos = new FileOutputStream(fileBinary, true);
			
			//Manipulate primitive types
			dos = new DataOutputStream(fos);
			
			dos.writeInt(a.getNumDep());
			dos.writeUTF(a.getNombre());
			dos.writeUTF(a.getLocalidad());		
			
			
			fos.close();
			dos.close();
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	private static void recordDepCharacterWay(Departamento a) throws IOException {
		
		FileWriter fw = null;
		BufferedWriter buffer = null;
		
		try {
			
			if(!fileText.exists()) {
				fileText.createNewFile();
			}
			
			fw = new FileWriter(fileText, true);
			buffer = new BufferedWriter(fw);
			
			buffer.write(String.valueOf(a.getNumDep())+'/');
			buffer.write(a.getNombre()+'/');
			buffer.write(a.getLocalidad());

			buffer.newLine();
			
			buffer.close();
			fw.close();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void main(String[] args) throws IOException {
		
		
		
		Departamento[] deps = {
				new Departamento(2, "Informática", "Sur"),
				new Departamento(3, "Ciencias", "Norte"),
				new Departamento(4, "Contabilidad", "Este"),
				new Departamento(5, "Ventas", "Oeste")
		};
		
		for(Departamento a: deps) {
			recordDepBynaryWay(a);
			recordDepCharacterWay(a);
		}
	}

}
