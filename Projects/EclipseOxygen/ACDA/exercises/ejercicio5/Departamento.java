package ejercicio5;

public class Departamento{
	
	private int numDep;
	private String nombre, localidad;
	
	public int getNumDep() {
		return numDep;
	}
	
	public String getNombre() {
		return nombre;
	}
	
	public String getLocalidad() {
		return localidad;
	}
	
	public void setNumDep(int numDep) {
		this.numDep = numDep;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}
	
	//Gives a line with all fields of the Departament
	public String ToLine() {
		return numDep+"/"+nombre+"/"+localidad;
	}
	
	@Override
	public String toString() {
		return "Departamento [numDep=" + numDep + ", nombre=" + nombre + ", localidad=" + localidad + "]";
	}

	public Departamento(int numDep, String nombre, String localidad) {
		this.numDep=numDep;
		this.nombre=nombre;
		this.localidad=localidad;
	}
}

