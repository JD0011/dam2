package ejercicio2;

import java.io.File;

public class Main {
	
	static int counter = 1;
	
	public static void listFiles(File file) {
		for(File a: file.listFiles()) {
			for(int i=0;i<counter;i++) {
				System.out.print("  ");
			}
			System.out.println(a.getName());
			if (a.isDirectory()){
				counter++;
				listFiles(a);
			}else {
				counter--;
			}
		}
	}
	
	public static void main(String[] args) {
		
		File dir = new File ("./");
		
		//Printing the name of each file in the console
		listFiles(dir);
	}
}
