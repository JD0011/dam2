package ejercicio9;

import java.io.File;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		
		//Creo empledaos
		Empleado[] emps = {
				new Empleado(1, "Pepe", 32, 1300),
				new Empleado(2, "Antonio", 41, 1400),
				new Empleado(3, "Ana", 38, 1400),
				new Empleado(4, "Mar�a", 25, 1300) 
		};
		
		
		EmpleadoWriter writer = new EmpleadoWriter("./Ficheros/Empleados.dat");
				
		for(Empleado a: emps) {
			writer.writeEmployeeObject(a);
		}
		
		EmpleadoReader reader = new EmpleadoReader("./Ficheros/Empleados.dat");

		Scanner sc = new Scanner(System.in);
		
		System.out.println("Introduce un ID de empleado");
		
		reader.readEmployeeObjectByID(sc.nextInt());
		
		
	}

}
