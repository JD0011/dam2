package ejercicio9;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;

public class EmpleadoReader {
	
	private File _fSource;
	private Empleado _empTarget;
	
	public EmpleadoReader(String filePath) {
		_fSource = new File(filePath);
	}
	
	public void readEmployeeBinaryByID(int IDBuscado) {
		
		//Flujos de entrada
		FileInputStream fis = null;
		DataInputStream dis = null;
		
		try {			
			
			//Inicializa los flujos
			fis = new FileInputStream(_fSource);
			dis = new DataInputStream(fis);
			
			String nombre;
			int ID, edad;
			double sueldo;
			
			//Recorre los datos del fichero
			while(dis.available()>0) {
				
				ID = dis.readInt();
				nombre = dis.readUTF();
				edad = dis.readInt();
				sueldo = dis.readDouble();
				
				if(ID == IDBuscado) {
					
					_empTarget = new Empleado(ID, nombre, edad, sueldo);
					printEmployeeInfo();
					
				}
			}
			
			//Notifica que no existe el empleado
			if(_empTarget == null) {
				System.out.println("�El empleado con ID "+IDBuscado+" no existe!");
			}
			
			//Cierra los flujos
			fis.close();
			dis.close();
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void readEmployeeObjectByID(int ID) {
		
		FileInputStream fis = null;
		ObjectInputStream ois = null;
		try {
			
			fis = new FileInputStream(_fSource);
			ois = new ObjectInputStream(fis);
			
			Empleado a = (Empleado) ois.readObject();
			
			System.out.println(a.toString());
			
			fis.close();
			ois.close();
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
	}
	
	/**
	 * Imprime los datos del empleado
	 */
	public void printEmployeeInfo() {
		System.out.println(_empTarget.toString());
	}

	

}
