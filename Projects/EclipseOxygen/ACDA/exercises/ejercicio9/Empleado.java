package ejercicio9;

import java.io.Serializable;
import java.util.Date;

public class Empleado implements Serializable{
	
	private int ID;
	private String nombre;
	private int edad;
	private double sueldo;
	
	public Empleado(int ID, String nombre, int edad, double sueldo) {
		this.ID=ID;
		this.nombre = nombre;
		this.edad = edad;
		this.sueldo = sueldo;
	}
	
	public int getID() {
		return ID;
	}
	public String getNombre() {
		return nombre;
	}
	public int getEdad() {
		return edad;
	}
	public double getSueldo() {
		return sueldo;
	}

	@Override
	public String toString() {
		return "Empleado [ID=" + ID + ", nombre=" + nombre + ", edad=" + edad + ", sueldo=" + sueldo + "]";
	}
	
	

}
