package ejercicio9;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class EmpleadoWriter {
	
	private File _fDest;
	
	public EmpleadoWriter(String fileDest) {
		_fDest = new File(fileDest);
	}
	
	public void writeEmployeeBinary(Empleado emp) {
		
		//Flujos de salida
		FileOutputStream fos = null;
		DataOutputStream dos = null;
		
		try {
			
			//Crea el fichero a escribir si no existe
			if(!_fDest.exists()) {
				
				_fDest.createNewFile();
				
			}
			
			//Inicializa de los flujos
			fos = new FileOutputStream(_fDest,true);
			dos = new DataOutputStream(fos);
			
			//Escribe el empleado
			dos.writeInt(emp.getID());
			dos.writeUTF(emp.getNombre());
			dos.writeInt(emp.getEdad());
			dos.writeDouble(emp.getSueldo());
			
			
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void writeEmployeeObject(Empleado emp) {
		
		FileOutputStream fos = null;
		ObjectOutputStream oos = null;
		
		try {
			fos = new FileOutputStream(_fDest);
			oos = new ObjectOutputStream(fos);
			
			oos.writeObject(emp);
			
			oos.close();
			fos.close();
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
