
package ejercicio7;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.Logger;

import ejercicio5.Departamento;

public class DepartamentModifierBinary {
	
	private Logger l = Logger.getLogger(this.getClass().getSimpleName());
	
	private File source, resultFile;
	
	// Departament to work with
	private Departamento dep;
	
	private boolean depExists= false;
	
	private int numDepartaments=0;

	public DepartamentModifierBinary(String pathSource) {
		
		//Create the file objects
		source = new File(pathSource);
		resultFile = new File(source.getParent(),source.getName()+".temp");
		
		try {
			resultFile.createNewFile();
			
			l.info("The new file with updated info has been created");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	
		l.info("Modifier instantiated for file: "+pathSource);
		
	}
	
	/**
	 * This method reads from the specified path the Departaments written in binary and modifies the specified
	 * departament by <b>ID</b>
	 * @param pathSource
	 */
	public void deleteDepartamentById(int id) throws IOException {
		
		l.info("Request to modify the departament with id "+id);
		
		//Input & Output
		FileInputStream fis = null;
		FileOutputStream fos = null;
		
		//Output binary access
		DataInputStream dis = null;
		DataOutputStream dos = null;
		
		try {

			fis = new FileInputStream(source);
			dis = new DataInputStream(fis);


			fos = new FileOutputStream(resultFile);
			dos = new DataOutputStream(fos);
			
			int numDep;
			String nom, loc;
						
			while(dis.available()>0) {
				
				numDepartaments++;

				numDep = dis.readInt();
				nom = dis.readUTF();
				loc = dis.readUTF();
				
				// If they aren't the departaments we are looking for, save them in the new file
				if(numDep!=id) {
					dos.writeInt(numDep);
					dos.writeUTF(nom);
					dos.writeUTF(loc);
				
				}else {
					depExists = true;
					System.out.println("El departamento ha sido eliminado satisfactoriamente");
				}				
			}
			
			if(!depExists) {
				System.out.println("!El departamento no existe!");
			}
			
			//First of all, close the input & output streams, so the source file gets free
			fis.close();
			dis.close();
			
			fos.close();
			dos.close();
			
			//Replace the old file with the new file
			replaceFile();
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Will help to modify the fields of the Departamente from the console, using the console as the output. It will ask to the user
	 * which of the fields will be modified and to insert the new data
	 */
	private void modifyDepartament() {
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Informaci�n actual del departamento");
		System.out.println(
							"N�mero de departamento: "+dep.getNumDep()+"\n"
							+"Nombre: "+dep.getNombre()+"\n"
							+"Localidad: "+dep.getLocalidad()+"\n");
		
		System.out.println("�Qu� datos quiere modificar? ");
		System.out.println("1.Nombre \n2.Localidad");
		
		//Menu options processing
		switch (sc.nextInt()) {
		case 1:
				//Clear the scanner
				sc = new Scanner(System.in);
				
				System.out.println("Introduce el nuevo nombre: ");
				//Set the new name
				dep.setNombre(sc.nextLine());
				break;
		case 2:
				//Clear the scanner
				sc = new Scanner(System.in);
				
				System.out.println("Introduce la nueva localidad");
				//Set the new location
				dep.setLocalidad(sc.nextLine());
				break;
		default:
			break;
		}
	}
	
	private void replaceFile() {
		source.delete() ;
		resultFile.renameTo(source);
	}
	
	public int getNumDepartaments() {
		return numDepartaments;
	}

}
