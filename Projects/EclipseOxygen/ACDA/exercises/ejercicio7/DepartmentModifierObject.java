package ejercicio7;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Scanner;

import ejercicio5.Departamento;

public class DepartmentModifierObject {

	private File source, resultFile;
	
	// Departament to work with
	private Departamento dep;

	private boolean depExists= false;
	
	private int numDepartaments=0;
	
	public DepartmentModifierObject(String pathSource) {
		
		//Create the file objects
		source = new File(pathSource);
		resultFile = new File(source.getParent(),source.getName()+".temp");
		
		try {
			resultFile.createNewFile();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	
		
	}
	
	/**
	 * This method reads from the specified path the Departaments written in binary and modifies the specified
	 * departament by <b>ID</b>
	 * @param pathSource
	 * @throws ClassNotFoundException 
	 */
	public void deleteDepartamentById(int id) throws IOException, ClassNotFoundException {
		
		
		//Input & Output
		FileInputStream fis = null;
		ObjectInputStream ois= null;
		
		FileOutputStream fos = null;
		ObjectOutputStream oos= null;
		
		try {
			//Reader
			fis = new FileInputStream(source);
			ois= new ObjectInputStream(fis);

			//Writer
			fos = new FileOutputStream(resultFile);
			oos = new ObjectOutputStream(fos);

			
			int numDep;
			String nom, loc;
			
			//Read, modify and write
			while((dep = (Departamento) ois.readObject())!= null) {
				
				numDepartaments++;
				
				// If they aren't the departaments we are looking for, save them in the new file
				if(dep.getNumDep()!=id) {
					oos.writeObject(dep);
				
				}else {
					depExists = true;
					System.out.println("El departamento ha sido eliminado satisfactoriamente");
				}				
			}
			
			if(!depExists) {
				System.out.println("!El departamento no existe!");
			}
			
			//First of all, close the input & output streams, so the source file gets free
			fis.close();
			ois.close();
			
			fos.close();
			oos.close();
			
			
			//Replace the old file with the new file
			replaceFile();
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Will help to modify the fields of the Departamente from the console, using the console as the output. It will ask to the user
	 * which of the fields will be modified and to insert the new data
	 */
	private void modifyDepartament() {
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Informaci�n actual del departamento");
		System.out.println(
							"N�mero de departamento: "+dep.getNumDep()+"\n"
							+"Nombre: "+dep.getNombre()+"\n"
							+"Localidad: "+dep.getLocalidad()+"\n");
		
		System.out.println("�Qu� datos quiere modificar? ");
		System.out.println("1.Nombre \n2.Localidad");
		
		//Menu options processing
		switch (sc.nextInt()) {
		case 1:
				//Clear the scanner
				sc = new Scanner(System.in);
				
				System.out.println("Introduce el nuevo nombre: ");
				//Set the new name
				dep.setNombre(sc.nextLine());
				break;
		case 2:
				//Clear the scanner
				sc = new Scanner(System.in);
				
				System.out.println("Introduce la nueva localidad");
				//Set the new location
				dep.setLocalidad(sc.nextLine());
				break;
		default:
			break;
		}
	}
	
	private void replaceFile() {
		source.delete() ;
		resultFile.renameTo(source);
	}
	
	public int getNumDepartaments() {
		return numDepartaments;
	}

}