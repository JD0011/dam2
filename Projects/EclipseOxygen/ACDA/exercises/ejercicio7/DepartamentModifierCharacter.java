
package ejercicio7;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.Logger;

import ejercicio5.Departamento;

public class DepartamentModifierCharacter {
	
	private File source, resultFile;
	
	// Departament to work with
	private Departamento dep;

	private boolean depExists= false;
	
	private int numDepartaments=0;
	
	public DepartamentModifierCharacter(String pathSource) {
		
		//Create the file objects
		source = new File(pathSource);
		resultFile = new File(source.getParent(),source.getName()+".temp");
		
		try {
			resultFile.createNewFile();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}
	
	/**
	 * This method reads from the specified path the Departaments written in binary and modifies the specified
	 * departament by <b>ID</b>
	 * @param pathSource
	 */
	public void deleteDepartamentById(int id) throws IOException {
		
		
		//Input & Output
		FileReader reader = null;
		FileWriter writer = null;
		
		BufferedReader buffReader = null;
		BufferedWriter buffWriter = null;
		
		try {
			
			reader = new FileReader(source);
			buffReader = new BufferedReader(reader);

			writer = new FileWriter(resultFile);
			buffWriter = new BufferedWriter(writer);

			
			int numDep;
			String nom, loc;
			
			//Current line
			String line = null;
			//Read, modify and write
			while((line = buffReader.readLine())!= null) {
				
				numDepartaments++;
				
				//dRead data using '/' as separator
				numDep = Integer.parseInt(line.substring(0, line.indexOf('/')));
				nom = line.substring(line.indexOf('/')+1,line.lastIndexOf('/'));
				loc = line.substring(line.lastIndexOf('/')+1, line.length());
				
				// If they aren't the departaments we are looking for, save them in the new file
				if(numDep!=id) {
					buffWriter.write(line);
					buffWriter.newLine();
				
				}else {
					depExists = true;
					System.out.println("El departamento ha sido eliminado satisfactoriamente");
				}				
			}
			
			if(!depExists) {
				System.out.println("!El departamento no existe!");
			}
			
			//First of all, close the input & output streams, so the source file gets free
			buffReader.close();	
			reader.close();
		
			
			buffWriter.close();
			writer.close();
			
			
			//Replace the old file with the new file
			replaceFile();
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Will help to modify the fields of the Departamente from the console, using the console as the output. It will ask to the user
	 * which of the fields will be modified and to insert the new data
	 */
	private void modifyDepartament() {
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Informaci�n actual del departamento");
		System.out.println(
							"N�mero de departamento: "+dep.getNumDep()+"\n"
							+"Nombre: "+dep.getNombre()+"\n"
							+"Localidad: "+dep.getLocalidad()+"\n");
		
		System.out.println("�Qu� datos quiere modificar? ");
		System.out.println("1.Nombre \n2.Localidad");
		
		//Menu options processing
		switch (sc.nextInt()) {
		case 1:
				//Clear the scanner
				sc = new Scanner(System.in);
				
				System.out.println("Introduce el nuevo nombre: ");
				//Set the new name
				dep.setNombre(sc.nextLine());
				break;
		case 2:
				//Clear the scanner
				sc = new Scanner(System.in);
				
				System.out.println("Introduce la nueva localidad");
				//Set the new location
				dep.setLocalidad(sc.nextLine());
				break;
		default:
			break;
		}
	}
	
	private void replaceFile() {
		source.delete() ;
		resultFile.renameTo(source);
	}
	
	public int getNumDepartaments() {
		return numDepartaments;
	}

}