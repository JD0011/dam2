package ejercicio12;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;

import javax.xml.transform.TransformerException;

public class Main {
	
	public static void main(String[] args) {
		
		EmpleadoManager em = new EmpleadoManager();
		
		File dest = new File("./Ficheros/Departamentos.xml");
		
		ArrayList<Empleado> emps = new ArrayList<>();
		
		emps.add(new Empleado(1, "Juan", 21, 1400));
		emps.add(new Empleado(2, "Rosa", 24, 1350));
		emps.add(new Empleado(3, "Martin", 19, 1400));
		emps.add(new Empleado(4, "Ana", 15, 1450));
		emps.add(new Empleado(5, "Pepe", 21, 1250));
		
		em.createXMLFromObject(emps);
		
		try {
			em.dumpXML(dest);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

}
