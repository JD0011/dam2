package ejercicio12;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.SAXException;


public class EmpleadoManager {
	
	private Document docXML;
	private DocumentBuilder db;
	private DocumentBuilderFactory dbf;
	
	
	public EmpleadoManager() {
		dbf = DocumentBuilderFactory.newInstance();
		
		try {
			db = dbf.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void createXMLFromObject(ArrayList<Empleado> emps){
		
		Text content;
		
		docXML = db.newDocument();
		
		//Version
		docXML.setXmlVersion("1.0");
		
		//Elemento empleado
		Element empleados = docXML.createElement("Empleados");
		docXML.appendChild(empleados);
		
		for(Empleado emp : emps) {
			
			Element empleado = docXML.createElement("Empleado");
			
			empleados.appendChild(empleado);
			
			//ID de empleado	
			/**
			Element id = docXML.createElement("id");
			content = docXML.createTextNode(String.valueOf(emp.getID()));
			id.appendChild(content);
			**/
			empleado.setAttribute("id", String.valueOf(emp.getID()));
			
			//Nombre de empleado
			Element nombre = docXML.createElement("nombre");
			content = docXML.createTextNode(emp.getNombre());
			nombre.appendChild(content);
			
			empleado.appendChild(nombre);
			
			//Edad de empleado
			Element edad = docXML.createElement("edad");
			content = docXML.createTextNode(String.valueOf(emp.getEdad()));
			edad.appendChild(content);
			
			empleado.appendChild(edad);
			
			//Sueldo de empleado
			Element sueldo = docXML.createElement("sueldo");
			content = docXML.createTextNode(String.valueOf(emp.getSueldo()));
			sueldo.appendChild(content);
			
			empleado.appendChild(sueldo);
		}
		
		
		
		docXML.getDocumentElement().normalize();
		
	}
	
	public void dumpXML(File dest) throws FileNotFoundException, TransformerException {
		TransformerFactory tff =TransformerFactory.newInstance();
		Transformer tf;
		
		tf =tff.newTransformer();
		
		tf.setOutputProperty(OutputKeys.INDENT, "yes");
		tf.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
		
		Source src = new DOMSource(docXML);
		
		PrintWriter writer = new PrintWriter(dest);
		
		Result resultado = new StreamResult(writer);
		
		tf.transform(src, resultado);
		
	}
	
	public void readXML(File source) throws SAXException, IOException {
		
		//Parse de File a Document
		docXML = db.parse(source);
		
		//Lista de nodos "Empleado"
		NodeList emps = docXML.getElementsByTagName("Empleado");
		for(int i = 0;i<emps.getLength();i++) 
			
		{	//Empleado concreto
			Node empleado = emps.item(i);
			if(empleado.getNodeType() == Node.ELEMENT_NODE) {
				
				//Casting de nodo a elemento para poder acceder a sus elementos hijos
				Element element = (Element) empleado;
				System.out.printf("Nombre: %s\n",element.getElementsByTagName("nombre").item(0).getTextContent());
				System.out.printf("ID: %s\n",element.getAttribute("id"));
				System.out.printf("Edad: %s\n",element.getElementsByTagName("edad").item(0).getTextContent());
				System.out.printf("Sueldo: %s\n",element.getElementsByTagName("sueldo").item(0).getTextContent());
			}
		}

		
	}

}
