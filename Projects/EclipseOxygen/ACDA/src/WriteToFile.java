import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class WriteToFile {
	
	public static void main(String[] args) throws IOException {
		
		String cadena = "Este es el texto a escribir";
		String otraCadena ="Esta es otra cadena";
		
		File f1 = new File("./fichero.txt");
		
		FileWriter fw = null;
		BufferedWriter buffer = null;
		
		try {
			fw = new FileWriter(f1);
			buffer = new BufferedWriter(fw);
			buffer.write(cadena);
			buffer.newLine();
			
			buffer.write(otraCadena);
			buffer.newLine();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {

			fw.close();
			buffer.close();
		}
	}

}
