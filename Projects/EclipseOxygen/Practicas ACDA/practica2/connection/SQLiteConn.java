package connection;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Properties;
import java.util.logging.Logger;

/**
 * Permite realizar la conexi�n a la base de datos de SQLite. Esta conexi�n siempre crear�
 * la base de datos en cada ejecuci�n del programa para volcar la informaci�n relacionada
 * con el cliente conectado.
 * @since 21 nov. 2017
 * @author juand
 *
 */
public abstract class SQLiteConn {
	
	private static Logger l = Logger.getLogger(SQLiteConn.class.getSimpleName());
	private static String fichero = ".\\Denuncias.db";
	private static Connection conn;
	private static PreparedStatement pstmt;
	
	//CONSULTAS
	
	private final static String creaMultaDetalle =
					"CREATE TABLE 'multa_detalle'(" +
						"dni VARCHAR(9) NOT NULL," +
						"matricula VARCHAR(7) NOT NULL," +
						"denuncia TINYINT NOT NULL," +
						"nombre VARCHAR(40)," +
						"fecha_denuncia VARCHAR(20)," +
						"importe TINYINT," +
						"PRIMARY KEY(dni,matricula,denuncia));"; 
	
	private final static String borraTablaMulta = 
						"DROP TABLE 'multa_detalle';";
	
	private final static String grabaMulta=
					"INSERT INTO 'multa_detalle' "
							+ "VALUES	(?,?,?,?,?,?);";
	
	private final static String borraMulta=
					"DELETE FROM 'multa_detalle' WHERE denuncia=?";
	
	private final static String existeMulta=
					"SELECT COUNT(*) FROM 'multa_detalle' WHERE denuncia=?";
	
	private final static String consultaInfoMulta =	"SELECT * FROM 'multa_detalle';";
	
	private static ArrayList<Integer> multasEliminadas;

	static {
		multasEliminadas = new ArrayList<Integer>();
	}
	/**
	 * Abre la conexi�n con la base de datos
	 */
	private static void openConnection() {
		try {
			Class.forName("org.sqlite.JDBC");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			conn = DriverManager.getConnection("jdbc:sqlite:"+fichero);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Cierra la conexi�n con la base de datos
	 */
	private static void closeConnection() {
		try {
			
			if(!conn.isClosed()) {
				conn.close();
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Crea la tabla temporal en la que se volcar�n los datos al realizar la conexi�n con la
	 * base de datos. Utiliza una sentencia SQL predefinida con una estructura fija.
	 * @return Confirmaci�n de creaci�n de tabla
	 * @throws SQLException 
	 */
	public static void createTemporalTable() throws SQLException
	{
		openConnection();
		
		pstmt = conn.prepareStatement(creaMultaDetalle);
		
		pstmt.executeUpdate();
		pstmt.close();
		l.info("La tabla temporal detalle_multa ha sido creada");
		closeConnection();
	}
	
	/**
	 * Hace un DROP para eliminar la tabla temporal
	 * @throws SQLException 
	 */
	public static void deleteTemporalTable() {
		
		openConnection();
		//Eliminaci�n de tabla
		try {
			pstmt = conn.prepareStatement(borraTablaMulta);
			
			pstmt.execute();
			pstmt.close();
			
			l.info("La tabla temporal detalle_multa ha sido eliminada");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		closeConnection();
	}
	
	public static void deleteDatabase() {

		closeConnection();
		
		//Eliminar fichero
		if(new File(fichero).delete()) {
			l.info("La base de datos de SQLite ha sido eliminada satisfactoriamente");
		}else {
			l.info("No se puede eliminar la base de datos SQlite");
		}
	}
	
	/**
	 * Comprueba si el usuario tiene multas pendientes de resolver.
	 * @param id Identificador del usuario
	 * @return Si el cliente tiene multas pendientes
	 */
	public static boolean existeMulta(int id) {
		ResultSet data;
		boolean existe;
		
		openConnection();
		
		existe = false;
		try {
			pstmt = conn.prepareStatement(existeMulta);
			
			pstmt.setInt(1, id);
			
			pstmt.execute();
			
			data = pstmt.getResultSet();
			
			if(data.next()) {
				existe = data.getInt(1) >0;
			}
			
			pstmt.close();
			l.info("Consulta ejecutada: existeMulta");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		closeConnection();
		
		return existe;
	}
	
	/**
	 * Elimina la denuncia a partir de su propio identificador
	 * @param id Identificador de la denuncia
	 */
	public static void deleteMultaByID(int id) {
		
		openConnection();
		
		try {
			pstmt = conn.prepareStatement(borraMulta);
			pstmt.setInt(1, id);
			
			pstmt.execute();
			pstmt.close();
			
			for(int a: multasEliminadas) {
				System.out.println(a);
			}
			multasEliminadas.add(id);
			
			l.info("Consulta ejecutada: borraMulta");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		closeConnection();
	}
	
	/**
	 * Volcar� la informaci�n recibida desde mysql en su propia base de datos.
	 * @param multas
	 */
	public static void dumpInfoMultas(ArrayList<Properties> multas) {
		
		openConnection();
		
		try {
			//Itera entre todas las multas
			for(Properties p: multas) {
				pstmt = conn.prepareStatement(grabaMulta);
				
				//Accede a sus propiedades
				pstmt.setString(1, p.getProperty("DNI"));
				pstmt.setString(2, p.getProperty("matricula"));
				pstmt.setInt(3, (int)p.get("denuncia"));
				pstmt.setString(4, p.getProperty("nombre"));
				pstmt.setString(5, p.getProperty("fecha_denuncia"));
				pstmt.setDouble(6, (Double)p.get("importe"));
				
				//Graba en base de datos
				pstmt.execute();
				
				pstmt.close();
				l.info("SQLite ha a�adido nueva informaci�n");
			}			
			
			System.out.println("�Informaci�n sobre multas descargada!");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		closeConnection();
		
	}
	
	/**
	 * Recorre las columnas de la tabla y las almacena en un objecto properties, utilizando como
	 * clave el propio nombre de la columna, a la cual se le encadena el valor. Todos los valores
	 * que se obtengan se almacenaran en la variable {@link #infoMultas}
	 * @return Informaci�n de la multa
	 */
	public static ArrayList<Properties> readInfoMultas() {
		ArrayList<Properties> infoMultas;
		Properties detalleMulta;
		ResultSetMetaData mdata;
		ResultSet data;
		
		openConnection();
		
		infoMultas = new ArrayList<Properties>();
		try {
			
			pstmt= conn.prepareStatement(consultaInfoMulta);
			pstmt.execute();
			
			data = pstmt.getResultSet();
			mdata = data.getMetaData();
			
			while(data.next()) {
				detalleMulta = new Properties();
				//Itera entre las columnas
				for(int i=1;i<=mdata.getColumnCount();i++) {
					detalleMulta.put(mdata.getColumnLabel(i), data.getObject(i));
				}
				
				infoMultas.add(detalleMulta);
			}
			
			pstmt.close();
			
			l.info("Consulta ejecutada: consultaInfoMulta");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		closeConnection();
		
		return infoMultas;
	}
	
	/**
	 * Devuelve las multas eliminadas. Tambi�n limpia la lista que contiene las multas
	 * eliminadas para evitar errores en las proximas conexiones relacionados con la 
	 * eliminaci�n de las multa anteriores (ya eliminadas)
	 * @return {@link #multasEliminadas}
	 */
	public static ArrayList<Integer> deletedMultas() {
		ArrayList<Integer> aux;
		
		aux = new ArrayList<>();
		aux.addAll(multasEliminadas);
		
		multasEliminadas.clear();
		l.info("La lista de multas eliminadas ha sido vaciada");
		return aux;
	}
}
