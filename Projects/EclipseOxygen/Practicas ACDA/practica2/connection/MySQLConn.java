package connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Properties;
import java.util.logging.Logger;

/**
 * Realiza la conexi�n con la base de datos y permite realizar ciertas funcionalidades
 * definidas por los m�todos
 * @author juand
 *
 */
public abstract class MySQLConn {

	private static Connection conn;
	private static PreparedStatement pstmt;

	//CONSULTAS
	
	private static final String existeDNI=
						"SELECT COUNT(*) FROM usuarios WHERE DNI=?;";
	
	private static final String compruebaDenuncia =
						"SELECT COUNT(*) FROM multas WHERE vehiculo=(" + 
								"SELECT matricula FROM coches WHERE Propietario=?);";
	
	private static final String consultaMulta =
						"SELECT u.DNI, c.matricula, m.denuncia, u.nombre, m.fecha_denuncia, m.importe " + 
								"FROM multas m " + 
								"INNER JOIN coches c ON m.vehiculo=c.matricula " +
								"INNER JOIN usuarios u ON u.dni=c.propietario " + 
								"WHERE u.dni=?;";
	
	private static final String borraMulta=
			"DELETE FROM multas WHERE multas.denuncia=?;";
	
	/**
	 * Se encarga de cargar el driver
	 */
	public static void openConnection() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/denuncias", "root","");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Cierra la conexi�n
	 */
	public static void closeConnection() {
		try {			
			if(!conn.isClosed()) {
				conn.close();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Comprueba que existe alg�n registro en la tabla de usuarios con respecto al DNI
	 * introducido por par�metros
	 * @param DNI
	 * @return
	 */
	public static boolean existeDNI(String DNI) {
		boolean existe;
		
		existe = false;
		
		try {
			pstmt = conn.prepareStatement(existeDNI);
			
			pstmt.setString(1, DNI);
			
			pstmt.execute();
			
			if(pstmt.getResultSet().next()) {
				existe = pstmt.getResultSet().getInt(1) >0;
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return existe;
	}
	
	/**
	 * Comprueba si existe alg�n registro de denuncia con el DNI especificado
	 * @param DNI
	 * @return
	 */
	public static boolean tieneDenuncia(String DNI) {
		boolean tieneDenuncia;
		
		tieneDenuncia = false;
		
		try {
			pstmt = conn.prepareStatement(compruebaDenuncia);
			
			pstmt.setString(1, DNI);
			
			pstmt.execute();
			
			if(pstmt.getResultSet().next()) {
				tieneDenuncia = pstmt.getResultSet().getInt(1) > 0;
			}
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return tieneDenuncia;
	}
	
	/**
	 * Lee los registros en la base de datos de MySQL y los devuelve en forma de arraylist de propiedades
	 * @param DNI
	 * @return
	 */
	public static ArrayList<Properties> readInfoMultaOf(String DNI) {
		ArrayList<Properties> infoMultas;
		Properties detalleMulta;
		ResultSet data;
		ResultSetMetaData mdata;
		
		infoMultas = new ArrayList<Properties>();
		
		try {
			
			pstmt = conn.prepareStatement(consultaMulta);
			
			pstmt.setString(1, DNI);
			pstmt.execute();

			data = pstmt.getResultSet();
			mdata = data.getMetaData();
			
			//Almacenamiento de valores en infoMultas
			while(data.next()) {
				detalleMulta = new Properties();
				//Itera entre las columnas
				for(int i=1;i<=mdata.getColumnCount();i++) {
					//Guardalas con el mismo nombre que en la tabla
					detalleMulta.put(mdata.getColumnLabel(i), data.getObject(i));
				}
				
				infoMultas.add(detalleMulta);
			}
			
			pstmt.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return infoMultas;
	}
	
	/**
	 * Actualiza las multas en la base de datos a partir del identificador de las multas eliminadas
	 * @param multas
	 */
	public static void updateMultas(ArrayList<Integer> multas) {
		
		if(multas.size()==0) {
			System.out.print("No se han realizado modificaciones.");
		}else {
			try {
				for(Integer id: multas) {
					pstmt = conn.prepareStatement(borraMulta);
					
					pstmt.setInt(1, id);
					pstmt.executeUpdate();
					
					pstmt.close();
				}
				
				System.out.print("Informaci�n en MySQL actualizada.");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		System.out.println(" Vuelva cuando quiera.");
	}
}
