package view;

import java.sql.SQLException;
import java.util.Scanner;

import connection.MySQLConn;
import connection.SQLiteConn;

/**
 * Vista principal de la aplicaci�n
 * @author juand
 *
 */
public abstract class MainView {
	
	/**
	 * Mostrar� el men� principal de la aplicaci�n
	 */
	public static void show() {
		int respuesta;
		
		boolean exitCondition;
		Scanner sc;
		
		System.out.println("Bienvenido al programa de Multas");
		
		//Procesamiento de men�
		exitCondition = false;
		while(!exitCondition) {
			
			System.out.println(
					"Elija una opci�n\n"
					+ "1. Iniciar sesi�n\n"
					+ "2. Salir");
			
			sc = new Scanner(System.in);
			
			try {
				respuesta = sc.nextInt();
			}catch(Exception e) {
				System.out.println("Formato de respuesta incorrecto");
				respuesta = 0;
			}
			
			
			switch (respuesta) {
			case 1:
				String dni;
				sc= new Scanner(System.in);
				
				System.out.println("Introduzca su DNI");
				
				dni = sc.nextLine();

				MySQLConn.openConnection();
				
				//Comprueba si existe el DNI. Si no existe, el usuario volver� al men�
				if(!MySQLConn.existeDNI(dni)) {
					System.out.println("Usuario no encontrado");
				}else {
					System.out.println("Bienvenido a la aplicaci�n");
					
					//Se asegura de machacar la base de datos SQLite si ya existe
					try {
						SQLiteConn.createTemporalTable();
					} catch (SQLException e) {
						//Si la tabla ya existe, borrala
						SQLiteConn.deleteTemporalTable();
						try {
							//Y creala de nuevo
							SQLiteConn.createTemporalTable();
						} catch (SQLException e1) {
							e1.printStackTrace();
						}
					}
					
					//Volcado de informaci�n en SQLite
					SQLiteConn.dumpInfoMultas(MySQLConn.readInfoMultaOf(dni));
					
					MySQLConn.closeConnection();
					
					//Vista para manipular los datos
					MultasView.show();
					
					
					//Actualizaci�n en MySQL de las multas borradas
					MySQLConn.openConnection();
					MySQLConn.updateMultas(SQLiteConn.deletedMultas());
					MySQLConn.closeConnection();
					
					//Eliminaci�n de tabla y base de datos
					SQLiteConn.deleteTemporalTable();
					SQLiteConn.deleteDatabase();
					
				}
				break;
				
			case 2:
				exitCondition = true;
				break;

			default:
				break;
			}
			
		}
	}

}
