package view;

import java.util.ArrayList;
import java.util.Properties;
import java.util.Scanner;

import connection.SQLiteConn;

/**
 * Vista que gestiona las multas
 * @author juand
 *
 */
public abstract class MultasView {
	
	public static void show(){
		ArrayList<Properties> infoMultas;
		Scanner sc;
		boolean exitCondition;
		String respuesta;
		exitCondition = false;
		
		while(!exitCondition) {
			
			//Lectura de multas
			infoMultas = SQLiteConn.readInfoMultas();
			
			if(infoMultas.size()==0) {
				System.out.println("No tiene multas pendientes.");
				
				System.out.println("Pulse enter para continuar...");
				
				sc = new Scanner(System.in);
				
				sc.nextLine();
				exitCondition = true;
				
			}else {
				for(Properties p: infoMultas) {
					System.out.println(
									"DNI: "+p.getProperty("dni") +
									" MATR�CULA: "+p.getProperty("matricula") +
									" DENUNCIA: "+(int)p.get("denuncia") +
									" NOMBRE: "+p.getProperty("nombre") +
									" FECHA_DENUNCIA: "+p.getProperty("fecha_denuncia") +
									" IMPORTE: "+ p.get("importe")+" �"
									);
				}
				
				System.out.println("�Desea anular una denuncia?[S/N]");
				
				sc = new Scanner(System.in);
				
				respuesta = sc.next();
				
				//Procesamiento de respuesta
				switch (respuesta.toUpperCase()) {
					case "S":
						int id;
						sc = new Scanner(System.in);
						
						System.out.print("Introduce ID de la denuncia -> ");
						id = sc.nextInt();
						
						if(SQLiteConn.existeMulta(id)) {
							SQLiteConn.deleteMultaByID(id);
							System.out.println("-- Multa eliminada --");
						}else {
							System.out.println("-- La multa no existe o ya ha sido eliminada --");
						}
						
						break;
					case "N":
						exitCondition = true;
						break;
		
					default:
						break;
				}
			}
			
		}
	}
}
