package xml;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Logger;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import dataModels.Registro;

/**
 * Will generate a XML file customized for registries (object Registro). It allows to set the root element, specify the target
 * file and add many registries as wanted.
 * 
 * @author Juan de Dios Delgado Bermm�dez
 * @since 17/10/2017
 *
 */
public class XMLRegistryGenerator {
	
	//VARS
	
	private Logger _l = Logger.getGlobal();
	//File to save the XML
	private File _flXMLTarget;
	//XML Document and root element
	private Document doc;
	private Element _elRoot;
	//Registry element
	private Element _elReg;
	
	//CONSTRUCTOR
	
	public XMLRegistryGenerator (File flXMLTarget) {
		_flXMLTarget = flXMLTarget;
		
		Initialize();
	}
	
	//METHODS
	
	/**
	 * It will setup the initial configuration of the builders object for creating a Document. Also will set the XML version
	 */
	private void Initialize() {
		DocumentBuilderFactory dbf =DocumentBuilderFactory.newInstance();
		
		DocumentBuilder db;
		try {
			
			db = dbf.newDocumentBuilder();
			doc = db.newDocument();

			
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		doc.setXmlVersion("1.0");
		_l.info("Documento XML creado con encoding version 1.0");
	}
	
	//METHODS
	
	/**
	 * Will generate the root element through the given name
	 * @param rootName Root name
	 */
	public void generateRoot(String rootName) {
		
		//If there is already a defined root element, dont asign it
		if(_elRoot == null) {
			_elRoot = doc.createElement(rootName);
			doc.appendChild(_elRoot);
			
			_l.info("Elemento ra�z '"+rootName+"' creado para el documento XML");
			
		}else {
			_l.info("Ya se ha definido un elemento raiz para el documento XML!");
		}
	}
	
	/**
	 * Adds the registry to the root element taking his information and saving it into different elements. Then they are append
	 * to the registry element, and then to the root element.
	 * @param reg Registry to record
	 */
	public void addRegistry(Registro reg) {
		_elReg = doc.createElement("registro");
		
		//Create the elements to save the info
		Element elFecha,elConcepto, elCantidad, elTipo;
		
		//Set the values to the elements
		
		elFecha = doc.createElement("fecha");
		elFecha.setTextContent(reg.getFechaString());
		
		elConcepto = doc.createElement("concepto");
		elConcepto.setTextContent(reg.getConcepto());
		
		elCantidad = doc.createElement("cantidad");
		elCantidad.setTextContent(String.valueOf(reg.getCantidad()));
		
		elTipo = doc.createElement("tipo");
		elTipo.setTextContent(String.valueOf(reg.getTipo()));
		
		//Append them to the registry element
		_elReg.appendChild(elFecha);
		_elReg.appendChild(elConcepto);
		_elReg.appendChild(elCantidad);
		_elReg.appendChild(elTipo);
		
		//Append registry to root
		_elRoot.appendChild(_elReg);
	}
	
	/**
	 * It will dump the Document to the specified file in the constructor creating the neededs objects for this process
	 */
	public void dumpToXML() {
		
		//Transformer objects
		TransformerFactory tfactory = TransformerFactory.newInstance();
		Transformer transformer = null;
		
		//Output to the file
		FileWriter writer = null;
		
		try {
			transformer = tfactory.newTransformer();
			writer = new FileWriter(_flXMLTarget);
		} catch (TransformerConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//Stream to the file
		PrintWriter pw = new PrintWriter(writer);
		Result result = new StreamResult(pw);
		
		//Source of the XML
		Source src = new DOMSource(doc);
		
		//Enable the indent
		transformer.setOutputProperty(OutputKeys.INDENT,"yes");
		
		//DUMP!
		try {
			transformer.transform(src, result);
			_l.info("Documento XML creado satisfactoriamente en: "+_flXMLTarget.getAbsolutePath());
		} catch (TransformerException e) {
			e.printStackTrace();
		}
		
		//Close streams
		try {
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		pw.close();
		
		
	}
	

}
