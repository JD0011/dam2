import java.io.File;
import java.util.Scanner;

import dataModels.Registro;
import file.FileSearcher;
import registryIO.BinaryRecorder;
import registryIO.RegistryManager;
import xml.XMLRegistryGenerator;

public class Main {

	public static void main(String[] args) {
		
		//VARS
		
		RegistryManager rm;
		//Binary recorder (one for each binary file)
		BinaryRecorder ingresosRecorder,gastosRecorder;
		//XML Generator
		XMLRegistryGenerator xmlrg;
		//Searchs for the given filename
		FileSearcher fs = null;
		
		
		//Ask the user for a disk unit
		Scanner sc = new Scanner(System.in);
		String strAnswer;
		
		do {
			System.out.println("Indique la unidad en la que se almacena 'contabilidad.txt'[C/D]");
			strAnswer = sc.nextLine();
		}while(!strAnswer.equalsIgnoreCase("C") && !strAnswer.equalsIgnoreCase("D"));
		
		fs = new FileSearcher("contabilidad.txt", strAnswer.toUpperCase());
		
		//Search recursively for the file
		fs.recursiveSearch();
				
		
		//Initialize binary recorders
		ingresosRecorder = new BinaryRecorder(new File(fs.getTarget().getParent()+File.separator+"Ingresos.dat"));
		gastosRecorder = new BinaryRecorder(new File(fs.getTarget().getParent()+File.separator+"Gastos.dat"));
		
		//Initialize XML generator
		xmlrg = new XMLRegistryGenerator(new File(fs.getTarget().getParent()+File.separator+"Contabilidad.xml"));
		
		//Set the xml root element
		xmlrg.generateRoot("Contabilidad");
		
		//Initialize the Registry Manager
		rm = new RegistryManager(fs.getTarget());
		
		//Read the records
		Registro reg = null;
		while((reg = rm.readRegistry())!=null) {
			
			if(reg.getConcepto().equals("Almuerzo")) {
				reg.setConcepto("Dieta");
			}
			
			//Save it into the temporal file
			rm.recordRegistry(reg);
			
			//Also..
			
			//Record the registry depending on his type in a binary file
			if(reg.getTipo()==Registro.TypeRegistry.DEPOSIT.getValue()) {
				ingresosRecorder.recordRegistry(reg);
			}else if(reg.getTipo()==Registro.TypeRegistry.SPENDING.getValue()) {
				gastosRecorder.recordRegistry(reg);
			}
			
			//Add the registry to the XML
			xmlrg.addRegistry(reg);
			
		}
		
		//Replace the original file with the temporal
		rm.replaceFile();
		
		//Dump the XML into the file
		xmlrg.dumpToXML();

	}

}
