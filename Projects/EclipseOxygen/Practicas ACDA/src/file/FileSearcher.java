package file;

import java.io.File;
import java.util.Collection;
import java.util.logging.Logger;

/**
 * Se encarga de buscar un fichero determinado en la unidad especificada, pudiendo decantarse por 
 * "C:/" o "D:/" y lo almacena en una variable de clase.
 * 
 * @author Juan de Dios Delgado Berm�dez
 * @since 17 oct. 2017
 *
 */
public class FileSearcher {
	
	//VARS
	
	private Logger _l = Logger.getGlobal();
	//Nombre del fichero buscado
	private String _strFileName;
	//Letra de la unidad de disco
	private String _strDiskUnit;
	//Fichero desde el que partimos al realizar la b�squeda
	private File _flStartPoint;
	//Fichero de salida (creado con el nombre de contabilidad.txt)
	private File _flTarget;
	boolean exitCondition = false;
	
	//CONSTRUCTOR
	
	public FileSearcher(String strFileName, String strDiskUnit) {
		_strFileName = strFileName;
		_strDiskUnit = strDiskUnit+":/";
		
	}
	
	//GETTERS
	
	public File getTarget() {
		return _flTarget;
	}
	
	//METHODS
	
	/**
	 * Determines the chosen disk unit and realizes the search calling findFile(File _FLStartPoint) method
	 */
	public void recursiveSearch() {
		
		//Creates the file with the disk unit letter
		_flStartPoint = new File(_strDiskUnit);
		
		_l.info("Buscando fichero con nombre : "+_strFileName);
		findFile(_flStartPoint);
		
		//If it doesn't exist (atleast using this recursive method) print a notification.
		if(_flTarget==null) {
			_l.info("No se ha podido encontrar "+_strFileName +" en la unidad "+_strDiskUnit+"!");
		}
	}
	
	
	
	/**
	 * Realizes a recursive search to find the specified filename stored in _strFileName from the choosen starting point.
	 * 
	 * Note: that exitCondition is needed because if an exception is generated while accesing to the files, it will never reach
	 * the break sentence
	 * @param startPoint
	 */
	private void findFile(File startPoint) {
		
		File aux;
		try {
			loop:
			for(int i = 0;i<startPoint.listFiles().length && !exitCondition;i++) {
					aux = startPoint.listFiles()[i];
					
						if(aux.isFile()) {
							if(aux.getName().equals(_strFileName)) {
								_flTarget = aux;
								_l.info("Fichero encontrado en: "+aux.getAbsolutePath());
								exitCondition = true;
								break loop;
							}
						}else{
							findFile(aux);
						}
			}	
		//Excepci�n al entrar en una carpeta sin permisos
		}catch(NullPointerException e) {
			
		}
			
	}

}
