package dataModels;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Represents each of the records that the program will be managing and helps to centralice the information relative to the same
 * object
 * @author Juan de Dios Delgado Berm�dez
 * @since 17/10/2017
 *
 */
public class Registro {
	
	//VARS
	
	private Date _dtFecha;
	private DateFormat _dfFormat;
	private String _strConcepto;
	private double _dbCantidad;
	private char _cTipo;
	
	//Type of existent registries
	public enum TypeRegistry{
		
		DEPOSIT('I'),SPENDING('G');
		
		private char _cValue;
		
		private TypeRegistry(char cValue) {
			_cValue = cValue;
		}
		
		public char getValue() {
			return _cValue;
		}
	}
	
	//CONSTRUCTOR
	
	public Registro(String strFecha, String strConcepto, double dbCantidad, char cTipo) {		
				
		_strConcepto = strConcepto;
		_dbCantidad = dbCantidad;
		_cTipo = cTipo;
		
		_dfFormat = new SimpleDateFormat("dd/mm/yyyy");
		
		try {
			_dtFecha = _dfFormat.parse(strFecha);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	//SETTERS
	
	public void setFecha(Date dtFecha) {
		_dtFecha = dtFecha;
	}

	public void setConcepto(String strConcepto) {
		_strConcepto = strConcepto;
	}

	public void setCantidad(double dbCantidad) {
		_dbCantidad = dbCantidad;
	}

	public void setTipo(char cTipo) {
		_cTipo = cTipo;
	}
	
	//GETTERS

	public Date getFecha() {
		return _dtFecha;
	}
	
	public String getFechaString() {
		return _dfFormat.format(_dtFecha);
	}

	public String getConcepto() {
		return _strConcepto;
	}

	public double getCantidad() {
		return _dbCantidad;
	}

	public char getTipo() {
		return _cTipo;
	}
	
	public String toString() {
		return _dfFormat.format(_dtFecha)+" "+_strConcepto +" "+_dbCantidad+" "+_cTipo;
	}
	
}
