package registryIO;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.logging.Logger;

import dataModels.Registro;

/**
 * Allows to manage files with data related to Registry object in a sequential order, giving the needed methods to read 
 * these objects.
 * 
 * @author Juan de Dios Delgado Berm�dez
 * @since 17/10/2017
 */
public class RegistryManager {
	
	//VARS
	
	private Logger _l = Logger.getGlobal();
	private File _flTarget;
	private File _flTemporal;	
	//Streams
	private FileReader _frReader;
	private LineNumberReader _lmrLineReader;
	private FileWriter _fwWriter;
	private BufferedWriter _bwWriter;
	
	public RegistryManager(File flTarget) {
		_flTarget = flTarget;
		
		_l.info("Administrador de registros creado para el fichero "+flTarget.getName());
		
	}
	
	/**
	 * Lee del archivo los registros con el concepto especificado y los modifica
	 * @return Registro que se ha leido por �ltima vez
	 */
	public Registro readRegistry() {
		
		//Genera archivo temporal
		if(_flTemporal==null) {
			generateTemporalFile();
		}
		
		//Almacenamiento de datos
		
		Registro reg = null;
		
		String dtFecha;
		String strConcepto;
		double dbCantidad;
		char cTipo;
		
		try {
			//Inicializa flujos solo si son nulos
			if(_frReader==null && _lmrLineReader ==null) {
				_frReader = new FileReader(_flTarget);
				_lmrLineReader = new LineNumberReader(_frReader);
			}
			
			boolean exitCondition = false;
			String line = null;
			
			while(!exitCondition && (line = _lmrLineReader.readLine())!=null) {
				
				//Desde 0 hasta el primer espacio
				dtFecha = line.substring(0, line.indexOf((char)32)); //(char)32 = Espacio
				
				//Desde el primer espacio hasta el segundo
				strConcepto = line.substring(line.indexOf((char)32)+1,line.indexOf((char)32, line.indexOf((char)32)+1));
				
				//Desde el segundo espacio hasta el tercero
				dbCantidad = Double.parseDouble(
						line.substring(
								line.indexOf((char)32, line.indexOf((char)32)+1),
								line.lastIndexOf((char)32)));

				//Desde el tercero hasta el final
				cTipo = line.substring(line.length()-1, line.length()).charAt(0);
				
				//Almacena el registro
				reg = new Registro(dtFecha, strConcepto, dbCantidad, cTipo);
							
				//Sal
				exitCondition = true;
			}
			
			//Cierra los flujos si ya se ha terminado de trabajar
			if(line==null) {
				_frReader.close();
				_lmrLineReader.close();
			}
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return reg;
	}
	
	/**
	 * Se encarga de generar el archivo temporal necesario para la lectura y edici�n secuencial
	 */
	private void generateTemporalFile() {
		_flTemporal = new File(_flTarget.getAbsolutePath()+".temp");		
		
		try {
			_flTemporal.createNewFile();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Graba el registro especificado en el archivo temporal
	 * @param reg Registro a grabar
	 */
	public void recordRegistry(Registro reg) {
		
		try {
			_fwWriter = new FileWriter(_flTemporal,true); //true -> No sobreescribas
			_bwWriter = new BufferedWriter(_fwWriter);
			
			_bwWriter.write(reg.toString());
			_bwWriter.newLine();
			
			_bwWriter.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			try {
				_fwWriter.close();
				_bwWriter.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
	
	/**
	 * Reemplaza el fichero original por el temporal
	 */
	public void replaceFile() {
		
		_l.info(_flTarget.delete()? "Fichero original eliminado" : "No se ha podido eliminar el fichero original!");
		_l.info(_flTemporal.renameTo(_flTarget)? "Fichero temporal renombrado a original" : "No se ha podido renombrar el fichero temporal!");		
	}
}
