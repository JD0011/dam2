package registryIO;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.logging.Logger;

import dataModels.Registro;

/**
 * Simple implementation of a binary recorder, in this case, specific for record Registry objects. It uses binary streams like
 *  FileOutputStream and DataOutputStream for handling primitive data types.
 * 
 * @author Juan de Dios Delgado Berm�dez
 * @since 17/10/2017
 *
 */
public class BinaryRecorder {
	
	//VARS
	
	private Logger _l = Logger.getGlobal();
	//Target where to save the registries
	private File _flTarget;
	
	//CONSTRUCTOR
	
	public BinaryRecorder(File flTarget) {
		
		_flTarget = flTarget;		
		
		_l.info("Grabadora binaria creada para: "+flTarget.getName());
	}
	
	//METHODS
	
	/**
	 * This method reads from the specified path the Departaments written in binary and modifies the specified
	 * departament by <b>ID</b>
	 * @param pathSource
	 */
	public void recordRegistry(Registro reg){
		
		//Output binary
		FileOutputStream fos = null;
		DataOutputStream dos = null;
		
		try {
			//Create the target file if its the first time recording into it
			if(!_flTarget.exists()) {
				_flTarget.createNewFile();
			}
			
			fos = new FileOutputStream(_flTarget,true); //True -> Don't overwrite
			dos = new DataOutputStream(fos);
			
			//Write the information, using ' ' as separator
			dos.writeUTF(reg.getFechaString());
			dos.writeChar(' ');
			dos.writeUTF(reg.getConcepto());
			dos.writeChar(' ');
			dos.writeDouble(reg.getCantidad());
			dos.writeChar(' ');
			dos.writeChar(reg.getTipo());
			
		}catch(FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		//Close the output streams, so the target file gets free
		
		try {
			fos.close();
			dos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
