﻿/*
* PRÁCTICA.............: Práctica Inicial.
* NOMBRE y APELLIDOS...: Juan de Dios Delgado Bermúdez
* CURSO y GRUPO........: 2o Desarrollo de Interfaces
* TÍTULO de la PRÁCTICA: Uso del IDE V.Studio
* FECHA de ENTREGA.....: 16 de Octubre de 2017
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PracticaInicial.Funcion;
using System.Drawing;

namespace PracticaInicial
{
    class Principal
    {
        static void Main(string[] args)
        {
            bool exitCondition;
            int intRespuesta, counter;

            Console.WriteLine("!Bienvenido a la primera práctica de desarrollo de interfaces¡");

            counter = 1;
            while (true)
            {

                AntiFlood(counter);

                //Opciones
                Console.WriteLine("Seleccione una opción:");
                Console.WriteLine("1. Resolver Ecuación de 2º Grado");
                Console.WriteLine("2. Mostrar tabla de multiplicar");
                Console.WriteLine("3. Salir");
                
                try
                {
                    intRespuesta = int.Parse(Console.ReadLine());
                    Console.Clear();
                }
                catch (OverflowException)
                {
                    PrintError("Debe introducir un número entre 2,147,483,647 y -2,147,483,647");
                    counter++;
                    continue;
                }
                catch (FormatException)
                {
                    PrintError("Debe introducir un número entero");
                    counter++;
                    continue;
                }

                //Vistas
                switch (intRespuesta)
                {
                    //Salir
                    case 1:

                        double coefA, coefB, coefC;
                        Ecuacion eqResolver;

                        exitCondition = false;
                        
                        do
                        {
                            AntiFlood(counter);

                            Console.WriteLine("Resolvedor de ecuaciones de segundo grado");
                            Console.WriteLine("Introduce el valor de los coeficientes");

                            try
                            {
                                Console.Write("a ->");
                                coefA = double.Parse(Console.ReadLine());

                                Console.Write("b ->");
                                coefB = double.Parse(Console.ReadLine());

                                Console.Write("c ->");
                                coefC = double.Parse(Console.ReadLine());
                            }
                            catch (OverflowException)
                            {
                                PrintError("Debe introducir un número entre 2,147,483,647 y -2,147,483,647");
                                counter++;
                                continue;
                            }
                            catch (FormatException)
                            {
                                PrintError("Debe introducir un número entero o decimal");
                                counter++;
                                continue;
                            }

                            //Todo ok
                            exitCondition = true;

                            Console.WriteLine(); //Separador

                            eqResolver = new Ecuacion(coefA, coefB, coefC);
                            eqResolver.solve();

                            eqResolver.PrintEcuacionResult();
                            
                            Console.WriteLine("\n\nPresione una tecla para continuar ...");
                            Console.ReadKey();
                            Console.Clear();

                        } while (!exitCondition);

                        break;

                    //Tabla
                    case 2:

                        int numTable, numElements;
                        Tabla tableNum;

                        exitCondition = false;
                        do
                        {
                            AntiFlood(counter);

                            Console.WriteLine("Tabla de multiplicar");
                            Console.WriteLine("Introduce los siguientes parámetros: ");

                            try
                            {
                                Console.Write("Operando -> ");
                                numTable = int.Parse(Console.ReadLine());

                                Console.Write("Operaciones a mostrar -> ");
                                numElements = int.Parse(Console.ReadLine());
                                
                                if (numElements < 0)
                                {
                                    PrintError("Debe introducir un valor positivo");
                                    continue;
                                }
                                else
                                {
                                    //Todo ok
                                    exitCondition = true;
                                }
                            }
                            catch (OverflowException)
                            {
                                PrintError("Debe introducir un número entre 2,147,483,647 y -2,147,483,647");
                                counter++;
                                continue;
                            }
                            catch (FormatException)
                            {
                                PrintError("Debe introducir un número entero");
                                counter++;
                                continue;
                            }       

                            Console.WriteLine();

                            tableNum = new Tabla(numTable, numElements);
                            tableNum.PrintTable();

                        } while (!exitCondition);

                        break;

                    //Salir
                    case 3:
                        
                        //Código 0: ejecución satisfactoria y finalización del proceso
                        Environment.Exit(0);
                        break;

                    default:

                        PrintError("Debe introducir una opción existente");
                        break;
                }
            }
        }

        public static void PrintError(String strError)
        {
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Error.WriteLine(strError);
            Console.ResetColor();
        }

        //Imprime error cuando se falla 10 veces
        public static void AntiFlood(int errorCounter)
        {
            if (errorCounter%10==0)
            {
                errorCounter = 0;
                PrintError("Demasiados fallos al introducir datos (Esperando 3 segundos...)");
                System.Threading.Thread.Sleep(3000);
            }
        }
    }

   
}
