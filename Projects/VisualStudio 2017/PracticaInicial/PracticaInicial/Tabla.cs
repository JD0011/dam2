﻿/*
* PRÁCTICA.............: Práctica Inicial.
* NOMBRE y APELLIDOS...: Juan de Dios Delgado Bermúdez
* CURSO y GRUPO........: 2o Desarrollo de Interfaces
* TÍTULO de la PRÁCTICA: Uso del IDE V.Studio
* FECHA de ENTREGA.....: 16 de Octubre de 2017
*/

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticaInicial.Funcion
{
    class Tabla
    {
        #region Vars

        int _intNumTabla, _intNumElementos;

        #endregion Vars

        #region Constructor
        
        public Tabla(int intNumero, int intNumElementos)
        {
            _intNumTabla = intNumero;
            _intNumElementos = intNumElementos;
        }

        #endregion Constructor

        #region Methods

        public void PrintTable()
        {
            //Pagina si son más de 20 elementos
            if (_intNumElementos > 20)
            {
                PrintPaginatedTable(20);
            }
            else
            {
                PrintFormattedTable();
            }
        }

        public void PrintFormattedTable()
        {
            string strTableFormat;
            PrintTableHeader();
            Console.BackgroundColor = ConsoleColor.Blue;
            
            for(int i = 1; i <= _intNumElementos; i++)
            {
                //Alterna colores para facilitar visualización
                if (Console.BackgroundColor == ConsoleColor.Blue)
                {
                    Console.BackgroundColor = ConsoleColor.DarkGray;
                }
                else
                {
                    Console.BackgroundColor = ConsoleColor.Blue;
                }
                
                //Formato: 4 x 4   =   16
                strTableFormat = String.Format("{0,3}{1,2}{2,2}{3,4}{4,6}", _intNumTabla, "x", i, "=", (_intNumTabla*i));

                Console.WriteLine(strTableFormat);
            }
            Console.ResetColor();

            Console.WriteLine("\n\nPresione una tecla para continuar ...");
            Console.ReadKey();
            Console.Clear();
        }

        public void PrintPaginatedTable(int intResultsPerPage)
        {
            int intRegistroActual, intTotalPags, intPaginaActual;
            bool exitCondition;

            //Inicialización de variables
            intRegistroActual = 0;
            intPaginaActual = 1;

            //Establece páginas
            if (_intNumElementos % intResultsPerPage != 0)
            {
                intTotalPags = (_intNumElementos / intResultsPerPage) + 1;
            }
            else
            {
                intTotalPags = _intNumElementos / intResultsPerPage;
            }

            exitCondition = false;
            do
            {
                Console.Clear();

                //Mueve cursor al principio
                Console.SetCursorPosition(0, 0);

                Console.WriteLine("(Pulsa cualquier tecla para avanzar y salir o Esc para salir directamente)");
                PrintTableHeader();

                //Imprime resultados especificados y mientras haya operaciones restantes
                for (int i = 0; i < intResultsPerPage && intRegistroActual < _intNumElementos; i++)
                {
                    //Alterna colores de fila
                    if (i % 2 == 0)
                    {
                        Console.BackgroundColor = ConsoleColor.Blue;
                    }
                    else
                    {
                        Console.BackgroundColor = ConsoleColor.DarkGray;
                    }

                    intRegistroActual++;

                    //Formato: 4 x 4   =   16
                    Console.WriteLine(String.Format("{0,3}{1,2}{2,2}{3,4}{4,6}", _intNumTabla, "x", intRegistroActual, "=", (_intNumTabla * intRegistroActual)));
                }

                Console.ResetColor();
                Console.WriteLine("Página " + intPaginaActual + " de " + intTotalPags);
                if(Console.ReadKey(true).Key == ConsoleKey.Escape)
                {
                    Console.Clear();
                    exitCondition = true;
                    continue;
                }
                //Suma pagina si posible
                else if (intPaginaActual < intTotalPags)
                {
                    intPaginaActual++;
                    //Deshecha las teclas
                    while (Console.KeyAvailable)
                    {
                        Console.ReadKey(false);
                    }
                    //Pequeña pausa entre carga y carga de página
                    System.Threading.Thread.Sleep(5);
                    
                }
                else
                {
                    Console.Clear();
                    exitCondition = true;
                    continue;
                }
                

            } while (!exitCondition);
        }

        private static void PrintTableHeader()
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine(String.Format("{0,-12}{1,-5}", "Operación", "Resultado"));
            Console.ResetColor();
        }

        #endregion Methods
    }
}
