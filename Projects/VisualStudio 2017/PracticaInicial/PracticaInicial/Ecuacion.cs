﻿/*
* PRÁCTICA.............: Práctica Inicial.
* NOMBRE y APELLIDOS...: Juan de Dios Delgado Bermúdez
* CURSO y GRUPO........: 2o Desarrollo de Interfaces
* TÍTULO de la PRÁCTICA: Uso del IDE V.Studio
* FECHA de ENTREGA.....: 16 de Octubre de 2017
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticaInicial.Funcion
{
    class Ecuacion
    {
        #region Vars
        
        double _dbA, _dbB, _dbC, _dbDiscriminante;
        
        /**
         * En caso de discriminante negativo, se establece un tamaño de 2, 
         * para almacenar los sumandos del resultado, ya que no se
         * pueden operar, y se concatenan cuando se muestre el resultado
         */ 
        double[] _arrResultPositivo;
        double[] _arrResultNegativo;

        #endregion Vars

        #region Constructor

        public Ecuacion(double a, double b, double c)
        {
            this._dbA = a;
            this._dbB = b;
            this._dbC = c;
            
            _dbDiscriminante = Math.Pow(b, 2) - (4d * a * c);
        }
        #endregion Constructor

        #region Methods

        public void solve()
        {
            //Una raíz real
            if (_dbDiscriminante == 0)
            {
                _arrResultPositivo = new double[1];

                _arrResultPositivo[0] = -_dbB / (2 * _dbA);

            }
            //Dos raices reales
            if (_dbDiscriminante > 0)
            {
                _arrResultPositivo = new double[1];
                _arrResultNegativo = new double[1];

                _arrResultPositivo[0] = (-_dbB + (Math.Sqrt(_dbDiscriminante))) / (2 * _dbA);
                _arrResultNegativo[0] = (-_dbB - (Math.Sqrt(_dbDiscriminante))) / (2 * _dbA);

            }

            // Discriminante negativo: se extrae -1 de la raiz y se almacena en i
            else
            {
                _arrResultPositivo = new double[2];
                _arrResultNegativo = new double[2];

                //Operando+ 1 y 2
                _arrResultPositivo[0] = -_dbB / (2 * _dbA);
                _arrResultPositivo[1] = Math.Sqrt(_dbDiscriminante * (-1)) / (2 * _dbA);

                //Operando- 1 y 2
                _arrResultNegativo[0] = -_dbB / (2 * _dbA);
                _arrResultNegativo[1] = Math.Sqrt(_dbDiscriminante * (-1)) / (2 * _dbA);

            }
        }

        public void PrintEcuacionResult()
        {
            Console.ForegroundColor = ConsoleColor.Green;

            if (_dbDiscriminante > 0)
            {
                Console.WriteLine(
                    "Resultados:\n" +
                    "x1 = " + Math.Round(_arrResultPositivo[0], 4)+"\n"+
                    "x2 = " + Math.Round(_arrResultNegativo[0], 4));

            }
            else if (_dbDiscriminante < 0)
            {   
                Console.WriteLine(
                    "Resultados:\n" +
                    "x1 = " + Math.Round(_arrResultPositivo[0], 4) + " + " + Math.Round(_arrResultPositivo[1], 4) + "i\n" +
                    "x2 = " + Math.Round(_arrResultNegativo[0], 4) + " - " + Math.Round(_arrResultNegativo[1], 4) + "i");
            }
            else
            {
                Console.WriteLine(
                    "Resultado:\n" +
                    "x1 = " + Math.Round(_arrResultPositivo[0], 4));
            }

            Console.ResetColor();
        }

        #endregion Methods
    }
}
