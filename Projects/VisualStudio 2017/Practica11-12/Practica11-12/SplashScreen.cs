﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Practica11_12
{
    public partial class SplashScreen : Form
    {
        Timer tmr = new Timer();
        public SplashScreen()
        {
            InitializeComponent();
        }

        private void SplashScreen_Load(object sender, EventArgs e)
        {
            tmr.Interval = 2500;
            tmr.Tick += LaunchForm;
            tmr.Start();

        }

        private void LaunchForm(object sender, EventArgs e)
        {
            tmr.Stop();
            this.Hide();
            ConnectionForm mform = new ConnectionForm();
            mform.Show();
            
        }
    }
}
