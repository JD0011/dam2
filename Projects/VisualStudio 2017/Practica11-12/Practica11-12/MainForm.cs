﻿using SerialPortDLL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Practica11_12
{
    public partial class MainForm : Form
    {
        #region Variables de clase
        private ConfigurationForm _cform;
        private string _strDownloadFolder;
        private SerialPortTransferrer _sptCOM;

        private const string PatternFileName = "[a-zA-Z0-9]";
        private const int MaxMSGLength = 100;
        #endregion Variables de clase

        public MainForm(SerialPort sport, string strUsername, string strDownloadFolder)
        {
            _strDownloadFolder = strDownloadFolder;

            InitializeComponent();
            InitializeCustomComponents();

            _sptCOM = new SerialPortTransferrer(sport, strUsername, strDownloadFolder, rtbInput, rtbOutput, this);

            UpdateFormTitle();
        }

        private void InitializeCustomComponents()
        {
            if (_strDownloadFolder.Equals(ConnectionForm.DefaultDownloadFolder))
            {
                if (!File.Exists(_strDownloadFolder))
                {
                    Directory.CreateDirectory(_strDownloadFolder);
                }
            }
        }

        public void UpdateFormTitle()
        {
            this.Text = String.Format("Cliente Puerto : [{0}] Usuario : {1}", _sptCOM.SerialPort.PortName, _sptCOM.Username);
        }

        public void BlockUI()
        {
            this.Invoke((MethodInvoker)(() => btnEnviar.Enabled = false));
            this.Invoke((MethodInvoker)(() => rtbOutput.Enabled = false));
        }

        public void UnblockUI()
        {
            if (this.IsDisposed)
            {
                btnEnviar.Enabled = true;
                rtbOutput.Enabled = true;
            }
            else
            {
                this.Invoke((MethodInvoker)(() => btnEnviar.Enabled = true));
                this.Invoke((MethodInvoker)(() => rtbOutput.Enabled = true));
            }           
        }

        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void btnEnviar_Click(object sender, EventArgs e)
        {
            if (!rtbOutput.Text.Equals(""))
            {
                _sptCOM.SendMessage();

                rtbOutput.Clear();
            }
        }

        private void rtbOutput_TextChanged(object sender, EventArgs e)
        {
            int intMsgLength = rtbOutput.Text.Length;
            btnEnviar.Enabled = intMsgLength > 0 ? true : false;

            if(intMsgLength > MaxMSGLength)
            {
                rtbOutput.Text = rtbOutput.Text.Substring(0, 99);
                MessageBox.Show(
                    "El mensaje es demasiado grande. No debe superar los " + MaxMSGLength + " caracteres",
                    "Mensaje demasiado grande",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Warning
                    );
            }
        }

        private void btnEnviarArchivo_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();

            if(ofd.ShowDialog() == DialogResult.OK)
            {
                if (new FileInfo(ofd.FileName).Length > 1000000)// Superior a 1 mb
                {
                    if(_sptCOM.SerialPort.BaudRate!= 128000)
                    {
                        DialogResult result = MessageBox.Show(
                        this,
                        "Si desea transmitir ficheros superiores a 1 MB, es recomendable aumentar " +
                        "la velocidad de transferencia desde la configuración al máximo posible. ¿Desea enviar el fichero igualmente?",
                        "Peso de fichero elevado",
                        MessageBoxButtons.YesNo,
                        MessageBoxIcon.Warning
                        );

                        if (result == DialogResult.Yes)
                        {
                            string strFileName = ofd.FileName;
                            if (new Regex(PatternFileName).IsMatch(strFileName))
                            {
                                _sptCOM.AskForFile(ofd.FileName);
                            }
                            else
                            {
                                MessageBox.Show(
                                    this,
                                    "El nombre del archivo solo puede contener letras(A-Z) y números",
                                    "Nombre de fichero no válido",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Error
                                    );
                            }
                        }
                    }
                }
            }
        }

        private void acercaDeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AboutTheApp ata = new AboutTheApp();
            ata.ShowDialog();
        }

        private void configuraciónToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bool mustOpen = true;
            if (_sptCOM.IsTransferring)
            {
                DialogResult result = MessageBox.Show(
                    "Si edita la conexión durante una transferencia de archivo, este quedará corrupto." +
                    " ¿Desea editarla igualmente?",
                    "Aviso",
                    MessageBoxButtons.OKCancel,
                    MessageBoxIcon.Warning
                    );

                if(result == DialogResult.OK)
                {
                    _sptCOM.CancelTransfer();
                }
                else
                {
                    mustOpen = false;
                }
            }

            if (mustOpen)
            {
                _cform = new ConfigurationForm(_sptCOM);
                _cform.ShowDialog();
            }
        }

        private void btnCancelarTrans_Click(object sender, EventArgs e)
        {
            if (_sptCOM.IsTransferring)
            {
                _sptCOM.CancelTransfer();
            }

            ShowSendFileButton();
            UnblockUI();
        }

        public void ShowProgressBar()
        {
            this.Invoke((MethodInvoker)(() => btnEnviarArchivo.Visible = false));
            this.Invoke((MethodInvoker)(() => pbTransfer.Visible = true));
            this.Invoke((MethodInvoker)(() => btnCancelarTrans.Visible = true));
        }

        public void ShowSendFileButton()
        {
            if (this.IsDisposed)
            {
                btnEnviarArchivo.Visible = true;
                pbTransfer.Visible = false;
                btnCancelarTrans.Visible = false;
            }
            else
            {
                this.Invoke((MethodInvoker)(() => btnEnviarArchivo.Visible = true));
                this.Invoke((MethodInvoker)(() => pbTransfer.Visible = false));
                this.Invoke((MethodInvoker)(() => btnCancelarTrans.Visible = false));
            }
        }

        public void EnableReceivingAdvice()
        {
            this.Invoke((MethodInvoker)(() => btnEnviarArchivo.Enabled = false));
            this.Invoke((MethodInvoker)(() => btnCancelReceivingFile.Visible = true));
            this.Invoke((MethodInvoker)(() => btnEnviarArchivo.Text = "Recibiendo archivo"));
        }

        public void DisableReceivingAdvice()
        {
            this.Invoke((MethodInvoker)(() => btnEnviarArchivo.Text = "Enviar archivo"));
            this.Invoke((MethodInvoker)(() => btnEnviarArchivo.Enabled = true));
            this.Invoke((MethodInvoker)(() => btnCancelReceivingFile.Visible = false));
        }

        private void btnCancelReceivingFile_Click(object sender, EventArgs e)
        {
            _sptCOM.DeactivateTransfer();
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (_sptCOM.IsTransferring)
            {
                _sptCOM.CancelTransfer();
            }

            ShowSendFileButton();
            UnblockUI();
        }
    }
}
