﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica11_12
{
    //Guarda la configuración del puerto serie en el registro
    public class ConfigSaver
    {
        private const string RootKey = "HKEY_CURRENT_USER\\Software\\ChatSP";
        private const string SubKey = "Configuration";
        private const string KeyName = RootKey + "\\" + SubKey;

        #region Serial Port Fields
        private const string PortName = "PortName";
        private const string BaudRate = "BaudRate";
        private const string Parity = "Parity";
        private const string StopBits = "StopBits";
        private const string DataBits = "DataBits";
        #endregion

        #region User Fields
        private const string Username = "Username";
        private const string DownloadFolder = "DownloadFolder";
        #endregion

        public void SaveSerialPortConfig(SerialPort sport)
        {
            Registry.SetValue(KeyName, PortName, sport.PortName);
            Registry.SetValue(KeyName, BaudRate, sport.BaudRate);
            Registry.SetValue(KeyName, Parity, sport.Parity);
            Registry.SetValue(KeyName, StopBits, sport.StopBits);
            Registry.SetValue(KeyName, DataBits, sport.DataBits);
        }

        public SerialPort ReadSerialPortConfig()
        {
            SerialPort spAux;
            try
            {
                string strPortName = Registry.GetValue(KeyName, PortName, null).ToString();
                int baudRate = int.Parse(Registry.GetValue(KeyName, BaudRate, null).ToString());
                Parity selectedParity = 
                    (Parity)Enum.Parse(typeof(Parity), Registry.GetValue(KeyName, Parity, null).ToString());
                int selectedDataBits = int.Parse(Registry.GetValue(KeyName, DataBits, null).ToString());
                StopBits selectedSB = 
                    (StopBits)Enum.Parse(typeof(StopBits), Registry.GetValue(KeyName, StopBits, null).ToString());

                spAux = new SerialPort(strPortName, baudRate, selectedParity, selectedDataBits, selectedSB);
            }
            catch (Exception)
            {
                spAux = null;
            }

            return spAux;
        }

        public void SaveDownloadFolder(string strDownloadFolder)
        {
            Registry.SetValue(KeyName, DownloadFolder, strDownloadFolder);
        }

        public string ReadDownloadFolder()
        {
            string aux;
            try
            {
                aux = Registry.GetValue(KeyName, DownloadFolder, null).ToString();
            }
            catch (Exception)
            {
                aux = null;
            }


            return aux;
        }
    }
}
