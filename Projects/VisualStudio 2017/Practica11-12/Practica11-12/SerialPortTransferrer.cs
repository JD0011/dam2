﻿using SerialPortDLL;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Practica11_12
{
    public class SerialPortTransferrer
    {
        #region Variables
        
        private string _strFilePath;
        private string _strReceivedFileName;
        private string _strUsername;
        private string _strDownloadFolder;

        private RichTextBox _rtbInput;
        private RichTextBox _rtbOutput;
        private MainForm _mform;
        
        private FileStream _fsFile;

        private SerialPort _sport;
        private bool _isTransferring;
        private bool _deniedFromDestination;
        
        #endregion Variables

        #region Propiedades
        public SerialPort SerialPort { get => _sport; set => _sport = value; }
        public string Username { get => _strUsername; }
        public bool IsTransferring { get => _isTransferring; }
        #endregion

        public SerialPortTransferrer(SerialPort sport, string strUsername, string strDownloadFolder, RichTextBox rtbInput, RichTextBox rtbOutput, MainForm mform)
        {
            _sport = sport;
            _rtbInput = rtbInput;
            _rtbOutput = rtbOutput;
            _strUsername = strUsername;
            _mform = mform;
            _strDownloadFolder = strDownloadFolder;

            InitializeCustomComponents();
        }

        private void InitializeCustomComponents()
        {
            _sport.DataReceived += new SerialDataReceivedEventHandler(DataHandler);
        }

        private void DataHandler(object sender, SerialDataReceivedEventArgs e)
        {
            byte[] header = new byte[3];
            byte[] content;

            _sport.Read(header, 0, header.Length);

            content = new byte[_sport.BytesToRead];
            _sport.Read(content, 0, content.Length);

            string cmd = System.Text.Encoding.Default.GetString(header);
            switch (cmd)
            {
                //Mensaje
                case TCommands.Message:
                    string strMSG = System.Text.Encoding.Default.GetString(content);
                    _rtbInput.Invoke((MethodInvoker)(() => _rtbInput.AppendText(MessageFormatter.FormatMessage(strMSG) + "\n")));
                    break;

                //Nombre de fichero
                case TCommands.FileRequest:
                    string strAppendedText = System.Text.Encoding.Default.GetString(content);
                    string[] strParts = strAppendedText.Split(new string[] { TCommands.Separator }, StringSplitOptions.None);

                    _strReceivedFileName = strParts[0];
                    long kbSize = Convert.ToInt64(strParts[1]) / 1000;
                    string strFileSize = kbSize.ToString() + " kb";

                    DialogResult result = DialogResult.No;
                    _mform.Invoke(new Action(() => {
                        result = MessageBox.Show(
                                                _mform,
                                                String.Format("Desea descargar el fichero {0} ({1})?", _strReceivedFileName, strFileSize),
                                                "Archivo a recibir",
                                                MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                    }));

                    bool shouldReceive;
                    if (result == DialogResult.No)
                    {
                        shouldReceive = false;
                    }
                    else
                    {
                        if (!File.Exists(Path.Combine(_strDownloadFolder, _strReceivedFileName)))
                        {
                            shouldReceive = true;
                        }
                        else
                        {
                            result = DialogResult.No;
                            _mform.Invoke(new Action(() => {
                               result = MessageBox.Show(
                                String.Format("Ya existe el fichero {0} en la carpeta de descargas. ¿Quiere sobreescribirlo?", _strReceivedFileName),
                                "Conflicto",
                                MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                            }));
                            

                            shouldReceive = result == DialogResult.Yes ? true : false;
                        }
                    }

                    if (shouldReceive)
                    {
                        _fsFile = new FileStream(Path.Combine(_strDownloadFolder, _strReceivedFileName), FileMode.Create);
                        _sport.Write(TCommands.AcceptFile);
                        _mform.EnableReceivingAdvice();

                    }
                    else
                    {
                        _sport.Write(TCommands.DenyFile);
                    }
                    break;

                //Fichero aceptado
                case TCommands.AcceptFile:

                    _mform.ShowProgressBar();

                    _mform.BlockUI();
                    new Thread(new ThreadStart(SendFile)).Start();
                    _mform.UnblockUI();
                    
                    break;

                case TCommands.CancelFileTransfer:

                    _fsFile.Close();
                    _mform.DisableReceivingAdvice();
                    _mform.ShowSendFileButton();

                    result = DialogResult.No;
                    _mform.Invoke(new Action(() => {
                        result = MessageBox.Show(
                        "El usuario ha cancelado la transferencia, por lo tanto, el archivo quedó corrupto. ¿Desea eliminar" + _strReceivedFileName + "?",
                        "Transferencia cancelada",
                        MessageBoxButtons.YesNo,
                        MessageBoxIcon.Warning
                        );
                    }));

                    if (result == DialogResult.Yes)
                    {
                        File.Delete(Path.Combine(_strDownloadFolder, _strReceivedFileName));

                        _mform.Invoke(new Action(() => {
                            MessageBox.Show(
                            "Fichero " + _strReceivedFileName + " eliminado satisfactoriamente",
                            "Fichero eliminado",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Information);
                        }));
                    }

                    break;

                //Fichero rechazado : ante / durante el envío
                case TCommands.DenyFile:
                    //En caso de recibir comando durante envío...
                    _sport.DiscardOutBuffer();
                    _mform.UnblockUI();
                    _mform.ShowSendFileButton();
                    _deniedFromDestination = true;
                    
                    _mform.Invoke(new Action(() => {
                        MessageBox.Show(
                         "La recepción del fichero fue rechazada por el usuario",
                         "Aviso",
                         MessageBoxButtons.OK, MessageBoxIcon.Warning
                         );
                    }));                    
                    break;

                //Fin de transferencia
                case TCommands.EndTransmission:
                    _fsFile.Close();
                    _mform.DisableReceivingAdvice();

                    _mform.Invoke(new Action(() => {
                        MessageBox.Show("Fichero recibido satisfactoriamente.");
                    }));
                    break;

                //Chunks
                default:
                    //Header es parte de contenido
                    byte[] data = new byte[header.Length + content.Length];
                    Buffer.BlockCopy(header, 0, data, 0, header.Length);
                    Buffer.BlockCopy(content, 0, data, header.Length, content.Length);

                    //Controla que el flujo esté abierto
                    try
                    {
                        _fsFile.Write(data, 0, data.Length);
                    }
                    catch (ObjectDisposedException)
                    {
                        
                    }
                    break;
            }
        }

        public void DeactivateTransfer()
        {
            //Notifica al remitente que pare de enviar
            _sport.Write(TCommands.DenyFile);
            _mform.DisableReceivingAdvice();
            _mform.ShowSendFileButton();
            _fsFile.Close();

            DialogResult result = DialogResult.No;
            _mform.Invoke(new Action(() => {
                result = MessageBox.Show(
                "Ha cancelado la transferencia, por lo tanto, el archivo quedó corrupto. ¿Desea eliminar" + _strReceivedFileName + "?",
                "Transferencia cancelada",
                MessageBoxButtons.YesNo,
                MessageBoxIcon.Warning
                );
            }));

            if (result == DialogResult.Yes)
            {
                File.Delete(Path.Combine(_strDownloadFolder, _strReceivedFileName));
                MessageBox.Show(
                    "Fichero " + _strReceivedFileName + " eliminado satisfactoriamente",
                    "Fichero eliminado",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
            }

        }

        public void SendMessage()
        {
            string msg = (_strUsername) + ": " + _rtbOutput.Text;
            _rtbInput.AppendText(MessageFormatter.FormatMessage(msg) + "\n");

            _sport.Write(TCommands.Message + msg);
        }

        public void AskForFile(string strFilePath)
        {
            _strFilePath = strFilePath;
            _sport.Write(
                TCommands.FileRequest + 
                //Nombre fichero
                Path.GetFileName(strFilePath) +
                TCommands.Separator + 
                //Tamaño de fichero
                new FileInfo(strFilePath).Length);
        }
        
        public void SendFile()
        {
            FileStream fs = new FileStream(_strFilePath, FileMode.Open);
            //Lee todos los bytes
            BinaryReader br = new BinaryReader(fs);

            _isTransferring = true;
            
            //?
            System.Threading.Thread.Sleep(200);
            try
            {
                _sport.Write(br.ReadBytes((int)fs.Length), 0, (int)fs.Length);
                //?
                System.Threading.Thread.Sleep(200);

                _sport.Write(TCommands.EndTransmission);
            }
            catch (IOException)
            {
                //?
                System.Threading.Thread.Sleep(200);
                if (!_deniedFromDestination)
                {
                    _sport.Write(TCommands.CancelFileTransfer);
                }
                else
                {
                    _deniedFromDestination = false;
                }
            }

            br.Close();
            fs.Close();

            _mform.ShowSendFileButton();

            

            _isTransferring = false;

        }

        //Para cliente que envía
        public void CancelTransfer()
        {
            //Provoca excepción controlada en envío
            _sport.DiscardOutBuffer();

            _mform.ShowSendFileButton();
        }
    }           
}
