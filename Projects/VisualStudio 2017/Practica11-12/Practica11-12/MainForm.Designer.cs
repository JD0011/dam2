﻿using System;

namespace Practica11_12
{
    partial class MainForm
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.serialPort = new System.IO.Ports.SerialPort(this.components);
            this.msPrincipal = new System.Windows.Forms.MenuStrip();
            this.configuracionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.acercaDeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rtbInput = new System.Windows.Forms.RichTextBox();
            this.rtbOutput = new System.Windows.Forms.RichTextBox();
            this.btnEnviar = new System.Windows.Forms.Button();
            this.btnEnviarArchivo = new System.Windows.Forms.Button();
            this.btnCancelarTrans = new System.Windows.Forms.Button();
            this.pbTransfer = new System.Windows.Forms.ProgressBar();
            this.btnCancelReceivingFile = new System.Windows.Forms.Button();
            this.msPrincipal.SuspendLayout();
            this.SuspendLayout();
            // 
            // msPrincipal
            // 
            this.msPrincipal.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.configuracionToolStripMenuItem,
            this.acercaDeToolStripMenuItem});
            this.msPrincipal.Location = new System.Drawing.Point(0, 0);
            this.msPrincipal.Name = "msPrincipal";
            this.msPrincipal.Size = new System.Drawing.Size(421, 24);
            this.msPrincipal.TabIndex = 1;
            this.msPrincipal.Text = "menuStrip1";
            // 
            // configuracionToolStripMenuItem
            // 
            this.configuracionToolStripMenuItem.Name = "configuracionToolStripMenuItem";
            this.configuracionToolStripMenuItem.Size = new System.Drawing.Size(95, 20);
            this.configuracionToolStripMenuItem.Text = "Configuración";
            this.configuracionToolStripMenuItem.Click += new System.EventHandler(this.configuraciónToolStripMenuItem_Click);
            // 
            // acercaDeToolStripMenuItem
            // 
            this.acercaDeToolStripMenuItem.Name = "acercaDeToolStripMenuItem";
            this.acercaDeToolStripMenuItem.Size = new System.Drawing.Size(77, 20);
            this.acercaDeToolStripMenuItem.Text = "Acerca de..";
            this.acercaDeToolStripMenuItem.Click += new System.EventHandler(this.acercaDeToolStripMenuItem_Click);
            // 
            // rtbInput
            // 
            this.rtbInput.Location = new System.Drawing.Point(12, 27);
            this.rtbInput.Name = "rtbInput";
            this.rtbInput.ReadOnly = true;
            this.rtbInput.Size = new System.Drawing.Size(395, 231);
            this.rtbInput.TabIndex = 1;
            this.rtbInput.TabStop = false;
            this.rtbInput.Text = "";
            // 
            // rtbOutput
            // 
            this.rtbOutput.Location = new System.Drawing.Point(12, 264);
            this.rtbOutput.Name = "rtbOutput";
            this.rtbOutput.Size = new System.Drawing.Size(297, 37);
            this.rtbOutput.TabIndex = 2;
            this.rtbOutput.Text = "";
            this.rtbOutput.TextChanged += new System.EventHandler(this.rtbOutput_TextChanged);
            // 
            // btnEnviar
            // 
            this.btnEnviar.Enabled = false;
            this.btnEnviar.Location = new System.Drawing.Point(315, 264);
            this.btnEnviar.Name = "btnEnviar";
            this.btnEnviar.Size = new System.Drawing.Size(92, 37);
            this.btnEnviar.TabIndex = 3;
            this.btnEnviar.Text = "Enviar";
            this.btnEnviar.UseVisualStyleBackColor = true;
            this.btnEnviar.Click += new System.EventHandler(this.btnEnviar_Click);
            // 
            // btnEnviarArchivo
            // 
            this.btnEnviarArchivo.AutoSize = true;
            this.btnEnviarArchivo.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnEnviarArchivo.Location = new System.Drawing.Point(12, 307);
            this.btnEnviarArchivo.Name = "btnEnviarArchivo";
            this.btnEnviarArchivo.Size = new System.Drawing.Size(85, 23);
            this.btnEnviarArchivo.TabIndex = 2;
            this.btnEnviarArchivo.Text = "Enviar archivo";
            this.btnEnviarArchivo.UseVisualStyleBackColor = true;
            this.btnEnviarArchivo.Click += new System.EventHandler(this.btnEnviarArchivo_Click);
            // 
            // btnCancelarTrans
            // 
            this.btnCancelarTrans.AutoSize = true;
            this.btnCancelarTrans.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnCancelarTrans.Location = new System.Drawing.Point(103, 307);
            this.btnCancelarTrans.Name = "btnCancelarTrans";
            this.btnCancelarTrans.Size = new System.Drawing.Size(123, 23);
            this.btnCancelarTrans.TabIndex = 7;
            this.btnCancelarTrans.Text = "Cancelar transferencia";
            this.btnCancelarTrans.UseVisualStyleBackColor = true;
            this.btnCancelarTrans.Visible = false;
            this.btnCancelarTrans.Click += new System.EventHandler(this.btnCancelarTrans_Click);
            // 
            // pbTransfer
            // 
            this.pbTransfer.Location = new System.Drawing.Point(12, 307);
            this.pbTransfer.MarqueeAnimationSpeed = 30;
            this.pbTransfer.Name = "pbTransfer";
            this.pbTransfer.Size = new System.Drawing.Size(85, 23);
            this.pbTransfer.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.pbTransfer.TabIndex = 8;
            this.pbTransfer.Visible = false;
            // 
            // btnCancelReceivingFile
            // 
            this.btnCancelReceivingFile.Location = new System.Drawing.Point(120, 307);
            this.btnCancelReceivingFile.Name = "btnCancelReceivingFile";
            this.btnCancelReceivingFile.Size = new System.Drawing.Size(75, 23);
            this.btnCancelReceivingFile.TabIndex = 9;
            this.btnCancelReceivingFile.Text = "Cancelar";
            this.btnCancelReceivingFile.UseVisualStyleBackColor = true;
            this.btnCancelReceivingFile.Visible = false;
            this.btnCancelReceivingFile.Click += new System.EventHandler(this.btnCancelReceivingFile_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(421, 339);
            this.Controls.Add(this.pbTransfer);
            this.Controls.Add(this.btnCancelarTrans);
            this.Controls.Add(this.btnEnviarArchivo);
            this.Controls.Add(this.msPrincipal);
            this.Controls.Add(this.btnEnviar);
            this.Controls.Add(this.rtbInput);
            this.Controls.Add(this.rtbOutput);
            this.Controls.Add(this.btnCancelReceivingFile);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.msPrincipal;
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainForm_FormClosed);
            this.msPrincipal.ResumeLayout(false);
            this.msPrincipal.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.IO.Ports.SerialPort serialPort;
        private System.Windows.Forms.MenuStrip msPrincipal;
        private System.Windows.Forms.ToolStripMenuItem configuracionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem acercaDeToolStripMenuItem;
        private System.Windows.Forms.RichTextBox rtbInput;
        private System.Windows.Forms.RichTextBox rtbOutput;
        private System.Windows.Forms.Button btnEnviar;
        private System.Windows.Forms.Button btnEnviarArchivo;
        private System.Windows.Forms.Button btnCancelarTrans;
        private System.Windows.Forms.ProgressBar pbTransfer;
        private System.Windows.Forms.Button btnCancelReceivingFile;
    }
}

