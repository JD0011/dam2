﻿using SerialPortDLL;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Practica11_12
{
    public partial class ConnectionForm : Form
    {
        private SerialPort _sport;
        private ConfigSaver _cfgSaver;

        private string _strUserame;
        private string _strDownloadFolder;
        private List<string> _rngPortNames;

        public const string DefaultDownloadFolder = "./Descargas";

        public ConnectionForm()
        {
            _cfgSaver = new ConfigSaver();

            InitializeComponent();
            InitializeCustomComponents();
        }

        private void UpdatePortNames()
        {

            _rngPortNames = SerialPort.GetPortNames().ToList();

            cbConexionesCOM.Items.Clear();
            if (_rngPortNames.Count != 0)
            {
                _rngPortNames.Sort();
                cbConexionesCOM.Items.AddRange(_rngPortNames.ToArray());
            }
            else
            {
                cbConexionesCOM.Items.Add("No disponibles");
            }

            cbConexionesCOM.SelectedIndex = 0;
        }

        private void InitializeCustomComponents()
        {

            UpdatePortNames();

            cbParidad.DataSource = Enum.GetValues(typeof(Parity));

            cbDataBits.Items.Add("7");
            cbDataBits.Items.Add("8");

            cbStopBits.Items.Add(StopBits.One);
            cbStopBits.Items.Add(StopBits.OnePointFive);
            cbStopBits.Items.Add(StopBits.Two);

            cbVelocidades.Items.Add(75);
            cbVelocidades.Items.Add(150);
            cbVelocidades.Items.Add(300);
            cbVelocidades.Items.Add(600);
            cbVelocidades.Items.Add(1200);
            cbVelocidades.Items.Add(1800);
            cbVelocidades.Items.Add(2400);
            cbVelocidades.Items.Add(4800);
            cbVelocidades.Items.Add(7200);
            cbVelocidades.Items.Add(9600);
            cbVelocidades.Items.Add(14400);
            cbVelocidades.Items.Add(19200);
            cbVelocidades.Items.Add(38400);
            cbVelocidades.Items.Add(56000);
            cbVelocidades.Items.Add(57600);
            cbVelocidades.Items.Add(115200);
            cbVelocidades.Items.Add(128000);

            tbCarpetaDescargas.Text = _strDownloadFolder = DefaultDownloadFolder;

            if ((_sport = _cfgSaver.ReadSerialPortConfig()) != null)
            {
                //Añade el puerto si existe
                string comPort = _sport.PortName;
                if (_rngPortNames.Contains(comPort))
                {
                    cbConexionesCOM.SelectedIndex = cbConexionesCOM.FindStringExact(_sport.PortName);
                }
                else
                {
                    cbConexionesCOM.SelectedIndex = 0;
                }

                //Independiente al puerto
                cbVelocidades.SelectedIndex = cbVelocidades.FindStringExact(_sport.BaudRate.ToString());
                cbParidad.SelectedIndex = cbParidad.FindStringExact(_sport.Parity.ToString());
                cbDataBits.SelectedIndex = cbDataBits.FindStringExact(_sport.DataBits.ToString());
                cbStopBits.SelectedIndex = cbStopBits.FindStringExact(_sport.StopBits.ToString());
            }
            else
            {
                cbConexionesCOM.SelectedIndex = 0;
                cbVelocidades.SelectedIndex = 0;
                cbParidad.SelectedIndex = 0;
                cbDataBits.SelectedIndex = 0;
                cbStopBits.SelectedIndex = 0;
            }

            string strDownloadPath;
            if ((strDownloadPath = _cfgSaver.ReadDownloadFolder()) != null)
            {
                tbCarpetaDescargas.Text = strDownloadPath;
            }
        }

        private void btnConectar_Click(object sender, EventArgs e)
        {
            if (cbConexionesCOM.Text.Equals("No disponibles"))
            {
                MessageBox.Show(
                    this,
                    "No hay puertos COM disponibles",
                    "Ausencia de puertos",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
            else if (!FormatChecker.ValidateUsername((_strUserame = tbNomUsuario.Text)))
            {
                MessageBox.Show(
                     this,
                     "El nombre de usuario no es válido. Debe tener una longitud de entre 5 y 12 caracteres",
                     "Nombre incorrecto",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Error);
            }
            else
            {
                Parity selectedParity = (Parity)Enum.Parse(typeof(Parity), cbParidad.Text);
                StopBits selectedStopBits = (StopBits)Enum.Parse(typeof(StopBits), cbStopBits.Text);
                _sport = new SerialPort(cbConexionesCOM.Text, int.Parse(cbVelocidades.Text), selectedParity, int.Parse(cbDataBits.Text), selectedStopBits);

                bool isOpen;
                try
                {
                    _sport.Open();
                    isOpen = true;
                }
                catch (UnauthorizedAccessException)
                {
                    isOpen = false;
                    MessageBox.Show(
                        "El puerto seleccionado se encuentra en uso por otra aplicación",
                        "Puerto ocupado",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                catch (IOException)
                {
                    isOpen = false;
                    MessageBox.Show(
                        "El puerto seleccionado no está conectado a ningún dispositivo",
                        "Puerto no conectado",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }

                if (isOpen)
                {
                    _cfgSaver.SaveSerialPortConfig(_sport);

                    MainForm mform = new MainForm(_sport, _strUserame, _strDownloadFolder);
                    this.Hide();
                    mform.Show();
                }
            }
        }

        private void ConnectionForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void btnSetCarpetaDescargas_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();

            if (fbd.ShowDialog() == DialogResult.OK)
            {
                tbCarpetaDescargas.Text = _strDownloadFolder = fbd.SelectedPath;
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            UpdatePortNames();
        }
    }
}
