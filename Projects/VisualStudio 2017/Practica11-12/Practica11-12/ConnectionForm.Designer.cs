﻿namespace Practica11_12
{
    partial class ConnectionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ConnectionForm));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.lblStopBits = new System.Windows.Forms.Label();
            this.cbStopBits = new System.Windows.Forms.ComboBox();
            this.cbDataBits = new System.Windows.Forms.ComboBox();
            this.lbl = new System.Windows.Forms.Label();
            this.cbParidad = new System.Windows.Forms.ComboBox();
            this.lblParidad = new System.Windows.Forms.Label();
            this.cbVelocidades = new System.Windows.Forms.ComboBox();
            this.lblVelocidadConexion = new System.Windows.Forms.Label();
            this.cbConexionesCOM = new System.Windows.Forms.ComboBox();
            this.lblPuertoConexion = new System.Windows.Forms.Label();
            this.btnConectar = new System.Windows.Forms.Button();
            this.gbDatosUsuario = new System.Windows.Forms.GroupBox();
            this.tbCarpetaDescargas = new System.Windows.Forms.TextBox();
            this.btnSetCarpetaDescargas = new System.Windows.Forms.Button();
            this.tbNomUsuario = new System.Windows.Forms.TextBox();
            this.lblNombreUsuario = new System.Windows.Forms.Label();
            this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.groupBox1.SuspendLayout();
            this.gbDatosUsuario.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnRefresh);
            this.groupBox1.Controls.Add(this.lblStopBits);
            this.groupBox1.Controls.Add(this.cbStopBits);
            this.groupBox1.Controls.Add(this.cbDataBits);
            this.groupBox1.Controls.Add(this.lbl);
            this.groupBox1.Controls.Add(this.cbParidad);
            this.groupBox1.Controls.Add(this.lblParidad);
            this.groupBox1.Controls.Add(this.cbVelocidades);
            this.groupBox1.Controls.Add(this.lblVelocidadConexion);
            this.groupBox1.Controls.Add(this.cbConexionesCOM);
            this.groupBox1.Controls.Add(this.lblPuertoConexion);
            this.groupBox1.Location = new System.Drawing.Point(6, 15);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(246, 190);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Conexión";
            // 
            // btnRefresh
            // 
            this.btnRefresh.Image = ((System.Drawing.Image)(resources.GetObject("btnRefresh.Image")));
            this.btnRefresh.Location = new System.Drawing.Point(206, 17);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(28, 24);
            this.btnRefresh.TabIndex = 10;
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // lblStopBits
            // 
            this.lblStopBits.AutoSize = true;
            this.lblStopBits.Location = new System.Drawing.Point(57, 157);
            this.lblStopBits.Name = "lblStopBits";
            this.lblStopBits.Size = new System.Drawing.Size(49, 13);
            this.lblStopBits.TabIndex = 9;
            this.lblStopBits.Text = "Stop Bits";
            // 
            // cbStopBits
            // 
            this.cbStopBits.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbStopBits.FormattingEnabled = true;
            this.cbStopBits.Location = new System.Drawing.Point(113, 153);
            this.cbStopBits.Name = "cbStopBits";
            this.cbStopBits.Size = new System.Drawing.Size(121, 21);
            this.cbStopBits.TabIndex = 8;
            // 
            // cbDataBits
            // 
            this.cbDataBits.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbDataBits.FormattingEnabled = true;
            this.cbDataBits.Location = new System.Drawing.Point(113, 117);
            this.cbDataBits.Name = "cbDataBits";
            this.cbDataBits.Size = new System.Drawing.Size(121, 21);
            this.cbDataBits.TabIndex = 7;
            // 
            // lbl
            // 
            this.lbl.AutoSize = true;
            this.lbl.Location = new System.Drawing.Point(31, 121);
            this.lbl.Name = "lbl";
            this.lbl.Size = new System.Drawing.Size(75, 13);
            this.lbl.TabIndex = 6;
            this.lbl.Text = "Bits / Caracter";
            // 
            // cbParidad
            // 
            this.cbParidad.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbParidad.FormattingEnabled = true;
            this.cbParidad.Location = new System.Drawing.Point(113, 83);
            this.cbParidad.Name = "cbParidad";
            this.cbParidad.Size = new System.Drawing.Size(121, 21);
            this.cbParidad.TabIndex = 5;
            // 
            // lblParidad
            // 
            this.lblParidad.AutoSize = true;
            this.lblParidad.Location = new System.Drawing.Point(63, 87);
            this.lblParidad.Name = "lblParidad";
            this.lblParidad.Size = new System.Drawing.Size(43, 13);
            this.lblParidad.TabIndex = 4;
            this.lblParidad.Text = "Paridad";
            // 
            // cbVelocidades
            // 
            this.cbVelocidades.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbVelocidades.FormattingEnabled = true;
            this.cbVelocidades.Location = new System.Drawing.Point(112, 52);
            this.cbVelocidades.Name = "cbVelocidades";
            this.cbVelocidades.Size = new System.Drawing.Size(121, 21);
            this.cbVelocidades.TabIndex = 3;
            // 
            // lblVelocidadConexion
            // 
            this.lblVelocidadConexion.AutoSize = true;
            this.lblVelocidadConexion.Location = new System.Drawing.Point(52, 56);
            this.lblVelocidadConexion.Name = "lblVelocidadConexion";
            this.lblVelocidadConexion.Size = new System.Drawing.Size(54, 13);
            this.lblVelocidadConexion.TabIndex = 2;
            this.lblVelocidadConexion.Text = "Velocidad";
            // 
            // cbConexionesCOM
            // 
            this.cbConexionesCOM.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbConexionesCOM.FormattingEnabled = true;
            this.cbConexionesCOM.Location = new System.Drawing.Point(112, 20);
            this.cbConexionesCOM.Name = "cbConexionesCOM";
            this.cbConexionesCOM.Size = new System.Drawing.Size(87, 21);
            this.cbConexionesCOM.TabIndex = 1;
            // 
            // lblPuertoConexion
            // 
            this.lblPuertoConexion.AutoSize = true;
            this.lblPuertoConexion.Location = new System.Drawing.Point(7, 24);
            this.lblPuertoConexion.Name = "lblPuertoConexion";
            this.lblPuertoConexion.Size = new System.Drawing.Size(99, 13);
            this.lblPuertoConexion.TabIndex = 0;
            this.lblPuertoConexion.Text = "Puerto de conexión";
            // 
            // btnConectar
            // 
            this.btnConectar.Location = new System.Drawing.Point(177, 305);
            this.btnConectar.Name = "btnConectar";
            this.btnConectar.Size = new System.Drawing.Size(75, 23);
            this.btnConectar.TabIndex = 2;
            this.btnConectar.Text = "Conectar";
            this.btnConectar.UseVisualStyleBackColor = true;
            this.btnConectar.Click += new System.EventHandler(this.btnConectar_Click);
            // 
            // gbDatosUsuario
            // 
            this.gbDatosUsuario.Controls.Add(this.tbCarpetaDescargas);
            this.gbDatosUsuario.Controls.Add(this.btnSetCarpetaDescargas);
            this.gbDatosUsuario.Controls.Add(this.tbNomUsuario);
            this.gbDatosUsuario.Controls.Add(this.lblNombreUsuario);
            this.gbDatosUsuario.Location = new System.Drawing.Point(6, 211);
            this.gbDatosUsuario.Name = "gbDatosUsuario";
            this.gbDatosUsuario.Size = new System.Drawing.Size(246, 88);
            this.gbDatosUsuario.TabIndex = 3;
            this.gbDatosUsuario.TabStop = false;
            this.gbDatosUsuario.Text = "Datos de usuario";
            // 
            // tbCarpetaDescargas
            // 
            this.tbCarpetaDescargas.Location = new System.Drawing.Point(88, 57);
            this.tbCarpetaDescargas.Name = "tbCarpetaDescargas";
            this.tbCarpetaDescargas.ReadOnly = true;
            this.tbCarpetaDescargas.Size = new System.Drawing.Size(145, 20);
            this.tbCarpetaDescargas.TabIndex = 3;
            // 
            // btnSetCarpetaDescargas
            // 
            this.btnSetCarpetaDescargas.Location = new System.Drawing.Point(7, 56);
            this.btnSetCarpetaDescargas.Name = "btnSetCarpetaDescargas";
            this.btnSetCarpetaDescargas.Size = new System.Drawing.Size(75, 23);
            this.btnSetCarpetaDescargas.TabIndex = 2;
            this.btnSetCarpetaDescargas.Text = "Descargas";
            this.btnSetCarpetaDescargas.UseVisualStyleBackColor = true;
            this.btnSetCarpetaDescargas.Click += new System.EventHandler(this.btnSetCarpetaDescargas_Click);
            // 
            // tbNomUsuario
            // 
            this.tbNomUsuario.Location = new System.Drawing.Point(113, 23);
            this.tbNomUsuario.Name = "tbNomUsuario";
            this.tbNomUsuario.Size = new System.Drawing.Size(120, 20);
            this.tbNomUsuario.TabIndex = 1;
            // 
            // lblNombreUsuario
            // 
            this.lblNombreUsuario.AutoSize = true;
            this.lblNombreUsuario.Location = new System.Drawing.Point(7, 27);
            this.lblNombreUsuario.Name = "lblNombreUsuario";
            this.lblNombreUsuario.Size = new System.Drawing.Size(96, 13);
            this.lblNombreUsuario.TabIndex = 0;
            this.lblNombreUsuario.Text = "Nombre de usuario";
            // 
            // ConnectionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(260, 340);
            this.Controls.Add(this.gbDatosUsuario);
            this.Controls.Add(this.btnConectar);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "ConnectionForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ConnectionForm";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.ConnectionForm_FormClosed);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.gbDatosUsuario.ResumeLayout(false);
            this.gbDatosUsuario.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblStopBits;
        private System.Windows.Forms.ComboBox cbStopBits;
        private System.Windows.Forms.ComboBox cbDataBits;
        private System.Windows.Forms.Label lbl;
        private System.Windows.Forms.ComboBox cbParidad;
        private System.Windows.Forms.Label lblParidad;
        private System.Windows.Forms.ComboBox cbVelocidades;
        private System.Windows.Forms.Label lblVelocidadConexion;
        private System.Windows.Forms.ComboBox cbConexionesCOM;
        private System.Windows.Forms.Label lblPuertoConexion;
        private System.Windows.Forms.Button btnConectar;
        private System.Windows.Forms.GroupBox gbDatosUsuario;
        private System.Windows.Forms.TextBox tbCarpetaDescargas;
        private System.Windows.Forms.Button btnSetCarpetaDescargas;
        private System.Windows.Forms.TextBox tbNomUsuario;
        private System.Windows.Forms.Label lblNombreUsuario;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
        private System.Windows.Forms.Button btnRefresh;
    }
}