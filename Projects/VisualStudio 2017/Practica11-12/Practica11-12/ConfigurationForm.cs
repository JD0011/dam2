﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Practica11_12
{
    public partial class ConfigurationForm : Form
    {
        private SerialPortTransferrer _sptCOM;
        private ConfigSaver _cfgSaver;

        public ConfigurationForm(SerialPortTransferrer sptCOM)
        {
            _sptCOM = sptCOM;
            _cfgSaver = new ConfigSaver();

            InitializeComponent();
            InitializeCustomComponents();
        }

        private void InitializeCustomComponents()
        {
            cbParidad.DataSource = Enum.GetValues(typeof(Parity));

            cbDataBits.Items.Add("7");
            cbDataBits.Items.Add("8");

            cbStopBits.Items.Add(StopBits.One);
            cbStopBits.Items.Add(StopBits.OnePointFive);
            cbStopBits.Items.Add(StopBits.Two);
            
            cbVelocidades.Items.Add(75);
            cbVelocidades.Items.Add(150);
            cbVelocidades.Items.Add(300);
            cbVelocidades.Items.Add(600);
            cbVelocidades.Items.Add(1200);
            cbVelocidades.Items.Add(1800);
            cbVelocidades.Items.Add(2400);
            cbVelocidades.Items.Add(4800);
            cbVelocidades.Items.Add(7200);
            cbVelocidades.Items.Add(9600);
            cbVelocidades.Items.Add(14400);
            cbVelocidades.Items.Add(19200);
            cbVelocidades.Items.Add(38400);
            cbVelocidades.Items.Add(56000);
            cbVelocidades.Items.Add(57600);
            cbVelocidades.Items.Add(115200);
            cbVelocidades.Items.Add(128000);


            cbVelocidades.SelectedIndex = cbVelocidades.FindString(_sptCOM.SerialPort.BaudRate.ToString());
            cbParidad.SelectedIndex = cbParidad.FindString(_sptCOM.SerialPort.Parity.ToString());
            cbDataBits.SelectedIndex = cbDataBits.FindString(_sptCOM.SerialPort.DataBits.ToString());
            cbStopBits.SelectedIndex = cbStopBits.FindString(_sptCOM.SerialPort.StopBits.ToString());
        }

        private void ConnectionForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void cbVelocidades_SelectedIndexChanged(object sender, EventArgs e)
        {
            _sptCOM.SerialPort.BaudRate = int.Parse(cbVelocidades.Text);
            _cfgSaver.SaveSerialPortConfig(_sptCOM.SerialPort);
        }

        private void cbParidad_SelectedIndexChanged(object sender, EventArgs e)
        {
            _sptCOM.SerialPort.Parity = (Parity)Enum.Parse(typeof(Parity), cbParidad.Text);
            _cfgSaver.SaveSerialPortConfig(_sptCOM.SerialPort);
        }

        private void cbDataBits_SelectedIndexChanged(object sender, EventArgs e)
        {
            _sptCOM.SerialPort.DataBits = int.Parse(cbDataBits.Text);
            _cfgSaver.SaveSerialPortConfig(_sptCOM.SerialPort);
        }

        private void cbStopBits_SelectedIndexChanged(object sender, EventArgs e)
        {
            _sptCOM.SerialPort.StopBits = (StopBits)Enum.Parse(typeof(StopBits), cbStopBits.Text);
            _cfgSaver.SaveSerialPortConfig(_sptCOM.SerialPort);
        }
    }
}