﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica11_12
{
    public static class TCommands
    {
        public const string Separator = "SEP";
        public const string Message = "MSG";
        public const string FileRequest = "FRE";
        public const string AcceptFile = "YES";
        public const string DenyFile = "NOP";
        public const string CancelFileTransfer = "CFT";
        public const string EndTransmission = "END";
    }
}
