﻿namespace Practica11_12
{
    partial class ConfigurationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ConfigurationForm));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblStopBits = new System.Windows.Forms.Label();
            this.cbStopBits = new System.Windows.Forms.ComboBox();
            this.cbDataBits = new System.Windows.Forms.ComboBox();
            this.lbl = new System.Windows.Forms.Label();
            this.cbParidad = new System.Windows.Forms.ComboBox();
            this.lblParidad = new System.Windows.Forms.Label();
            this.cbVelocidades = new System.Windows.Forms.ComboBox();
            this.lblVelocidadConexion = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lblStopBits);
            this.groupBox1.Controls.Add(this.cbStopBits);
            this.groupBox1.Controls.Add(this.cbDataBits);
            this.groupBox1.Controls.Add(this.lbl);
            this.groupBox1.Controls.Add(this.cbParidad);
            this.groupBox1.Controls.Add(this.lblParidad);
            this.groupBox1.Controls.Add(this.cbVelocidades);
            this.groupBox1.Controls.Add(this.lblVelocidadConexion);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(229, 162);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Conexión";
            // 
            // lblStopBits
            // 
            this.lblStopBits.AutoSize = true;
            this.lblStopBits.Location = new System.Drawing.Point(35, 124);
            this.lblStopBits.Name = "lblStopBits";
            this.lblStopBits.Size = new System.Drawing.Size(49, 13);
            this.lblStopBits.TabIndex = 9;
            this.lblStopBits.Text = "Stop Bits";
            // 
            // cbStopBits
            // 
            this.cbStopBits.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbStopBits.FormattingEnabled = true;
            this.cbStopBits.Location = new System.Drawing.Point(91, 120);
            this.cbStopBits.Name = "cbStopBits";
            this.cbStopBits.Size = new System.Drawing.Size(121, 21);
            this.cbStopBits.TabIndex = 8;
            this.cbStopBits.SelectedIndexChanged += new System.EventHandler(this.cbStopBits_SelectedIndexChanged);
            // 
            // cbDataBits
            // 
            this.cbDataBits.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbDataBits.FormattingEnabled = true;
            this.cbDataBits.Location = new System.Drawing.Point(91, 84);
            this.cbDataBits.Name = "cbDataBits";
            this.cbDataBits.Size = new System.Drawing.Size(121, 21);
            this.cbDataBits.TabIndex = 7;
            this.cbDataBits.SelectedIndexChanged += new System.EventHandler(this.cbDataBits_SelectedIndexChanged);
            // 
            // lbl
            // 
            this.lbl.AutoSize = true;
            this.lbl.Location = new System.Drawing.Point(9, 88);
            this.lbl.Name = "lbl";
            this.lbl.Size = new System.Drawing.Size(75, 13);
            this.lbl.TabIndex = 6;
            this.lbl.Text = "Bits / Caracter";
            // 
            // cbParidad
            // 
            this.cbParidad.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbParidad.FormattingEnabled = true;
            this.cbParidad.Location = new System.Drawing.Point(91, 50);
            this.cbParidad.Name = "cbParidad";
            this.cbParidad.Size = new System.Drawing.Size(121, 21);
            this.cbParidad.TabIndex = 5;
            this.cbParidad.SelectedIndexChanged += new System.EventHandler(this.cbParidad_SelectedIndexChanged);
            // 
            // lblParidad
            // 
            this.lblParidad.AutoSize = true;
            this.lblParidad.Location = new System.Drawing.Point(41, 54);
            this.lblParidad.Name = "lblParidad";
            this.lblParidad.Size = new System.Drawing.Size(43, 13);
            this.lblParidad.TabIndex = 4;
            this.lblParidad.Text = "Paridad";
            // 
            // cbVelocidades
            // 
            this.cbVelocidades.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbVelocidades.FormattingEnabled = true;
            this.cbVelocidades.Location = new System.Drawing.Point(90, 19);
            this.cbVelocidades.Name = "cbVelocidades";
            this.cbVelocidades.Size = new System.Drawing.Size(121, 21);
            this.cbVelocidades.TabIndex = 3;
            this.cbVelocidades.SelectedIndexChanged += new System.EventHandler(this.cbVelocidades_SelectedIndexChanged);
            // 
            // lblVelocidadConexion
            // 
            this.lblVelocidadConexion.AutoSize = true;
            this.lblVelocidadConexion.Location = new System.Drawing.Point(30, 23);
            this.lblVelocidadConexion.Name = "lblVelocidadConexion";
            this.lblVelocidadConexion.Size = new System.Drawing.Size(54, 13);
            this.lblVelocidadConexion.TabIndex = 2;
            this.lblVelocidadConexion.Text = "Velocidad";
            // 
            // ConfigurationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(251, 184);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ConfigurationForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "ConfigurationForm";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblStopBits;
        private System.Windows.Forms.ComboBox cbStopBits;
        private System.Windows.Forms.ComboBox cbDataBits;
        private System.Windows.Forms.Label lbl;
        private System.Windows.Forms.ComboBox cbParidad;
        private System.Windows.Forms.Label lblParidad;
        private System.Windows.Forms.ComboBox cbVelocidades;
        private System.Windows.Forms.Label lblVelocidadConexion;
    }
}