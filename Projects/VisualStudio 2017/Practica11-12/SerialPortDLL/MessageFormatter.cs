﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SerialPortDLL
{
    public class MessageFormatter
    {
        public static string FormatMessage(string strMsg)
        {
            return String.Format(
                "[{0}] {1}",
                DateTime.Now.ToString("T"),
                strMsg);
        }

        public string FormatReceivedFileMessage(bool isReceived, long lFileSize, string strFileName)
        {
            return String.Format(
                "{0} [{1}]: Peso -> {2} \t Archivo -> {3}", 
                DateTime.Now.ToString("T"), 
                isReceived ? "Recibido" : "Enviado",
                lFileSize, 
                strFileName);
        }
    }
}
