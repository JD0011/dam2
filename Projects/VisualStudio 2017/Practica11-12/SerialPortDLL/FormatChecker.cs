﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SerialPortDLL
{
    public class FormatChecker
    {
        public static bool ValidateUsername(string username)
        {
            return new Regex("^[a-zA-Z]{5,12}$").IsMatch(username);
        }
    }
}
