﻿/*
* PRÁCTICA.............: Práctica 9.
* NOMBRE Y APELLIDOS...: Juan de Dios Delgado Bermúdez
* CURSO Y GRUPO........: 2º Desarrollo de Interfaces
* TÍTULO DE LA PRÁCTICA: Aplicaciones de Formulario II. Más Controles.
* FECHA DE ENTREGA.....: 24 de Enero de 2018
*/

using Practica5;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Timers;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;

namespace Practica9
{
    public partial class FormCreacionGrupo : Form
    {
        #region Variables

        private List<string> _listAsignaturasElegidas;
        private BindingList<string> _blistAsignaturas;
        private FormPrincipal _formPrincipal;

        //Grupo cargado
        private Grupo _grp;
        private List<Asignatura> _listAsignaturas;

        #region Patrones

        private const string patternNombreGrupo = "^[a-zA-ZñÑáéíóúÁÉÍÓÚ]{3,15}-[0-9]{1}[a-zA-Z]{0,1}$";
        private const string patternNombreAlumno = "^[a-zA-ZñÑáéíóúÁÉÍÓÚ ]{2,20}$";
        private const string patternAsignatura = "^[a-zA-ZñÑáéíóúÁÉÍÓÚ]{4,18}$";
        private const string pathAsignaturas = "asignaturas.txt";

        #endregion Patrones

        #endregion Variables

        public FormCreacionGrupo(FormPrincipal formPrincipal)
        {
            _formPrincipal = formPrincipal;

            InitializeComponent();
            
            _listAsignaturasElegidas = new List<string>();
            _listAsignaturas = new List<Asignatura>();

            //Carga asignaturas
            LoadAsignaturasFromFile();
            SetDataSource();

            tbNombreGrupo.Focus();
        }

        private void LoadAsignaturasFromFile()
        {
            if (File.Exists(pathAsignaturas))
            {
                FileStream stream = new FileStream(pathAsignaturas, FileMode.Open);
                BinaryFormatter formater = new BinaryFormatter();

                _blistAsignaturas = (BindingList<string>)formater.Deserialize(stream);
                stream.Close();
            }
            else
            {
                _blistAsignaturas = new BindingList<string>();
            }
        }

        private void SaveAsignaturasIntoFile()
        {
            if (File.Exists(pathAsignaturas))
            {
                File.Delete(pathAsignaturas);
            }

            FileStream stream = new FileStream(pathAsignaturas, FileMode.Create);
            BinaryFormatter formater = new BinaryFormatter();

            formater.Serialize(stream, _blistAsignaturas);
            stream.Close();
            
        }

        private bool SaveGrupo()
        {
            bool isSaved;

            SaveFileDialog sfd = new SaveFileDialog();

            sfd.DefaultExt = ".gru";
            sfd.FileName = _grp.Nombre;
            sfd.Title = "Seleccione la ruta para guardar el archivo";
            sfd.Filter = "Grupo | *.gru";

            //Procesa la opción
            if (isSaved = sfd.ShowDialog() == DialogResult.OK)
            {
                //Guarda el archivo
                FileStream stream = (FileStream)sfd.OpenFile();
                BinaryFormatter formatter = new BinaryFormatter();

                formatter.Serialize(stream, _grp);
                stream.Close();
                isSaved = true;
            }

            return isSaved;
        }

        private void SetDataSource()
        {
            bsAsignaturas.DataSource = _blistAsignaturas;
            lbAsignaturas.DataSource = _blistAsignaturas;
            clbAsignaturas.DataSource = bsAsignaturas;
        }

        private bool CompruebaDNI(string text)
        {
            bool isOkey;
            string auxNIF = "";
            int numNIF;
            
            char[] listaLetras = { 'T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D', 'X', 'B', 'N', 'J', 'Z', 'S', 'Q', 'V', 'H', 'L', 'C', 'K', 'E' };
            
            try
            {
                for (int i = 0; i < 8; i++) auxNIF += text[i];
                numNIF = int.Parse(auxNIF) % 23;

                isOkey = text[8] == listaLetras[numNIF];
            }
            catch (Exception)
            {
                isOkey = false;
            }

            return isOkey;
        }

        private void tbNombreGrupo_Leave(object sender, EventArgs e)
        {
            string strText = tbNombreGrupo.Text;
            Regex rxParaNombreGrupo = new Regex(patternNombreGrupo);
            if (!tbNombreGrupo.Text.Equals(""))
            {
                if (!rxParaNombreGrupo.IsMatch(strText))
                {
                    MessageBox.Show(
                                        "El nombre del grupo debe tener el siguiente formato: \n" +
                                        "<NOMBRE>-<NUMERACIÓN>(OPCIONAL:LETRA)\n" +
                                        "Ejemplo: COMERCIO-1A",
                                        "Nombre de grupo",
                                        MessageBoxButtons.OK,
                                        MessageBoxIcon.Error
                                        );
                    
                    tbNombreGrupo.Focus();
                }
            }
        }

        private void tbNombreGrupo_TextChanged(object sender, EventArgs e)
        {
            if (tbNombreGrupo.Text.Length > 15)
            {
                MessageBox.Show(
                    "La longitud del nombre del grupo no puede ser superior a 10 caracteres",
                    "Nombre de grupo",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error
                    );

                tbNombreGrupo.Text = "";
            }
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            //Cuenta el número de asignaturas que coinciden
            if (_blistAsignaturas.Count(s => s.Equals(tbNombreAsignatura.Text))==0)
            {
                _blistAsignaturas.Add(tbNombreAsignatura.Text);
                _listAsignaturasElegidas.Clear();
                SaveAsignaturasIntoFile();
                tbNombreAsignatura.Text = "";
            }
            else
            {
                MessageBox.Show(
                    "La asignatura ya existe", 
                    "Nombre de asignatura",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Warning
                    );

                tbNombreAsignatura.Text = "";
            }
        }

        private void tbAsignatura_TextChanged(object sender, EventArgs e)
        {
            if (tbNombreAsignatura.Text.Count() > 4)
            {
                btnAgregar.Enabled = true;
            }
            else
            {
                btnAgregar.Enabled = false;
            }
        }

        private void lbAsignaturas_SelectedValueChanged(object sender, EventArgs e)
        {
            if (lbAsignaturas.SelectedItem != null)
            {
                btnEliminar.Enabled = true;
            }
            else
            {
                btnEliminar.Enabled = false;
            }
        }
        
        private void btnEliminar_Click(object sender, EventArgs e)
        {
            //Elimina de binding source y asignaturas elegidas
            string strDeletedAsignatura = lbAsignaturas.SelectedItem.ToString();
            _blistAsignaturas.Remove(strDeletedAsignatura);
            _listAsignaturasElegidas.Clear();

            SaveAsignaturasIntoFile();
        }

        private void clbAsignaturas_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            //Añade o elimina el valor en función de Checked / Unchecked
            if(e.NewValue == CheckState.Checked)
            {
                if (!_listAsignaturasElegidas.Contains((string)clbAsignaturas.SelectedValue))
                {
                    _listAsignaturasElegidas.Add((string)clbAsignaturas.SelectedValue);
                }
                
            }
            else
            {
                _listAsignaturasElegidas.Remove((string)clbAsignaturas.SelectedValue);
            }
        }

        private void tbAsignatura_Leave(object sender, EventArgs e)
        {
            if (!tbNombreAsignatura.Text.Equals(""))
            {
                Regex rxParaAsignatura = new Regex(patternAsignatura);
                if (!rxParaAsignatura.IsMatch(tbNombreAsignatura.Text))
                {
                    MessageBox.Show(
                    "La asignatura no puede contener espacios ni ser superior a 18 caracteres",
                    "Nombre de asignatura",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Warning
                    );

                    tbNombreAsignatura.Text = "";
                }
            }
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {

            int checkedItems = _listAsignaturasElegidas.Count();

            //Comprueba nombre
            string strNombreGrupo = tbNombreGrupo.Text;
            if (!new Regex(patternNombreGrupo).IsMatch(strNombreGrupo))
            {
                MessageBox.Show(
                "El nombre del grupo debe tener el siguiente formato: \n" +
                "<NOMBRE>-<NUMERACIÓN>(OPCIONAL:LETRA)\n" +
                "Ejemplo: COMERCIO-1A",
                "Nombre de grupo",
                MessageBoxButtons.OK,
                MessageBoxIcon.Error
                );
            }
            //Comprueba checked items
            else if (!(checkedItems >= 4 && checkedItems <= 7))
            {
                MessageBox.Show(
                "Debe elegir entre 4 y 7 asignaturas. Si no existen, diríjase al gestor de asignaturas para crearlas",
                "Número de asignaturas no válido",
                MessageBoxButtons.OK,
                MessageBoxIcon.Warning
                );
            }
            else
            {
                _grp = new Grupo(tbNombreGrupo.Text, _listAsignaturasElegidas.ToArray());

                if (SaveGrupo())
                {
                    if (_formPrincipal.FormEditorGrupo == null)
                    {
                        _formPrincipal.FormEditorGrupo = new FormEditorGrupo(_formPrincipal);
                    }

                    if (!_formPrincipal.FormEditorGrupo.Visible)
                    {
                        _formPrincipal.FormEditorGrupo.Show();
                        _formPrincipal.ButtonEditarGrupo.Enabled = false;
                    }

                    _formPrincipal.FormEditorGrupo.LoadGrupoIntoDataGridView(_grp);
                    _formPrincipal.FormEditorGrupo.Show();
                }
            }
        }

        private void tbNombreGrupo_MouseClick(object sender, MouseEventArgs e)
        {
            tTipPrincipal.Show("Introduzca un nombre en formato: <nombre>-<curso>(letra)", tbNombreGrupo);
        }

        private void tbNombreAsignatura_MouseClick(object sender, MouseEventArgs e)
        {
            tTipPrincipal.Show("La asignatura debe tener entre 4 y 18 caracteres sin espacios", tbNombreAsignatura);
        }

        private void FormCreacionGrupo_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            this.Hide();
            _formPrincipal.ButtonCrearGrupo.Enabled = true;
        }
    }
}
