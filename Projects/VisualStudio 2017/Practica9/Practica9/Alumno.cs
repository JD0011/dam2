﻿/*
* PRÁCTICA.............: Práctica 5.
* NOMBRE Y APELLIDOS...: Juan de Dios Delgado Bermúdez
* CURSO Y GRUPO........: 2º Desarrollo de Interfaces
* TÍTULO DE LA PRÁCTICA: Estructuras de Datos Internas y Manejo de Ficheros.
* FECHA DE ENTREGA.....: 12 de Diciembre de 2017
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica5
{
    [Serializable]
    public class Alumno : IComparable<Alumno>
    {
        #region Variables de clase

        string _strDNI;
        string _strNombre;
        double[] _rngNotas;
        Grupo _grpMiembro;
        
        #endregion Variables de clase

        #region Propiedades

        public string DNI { get => _strDNI; }
        public string Nombre { get => _strNombre; }
        public double[] Notas { get => _rngNotas; }

        #endregion Propiedades

        #region Constructor

        public Alumno(string strDNI, string strNombre,Grupo grpMiembro, double[] rngNotas)
        {
            _strDNI = strDNI;
            _strNombre = strNombre;
            _grpMiembro = grpMiembro;
            _rngNotas = rngNotas;
            
        }

        #endregion Constructor

        #region Métodos

        public double NotaMedia()
        {
            int dbTotal;

            dbTotal = 0;
            foreach(int nota in _rngNotas)
            {
                dbTotal += nota;
            }

            return dbTotal / Convert.ToDouble(_rngNotas.Length);
        }

        public int Suspensos()
        {
            int intSuspensos;

            intSuspensos = 0;
            foreach (int nota in _rngNotas)
            {
                if (nota < 5) { intSuspensos++; }
            }

            return intSuspensos;
        }

        public int CompareTo(Alumno other)
        {
            int intValue;
            if (Char.ToUpper(_strNombre[0]) < Char.ToUpper(other.Nombre[0]))
            {
                intValue = -1;
            }
            else if (other.Nombre ==null)
            {
                intValue = 0;
            }
            else
            {
                intValue = 1;
            }

            return intValue;
        }

        #endregion Métodos
    }
}