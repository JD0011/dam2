﻿/*
* PRÁCTICA.............: Práctica 9.
* NOMBRE Y APELLIDOS...: Juan de Dios Delgado Bermúdez
* CURSO Y GRUPO........: 2º Desarrollo de Interfaces
* TÍTULO DE LA PRÁCTICA: Aplicaciones de Formulario II. Más Controles.
* FECHA DE ENTREGA.....: 24 de Enero de 2018
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica9
{
    class Asignatura
    {
        private string _strNombre;
        private double _dblNota;

        public string Nombre { get => _strNombre; set => _strNombre = value; }
        public double Nota { get => _dblNota; set => _dblNota = value; }

        public Asignatura(string strNombre, double dblNota)
        {
            _strNombre = strNombre;
            _dblNota = dblNota;
        }
    }
}
