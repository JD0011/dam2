﻿namespace Practica9
{
    partial class FormEditorGrupo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormEditorGrupo));
            this.cboxAprobados = new System.Windows.Forms.CheckBox();
            this.btnBorrarAlumno = new System.Windows.Forms.Button();
            this.btnNuevaFila = new System.Windows.Forms.Button();
            this.dgvGrupos = new System.Windows.Forms.DataGridView();
            this.lblValorGrupoCargado = new System.Windows.Forms.Label();
            this.lblGrupoCargado = new System.Windows.Forms.Label();
            this.btnCargarGrupo = new System.Windows.Forms.Button();
            this.btnGuardarGrupo = new System.Windows.Forms.Button();
            this.tTipEditor = new System.Windows.Forms.ToolTip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dgvGrupos)).BeginInit();
            this.SuspendLayout();
            // 
            // cboxAprobados
            // 
            this.cboxAprobados.AutoSize = true;
            this.cboxAprobados.Location = new System.Drawing.Point(12, 215);
            this.cboxAprobados.Name = "cboxAprobados";
            this.cboxAprobados.Size = new System.Drawing.Size(77, 17);
            this.cboxAprobados.TabIndex = 19;
            this.cboxAprobados.Text = "Aprobados";
            this.tTipEditor.SetToolTip(this.cboxAprobados, "Muestra únicamente los lumnos aprobados");
            this.cboxAprobados.UseVisualStyleBackColor = true;
            this.cboxAprobados.CheckedChanged += new System.EventHandler(this.cboxAprobados_CheckedChanged);
            // 
            // btnBorrarAlumno
            // 
            this.btnBorrarAlumno.Enabled = false;
            this.btnBorrarAlumno.Location = new System.Drawing.Point(551, 12);
            this.btnBorrarAlumno.Name = "btnBorrarAlumno";
            this.btnBorrarAlumno.Size = new System.Drawing.Size(23, 23);
            this.btnBorrarAlumno.TabIndex = 18;
            this.btnBorrarAlumno.Text = "-";
            this.tTipEditor.SetToolTip(this.btnBorrarAlumno, "Elimina una fila");
            this.btnBorrarAlumno.UseVisualStyleBackColor = true;
            this.btnBorrarAlumno.Click += new System.EventHandler(this.btnBorrarAlumno_Click);
            // 
            // btnNuevaFila
            // 
            this.btnNuevaFila.AutoSize = true;
            this.btnNuevaFila.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnNuevaFila.Enabled = false;
            this.btnNuevaFila.Location = new System.Drawing.Point(522, 12);
            this.btnNuevaFila.Name = "btnNuevaFila";
            this.btnNuevaFila.Size = new System.Drawing.Size(23, 23);
            this.btnNuevaFila.TabIndex = 17;
            this.btnNuevaFila.Text = "+";
            this.tTipEditor.SetToolTip(this.btnNuevaFila, "Añade una nueva fila");
            this.btnNuevaFila.UseVisualStyleBackColor = true;
            this.btnNuevaFila.Click += new System.EventHandler(this.btnNuevaFila_Click);
            // 
            // dgvGrupos
            // 
            this.dgvGrupos.AllowUserToAddRows = false;
            this.dgvGrupos.AllowUserToDeleteRows = false;
            this.dgvGrupos.AllowUserToResizeColumns = false;
            this.dgvGrupos.AllowUserToResizeRows = false;
            this.dgvGrupos.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvGrupos.ColumnHeadersHeight = 24;
            this.dgvGrupos.Location = new System.Drawing.Point(12, 41);
            this.dgvGrupos.MultiSelect = false;
            this.dgvGrupos.Name = "dgvGrupos";
            this.dgvGrupos.RowHeadersWidth = 50;
            this.dgvGrupos.Size = new System.Drawing.Size(562, 165);
            this.dgvGrupos.TabIndex = 16;
            this.dgvGrupos.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvGrupos_CellClick);
            this.dgvGrupos.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvGrupos_CellEndEdit);
            this.dgvGrupos.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvGrupos_CellEnter);
            this.dgvGrupos.Resize += new System.EventHandler(this.dgvGrupos_Resize);
            // 
            // lblValorGrupoCargado
            // 
            this.lblValorGrupoCargado.AutoSize = true;
            this.lblValorGrupoCargado.Location = new System.Drawing.Point(339, 217);
            this.lblValorGrupoCargado.Name = "lblValorGrupoCargado";
            this.lblValorGrupoCargado.Size = new System.Drawing.Size(47, 13);
            this.lblValorGrupoCargado.TabIndex = 15;
            this.lblValorGrupoCargado.Text = "Ninguno";
            this.lblValorGrupoCargado.Visible = false;
            // 
            // lblGrupoCargado
            // 
            this.lblGrupoCargado.AutoSize = true;
            this.lblGrupoCargado.Location = new System.Drawing.Point(252, 217);
            this.lblGrupoCargado.Name = "lblGrupoCargado";
            this.lblGrupoCargado.Size = new System.Drawing.Size(81, 13);
            this.lblGrupoCargado.TabIndex = 14;
            this.lblGrupoCargado.Text = "Grupo cargado:";
            this.lblGrupoCargado.Visible = false;
            // 
            // btnCargarGrupo
            // 
            this.btnCargarGrupo.AutoSize = true;
            this.btnCargarGrupo.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnCargarGrupo.Location = new System.Drawing.Point(13, 12);
            this.btnCargarGrupo.Name = "btnCargarGrupo";
            this.btnCargarGrupo.Size = new System.Drawing.Size(78, 23);
            this.btnCargarGrupo.TabIndex = 13;
            this.btnCargarGrupo.Text = "Cargar grupo";
            this.tTipEditor.SetToolTip(this.btnCargarGrupo, "Carga un grupo mediante el Asistente");
            this.btnCargarGrupo.UseVisualStyleBackColor = true;
            this.btnCargarGrupo.Click += new System.EventHandler(this.btnCargarGrupo_Click);
            // 
            // btnGuardarGrupo
            // 
            this.btnGuardarGrupo.AutoSize = true;
            this.btnGuardarGrupo.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnGuardarGrupo.Location = new System.Drawing.Point(489, 212);
            this.btnGuardarGrupo.Name = "btnGuardarGrupo";
            this.btnGuardarGrupo.Size = new System.Drawing.Size(85, 23);
            this.btnGuardarGrupo.TabIndex = 12;
            this.btnGuardarGrupo.Text = "Guardar grupo";
            this.tTipEditor.SetToolTip(this.btnGuardarGrupo, "Guarda el grupo cargado");
            this.btnGuardarGrupo.UseVisualStyleBackColor = true;
            this.btnGuardarGrupo.Click += new System.EventHandler(this.btnGuardarGrupo_Click);
            // 
            // FormEditorGrupo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(586, 246);
            this.Controls.Add(this.cboxAprobados);
            this.Controls.Add(this.btnBorrarAlumno);
            this.Controls.Add(this.btnNuevaFila);
            this.Controls.Add(this.dgvGrupos);
            this.Controls.Add(this.lblValorGrupoCargado);
            this.Controls.Add(this.lblGrupoCargado);
            this.Controls.Add(this.btnCargarGrupo);
            this.Controls.Add(this.btnGuardarGrupo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormEditorGrupo";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Editor de grupos";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormEditorGrupo_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.dgvGrupos)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox cboxAprobados;
        private System.Windows.Forms.Button btnBorrarAlumno;
        private System.Windows.Forms.Button btnNuevaFila;
        private System.Windows.Forms.DataGridView dgvGrupos;
        private System.Windows.Forms.Label lblValorGrupoCargado;
        private System.Windows.Forms.Label lblGrupoCargado;
        private System.Windows.Forms.Button btnCargarGrupo;
        private System.Windows.Forms.Button btnGuardarGrupo;
        private System.Windows.Forms.ToolTip tTipEditor;
    }
}