﻿/*
* PRÁCTICA.............: Práctica 9.
* NOMBRE Y APELLIDOS...: Juan de Dios Delgado Bermúdez
* CURSO Y GRUPO........: 2º Desarrollo de Interfaces
* TÍTULO DE LA PRÁCTICA: Aplicaciones de Formulario II. Más Controles.
* FECHA DE ENTREGA.....: 24 de Enero de 2018
*/

using Practica5;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Practica9
{
    public partial class FormEditorGrupo : Form
    {
        #region Variables

        //Grupo cargado
        private Grupo _grp;
        private FormPrincipal _formPrincipal;
        private DataGridViewCell _dgvCurrentCell;
        private bool isAprobadosFile;

        #region Patrones

        private const string patternNombreGrupo = "^[a-zA-ZñÑáéíóúÁÉÍÓÚ]{3,15}-[0-9]{1}[a-zA-Z]{0,1}$";
        private const string patternNombreAlumno = "^[a-zA-ZñÑáéíóúÁÉÍÓÚ ]{2,20}$";
        private const string patternAsignatura = "^[a-zA-ZñÑáéíóúÁÉÍÓÚ]{4,18}$";
        private const string pathAsignaturas = "pathAsignaturas.txt";

        #endregion Patrones

        #endregion Variables

        public FormEditorGrupo(FormPrincipal formPrincipal)
        {
            _formPrincipal = formPrincipal;
            InitializeComponent();
        }
        private bool LoadGrupo()
        {
            //Desactiva check en cada carga
            cboxAprobados.Checked = false;

            bool fileChoosen;
            OpenFileDialog ofd = new OpenFileDialog();

            ofd.Filter = "Grupo |*.gru";
            ofd.Title = "Seleccione el grupo a inspeccionar";

            if ((fileChoosen = (ofd.ShowDialog() == DialogResult.OK)))
            {
                try
                {
                    //Lee el archivo
                    FileStream stream = (FileStream)ofd.OpenFile();
                    BinaryFormatter formatter = new BinaryFormatter();

                    _grp = (Grupo)formatter.Deserialize(stream);
                    stream.Close();
                }
                catch (SerializationException)
                {
                    fileChoosen = false;

                    MessageBox.Show(
                        "El archivo seleccionado está corrupto o no es compatible con la aplicación",
                        "Archivo incorrecto",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error
                        );
                }
            }

            if ((isAprobadosFile = ofd.FileName.Contains("-aprobados"))){
                cboxAprobados.Checked = true;
                cboxAprobados.Enabled = false;
                dgvGrupos.ReadOnly = true;
            }
            else
            {
                dgvGrupos.ReadOnly = false;
                cboxAprobados.Enabled = true;
                cboxAprobados.Checked = false;
            }
                

            return fileChoosen;
        }

        public void LoadGrupoIntoDataGridView(Grupo grp)
        {
            _grp = grp;

            if (dgvGrupos.Columns.Count > 0)
            {
                dgvGrupos.Columns.Clear();
            }

            
            lblValorGrupoCargado.Text = grp.Nombre;
            lblValorGrupoCargado.Visible = true;
            lblGrupoCargado.Visible = true;

            dgvGrupos.Columns.Add("DNI", "DNI");
            dgvGrupos.Columns.Add("Nombre", "Nombre");

            //Agrega las columnas de asignaturas
            foreach (string s in grp.CodAsignatura)
            {
                DataGridViewTextBoxColumn dgvTBColumnAsignatura = new DataGridViewTextBoxColumn();
                dgvTBColumnAsignatura.HeaderText = s;
                dgvGrupos.Columns.Add(dgvTBColumnAsignatura);
            }

            //Establece tamaño en función de columnas
            if (grp.NumAsignaturas < 4) {
                dgvGrupos.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            }
            else
            {
                dgvGrupos.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.None;
            }

            LoadAlumnosFromGrupo(grp);
        }

        private void LoadAlumnosFromGrupo(Grupo grp)
        {
            int numAlumnos = grp.Alumnos.Count;
            if (numAlumnos == 0)
            {
                //Registro en blanco
                dgvGrupos.Rows.Add(new DataGridViewRow());
            }
            else
            {
                foreach (Alumno a in grp.Alumnos)
                {
                    //Coge la fila recién creada
                    DataGridViewRow row = dgvGrupos.Rows[dgvGrupos.Rows.Add()];

                    row.Cells[0].Value = a.DNI;
                    row.Cells[1].Value = a.Nombre;

                    for (int i = 2; i < grp.NumAsignaturas + 2; i++)
                    {
                        row.Cells[i].Value = a.Notas[i - 2];
                    }
                }

                EnableEditingComponents();
            }
            
        }

        private void SaveAlumnosIntoGrupo()
        {
            _grp.Alumnos.Clear();

            double[] notas = new double[_grp.NumAsignaturas];
            foreach (DataGridViewRow row in dgvGrupos.Rows)
            {
                if (row.Visible && !CheckNullCells(row.Index))
                {
                    for (int i = 2; i < row.Cells.Count; i++)
                    {
                        notas[i - 2] = double.Parse(row.Cells[i].Value.ToString());
                    }

                    string strDNI = row.Cells[0].Value.ToString();
                    string strNombre = row.Cells[1].Value.ToString();

                    _grp.Alumnos.Add(new Alumno(strDNI, strNombre, _grp, notas));
                }
            }
        }

        private bool SaveGrupo()
        {
            bool isSaved;

            SaveFileDialog sfd = new SaveFileDialog();

            sfd.DefaultExt = ".gru";
            sfd.FileName = cboxAprobados.Checked ? _grp.Nombre + "-aprobados" : _grp.Nombre;
            sfd.Title = "Seleccione la ruta para guardar el archivo";
            sfd.Filter = "Grupo | *.gru";

            //Procesa la opción
            if (isSaved = sfd.ShowDialog() == DialogResult.OK)
            {
                //Guarda el archivo
                FileStream stream = (FileStream)sfd.OpenFile();
                BinaryFormatter formatter = new BinaryFormatter();

                formatter.Serialize(stream, _grp);
                stream.Close();
                isSaved = true;
            }

            return isSaved;
        }

        private void ShowAprobados()
        {
            foreach (DataGridViewRow row in dgvGrupos.Rows)
            {
                bool pass = true;
                for (int i = 2; i < row.Cells.Count; i++)
                {
                    try
                    {
                        if (double.Parse(Convert.ToString(row.Cells[i].Value)) < 5)
                        {
                            pass = false;
                            break;
                        }
                    }
                    catch (Exception)
                    {
                        pass = false;
                    }
                }

                row.Visible = pass;
            }
        }

        private void UnhideAlumnos()
        {
            foreach (DataGridViewRow row in dgvGrupos.Rows)
            {
                row.Visible = true;
            }
        }

        private void EnableEditingComponents()
        {
            btnNuevaFila.Enabled = true;
            dgvGrupos.Enabled = true;
        }

        private void DisableEditingComponents()
        {
            btnNuevaFila.Enabled = false;
            btnBorrarAlumno.Enabled = false;
            dgvGrupos.Enabled = false;
        }

        private bool CheckNullCells(int rowIndex)
        {
            bool someCellIsNull = false;
            foreach (DataGridViewCell cell in dgvGrupos.Rows[rowIndex].Cells)
            {
                if (cell.Value == null)
                {
                    someCellIsNull = true;
                    break;
                }
            }

            return someCellIsNull;
        }

        private bool isRepeatedDNI(DataGridViewRow rowDNI)
        {
            bool isRepeated = false;
            foreach(DataGridViewRow row in dgvGrupos.Rows)
            {
                if (row.Index != rowDNI.Index)
                {
                    if (row.Cells[0].Value.ToString().Equals(rowDNI.Cells[0].Value.ToString()))
                    {
                        isRepeated = true;
                    }
                }
            }

            return isRepeated;
        }
        private bool CompruebaDNI(string text)
        {
            bool letraOK;
            try
            {
                char[] letrasNIF = { 'T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D', 'X', 'B','N','J','Z','S','Q','V','H'
                ,'L','C','K','E'};
                int indexLetra;

                string strNumero = "";
                foreach (char c in text.Substring(0, text.Length - 1))
                {
                    strNumero += c;
                }

                indexLetra = int.Parse(strNumero) % 23;

                letraOK = Char.ToUpper(text[8]).Equals(letrasNIF[indexLetra]);
            }
            catch (Exception)
            {
                letraOK = false;
            }

            return letraOK;
        }

        private void cboxAprobados_CheckedChanged(object sender, EventArgs e)
        {
            if (cboxAprobados.Checked)
            {
                ShowAprobados();
            }
            else
            {
                UnhideAlumnos();
            }
        }

        private void btnBorrarAlumno_Click(object sender, EventArgs e)
        {
            if (dgvGrupos.Rows.Count != 0)
            {
                dgvGrupos.Rows.Remove(dgvGrupos.CurrentRow);
            }
        }

        private void dgvGrupos_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            btnBorrarAlumno.Enabled = true;
        }

        private void dgvGrupos_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewColumn column = dgvGrupos.Columns[e.ColumnIndex];

            bool isOkey = true;
            //Valida los datos en función de la columna
            switch (column.HeaderText)
            {
                case "DNI":
                    string text = Convert.ToString(dgvGrupos[e.ColumnIndex, e.RowIndex].Value);
                    if (!CompruebaDNI(Convert.ToString(dgvGrupos[e.ColumnIndex, e.RowIndex].Value)))
                    {
                        MessageBox.Show(
                            "El DNI no tiene formato correcto o la letra no es correcta. " +
                            "Ej: 12345678S",
                            "Error en alumno",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Warning
                        );
                        isOkey = false;
                    }
                    else
                    {
                        if (isRepeatedDNI(dgvGrupos.Rows[e.RowIndex]))
                        {
                            MessageBox.Show(
                                "El DNI no puede estar repetido. " +
                                "Duplicidad de datos",
                                "Error en alumno",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Warning
                            );
                            isOkey = false;
                        }
                    }
                    break;
                case "Nombre":
                    Regex rx = new Regex(patternNombreAlumno);
                    if (dgvGrupos.CurrentCell.Value != null && !rx.IsMatch(dgvGrupos.CurrentCell.Value.ToString()))
                    {
                        MessageBox.Show(
                            "El nombre debe estar entre 2 y 20 caracteres y formado" +
                            " únicamente por letras",
                            "Error en alumno",
                            MessageBoxButtons.OK, MessageBoxIcon.Warning);

                        dgvGrupos.CurrentCell.Value = dgvGrupos[e.ColumnIndex, e.RowIndex];
                        isOkey = false;
                    }
                    break;

                //Asignaturas
                default:
                    if (dgvGrupos.CurrentCell.Value != null)
                    {
                        double nota = 0;
                        try
                        {
                            nota = double.Parse(Convert.ToString(dgvGrupos[e.ColumnIndex, e.RowIndex].Value));
                        }
                        catch (Exception)
                        {
                            isOkey = false;
                        }

                        if (isOkey)
                        {
                            if (!(nota > 0 && nota < 10))
                            {
                                isOkey = false;
                            }
                        }

                        if (!isOkey)
                        {
                            MessageBox.Show(
                            "La nota debe ser un número entero entre 0 y 10",
                            "Error en nota de "+column.HeaderText,
                            MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }
                        else
                        {
                            //Redondea
                            dgvGrupos[e.ColumnIndex, e.RowIndex].Value = Math.Round(nota, 2);
                        }
                    }
                    break;
            }

            if (!isOkey)
            {
                dgvGrupos[e.ColumnIndex, e.RowIndex].Value = "";
            }

            btnNuevaFila.Enabled = !CheckNullCells(e.RowIndex);
        }

        private void btnCargarGrupo_Click(object sender, EventArgs e)
        {
            if (LoadGrupo())
            {
                LoadGrupoIntoDataGridView(_grp);
            }
        }

        private void btnGuardarGrupo_Click(object sender, EventArgs e)
        {
            if (_grp == null)
            {
                MessageBox.Show(
                    "No hay datos para guardar",
                    "Aviso",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Warning
                    );
            }
            else
            {
                bool hasNullCells = false;
                for (int i = 0; i < dgvGrupos.RowCount; i++)
                {
                    if (CheckNullCells(i))
                    {
                        hasNullCells = true;
                        break;
                    }
                }

                //Puede guardar directamente si son aprobados
                if (isAprobadosFile)
                {
                    //Limpia alumnos cargados
                    _grp.Alumnos.Clear();
                    SaveAlumnosIntoGrupo();
                    SaveGrupo();
                }
                else if (dgvGrupos.Rows.Count != 0 && !hasNullCells)
                {
                    //Limpia alumnos cargados
                    _grp.Alumnos.Clear();
                    SaveAlumnosIntoGrupo();
                    SaveGrupo();
                }
                else
                {
                    MessageBox.Show(
                        "No puede haber celdas vacías, corrija los datos.",
                        "Error en las celdas",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error
                        );
                }
            }
            
        }

        private void btnNuevaFila_Click(object sender, EventArgs e)
        {
            dgvGrupos.Rows.Add(new DataGridViewRow());
            btnNuevaFila.Enabled = false;
        }

        private void dgvGrupos_Resize(object sender, EventArgs e)
        {
            this.Size = new Size(this.Size.Height,900);
        }

        private void FormEditorGrupo_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            if (_grp!=null)
            {
                DialogResult result = MessageBox.Show(this, "¿Está seguro de que desea cerrar el editor?", "Atención", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (result == DialogResult.Yes)
                {
                    this.Hide();
                    _formPrincipal.ButtonEditarGrupo.Enabled = true;
                }

            }
            else
            {
                this.Hide();
                _formPrincipal.ButtonEditarGrupo.Enabled = true;
            }
        }

        private void dgvGrupos_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            _dgvCurrentCell = dgvGrupos[e.ColumnIndex, e.RowIndex];
        }
    }
}
