﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Practica9
{
    public partial class FormPrincipal : Form
    {
        private FormEditorGrupo _formEditorGRP;
        private FormCreacionGrupo _formCreadorGrp;

        public Button ButtonCrearGrupo { get => this.btnCrearGrupo; }
        public Button ButtonEditarGrupo { get => this.btnEditarGrupo; }
        public FormEditorGrupo FormEditorGrupo { get => _formEditorGRP; set => _formEditorGRP = value; }
        public FormCreacionGrupo FormCreacionGrupo { get => _formCreadorGrp; }

        public FormPrincipal()
        {
            InitializeComponent();
        }

        private void CloseIfPossible()
        {
            bool shouldClose = true;
            bool formEditorIsActive = _formEditorGRP != null && _formEditorGRP.Visible;
            bool formCreadorIsActive = _formCreadorGrp != null && _formCreadorGrp.Visible;
            if(formEditorIsActive && formCreadorIsActive)
            {
                MessageBox.Show(
                       "Cierre el editor y el creador de grupos primero",
                       "Información",
                       MessageBoxButtons.OK,
                       MessageBoxIcon.Information
                       );

                shouldClose = false;
            }
            else
            {
                if (formEditorIsActive)
                {
                    MessageBox.Show(
                        "Cierre el editor de grupos primero",
                        "Información",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Information
                        );

                    shouldClose = false;
                }
                else if (formCreadorIsActive)
                {
                    MessageBox.Show(
                        "Cierre el creador de grupos primero",
                        "Información",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Information
                        );

                    shouldClose = false;
                }
            }

            if (shouldClose)
            {
                Environment.Exit(0);
            }
        }

        private void btnCrearGrupo_Click(object sender, EventArgs e)
        {
            _formCreadorGrp = new FormCreacionGrupo(this);

            _formCreadorGrp.Show();
            btnCrearGrupo.Enabled = false;
        }

        private void btnEditarGrupo_Click(object sender, EventArgs e)
        {
            _formEditorGRP = new FormEditorGrupo(this);

            _formEditorGRP.Show();
            btnEditarGrupo.Enabled = false;
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            CloseIfPossible();
        }

        private void FormPrincipal_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            CloseIfPossible();
        }
    }
}
