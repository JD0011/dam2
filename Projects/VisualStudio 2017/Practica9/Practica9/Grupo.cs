﻿/*
* PRÁCTICA.............: Práctica 9.
* NOMBRE Y APELLIDOS...: Juan de Dios Delgado Bermúdez
* CURSO Y GRUPO........: 2º Desarrollo de Interfaces
* TÍTULO DE LA PRÁCTICA: Aplicaciones de Formulario II. Más Controles.
* FECHA DE ENTREGA.....: 24 de Enero de 2018
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica5
{
    [Serializable]
    public class Grupo : IComparable<Grupo>
    {
        #region Variables de clase

        string _strNombre;
        string[] _rngCodAsignaturas;
        List<Alumno> _listAlumnos;

        #endregion Variables de clase

        #region Propiedades

        public string Nombre { get => _strNombre; }
        public int NumAsignaturas { get => _rngCodAsignaturas.Length; }
        public string[] CodAsignatura { get => _rngCodAsignaturas; }
        public int NumAlumnos { get => _listAlumnos != null ? _listAlumnos.Count : 0; }
        public List<Alumno> Alumnos { get => _listAlumnos; }

        #endregion Propiedades

        #region Constructor

        public Grupo(string strNombre, string[] rngCodAsignaturas)
        {
            _strNombre = strNombre;
            _rngCodAsignaturas = rngCodAsignaturas;

            _listAlumnos = new List<Alumno>();
        }

        

        #endregion Constructor

        #region Métodos

        public void AddAlumno(Alumno a)
        {
            _listAlumnos.Add(a);
        }

        public double MediaGrupo(int intCodAsignatura)
        {
            double dblTotalNota;

            dblTotalNota = 0;
            foreach (Alumno a in _listAlumnos)
            {
                dblTotalNota+=a.Notas[intCodAsignatura];
            }

            return dblTotalNota / Convert.ToDouble(_listAlumnos.Count);
        }

        public int CompareTo(Grupo other)
        {
            int intValue;
            if (Char.ToUpper(_strNombre[0]) < Char.ToUpper(other.Nombre[0]))
            {
                intValue = 1;
            }
            else if (other.Nombre == null)
            {
                intValue = 0;
            }
            else
            {
                intValue = -1;
            }

            return intValue;
        }

        #endregion Métodos
    }
}
