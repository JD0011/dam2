﻿namespace Practica9
{
    partial class FormCreacionGrupo
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormCreacionGrupo));
            this.tcGestorGrupos = new System.Windows.Forms.TabControl();
            this.tabNuevoGrupo = new System.Windows.Forms.TabPage();
            this.label2 = new System.Windows.Forms.Label();
            this.clbAsignaturas = new System.Windows.Forms.CheckedListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tbNombreGrupo = new System.Windows.Forms.TextBox();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.tabGestorAsignaturas = new System.Windows.Forms.TabPage();
            this.lblHeader = new System.Windows.Forms.Label();
            this.tbNombreAsignatura = new System.Windows.Forms.TextBox();
            this.btnAgregar = new System.Windows.Forms.Button();
            this.btnEliminar = new System.Windows.Forms.Button();
            this.lbAsignaturas = new System.Windows.Forms.ListBox();
            this.bsAsignaturas = new System.Windows.Forms.BindingSource(this.components);
            this.tTipPrincipal = new System.Windows.Forms.ToolTip(this.components);
            this.tcGestorGrupos.SuspendLayout();
            this.tabNuevoGrupo.SuspendLayout();
            this.tabGestorAsignaturas.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bsAsignaturas)).BeginInit();
            this.SuspendLayout();
            // 
            // tcGestorGrupos
            // 
            this.tcGestorGrupos.Controls.Add(this.tabNuevoGrupo);
            this.tcGestorGrupos.Controls.Add(this.tabGestorAsignaturas);
            this.tcGestorGrupos.Location = new System.Drawing.Point(12, 12);
            this.tcGestorGrupos.Name = "tcGestorGrupos";
            this.tcGestorGrupos.SelectedIndex = 0;
            this.tcGestorGrupos.Size = new System.Drawing.Size(401, 289);
            this.tcGestorGrupos.TabIndex = 0;
            // 
            // tabNuevoGrupo
            // 
            this.tabNuevoGrupo.Controls.Add(this.label2);
            this.tabNuevoGrupo.Controls.Add(this.clbAsignaturas);
            this.tabNuevoGrupo.Controls.Add(this.label1);
            this.tabNuevoGrupo.Controls.Add(this.tbNombreGrupo);
            this.tabNuevoGrupo.Controls.Add(this.btnGuardar);
            this.tabNuevoGrupo.Location = new System.Drawing.Point(4, 22);
            this.tabNuevoGrupo.Name = "tabNuevoGrupo";
            this.tabNuevoGrupo.Padding = new System.Windows.Forms.Padding(3);
            this.tabNuevoGrupo.Size = new System.Drawing.Size(393, 263);
            this.tabNuevoGrupo.TabIndex = 0;
            this.tabNuevoGrupo.Text = "Nuevo grupo";
            this.tabNuevoGrupo.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 43);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(293, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Seleccione entre 4 y 7 asignturas (Ver gestor de asignaturas)";
            // 
            // clbAsignaturas
            // 
            this.clbAsignaturas.CheckOnClick = true;
            this.clbAsignaturas.FormattingEnabled = true;
            this.clbAsignaturas.Location = new System.Drawing.Point(6, 59);
            this.clbAsignaturas.Name = "clbAsignaturas";
            this.clbAsignaturas.Size = new System.Drawing.Size(368, 139);
            this.clbAsignaturas.TabIndex = 6;
            this.clbAsignaturas.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.clbAsignaturas_ItemCheck);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Nombre de grupo";
            // 
            // tbNombreGrupo
            // 
            this.tbNombreGrupo.Location = new System.Drawing.Point(101, 11);
            this.tbNombreGrupo.Name = "tbNombreGrupo";
            this.tbNombreGrupo.Size = new System.Drawing.Size(100, 20);
            this.tbNombreGrupo.TabIndex = 4;
            this.tTipPrincipal.SetToolTip(this.tbNombreGrupo, "Introduzca un nombre en formato: <nombre>-<curso>(letra)");
            this.tbNombreGrupo.MouseClick += new System.Windows.Forms.MouseEventHandler(this.tbNombreGrupo_MouseClick);
            this.tbNombreGrupo.TextChanged += new System.EventHandler(this.tbNombreGrupo_TextChanged);
            this.tbNombreGrupo.Leave += new System.EventHandler(this.tbNombreGrupo_Leave);
            // 
            // btnGuardar
            // 
            this.btnGuardar.Location = new System.Drawing.Point(299, 203);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(75, 23);
            this.btnGuardar.TabIndex = 1;
            this.btnGuardar.Text = "Guardar";
            this.btnGuardar.UseVisualStyleBackColor = true;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // tabGestorAsignaturas
            // 
            this.tabGestorAsignaturas.Controls.Add(this.lblHeader);
            this.tabGestorAsignaturas.Controls.Add(this.tbNombreAsignatura);
            this.tabGestorAsignaturas.Controls.Add(this.btnAgregar);
            this.tabGestorAsignaturas.Controls.Add(this.btnEliminar);
            this.tabGestorAsignaturas.Controls.Add(this.lbAsignaturas);
            this.tabGestorAsignaturas.Location = new System.Drawing.Point(4, 22);
            this.tabGestorAsignaturas.Name = "tabGestorAsignaturas";
            this.tabGestorAsignaturas.Size = new System.Drawing.Size(393, 263);
            this.tabGestorAsignaturas.TabIndex = 2;
            this.tabGestorAsignaturas.Text = "Gestor de asignaturas";
            this.tabGestorAsignaturas.UseVisualStyleBackColor = true;
            // 
            // lblHeader
            // 
            this.lblHeader.AutoSize = true;
            this.lblHeader.Location = new System.Drawing.Point(3, 10);
            this.lblHeader.Name = "lblHeader";
            this.lblHeader.Size = new System.Drawing.Size(185, 13);
            this.lblHeader.TabIndex = 5;
            this.lblHeader.Text = "Establezca las asignaturas asignables";
            // 
            // tbNombreAsignatura
            // 
            this.tbNombreAsignatura.Location = new System.Drawing.Point(6, 27);
            this.tbNombreAsignatura.Name = "tbNombreAsignatura";
            this.tbNombreAsignatura.Size = new System.Drawing.Size(121, 20);
            this.tbNombreAsignatura.TabIndex = 4;
            this.tTipPrincipal.SetToolTip(this.tbNombreAsignatura, "La asignatura debe tener entre 4 y 18 caracteres sin espacios");
            this.tbNombreAsignatura.MouseClick += new System.Windows.Forms.MouseEventHandler(this.tbNombreAsignatura_MouseClick);
            this.tbNombreAsignatura.TextChanged += new System.EventHandler(this.tbAsignatura_TextChanged);
            this.tbNombreAsignatura.Leave += new System.EventHandler(this.tbAsignatura_Leave);
            // 
            // btnAgregar
            // 
            this.btnAgregar.Enabled = false;
            this.btnAgregar.Location = new System.Drawing.Point(133, 26);
            this.btnAgregar.Name = "btnAgregar";
            this.btnAgregar.Size = new System.Drawing.Size(75, 23);
            this.btnAgregar.TabIndex = 3;
            this.btnAgregar.Text = "Agregar";
            this.tTipPrincipal.SetToolTip(this.btnAgregar, "Agrege la asignatura escrita");
            this.btnAgregar.UseVisualStyleBackColor = true;
            this.btnAgregar.Click += new System.EventHandler(this.btnAgregar_Click);
            // 
            // btnEliminar
            // 
            this.btnEliminar.Enabled = false;
            this.btnEliminar.Location = new System.Drawing.Point(301, 203);
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Size = new System.Drawing.Size(75, 23);
            this.btnEliminar.TabIndex = 2;
            this.btnEliminar.Text = "Eliminar";
            this.tTipPrincipal.SetToolTip(this.btnEliminar, "Elimine la asignatura seleccionada en la lista");
            this.btnEliminar.UseVisualStyleBackColor = true;
            this.btnEliminar.Click += new System.EventHandler(this.btnEliminar_Click);
            // 
            // lbAsignaturas
            // 
            this.lbAsignaturas.FormattingEnabled = true;
            this.lbAsignaturas.Location = new System.Drawing.Point(6, 63);
            this.lbAsignaturas.Name = "lbAsignaturas";
            this.lbAsignaturas.Size = new System.Drawing.Size(370, 134);
            this.lbAsignaturas.TabIndex = 0;
            this.lbAsignaturas.SelectedValueChanged += new System.EventHandler(this.lbAsignaturas_SelectedValueChanged);
            // 
            // FormCreacionGrupo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(423, 311);
            this.Controls.Add(this.tcGestorGrupos);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormCreacionGrupo";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Gestor de grupos";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormCreacionGrupo_FormClosing);
            this.tcGestorGrupos.ResumeLayout(false);
            this.tabNuevoGrupo.ResumeLayout(false);
            this.tabNuevoGrupo.PerformLayout();
            this.tabGestorAsignaturas.ResumeLayout(false);
            this.tabGestorAsignaturas.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bsAsignaturas)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tcGestorGrupos;
        private System.Windows.Forms.TabPage tabNuevoGrupo;
        private System.Windows.Forms.TabPage tabGestorAsignaturas;
        private System.Windows.Forms.TextBox tbNombreAsignatura;
        private System.Windows.Forms.Button btnAgregar;
        private System.Windows.Forms.Button btnEliminar;
        private System.Windows.Forms.ListBox lbAsignaturas;
        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.BindingSource bsAsignaturas;
        private System.Windows.Forms.Label lblHeader;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckedListBox clbAsignaturas;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbNombreGrupo;
        private System.Windows.Forms.ToolTip tTipPrincipal;
    }
}