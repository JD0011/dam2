﻿/*
* PRÁCTICA.............: Práctica 2.
* NOMBRE y APELLIDOS...: Juan de Dios Delgado Bermúdez
* CURSO y GRUPO........: 2o Desarrollo de Interfaces
* TÍTULO de la PRÁCTICA: Definición de Clases. Uso de Métodos.
* FECHA de ENTREGA.....: 2 de Noviembre de 2017
*/

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Practica2
{
    class Program
    {
        static void Main(string[] args)
        {
            int intLinea;
            Empleado empEmpleadoNom;
            Nomina nmnNomPrincipal;
            String strAuxFecha;
            bool blEsCorrecto;

            empEmpleadoNom = new Empleado();

            Console.WriteLine("Introduce los datos del empleado ...");

            #region Nombre

            //Almacena posición actual
            intLinea = Console.CursorTop;
            do
            {
                //Devuelve el cursor a la posición especificada, limpiando linea
                LimpiarLinea(intLinea);

                Console.Write("\tNombre: ");

                empEmpleadoNom.Nombre = Console.ReadLine();

                //Bloque de control de formato
                if(!(blEsCorrecto = FormatoCorrecto("^[a-zA-Z ]{2,32}$", empEmpleadoNom.Nombre)))
                {
                    PrintError(intLinea, "Debe introducir un nombre entre 2 y 32 caracteres inclusive (solo letras)");
                    continue;
                }

            } while (!blEsCorrecto);

            #endregion Nombre

            #region NIF

            intLinea = Console.CursorTop;
            do
            {
                LimpiarLinea(intLinea);

                Console.Write("\tNIF: ");

                empEmpleadoNom.Nif = Console.ReadLine();
                
                if (!(blEsCorrecto = FormatoCorrecto("^[0-9]{8}[a-zA-Z]{1}$", empEmpleadoNom.Nif)))
                {
                    PrintError(intLinea, "Debe introducir un NIF con 8 números y 1 letra");
                    continue;
                }
                
            } while (!blEsCorrecto);

            #endregion NIF

            #region Categoria

            intLinea = Console.CursorTop;
            blEsCorrecto = false;
            do
            {
                LimpiarLinea(intLinea);

                Console.Write("\tCategoría: ");

                try
                {
                    empEmpleadoNom.Categoria = Convert.ToInt32(Console.ReadLine());
                }
                catch (FormatException)
                {
                    PrintError(intLinea, "Debe introducir una cantidad numérica (de 1 a 3)");
                    continue;
                }
                catch (OverflowException)
                {
                    PrintError(intLinea, "Número demasiado alto (permitido de 1 a 3)");
                    continue;
                }

                if (!(blEsCorrecto = FormatoCorrecto("^[1-3]{1}$", empEmpleadoNom.Categoria)))
                {
                    PrintError(intLinea, "Debe introducir una categoría existente (de 1 a 3)");
                    continue;
                }

            } while (!blEsCorrecto);

            #endregion Categoria

            #region NoTrienios

            intLinea = Console.CursorTop;
            blEsCorrecto = false;
            do
            {
                LimpiarLinea(intLinea);

                Console.Write("\tNº de trienios: ");

                try
                {
                    empEmpleadoNom.NumTrienios = Convert.ToInt32(Console.ReadLine());
                }
                catch (FormatException)
                {
                    PrintError(intLinea, "Debe introducir una cifra de trienios razonable");
                    continue;
                }
                catch (OverflowException)
                {
                    PrintError(intLinea, "Número demasiado alto");
                    continue;
                }

                if (!(blEsCorrecto = FormatoCorrecto("[0-9]{1,2}", empEmpleadoNom.NumTrienios)))
                {
                    PrintError(intLinea, "Debe introducir una cifra de dos dígitos");
                    continue;
                }

                //Bloque de control de correctitud del dato
                if(empEmpleadoNom.NumTrienios < 0 || empEmpleadoNom.NumTrienios > 12)
                {
                    empEmpleadoNom.NumTrienios = empEmpleadoNom.NumTrienios < 0 ? 0 : 12;
                    PrintAdvertencia(
                        "Valores permitidos: de 0 a 12. Establecido en "+ empEmpleadoNom.NumTrienios);
                }

            } while (!blEsCorrecto);

            #endregion NoTrienios

            #region NoHijos

            intLinea = Console.CursorTop;
            blEsCorrecto = false;
            do
            {
                LimpiarLinea(intLinea);

                Console.Write("\tNº de hijos: ");

                try
                {
                    empEmpleadoNom.NumHijos = Convert.ToInt32(Console.ReadLine());
                    blEsCorrecto = true;
                }
                catch (FormatException)
                {
                    PrintError(intLinea, "Debe introducir una cantidad numérica");
                    continue;
                }
                catch (OverflowException)
                {
                    PrintError(intLinea, "Número demasiado alto");
                    continue;
                }

                if (empEmpleadoNom.NumHijos < 0 || empEmpleadoNom.NumHijos > 10)
                {
                    empEmpleadoNom.NumHijos = empEmpleadoNom.NumHijos < 0 ? 0 : 10;
                    PrintAdvertencia(
                        "Valores permitidos: de 0 a 9. Establecido en " + empEmpleadoNom.NumHijos);
                }
                
            } while (!blEsCorrecto);

            #endregion NoHijos

            Console.WriteLine("\nIntroduzca la siguiente información con respecto a la nómina");
            nmnNomPrincipal = new Nomina(empEmpleadoNom);

            #region FechLiquidacion

            intLinea = Console.CursorTop;
            do
            {
                LimpiarLinea(intLinea);

                Console.Write("\tFecha de liquidación (DD/MM/AAAA): ");

                strAuxFecha = Console.ReadLine();
                
                //Control formato
                if (!(blEsCorrecto = FormatoCorrecto("^[0-9]{2}/[0-9]{2}/[0-9]{4}$", strAuxFecha)))
                {
                    PrintError(intLinea, "Debe introducir una fecha con el formato especificado (DD/MM/AAAA)");
                    continue;
                }

                //Control parsing
                try
                {
                    nmnNomPrincipal.FechNomina = DateTime.Parse(strAuxFecha);
                }
                catch (FormatException)
                {
                    PrintError(intLinea, "La fecha introducida no existe");
                    blEsCorrecto = false;
                }

                //Control de correctitud del dato
                DateTime last = new DateTime(1970, 1, 1);
                if (nmnNomPrincipal.FechNomina < last || nmnNomPrincipal.FechNomina > DateTime.Now.AddYears(1))
                {
                    PrintError(intLinea, "Debe introducir una fecha posterior a 1/1/1970 y anterior a la actual");
                    blEsCorrecto = false;
                    continue;
                }

            } while (!blEsCorrecto);

            #endregion FechLiquidacion

            #region NoHorasExtras

            intLinea = Console.CursorTop;
            blEsCorrecto = false;
            do
            {
                LimpiarLinea(intLinea);

                Console.Write("\tNº de horas extras: ");

                try
                {
                    nmnNomPrincipal.NumHorasExtras = Convert.ToInt32(Console.ReadLine());
                }
                catch(FormatException)
                {
                    PrintError(intLinea, "Debe introducid una cantidad numérica en relación con las horas extras trabajadas");
                    continue;
                }
                catch (OverflowException)
                {
                    PrintError(intLinea, "Número demasiado alto");
                    continue;
                }

                if (!(blEsCorrecto = FormatoCorrecto("^[0-9]{1,2}$", nmnNomPrincipal.NumHorasExtras)))
                {
                    PrintError(intLinea, "Debe introducir una cantidad de horas extras razonable");
                    continue;
                }

            } while (!blEsCorrecto);

            LimpiarLinea(intLinea + 1);

            #endregion NoHorasExtras

            Console.WriteLine("\n\nPulse una tecla para mostrar la hoja salarial ...");
            Console.ReadKey();
            Console.Clear();

            nmnNomPrincipal.MostrarHojaSalarial();

            Console.WriteLine("\n\nPulse una tecla para salir ...");
            Console.ReadKey();
        }
        
        public static void LimpiarLinea(int intLinea)
        {
            Console.SetCursorPosition(0, intLinea);
            Console.Write(new string(' ', Console.WindowWidth));
            Console.SetCursorPosition(0, intLinea);
        }

        //Imprime error en siguiente línea
        public static void PrintError(int intLinea, string strError)
        {
            Console.SetCursorPosition(0, intLinea + 1);
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("\t"+strError);
            Console.ResetColor();
        }

        public static void PrintAdvertencia(string strError)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("\t"+strError);
            Console.ResetColor();
        }

        public static bool FormatoCorrecto(string strFormato, string strInput)
        {
            return new Regex(strFormato).IsMatch(strInput);
        }

        public static bool FormatoCorrecto(string strFormato, int intInput)
        {
            return new Regex(strFormato).IsMatch(Convert.ToString(intInput));
        }
    }
}
