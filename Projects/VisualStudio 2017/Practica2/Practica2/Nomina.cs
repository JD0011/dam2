﻿/*
* PRÁCTICA.............: Práctica 2.
* NOMBRE y APELLIDOS...: Juan de Dios Delgado Bermúdez
* CURSO y GRUPO........: 2o Desarrollo de Interfaces
* TÍTULO de la PRÁCTICA: Definición de Clases. Uso de Métodos.
* FECHA de ENTREGA.....: 2 de Noviembre de 2017
*/

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica2
{
    class Nomina
    {
        #region Variables

        Empleado _empEmpleadoNomina;
        DateTime _dtFechNomina;
        int _intNumHorasExtras;

        #endregion Variables

        #region Propiedades

        public DateTime FechNomina
        {
            get => _dtFechNomina;
            set => _dtFechNomina = value;
        }
        public int NumHorasExtras
        {
            get => _intNumHorasExtras;
            set => _intNumHorasExtras = value;
        }

        #endregion Propiedades

        #region Constructor
        
        public Nomina(Empleado empObjetivo)
        {
            _empEmpleadoNomina = empObjetivo;
        }

        #endregion Constructor

        #region Métodos

        public double BaseCotizacion() => DevengosPagaExtra() + DevengosPagaExtra() / 6;

        public double CotizacionSegDes() => DevengosPagaExtra() * 1.97 / 100;

        public double CotizacionSegSoc() => BaseCotizacion() * 4.51 / 100;

        public double DevengosPagaExtra() => SalarioBase() + ImporteAntiguedad();

        public double ImporteAntiguedad() => _empEmpleadoNomina.NumTrienios * SalarioBase() * 4 / 100;

        public double ImporteHorasExtras() => _intNumHorasExtras * SalarioBase() * 1 / 100;

        public double LiquidoPercibir() => TotalDevengado() - TotalDescuentos();

        public int PorcentajeIRPF()
        {
            int intPorcentaje = 0;

            switch (_empEmpleadoNomina.Categoria)
            {
                case 1: intPorcentaje = 18 - _empEmpleadoNomina.NumHijos; break;
                case 2: intPorcentaje = 15 - _empEmpleadoNomina.NumHijos; break;
                case 3: intPorcentaje = 12 - _empEmpleadoNomina.NumHijos; break;
            }

            return intPorcentaje;
        }

        public double RetencionIRPF() => TotalDevengado() * PorcentajeIRPF() / 100;

        public double SalarioBase()
        {
            double aux = 0;

            switch (_empEmpleadoNomina.Categoria)
            {
                case 1: aux = 2500; break;
                case 2: aux = 2000; break;
                case 3: aux = 1500; break;
            }

            return aux;
        }

        public double TotalDescuentos() => CotizacionSegSoc() + CotizacionSegDes() + RetencionIRPF();

        public double TotalDevengado()
        {
            double dbDevengado;

            if (_dtFechNomina.Month == 6 || _dtFechNomina.Month == 12)
            {
                dbDevengado = SalarioBase() + ImporteAntiguedad() + ImporteHorasExtras() + DevengosPagaExtra();
            }
            else
            {
                dbDevengado = SalarioBase() + ImporteAntiguedad() + ImporteHorasExtras();
            }
            
            return dbDevengado;
        }

        public void MostrarHojaSalarial()
        {
            Console.WriteLine("LIQUIDACIÓN DE HABERES AL "+ _dtFechNomina.ToString("dd/MM/yyyy")+ "\n");

            //CABECERA
            Console.WriteLine(
                "Nombre........:{0}\n"+
                "NIF...........:{1}\n" +
                "Categoría.....:{2}\n" +
                "Nº de trienios:{3}\n" +
                "Nº de Hijos...:{4}\n",
                _empEmpleadoNomina.Nombre,
                _empEmpleadoNomina.Nif,
                _empEmpleadoNomina.Categoria,
                _empEmpleadoNomina.NumTrienios,
                _empEmpleadoNomina.NumHijos);

            //DEVENGOS Y DESCUENTOS
            Console.WriteLine(
                "DEVENGOS \t\t\t\tDESCUENTOS\n" +
                "-------- \t\t\t\t----------");
            
            Console.WriteLine(
                "Salario base    \t{0,9:0.00}   \tCotización Seg. Soc. \t{1,9:0.00}\n" +
                "Antiguedad      \t{2,9:0.00}   \tCotización Seg. Des. \t{3,9:0.00}\n" +
                "Importe Hor.Ext.\t{4,9:0.00}   \tRetención IRPF       \t{5,9:0.00}\n" +
                "Paga Extra      \t{6,9:0.00}\n",
                SalarioBase(),          CotizacionSegSoc(),
                ImporteAntiguedad(),    CotizacionSegDes(),
                ImporteHorasExtras(),   RetencionIRPF(),
                DevengosPagaExtra()
                );

            //TOTALES
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(
                "Total Devengos \t\t{0,9:0.00} \tTotal Descuentos \t{1,9:0.00}\n", 
                TotalDevengado(),TotalDescuentos());
            Console.ResetColor();

            //A PERCIBIR
            Console.WriteLine("-------------------------------");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("LÍQUIDO A PERCIBIR {0:0.00}", LiquidoPercibir());
            Console.ResetColor();
            Console.WriteLine("*******************************");
        }

        #endregion Métodos
    }
}

