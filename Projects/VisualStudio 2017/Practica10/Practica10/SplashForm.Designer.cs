﻿namespace Practica10
{
    partial class SplashForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SplashForm));
            this.pbBackground = new System.Windows.Forms.PictureBox();
            this.tbCargando = new System.Windows.Forms.Label();
            this.pnWhiteBackground = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.pbBackground)).BeginInit();
            this.pnWhiteBackground.SuspendLayout();
            this.SuspendLayout();
            // 
            // pbBackground
            // 
            this.pbBackground.Image = ((System.Drawing.Image)(resources.GetObject("pbBackground.Image")));
            this.pbBackground.Location = new System.Drawing.Point(-25, -1);
            this.pbBackground.Name = "pbBackground";
            this.pbBackground.Size = new System.Drawing.Size(383, 338);
            this.pbBackground.TabIndex = 0;
            this.pbBackground.TabStop = false;
            // 
            // tbCargando
            // 
            this.tbCargando.AutoSize = true;
            this.tbCargando.Location = new System.Drawing.Point(141, 10);
            this.tbCargando.Name = "tbCargando";
            this.tbCargando.Size = new System.Drawing.Size(68, 13);
            this.tbCargando.TabIndex = 1;
            this.tbCargando.Text = "CARGANDO";
            // 
            // pnWhiteBackground
            // 
            this.pnWhiteBackground.Controls.Add(this.tbCargando);
            this.pnWhiteBackground.Location = new System.Drawing.Point(0, 267);
            this.pnWhiteBackground.Name = "pnWhiteBackground";
            this.pnWhiteBackground.Size = new System.Drawing.Size(358, 31);
            this.pnWhiteBackground.TabIndex = 2;
            // 
            // SplashForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(356, 299);
            this.Controls.Add(this.pnWhiteBackground);
            this.Controls.Add(this.pbBackground);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "SplashForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SplashForm";
            this.Shown += new System.EventHandler(this.SplashForm_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.pbBackground)).EndInit();
            this.pnWhiteBackground.ResumeLayout(false);
            this.pnWhiteBackground.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pbBackground;
        private System.Windows.Forms.Label tbCargando;
        private System.Windows.Forms.Panel pnWhiteBackground;
    }
}