﻿/*
* PRÁCTICA.............: Práctica 10.
* NOMBRE Y APELLIDOS...: Juan de Dios Delgado Bermúdez
* CURSO Y GRUPO........: 2o Desarrollo de Interfaces
* TÍTULO DE LA PRÁCTICA: Editor de Textos.
* FECHA DE ENTREGA.....: 31 de Enero de 2018
*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Practica10
{
    public partial class SplashForm : Form
    {
        private const int TiempoVisualizacion = 2500; //ms
        private Timer _tmrVisualizacion;

        public SplashForm()
        {
            InitializeComponent();
        }

        private void SplashForm_Shown(object sender, EventArgs e)
        {
            _tmrVisualizacion = new Timer();
            _tmrVisualizacion.Interval = TiempoVisualizacion;
            _tmrVisualizacion.Start();
            _tmrVisualizacion.Tick += FinishTick;

        }

        private void FinishTick(object sender, EventArgs e)
        {
            _tmrVisualizacion.Stop();
            FormPrincipal fPrincipal = new FormPrincipal();
            fPrincipal.Show();
            this.Hide();
        }
    }
}
