﻿/*
* PRÁCTICA.............: Práctica 10.
* NOMBRE Y APELLIDOS...: Juan de Dios Delgado Bermúdez
* CURSO Y GRUPO........: 2o Desarrollo de Interfaces
* TÍTULO DE LA PRÁCTICA: Editor de Textos.
* FECHA DE ENTREGA.....: 31 de Enero de 2018
*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Practica10
{
    public partial class FormPrincipal : Form
    {
        private const int DefaultFontSize = 3;
        private const int DefaultFontFamily = 21;

        private string _strFilepath;

        public FormPrincipal()
        {
            InitializeComponent();
            InitializeCustomComponents();
            rtbContenido.Focus();
        }

        private void InitializeCustomComponents()
        {
            //Rellena tipos de letra
            foreach (FontFamily font in System.Drawing.FontFamily.Families)
            {
                string strNombre = font.Name;
                cboxFuentes.Items.Add(strNombre);
            }

            //Valores por defecto
            cboxTamLetra.SelectedIndex = DefaultFontSize;

            cboxFuentes.SelectedIndex = DefaultFontFamily;
        }

        private int CuentaPalabras()
        {
            StringBuilder sbContent = new StringBuilder(rtbContenido.Text);
            int index = 0;
            int palabras = 0;
            while(index < sbContent.Length)
            {
                //Comprueba si son caracteres sobre lo que se itera. Cuando encuentra espacio, suma una palabra
                while(index < sbContent.Length && !char.IsWhiteSpace(sbContent[index]))
                {
                    index++;
                }

                palabras++;

                //Recorre el texto hasta pasar los espacios después de la palabra
                while (index < sbContent.Length && char.IsWhiteSpace(sbContent[index]))
                {
                    index++;
                }
            }

            return palabras;
        }

        private void abrirArchivoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (guardarArchivoToolStripMenuItem.Enabled)
            {
                DialogResult result;

                result = MessageBox.Show(
                            "¿Desea guardar el documento actualmente abierto?",
                            "Guarde el documento", 
                            MessageBoxButtons.OKCancel, 
                            MessageBoxIcon.Warning);

                if(result == DialogResult.OK)
                {
                    ShowSaveDialog();
                }
            }

            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "Documento enriquecido (*.rtf)|*.rtf |Archivo de texto (*.txt)|*.txt";

            if (ofd.ShowDialog() == DialogResult.OK)
            {
                _strFilepath = ofd.FileName;

                rtbContenido.Text = "";
                try
                {
                    switch (ofd.FilterIndex)
                    {
                        //txt
                        case 1:
                            StringBuilder content = new StringBuilder();
                            content.Append(File.ReadAllText(ofd.FileName));
                            rtbContenido.Text = content.ToString();
                            break;

                        //rtf
                        case 2:
                            rtbContenido.LoadFile(ofd.FileName);
                            break;
                    }
                }
                catch (Exception)
                {
                    MessageBox.Show("El archivo está corrupto",
                        "Error de lectura",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                }

                //Desactiva guardar, ya que se disparó textChanged al cargar el contenido
                guardarArchivoToolStripMenuItem.Enabled = false;
            }
        }

        private void guardarArchivoComoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ShowSaveDialog();
        }

        private void ShowSaveDialog()
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "Archivo de texto (*.txt)|*.txt|Documento enriquecido (*.rtf)|*.rtf";
            sfd.DefaultExt = "Documento enriquecido | *.rtf";

            if (sfd.ShowDialog() == DialogResult.OK)
            {
                _strFilepath = sfd.FileName;

                switch (sfd.FilterIndex)
                {
                    //txt
                    case 1:
                        File.WriteAllText(sfd.FileName, rtbContenido.Text);
                        break;

                    //rtf
                    case 2:
                        rtbContenido.SaveFile(sfd.FileName);
                        break;
                }

                guardarArchivoToolStripMenuItem.Enabled = false;
            }
        }

        private void rtbContenido_TextChanged(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(rtbContenido.Text))
            {
                guardarArchivoToolStripMenuItem.Enabled = true;
            }
            

            //Actualiza indicadores
            tslabelValorLinea.Text = 
                rtbContenido.GetLineFromCharIndex(rtbContenido.SelectionStart).ToString();

            tslabelValorPalabras.Text = CuentaPalabras().ToString();
        }

        private void cboxFuentes_DrawItem(object sender, DrawItemEventArgs e)
        {
            e.DrawBackground();
            string strTexto = cboxFuentes.Items[e.Index].ToString();
            Font fuente = new Font(strTexto, e.Font.Size);

            e.Graphics.DrawString(strTexto, fuente, new SolidBrush(e.ForeColor), e.Bounds.Left + 2, e.Bounds.Top + 2);
            e.DrawFocusRectangle();
        }

        private void cboxFuentes_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Establece la fuente como elegida
            cboxFuentes.Font = new Font(cboxFuentes.Text, cboxFuentes.Font.Size);

            Font currentFont = rtbContenido.SelectionFont;
            Font newFont = new Font(cboxFuentes.SelectedItem.ToString(), int.Parse(cboxTamLetra.SelectedItem.ToString()));

            rtbContenido.SelectionFont = newFont;
            rtbContenido.Focus();
        }

        private void FormPrincipal_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void cboxTamLetra_SelectedIndexChanged(object sender, EventArgs e)
        {
            int intSize;
            if(cboxTamLetra.SelectedItem!=null)
            {
                intSize = int.Parse(cboxTamLetra.SelectedItem.ToString());
                Font currentFont = rtbContenido.SelectionFont;
                rtbContenido.SelectionFont = new Font(currentFont.FontFamily, intSize);
            }
            rtbContenido.Focus();
        }

        private void rtbContenido_SelectionChanged(object sender, EventArgs e)
        {
            Font currentFont = rtbContenido.SelectionFont;

            if (currentFont != null)
            {
                cboxFuentes.SelectedItem = currentFont.Name;
                cboxTamLetra.SelectedItem = currentFont.Size.ToString();

                if (currentFont.Bold)
                {
                    btnBold.BackColor = System.Drawing.SystemColors.ActiveCaption;
                }
                else
                {
                    btnBold.BackColor = Color.LightGray;
                }

                if (currentFont.Italic)
                {
                    btnItalic.BackColor = System.Drawing.SystemColors.ActiveCaption;
                }
                else
                {
                    btnItalic.BackColor = Color.LightGray;
                }

                if (currentFont.Underline)
                {
                    btnUnderlined.BackColor = System.Drawing.SystemColors.ActiveCaption;
                }
                else
                {
                    btnUnderlined.BackColor = Color.LightGray;
                }
            }

            //Actualiza línea
            tslabelValorLinea.Text =
                rtbContenido.GetLineFromCharIndex(rtbContenido.SelectionStart).ToString();
        }

        private void btnBold_Click(object sender, EventArgs e)
        {
            Font currentFont, newFont;
            currentFont = rtbContenido.SelectionFont;

            if (currentFont.Bold)
            {
                newFont = new Font(currentFont, currentFont.Style & ~FontStyle.Bold);
                btnBold.BackColor = Color.LightGray;
            }
            else
            {
                newFont = new Font(currentFont, currentFont.Style | FontStyle.Bold);
                btnBold.BackColor = System.Drawing.SystemColors.ActiveCaption;
            }

            rtbContenido.SelectionFont = newFont;
            rtbContenido.Focus();
        }

        private void btnItalic_Click(object sender, EventArgs e)
        {
            Font currentFont, newFont;
            currentFont = rtbContenido.SelectionFont;

            if (currentFont.Italic)
            {
                newFont = new Font(currentFont, currentFont.Style & ~FontStyle.Italic);
                btnItalic.BackColor = Color.LightGray;
            }
            else
            {
                newFont = new Font(currentFont, currentFont.Style | FontStyle.Italic);
                btnItalic.BackColor = System.Drawing.SystemColors.ActiveCaption;
            }

            rtbContenido.SelectionFont = newFont;
            rtbContenido.Focus();
        }

        private void btnUnderlined_Click(object sender, EventArgs e)
        {
            Font currentFont, newFont;
            currentFont = rtbContenido.SelectionFont;

            if (currentFont.Underline)
            {
                newFont = new Font(currentFont, currentFont.Style & ~FontStyle.Underline);
                btnUnderlined.BackColor = Color.LightGray;
            }
            else
            {
                newFont = new Font(currentFont, currentFont.Style | FontStyle.Underline);
                btnUnderlined.BackColor = System.Drawing.SystemColors.ActiveCaption;
            }

            rtbContenido.SelectionFont = newFont;
            rtbContenido.Focus();
        }

        private void sobreLaAplicaciónToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AboutTheApp formAbout = new AboutTheApp();
            formAbout.ShowDialog();
        }

        private void copiarToolStripMenuItem_Click(object sender, EventArgs e)
        {
                Clipboard.SetText(rtbContenido.SelectedRtf, TextDataFormat.Rtf);
        }

        private void cortarToolStripMenuItem_Click(object sender, EventArgs e)
        {
                Clipboard.SetText(rtbContenido.SelectedRtf, TextDataFormat.Rtf);
                rtbContenido.SelectedRtf = "";
        }

        private void pegarToolStripMenuItem_Click(object sender, EventArgs e)
        {
                rtbContenido.SelectedRtf = Clipboard.GetText(TextDataFormat.Rtf);
        }

        private void FormPrincipal_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (guardarArchivoToolStripMenuItem.Enabled)
            {
                DialogResult result;
                result = MessageBox.Show(
                            "El documento actual no ha sido guardado. ¿Desea salir de todas formas?",
                            "Antes de cerrar...",
                            MessageBoxButtons.YesNo,
                            MessageBoxIcon.Question);

                if (result == DialogResult.No)
                {
                    e.Cancel = true;
                }
            }
        }

        private void guardarArchivoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (_strFilepath != null)
            {
                string extension = _strFilepath.Substring(_strFilepath.LastIndexOf('.'));
                switch (extension)
                {
                    case ".txt" :
                        File.WriteAllText(_strFilepath, rtbContenido.Text);
                        break;
                    case ".rtf":
                        rtbContenido.SaveFile(_strFilepath);
                        break;
                }

                guardarArchivoToolStripMenuItem.Enabled = false;
            }
            else
            {
                ShowSaveDialog();
            }
        }

        private void notifyIcon_DoubleClick(object sender, EventArgs e)
        {
            this.Show();
            this.WindowState = FormWindowState.Normal;
        }

        private void seleccionarTodoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            rtbContenido.SelectAll();
        }

        private void eliminarSelecciónToolStripMenuItem_Click(object sender, EventArgs e)
        {
            rtbContenido.SelectedRtf = "";
        }

        private void manualOnlineToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("http://www.proyectoautodidacta.com/comics/procesadores-y-editores-de-textos/");
        }

        private void cboxTamLetra_Leave(object sender, EventArgs e)
        {
            if (cboxTamLetra.SelectedIndex == -1)
            {
                cboxTamLetra.SelectedIndex = DefaultFontSize;
            }
        }

        private void cboxFuentes_Leave(object sender, EventArgs e)
        {
            if (cboxFuentes.SelectedIndex == -1)
            {
                cboxFuentes.SelectedIndex = DefaultFontFamily;
            }
        }

        private void rtbContenido_Click(object sender, EventArgs e)
        {
            Font currentFont;

            if ((currentFont = rtbContenido.SelectionFont) != null)
            {
                cboxFuentes.SelectedItem = currentFont.Name;
                cboxTamLetra.SelectedItem = currentFont.Size.ToString();
            }
        }

        private void FormPrincipal_Resize(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Minimized)
            {
                notifyIcon.BalloonTipText = "La aplicación ha sido minimizada";
                notifyIcon.ShowBalloonTip(1000);
                this.Hide();
            }
        }
    }
}
