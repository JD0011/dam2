﻿namespace Practica10
{
    partial class AboutTheApp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AboutTheApp));
            this.pbPantallas = new System.Windows.Forms.PictureBox();
            this.lblAbout = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pbPantallas)).BeginInit();
            this.SuspendLayout();
            // 
            // pbPantallas
            // 
            this.pbPantallas.Image = ((System.Drawing.Image)(resources.GetObject("pbPantallas.Image")));
            this.pbPantallas.InitialImage = null;
            this.pbPantallas.Location = new System.Drawing.Point(12, 12);
            this.pbPantallas.Name = "pbPantallas";
            this.pbPantallas.Size = new System.Drawing.Size(320, 180);
            this.pbPantallas.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pbPantallas.TabIndex = 0;
            this.pbPantallas.TabStop = false;
            // 
            // lblAbout
            // 
            this.lblAbout.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.lblAbout.Location = new System.Drawing.Point(12, 195);
            this.lblAbout.Name = "lblAbout";
            this.lblAbout.Size = new System.Drawing.Size(320, 104);
            this.lblAbout.TabIndex = 1;
            this.lblAbout.Text = resources.GetString("lblAbout.Text");
            this.lblAbout.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // AboutTheApp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(339, 308);
            this.Controls.Add(this.lblAbout);
            this.Controls.Add(this.pbPantallas);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AboutTheApp";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            ((System.ComponentModel.ISupportInitialize)(this.pbPantallas)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pbPantallas;
        private System.Windows.Forms.Label lblAbout;
    }
}