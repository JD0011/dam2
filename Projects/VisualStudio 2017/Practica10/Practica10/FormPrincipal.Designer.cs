﻿namespace Practica10
{
    partial class FormPrincipal
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormPrincipal));
            this.msOpciones = new System.Windows.Forms.MenuStrip();
            this.archivoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.abrirArchivoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.guardarArchivoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.guardarComoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ediciónToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.copiarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cortarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pegarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.seleccionarTodoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eliminarSelecciónToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ayudaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ayudaToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.manualOnlineToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sobreLaAplicaciónToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rtbContenido = new System.Windows.Forms.RichTextBox();
            this.tsInformacion = new System.Windows.Forms.ToolStrip();
            this.tslabelValorLinea = new System.Windows.Forms.ToolStripLabel();
            this.tslabelLinea = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.tslabelValorPalabras = new System.Windows.Forms.ToolStripLabel();
            this.tslabelPalabras = new System.Windows.Forms.ToolStripLabel();
            this.pnlButtons = new System.Windows.Forms.Panel();
            this.btnUnderlined = new System.Windows.Forms.Button();
            this.btnItalic = new System.Windows.Forms.Button();
            this.btnBold = new System.Windows.Forms.Button();
            this.cboxTamLetra = new System.Windows.Forms.ComboBox();
            this.cboxFuentes = new System.Windows.Forms.ComboBox();
            this.bgWorker = new System.ComponentModel.BackgroundWorker();
            this.notifyIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.msOpciones.SuspendLayout();
            this.tsInformacion.SuspendLayout();
            this.pnlButtons.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // msOpciones
            // 
            this.msOpciones.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.archivoToolStripMenuItem,
            this.ediciónToolStripMenuItem,
            this.ayudaToolStripMenuItem});
            this.msOpciones.Location = new System.Drawing.Point(0, 0);
            this.msOpciones.Name = "msOpciones";
            this.msOpciones.Size = new System.Drawing.Size(784, 24);
            this.msOpciones.TabIndex = 0;
            // 
            // archivoToolStripMenuItem
            // 
            this.archivoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.abrirArchivoToolStripMenuItem,
            this.toolStripSeparator2,
            this.guardarArchivoToolStripMenuItem,
            this.guardarComoToolStripMenuItem});
            this.archivoToolStripMenuItem.Name = "archivoToolStripMenuItem";
            this.archivoToolStripMenuItem.Size = new System.Drawing.Size(60, 20);
            this.archivoToolStripMenuItem.Text = "Archivo";
            // 
            // abrirArchivoToolStripMenuItem
            // 
            this.abrirArchivoToolStripMenuItem.Name = "abrirArchivoToolStripMenuItem";
            this.abrirArchivoToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.abrirArchivoToolStripMenuItem.Size = new System.Drawing.Size(245, 22);
            this.abrirArchivoToolStripMenuItem.Text = "Abrir archivo";
            this.abrirArchivoToolStripMenuItem.Click += new System.EventHandler(this.abrirArchivoToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(242, 6);
            // 
            // guardarArchivoToolStripMenuItem
            // 
            this.guardarArchivoToolStripMenuItem.Enabled = false;
            this.guardarArchivoToolStripMenuItem.Name = "guardarArchivoToolStripMenuItem";
            this.guardarArchivoToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.guardarArchivoToolStripMenuItem.Size = new System.Drawing.Size(245, 22);
            this.guardarArchivoToolStripMenuItem.Text = "Guardar";
            this.guardarArchivoToolStripMenuItem.Click += new System.EventHandler(this.guardarArchivoToolStripMenuItem_Click);
            // 
            // guardarComoToolStripMenuItem
            // 
            this.guardarComoToolStripMenuItem.Name = "guardarComoToolStripMenuItem";
            this.guardarComoToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.S)));
            this.guardarComoToolStripMenuItem.Size = new System.Drawing.Size(245, 22);
            this.guardarComoToolStripMenuItem.Text = "Guardar como...";
            this.guardarComoToolStripMenuItem.Click += new System.EventHandler(this.guardarArchivoComoToolStripMenuItem_Click);
            // 
            // ediciónToolStripMenuItem
            // 
            this.ediciónToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.copiarToolStripMenuItem,
            this.cortarToolStripMenuItem,
            this.pegarToolStripMenuItem,
            this.toolStripSeparator1,
            this.seleccionarTodoToolStripMenuItem,
            this.eliminarSelecciónToolStripMenuItem});
            this.ediciónToolStripMenuItem.Name = "ediciónToolStripMenuItem";
            this.ediciónToolStripMenuItem.Size = new System.Drawing.Size(58, 20);
            this.ediciónToolStripMenuItem.Text = "Edición";
            // 
            // copiarToolStripMenuItem
            // 
            this.copiarToolStripMenuItem.Name = "copiarToolStripMenuItem";
            this.copiarToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C)));
            this.copiarToolStripMenuItem.Size = new System.Drawing.Size(211, 22);
            this.copiarToolStripMenuItem.Text = "Copiar";
            this.copiarToolStripMenuItem.Click += new System.EventHandler(this.copiarToolStripMenuItem_Click);
            // 
            // cortarToolStripMenuItem
            // 
            this.cortarToolStripMenuItem.Name = "cortarToolStripMenuItem";
            this.cortarToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.X)));
            this.cortarToolStripMenuItem.Size = new System.Drawing.Size(211, 22);
            this.cortarToolStripMenuItem.Text = "Cortar";
            this.cortarToolStripMenuItem.Click += new System.EventHandler(this.cortarToolStripMenuItem_Click);
            // 
            // pegarToolStripMenuItem
            // 
            this.pegarToolStripMenuItem.Name = "pegarToolStripMenuItem";
            this.pegarToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.V)));
            this.pegarToolStripMenuItem.Size = new System.Drawing.Size(211, 22);
            this.pegarToolStripMenuItem.Text = "Pegar";
            this.pegarToolStripMenuItem.Click += new System.EventHandler(this.pegarToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(208, 6);
            // 
            // seleccionarTodoToolStripMenuItem
            // 
            this.seleccionarTodoToolStripMenuItem.Name = "seleccionarTodoToolStripMenuItem";
            this.seleccionarTodoToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.A)));
            this.seleccionarTodoToolStripMenuItem.Size = new System.Drawing.Size(211, 22);
            this.seleccionarTodoToolStripMenuItem.Text = "Seleccionar todo";
            this.seleccionarTodoToolStripMenuItem.Click += new System.EventHandler(this.seleccionarTodoToolStripMenuItem_Click);
            // 
            // eliminarSelecciónToolStripMenuItem
            // 
            this.eliminarSelecciónToolStripMenuItem.Name = "eliminarSelecciónToolStripMenuItem";
            this.eliminarSelecciónToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D)));
            this.eliminarSelecciónToolStripMenuItem.Size = new System.Drawing.Size(211, 22);
            this.eliminarSelecciónToolStripMenuItem.Text = "Eliminar selección";
            this.eliminarSelecciónToolStripMenuItem.Click += new System.EventHandler(this.eliminarSelecciónToolStripMenuItem_Click);
            // 
            // ayudaToolStripMenuItem
            // 
            this.ayudaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ayudaToolStripMenuItem1,
            this.sobreLaAplicaciónToolStripMenuItem});
            this.ayudaToolStripMenuItem.Name = "ayudaToolStripMenuItem";
            this.ayudaToolStripMenuItem.Size = new System.Drawing.Size(77, 20);
            this.ayudaToolStripMenuItem.Text = "Acerca de..";
            // 
            // ayudaToolStripMenuItem1
            // 
            this.ayudaToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.manualOnlineToolStripMenuItem});
            this.ayudaToolStripMenuItem1.Name = "ayudaToolStripMenuItem1";
            this.ayudaToolStripMenuItem1.Size = new System.Drawing.Size(173, 22);
            this.ayudaToolStripMenuItem1.Text = "Ayuda";
            // 
            // manualOnlineToolStripMenuItem
            // 
            this.manualOnlineToolStripMenuItem.Name = "manualOnlineToolStripMenuItem";
            this.manualOnlineToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.manualOnlineToolStripMenuItem.Text = "Manual online";
            this.manualOnlineToolStripMenuItem.Click += new System.EventHandler(this.manualOnlineToolStripMenuItem_Click);
            // 
            // sobreLaAplicaciónToolStripMenuItem
            // 
            this.sobreLaAplicaciónToolStripMenuItem.Name = "sobreLaAplicaciónToolStripMenuItem";
            this.sobreLaAplicaciónToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.sobreLaAplicaciónToolStripMenuItem.Text = "Sobre la aplicación";
            this.sobreLaAplicaciónToolStripMenuItem.Click += new System.EventHandler(this.sobreLaAplicaciónToolStripMenuItem_Click);
            // 
            // rtbContenido
            // 
            this.rtbContenido.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtbContenido.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtbContenido.Location = new System.Drawing.Point(3, 40);
            this.rtbContenido.Name = "rtbContenido";
            this.rtbContenido.Size = new System.Drawing.Size(778, 405);
            this.rtbContenido.TabIndex = 2;
            this.rtbContenido.Text = "";
            this.rtbContenido.SelectionChanged += new System.EventHandler(this.rtbContenido_SelectionChanged);
            this.rtbContenido.Click += new System.EventHandler(this.rtbContenido_Click);
            this.rtbContenido.TextChanged += new System.EventHandler(this.rtbContenido_TextChanged);
            // 
            // tsInformacion
            // 
            this.tsInformacion.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tsInformacion.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tslabelValorLinea,
            this.tslabelLinea,
            this.toolStripSeparator3,
            this.tslabelValorPalabras,
            this.tslabelPalabras});
            this.tsInformacion.Location = new System.Drawing.Point(0, 448);
            this.tsInformacion.Name = "tsInformacion";
            this.tsInformacion.Size = new System.Drawing.Size(784, 20);
            this.tsInformacion.TabIndex = 3;
            // 
            // tslabelValorLinea
            // 
            this.tslabelValorLinea.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tslabelValorLinea.Name = "tslabelValorLinea";
            this.tslabelValorLinea.Size = new System.Drawing.Size(13, 17);
            this.tslabelValorLinea.Text = "0";
            // 
            // tslabelLinea
            // 
            this.tslabelLinea.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tslabelLinea.Name = "tslabelLinea";
            this.tslabelLinea.Size = new System.Drawing.Size(38, 17);
            this.tslabelLinea.Text = "Línea:";
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 20);
            // 
            // tslabelValorPalabras
            // 
            this.tslabelValorPalabras.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tslabelValorPalabras.Name = "tslabelValorPalabras";
            this.tslabelValorPalabras.Size = new System.Drawing.Size(13, 17);
            this.tslabelValorPalabras.Tag = "";
            this.tslabelValorPalabras.Text = "0";
            // 
            // tslabelPalabras
            // 
            this.tslabelPalabras.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tslabelPalabras.Name = "tslabelPalabras";
            this.tslabelPalabras.Size = new System.Drawing.Size(54, 17);
            this.tslabelPalabras.Text = "Palabras:";
            // 
            // pnlButtons
            // 
            this.pnlButtons.Controls.Add(this.btnUnderlined);
            this.pnlButtons.Controls.Add(this.btnItalic);
            this.pnlButtons.Controls.Add(this.btnBold);
            this.pnlButtons.Controls.Add(this.cboxTamLetra);
            this.pnlButtons.Controls.Add(this.cboxFuentes);
            this.pnlButtons.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlButtons.Location = new System.Drawing.Point(3, 3);
            this.pnlButtons.Name = "pnlButtons";
            this.pnlButtons.Size = new System.Drawing.Size(778, 31);
            this.pnlButtons.TabIndex = 4;
            // 
            // btnUnderlined
            // 
            this.btnUnderlined.BackColor = System.Drawing.Color.LightGray;
            this.btnUnderlined.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUnderlined.Location = new System.Drawing.Point(334, 3);
            this.btnUnderlined.Name = "btnUnderlined";
            this.btnUnderlined.Size = new System.Drawing.Size(25, 25);
            this.btnUnderlined.TabIndex = 4;
            this.btnUnderlined.Text = "U";
            this.btnUnderlined.UseVisualStyleBackColor = false;
            this.btnUnderlined.Click += new System.EventHandler(this.btnUnderlined_Click);
            // 
            // btnItalic
            // 
            this.btnItalic.BackColor = System.Drawing.Color.LightGray;
            this.btnItalic.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnItalic.Location = new System.Drawing.Point(303, 3);
            this.btnItalic.Name = "btnItalic";
            this.btnItalic.Size = new System.Drawing.Size(25, 25);
            this.btnItalic.TabIndex = 3;
            this.btnItalic.Text = "I";
            this.btnItalic.UseVisualStyleBackColor = false;
            this.btnItalic.Click += new System.EventHandler(this.btnItalic_Click);
            // 
            // btnBold
            // 
            this.btnBold.BackColor = System.Drawing.Color.LightGray;
            this.btnBold.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBold.Location = new System.Drawing.Point(272, 3);
            this.btnBold.Name = "btnBold";
            this.btnBold.Size = new System.Drawing.Size(25, 25);
            this.btnBold.TabIndex = 2;
            this.btnBold.Text = "B";
            this.btnBold.UseVisualStyleBackColor = false;
            this.btnBold.Click += new System.EventHandler(this.btnBold_Click);
            // 
            // cboxTamLetra
            // 
            this.cboxTamLetra.FormattingEnabled = true;
            this.cboxTamLetra.Items.AddRange(new object[] {
            "8",
            "9",
            "10",
            "11",
            "12",
            "14",
            "16",
            "18",
            "20",
            "22",
            "24",
            "26",
            "28",
            "36",
            "48",
            "72"});
            this.cboxTamLetra.Location = new System.Drawing.Point(135, 4);
            this.cboxTamLetra.Name = "cboxTamLetra";
            this.cboxTamLetra.Size = new System.Drawing.Size(121, 21);
            this.cboxTamLetra.TabIndex = 1;
            this.cboxTamLetra.SelectedIndexChanged += new System.EventHandler(this.cboxTamLetra_SelectedIndexChanged);
            this.cboxTamLetra.Leave += new System.EventHandler(this.cboxTamLetra_Leave);
            // 
            // cboxFuentes
            // 
            this.cboxFuentes.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
            this.cboxFuentes.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboxFuentes.FormattingEnabled = true;
            this.cboxFuentes.Location = new System.Drawing.Point(8, 4);
            this.cboxFuentes.Name = "cboxFuentes";
            this.cboxFuentes.Size = new System.Drawing.Size(121, 21);
            this.cboxFuentes.TabIndex = 0;
            this.cboxFuentes.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.cboxFuentes_DrawItem);
            this.cboxFuentes.SelectedIndexChanged += new System.EventHandler(this.cboxFuentes_SelectedIndexChanged);
            this.cboxFuentes.Leave += new System.EventHandler(this.cboxFuentes_Leave);
            // 
            // notifyIcon
            // 
            this.notifyIcon.BalloonTipText = "La aplicación ha sido minimizada";
            this.notifyIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon.Icon")));
            this.notifyIcon.Text = "Editor de texto";
            this.notifyIcon.Visible = true;
            this.notifyIcon.DoubleClick += new System.EventHandler(this.notifyIcon_DoubleClick);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.pnlButtons, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tsInformacion, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.rtbContenido, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 24);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(784, 468);
            this.tableLayoutPanel1.TabIndex = 5;
            // 
            // FormPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 492);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.msOpciones);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.msOpciones;
            this.MinimumSize = new System.Drawing.Size(800, 530);
            this.Name = "FormPrincipal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Editor de Texto";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormPrincipal_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormPrincipal_FormClosed);
            this.Resize += new System.EventHandler(this.FormPrincipal_Resize);
            this.msOpciones.ResumeLayout(false);
            this.msOpciones.PerformLayout();
            this.tsInformacion.ResumeLayout(false);
            this.tsInformacion.PerformLayout();
            this.pnlButtons.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip msOpciones;
        private System.Windows.Forms.ToolStripMenuItem archivoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ediciónToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ayudaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ayudaToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem sobreLaAplicaciónToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem abrirArchivoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem guardarArchivoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem guardarComoToolStripMenuItem;
        private System.Windows.Forms.RichTextBox rtbContenido;
        private System.Windows.Forms.ToolStrip tsInformacion;
        private System.Windows.Forms.ToolStripLabel tslabelLinea;
        private System.Windows.Forms.ToolStripLabel tslabelValorLinea;
        private System.Windows.Forms.Panel pnlButtons;
        private System.Windows.Forms.ComboBox cboxFuentes;
        private System.Windows.Forms.ComboBox cboxTamLetra;
        private System.Windows.Forms.Button btnUnderlined;
        private System.Windows.Forms.Button btnItalic;
        private System.Windows.Forms.Button btnBold;
        private System.ComponentModel.BackgroundWorker bgWorker;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem copiarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cortarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pegarToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem seleccionarTodoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem eliminarSelecciónToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripLabel tslabelPalabras;
        private System.Windows.Forms.ToolStripLabel tslabelValorPalabras;
        private System.Windows.Forms.NotifyIcon notifyIcon;
        private System.Windows.Forms.ToolStripMenuItem manualOnlineToolStripMenuItem;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
    }
}

