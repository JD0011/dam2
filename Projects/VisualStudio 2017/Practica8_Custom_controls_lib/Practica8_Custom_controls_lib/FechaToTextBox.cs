﻿/*
* PRÁCTICA.............: Práctica 8
* NOMBRE Y APELLIDOS...: Juan de Dios Delgado Bermúdez
* CURSO Y GRUPO........: 2o Desarrollo de Interfaces
* TÍTULO DE LA PRÁCTICA: Generación de Componentes.
* FECHA DE ENTREGA.....: 10 de Enero de 2018
*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace Practica8_Custom_controls_lib
{
    public partial class FechaToTextBox : UserControl
    {
        private CalendarForm _cformCalendar;
        private bool _inputOK = false;
        private bool _onlyYearMonth = false;

        public bool InputOK { get => _inputOK; set => _inputOK = value; }
        public bool OnlyYearMonth { get => _onlyYearMonth; set => _onlyYearMonth = value; }

        public override string Text { get => textBox.Text; set => textBox.Text = value; }

        public FechaToTextBox()
        {
            InitializeComponent();

            toolTipCalendar.ToolTipIcon = ToolTipIcon.Info;

            _cformCalendar = new CalendarForm(textBox, _onlyYearMonth, this);
        }

        public void ClearText()
        {
            textBox.Text = "";
        }

        private void textBox_Leave(object sender, EventArgs e)
        {
            if (!_cformCalendar.Focused)
            {
                _cformCalendar.Hide();
            }

            if (!textBox.Text.Equals(""))
            {
                string strDateRegex;
                if (_onlyYearMonth)
                {
                    strDateRegex = "^[0-9]{2}/[0-9]{4}$";
                }
                else
                {
                    strDateRegex = "^[0-9]{2}/[0-9]{2}/[0-9]{4}$";
                }

                if (!new Regex(strDateRegex).IsMatch(textBox.Text))
                {
                    _inputOK = false;
                    toolTipTextBox.Show(@"Solo puede introducir una fecha en formato "+DateTime.Now.ToString("MM/ dd/ yyyy"), this);
                }
                else
                {
                    _inputOK = true;
                    toolTipTextBox.Hide(textBox);
                }
            }
        }

        private void textBox_Click(object sender, EventArgs e)
        {
                _cformCalendar.Location = this.PointToScreen(new Point(textBox.Left, textBox.Top + textBox.Height));

            if (!_cformCalendar.Visible)
            {
                _cformCalendar.Show();
            }
        }
        
        private void toolTipCalendar_Popup(object sender, PopupEventArgs e)
        {
            toolTipCalendar.Show("Seleccione una fecha a establecer", this);
        }
    }
}
