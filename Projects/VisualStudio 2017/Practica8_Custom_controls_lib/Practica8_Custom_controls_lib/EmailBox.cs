﻿/*
* PRÁCTICA.............: Práctica 8
* NOMBRE Y APELLIDOS...: Juan de Dios Delgado Bermúdez
* CURSO Y GRUPO........: 2o Desarrollo de Interfaces
* TÍTULO DE LA PRÁCTICA: Generación de Componentes.
* FECHA DE ENTREGA.....: 10 de Enero de 2018
*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Practica8_Custom_controls_lib
{
    public partial class EmailBox : UserControl
    {
        private bool _inputOK = false;
        public bool InputOK { get => _inputOK; set => _inputOK = value; }
        public override string Text { get => textBox.Text; set => textBox.Text = value; }
        public EmailBox()
        {
            InitializeComponent();
        }

        public void ClearText()
        {
            textBox.Text = "";
        }
        private void textBox_Leave(object sender, EventArgs e)
        {
            if (!textBox.Text.Equals(""))
            {
                
                string mail = textBox.Text;

                bool emailOK = true;
                //Tiene '@' y '.'?
                if (mail.Contains('@'))
                {
                    int numArrobas = mail.Count(x => x.Equals('@'));
                    int numPuntos = mail.Substring(mail.LastIndexOf('@'), mail.Length).Count(x => x.Equals('.'));

                    if(numArrobas==1 || numPuntos==1)
                    {
                        //Tiene caracteres permitidos(letras, '@' y '.')?
                        foreach(char c in mail)
                        {
                            if (c!='@' || c!='.' || !Char.IsLetter(c) )
                            {
                                emailOK = false;
                                break;
                            }
                        }
                    }
                }

                if (!emailOK)
                {
                    _inputOK = false;
                    toolTip.Show("Formato de correo electrónico erróneo", this);
                    textBox.Focus();
                }
                else
                {
                    _inputOK = true;
                    toolTip.Hide(this);
                }
                
            }
        }
    }
}
