﻿using System;

namespace Practica8_Custom_controls_lib
{
    partial class FechaToTextBox
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.textBox = new System.Windows.Forms.TextBox();
            this.toolTipTextBox = new System.Windows.Forms.ToolTip(this.components);
            this.toolTipCalendar = new System.Windows.Forms.ToolTip(this.components);
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.SuspendLayout();
            // 
            // textBox
            // 
            this.textBox.Location = new System.Drawing.Point(6, 3);
            this.textBox.Name = "textBox";
            this.textBox.Size = new System.Drawing.Size(100, 20);
            this.textBox.TabIndex = 0;
            this.textBox.Click += new System.EventHandler(this.textBox_Click);
            this.textBox.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // toolTipTextBox
            // 
            this.toolTipTextBox.AutoPopDelay = 3000;
            this.toolTipTextBox.InitialDelay = 500;
            this.toolTipTextBox.ReshowDelay = 100;
            this.toolTipTextBox.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Error;
            this.toolTipTextBox.ToolTipTitle = "Dato incorrecto";
            // 
            // toolTipCalendar
            // 
            this.toolTipCalendar.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.toolTipCalendar.Popup += new System.Windows.Forms.PopupEventHandler(this.toolTipCalendar_Popup);
            // 
            // FechaToTextBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.Controls.Add(this.textBox);
            this.Name = "FechaToTextBox";
            this.Size = new System.Drawing.Size(109, 26);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox;
        private System.Windows.Forms.ToolTip toolTipTextBox;
        private System.Windows.Forms.ToolTip toolTipCalendar;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
    }
}
