﻿/*
* PRÁCTICA.............: Práctica 8
* NOMBRE Y APELLIDOS...: Juan de Dios Delgado Bermúdez
* CURSO Y GRUPO........: 2o Desarrollo de Interfaces
* TÍTULO DE LA PRÁCTICA: Generación de Componentes.
* FECHA DE ENTREGA.....: 10 de Enero de 2018
*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace Practica8_Custom_controls_lib
{
    public partial class LetrasMinusCapitalPrimera : UserControl
    {
        private bool _inputOK = false;
        public bool InputOK { get => _inputOK; set => _inputOK = value; }
        public override string Text { get => textBox.Text; set => textBox.Text = value; }

        public LetrasMinusCapitalPrimera()
        {
            InitializeComponent();
        }

        public void ClearText()
        {
            textBox.Text = "";
        }

        private void textBox_Leave(object sender, EventArgs e)
        {
            if (!textBox.Text.Equals(""))
            {
                string[] strText = textBox.Text.Split(' ');
                foreach (string word in strText)
                {
                    if (!new Regex("^[A-Z]{1}[a-z]*$").IsMatch(word))
                    {
                        _inputOK = false;
                        toolTip.ToolTipIcon = ToolTipIcon.Error;
                        toolTip.Show("Solo se pueden introducir letras. Primera letra capital", textBox);
                        textBox.Focus();
                    }
                    else
                    {
                        _inputOK = true;
                        toolTip.Hide(textBox);
                    }
                }
            }
        }
    }
}
