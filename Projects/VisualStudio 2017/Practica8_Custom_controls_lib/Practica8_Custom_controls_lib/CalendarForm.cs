﻿/*
* PRÁCTICA.............: Práctica 8
* NOMBRE Y APELLIDOS...: Juan de Dios Delgado Bermúdez
* CURSO Y GRUPO........: 2o Desarrollo de Interfaces
* TÍTULO DE LA PRÁCTICA: Generación de Componentes.
* FECHA DE ENTREGA.....: 10 de Enero de 2018
*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Practica8_Custom_controls_lib
{
    public partial class CalendarForm : Form
    {
        private bool _onlyYearMonth;
        private bool _inputOK;
        private TextBox _textBox;

        public bool InputOK { get => _inputOK; }

        public CalendarForm(TextBox textbox, bool onlyYearMonth, FechaToTextBox fttb)
        {
            InitializeComponent();

            _textBox = textbox;
            _onlyYearMonth = onlyYearMonth;

            this.StartPosition = FormStartPosition.Manual;
            this.Location = fttb.PointToScreen(new Point(_textBox.Left, _textBox.Top + _textBox.Height));
        }

        private void monthCalendar_Leave(object sender, EventArgs e)
        {
            monthCalendar.Visible = false;
        }

        private void monthCalendar_DateSelected(object sender, DateRangeEventArgs e)
        {
            if (_onlyYearMonth)
            {
                _textBox.Text = monthCalendar.SelectionStart.ToString("MM/yyyy");
            }
            else
            {
                _textBox.Text = monthCalendar.SelectionStart.ToString("dd/MM/yyyy");
            }

            this.Hide();
            _inputOK = true;
        }

        private void toolTipCalendar_Popup(object sender, PopupEventArgs e)
        {
            toolTipCalendar.Show("Seleccione una fecha a establecer", this);
        }
    }
}
