﻿/*
* PRÁCTICA.............: Práctica 4.
* NOMBRE y APELLIDOS...: Juan de Dios Delgado Bermúdez
* CURSO y GRUPO........: 2o Desarrollo de Interfaces
* TÍTULO de la PRÁCTICA: Diseño de clases. Herencia y polimorfismo.
* FECHA de ENTREGA.....: 22 de Noviembre de 2017
*/

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica4
{
    static class PruebaFichas
    {
        #region Variables
        const int MIN_ANYO = 1500;
        #endregion Variables

        #region Métodos

        public static int Menu()
        {
            Console.WriteLine("\nBIBLIOTECA\n");

            Utils.PrintColorMessage("Elija el tipo de Ficha a crear\n", ConsoleColor.Yellow);

            Console.WriteLine(
                "1. Libro\n" +
                "2. Libro Volumen\n" +
                "3. Revista\n" +
                "4. Pelicula en DVD\n" +
                "5. Consultar Biblioteca\n" +
                "6. SALIR"
                );  

            return Utils.LeerEntero(1,6);
        }

        public static FichaLibro CreaFichaLibro()
        {
            FichaLibro auxFicha;

            string strReferencia;
            string strTitulo;
            string strAutor;
            string strEditorial;
            int intNumEjemplares;

            Console.WriteLine("Introduzca los datos requeridos\n");

            Console.Write(String.Format("\t{0,-15}:", "Referencia"));
            strReferencia = Utils.LeerCadena(3, 15);

            Console.Write(String.Format("\t{0,-15}:", "Título"));
            strTitulo = Utils.LeerCadenayNumeros(2,50);

            Console.Write(String.Format("\t{0,-15}:", "Autor"));
            strAutor = Utils.LeerCadena(3,30);

            Console.Write(String.Format("\t{0,-15}:", "Editorial"));
            strEditorial = Utils.LeerCadenayNumeros(3,30);

            Console.Write("\nUnidades -> ");
            intNumEjemplares = Utils.LeerEntero(1, 15);

            auxFicha = new FichaLibro(strTitulo, strAutor, strReferencia, strEditorial, intNumEjemplares);

            Console.Clear();
            Utils.PrintColorMessage("Los datos introducidos son...\n", ConsoleColor.Yellow);
            auxFicha.Imprimir();

            return auxFicha;
        }

        public static FichaLibroVol CreaFichaLibroVol()
        {
            FichaLibroVol auxFicha;

            string strReferencia;
            string strTitulo;
            string strAutor;
            string strEditorial;
            int intNumEjemplares;
            int intNumVolumen;

            Console.WriteLine("Introduzca los datos requeridos\n");

            Console.Write(String.Format("\t{0,-15}:", "Referencia"));
            strReferencia = Utils.LeerCadena(3,15);

            Console.Write(String.Format("\t{0,-15}:", "Título"));
            strTitulo = Utils.LeerCadenayNumeros(2, 50);

            Console.Write(String.Format("\t{0,-15}:", "Autor"));
            strAutor = Utils.LeerCadena(3, 30);

            Console.Write(String.Format("\t{0,-15}:", "Editorial"));
            strEditorial = Utils.LeerCadenayNumeros(3, 15);

            Console.Write(String.Format("\t{0,-15}:", "Nº de Volumen"));
            intNumVolumen = Utils.LeerEntero(1,30);

            Console.Write("\nUnidades -> ");
            intNumEjemplares = Utils.LeerEntero(1, 15);

            auxFicha = new FichaLibroVol(strTitulo, strAutor, strReferencia, strEditorial, intNumEjemplares, intNumVolumen);

            Console.Clear();
            Utils.PrintColorMessage("Los datos introducidos son...\n", ConsoleColor.Yellow);
            auxFicha.Imprimir();

            return auxFicha;
        }

        public static FichaRevista CreaFichaRevista()
        {
            FichaRevista auxFicha;

            string strReferencia;
            string strTitulo;
            int intAnyo;
            int intNumEjemplares;
            int intNumero;

            Console.WriteLine("Introduzca los datos requeridos\n");

            Console.Write(String.Format("\t{0,-15}:", "Referencia"));
            strReferencia = Utils.LeerCadena(3, 15);

            Console.Write(String.Format("\t{0,-15}:", "Título"));
            strTitulo = Utils.LeerCadenayNumeros(2, 50);

            Console.Write(String.Format("\t{0,-15}:", "Número"));
            intNumero = Utils.LeerEntero(1,5000);

            Console.Write(String.Format("\t{0,-15}:", "Año"));
            intAnyo = Utils.LeerEntero(1500, DateTime.Now.Year);
            
            Console.Write("\nUnidades -> ");
            intNumEjemplares = Utils.LeerEntero(1, 15);

            auxFicha = new FichaRevista(strTitulo, strReferencia, intNumEjemplares, intNumero, intAnyo);

            Console.Clear();
            Utils.PrintColorMessage("Los datos introducidos son...\n", ConsoleColor.Yellow);
            auxFicha.Imprimir();

            return auxFicha;
        }

        public static FichaDVD CreaFichaDVD()
        {
            FichaDVD auxFicha;

            string strActor;
            string strReferencia;
            string strTitulo;
            string strDirector;
            int intAnyo;
            int intNumEjemplares;
            int intNumActores;
            List<string> rngActores;

            Console.WriteLine("Introduzca los datos requeridos\n");

            Console.Write(String.Format("\t{0,-15}:", "Referencia"));
            strReferencia = Utils.LeerCadena(3, 15);

            Console.Write(String.Format("\t{0,-15}:", "Título"));
            strTitulo = Utils.LeerCadenayNumeros(2, 50);

            Console.Write(String.Format("\t{0,-15}:", "Director"));
            strDirector = Utils.LeerCadena(3, 30);

            Console.Write(String.Format("\t{0,-15}:", "Año"));
            intAnyo = Utils.LeerEntero(1995,DateTime.Now.Year);

            Console.Write(String.Format("\t{0,-15}:", "Nº de Actores"));
            intNumActores = Utils.LeerEntero(0,10);

            rngActores = null;
            if (intNumActores != 0)
            {
                Console.WriteLine("Introduzca el nombre de los actores\n");
                rngActores = new List<string>();

                for (int i = 0; i < intNumActores; i++)
                {
                    Console.Write("\tActor Nº "+(i+1)+": ");
                    strActor = Utils.LeerCadena(3, 30);
                    rngActores.Add(strActor);
                }
            }

            Console.Write("\nUnidades -> ");
            intNumEjemplares = Utils.LeerEntero(1, 15);

            auxFicha = new FichaDVD(strTitulo, intNumEjemplares, strReferencia, strDirector, intAnyo, rngActores);

            Console.Clear();
            Utils.PrintColorMessage("\nLos datos introducidos son...\n", ConsoleColor.Yellow);
            auxFicha.Imprimir();

            return auxFicha;
        }

        public static bool FichaCorrecta()
        {
            bool esCorrecto;
            Console.WriteLine("\n¿Desea confirmar los datos?[Pulse S/N]");
            string strRespuesta;
            
            do
            {
                strRespuesta = Console.ReadKey(true).KeyChar.ToString();
                strRespuesta = strRespuesta.ToUpper();

            } while (!(strRespuesta.Equals("S") || strRespuesta.Equals("N")));

            esCorrecto = strRespuesta.Equals("S") ? true : false;

            Console.Clear();

            return esCorrecto;
        }

        static void Main (string[] args)
        {
            Biblioteca btcGeneral;
            bool exitCondition;
            
            btcGeneral = new Biblioteca();
            exitCondition = false;
            while (!exitCondition)
            {
                Console.Clear();

                switch (PruebaFichas.Menu())
                {
                    case 1:
                        FichaLibro fLibro;

                        fLibro = CreaFichaLibro();
                        if (FichaCorrecta())
                        {
                            btcGeneral.AddFichaLibro(fLibro);
                        }
                        break;

                    case 2:
                        FichaLibroVol fLibroVol;

                        fLibroVol = CreaFichaLibroVol();

                        if (FichaCorrecta())
                        {
                            btcGeneral.AddFichaLibroVol(fLibroVol);
                        }
                        break;

                    case 3:
                        FichaRevista fRevista;

                        fRevista = CreaFichaRevista();

                        if (FichaCorrecta())
                        {
                            btcGeneral.AddFichaRevista(fRevista);
                        }
                        break;

                    case 4:
                        FichaDVD fDVD;

                        fDVD = CreaFichaDVD();
                        if (FichaCorrecta())
                        {
                            btcGeneral.AddFichaDVD(fDVD);
                        }
                        break;

                    case 5:
                        bool salirMenu;

                        salirMenu = false;
                        while (!salirMenu)
                        {
                            Console.Clear();
                            Console.WriteLine();

                            switch (btcGeneral.Menu())
                            {
                                case 1: btcGeneral.MostrarFLibro(); break;
                                case 2: btcGeneral.MostrarFLibroVol(); break;
                                case 3: btcGeneral.MostrarFRevista(); break;
                                case 4: btcGeneral.MostrarFDVD(); break;
                                case 5: salirMenu = true; break;
                                default:
                                    Utils.PrintHeaderMessage("Debe introducir una opción existente");
                                    Console.Clear();
                                    break;
                            }
                        }
                        break;

                    case 6:exitCondition = true; break;

                    default:
                        Utils.PrintHeaderMessage("Debe introducir una opción existente");
                        Console.Clear();
                        break;
                }
            }
        }
        
        #endregion Métodos
    }
}
