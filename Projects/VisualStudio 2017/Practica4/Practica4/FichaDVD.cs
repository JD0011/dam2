﻿/*
* PRÁCTICA.............: Práctica 4.
* NOMBRE y APELLIDOS...: Juan de Dios Delgado Bermúdez
* CURSO y GRUPO........: 2o Desarrollo de Interfaces
* TÍTULO de la PRÁCTICA: Diseño de clases. Herencia y polimorfismo.
* FECHA de ENTREGA.....: 22 de Noviembre de 2017
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica4
{
    class FichaDVD : Ficha
    {
        #region Variables

        string _strDirector;
        int _intAnyo;
        List<string> _rngActores;

        #endregion Variables

        #region Constructor

        public FichaDVD(string strTitulo, int intNumEjemplares, string strReferencia, string strDirector, int intAnyo, List<string> rngActores) 
            : base(strTitulo, intNumEjemplares, strReferencia)
        {
            _strDirector = strDirector;
            _intAnyo = intAnyo;
            _rngActores = rngActores;
        }

        #endregion Constructor 

        #region Métodos
        
        public override void Imprimir()
        {
            base.Imprimir();

            Console.Write(
                "Director.....: \t{0}\n" +
                "Año..........: \t{1}\n",
                _strDirector,
                _intAnyo
                );

            Console.WriteLine("Actores:\n");

            if (_rngActores == null)
            {
                Console.WriteLine("\t{0}", "Ninguno");
            }
            else
            {
                for(int i = 0; i < _rngActores.Count; i++)
                {
                    Console.WriteLine("\t{0,-10}{1}", "Actor "+(i+1)+":", _rngActores[i]);
                }
            }
        }

        #endregion MétodosS
    }
}
