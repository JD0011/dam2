﻿/*
* PRÁCTICA.............: Práctica 4.
* NOMBRE y APELLIDOS...: Juan de Dios Delgado Bermúdez
* CURSO y GRUPO........: 2o Desarrollo de Interfaces
* TÍTULO de la PRÁCTICA: Diseño de clases. Herencia y polimorfismo.
* FECHA de ENTREGA.....: 22 de Noviembre de 2017
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica4
{
    class Biblioteca
    {
        #region Variables

        List<FichaLibro> rngFichasLibro;
        List<FichaLibroVol> rngFichasLibroVol;
        List<FichaRevista> rngFichasRevista;
        List<FichaDVD> rngFichasDVD;

        #endregion Variables

        #region Métodos

        public int Menu()
        {
            Utils.PrintColorMessage("Seleccione la categoría a consultar\n", ConsoleColor.Yellow);

            Console.WriteLine(
                "1. Consultar libros \t({0})\n" +
                "2. Consultar volúmenes \t({1})\n" +
                "3. Consultar revistas \t({2})\n" +
                "4. Consultar DVD's \t({3})\n" +
                "5. Volver",
                rngFichasLibro==null? 0 : rngFichasLibro.Count,
                rngFichasLibroVol == null ? 0 : rngFichasLibroVol.Count,
                rngFichasRevista == null ? 0 : rngFichasRevista.Count,
                rngFichasDVD == null ? 0 : rngFichasDVD.Count
                );

            return Utils.LeerEntero(1,5);
        }

        public void AddFichaLibro(FichaLibro fichaLibro)
        {
            if (rngFichasLibro == null)
            {
                rngFichasLibro = new List<FichaLibro>();
            }

            rngFichasLibro.Add(fichaLibro);
        }

        public void AddFichaLibroVol(FichaLibroVol fichaLibroVol)
        {
            if (rngFichasLibroVol == null)
            {
                rngFichasLibroVol = new List<FichaLibroVol>();
            }

            rngFichasLibroVol.Add(fichaLibroVol);
        }

        public void AddFichaRevista(FichaRevista fichaRevista)
        {
            if (rngFichasRevista == null)
            {
                rngFichasRevista = new List<FichaRevista>();
            }

            rngFichasRevista.Add(fichaRevista);
        }

        public void AddFichaDVD(FichaDVD fichaDVD)
        {
            if (rngFichasDVD == null)
            {
                rngFichasDVD = new List<FichaDVD>();
            }

            rngFichasDVD.Add(fichaDVD);
        }

        public void MostrarFLibro()
        {
            if (rngFichasLibro == null)
            {
                Utils.PrintHeaderMessage("No hay ninguna ficha almacenada");
            }
            else
            {
                for(int i=0; i< rngFichasLibro.Count; i++)
                {
                    Console.Clear();

                    Utils.PrintColorMessage("Fichas de Libros registrados\n", ConsoleColor.Yellow);
                    rngFichasLibro[i].Imprimir();

                    PulsaTecla(ref i, rngFichasLibro.Count);
                }
            }
        }

        public void MostrarFLibroVol()
        {
            if (rngFichasLibroVol == null)
            {
                Utils.PrintHeaderMessage("No hay ninguna ficha almacenada");
            }
            else
            {
                for (int i = 0; i < rngFichasLibroVol.Count; i++)
                {
                    Console.Clear();

                    Utils.PrintColorMessage("Fichas de Volúmenes registrados\n", ConsoleColor.Yellow);
                    rngFichasLibroVol[i].Imprimir();

                    PulsaTecla(ref i, rngFichasLibroVol.Count);
                }
            }
        }

        public void MostrarFRevista()
        {
            if (rngFichasRevista == null)
            {
                Utils.PrintHeaderMessage("No hay ninguna ficha almacenada");
            }
            else
            {
                for (int i = 0; i < rngFichasRevista.Count; i++)
                {
                    Console.Clear();

                    Utils.PrintColorMessage("Fichas de Revistas registradas\n", ConsoleColor.Yellow);
                    rngFichasRevista[i].Imprimir();

                    PulsaTecla(ref i, rngFichasRevista.Count);
                }
            }
        }

        public void MostrarFDVD()
        {
            if (rngFichasDVD == null)
            {
                Utils.PrintHeaderMessage("No hay ninguna ficha almacenada");
            }
            else
            {
                for (int i = 0; i < rngFichasDVD.Count; i++)
                {
                    Console.Clear();

                    Utils.PrintColorMessage("Fichas de DVD's registrados\n", ConsoleColor.Yellow);
                    rngFichasDVD[i].Imprimir();

                    PulsaTecla(ref i, rngFichasDVD.Count);
                }
            }
        }

        public static void PulsaTecla(ref int regActual, int totalRegs)
        {
            Console.WriteLine(
                        "\nPulsa enter para continuar / salir (Esc -> Salir). Mostrando: {0} de {1}",
                        regActual+1,
                        totalRegs
                        );

            ConsoleKey cKey;
            do
            {
                cKey = Console.ReadKey(true).Key;

                if (cKey == ConsoleKey.Escape) { regActual = totalRegs; }

            } while (!(cKey == ConsoleKey.Enter || cKey==ConsoleKey.Escape));
        }

        #endregion Métodos
    }
}
