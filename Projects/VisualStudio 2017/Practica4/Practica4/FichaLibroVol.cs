﻿/*
* PRÁCTICA.............: Práctica 4.
* NOMBRE y APELLIDOS...: Juan de Dios Delgado Bermúdez
* CURSO y GRUPO........: 2o Desarrollo de Interfaces
* TÍTULO de la PRÁCTICA: Diseño de clases. Herencia y polimorfismo.
* FECHA de ENTREGA.....: 22 de Noviembre de 2017
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica4
{
    class FichaLibroVol : FichaLibro
    {
        #region Variables

        int _intNumVol;

        #endregion Variables

        #region Constructor
        
        public FichaLibroVol(string strTitulo, string strReferencia, string strAutor, string strEditorial, int intNumEjemplares, int intNumVol) 
            : base(strTitulo, strAutor, strReferencia, strEditorial, intNumEjemplares)
        {
            _intNumVol = intNumVol;
        }

        #endregion Cosntructor

        #region Métodos

        public override void Imprimir()
        {
            base.Imprimir();

            Console.Write(
                "Nº Volumen...: \t{0}\n",
                _intNumVol
                );
        }

        #endregion Métodos
    }
}
