﻿/*
* PRÁCTICA.............: Práctica 4.
* NOMBRE y APELLIDOS...: Juan de Dios Delgado Bermúdez
* CURSO y GRUPO........: 2o Desarrollo de Interfaces
* TÍTULO de la PRÁCTICA: Diseño de clases. Herencia y polimorfismo.
* FECHA de ENTREGA.....: 22 de Noviembre de 2017
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica4
{
    abstract class Ficha
    {
        #region Variables

        string _strReferencia;
        string _strTitulo;
        int _intNumEjemplares;
        static int _intNumOrden;

        #endregion Variables  

        #region Constructor

        public Ficha(string strTitulo, int intNumEjemplares, string strReferencia)
        {
            _intNumOrden++;
            _strTitulo = strTitulo;
            _intNumEjemplares = intNumEjemplares;

            //Agrega solo letras
            _strReferencia = "";
            for (int i=0; i<strReferencia.Length;i++)
            {
                if(strReferencia [i]!= ' ' && _strReferencia.Length<3)
                {
                    _strReferencia += strReferencia[i];
                }
            }
            _strReferencia += "/" + _intNumOrden;
        }

        #endregion Constructor

        #region Métodos

        public virtual void Imprimir()
        {
            Console.Write(
                "Referencia...: \t{0}\n" +
                "Titulo.......: \t{1}\n" +
                "Nº Ejemplares: \t{2}\n",
                _strReferencia,
                _strTitulo,
                _intNumEjemplares
                );
        }

        #endregion Métodos
    }
}
