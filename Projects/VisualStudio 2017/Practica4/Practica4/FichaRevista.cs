﻿/*
* PRÁCTICA.............: Práctica 4.
* NOMBRE y APELLIDOS...: Juan de Dios Delgado Bermúdez
* CURSO y GRUPO........: 2o Desarrollo de Interfaces
* TÍTULO de la PRÁCTICA: Diseño de clases. Herencia y polimorfismo.
* FECHA de ENTREGA.....: 22 de Noviembre de 2017
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica4
{
    class FichaRevista : Ficha
    {
        #region Variables

        int _intNumero;
        int _intAnyo;

        #endregion Variables

        #region Constructor
        
        public FichaRevista(string strTitulo, string strReferencia, int intNumEjemplares, int intNumero, int intAnyo) 
            : base(strTitulo, intNumEjemplares, strReferencia)
        {
            _intNumero = intNumero;
            _intAnyo = intAnyo;
        }

        public override void Imprimir()
        {
            base.Imprimir();

            Console.Write(
                "Numero.......: \t{0}\n" +
                "Año..........: \t{1}\n",
                _intNumero,
                _intAnyo
                );
        }

        #endregion Constructor
    }
}
