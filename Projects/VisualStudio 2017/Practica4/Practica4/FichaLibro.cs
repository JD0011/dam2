﻿/*
* PRÁCTICA.............: Práctica 4.
* NOMBRE y APELLIDOS...: Juan de Dios Delgado Bermúdez
* CURSO y GRUPO........: 2o Desarrollo de Interfaces
* TÍTULO de la PRÁCTICA: Diseño de clases. Herencia y polimorfismo.
* FECHA de ENTREGA.....: 22 de Noviembre de 2017
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica4
{
    class FichaLibro : Ficha
    {
        #region Variables

        string _strAutor;
        string _strEditorial;

        #endregion Variables

        #region Constructor
        
        public FichaLibro(string strTitulo, string strAutor, string strReferencia, string strEditorial, int intNumEjemplares)
            :base(strTitulo, intNumEjemplares, strReferencia)
        {
            _strAutor = strAutor;
            _strEditorial = strEditorial;
        }

        public override void Imprimir()
        {
            base.Imprimir();

            Console.Write(
                "Autor........: \t{0}\n" +
                "Editorial....: \t{1}\n",
                _strAutor,
                _strEditorial
                );
        }

        #endregion Constructor
    }
}
