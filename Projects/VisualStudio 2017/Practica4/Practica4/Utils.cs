﻿/*
* PRÁCTICA.............: Práctica 4.
* NOMBRE y APELLIDOS...: Juan de Dios Delgado Bermúdez
* CURSO y GRUPO........: 2o Desarrollo de Interfaces
* TÍTULO de la PRÁCTICA: Diseño de clases. Herencia y polimorfismo.
* FECHA de ENTREGA.....: 22 de Noviembre de 2017
*/

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Practica4
{
    static class Utils
    {
        #region Métodos
        
        public static string LeerCadena(int minLength, int maxLength)
        {
            Point ptPosicionCursor;
            string strRespuesta;
            bool exitCondition;

            strRespuesta = "";
            exitCondition = false;
            ptPosicionCursor = new Point(Console.CursorLeft, Console.CursorTop);
            while (!exitCondition)
            {
                Console.ForegroundColor = ConsoleColor.Cyan;
                strRespuesta = Console.ReadLine();
                Console.ResetColor();

                if (strRespuesta.Trim().Length == 0)
                {
                    PrintHeaderMessage("\aDebe introducir algún carácter (longitud: " + minLength + " a " + maxLength + ")");
                    LimpiarEspacio(ptPosicionCursor, strRespuesta.Length);
                }
                else if (!FormatoCorrecto("^[a-zA-ZñÑáéíóúÁÉÍÓÚ. ]{" + minLength + "," + maxLength + "}$", strRespuesta.Trim()))
                {
                    PrintHeaderMessage(
                        "\aDebe introducir letras y puntos exclusivamente (longitud: " + minLength + " a "+maxLength+")");
                    LimpiarEspacio(ptPosicionCursor, strRespuesta.Length);
                }
                else
                {
                    exitCondition = true;
                }
            }

            return strRespuesta.Trim();
        }

        public static string LeerCadenayNumeros(int minLength, int maxLength)
        {
            Point ptPosicionCursor;
            string strRespuesta;
            bool exitCondition;

            strRespuesta = "";
            exitCondition = false;
            ptPosicionCursor = new Point(Console.CursorLeft, Console.CursorTop);
            while (!exitCondition)
            {
                Console.ForegroundColor = ConsoleColor.Cyan;
                strRespuesta = Console.ReadLine();
                Console.ResetColor();

                if (strRespuesta.Trim().Length == 0)
                {
                    PrintHeaderMessage("\aDebe introducir algún carácter (longitud: " + minLength + " a " + maxLength + ")");
                    LimpiarEspacio(ptPosicionCursor, strRespuesta.Length);
                }
                else if (!FormatoCorrecto("^[a-zA-ZñÑáéíóúÁÉÍÓÚ0-9. ]{" + minLength + "," + maxLength + "}$", strRespuesta.Trim()))
                {
                    PrintHeaderMessage(
                        "\aDebe introducir letras, puntos y números exclusivamente (longitud: " + minLength + " a " + maxLength + ")");
                    LimpiarEspacio(ptPosicionCursor, strRespuesta.Length);
                }
                else
                {
                    exitCondition = true;
                }
            }

            return strRespuesta.Trim();
        }

        public static int LeerEntero(int min, int max)
        {
            Point ptPosicionCursor;
            string strAux;
            int intRespuesta;
            bool exitCondition;
            
            intRespuesta = 0;
            ptPosicionCursor = new Point(Console.CursorLeft, Console.CursorTop);
            exitCondition = false;
            while (!exitCondition)
            {
                Console.ForegroundColor = ConsoleColor.Cyan;
                strAux = Console.ReadLine();
                Console.ResetColor();

                try
                {
                    intRespuesta = int.Parse(strAux);
                }
                catch (FormatException)
                {
                    PrintHeaderMessage("\aDebe introducir un número entero entre "+min+" y "+max);
                    LimpiarEspacio(ptPosicionCursor, strAux.Length);
                    continue;
                }
                catch (OverflowException)
                {
                    PrintHeaderMessage("\aDebe introducir un número entero entre " + min + " y " + max);
                    LimpiarEspacio(ptPosicionCursor, strAux.Length);
                    continue;
                }

                if (intRespuesta < min || intRespuesta > max)
                {
                    PrintHeaderMessage("\aDebe introducir un número entero entre " + min + " y " + max);
                    LimpiarEspacio(ptPosicionCursor, strAux.Length);
                    continue;
                }

                exitCondition = true;
            }

            return intRespuesta;
        }
        
        public static void PrintHeaderMessage(string strMessage)
        {
            Point ptPosicionCursor;

            ptPosicionCursor = new Point(Console.CursorLeft, Console.CursorTop);

            Console.SetCursorPosition(0, 0);

            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write(strMessage+" (Espere 2.5 segundos)");
            Console.SetCursorPosition(ptPosicionCursor.X, ptPosicionCursor.Y);
            Console.ResetColor();

            System.Threading.Thread.Sleep(2500);

            Console.SetCursorPosition(0, 0);
            Console.Write(new string(' ', Console.WindowWidth));

            //Limpia buffer
            while (Console.KeyAvailable)
            {
                Console.ReadKey(true);
            }

            Console.SetCursorPosition(ptPosicionCursor.X, ptPosicionCursor.Y);
        }

        public static void PrintColorMessage(string message, ConsoleColor cColor)
        {
            Console.ForegroundColor = cColor;
            Console.WriteLine(message);
            Console.ResetColor();
        }

        public static void LimpiarEspacio(Point ptPosCursorInicial, int intEspacios)
        {
            Console.SetCursorPosition(ptPosCursorInicial.X, ptPosCursorInicial.Y);
            Console.Write(new string(' ', intEspacios));
            Console.SetCursorPosition(ptPosCursorInicial.X, ptPosCursorInicial.Y);
        }

        public static bool FormatoCorrecto(string strPatron, string strMessage)
        {
            return new Regex(strPatron).IsMatch(strMessage);
        }

        #endregion Métodos
    }
}
