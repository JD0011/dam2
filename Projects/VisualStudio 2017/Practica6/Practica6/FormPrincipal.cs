﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;

namespace Practica6
{
    public partial class FormPrincipal : Form
    {
        Empleado _emp;
        Nomina _nom;
        List<TextBox> _listContent;

        public FormPrincipal()
        {
            InitializeComponent();

            _emp = new Empleado();
            _nom = new Nomina(_emp);

            _listContent = new List<TextBox>();

            _listContent.Add(tbCategoria);
            _listContent.Add(tbFechLiquidacion);
            _listContent.Add(tbNHijos);
            _listContent.Add(tbNHorasExtras);
            _listContent.Add(tbNIF);
            _listContent.Add(tbNombre);
            _listContent.Add(tbNTrienios);

            //Oculta la nómina
            HideNomina();
        }

        #region Handler de eventos

        private void tbNombre_Leave(object sender, EventArgs e)
        {
            if (btnCerrar.Focused) { return; }

            if (!new Validator().ValidateWithRegex("^[a-zA-ZÁáÀàÉéÈèÍíÌìÓóÒòÚúÙùÑñüÜ ]{2,20}$", tbNombre.Text))
            {
                MessageBox.Show(
                    "El nombre debe estar formado por letras (longitud: 2 a 20 caracteres)",
                    "Error en nombre",
                MessageBoxButtons.OK, MessageBoxIcon.Error);

                tbNombre.Text = "";
                tbNombre.Focus();
            }
            else
            {
                _emp.Nombre = tbNombre.Text;
            }
        }

        private void tbNIF_Leave(object sender, EventArgs e)
        {
            if (btnCerrar.Focused) { return; }

            if (!new Validator().ValidateWithRegex("^[0-9]{8}[a-zA-Z]{1}$", tbNIF.Text))
            {
                MessageBox.Show(
                    "El formato requerido para el NIF es de 8 números y una letra, sin separación",
                    "Error en NIF",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);

                tbNIF.Text = "";
                tbNIF.Focus();
            }
            else
            {
                //Validación de letra del NIF
                char[] letrasNIF = { 'T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D', 'X', 'B','N','J','Z','S','Q','V','H'
                ,'L','C','K','E'};
                int indexLetra;

                string strNumero = "";
                foreach(char c in tbNIF.Text.Substring(0,8))
                {
                    strNumero += c;
                }

                indexLetra = int.Parse(strNumero) % 23;

                if (!Char.ToUpper(tbNIF.Text[8]).Equals(letrasNIF[indexLetra]))
                {
                    MessageBox.Show(
                        "La letra del NIF es incorrecta",
                        "El NIF no existe",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);

                    tbNIF.Text = "";
                    tbNIF.Focus();
                }
                
                
                _emp.Nif = tbNIF.Text;
            }
        }

        private void tbFechLiquidacion_Leave(object sender, EventArgs e)
        {
            if (btnCerrar.Focused) { return; }

            if (!new Validator().ValidateWithRegex("^[0-9]{1,2}/[0-9]{1,2}/[0-9]{4}$", tbFechLiquidacion.Text))
            {
                MessageBox.Show(
                    "La fecha debe representarse con el siguiente formato: dd/mm/aaaa",
                    "Error en la fecha de liquidación",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);

                tbFechLiquidacion.Text = "";
                tbFechLiquidacion.Focus();
            }
            else
            {
                _nom.FechNomina = DateTime.Parse(tbFechLiquidacion.Text);

                if(_nom.FechNomina.Year < 1970 || _nom.FechNomina.Year > DateTime.Now.Year)
                {
                    MessageBox.Show(
                        "La fecha debe estar entre 1970 y el año actual",
                        "Error en la fecha de liquidación",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);

                    tbFechLiquidacion.Text = "";
                    tbFechLiquidacion.Focus();
                }
            }
        }

        private void tbCategoria_Leave(object sender, EventArgs e)
        {
            if (btnCerrar.Focused) { return; }

            bool isCorrect;
            try
            {
                isCorrect = new Validator().ValidateNumero(1, 3, int.Parse(tbCategoria.Text));
            }
            catch (Exception)
            {
                isCorrect = false;
            }

            if (!isCorrect)
            {
                MessageBox.Show(
                    "La categoría solo admite los valores 1, 2 y 3",
                    "Error en la categoría",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);

                tbCategoria.Text = "";
                tbCategoria.Focus();
            }
            else
            {
                _emp.Categoria = int.Parse(tbCategoria.Text);
            }
        }

        private void tbHijos_Leave(object sender, EventArgs e)
        {
            if (btnCerrar.Focused) { return; }

            bool isCorrect;
            try
            {
                isCorrect = new Validator().ValidateNumero(0, 15, int.Parse(tbNHijos.Text));
            }
            catch (Exception)
            {
                isCorrect = false;
            }

            if (!isCorrect)
            {
                MessageBox.Show(
                    "El Nº de Hijos debe ser un número entre 0 y 15",
                    "Error en el número de hijos",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);

                tbNHijos.Text = "";
                tbNHijos.Focus();
            }
            else
            {
                _emp.NumHijos = int.Parse(tbNHijos.Text);
            }
        }

        private void tbTrienios_Leave(object sender, EventArgs e)
        {
            if (btnCerrar.Focused) { return; }

            bool isCorrect;
            try
            {
                isCorrect = new Validator().ValidateNumero(0, 12, int.Parse(tbNTrienios.Text));
            }
            catch (Exception)
            {
                isCorrect = false;
            }

            if (!isCorrect)
            {
                MessageBox.Show(
                    "El valor de los trienios debe estar comprendido entre 0 y 12",
                    "Error en el número de trienios",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);

                tbNTrienios.Text = "";
                tbNTrienios.Focus();
            }
            else
            {
                _emp.NumTrienios = int.Parse(tbNTrienios.Text);
            }
        }

        private void tbHorasExtras_Leave(object sender, EventArgs e)
        {
            if (btnCerrar.Focused) { return; }

            bool isCorrect;
            try
            {
                isCorrect = new Validator().ValidateNumero(0, 80, int.Parse(tbNHorasExtras.Text));
            }
            catch (Exception)
            {
                isCorrect = false;
            }

            if (!isCorrect)
            {
                MessageBox.Show(
                    " El Nº horas extras debe estar entre 0 y 80",
                    "Error en el número de horas extras",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);

                tbNHorasExtras.Text = "";
                tbNHorasExtras.Focus();
            }
            else
            {
                _nom.NumHorasExtras = int.Parse(tbNHorasExtras.Text);
            }
        }

        private void btnFin_Click(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }

        
        private void textBox_CambioTexto(object sender, EventArgs e)
        {
            //No activa el botón a menos que los tb tengan texto
            if (Utils.AllHasText(_listContent))
            {
                btnGeneraNom.Enabled = true;
            }
        }

        private void btnGeneraNom_Click(object sender, EventArgs e)
        {
            if (Utils.AllHasText(_listContent)){

                foreach (TextBox tb in _listContent)
                {
                    tb.Enabled = false;
                }

                //NOMINA
                tbAntiguedad.Text = _nom.ImporteAntiguedad().ToString();
                tbCSegSoc.Text = _nom.CotizacionSegSoc().ToString("#.##");
                tbCSegDesem.Text = _nom.CotizacionSegDes().ToString("#.##");
                tbHorasExtras.Text = _nom.ImporteHorasExtras().ToString("#.##");
                tbIRPF.Text = _nom.RetencionIRPF().ToString("#.##");
                tbLiquido.Text = _nom.LiquidoPercibir().ToString("#.##");
                tbPagaExtra.Text = _nom.DevengosPagaExtra().ToString("#.##");
                tbSalarioBase.Text = _nom.SalarioBase().ToString("#.##");
                tbTotalDescuento.Text = _nom.TotalDescuentos().ToString("#.##");
                tbTotalDevengo.Text = _nom.TotalDevengado().ToString("#.##");

                btnGeneraNom.Enabled = false;

                btnModificar.Enabled = true;
                btnGuardar.Enabled = true;

                ShowNomina();
            }

        }

        public void ActivaBtnGeneraNom()
        {
            btnGeneraNom.Enabled = true;
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            btnCerrar.Focus();
            this.Dispose();
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            foreach(TextBox tb in _listContent)
            {
                tb.Enabled = true;
            }
            
            btnModificar.Enabled = false;
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfdNomina = new SaveFileDialog();

            sfdNomina.Filter = "Fichero de texto | *.txt";
            sfdNomina.Title = "Guardar nómina salarial de " + _emp.Nombre;
            sfdNomina.FileName = "Nómina " + _emp.Nombre + " " + _nom.FechNomina.ToString("MM-yyyy");

            if (sfdNomina.ShowDialog() == DialogResult.OK && sfdNomina.FileName != "")
            {
                StreamWriter sw = new StreamWriter(sfdNomina.OpenFile());
                sw.Write(_nom.GeneraNominaSalarial());
                sw.Close();
            }
        }

        #endregion Handler de eventos

        #region Eventos de validación

        private void SoloLetrasTabEnter(object sender, KeyPressEventArgs e)
        {
            if (!char.IsLetter(e.KeyChar))
            {
                if (!char.IsControl(e.KeyChar))
                {
                    e.Handled = true;
                }
                else if (e.KeyChar == (char)Keys.Return)
                {
                    e.Handled = true;
                    SendKeys.Send("{TAB}");
                }
            }
        }

        private void SoloNumerosTabEnter(object sender, KeyPressEventArgs e)
        {
            if (!char.IsNumber(e.KeyChar))
            {
                if (!char.IsControl(e.KeyChar))
                {
                    e.Handled = true;
                }
                else if (e.KeyChar == (char)Keys.Return)
                {
                    SendKeys.Send("{TAB}");
                }
            }
        }

        private void SoloLetrasNumeros(object sender, KeyPressEventArgs e)
        {
            if (!char.IsLetterOrDigit(e.KeyChar))
            {
                if (!char.IsControl(e.KeyChar))
                {
                    e.Handled = true;
                }
                else if (e.KeyChar == (char)Keys.Return)
                {
                    SendKeys.Send("{TAB}");
                }
            }
        }

        private void SoloNumerosBarra(object sender, KeyPressEventArgs e)
        {
            if (!(e.KeyChar.Equals('/') || char.IsNumber(e.KeyChar)))
            {
                if (!char.IsControl(e.KeyChar))
                {
                    e.Handled = true;
                }
                else if (e.KeyChar == (char)Keys.Return)
                {
                    SendKeys.Send("{TAB}");
                }
            }
        }

        #endregion Eventos de validación

        public void HideNomina()
        {
            gbDevengos.Hide();
            gbDescuentos.Hide();
            gbLiquido.Hide();
            pnlBotonesNomina.Hide();
        }

        public void ShowNomina()
        {
            gbDevengos.Show();
            gbDescuentos.Show();
            gbLiquido.Show();
            pnlBotonesNomina.Show();
        }
    }
}
