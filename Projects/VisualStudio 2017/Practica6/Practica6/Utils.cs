﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Practica6
{
    class Utils
    {
        public static bool AllHasText(List<TextBox> content)
        {
            bool hasText;

            hasText = true;
            foreach(TextBox tb in content)
            {
                if (tb.Text.Equals(""))
                {
                    hasText = false;
                    break;
                }
            }

            return hasText;
        }

    }
}
