﻿namespace Practica6
{
    partial class FormPrincipal
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormPrincipal));
            this.btnGeneraNom = new System.Windows.Forms.Button();
            this.gbPrincipal = new System.Windows.Forms.GroupBox();
            this.flowLayoutPanel5 = new System.Windows.Forms.FlowLayoutPanel();
            this.label7 = new System.Windows.Forms.Label();
            this.tbFechLiquidacion = new System.Windows.Forms.TextBox();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.lblCategoria = new System.Windows.Forms.Label();
            this.tbCategoria = new System.Windows.Forms.TextBox();
            this.lblNHijos = new System.Windows.Forms.Label();
            this.tbNHijos = new System.Windows.Forms.TextBox();
            this.lblNTrienios = new System.Windows.Forms.Label();
            this.tbNTrienios = new System.Windows.Forms.TextBox();
            this.lblNHorasExtras = new System.Windows.Forms.Label();
            this.tbNHorasExtras = new System.Windows.Forms.TextBox();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.lblNombre = new System.Windows.Forms.Label();
            this.tbNombre = new System.Windows.Forms.TextBox();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.lblNif = new System.Windows.Forms.Label();
            this.tbNIF = new System.Windows.Forms.TextBox();
            this.gbLiquido = new System.Windows.Forms.GroupBox();
            this.tbLiquido = new System.Windows.Forms.TextBox();
            this.lblLiquido = new System.Windows.Forms.Label();
            this.gbDescuentos = new System.Windows.Forms.GroupBox();
            this.lblCSegSocial = new System.Windows.Forms.Label();
            this.lblTDescuentos = new System.Windows.Forms.Label();
            this.lblCSegDesemp = new System.Windows.Forms.Label();
            this.lblIRPF = new System.Windows.Forms.Label();
            this.tbCSegSoc = new System.Windows.Forms.TextBox();
            this.tbCSegDesem = new System.Windows.Forms.TextBox();
            this.tbIRPF = new System.Windows.Forms.TextBox();
            this.tbTotalDescuento = new System.Windows.Forms.TextBox();
            this.gbDevengos = new System.Windows.Forms.GroupBox();
            this.lblSalarioBase = new System.Windows.Forms.Label();
            this.lblAntiguedad = new System.Windows.Forms.Label();
            this.lblPagaExtra = new System.Windows.Forms.Label();
            this.lblHExtras = new System.Windows.Forms.Label();
            this.tbSalarioBase = new System.Windows.Forms.TextBox();
            this.tbAntiguedad = new System.Windows.Forms.TextBox();
            this.tbHorasExtras = new System.Windows.Forms.TextBox();
            this.tbTotalDevengo = new System.Windows.Forms.TextBox();
            this.tbPagaExtra = new System.Windows.Forms.TextBox();
            this.lblTDevengos = new System.Windows.Forms.Label();
            this.btnCerrar = new System.Windows.Forms.Button();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.btnModificar = new System.Windows.Forms.Button();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.pnlBotonesNomina = new System.Windows.Forms.Panel();
            this.gbPrincipal.SuspendLayout();
            this.flowLayoutPanel5.SuspendLayout();
            this.flowLayoutPanel4.SuspendLayout();
            this.flowLayoutPanel3.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            this.gbLiquido.SuspendLayout();
            this.gbDescuentos.SuspendLayout();
            this.gbDevengos.SuspendLayout();
            this.pnlBotonesNomina.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnGeneraNom
            // 
            this.btnGeneraNom.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnGeneraNom.Enabled = false;
            this.btnGeneraNom.Location = new System.Drawing.Point(335, 86);
            this.btnGeneraNom.Name = "btnGeneraNom";
            this.btnGeneraNom.Size = new System.Drawing.Size(103, 23);
            this.btnGeneraNom.TabIndex = 7;
            this.btnGeneraNom.Text = "Generar nómina";
            this.btnGeneraNom.UseVisualStyleBackColor = true;
            this.btnGeneraNom.Click += new System.EventHandler(this.btnGeneraNom_Click);
            // 
            // gbPrincipal
            // 
            this.gbPrincipal.Controls.Add(this.btnGeneraNom);
            this.gbPrincipal.Controls.Add(this.flowLayoutPanel5);
            this.gbPrincipal.Controls.Add(this.flowLayoutPanel4);
            this.gbPrincipal.Controls.Add(this.flowLayoutPanel3);
            this.gbPrincipal.Controls.Add(this.flowLayoutPanel2);
            this.gbPrincipal.Location = new System.Drawing.Point(12, 12);
            this.gbPrincipal.Name = "gbPrincipal";
            this.gbPrincipal.Size = new System.Drawing.Size(458, 117);
            this.gbPrincipal.TabIndex = 0;
            this.gbPrincipal.TabStop = false;
            this.gbPrincipal.Text = "Información de empleado";
            // 
            // flowLayoutPanel5
            // 
            this.flowLayoutPanel5.AutoSize = true;
            this.flowLayoutPanel5.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.flowLayoutPanel5.Controls.Add(this.label7);
            this.flowLayoutPanel5.Controls.Add(this.tbFechLiquidacion);
            this.flowLayoutPanel5.Location = new System.Drawing.Point(10, 83);
            this.flowLayoutPanel5.Name = "flowLayoutPanel5";
            this.flowLayoutPanel5.Size = new System.Drawing.Size(223, 26);
            this.flowLayoutPanel5.TabIndex = 4;
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(3, 6);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(111, 13);
            this.label7.TabIndex = 5;
            this.label7.Text = "Periodo de liquidación";
            // 
            // tbFechLiquidacion
            // 
            this.tbFechLiquidacion.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbFechLiquidacion.Location = new System.Drawing.Point(120, 3);
            this.tbFechLiquidacion.Name = "tbFechLiquidacion";
            this.tbFechLiquidacion.Size = new System.Drawing.Size(100, 20);
            this.tbFechLiquidacion.TabIndex = 6;
            this.tbFechLiquidacion.TextChanged += new System.EventHandler(this.textBox_CambioTexto);
            this.tbFechLiquidacion.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.SoloNumerosBarra);
            this.tbFechLiquidacion.Leave += new System.EventHandler(this.tbFechLiquidacion_Leave);
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.flowLayoutPanel4.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.flowLayoutPanel4.BackColor = System.Drawing.SystemColors.Control;
            this.flowLayoutPanel4.Controls.Add(this.lblCategoria);
            this.flowLayoutPanel4.Controls.Add(this.tbCategoria);
            this.flowLayoutPanel4.Controls.Add(this.lblNHijos);
            this.flowLayoutPanel4.Controls.Add(this.tbNHijos);
            this.flowLayoutPanel4.Controls.Add(this.lblNTrienios);
            this.flowLayoutPanel4.Controls.Add(this.tbNTrienios);
            this.flowLayoutPanel4.Controls.Add(this.lblNHorasExtras);
            this.flowLayoutPanel4.Controls.Add(this.tbNHorasExtras);
            this.flowLayoutPanel4.Location = new System.Drawing.Point(10, 51);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(428, 26);
            this.flowLayoutPanel4.TabIndex = 3;
            // 
            // lblCategoria
            // 
            this.lblCategoria.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblCategoria.AutoSize = true;
            this.lblCategoria.Location = new System.Drawing.Point(3, 6);
            this.lblCategoria.Name = "lblCategoria";
            this.lblCategoria.Size = new System.Drawing.Size(54, 13);
            this.lblCategoria.TabIndex = 1;
            this.lblCategoria.Text = "Categoría";
            // 
            // tbCategoria
            // 
            this.tbCategoria.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbCategoria.Location = new System.Drawing.Point(63, 3);
            this.tbCategoria.Name = "tbCategoria";
            this.tbCategoria.Size = new System.Drawing.Size(25, 20);
            this.tbCategoria.TabIndex = 3;
            this.tbCategoria.TextChanged += new System.EventHandler(this.textBox_CambioTexto);
            this.tbCategoria.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.SoloNumerosTabEnter);
            this.tbCategoria.Leave += new System.EventHandler(this.tbCategoria_Leave);
            // 
            // lblNHijos
            // 
            this.lblNHijos.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblNHijos.AutoSize = true;
            this.lblNHijos.Location = new System.Drawing.Point(94, 6);
            this.lblNHijos.Name = "lblNHijos";
            this.lblNHijos.Size = new System.Drawing.Size(45, 13);
            this.lblNHijos.TabIndex = 3;
            this.lblNHijos.Text = "Nº Hijos";
            // 
            // tbNHijos
            // 
            this.tbNHijos.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbNHijos.Location = new System.Drawing.Point(145, 3);
            this.tbNHijos.Name = "tbNHijos";
            this.tbNHijos.Size = new System.Drawing.Size(25, 20);
            this.tbNHijos.TabIndex = 4;
            this.tbNHijos.TextChanged += new System.EventHandler(this.textBox_CambioTexto);
            this.tbNHijos.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.SoloNumerosTabEnter);
            this.tbNHijos.Leave += new System.EventHandler(this.tbHijos_Leave);
            // 
            // lblNTrienios
            // 
            this.lblNTrienios.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblNTrienios.AutoSize = true;
            this.lblNTrienios.Location = new System.Drawing.Point(176, 6);
            this.lblNTrienios.Name = "lblNTrienios";
            this.lblNTrienios.Size = new System.Drawing.Size(55, 13);
            this.lblNTrienios.TabIndex = 5;
            this.lblNTrienios.Text = "Nº trienios";
            // 
            // tbNTrienios
            // 
            this.tbNTrienios.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbNTrienios.Location = new System.Drawing.Point(237, 3);
            this.tbNTrienios.Name = "tbNTrienios";
            this.tbNTrienios.Size = new System.Drawing.Size(25, 20);
            this.tbNTrienios.TabIndex = 6;
            this.tbNTrienios.TextChanged += new System.EventHandler(this.textBox_CambioTexto);
            this.tbNTrienios.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.SoloNumerosTabEnter);
            this.tbNTrienios.Leave += new System.EventHandler(this.tbTrienios_Leave);
            // 
            // lblNHorasExtras
            // 
            this.lblNHorasExtras.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblNHorasExtras.AutoSize = true;
            this.lblNHorasExtras.Location = new System.Drawing.Point(268, 6);
            this.lblNHorasExtras.Name = "lblNHorasExtras";
            this.lblNHorasExtras.Size = new System.Drawing.Size(79, 13);
            this.lblNHorasExtras.TabIndex = 7;
            this.lblNHorasExtras.Text = "Nº horas extras";
            // 
            // tbNHorasExtras
            // 
            this.tbNHorasExtras.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbNHorasExtras.Location = new System.Drawing.Point(353, 3);
            this.tbNHorasExtras.Name = "tbNHorasExtras";
            this.tbNHorasExtras.Size = new System.Drawing.Size(25, 20);
            this.tbNHorasExtras.TabIndex = 8;
            this.tbNHorasExtras.TextChanged += new System.EventHandler(this.textBox_CambioTexto);
            this.tbNHorasExtras.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.SoloNumerosTabEnter);
            this.tbNHorasExtras.Leave += new System.EventHandler(this.tbHorasExtras_Leave);
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.flowLayoutPanel3.AutoSize = true;
            this.flowLayoutPanel3.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.flowLayoutPanel3.Controls.Add(this.lblNombre);
            this.flowLayoutPanel3.Controls.Add(this.tbNombre);
            this.flowLayoutPanel3.Location = new System.Drawing.Point(10, 19);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(217, 26);
            this.flowLayoutPanel3.TabIndex = 1;
            // 
            // lblNombre
            // 
            this.lblNombre.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblNombre.AutoSize = true;
            this.lblNombre.Location = new System.Drawing.Point(3, 6);
            this.lblNombre.Name = "lblNombre";
            this.lblNombre.Size = new System.Drawing.Size(44, 13);
            this.lblNombre.TabIndex = 1;
            this.lblNombre.Text = "Nombre";
            // 
            // tbNombre
            // 
            this.tbNombre.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbNombre.Location = new System.Drawing.Point(53, 3);
            this.tbNombre.Name = "tbNombre";
            this.tbNombre.Size = new System.Drawing.Size(161, 20);
            this.tbNombre.TabIndex = 1;
            this.tbNombre.TextChanged += new System.EventHandler(this.textBox_CambioTexto);
            this.tbNombre.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.SoloLetrasTabEnter);
            this.tbNombre.Leave += new System.EventHandler(this.tbNombre_Leave);
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.flowLayoutPanel2.AutoSize = true;
            this.flowLayoutPanel2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.flowLayoutPanel2.Controls.Add(this.lblNif);
            this.flowLayoutPanel2.Controls.Add(this.tbNIF);
            this.flowLayoutPanel2.Location = new System.Drawing.Point(302, 19);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(136, 26);
            this.flowLayoutPanel2.TabIndex = 2;
            // 
            // lblNif
            // 
            this.lblNif.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblNif.AutoSize = true;
            this.lblNif.Location = new System.Drawing.Point(3, 6);
            this.lblNif.Name = "lblNif";
            this.lblNif.Size = new System.Drawing.Size(24, 13);
            this.lblNif.TabIndex = 1;
            this.lblNif.Text = "NIF";
            // 
            // tbNIF
            // 
            this.tbNIF.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbNIF.Location = new System.Drawing.Point(33, 3);
            this.tbNIF.Name = "tbNIF";
            this.tbNIF.Size = new System.Drawing.Size(100, 20);
            this.tbNIF.TabIndex = 2;
            this.tbNIF.TextChanged += new System.EventHandler(this.textBox_CambioTexto);
            this.tbNIF.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.SoloLetrasNumeros);
            this.tbNIF.Leave += new System.EventHandler(this.tbNIF_Leave);
            // 
            // gbLiquido
            // 
            this.gbLiquido.Controls.Add(this.tbLiquido);
            this.gbLiquido.Controls.Add(this.lblLiquido);
            this.gbLiquido.Location = new System.Drawing.Point(12, 306);
            this.gbLiquido.Name = "gbLiquido";
            this.gbLiquido.Size = new System.Drawing.Size(208, 51);
            this.gbLiquido.TabIndex = 73;
            this.gbLiquido.TabStop = false;
            // 
            // tbLiquido
            // 
            this.tbLiquido.Location = new System.Drawing.Point(101, 18);
            this.tbLiquido.Name = "tbLiquido";
            this.tbLiquido.ReadOnly = true;
            this.tbLiquido.Size = new System.Drawing.Size(100, 20);
            this.tbLiquido.TabIndex = 59;
            // 
            // lblLiquido
            // 
            this.lblLiquido.AutoSize = true;
            this.lblLiquido.Location = new System.Drawing.Point(7, 21);
            this.lblLiquido.Name = "lblLiquido";
            this.lblLiquido.Size = new System.Drawing.Size(51, 13);
            this.lblLiquido.TabIndex = 49;
            this.lblLiquido.Text = "LÍQUIDO";
            // 
            // gbDescuentos
            // 
            this.gbDescuentos.Controls.Add(this.lblCSegSocial);
            this.gbDescuentos.Controls.Add(this.lblTDescuentos);
            this.gbDescuentos.Controls.Add(this.lblCSegDesemp);
            this.gbDescuentos.Controls.Add(this.lblIRPF);
            this.gbDescuentos.Controls.Add(this.tbCSegSoc);
            this.gbDescuentos.Controls.Add(this.tbCSegDesem);
            this.gbDescuentos.Controls.Add(this.tbIRPF);
            this.gbDescuentos.Controls.Add(this.tbTotalDescuento);
            this.gbDescuentos.Location = new System.Drawing.Point(234, 135);
            this.gbDescuentos.Name = "gbDescuentos";
            this.gbDescuentos.Size = new System.Drawing.Size(236, 165);
            this.gbDescuentos.TabIndex = 72;
            this.gbDescuentos.TabStop = false;
            this.gbDescuentos.Text = "Descuentos";
            // 
            // lblCSegSocial
            // 
            this.lblCSegSocial.AutoSize = true;
            this.lblCSegSocial.Location = new System.Drawing.Point(6, 25);
            this.lblCSegSocial.Name = "lblCSegSocial";
            this.lblCSegSocial.Size = new System.Drawing.Size(83, 13);
            this.lblCSegSocial.TabIndex = 41;
            this.lblCSegSocial.Text = "Cot. Seg. Social";
            // 
            // lblTDescuentos
            // 
            this.lblTDescuentos.AutoSize = true;
            this.lblTDescuentos.Location = new System.Drawing.Point(6, 133);
            this.lblTDescuentos.Name = "lblTDescuentos";
            this.lblTDescuentos.Size = new System.Drawing.Size(89, 13);
            this.lblTDescuentos.TabIndex = 46;
            this.lblTDescuentos.Text = "Total descuentos";
            // 
            // lblCSegDesemp
            // 
            this.lblCSegDesemp.AutoSize = true;
            this.lblCSegDesemp.Location = new System.Drawing.Point(6, 51);
            this.lblCSegDesemp.Name = "lblCSegDesemp";
            this.lblCSegDesemp.Size = new System.Drawing.Size(110, 13);
            this.lblCSegDesemp.TabIndex = 47;
            this.lblCSegDesemp.Text = "Cot. Seg.  Desempleo";
            // 
            // lblIRPF
            // 
            this.lblIRPF.AutoSize = true;
            this.lblIRPF.Location = new System.Drawing.Point(6, 77);
            this.lblIRPF.Name = "lblIRPF";
            this.lblIRPF.Size = new System.Drawing.Size(86, 13);
            this.lblIRPF.TabIndex = 48;
            this.lblIRPF.Text = "Rertención IRPF";
            // 
            // tbCSegSoc
            // 
            this.tbCSegSoc.Location = new System.Drawing.Point(122, 21);
            this.tbCSegSoc.Name = "tbCSegSoc";
            this.tbCSegSoc.ReadOnly = true;
            this.tbCSegSoc.Size = new System.Drawing.Size(100, 20);
            this.tbCSegSoc.TabIndex = 55;
            // 
            // tbCSegDesem
            // 
            this.tbCSegDesem.Location = new System.Drawing.Point(122, 48);
            this.tbCSegDesem.Name = "tbCSegDesem";
            this.tbCSegDesem.ReadOnly = true;
            this.tbCSegDesem.Size = new System.Drawing.Size(100, 20);
            this.tbCSegDesem.TabIndex = 56;
            // 
            // tbIRPF
            // 
            this.tbIRPF.Location = new System.Drawing.Point(122, 74);
            this.tbIRPF.Name = "tbIRPF";
            this.tbIRPF.ReadOnly = true;
            this.tbIRPF.Size = new System.Drawing.Size(100, 20);
            this.tbIRPF.TabIndex = 57;
            // 
            // tbTotalDescuento
            // 
            this.tbTotalDescuento.Location = new System.Drawing.Point(121, 130);
            this.tbTotalDescuento.Name = "tbTotalDescuento";
            this.tbTotalDescuento.ReadOnly = true;
            this.tbTotalDescuento.Size = new System.Drawing.Size(100, 20);
            this.tbTotalDescuento.TabIndex = 58;
            // 
            // gbDevengos
            // 
            this.gbDevengos.Controls.Add(this.lblSalarioBase);
            this.gbDevengos.Controls.Add(this.lblAntiguedad);
            this.gbDevengos.Controls.Add(this.lblPagaExtra);
            this.gbDevengos.Controls.Add(this.lblHExtras);
            this.gbDevengos.Controls.Add(this.tbSalarioBase);
            this.gbDevengos.Controls.Add(this.tbAntiguedad);
            this.gbDevengos.Controls.Add(this.tbHorasExtras);
            this.gbDevengos.Controls.Add(this.tbTotalDevengo);
            this.gbDevengos.Controls.Add(this.tbPagaExtra);
            this.gbDevengos.Controls.Add(this.lblTDevengos);
            this.gbDevengos.Location = new System.Drawing.Point(12, 135);
            this.gbDevengos.Name = "gbDevengos";
            this.gbDevengos.Size = new System.Drawing.Size(208, 165);
            this.gbDevengos.TabIndex = 71;
            this.gbDevengos.TabStop = false;
            this.gbDevengos.Text = "Devengos";
            // 
            // lblSalarioBase
            // 
            this.lblSalarioBase.AutoSize = true;
            this.lblSalarioBase.Location = new System.Drawing.Point(7, 22);
            this.lblSalarioBase.Name = "lblSalarioBase";
            this.lblSalarioBase.Size = new System.Drawing.Size(65, 13);
            this.lblSalarioBase.TabIndex = 40;
            this.lblSalarioBase.Text = "Salario base";
            // 
            // lblAntiguedad
            // 
            this.lblAntiguedad.AutoSize = true;
            this.lblAntiguedad.Location = new System.Drawing.Point(7, 51);
            this.lblAntiguedad.Name = "lblAntiguedad";
            this.lblAntiguedad.Size = new System.Drawing.Size(61, 13);
            this.lblAntiguedad.TabIndex = 42;
            this.lblAntiguedad.Text = "Antiguedad";
            // 
            // lblPagaExtra
            // 
            this.lblPagaExtra.AutoSize = true;
            this.lblPagaExtra.Location = new System.Drawing.Point(7, 100);
            this.lblPagaExtra.Name = "lblPagaExtra";
            this.lblPagaExtra.Size = new System.Drawing.Size(58, 13);
            this.lblPagaExtra.TabIndex = 43;
            this.lblPagaExtra.Text = "Paga extra";
            // 
            // lblHExtras
            // 
            this.lblHExtras.AutoSize = true;
            this.lblHExtras.Location = new System.Drawing.Point(7, 74);
            this.lblHExtras.Name = "lblHExtras";
            this.lblHExtras.Size = new System.Drawing.Size(67, 13);
            this.lblHExtras.TabIndex = 44;
            this.lblHExtras.Text = "Horas Extras";
            // 
            // tbSalarioBase
            // 
            this.tbSalarioBase.Location = new System.Drawing.Point(101, 22);
            this.tbSalarioBase.Name = "tbSalarioBase";
            this.tbSalarioBase.ReadOnly = true;
            this.tbSalarioBase.Size = new System.Drawing.Size(100, 20);
            this.tbSalarioBase.TabIndex = 50;
            // 
            // tbAntiguedad
            // 
            this.tbAntiguedad.Location = new System.Drawing.Point(101, 48);
            this.tbAntiguedad.Name = "tbAntiguedad";
            this.tbAntiguedad.ReadOnly = true;
            this.tbAntiguedad.Size = new System.Drawing.Size(100, 20);
            this.tbAntiguedad.TabIndex = 51;
            // 
            // tbHorasExtras
            // 
            this.tbHorasExtras.Location = new System.Drawing.Point(101, 71);
            this.tbHorasExtras.Name = "tbHorasExtras";
            this.tbHorasExtras.ReadOnly = true;
            this.tbHorasExtras.Size = new System.Drawing.Size(100, 20);
            this.tbHorasExtras.TabIndex = 52;
            // 
            // tbTotalDevengo
            // 
            this.tbTotalDevengo.Location = new System.Drawing.Point(101, 135);
            this.tbTotalDevengo.Name = "tbTotalDevengo";
            this.tbTotalDevengo.ReadOnly = true;
            this.tbTotalDevengo.Size = new System.Drawing.Size(100, 20);
            this.tbTotalDevengo.TabIndex = 54;
            // 
            // tbPagaExtra
            // 
            this.tbPagaExtra.Location = new System.Drawing.Point(101, 97);
            this.tbPagaExtra.Name = "tbPagaExtra";
            this.tbPagaExtra.ReadOnly = true;
            this.tbPagaExtra.Size = new System.Drawing.Size(100, 20);
            this.tbPagaExtra.TabIndex = 53;
            // 
            // lblTDevengos
            // 
            this.lblTDevengos.AutoSize = true;
            this.lblTDevengos.Location = new System.Drawing.Point(7, 135);
            this.lblTDevengos.Name = "lblTDevengos";
            this.lblTDevengos.Size = new System.Drawing.Size(81, 13);
            this.lblTDevengos.TabIndex = 45;
            this.lblTDevengos.Text = "Total devengos";
            // 
            // btnCerrar
            // 
            this.btnCerrar.Location = new System.Drawing.Point(395, 322);
            this.btnCerrar.Name = "btnCerrar";
            this.btnCerrar.Size = new System.Drawing.Size(75, 23);
            this.btnCerrar.TabIndex = 70;
            this.btnCerrar.Text = "Cerrar";
            this.btnCerrar.UseVisualStyleBackColor = true;
            this.btnCerrar.Click += new System.EventHandler(this.btnCerrar_Click);
            // 
            // btnGuardar
            // 
            this.btnGuardar.Location = new System.Drawing.Point(5, 0);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(75, 23);
            this.btnGuardar.TabIndex = 69;
            this.btnGuardar.Text = "Guardar";
            this.btnGuardar.UseVisualStyleBackColor = true;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // btnModificar
            // 
            this.btnModificar.Enabled = false;
            this.btnModificar.Location = new System.Drawing.Point(86, 0);
            this.btnModificar.Name = "btnModificar";
            this.btnModificar.Size = new System.Drawing.Size(75, 23);
            this.btnModificar.TabIndex = 74;
            this.btnModificar.Text = "Modificar";
            this.btnModificar.UseVisualStyleBackColor = true;
            this.btnModificar.Click += new System.EventHandler(this.btnModificar_Click);
            // 
            // pnlBotonesNomina
            // 
            this.pnlBotonesNomina.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.pnlBotonesNomina.Controls.Add(this.btnGuardar);
            this.pnlBotonesNomina.Controls.Add(this.btnModificar);
            this.pnlBotonesNomina.Location = new System.Drawing.Point(228, 322);
            this.pnlBotonesNomina.Name = "pnlBotonesNomina";
            this.pnlBotonesNomina.Size = new System.Drawing.Size(161, 23);
            this.pnlBotonesNomina.TabIndex = 75;
            // 
            // FormPrincipal
            // 
            this.ClientSize = new System.Drawing.Size(482, 369);
            this.Controls.Add(this.pnlBotonesNomina);
            this.Controls.Add(this.gbLiquido);
            this.Controls.Add(this.gbDescuentos);
            this.Controls.Add(this.gbDevengos);
            this.Controls.Add(this.btnCerrar);
            this.Controls.Add(this.gbPrincipal);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FormPrincipal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Calculadora de nómina";
            this.gbPrincipal.ResumeLayout(false);
            this.gbPrincipal.PerformLayout();
            this.flowLayoutPanel5.ResumeLayout(false);
            this.flowLayoutPanel5.PerformLayout();
            this.flowLayoutPanel4.ResumeLayout(false);
            this.flowLayoutPanel4.PerformLayout();
            this.flowLayoutPanel3.ResumeLayout(false);
            this.flowLayoutPanel3.PerformLayout();
            this.flowLayoutPanel2.ResumeLayout(false);
            this.flowLayoutPanel2.PerformLayout();
            this.gbLiquido.ResumeLayout(false);
            this.gbLiquido.PerformLayout();
            this.gbDescuentos.ResumeLayout(false);
            this.gbDescuentos.PerformLayout();
            this.gbDevengos.ResumeLayout(false);
            this.gbDevengos.PerformLayout();
            this.pnlBotonesNomina.ResumeLayout(false);
            this.ResumeLayout(false);

        }


        private System.Windows.Forms.Button btnGeneraNom;
        private System.Windows.Forms.GroupBox gbPrincipal;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tbFechLiquidacion;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private System.Windows.Forms.Label lblCategoria;
        private System.Windows.Forms.TextBox tbCategoria;
        private System.Windows.Forms.Label lblNHijos;
        private System.Windows.Forms.TextBox tbNHijos;
        private System.Windows.Forms.Label lblNTrienios;
        private System.Windows.Forms.TextBox tbNTrienios;
        private System.Windows.Forms.Label lblNHorasExtras;
        private System.Windows.Forms.TextBox tbNHorasExtras;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.Label lblNombre;
        private System.Windows.Forms.TextBox tbNombre;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.Label lblNif;
        private System.Windows.Forms.TextBox tbNIF;
        private System.Windows.Forms.GroupBox gbLiquido;
        private System.Windows.Forms.TextBox tbLiquido;
        private System.Windows.Forms.Label lblLiquido;
        private System.Windows.Forms.GroupBox gbDescuentos;
        private System.Windows.Forms.Label lblCSegSocial;
        private System.Windows.Forms.Label lblTDescuentos;
        private System.Windows.Forms.Label lblCSegDesemp;
        private System.Windows.Forms.Label lblIRPF;
        private System.Windows.Forms.TextBox tbCSegSoc;
        private System.Windows.Forms.TextBox tbCSegDesem;
        private System.Windows.Forms.TextBox tbIRPF;
        private System.Windows.Forms.TextBox tbTotalDescuento;
        private System.Windows.Forms.GroupBox gbDevengos;
        private System.Windows.Forms.Label lblSalarioBase;
        private System.Windows.Forms.Label lblAntiguedad;
        private System.Windows.Forms.Label lblPagaExtra;
        private System.Windows.Forms.Label lblHExtras;
        private System.Windows.Forms.TextBox tbSalarioBase;
        private System.Windows.Forms.TextBox tbAntiguedad;
        private System.Windows.Forms.TextBox tbHorasExtras;
        private System.Windows.Forms.TextBox tbTotalDevengo;
        private System.Windows.Forms.TextBox tbPagaExtra;
        private System.Windows.Forms.Label lblTDevengos;
        private System.Windows.Forms.Button btnCerrar;
        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.Button btnModificar;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.Panel pnlBotonesNomina;
    }
}

