﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Practica6
{
    public partial class FormNomina : Form
    {
        private FormPrincipal _formPrincipal;

        public FormNomina(FormPrincipal formPrincipal)
        {
            _formPrincipal = formPrincipal;
            InitializeComponent();            
        }

        internal void UpdateLabel(string nomOutput)
        {
            this.lblNomina.Text = nomOutput;
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            this.Close();

            _formPrincipal.ActivaBtnGeneraNom();
        }
    }
}
