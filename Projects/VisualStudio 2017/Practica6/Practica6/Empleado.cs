﻿/*
* PRÁCTICA.............: Práctica 2.
* NOMBRE y APELLIDOS...: Juan de Dios Delgado Bermúdez
* CURSO y GRUPO........: 2o Desarrollo de Interfaces
* TÍTULO de la PRÁCTICA: Definición de Clases. Uso de Métodos.
* FECHA de ENTREGA.....: 2 de Noviembre de 2017
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica6
{
    class Empleado
    {
        #region Variables

        string _strNif;
        string _strNombre;
        int _intCategoria;
        int _intNumHijos;
        int _intNumTrienios;

        #endregion Variables

        #region Propiedades

        public string Nif
        {
            get => _strNif;
            set => _strNif = value;
        }

        public string Nombre
        {
            get => _strNombre;
            set => _strNombre = value;
        }

        public int Categoria
        {
            get => _intCategoria; 
            set => _intCategoria = value;
        }        

        public int NumHijos
        {
            get => _intNumHijos; 
            set => _intNumHijos = value;
        }

        public int NumTrienios
        {
            get => _intNumTrienios; 
            set => _intNumTrienios = value; 
        }

        #endregion Propiedades
    }
}
