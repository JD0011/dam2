﻿/*
* PRÁCTICA.............: Práctica 2.
* NOMBRE y APELLIDOS...: Juan de Dios Delgado Bermúdez
* CURSO y GRUPO........: 2o Desarrollo de Interfaces
* TÍTULO de la PRÁCTICA: Definición de Clases. Uso de Métodos.
* FECHA de ENTREGA.....: 2 de Noviembre de 2017
*/

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica6
{
    class Nomina
    {
        #region Variables

        Empleado _empEmpleadoNomina;
        DateTime _dtFechNomina;
        int _intNumHorasExtras;

        #endregion Variables

        #region Propiedades

        public DateTime FechNomina
        {
            get => _dtFechNomina;
            set => _dtFechNomina = value;
        }
        public int NumHorasExtras
        {
            get => _intNumHorasExtras;
            set => _intNumHorasExtras = value;
        }

        #endregion Propiedades

        #region Constructor
        
        public Nomina(Empleado empObjetivo)
        {
            _empEmpleadoNomina = empObjetivo;
        }

        #endregion Constructor

        #region Métodos

        public double BaseCotizacion() => DevengosPagaExtra() + DevengosPagaExtra() / 6;

        public double CotizacionSegDes() => DevengosPagaExtra() * 1.97 / 100;

        public double CotizacionSegSoc() => BaseCotizacion() * 4.51 / 100;

        public double DevengosPagaExtra() => SalarioBase() + ImporteAntiguedad();

        public double ImporteAntiguedad() => _empEmpleadoNomina.NumTrienios * SalarioBase() * 4 / 100;

        public double ImporteHorasExtras() => _intNumHorasExtras * SalarioBase() * 1 / 100;

        public double LiquidoPercibir() => TotalDevengado() - TotalDescuentos();

        public int PorcentajeIRPF()
        {
            int intPorcentaje = 0;

            switch (_empEmpleadoNomina.Categoria)
            {
                case 1: intPorcentaje = 18 - _empEmpleadoNomina.NumHijos; break;
                case 2: intPorcentaje = 15 - _empEmpleadoNomina.NumHijos; break;
                case 3: intPorcentaje = 12 - _empEmpleadoNomina.NumHijos; break;
            }

            return intPorcentaje;
        }

        public double RetencionIRPF() => TotalDevengado() * PorcentajeIRPF() / 100;

        public double SalarioBase()
        {
            double aux = 0;

            switch (_empEmpleadoNomina.Categoria)
            {
                case 1: aux = 2500; break;
                case 2: aux = 2000; break;
                case 3: aux = 1500; break;
            }

            return aux;
        }

        public double TotalDescuentos() => CotizacionSegSoc() + CotizacionSegDes() + RetencionIRPF();

        public double TotalDevengado()
        {
            double dbDevengado;

            if (_dtFechNomina.Month == 6 || _dtFechNomina.Month == 12)
            {
                dbDevengado = SalarioBase() + ImporteAntiguedad() + ImporteHorasExtras() + DevengosPagaExtra();
            }
            else
            {
                dbDevengado = SalarioBase() + ImporteAntiguedad() + ImporteHorasExtras();
            }
            
            return dbDevengado;
        }

        public StringBuilder GeneraNominaSalarial()
        {
            StringBuilder builderNomina;

            builderNomina = new StringBuilder();

            builderNomina.AppendLine("LIQUIDACIÓN DE HABERES AL " + _dtFechNomina.ToString("dd/MM/yyyy") + "\n");
            builderNomina.AppendLine();

            //CABECERA
            builderNomina.AppendLine("Nombre........:"+ _empEmpleadoNomina.Nombre);
            builderNomina.AppendLine("NIF...........:" + _empEmpleadoNomina.Nif);
            builderNomina.AppendLine("Categoría.....:" + _empEmpleadoNomina.Categoria);
            builderNomina.AppendLine("Nº de trienios:" + _empEmpleadoNomina.NumTrienios);
            builderNomina.AppendLine("Nº de Hijos...:" + _empEmpleadoNomina.NumHijos);
            builderNomina.AppendLine();

            //DEVENGOS Y DESCUENTOS
            builderNomina.AppendLine("DEVENGOS \t\t\t\tDESCUENTOS");
            builderNomina.AppendLine("-------- \t\t\t\t----------");

            builderNomina.AppendLine(
                "Salario base    \t"+ SalarioBase().ToString("#.##") + 
                "   \tCotización Seg. Soc. \t"+CotizacionSegSoc().ToString("#.##"));

            builderNomina.AppendLine(
                "Antiguedad      \t"+ ImporteAntiguedad().ToString("#.##") + 
                "   \tCotización Seg. Des. \t"+CotizacionSegDes().ToString("#.##"));

            builderNomina.AppendLine(
                "Importe Hor.Ext.\t" + ImporteHorasExtras().ToString("#.##") + 
                "  \tRetención IRPF       \t"+ RetencionIRPF().ToString("#.##"));

            builderNomina.AppendLine(
                "Paga Extra      \t"+DevengosPagaExtra().ToString("#.##"));
            builderNomina.AppendLine();

            //TOTALES
            builderNomina.AppendLine(
                "Total Devengos \t\t" + TotalDevengado().ToString("#.##") + 
                " \tTotal Descuentos \t" + TotalDescuentos().ToString("#.##"));

            //A PERCIBIR
            builderNomina.AppendLine("-------------------------------");
            builderNomina.AppendLine("LÍQUIDO A PERCIBIR "+ LiquidoPercibir().ToString("#.##"));
            builderNomina.AppendLine("*******************************");

            return builderNomina;
        }

        #endregion Métodos
    }
}

