﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Practica6
{
    class Validator
    {

        public bool ValidateWithRegex(string regex, string text)
        {
            return new Regex(regex).IsMatch(text);
        }

        public bool ValidateNumero(double min, double max, double number)
        {
            return number >= min && number <= max;
        }
    }
}
