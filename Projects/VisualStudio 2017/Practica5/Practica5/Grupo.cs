﻿/*
* PRÁCTICA.............: Práctica 5.
* NOMBRE Y APELLIDOS...: Juan de Dios Delgado Bermúdez
* CURSO Y GRUPO........: 2º Desarrollo de Interfaces
* TÍTULO DE LA PRÁCTICA: Estructuras de Datos Internas y Manejo de Ficheros.
* FECHA DE ENTREGA.....: 12 de Diciembre de 2017
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica5
{
    public class Grupo : IComparable<Grupo>
    {
        #region Variables de clase

        string _strNombre;
        string[] _rngCodAsignaturas;
        List<Alumno> _listAlumnos;

        #endregion Variables de clase

        #region Propiedades

        public string Nombre { get => _strNombre; }
        public int NumAsignaturas { get => _rngCodAsignaturas.Length; }
        public string[] CodAsignatura { get => _rngCodAsignaturas; }
        public int NumAlumnos { get => _listAlumnos != null ? _listAlumnos.Count : 0; }
        public List<Alumno> Alumnos { get => _listAlumnos; }
        public int UltimaMatricula { get => _listAlumnos.AsReadOnly().OrderByDescending(x =>x.Matricula).First().Matricula; }

        #endregion Propiedades

        #region Constructor

        public Grupo(string strNombre, string[] rngCodAsignaturas)
        {
            _strNombre = strNombre;
            _rngCodAsignaturas = rngCodAsignaturas;

            _listAlumnos = new List<Alumno>();
        }

        

        #endregion Constructor

        #region Métodos

        public void AddAlumno(Alumno a)
        {
            _listAlumnos.Add(a);
        }

        public Alumno BuscaAlumno(int intMatricula)
        {
            return _listAlumnos.Find(a => a.Matricula == intMatricula);
        }

        public bool BorraAlumno(int intMatricula)
        {
            return _listAlumnos.Remove(_listAlumnos.Find(a => a.Matricula == intMatricula));
        }

        public void ListaAlumnos()
        {
            Utils.PrintColorMessage("\nLISTA DE ALUMNOS\n",ConsoleColor.Yellow);
            foreach(Alumno a in _listAlumnos)
            {
                Console.WriteLine("Matrícula: " + a.Matricula + "\tNombre: "+a.Nombre);
            }
        }

        public double MediaGrupo(int intCodAsignatura)
        {
            double dblTotalNota;

            dblTotalNota = 0;
            foreach (Alumno a in _listAlumnos)
            {
                dblTotalNota+=a.Notas[intCodAsignatura];
            }

            return dblTotalNota / Convert.ToDouble(_listAlumnos.Count);
        }

        public void ActaGrupo()
        {
            if (_listAlumnos.Count == 0)
            {
                Utils.PrintColorMessage("\nNo hay alumnos matriculados todavía", ConsoleColor.Red);
            }
            else
            {
                bool cambiaColor;

                //Header
                Utils.PrintColorMessage("\n\nACTA DE GRUPO " + _strNombre+"\n", ConsoleColor.Yellow);
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.Write(
                        "{0,-12}{1,-20}",
                        "MATRÍCULA",
                        "NOMBRE"
                        );

                foreach(string cod in _rngCodAsignaturas)
                {
                    Console.Write("\t"+cod);
                }
                
                Console.WriteLine("\tMEDIA\tNº SUS");
                Console.WriteLine();

                //Contenido
                _listAlumnos.Sort();
                cambiaColor = true;
                foreach (Alumno a in _listAlumnos)
                {
                    if (cambiaColor)
                    {
                        Console.BackgroundColor = ConsoleColor.DarkCyan;
                    }
                    else
                    {
                        Console.BackgroundColor = ConsoleColor.DarkBlue;
                    }
                    cambiaColor = !cambiaColor;

                    Console.Write(
                        String.Format(
                            "{0,-12}{1,-20}",
                            a.Matricula,
                            a.Nombre
                            )
                        );

                    foreach(int nota in a.Notas)
                    {
                        Console.Write("\t"+nota);
                    }

                    Console.WriteLine("\t" + String.Format("{0:0.00}",a.NotaMedia()) + "\t" +a.Suspensos());
                }

                Console.WriteLine();

                Console.ResetColor();

                //Footer
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.Write(
                        String.Format(
                            "{0,35}",
                            "MEDIA GRUPO"
                            )
                        );
                
                for(int i = 0; i < _rngCodAsignaturas.Length; i++)
                {
                    if (MediaGrupo(i) < 5)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                    }
                    else
                    {
                        Console.ForegroundColor = ConsoleColor.DarkGreen;
                    }
                    Console.Write("\t"+String.Format("{0:0.00}", MediaGrupo(i)));
                    Console.ResetColor();
                }
                Console.WriteLine();

                Console.WriteLine();

                int intSuspensos;
                Console.WriteLine("Nº de alumnos con ...");
                for (int i = 0; i < 3; i++)
                {
                    intSuspensos = 0;
                    
                    foreach (Alumno a in _listAlumnos)
                    {
                        if (a.Suspensos() == i)
                        {
                            intSuspensos++;
                        }
                    }

                    Console.WriteLine("\t"+i + " suspensos: " + intSuspensos);
                }

                intSuspensos = 0;
                foreach (Alumno a in _listAlumnos)
                {
                    if (a.Suspensos() >= 3)
                    {
                        intSuspensos++;
                    }
                }

                Console.WriteLine("\t3 o más suspensos: " + intSuspensos);
            }
            

        }

        public int CompareTo(Grupo other)
        {
            int intValue;
            if (Char.ToUpper(_strNombre[0]) < Char.ToUpper(other.Nombre[0]))
            {
                intValue = 1;
            }
            else if (other.Nombre == null)
            {
                intValue = 0;
            }
            else
            {
                intValue = -1;
            }

            return intValue;
        }

        #endregion Métodos
    }
}
