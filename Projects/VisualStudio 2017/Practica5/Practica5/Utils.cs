﻿/*
* PRÁCTICA.............: Práctica 5.
* NOMBRE Y APELLIDOS...: Juan de Dios Delgado Bermúdez
* CURSO Y GRUPO........: 2º Desarrollo de Interfaces
* TÍTULO DE LA PRÁCTICA: Estructuras de Datos Internas y Manejo de Ficheros.
* FECHA DE ENTREGA.....: 12 de Diciembre de 2017
*/

using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Timers;
using System.Xml;
using System.Xml.Serialization;

namespace Practica5
{
    static class Utils
    {
        #region Variables de clase

        static Timer _tmrDisparador = new Timer();
        static string _strHeaderMessage; //Para usarlo en el evento BorraMensajeHeader

        #endregion Variables de clase

        #region Métodos
        
        #region Introducción de datos

        public static string LeerCadena(string strRegex, string strErrorMessage)
        {
            Point ptPosicionCursor;
            string strInput;
            bool exitCondition;

            strInput = "";
            exitCondition = false;
            ptPosicionCursor = new Point(Console.CursorLeft, Console.CursorTop);
            while (!exitCondition)
            {
                Console.ForegroundColor = ConsoleColor.Cyan;
                strInput = Console.ReadLine();
                Console.ResetColor();

                if(!(new Regex(strRegex).IsMatch(strInput.Trim())))
                {
                    PrintHeaderMessage(strErrorMessage);
                    LimpiarEspacio(ptPosicionCursor, strInput.Length);
                }
                else
                {
                    exitCondition = true;
                }
            }

            return strInput.Trim();
        }

        public static int LeerEntero(int min, int max)
        {
            Point ptPosicionCursor;
            string strAux;
            int intRespuesta;
            bool exitCondition;
            bool printError;

            intRespuesta = 0;
            ptPosicionCursor = new Point(Console.CursorLeft, Console.CursorTop);
            exitCondition = false;
            while (!exitCondition)
            {
                Console.ForegroundColor = ConsoleColor.Cyan;
                strAux = Console.ReadLine();
                Console.ResetColor();

                printError = false;
                try
                {
                    intRespuesta = int.Parse(strAux);
                }
                catch (FormatException)
                {
                    printError = true;
                }
                catch (OverflowException)
                {
                    printError = true;
                }

                if (intRespuesta < min || intRespuesta > max)
                {
                    printError = true;
                }

                if (printError)
                {
                    PrintHeaderMessage("Debe introducir un número entero entre " + min + " y " + max);
                    LimpiarEspacio(ptPosicionCursor, strAux.Length);
                }
                else
                {
                    exitCondition = true;
                }
                
            }

            return intRespuesta;
        }

        #endregion Introducción de datos

        #region Notificación de errores

        public static void PrintHeaderMessage(string strMessage)
        {
            Point ptPosicionCursor;

            _strHeaderMessage = strMessage;

            ptPosicionCursor = new Point(Console.CursorLeft, Console.CursorTop);
            Console.SetCursorPosition(0, 0);

            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write("\a"+ strMessage);
            Console.SetCursorPosition(ptPosicionCursor.X, ptPosicionCursor.Y);
            Console.ResetColor();
            ActivaDisparador();

            Console.SetCursorPosition(ptPosicionCursor.X, ptPosicionCursor.Y);
        }

        //Borra mensaje sin bloqueo. Asegura ejecución evento cada 2 segundos desde el último error
        private static void ActivaDisparador()
        {
            if (_tmrDisparador.Enabled == false)
            {
                _tmrDisparador.Elapsed += BorraMensajeHeader;

                _tmrDisparador.AutoReset = false; //1 vez
                _tmrDisparador.Interval = 2500;
                _tmrDisparador.Enabled = true;
            }
            else
            {
                _tmrDisparador.Stop();
                _tmrDisparador.Start();
            }
            
        }
        private static void BorraMensajeHeader(object source, System.Timers.ElapsedEventArgs e)
        {
            Point ptPosicionCursor;

            ptPosicionCursor = new Point(Console.CursorLeft, Console.CursorTop);

            Console.SetCursorPosition(0, 0);
            Console.Write(new string(' ', _strHeaderMessage.Length));
            Console.SetCursorPosition(ptPosicionCursor.X, ptPosicionCursor.Y);
        }

        public static void LimpiarEspacio(Point ptPosCursorInicial, int intEspacios)
        {
            Console.SetCursorPosition(ptPosCursorInicial.X, ptPosCursorInicial.Y);
            Console.Write(new string(' ', intEspacios));
            Console.SetCursorPosition(ptPosCursorInicial.X, ptPosCursorInicial.Y);
        }

        #endregion Notificación de errores

        #region Carga de datos

        public static void GuardaGrupos(List<Grupo> grupos)
        {
            XmlWriter writer;

            XmlDocument xmlDoc = new XmlDocument();

            XmlNode root = xmlDoc.CreateElement("grupos");

            XmlNode grupo, asignaturas, alumnos;

            //Grupos
            grupos.Sort();
            foreach (Grupo g in grupos)
            {
                grupo = xmlDoc.CreateElement("grupo");

                XmlAttribute nombreGrupo = xmlDoc.CreateAttribute("nombre");
                nombreGrupo.InnerText = g.Nombre;

                grupo.Attributes.Append(nombreGrupo);

                //Asignaturas
                asignaturas = xmlDoc.CreateElement("asignaturas");
                XmlNode cod;
                foreach (string s in g.CodAsignatura)
                {
                    cod = xmlDoc.CreateElement("cod");
                    cod.InnerText = s;

                    asignaturas.AppendChild(cod);
                }
                grupo.AppendChild(asignaturas);

                //Alumnos
                alumnos = xmlDoc.CreateElement("alumnos");

                g.Alumnos.Sort();

                XmlNode alumno, notas, matricula;
                XmlAttribute nombreAlumno;
                foreach (Alumno a in g.Alumnos)
                {
                    alumno = xmlDoc.CreateElement("alumno");

                    nombreAlumno = xmlDoc.CreateAttribute("nombre");
                    nombreAlumno.InnerText = a.Nombre;

                    matricula = xmlDoc.CreateElement("matricula");
                    matricula.InnerText = a.Matricula.ToString();

                    alumno.AppendChild(matricula);
                    alumno.Attributes.Append(nombreAlumno);

                    notas = xmlDoc.CreateElement("notas");
                    XmlNode notaAsignatura;
                    foreach (int nota in a.Notas)
                    {
                        notaAsignatura = xmlDoc.CreateElement("nota");
                        notaAsignatura.InnerText = nota.ToString();

                        notas.AppendChild(notaAsignatura);
                    }

                    alumno.AppendChild(notas);
                    alumnos.AppendChild(alumno);
                }

                grupo.AppendChild(alumnos);
                root.AppendChild(grupo);
            }

            xmlDoc.AppendChild(root);

            writer = XmlWriter.Create("./grupos.xml");

            xmlDoc.Save(writer);
            writer.Close();
        }

        public static List<Grupo> CargaGrupos()
        {
            List<Grupo> listGrupos;
            Grupo grpCurrent;
            List<string> codAsignaturas;
            XmlDocument xmlDoc = new XmlDocument();

            xmlDoc.Load("./grupos.xml");

            XmlNode nlGrupos = xmlDoc.SelectSingleNode("/grupos");

            listGrupos = new List<Grupo>();
            //Lee grupos
            XmlNode asignaturasNode, alumnosNode;
            foreach (XmlNode grupo in nlGrupos)
            {
                //Asignaturas
                asignaturasNode = grupo.SelectSingleNode("./asignaturas");                
                codAsignaturas = new List<string>();
                foreach (XmlNode cod in asignaturasNode.SelectNodes("./cod"))
                {
                    codAsignaturas.Add(cod.InnerText);
                }
                grpCurrent = new Grupo(grupo.SelectSingleNode("./@nombre").InnerText, codAsignaturas.ToArray());

                //Alumnos
                alumnosNode = grupo.SelectSingleNode("./alumnos");
                foreach(XmlNode alumno in alumnosNode)
                {
                    XmlNode notasNode = alumno.SelectSingleNode("./notas");
                    List<int> notas = new List<int>();
                    foreach(XmlNode notaNode in notasNode)
                    {
                        notas.Add(Int32.Parse(notaNode.InnerText));
                    }
                    grpCurrent.AddAlumno(
                        new Alumno(
                            alumno.SelectSingleNode("./@nombre").InnerText, 
                            grpCurrent, 
                            notas.ToArray(),
                            Int32.Parse(alumno.SelectSingleNode("./matricula").InnerText)));
                }

                listGrupos.Add(grpCurrent);
            }

            return listGrupos;
        }

        #endregion Carga de datos

        #region Otros

        public static void PrintColorMessage(string message, ConsoleColor cColor)
        {
            Console.ForegroundColor = cColor;
            Console.WriteLine(message);
            Console.ResetColor();
        }

        public static void PressEnter()
        {
            ConsoleKey cKey;
            Console.WriteLine("\nPulse enter para continuar...");

            do
            {
                cKey = Console.ReadKey().Key;
            } while (cKey != ConsoleKey.Enter);

            Console.Clear();
        }

        public static bool GrupoExistente(string strNombreGrupo, List<Grupo> grupos)
        {
            bool existe;

            existe = false;
            foreach (Grupo g in grupos)
            {
                if (g.Nombre.ToLower() == strNombreGrupo.ToLower())
                {
                    existe = true;
                    break;
                }
            }

            return existe;
        }

        public static bool AlumnoExistente(string strNombreAlumno, List<Alumno> alumnos)
        {
            bool existe;

            existe = false;
            foreach (Alumno a in alumnos)
            {
                if (a.Nombre.ToLower() == strNombreAlumno.ToLower())
                {
                    existe = true;
                    break;
                }
            }

            return existe;
        }

        #endregion Otros

        #endregion Métodos
    }
}
