﻿/*
* PRÁCTICA.............: Práctica 5.
* NOMBRE Y APELLIDOS...: Juan de Dios Delgado Bermúdez
* CURSO Y GRUPO........: 2º Desarrollo de Interfaces
* TÍTULO DE LA PRÁCTICA: Estructuras de Datos Internas y Manejo de Ficheros.
* FECHA DE ENTREGA.....: 12 de Diciembre de 2017
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Xml;

namespace Practica5
{
    class Aplicacion
    {
        #region Variables de clase

        static bool hayCambios;
        static List<Grupo> _listGrupos;

        #endregion Variables de clase

        #region Métodos

        public static int Menu()
        {
            int intRespuesta;

            Utils.PrintColorMessage("\nGESTOR DE GRUPOS\n", ConsoleColor.Yellow);
            Console.WriteLine(
                "1. Crear grupo\n" +
                "2. Lista de grupos ({0})\n" +
                "3. Listado por nombre\n" +
                "4. Eliminar grupos por nombre\n" +
                "5. Guardar ({1})\n" +
                "6. Salir",
                _listGrupos != null ? _listGrupos.Count : 0,
                hayCambios ? "Cambios por guardar" : "No hay cambios"
                );

            intRespuesta = Utils.LeerEntero(1, 6);
            Console.Clear();

            return intRespuesta;
        }
        //Añadir menu que gestione a los grupos
        public static void MenuGrupo(Grupo g)
        {
            int intRespuesta;
            bool exitCondition;

            exitCondition = false;
            do
            {
                Utils.PrintColorMessage("\nGRUPO - " + g.Nombre.ToUpper() + "\n", ConsoleColor.Yellow);

                Console.WriteLine(
                    "1. Añadir un alumno al grupo\n" +
                    "2. Borrar un alumno\n" +
                    "3. Consultar los datos de un alumno\n" +
                    "4. Acta del grupo\n" +
                    "5. Eliminar este grupo\n" +
                    "6. Salir"
                    );

                intRespuesta = Utils.LeerEntero(1, 6);
                Console.Clear();

                switch (intRespuesta)
                {
                    //Añadir alumno
                    case 1:
                        Alumno almnAux;

                        almnAux = creaAlumno(g);
                        if (Confirmar())
                        {
                            g.AddAlumno(almnAux);
                            Utils.PrintColorMessage("\nAlumno " + almnAux.Nombre + " añadido correctamente al grupo " + g.Nombre, ConsoleColor.Green);
                            Utils.PressEnter();

                            hayCambios = true;
                        }
                        break;

                    //Borrar alumno
                    case 2:
                        bool repeatCase;
                        int intMatricula;
                        string strNombre;

                        repeatCase = true;
                        do
                        {
                            if (g.NumAlumnos == 0)
                            {
                                Utils.PrintColorMessage("\nNo hay alumnos registrados todavia", ConsoleColor.Red);
                                Utils.PressEnter();

                                repeatCase = false;
                            }
                            else
                            {
                                g.ListaAlumnos();
                                Console.WriteLine();

                                Utils.PrintColorMessage("Introduce la matrícula del alumno a eliminar (0 para cancelar) -> ", ConsoleColor.Yellow);
                                intMatricula = Utils.LeerEntero(0, g.UltimaMatricula);

                                if (intMatricula == 0)
                                {
                                    repeatCase = false;
                                }
                                else
                                {
                                    if (g.BuscaAlumno(intMatricula) == null)
                                    {
                                        Console.Clear();

                                        Utils.PrintColorMessage("\nLa matrícula no corresponde a ningún alumno", ConsoleColor.Red);
                                        Utils.PressEnter();

                                    }
                                    else
                                    {
                                        Console.Clear();
                                        Utils.PrintColorMessage("\nALUMNO A ELIMINAR\n", ConsoleColor.Yellow);
                                        g.BuscaAlumno(intMatricula).ImprimeAlumnado();

                                        if (Confirmar())
                                        {
                                            strNombre = g.BuscaAlumno(intMatricula).Nombre;

                                            g.BorraAlumno(intMatricula);
                                            Utils.PrintColorMessage("\nAlumno " + strNombre + " borrado satisfactoriamente del grupo " + g.Nombre, ConsoleColor.Green);
                                            Utils.PressEnter();

                                            hayCambios = true;
                                        }
                                    }

                                    Console.Clear();
                                }
                            }
                        } while (repeatCase);
                        break;

                    //Consultar alumno
                    case 3:
                        if (g.NumAlumnos == 0)
                        {
                            Utils.PrintColorMessage("\nNo hay alumnos registrados todavia", ConsoleColor.Red);
                            Utils.PressEnter();
                        }
                        else
                        {
                            g.ListaAlumnos();
                            Console.WriteLine();

                            Utils.PrintColorMessage("Introduce la matrícula del alumno a consultar (0 para cancelar) -> ", ConsoleColor.Yellow);
                            intMatricula = Utils.LeerEntero(0, g.UltimaMatricula);

                            if (intMatricula != 0)
                            {
                                if (g.BuscaAlumno(intMatricula) == null)
                                {
                                    Utils.PrintHeaderMessage("La matrícula no corresponde a ningún alumno");
                                }
                                else
                                {
                                    Console.Clear();
                                    Utils.PrintColorMessage("\nINFORMACIÓN DE ALUMNO\n", ConsoleColor.Yellow);
                                    g.BuscaAlumno(intMatricula).ImprimeAlumnado();
                                    Utils.PressEnter();
                                }
                            }
                        }
                        break;

                    //Acta del grupo
                    case 4:
                        g.ActaGrupo();
                        Utils.PressEnter();
                        break;

                    //Eliminar este grupo
                    case 5:
                       

                        Utils.PrintColorMessage("\nEstá a punto de eliminar el grupo "+g.Nombre + "\n", ConsoleColor.Yellow);
                        if (Confirmar())
                        {
                            _listGrupos.Remove(g);
                        }
                        
                        hayCambios = true;

                        exitCondition = true;
                        break;

                    //Salir
                    case 6:
                        exitCondition = true;
                        break;
                }

                Console.Clear();

            } while (!exitCondition);
        }

        public static Grupo creaGrupo(List<Grupo> grupos)
        {
            bool exitCondition;
            string strNombre;
            int intAsignaturas;
            List<string> rngCodAsignaturas;
            Point ptCursorPosition;

            Utils.PrintColorMessage("\nIntroduce la siguiente información:\n", ConsoleColor.Yellow);

            Console.Write("Nombre del grupo (Nombre-Curso(Opcional)Letra: ");
            ptCursorPosition = new Point(Console.CursorLeft, Console.CursorTop);
            exitCondition = false;

            //Evita duplicidad de nombre
            do
            {
                strNombre = Utils.LeerCadena("^[a-zA-ZñÑáéíóúÁÉÍÓÚ]{1,15}-[0-9]{1}[a-zA-Z]{0,1}$", "Formato incorrecto. Ejemplo: COMERCIO-1A").ToUpper();
                if(Utils.GrupoExistente(strNombre, grupos))
                {
                    Utils.PrintHeaderMessage("El grupo "+strNombre+" ya ha sido registrado");
                    Utils.LimpiarEspacio(ptCursorPosition, strNombre.Length);
                }
                else
                {
                    exitCondition = true;
                }

            } while (!exitCondition);
           
            Console.Write("Nº de asignaturas (de 4 a 7): ");
            intAsignaturas = Utils.LeerEntero(4,7);
            
            Console.WriteLine("Introduce el código de las asignaturas (3 caracteres):");

            //Asegura introducción de valores no repetidos
            rngCodAsignaturas = new List<string>();
            for (int i = 0; i < intAsignaturas; i++)
            {
                Console.Write("\tAsignatura "+(i+1)+": ");

                ptCursorPosition = new Point(Console.CursorLeft, Console.CursorTop);
                do
                {
                    rngCodAsignaturas.Add(Utils.LeerCadena("^[a-zA-ZñÑáéíóúÁÉÍÓÚ]{3}$", "Formato requerido: XXX").ToUpper());

                    exitCondition = true;
                    if (i != 0)
                    {
                        //Comprobación con respecto a valores anteriores
                        for (int j = i-1; j >= 0; j--)
                        {
                            if (rngCodAsignaturas[i].Equals(rngCodAsignaturas[j]))
                            {
                                Utils.PrintHeaderMessage("La asignatura " + rngCodAsignaturas[i] + " ya ha sido registrada");
                                Utils.LimpiarEspacio(ptCursorPosition,rngCodAsignaturas[i].Length);

                                rngCodAsignaturas.RemoveAt(i);
                                exitCondition = false;
                                break;
                            }

                        }
                    }

                } while (!exitCondition);
            }

            rngCodAsignaturas.Sort();
            return new Grupo(strNombre, rngCodAsignaturas.ToArray());
        }

        public static Alumno creaAlumno(Grupo g)
        {
            Point ptCursorPosition;
            bool exitCondition;
            string strNombre;
            int[] rngNotas;

            Utils.PrintColorMessage("\nIntroduce la siguiente información:\n", ConsoleColor.Yellow);

            Console.Write("Nombre (20 caracteres máx.): ");
            ptCursorPosition = new Point(Console.CursorLeft, Console.CursorTop);
            exitCondition = false;
            do
            {
                strNombre = Utils.LeerCadena(
                "^[a-zA-ZñÑáéíóúÁÉÍÓÚ ]{2,20}$",
                "Debe introducir un nombre y apellido (longitud: de 2 a 20 caracteres)");
                if (Utils.AlumnoExistente(strNombre, g.Alumnos))
                {
                    Utils.PrintHeaderMessage("El alumno " + strNombre + " ya ha sido registrado");
                    Utils.LimpiarEspacio(ptCursorPosition, strNombre.Length);
                }
                else
                {
                    exitCondition = true;
                }

            } while (!exitCondition) ;
            
            Console.WriteLine("Introduce las notas correspondientes: ");

            rngNotas = new int[g.NumAsignaturas];
            for (int i = 0; i<g.NumAsignaturas; i++)
            {
                Console.Write("\t("+(i+1)+"/"+g.NumAsignaturas+") "+g.CodAsignatura[i]+":  ");
                rngNotas[i] = Utils.LeerEntero(0,10);
            }

            return new Alumno(strNombre, g, rngNotas);
        }

        public static int SeleccionaGrupo(List<Grupo> grupos)
        {
            int intRespuesta;

            Utils.PrintColorMessage("\nLISTA DE GRUPOS\n", ConsoleColor.Yellow);

            intRespuesta = 0;
            if (grupos.Count == 0) {
                Utils.PrintColorMessage("No hay grupos a listar", ConsoleColor.Red);
                Utils.PressEnter();
            }
            else
            {
                bool cambiaColor;
                Utils.PrintColorMessage("Seleccione un grupo mediante el número que le precede\n", ConsoleColor.Yellow);

                //HEADER
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("{0,-5}{1,-20}{2,-10}{3}","Nº","NOMBRE","ALUMNOS","ASIGNATURAS");
                Console.WriteLine();

                //REGISTROS
                int i = 0;
                _listGrupos.Sort();
                cambiaColor = true;
                foreach (Grupo g in grupos)
                {
                    if (cambiaColor)
                    {
                        Console.BackgroundColor = ConsoleColor.DarkCyan;
                    }
                    else
                    {
                        Console.BackgroundColor = ConsoleColor.DarkBlue;
                    }
                    cambiaColor = !cambiaColor;
                    Console.Write("{0,-5}{1,-23}{2,-9}", ++i, g.Nombre, g.NumAlumnos);

                    foreach(string cod in g.CodAsignatura)
                    {
                        Console.Write("\t"+cod);
                    }
                    Console.WriteLine();
                }
                Console.ResetColor();

                Console.Write("\nSelección (0 para cancelar)-> ");
                intRespuesta = Utils.LeerEntero(0, grupos.Count);
            }

            Console.Clear();

            return intRespuesta;
        }

        public static bool Confirmar()
        {
            Console.WriteLine("\n¿Está todo correcto?[Pulse S/N]");
            string strRespuesta;

            do
            {
                strRespuesta = Console.ReadKey(true).KeyChar.ToString();
                strRespuesta = strRespuesta.ToUpper();

            } while (!(strRespuesta.Equals("S") || strRespuesta.Equals("N")));

            return strRespuesta.Equals("S");
        }

        public static void ActivaCopiaSeguridad(List<Grupo> grupos, int intSegundos)
        {
            Timer tmrDisparador;

            tmrDisparador = new Timer();
            tmrDisparador.Interval = intSegundos*1000; // seg a ms
            tmrDisparador.AutoReset = true;
            
            tmrDisparador.Elapsed += EventoGuardar;

            tmrDisparador.Start();
        }

        public static void EventoGuardar(object source, System.Timers.ElapsedEventArgs e)
        {
            _listGrupos.Sort();

            //Guardar si hay cambios
            if (hayCambios)
            {
                Utils.GuardaGrupos(_listGrupos);
                hayCambios = false;
            }
            
        }

        #endregion Métodos

        #region Main

        static void Main(string[] args)
        {
            int intIDGrupo;
            bool exitCondition;
            
            if (File.Exists("./grupos.xml")){
                try
                {
                    _listGrupos = Utils.CargaGrupos();
                }
                catch (XmlException) //XML mal formado
                {
                    File.Delete("./grupos.xml");
                    _listGrupos = new List<Grupo>();
                }
            }
            else
            {
                _listGrupos = new List<Grupo>();
            }

            ActivaCopiaSeguridad(_listGrupos, 180); //cada 3 minutos (180 segs)

            exitCondition = false;
            while (!exitCondition)
            {
                switch (Menu())
                {
                    //Crear grupo
                    case 1:
                        Grupo aux;
                        aux = creaGrupo(_listGrupos);

                        if (Confirmar())
                        {
                            _listGrupos.Add(aux);
                            Utils.PrintColorMessage("\nGrupo " + aux.Nombre + " creado correctamente", ConsoleColor.Green);
                            Utils.PressEnter();

                            hayCambios = true;
                        }
                        break;

                    //Lista grupos
                    case 2:

                        intIDGrupo = SeleccionaGrupo(_listGrupos);

                        if (intIDGrupo != 0)
                        {
                            if (_listGrupos.Count != 0)
                            {
                                MenuGrupo(_listGrupos[intIDGrupo - 1]); // Número 1 = Posición 0                    
                            }
                        }
                        break;

                    //Búsqueda por nombre
                    case 3:
                        
                        if (_listGrupos.Count == 0)
                        {
                            Utils.PrintColorMessage("\nPara realizar una búsqueda filtrada, debe existir un grupo como mínimo", ConsoleColor.Red);
                            Utils.PressEnter();
                        }
                        else
                        {
                            string strNombreGrupo;
                            List<Grupo> listResultados;
                            Console.Write("\nIntroduce parte del nombre del grupo buscado (0 para cancelar)-> ");
                            strNombreGrupo = Utils.LeerCadena("^[a-zA-ZñÑáéíóúÁÉÍÓÚ0-9-]{1,15}$", "La longitud debe ser de 1 a 15 caracteres").ToUpper();

                            if (!strNombreGrupo.Equals("0"))
                            {
                                listResultados = _listGrupos.FindAll(g => g.Nombre.Contains(strNombreGrupo));
                                intIDGrupo = SeleccionaGrupo(listResultados);

                                if (intIDGrupo != 0)
                                {
                                    MenuGrupo(_listGrupos[_listGrupos.IndexOf(listResultados.ElementAt(intIDGrupo - 1))]); // Número 1 = Posición 0
                                }
                            }
                        }
                        break;

                    //Eliminar grupo
                    case 4:

                        if (_listGrupos.Count == 0)
                        {
                            Utils.PrintColorMessage("\nNo hay ningún grupos registrados", ConsoleColor.Red);
                            Utils.PressEnter();
                        }
                        else
                        {
                            string strNombreGrupo;
                            List<Grupo> listResultados;
                            Console.Write("\nIntroduce parte del nombre del grupo/s a eliminar (0 para cancelar)-> ");
                            strNombreGrupo = Utils.LeerCadena("^[a-zA-ZñÑáéíóúÁÉÍÓÚ0-9-]{1,15}$", "La longitud debe ser de 1 a 15 caracteres").ToUpper();

                            if (!strNombreGrupo.Equals("0"))
                            {
                                listResultados = _listGrupos.FindAll(g => g.Nombre.Contains(strNombreGrupo));

                                if (listResultados.Count == 0)
                                {
                                    Utils.PrintColorMessage("\nNo existen coincidencias", ConsoleColor.Red);
                                    Utils.PressEnter();
                                }
                                else
                                {
                                    Console.WriteLine();
                                    Utils.PrintColorMessage("GRUPO/S A ELIMINAR\n", ConsoleColor.Yellow);

                                    foreach (Grupo g in listResultados)
                                    {
                                        Console.WriteLine(g.Nombre);
                                    }

                                    if (Confirmar())
                                    {
                                        Console.Clear();

                                        foreach (Grupo g in listResultados)
                                        {
                                            _listGrupos.Remove(g);
                                        }

                                        hayCambios = true;
                                        
                                        if (listResultados.Count == 1)
                                        {
                                            Utils.PrintColorMessage("\n1 grupo ha sido eliminado satisfactoriamente", ConsoleColor.Green);
                                        }
                                        else
                                        {
                                            Utils.PrintColorMessage("\n"+listResultados.Count + " grupos han sido eliminados satisfactoriamente", ConsoleColor.Green);
                                        }

                                        Utils.PressEnter();
                                    }
                                }
                            }
                        }
                        break;

                    //Guardar
                    case 5:
                        Utils.GuardaGrupos(_listGrupos);
                        hayCambios = false;
                        break;
                    
                    //Salir
                    case 6:
                        exitCondition = true;
                        break;
                }

                Console.Clear();
            }
        }
    #endregion Main
    }
}
