﻿/*
* PRÁCTICA.............: Práctica 5.
* NOMBRE Y APELLIDOS...: Juan de Dios Delgado Bermúdez
* CURSO Y GRUPO........: 2º Desarrollo de Interfaces
* TÍTULO DE LA PRÁCTICA: Estructuras de Datos Internas y Manejo de Ficheros.
* FECHA DE ENTREGA.....: 12 de Diciembre de 2017
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica5
{
    public class Alumno : IComparable<Alumno>
    {
        #region Variables de clase

        int _intMatricula;
        string _strNombre;
        int[] _rngNotas;
        Grupo _grpMiembro;

        static int _intContador;

        #endregion Variables de clase

        #region Propiedades

        public int Matricula { get => _intMatricula; }
        public string Nombre { get => _strNombre; }
        public int[] Notas { get => _rngNotas; }

        #endregion Propiedades

        #region Constructor

        public Alumno(string nombre,Grupo grpMiembro, int[] rngNotas)
        {
            _strNombre = nombre;
            _grpMiembro = grpMiembro;
            _intMatricula = ++_intContador;
            _rngNotas = rngNotas;
            
        }

        public Alumno(string nombre, Grupo grpMiembro, int[] rngNotas, int intMatricula)
        {
            _strNombre = nombre;
            _grpMiembro = grpMiembro;
            _intMatricula = intMatricula;
            _rngNotas = rngNotas;

            //Evita duplicar matrículas asignando la última disponible como punto de partida
            if (intMatricula > _intContador)
            {   
                _intContador = intMatricula;
            }
        }

        #endregion Constructor

        #region Métodos

        public void ImprimeAlumnado()
        {
            Console.WriteLine(
                "Matrícula:  {0}\n" +
                "Nombre:     {1}\n" +
                "Nota media: {2}\n" +
                "Suspensos:  {3}\n", 
                _intMatricula, 
                _strNombre,
                NotaMedia(),
                Suspensos()
                );

            Console.WriteLine("Notas (sobre 10) ...");
            for(int i = 0; i < _grpMiembro.NumAsignaturas; i++)
            {
                Console.Write("\t" + _grpMiembro.CodAsignatura[i] + ": ");

                if (_rngNotas[i] < 5)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.DarkGreen;
                }

                Console.WriteLine(_rngNotas[i]);
                Console.ResetColor();
            }
        }

        public double NotaMedia()
        {
            int dbTotal;

            dbTotal = 0;
            foreach(int nota in _rngNotas)
            {
                dbTotal += nota;
            }

            return dbTotal / Convert.ToDouble(_rngNotas.Length);
        }

        public int Suspensos()
        {
            int intSuspensos;

            intSuspensos = 0;
            foreach (int nota in _rngNotas)
            {
                if (nota < 5) { intSuspensos++; }
            }

            return intSuspensos;
        }

        public int CompareTo(Alumno other)
        {
            int intValue;
            if (Char.ToUpper(_strNombre[0]) < Char.ToUpper(other.Nombre[0]))
            {
                intValue = -1;
            }
            else if (other.Nombre ==null)
            {
                intValue = 0;
            }
            else
            {
                intValue = 1;
            }

            return intValue;
        }

        #endregion Métodos
    }
}