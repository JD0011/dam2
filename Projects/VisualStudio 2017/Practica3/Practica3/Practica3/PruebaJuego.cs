﻿/*
* PRÁCTICA.............: Práctica 3.
* NOMBRE y APELLIDOS...: Juan de Dios Delgado Bermúdez
* CURSO y GRUPO........: 2º Desarrollo de Interfaces
* TÍTULO de la PRÁCTICA: Sentencias de Control. Excepciones.
* FECHA de ENTREGA.....: 16 de Septiembre de 2017
*/

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica3
{
    class PruebaJuego
    {
        #region Métodos

        #region Readers

        public TimeSpan LeerTiempoLimite()
        {
            TimeSpan tsTiempoRespuesta;
            bool exitCondition;

            tsTiempoRespuesta = new TimeSpan();
            exitCondition = false;
            do
            {
                Console.Write("Tiempo máximo -> ");

                try
                {
                    tsTiempoRespuesta = TimeSpan.FromSeconds(Convert.ToInt32(Console.ReadLine()));
                }
                catch (FormatException)
                {
                    PrintMessage("Debe introducir un tipo de dato entero.", ConsoleColor.Red);
                    continue;
                }
                catch (OverflowException)
                {
                    PrintMessage("Debe introducir valor entre 3 y 10.", ConsoleColor.Red);
                    continue;
                }

                if (tsTiempoRespuesta.Seconds >= 3 && tsTiempoRespuesta.Seconds <= 10)
                {
                    exitCondition = true;
                }
                else
                {
                    PrintMessage("Debe introducir valor entre 3 y 10.", ConsoleColor.Red);
                }

            } while (!exitCondition);

            return tsTiempoRespuesta;
        }

        public int LeerNumeroPreguntas()
        {
            int intNumPreguntas;
            bool exitCondition;

            intNumPreguntas = 0;
            exitCondition = false;
            do
            {
                Console.Write("Número de preguntas -> ");

                try
                {
                    intNumPreguntas = Convert.ToInt32(Console.ReadLine());
                }
                catch (FormatException)
                {
                    PrintMessage("Debe introducir un tipo de dato entero.", ConsoleColor.Red);
                    continue;
                }
                catch (OverflowException)
                {
                    PrintMessage("Debe introducir un número del 1 al 10 (inclusive).", ConsoleColor.Red);
                    continue;
                }

                if (intNumPreguntas >= 1 && intNumPreguntas <= 10)
                {
                    exitCondition = true;
                }
                else
                {
                    PrintMessage("Debe introducir un número del 1 al 10 (inclusive).", ConsoleColor.Red);
                }

            } while (!exitCondition);

            return intNumPreguntas;
        }

        private String RecogerRespuesta(TimeSpan tsIntervalo)
        {
            bool exitCondition;
            ConsoleKeyInfo ckiPressed;
            string strInput;
            DateTime dtLimite;
            Point ptCursorPosition;

            strInput = "";
            dtLimite = DateTime.Now.Add(tsIntervalo);
            exitCondition = false;
            ptCursorPosition = new Point(Console.CursorLeft,Console.CursorTop);
            do
            {
                //Captura entrada
                if (Console.KeyAvailable)
                {
                    ckiPressed = Console.ReadKey();

                    if (ckiPressed.Key == ConsoleKey.Enter)
                    {
                        exitCondition = true;
                    }
                    else if (ckiPressed.Key == ConsoleKey.Backspace)
                    {
                        if (Console.CursorLeft>=ptCursorPosition.X)
                        {
                            //Implementación borrar
                            strInput = strInput.Substring(0, strInput.Length - 1);
                            Console.SetCursorPosition(ptCursorPosition.X, ptCursorPosition.Y);
                            Console.Write(new String(' ', Console.WindowWidth - ptCursorPosition.X));

                            Console.SetCursorPosition(ptCursorPosition.X, ptCursorPosition.Y);
                            Console.Write(strInput);
                        }
                        else
                        {
                            Console.SetCursorPosition(ptCursorPosition.X, ptCursorPosition.Y);
                        }
                    }
                    else
                    {
                        strInput += ckiPressed.KeyChar;
                    }
                }

                //... dentro del intervalo establecido
                if (DateTime.Now > dtLimite)
                {
                    strInput = null;
                    exitCondition = true;
                }

            } while (!exitCondition);

            Console.WriteLine();

            return strInput;
        }

        #endregion Readers     
        
        #region Printers

        public int MuestraMenu()
        {
            int intInput;
            bool exitCondition;

            intInput = 0;
            exitCondition = false;
            do
            {
                Console.WriteLine("Menú de Juego de multiplicar");

                Console.WriteLine(
                    "1. Establecer tiempo máximo para las respuestas\n" +
                    "2. Establecer el número de preguntas\n" +
                    "3. Jugar\n" +
                    "4. Salir");

                try
                {
                    intInput = Convert.ToInt32(Console.ReadLine());
                }
                catch (FormatException)
                {
                    PrintMessage("Debe introducir un tipo de dato numérico.", ConsoleColor.Red);
                    continue;
                }
                catch (OverflowException)
                {
                    PrintMessage("Debe introducir un número del 1 al 4.", ConsoleColor.Red);
                    continue;
                }

                if (intInput > 0 && intInput <= 4)
                {
                    exitCondition = true;
                }
                else
                {
                    PrintMessage("Debe introducir un número del 1 al 4.", ConsoleColor.Red);
                }

            } while (!exitCondition);

            return intInput;
        }

        public void PrintResultadoOperacion(string strMsg, string strResultado, ConsoleColor cColor)
        {
            Console.ForegroundColor = cColor;
            Console.Write("-> "+strMsg +" ");
            Console.ResetColor();
            Console.WriteLine(strResultado);
        }

        public void PrintResultadoFinal(JuegoMultiplicar juego, int intNumPreguntas)
        {
            if (juego.Aciertos * 10 / intNumPreguntas >= 5)
            {
                Console.ForegroundColor = ConsoleColor.Green;
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
            }

            Console.WriteLine(
                "Nota -> {0} sobre 10",
                Math.Round(juego.Aciertos * Convert.ToDouble(10) / intNumPreguntas,2)
                );
            Console.ResetColor();

            Console.WriteLine(
                "Aciertos -> {0,-4} Fallos -> {1}",
                juego.Aciertos,
                juego.Fallos
                );
        }

        public void PrintInfoJuego(int intNumPreguntas, int intNumPreguntaActual, TimeSpan tsTiempoRespuesta)
        {
            Console.BackgroundColor = ConsoleColor.Gray;
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine(
                "Nº de preguntas -> {0,-4} Pregunta actual -> {1,-4} Tiempo de respuesta -> {2} ",
                intNumPreguntas,
                intNumPreguntaActual,
                tsTiempoRespuesta.Seconds + " segs"
                );
            
            Console.ResetColor();
        }

        public static void PrintMessage(string error, ConsoleColor cColor)
        {
            Console.Clear();
            Console.ForegroundColor = cColor;
            Console.WriteLine(error);
            Console.ResetColor();
        }

        #endregion Printers

        public void Jugar(TimeSpan tsTiempoRespuesta, int intNumPreguntas) 
        {
            JuegoMultiplicar jmJuego;
            string strRespuesta;
            ConsoleKey ckeypresed;

            PrintInfoJuego(intNumPreguntas, 0, tsTiempoRespuesta);
            Console.WriteLine();
            Console.WriteLine("¡Pulse una tecla para comenzar!");
            Console.ReadKey();

            jmJuego = new JuegoMultiplicar();
            for (int i = 1; i <= intNumPreguntas; i++)
            {
                Console.Clear();

                PrintInfoJuego(intNumPreguntas, i, tsTiempoRespuesta);
                Console.WriteLine();

                //Operación
                jmJuego.GenerarOperandos();
                Console.Write("{0,-5}{1,-4}{2,-3}{3,-3}", jmJuego.OperandoA, "*", jmJuego.OperandoB, "=");

                //Tratamiento respuesta
                strRespuesta = RecogerRespuesta(tsTiempoRespuesta);
                
                if (jmJuego.esCorrecto(strRespuesta))
                {
                    PrintResultadoOperacion("¡Bien!","Has acertado", ConsoleColor.Green);
                }
                else
                {
                    if (strRespuesta == null)
                    {
                        PrintResultadoOperacion("Tiempo superado!","Resultado correcto: " +jmJuego.Resultado, ConsoleColor.Yellow);
                    }
                    else
                    {
                        PrintResultadoOperacion("¡Mal!", "Resultado correcto: " + jmJuego.Resultado, ConsoleColor.Red);
                    }
                }

                Console.WriteLine("-> Pulse Enter para continuar.");
                do
                {
                    ckeypresed = Console.ReadKey().Key;
                } while (ckeypresed != ConsoleKey.Enter);
                
            }

            Console.Clear();
            
            PrintInfoJuego(intNumPreguntas, intNumPreguntas, tsTiempoRespuesta);
            Console.WriteLine();
            PrintResultadoFinal(jmJuego, intNumPreguntas);
            Console.WriteLine();

            Console.WriteLine("Pulse Esc para salir...");
            do
            {
                ckeypresed = Console.ReadKey().Key;
            } while (ckeypresed != ConsoleKey.Escape);
           
        }

        #endregion Métodos

        static void Main(string[] args)
        {
            PruebaJuego pjEmeplo;
            TimeSpan tsInvervalo;
            int intNumPreguntas;
            bool exitCondition;
            
            pjEmeplo = new PruebaJuego();

            tsInvervalo = TimeSpan.Zero;
            intNumPreguntas = 0;
            exitCondition = false;
            do
            {
                switch (pjEmeplo.MuestraMenu())
                {
                    case 1:
                        tsInvervalo = pjEmeplo.LeerTiempoLimite();
                        break;
                    case 2:
                        intNumPreguntas = pjEmeplo.LeerNumeroPreguntas();
                        break;
                    case 3:

                        if (tsInvervalo == TimeSpan.Zero && intNumPreguntas == 0)
                        {
                            PruebaJuego.PrintMessage(
                                "¡Debe ejecutar las opciones 1 y 2 para poder jugar!",
                                ConsoleColor.Yellow);
                            continue;
                                
                        }
                        else if (tsInvervalo == TimeSpan.Zero)
                        {
                            PruebaJuego.PrintMessage(
                                "¡Debe establecer un tiempo de respuesta!(Opción 1)",
                                ConsoleColor.Yellow);
                            continue;
                        }
                        else if (intNumPreguntas == 0)
                        {
                            PruebaJuego.PrintMessage(
                                    "¡Debe establecer un Nº de preguntas!(Opción 2)",
                                    ConsoleColor.Yellow);
                            continue;
                        }
                        else
                        {
                            Console.Clear();
                            pjEmeplo.Jugar(tsInvervalo, intNumPreguntas);
                        }
                        break;

                    case 4:
                        exitCondition = true;
                        break;
                }

                Console.Clear();

            } while (!exitCondition);
        }
    }
}
