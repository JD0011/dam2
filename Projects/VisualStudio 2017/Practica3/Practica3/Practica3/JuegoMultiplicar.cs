﻿/*
* PRÁCTICA.............: Práctica 3.
* NOMBRE y APELLIDOS...: Juan de Dios Delgado Bermúdez
* CURSO y GRUPO........: 2º Desarrollo de Interfaces
* TÍTULO de la PRÁCTICA: Sentencias de Control. Excepciones.
* FECHA de ENTREGA.....: 16 de Septiembre de 2017
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica3
{
    class JuegoMultiplicar
    {

        #region Vars

        int _intOperandoA, _intOperandoB;
        int _intAciertos, _intFallos;

        #endregion vars

        #region Propiedades

        public int OperandoA { get => _intOperandoA; }
        public int OperandoB { get => _intOperandoB; }
        public int Resultado { get => _intOperandoA * _intOperandoB; }
        public int Aciertos { get => _intAciertos; }
        public int Fallos { get => _intFallos; }

        #endregion Propiedades

        #region Constructor

        public JuegoMultiplicar()
        {
            _intAciertos = 0;
            _intFallos = 0;
        }

        #endregion Constructor

        #region Métodos

        public bool esCorrecto(string strResultUser)
        {
            bool blCorrecto;
            int intResultUser;

            try
            {
                intResultUser = int.Parse(strResultUser);

                if (blCorrecto = intResultUser == _intOperandoA * _intOperandoB)
                {
                    _intAciertos++;
                }
                else
                {
                    _intFallos++;
                }
            }
            catch (Exception)
            {
                //Automáticamente false en caso de FormatException / OverflowException
                _intFallos++;
                blCorrecto = false;
            }

            return blCorrecto;
        }

        public void GenerarOperandos()
        {
            Random randomizer = new Random();
            
            _intOperandoA = randomizer.Next(1, 9);
            _intOperandoB = randomizer.Next(1, 9);
        }

        #endregion Métodos
    }
}
