﻿/*
* PRÁCTICA.............: Práctica 8
* NOMBRE Y APELLIDOS...: Juan de Dios Delgado Bermúdez
* CURSO Y GRUPO........: 2o Desarrollo de Interfaces
* TÍTULO DE LA PRÁCTICA: Generación de Componentes.
* FECHA DE ENTREGA.....: 10 de Enero de 2018
*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;

namespace Practica8
{
    public partial class Form : System.Windows.Forms.Form
    {
        private bool esModificacion;
        private Biblioteca blbGeneral;
        private Libro lbrMostrado;
        public Form()
        {
            InitializeComponent();
            LoadLibrary();
        }

        private void LoadLibrary()
        {
            //Si existe, carga biblioteca
            if (File.Exists("./biblioteca.xml"))
            {
                XmlSerializer ser = new XmlSerializer(typeof(Biblioteca));

                XmlReader reader = XmlReader.Create("./biblioteca.xml");
                blbGeneral = (Biblioteca)ser.Deserialize(reader);

                reader.Close();
            }
            else
            {
                blbGeneral = new Biblioteca();
            }
        }

        private void SaveLibrary()
        {
            if (File.Exists("./biblioteca.xml"))
            {
                File.Delete("./biblioteca.xml");
            }

            XmlSerializer ser = new XmlSerializer(blbGeneral.GetType());

            File.Delete("./biblioteca.xml");

            XmlWriter writer = XmlWriter.Create("./biblioteca.xml");

            ser.Serialize(writer, blbGeneral);

            writer.Close();

        }

        private void ShowLibro(Libro l)
        {
            lbrMostrado = l;

            tbNombre.Text = l.Nombre;
            tbPublicacion.Text = l.Año.ToString();
            tbAutor.Text = l.Autor;
            tbCategoria.Text = l.Categoria;
            tbCantidad.Text = l.Cantidad.ToString();
        }
        
        private void ClearFieldsOfAsistent()
        {
            lmayusNombre.ClearText();
            fttextPublicacion.ClearText();
            lmcpAutor.ClearText();
            snumCantidad.ClearText();
        }

        private void ClearFieldsOfInfoTab()
        {
            tbNombre.Text = "";
            tbPublicacion.Text = "";
            tbAutor.Text = "";
            tbCantidad.Text = "";
            tbCategoria.Text = "";
        }

        private void EnableEdition()
        {
            tbNombre.ReadOnly = false;
            tbPublicacion.ReadOnly = false;
            tbAutor.ReadOnly = false;
            tbCategoria.ReadOnly = false;
            tbCantidad.ReadOnly = false;
        }
        private void DisableEdition()
        {
            tbNombre.ReadOnly = true;
            tbPublicacion.ReadOnly = true;
            tbAutor.ReadOnly = true;
            tbCategoria.ReadOnly = true;
            tbCantidad.ReadOnly = true;
        }

        private void RefreshCBOXLibros()
        {
            cboxLibros.DataSource = null;
            cboxLibros.Refresh();

            LoadLibrary();

            cboxLibros.DataSource = blbGeneral._listLibros;
            cboxLibros.DisplayMember = "Nombre";
            cboxLibros.ValueMember = "Nombre";
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            //Si todo ok, esconde campos de libro y graba
            bool todoOK = 
                lmayusNombre.InputOK && 
                fttextPublicacion.InputOK && 
                lmcpAutor.InputOK && 
                snumCantidad.InputOK && 
                cboxCategoria.SelectedItem != null;

            if (todoOK)
            {
                //Actualiza biblioteca
                Libro lbrNuevo = new Libro(lmayusNombre.Text, lmcpAutor.Text, cboxCategoria.Text, fttextPublicacion.Text, Int32.Parse(snumCantidad.Text));

                if (esModificacion)
                {
                    if (!lbrNuevo.Equals(lbrMostrado))
                    {
                        blbGeneral._listLibros.Remove(lbrMostrado);
                        blbGeneral._listLibros.Add(lbrNuevo);
                        SaveLibrary();

                        MessageBox.Show("El libro se ha actualizado correctamente.", "Todo correcto", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        MessageBox.Show("El libro no se ha actualizado, ya que no hay cambios", "No hay cambios", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                else
                {
                    blbGeneral._listLibros.Add(lbrNuevo);
                    SaveLibrary();

                    MessageBox.Show("Libro guardado correctamente", "Libro guardado", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

                RefreshCBOXLibros();

                ClearFieldsOfAsistent();               
            }
        }

       

        private void btnEliminaLibro_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show(
                                        "¿Está seguro de que desea eliminar el libro mostrado?", 
                                        "Verificación", 
                                        MessageBoxButtons.YesNo, 
                                        MessageBoxIcon.Warning);

            if (result == DialogResult.Yes)
            {
                ClearFieldsOfInfoTab();
                blbGeneral._listLibros.Remove(lbrMostrado);

                //Actualiza fichero xml
                SaveLibrary();
                RefreshCBOXLibros();

                MessageBox.Show(
                    "Libro eliminado correctamente",
                    "Correcto", 
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
            }
        }

        private void btnModLibro_Click(object sender, EventArgs e)
        {
            esModificacion = true;

            ClearFieldsOfInfoTab();
            tcContent.SelectedTab = tabCreaLibros;

            btnGuardar.Enabled = true;

            EnableEdition();
            lmayusNombre.Text = lbrMostrado.Nombre;
            lmayusNombre.InputOK = true;
            lmcpAutor.Text = lbrMostrado.Autor;
            lmcpAutor.InputOK = true;
            fttextPublicacion.Text = lbrMostrado.Año;
            fttextPublicacion.InputOK = true;
            cboxCategoria.SelectedItem = lbrMostrado;
            snumCantidad.Text = lbrMostrado.Cantidad.ToString();
            snumCantidad.InputOK = true;

        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            ClearFieldsOfAsistent();
        }

        

        private void FieldsAreOK(object sender, EventArgs e)
        {
            bool todoOK =
                lmayusNombre.InputOK &&
                fttextPublicacion.InputOK &&
                lmcpAutor.InputOK &&
                snumCantidad.InputOK &&
                cboxCategoria.SelectedItem != null;

            if (todoOK)
            {
                btnGuardar.Enabled = true;
            }
        }

        private void cboxLibros_Click(object sender, EventArgs e)
        {
            RefreshCBOXLibros();        
        }

        private void cboxLibros_SelectedIndexChanged(object sender, EventArgs e)
        {
            bool exists = false;
            foreach (Libro l in blbGeneral._listLibros)
            {
                if (l.Nombre.Equals(cboxLibros.SelectedValue.ToString()))
                {
                    ShowLibro(l);
                    exists = true;
                }
            }

            if (exists)
            {
                gbInfoLibro.Visible = true;
            }
            else
            {
                gbInfoLibro.Visible = false;
            }

            DisableEdition();
        }
    }
}
