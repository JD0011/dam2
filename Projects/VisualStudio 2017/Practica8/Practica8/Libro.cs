﻿/*
* PRÁCTICA.............: Práctica 8
* NOMBRE Y APELLIDOS...: Juan de Dios Delgado Bermúdez
* CURSO Y GRUPO........: 2o Desarrollo de Interfaces
* TÍTULO DE LA PRÁCTICA: Generación de Componentes.
* FECHA DE ENTREGA.....: 10 de Enero de 2018
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica8
{
    public class Libro
    {
        private string _strNombre;
        private string _strAutor;
        private string _strCategoria;
        private string _strPublicacion;
        private int _intCantidad;

        public string Nombre { get => _strNombre; set => _strNombre = value; }
        public string Autor { get => _strAutor; set => _strAutor = value; }
        public string Categoria { get => _strCategoria; set => _strCategoria = value; }
        public string Año { get => _strPublicacion; set => _strPublicacion = value; }
        public int Cantidad { get => _intCantidad; set => _intCantidad = value; }

        public Libro()
        {

        }
        public Libro(string strNombre, string strAutor, string strCategoria, string strPublicacion, int intCantidad)
        {
            _strNombre = strNombre;
            _strAutor = strAutor;
            _strCategoria = strCategoria;
            _strPublicacion = strPublicacion;
            _intCantidad = intCantidad;
        }
    }
}
