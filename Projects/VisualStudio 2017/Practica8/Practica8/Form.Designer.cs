﻿namespace Practica8
{
    partial class Form
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tcContent = new System.Windows.Forms.TabControl();
            this.tabCreaLibros = new System.Windows.Forms.TabPage();
            this.lalblInfoExtra = new System.Windows.Forms.Label();
            this.llblInfo = new System.Windows.Forms.Label();
            this.btnLimpiar = new System.Windows.Forms.Button();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.gbLibro = new System.Windows.Forms.GroupBox();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.lblAño = new System.Windows.Forms.Label();
            this.fpCategoria = new System.Windows.Forms.FlowLayoutPanel();
            this.lblCategoria = new System.Windows.Forms.Label();
            this.cboxCategoria = new System.Windows.Forms.ComboBox();
            this.fpCantidad = new System.Windows.Forms.FlowLayoutPanel();
            this.lblCantidad = new System.Windows.Forms.Label();
            this.fpNombre = new System.Windows.Forms.FlowLayoutPanel();
            this.lblNombre = new System.Windows.Forms.Label();
            this.fpAutor = new System.Windows.Forms.FlowLayoutPanel();
            this.lblAutor = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tabListaLibros = new System.Windows.Forms.TabPage();
            this.btnModLibro = new System.Windows.Forms.Button();
            this.btnEliminaLibro = new System.Windows.Forms.Button();
            this.cboxLibros = new System.Windows.Forms.ComboBox();
            this.lblSeleccionaLibro = new System.Windows.Forms.Label();
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.bindingSource2 = new System.Windows.Forms.BindingSource(this.components);
            this.flowLayoutPanel10 = new System.Windows.Forms.FlowLayoutPanel();
            this.tbPublicacion = new System.Windows.Forms.TextBox();
            this.lblAñoLibro = new System.Windows.Forms.Label();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.flowLayoutPanel9 = new System.Windows.Forms.FlowLayoutPanel();
            this.tbAutor = new System.Windows.Forms.TextBox();
            this.lblAutorLibro = new System.Windows.Forms.Label();
            this.flowLayoutPanel8 = new System.Windows.Forms.FlowLayoutPanel();
            this.tbNombre = new System.Windows.Forms.TextBox();
            this.lblNombreLibro = new System.Windows.Forms.Label();
            this.flowLayoutPanel7 = new System.Windows.Forms.FlowLayoutPanel();
            this.tbCantidad = new System.Windows.Forms.TextBox();
            this.lblCantLibro = new System.Windows.Forms.Label();
            this.flowLayoutPanel6 = new System.Windows.Forms.FlowLayoutPanel();
            this.tbCategoria = new System.Windows.Forms.TextBox();
            this.lblCatLibro = new System.Windows.Forms.Label();
            this.gbInfoLibro = new System.Windows.Forms.GroupBox();
            this.fttextPublicacion = new Practica8_Custom_controls_lib.FechaToTextBox();
            this.snumCantidad = new Practica8_Custom_controls_lib.SoloNumeros();
            this.lmayusNombre = new Practica8_Custom_controls_lib.LetrasMayus();
            this.lmcpAutor = new Practica8_Custom_controls_lib.LetrasMinusCapitalPrimera();
            this.tcContent.SuspendLayout();
            this.tabCreaLibros.SuspendLayout();
            this.gbLibro.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.fpCategoria.SuspendLayout();
            this.fpCantidad.SuspendLayout();
            this.fpNombre.SuspendLayout();
            this.fpAutor.SuspendLayout();
            this.tabListaLibros.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource2)).BeginInit();
            this.flowLayoutPanel10.SuspendLayout();
            this.flowLayoutPanel9.SuspendLayout();
            this.flowLayoutPanel8.SuspendLayout();
            this.flowLayoutPanel7.SuspendLayout();
            this.flowLayoutPanel6.SuspendLayout();
            this.gbInfoLibro.SuspendLayout();
            this.SuspendLayout();
            // 
            // tcContent
            // 
            this.tcContent.Controls.Add(this.tabCreaLibros);
            this.tcContent.Controls.Add(this.tabListaLibros);
            this.tcContent.Location = new System.Drawing.Point(12, 12);
            this.tcContent.Name = "tcContent";
            this.tcContent.SelectedIndex = 0;
            this.tcContent.Size = new System.Drawing.Size(464, 278);
            this.tcContent.TabIndex = 0;
            // 
            // tabCreaLibros
            // 
            this.tabCreaLibros.Controls.Add(this.lalblInfoExtra);
            this.tabCreaLibros.Controls.Add(this.llblInfo);
            this.tabCreaLibros.Controls.Add(this.btnLimpiar);
            this.tabCreaLibros.Controls.Add(this.btnGuardar);
            this.tabCreaLibros.Controls.Add(this.gbLibro);
            this.tabCreaLibros.Location = new System.Drawing.Point(4, 22);
            this.tabCreaLibros.Name = "tabCreaLibros";
            this.tabCreaLibros.Padding = new System.Windows.Forms.Padding(3);
            this.tabCreaLibros.Size = new System.Drawing.Size(456, 252);
            this.tabCreaLibros.TabIndex = 0;
            this.tabCreaLibros.Text = "Crear libro";
            this.tabCreaLibros.UseVisualStyleBackColor = true;
            // 
            // lalblInfoExtra
            // 
            this.lalblInfoExtra.AutoSize = true;
            this.lalblInfoExtra.Location = new System.Drawing.Point(6, 37);
            this.lalblInfoExtra.Name = "lalblInfoExtra";
            this.lalblInfoExtra.Size = new System.Drawing.Size(338, 13);
            this.lalblInfoExtra.TabIndex = 13;
            this.lalblInfoExtra.Text = "No olvide que debe introducir todos los datos requeridos para guardar.";
            // 
            // llblInfo
            // 
            this.llblInfo.AutoSize = true;
            this.llblInfo.Location = new System.Drawing.Point(6, 12);
            this.llblInfo.Name = "llblInfo";
            this.llblInfo.Size = new System.Drawing.Size(495, 13);
            this.llblInfo.TabIndex = 12;
            this.llblInfo.Text = "Con este asistente, podrá realizar la creación y/o edición de un libro y su almac" +
    "enamiento en el sistema.";
            // 
            // btnLimpiar
            // 
            this.btnLimpiar.Location = new System.Drawing.Point(317, 212);
            this.btnLimpiar.Name = "btnLimpiar";
            this.btnLimpiar.Size = new System.Drawing.Size(136, 31);
            this.btnLimpiar.TabIndex = 11;
            this.btnLimpiar.Text = "Limpiar";
            this.btnLimpiar.UseVisualStyleBackColor = true;
            this.btnLimpiar.Click += new System.EventHandler(this.btnLimpiar_Click);
            // 
            // btnGuardar
            // 
            this.btnGuardar.Enabled = false;
            this.btnGuardar.Location = new System.Drawing.Point(175, 212);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(136, 31);
            this.btnGuardar.TabIndex = 10;
            this.btnGuardar.Text = "Guardar";
            this.btnGuardar.UseVisualStyleBackColor = true;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // gbLibro
            // 
            this.gbLibro.Controls.Add(this.flowLayoutPanel1);
            this.gbLibro.Controls.Add(this.fpCategoria);
            this.gbLibro.Controls.Add(this.fpCantidad);
            this.gbLibro.Controls.Add(this.fpNombre);
            this.gbLibro.Controls.Add(this.fpAutor);
            this.gbLibro.Controls.Add(this.tableLayoutPanel1);
            this.gbLibro.Location = new System.Drawing.Point(6, 65);
            this.gbLibro.Name = "gbLibro";
            this.gbLibro.Size = new System.Drawing.Size(443, 141);
            this.gbLibro.TabIndex = 9;
            this.gbLibro.TabStop = false;
            this.gbLibro.Text = "Editor de libro";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoSize = true;
            this.flowLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.flowLayoutPanel1.Controls.Add(this.lblAño);
            this.flowLayoutPanel1.Controls.Add(this.fttextPublicacion);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(211, 19);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(183, 32);
            this.flowLayoutPanel1.TabIndex = 11;
            // 
            // lblAño
            // 
            this.lblAño.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblAño.AutoSize = true;
            this.lblAño.Location = new System.Drawing.Point(3, 9);
            this.lblAño.Name = "lblAño";
            this.lblAño.Size = new System.Drawing.Size(62, 13);
            this.lblAño.TabIndex = 0;
            this.lblAño.Text = "Publicación";
            // 
            // fpCategoria
            // 
            this.fpCategoria.AutoSize = true;
            this.fpCategoria.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.fpCategoria.Controls.Add(this.lblCategoria);
            this.fpCategoria.Controls.Add(this.cboxCategoria);
            this.fpCategoria.Location = new System.Drawing.Point(211, 57);
            this.fpCategoria.Name = "fpCategoria";
            this.fpCategoria.Size = new System.Drawing.Size(187, 27);
            this.fpCategoria.TabIndex = 4;
            // 
            // lblCategoria
            // 
            this.lblCategoria.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblCategoria.AutoSize = true;
            this.lblCategoria.Location = new System.Drawing.Point(3, 7);
            this.lblCategoria.Name = "lblCategoria";
            this.lblCategoria.Size = new System.Drawing.Size(54, 13);
            this.lblCategoria.TabIndex = 16;
            this.lblCategoria.Text = "Categoría";
            // 
            // cboxCategoria
            // 
            this.cboxCategoria.FormattingEnabled = true;
            this.cboxCategoria.Items.AddRange(new object[] {
            "Sci-fi",
            "Aventuras",
            "Prosa",
            "Terror",
            "Drama",
            "Comedia",
            "Tragicomedia",
            "Bélico",
            "Histórico",
            "Cuentos"});
            this.cboxCategoria.Location = new System.Drawing.Point(63, 3);
            this.cboxCategoria.Name = "cboxCategoria";
            this.cboxCategoria.Size = new System.Drawing.Size(121, 21);
            this.cboxCategoria.TabIndex = 4;
            this.cboxCategoria.SelectedIndexChanged += new System.EventHandler(this.FieldsAreOK);
            // 
            // fpCantidad
            // 
            this.fpCantidad.AutoSize = true;
            this.fpCantidad.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.fpCantidad.Controls.Add(this.lblCantidad);
            this.fpCantidad.Controls.Add(this.snumCantidad);
            this.fpCantidad.Location = new System.Drawing.Point(11, 95);
            this.fpCantidad.Name = "fpCantidad";
            this.fpCantidad.Size = new System.Drawing.Size(167, 32);
            this.fpCantidad.TabIndex = 5;
            // 
            // lblCantidad
            // 
            this.lblCantidad.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblCantidad.AutoSize = true;
            this.lblCantidad.Location = new System.Drawing.Point(3, 9);
            this.lblCantidad.Name = "lblCantidad";
            this.lblCantidad.Size = new System.Drawing.Size(49, 13);
            this.lblCantidad.TabIndex = 13;
            this.lblCantidad.Text = "Cantidad";
            // 
            // fpNombre
            // 
            this.fpNombre.AutoSize = true;
            this.fpNombre.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.fpNombre.Controls.Add(this.lblNombre);
            this.fpNombre.Controls.Add(this.lmayusNombre);
            this.fpNombre.Location = new System.Drawing.Point(16, 19);
            this.fpNombre.Name = "fpNombre";
            this.fpNombre.Size = new System.Drawing.Size(162, 32);
            this.fpNombre.TabIndex = 1;
            // 
            // lblNombre
            // 
            this.lblNombre.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblNombre.AutoSize = true;
            this.lblNombre.Location = new System.Drawing.Point(3, 9);
            this.lblNombre.Name = "lblNombre";
            this.lblNombre.Size = new System.Drawing.Size(44, 13);
            this.lblNombre.TabIndex = 4;
            this.lblNombre.Text = "Nombre";
            // 
            // fpAutor
            // 
            this.fpAutor.AutoSize = true;
            this.fpAutor.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.fpAutor.Controls.Add(this.lblAutor);
            this.fpAutor.Controls.Add(this.lmcpAutor);
            this.fpAutor.Location = new System.Drawing.Point(28, 57);
            this.fpAutor.Name = "fpAutor";
            this.fpAutor.Size = new System.Drawing.Size(150, 32);
            this.fpAutor.TabIndex = 3;
            // 
            // lblAutor
            // 
            this.lblAutor.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblAutor.AutoSize = true;
            this.lblAutor.Location = new System.Drawing.Point(3, 9);
            this.lblAutor.Name = "lblAutor";
            this.lblAutor.Size = new System.Drawing.Size(32, 13);
            this.lblAutor.TabIndex = 7;
            this.lblAutor.Text = "Autor";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.AutoSize = true;
            this.tableLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Location = new System.Drawing.Point(92, 198);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(0, 0);
            this.tableLayoutPanel1.TabIndex = 10;
            // 
            // tabListaLibros
            // 
            this.tabListaLibros.Controls.Add(this.btnModLibro);
            this.tabListaLibros.Controls.Add(this.btnEliminaLibro);
            this.tabListaLibros.Controls.Add(this.gbInfoLibro);
            this.tabListaLibros.Controls.Add(this.cboxLibros);
            this.tabListaLibros.Controls.Add(this.lblSeleccionaLibro);
            this.tabListaLibros.Location = new System.Drawing.Point(4, 22);
            this.tabListaLibros.Name = "tabListaLibros";
            this.tabListaLibros.Padding = new System.Windows.Forms.Padding(3);
            this.tabListaLibros.Size = new System.Drawing.Size(456, 252);
            this.tabListaLibros.TabIndex = 1;
            this.tabListaLibros.Text = "Mostrar libros";
            this.tabListaLibros.UseVisualStyleBackColor = true;
            // 
            // btnModLibro
            // 
            this.btnModLibro.Location = new System.Drawing.Point(314, 215);
            this.btnModLibro.Name = "btnModLibro";
            this.btnModLibro.Size = new System.Drawing.Size(136, 31);
            this.btnModLibro.TabIndex = 12;
            this.btnModLibro.Text = "Modificar libro";
            this.btnModLibro.UseVisualStyleBackColor = true;
            this.btnModLibro.Click += new System.EventHandler(this.btnModLibro_Click);
            // 
            // btnEliminaLibro
            // 
            this.btnEliminaLibro.Location = new System.Drawing.Point(172, 215);
            this.btnEliminaLibro.Name = "btnEliminaLibro";
            this.btnEliminaLibro.Size = new System.Drawing.Size(136, 31);
            this.btnEliminaLibro.TabIndex = 11;
            this.btnEliminaLibro.Text = "Eliminar libro";
            this.btnEliminaLibro.UseVisualStyleBackColor = true;
            this.btnEliminaLibro.Click += new System.EventHandler(this.btnEliminaLibro_Click);
            // 
            // cboxLibros
            // 
            this.cboxLibros.FormattingEnabled = true;
            this.cboxLibros.Location = new System.Drawing.Point(143, 12);
            this.cboxLibros.Name = "cboxLibros";
            this.cboxLibros.Size = new System.Drawing.Size(249, 21);
            this.cboxLibros.Sorted = true;
            this.cboxLibros.TabIndex = 1;
            this.cboxLibros.SelectionChangeCommitted += new System.EventHandler(this.cboxLibros_SelectedIndexChanged);
            this.cboxLibros.Click += new System.EventHandler(this.cboxLibros_Click);
            // 
            // lblSeleccionaLibro
            // 
            this.lblSeleccionaLibro.AutoSize = true;
            this.lblSeleccionaLibro.Location = new System.Drawing.Point(20, 12);
            this.lblSeleccionaLibro.Name = "lblSeleccionaLibro";
            this.lblSeleccionaLibro.Size = new System.Drawing.Size(97, 13);
            this.lblSeleccionaLibro.TabIndex = 0;
            this.lblSeleccionaLibro.Text = "Seleccione un libro";
            // 
            // flowLayoutPanel10
            // 
            this.flowLayoutPanel10.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flowLayoutPanel10.AutoSize = true;
            this.flowLayoutPanel10.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.flowLayoutPanel10.Controls.Add(this.lblAñoLibro);
            this.flowLayoutPanel10.Controls.Add(this.tbPublicacion);
            this.flowLayoutPanel10.Location = new System.Drawing.Point(34, 83);
            this.flowLayoutPanel10.Name = "flowLayoutPanel10";
            this.flowLayoutPanel10.Size = new System.Drawing.Size(138, 26);
            this.flowLayoutPanel10.TabIndex = 9;
            // 
            // tbPublicacion
            // 
            this.tbPublicacion.Location = new System.Drawing.Point(35, 3);
            this.tbPublicacion.Name = "tbPublicacion";
            this.tbPublicacion.Size = new System.Drawing.Size(100, 20);
            this.tbPublicacion.TabIndex = 20;
            // 
            // lblAñoLibro
            // 
            this.lblAñoLibro.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblAñoLibro.AutoSize = true;
            this.lblAñoLibro.Location = new System.Drawing.Point(3, 6);
            this.lblAñoLibro.Name = "lblAñoLibro";
            this.lblAñoLibro.Size = new System.Drawing.Size(26, 13);
            this.lblAñoLibro.TabIndex = 5;
            this.lblAñoLibro.Text = "Año";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.AutoSize = true;
            this.tableLayoutPanel2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Location = new System.Drawing.Point(92, 198);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(0, 0);
            this.tableLayoutPanel2.TabIndex = 10;
            // 
            // flowLayoutPanel9
            // 
            this.flowLayoutPanel9.AutoSize = true;
            this.flowLayoutPanel9.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.flowLayoutPanel9.Controls.Add(this.lblAutorLibro);
            this.flowLayoutPanel9.Controls.Add(this.tbAutor);
            this.flowLayoutPanel9.Location = new System.Drawing.Point(28, 51);
            this.flowLayoutPanel9.Name = "flowLayoutPanel9";
            this.flowLayoutPanel9.Size = new System.Drawing.Size(144, 26);
            this.flowLayoutPanel9.TabIndex = 11;
            // 
            // tbAutor
            // 
            this.tbAutor.Location = new System.Drawing.Point(41, 3);
            this.tbAutor.Name = "tbAutor";
            this.tbAutor.Size = new System.Drawing.Size(100, 20);
            this.tbAutor.TabIndex = 20;
            // 
            // lblAutorLibro
            // 
            this.lblAutorLibro.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblAutorLibro.AutoSize = true;
            this.lblAutorLibro.Location = new System.Drawing.Point(3, 6);
            this.lblAutorLibro.Name = "lblAutorLibro";
            this.lblAutorLibro.Size = new System.Drawing.Size(32, 13);
            this.lblAutorLibro.TabIndex = 7;
            this.lblAutorLibro.Text = "Autor";
            // 
            // flowLayoutPanel8
            // 
            this.flowLayoutPanel8.AutoSize = true;
            this.flowLayoutPanel8.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.flowLayoutPanel8.Controls.Add(this.lblNombreLibro);
            this.flowLayoutPanel8.Controls.Add(this.tbNombre);
            this.flowLayoutPanel8.Location = new System.Drawing.Point(16, 19);
            this.flowLayoutPanel8.Name = "flowLayoutPanel8";
            this.flowLayoutPanel8.Size = new System.Drawing.Size(156, 26);
            this.flowLayoutPanel8.TabIndex = 12;
            // 
            // tbNombre
            // 
            this.tbNombre.Location = new System.Drawing.Point(53, 3);
            this.tbNombre.Name = "tbNombre";
            this.tbNombre.Size = new System.Drawing.Size(100, 20);
            this.tbNombre.TabIndex = 19;
            // 
            // lblNombreLibro
            // 
            this.lblNombreLibro.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblNombreLibro.AutoSize = true;
            this.lblNombreLibro.Location = new System.Drawing.Point(3, 6);
            this.lblNombreLibro.Name = "lblNombreLibro";
            this.lblNombreLibro.Size = new System.Drawing.Size(44, 13);
            this.lblNombreLibro.TabIndex = 4;
            this.lblNombreLibro.Text = "Nombre";
            // 
            // flowLayoutPanel7
            // 
            this.flowLayoutPanel7.AutoSize = true;
            this.flowLayoutPanel7.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.flowLayoutPanel7.Controls.Add(this.lblCantLibro);
            this.flowLayoutPanel7.Controls.Add(this.tbCantidad);
            this.flowLayoutPanel7.Location = new System.Drawing.Point(226, 51);
            this.flowLayoutPanel7.Name = "flowLayoutPanel7";
            this.flowLayoutPanel7.Size = new System.Drawing.Size(161, 26);
            this.flowLayoutPanel7.TabIndex = 15;
            // 
            // tbCantidad
            // 
            this.tbCantidad.Location = new System.Drawing.Point(58, 3);
            this.tbCantidad.Name = "tbCantidad";
            this.tbCantidad.Size = new System.Drawing.Size(100, 20);
            this.tbCantidad.TabIndex = 20;
            // 
            // lblCantLibro
            // 
            this.lblCantLibro.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblCantLibro.AutoSize = true;
            this.lblCantLibro.Location = new System.Drawing.Point(3, 6);
            this.lblCantLibro.Name = "lblCantLibro";
            this.lblCantLibro.Size = new System.Drawing.Size(49, 13);
            this.lblCantLibro.TabIndex = 13;
            this.lblCantLibro.Text = "Cantidad";
            // 
            // flowLayoutPanel6
            // 
            this.flowLayoutPanel6.AutoSize = true;
            this.flowLayoutPanel6.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.flowLayoutPanel6.Controls.Add(this.lblCatLibro);
            this.flowLayoutPanel6.Controls.Add(this.tbCategoria);
            this.flowLayoutPanel6.Location = new System.Drawing.Point(220, 19);
            this.flowLayoutPanel6.Name = "flowLayoutPanel6";
            this.flowLayoutPanel6.Size = new System.Drawing.Size(166, 26);
            this.flowLayoutPanel6.TabIndex = 18;
            // 
            // tbCategoria
            // 
            this.tbCategoria.Location = new System.Drawing.Point(63, 3);
            this.tbCategoria.Name = "tbCategoria";
            this.tbCategoria.Size = new System.Drawing.Size(100, 20);
            this.tbCategoria.TabIndex = 20;
            // 
            // lblCatLibro
            // 
            this.lblCatLibro.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblCatLibro.AutoSize = true;
            this.lblCatLibro.Location = new System.Drawing.Point(3, 6);
            this.lblCatLibro.Name = "lblCatLibro";
            this.lblCatLibro.Size = new System.Drawing.Size(54, 13);
            this.lblCatLibro.TabIndex = 16;
            this.lblCatLibro.Text = "Categoría";
            // 
            // gbInfoLibro
            // 
            this.gbInfoLibro.Controls.Add(this.flowLayoutPanel6);
            this.gbInfoLibro.Controls.Add(this.flowLayoutPanel7);
            this.gbInfoLibro.Controls.Add(this.flowLayoutPanel8);
            this.gbInfoLibro.Controls.Add(this.flowLayoutPanel9);
            this.gbInfoLibro.Controls.Add(this.tableLayoutPanel2);
            this.gbInfoLibro.Controls.Add(this.flowLayoutPanel10);
            this.gbInfoLibro.Location = new System.Drawing.Point(6, 56);
            this.gbInfoLibro.Name = "gbInfoLibro";
            this.gbInfoLibro.Size = new System.Drawing.Size(443, 141);
            this.gbInfoLibro.TabIndex = 10;
            this.gbInfoLibro.TabStop = false;
            this.gbInfoLibro.Text = "Información de libro";
            // 
            // fttextPublicacion
            // 
            this.fttextPublicacion.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.fttextPublicacion.AutoSize = true;
            this.fttextPublicacion.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.fttextPublicacion.InputOK = false;
            this.fttextPublicacion.Location = new System.Drawing.Point(71, 3);
            this.fttextPublicacion.Name = "fttextPublicacion";
            this.fttextPublicacion.OnlyYearMonth = true;
            this.fttextPublicacion.Size = new System.Drawing.Size(109, 26);
            this.fttextPublicacion.TabIndex = 1;
            this.fttextPublicacion.Leave += new System.EventHandler(this.FieldsAreOK);
            // 
            // snumCantidad
            // 
            this.snumCantidad.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.snumCantidad.AutoSize = true;
            this.snumCantidad.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.snumCantidad.InputOK = false;
            this.snumCantidad.Location = new System.Drawing.Point(58, 3);
            this.snumCantidad.Name = "snumCantidad";
            this.snumCantidad.Size = new System.Drawing.Size(106, 26);
            this.snumCantidad.TabIndex = 5;
            this.snumCantidad.Leave += new System.EventHandler(this.FieldsAreOK);
            // 
            // lmayusNombre
            // 
            this.lmayusNombre.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lmayusNombre.AutoSize = true;
            this.lmayusNombre.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.lmayusNombre.InputOK = false;
            this.lmayusNombre.Location = new System.Drawing.Point(53, 3);
            this.lmayusNombre.Name = "lmayusNombre";
            this.lmayusNombre.Size = new System.Drawing.Size(106, 26);
            this.lmayusNombre.TabIndex = 1;
            this.lmayusNombre.Leave += new System.EventHandler(this.FieldsAreOK);
            // 
            // lmcpAutor
            // 
            this.lmcpAutor.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lmcpAutor.AutoSize = true;
            this.lmcpAutor.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.lmcpAutor.InputOK = false;
            this.lmcpAutor.Location = new System.Drawing.Point(41, 3);
            this.lmcpAutor.Name = "lmcpAutor";
            this.lmcpAutor.Size = new System.Drawing.Size(106, 26);
            this.lmcpAutor.TabIndex = 3;
            this.lmcpAutor.Leave += new System.EventHandler(this.FieldsAreOK);
            // 
            // Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(488, 313);
            this.Controls.Add(this.tcContent);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Form";
            this.Text = "Gestor usuarios";
            this.tcContent.ResumeLayout(false);
            this.tabCreaLibros.ResumeLayout(false);
            this.tabCreaLibros.PerformLayout();
            this.gbLibro.ResumeLayout(false);
            this.gbLibro.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.fpCategoria.ResumeLayout(false);
            this.fpCategoria.PerformLayout();
            this.fpCantidad.ResumeLayout(false);
            this.fpCantidad.PerformLayout();
            this.fpNombre.ResumeLayout(false);
            this.fpNombre.PerformLayout();
            this.fpAutor.ResumeLayout(false);
            this.fpAutor.PerformLayout();
            this.tabListaLibros.ResumeLayout(false);
            this.tabListaLibros.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource2)).EndInit();
            this.flowLayoutPanel10.ResumeLayout(false);
            this.flowLayoutPanel10.PerformLayout();
            this.flowLayoutPanel9.ResumeLayout(false);
            this.flowLayoutPanel9.PerformLayout();
            this.flowLayoutPanel8.ResumeLayout(false);
            this.flowLayoutPanel8.PerformLayout();
            this.flowLayoutPanel7.ResumeLayout(false);
            this.flowLayoutPanel7.PerformLayout();
            this.flowLayoutPanel6.ResumeLayout(false);
            this.flowLayoutPanel6.PerformLayout();
            this.gbInfoLibro.ResumeLayout(false);
            this.gbInfoLibro.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tcContent;
        private System.Windows.Forms.TabPage tabCreaLibros;
        private System.Windows.Forms.TabPage tabListaLibros;
        private System.Windows.Forms.BindingSource bindingSource1;
        private System.Windows.Forms.BindingSource bindingSource2;
        private System.Windows.Forms.GroupBox gbLibro;
        private Practica8_Custom_controls_lib.LetrasMayus lmayusNombre;
        private Practica8_Custom_controls_lib.LetrasMinusCapitalPrimera lmcpAutor;
        private System.Windows.Forms.Label lblNombre;
        private System.Windows.Forms.Label lblAutor;
        private Practica8_Custom_controls_lib.SoloNumeros snumCantidad;
        private System.Windows.Forms.Label lblCantidad;
        private System.Windows.Forms.FlowLayoutPanel fpNombre;
        private System.Windows.Forms.FlowLayoutPanel fpAutor;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.FlowLayoutPanel fpCategoria;
        private System.Windows.Forms.Label lblCategoria;
        private System.Windows.Forms.FlowLayoutPanel fpCantidad;
        private System.Windows.Forms.ComboBox cboxCategoria;
        private System.Windows.Forms.Button btnLimpiar;
        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.ComboBox cboxLibros;
        private System.Windows.Forms.Label lblSeleccionaLibro;
        private System.Windows.Forms.Button btnModLibro;
        private System.Windows.Forms.Button btnEliminaLibro;
        private System.Windows.Forms.Label lalblInfoExtra;
        private System.Windows.Forms.Label llblInfo;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Label lblAño;
        private Practica8_Custom_controls_lib.FechaToTextBox fttextPublicacion;
        private System.Windows.Forms.GroupBox gbInfoLibro;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel6;
        private System.Windows.Forms.Label lblCatLibro;
        private System.Windows.Forms.TextBox tbCategoria;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel7;
        private System.Windows.Forms.Label lblCantLibro;
        private System.Windows.Forms.TextBox tbCantidad;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel8;
        private System.Windows.Forms.Label lblNombreLibro;
        private System.Windows.Forms.TextBox tbNombre;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel9;
        private System.Windows.Forms.Label lblAutorLibro;
        private System.Windows.Forms.TextBox tbAutor;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel10;
        private System.Windows.Forms.Label lblAñoLibro;
        private System.Windows.Forms.TextBox tbPublicacion;
    }
}

