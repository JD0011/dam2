﻿/*
* PRÁCTICA.............: Práctica 1.
* NOMBRE y APELLIDOS...: Juan de Dios Delgado Bermúdez
* CURSO y GRUPO........: 2o Desarrollo de Interfaces
* TÍTULO de la PRÁCTICA: Diseño de clases. Herencia y polimorfismo.
* FECHA de ENTREGA.....: 23 de Octubre de 2017
*/

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica1
{
    class Tv
    {
        #region Vars

        string _strMarca;
        double _dbPulgadas;
        int _intConsumo;
        double _dbPrecio;
        bool _blOnOff;
        int _intCanal;
        int _intCanalAnterior;
        int _intVolumen;

        //Almacena canal desde el que se parte a CanalAnterior
        int _intAuxCanal;

        #endregion Vars

        #region Constructores
        
        public Tv(string strMarca, double dbPulgadas, double dbPrecio)
        {
            _strMarca = strMarca;
            _dbPulgadas = dbPulgadas;
            _dbPrecio = dbPrecio;

            //Valor por defecto
            _blOnOff = false;
        }

        public Tv(string strMarca, double dbPulgadas, int intConsumo, double dbPrecio)
        {
            _strMarca = strMarca;
            _dbPulgadas = dbPulgadas;
            _intConsumo = intConsumo;
            _dbPrecio = dbPrecio;

            //Valor por defecto
            _blOnOff = false;
        }

        #endregion Constructores

        #region Métodos

        public void pulsarOnOff()
        {
            if (!_blOnOff)
            {
                _blOnOff = true;

                _intCanal = 1;
                _intCanalAnterior = 1;
                _intVolumen = 25;
            }
            else
            {
                _blOnOff = false;
            }
        }

        public void SubirVolumen()
        {
            if (_blOnOff)
            {
                if (_intVolumen < 100)
                {
                    _intVolumen++;
                }
                else
                {
                    Console.Beep();
                }
            }
            else
            {
                Console.Beep();
                Console.Beep();
                Console.Beep();
            }
            
        }

        public void BajarVolumen()
        {
            if (_blOnOff)
            {
                if (_intVolumen > 0)
                {
                    _intVolumen--;
                }
                else
                {
                    Console.Beep();
                }
            }
            else
            {
                Console.Beep();
                Console.Beep();
                Console.Beep();
            }
            
        }

        public void PonerCanal(int intNuevoCanal)
        {
            if (_blOnOff)
            {
                if (intNuevoCanal > 0 && intNuevoCanal < 100)
                {
                    //Reasigna canal anterior
                    _intCanalAnterior = _intCanal;
                    _intCanal = intNuevoCanal;
                }
                else
                {
                    Console.Beep();
                }
            }
            else
            {
                Console.Beep();
                Console.Beep();
                Console.Beep();
            }
            
        }
        
        public void CambiarCanalAnterior()
        {
            if (_blOnOff)
            {
                //Guarda canal actual en auxiliar y ve al anterior
                _intAuxCanal = _intCanal;
                _intCanal = _intCanalAnterior;
                //Reasigna anterior al antiguo canal actual
                _intCanalAnterior = _intAuxCanal;
            }
            else
            {
                Console.Beep();
                Console.Beep();
                Console.Beep();
            }

        }

        public void EstadoActual()
        {
            
            if (_blOnOff)
            {
                Console.WriteLine(
                    "Encendida ->\n " +
                    "Canal {0}, Volumen: {1}", _intCanal, _intVolumen);
            }
            else
            {
                Console.WriteLine("La televisión está apagada");
            }
            
        }

        public void InformacionTecnica()
        {
            Console.WriteLine("Marca: {0}. Tamaño : {1}\". Precio:{2} ", _strMarca,_dbPulgadas,_dbPrecio);
        }

        #endregion Métodos
    }


}
