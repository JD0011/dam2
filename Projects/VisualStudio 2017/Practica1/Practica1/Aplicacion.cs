﻿/*
* PRÁCTICA.............: Práctica 1.
* NOMBRE y APELLIDOS...: Juan de Dios Delgado Bermúdez
* CURSO y GRUPO........: 2o Desarrollo de Interfaces
* TÍTULO de la PRÁCTICA: Diseño de clases. Herencia y polimorfismo.
* FECHA de ENTREGA.....: 23 de Octubre de 2017
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica1
{
    class Aplicacion
    {
        static void Main(string[] args)
        {
            ConsoleKey ckKeyPulsada;
            bool exitCondition;
            int errorCounter;
            Tv tvSanyo;

            tvSanyo = new Tv("Sanyo", 28, 449.95);

            errorCounter = 0;
            exitCondition = false;
            while (!exitCondition)
            {
                Console.WriteLine("(Pulse Escape para finalizar la aplicación)");

                MuestraInfoBotones();
                tvSanyo.EstadoActual();

                ckKeyPulsada = Console.ReadKey(true).Key;

                //Mientras haya un flujo de entrada disponible, deshecha las teclas entrantes. Evita delay.
                while (Console.KeyAvailable)
                {
                    Console.ReadKey(false);
                }

                switch (ckKeyPulsada)
                {
                    //Enter
                    case ConsoleKey.Enter:
                        errorCounter = 0;

                        tvSanyo.pulsarOnOff();
                        break;

                    //Espacio
                    case ConsoleKey.Spacebar:
                        errorCounter = 0;

                        string strCanal;
                        ConsoleKeyInfo ckNumber;

                        Console.Write("Canal... ");
                        strCanal = "";

                        //Recoge hasta 3 caracteres
                        do
                        {
                            ckNumber = Console.ReadKey();
                            strCanal += ckNumber.KeyChar;
                        } while (ckNumber.Key != ConsoleKey.Spacebar && strCanal.Length < 3);

                        try
                        {
                            tvSanyo.PonerCanal(int.Parse(strCanal));
                        }
                        catch (FormatException)
                        {
                            PrintError("Debe introducir valores numéricos para establecer el canal");
                            
                            strCanal = "";
                        }

                        break;

                    //(Retroceso)
                    case ConsoleKey.Backspace:
                        errorCounter = 0;

                        tvSanyo.CambiarCanalAnterior();
                        break;

                    //( + )
                    case ConsoleKey.Add:
                        errorCounter = 0;

                        tvSanyo.SubirVolumen();
                        break;

                    //( - )
                    case ConsoleKey.Subtract:
                        errorCounter = 0;

                        tvSanyo.BajarVolumen();
                        break;

                    // I
                    case ConsoleKey.I:
                        errorCounter = 0;

                        tvSanyo.InformacionTecnica();

                        Console.WriteLine("\nPulse cualquier tecla para continuar ...");
                        Console.ReadKey(false);
                        break;
                    
                    //Salir
                    case ConsoleKey.Escape:
                        errorCounter = 0;

                        exitCondition = true;
                        break;

                    //Cualquier otra tecla
                    default:
                        errorCounter++;

                        if (errorCounter == 10)
                        {
                            PrintError("Debe pulsar las teclas predefinidas.");

                            errorCounter = 0;
                            Console.Clear();
                        }

                        break;
                }
                Console.Clear();
            }
        }

        public static void PrintError(string strError)
        {
            ConsoleKey ckKeyPressed;
            do {
                Console.Clear();

                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(strError);
                Console.ResetColor();
                
                Console.WriteLine("\nPulse Enter para continuar ...");
                ckKeyPressed = Console.ReadKey(true).Key;

            } while(ckKeyPressed!=ConsoleKey.Enter);
        }

        public static void MuestraInfoBotones()
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("Guía de botones:");
            Console.ResetColor();

            Console.WriteLine(
                "Enter -> Encender/Apagar televisión\n" +
                "Barra espaciadora -> Activar y/o Finalizar inserción de canal\n" +
                "Retroceso -> Volver al canal anterior\n" +
                "( + ) -> Subir volmen\n"+
                "( - ) -> Restar volumen\n"+
                "I -> Información técnica de la televisión\n");            
        }
    }
}