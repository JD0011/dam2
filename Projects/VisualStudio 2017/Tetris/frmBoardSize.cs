﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Tetris
{
    public partial class frmBoardSize : Form
    {
        int _numFilas;
        int _numColumnas;
        bool hasChanged;
        public int NumColumnas { get => _numColumnas; }
        public int NumFilas { get => _numFilas; }
        public bool HasChanged { get => hasChanged; }
        public frmBoardSize(int numFilas, int numColumnas)
        {
            _numFilas = numFilas;
            _numColumnas = numColumnas;

            InitializeComponent();
        }

        private void frmBoardSize_Load(object sender, EventArgs e)
        {
            for (int i = Constantes.MIN_FILAS_PANTALLA; i <= Constantes.MAX_FILAS_PANTALLA; i++)
            {
                cbFilas.Items.Add(i);
            }

            cbFilas.Text = _numFilas.ToString();

            for (int i= Constantes.MIN_COLUMNAS_PANTALLA; i <= Constantes.MAX_COLUMNAS_PANTALLA; i++)
            {
                cbColumnas.Items.Add(i);
            }

            cbColumnas.Text = _numColumnas.ToString();

        }

        private void btnAplicar_Click(object sender, EventArgs e)
        {
            bool filasOkey, columnasOkey;
            _numFilas = 0;
            _numColumnas = 0;

            try
            {
                _numFilas = Int32.Parse(cbFilas.Text);
                filasOkey = _numFilas >= Constantes.MIN_FILAS_PANTALLA && _numFilas <= Constantes.MAX_FILAS_PANTALLA;
            }
            catch (Exception)
            {
                filasOkey = false;
            }

            try
            {
                _numColumnas = Int32.Parse(cbColumnas.Text);
                columnasOkey = _numColumnas >= Constantes.MIN_COLUMNAS_PANTALLA && _numColumnas <= Constantes.MAX_COLUMNAS_PANTALLA;
            }
            catch (Exception)
            {
                columnasOkey = false;
            }
            
            if (!filasOkey)
            {
                errorProvider.SetError(
                   cbFilas,
                   String.Format("El valor de las filas debe ser un entero entre {0} y {1}",
                   Constantes.MIN_FILAS_PANTALLA,
                   Constantes.MAX_FILAS_PANTALLA));
            }
            else if (!columnasOkey)
            {
                errorProvider.SetError(
                   cbFilas,
                   String.Format("El valor de las columnas debe ser un entero entre {0} y {1}",
                   Constantes.MIN_COLUMNAS_PANTALLA,
                   Constantes.MAX_COLUMNAS_PANTALLA));
            }
            else
            {
                hasChanged = true;
                this.Dispose();
            }
        }
    }
}
