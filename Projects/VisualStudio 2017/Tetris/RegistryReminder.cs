﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace Tetris
{
    //Guarda la configuración del puerto serie en el registro
    public class RegistryReminder
    {
        #region Variables
        private const string RootKey = "HKEY_CURRENT_USER\\Software\\ChatSP";
        private const string SubKey = "Configuration";
        private const string KeyName = RootKey + "\\" + SubKey;
        
        private const string FormPosition = "FormPosition";
        #endregion

        public void SaveFormPosition(frmGUI fGUI)
        {
            string strPointToString = fGUI.Location.X + "x" + fGUI.Location.Y;
            Registry.SetValue(KeyName, FormPosition, strPointToString);
        }

        public Point LoadFormPosition()
        {
            Point ptLocation = Point.Empty;

            var position = Registry.GetValue(KeyName, FormPosition, null);
            if (position != null)
            {
                string strPointAsString = position.ToString();
                string[] coords = strPointAsString.Split('x');
                ptLocation = new Point(Int32.Parse(coords[0]), Int32.Parse(coords[1]));
            }

            return ptLocation;
        }
    }
}
