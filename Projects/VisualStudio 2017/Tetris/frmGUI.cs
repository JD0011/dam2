using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;

namespace Tetris
{
	/// <summary>
	/// Descripci�n breve de Form1.
	/// </summary>
	public class frmGUI : System.Windows.Forms.Form
	{
		private System.Windows.Forms.PictureBox pbPantallaJuego;
		private System.Windows.Forms.Timer tmrMuevePiezas;
		private System.ComponentModel.IContainer components;
		private Tetris t;
        private static int _numColumnas;
        private static int _numFilas;
		private bool haciaAbajo;
		private bool haciaDerecha;
		private bool rotaDerecha;
		private bool rotaIzquierda;
		private bool enPausa;
        private bool juegoIniciado;
		private System.Windows.Forms.Timer timer2;
		private System.Windows.Forms.Label lblNumLineas;
		private System.Windows.Forms.Label lblNivel;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.MainMenu mMenu;
		private System.Windows.Forms.MenuItem menuItem1;
		private System.Windows.Forms.MenuItem menuItem2;
		private System.Windows.Forms.MenuItem menuItem3;
		private System.Windows.Forms.MenuItem menuItem4;
		private System.Windows.Forms.MenuItem menuItem5;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.PictureBox pbPiezaSiguiente;
		private System.Windows.Forms.MenuItem menuItem6;
        private ToolTip ttConsejos;
        private MenuItem menuItem7;
        private MenuItem menuItem8;
        private PictureBox pbPause;
        private bool haciaIzquierda;

        public int NumFilas { get => _numFilas; }
        public int NumColumnas { get => _numColumnas; }
        public Tetris Tetris { get => t; set => t = value; }
        public PictureBox PantallaJuego { get => pbPantallaJuego; set => pbPantallaJuego = value; }

		public frmGUI()
		{
			//
			// Necesario para admitir el Dise�ador de Windows Forms
			//
			InitializeComponent();

			//
			// TODO: agregar c�digo de constructor despu�s de llamar a InitializeComponent
			//
		}

		/// <summary>
		/// Limpiar los recursos que se est�n utilizando.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region C�digo generado por el Dise�ador de Windows Forms
		/// <summary>
		/// M�todo necesario para admitir el Dise�ador. No se puede modificar
		/// el contenido del m�todo con el editor de c�digo.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmGUI));
            this.pbPantallaJuego = new System.Windows.Forms.PictureBox();
            this.tmrMuevePiezas = new System.Windows.Forms.Timer(this.components);
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.lblNumLineas = new System.Windows.Forms.Label();
            this.lblNivel = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.mMenu = new System.Windows.Forms.MainMenu(this.components);
            this.menuItem1 = new System.Windows.Forms.MenuItem();
            this.menuItem3 = new System.Windows.Forms.MenuItem();
            this.menuItem6 = new System.Windows.Forms.MenuItem();
            this.menuItem4 = new System.Windows.Forms.MenuItem();
            this.menuItem2 = new System.Windows.Forms.MenuItem();
            this.menuItem5 = new System.Windows.Forms.MenuItem();
            this.menuItem7 = new System.Windows.Forms.MenuItem();
            this.menuItem8 = new System.Windows.Forms.MenuItem();
            this.pbPiezaSiguiente = new System.Windows.Forms.PictureBox();
            this.label3 = new System.Windows.Forms.Label();
            this.ttConsejos = new System.Windows.Forms.ToolTip(this.components);
            this.pbPause = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pbPantallaJuego)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbPiezaSiguiente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbPause)).BeginInit();
            this.SuspendLayout();
            // 
            // pbPantallaJuego
            // 
            this.pbPantallaJuego.BackColor = System.Drawing.Color.Black;
            this.pbPantallaJuego.Location = new System.Drawing.Point(136, 24);
            this.pbPantallaJuego.Name = "pbPantallaJuego";
            this.pbPantallaJuego.Size = new System.Drawing.Size(260, 420);
            this.pbPantallaJuego.TabIndex = 0;
            this.pbPantallaJuego.TabStop = false;
            // 
            // tmrMuevePiezas
            // 
            this.tmrMuevePiezas.Interval = 800;
            this.tmrMuevePiezas.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // timer2
            // 
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // lblNumLineas
            // 
            this.lblNumLineas.BackColor = System.Drawing.Color.Black;
            this.lblNumLineas.ForeColor = System.Drawing.Color.White;
            this.lblNumLineas.Location = new System.Drawing.Point(16, 175);
            this.lblNumLineas.Name = "lblNumLineas";
            this.lblNumLineas.Size = new System.Drawing.Size(100, 23);
            this.lblNumLineas.TabIndex = 1;
            this.lblNumLineas.Text = "0";
            this.lblNumLineas.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.ttConsejos.SetToolTip(this.lblNumLineas, "N�mero de l�neas conseguidas");
            // 
            // lblNivel
            // 
            this.lblNivel.BackColor = System.Drawing.Color.Black;
            this.lblNivel.ForeColor = System.Drawing.Color.White;
            this.lblNivel.Location = new System.Drawing.Point(16, 231);
            this.lblNivel.Name = "lblNivel";
            this.lblNivel.Size = new System.Drawing.Size(100, 23);
            this.lblNivel.TabIndex = 2;
            this.lblNivel.Text = "1";
            this.lblNivel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.ttConsejos.SetToolTip(this.lblNivel, "Nivel actual en la partida");
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(16, 215);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 16);
            this.label2.TabIndex = 4;
            this.label2.Text = "Nivel:";
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(16, 159);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 16);
            this.label1.TabIndex = 5;
            this.label1.Text = "Lineas:";
            // 
            // mMenu
            // 
            this.mMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItem1,
            this.menuItem7,
            this.menuItem2});
            // 
            // menuItem1
            // 
            this.menuItem1.Index = 0;
            this.menuItem1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItem3,
            this.menuItem6,
            this.menuItem4});
            this.menuItem1.Text = "Juego";
            // 
            // menuItem3
            // 
            this.menuItem3.Index = 0;
            this.menuItem3.Text = "Nuevo";
            this.menuItem3.Click += new System.EventHandler(this.menuItem3_Click);
            // 
            // menuItem6
            // 
            this.menuItem6.Index = 1;
            this.menuItem6.Text = "Pausa";
            this.menuItem6.Click += new System.EventHandler(this.menuItem6_Click);
            // 
            // menuItem4
            // 
            this.menuItem4.Index = 2;
            this.menuItem4.Text = "Salir";
            this.menuItem4.Click += new System.EventHandler(this.menuItem4_Click);
            // 
            // menuItem2
            // 
            this.menuItem2.Index = 2;
            this.menuItem2.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItem5});
            this.menuItem2.Text = "Otros";
            // 
            // menuItem5
            // 
            this.menuItem5.Index = 0;
            this.menuItem5.Text = "Acerca de...";
            this.menuItem5.Click += new System.EventHandler(this.menuItem5_Click);
            // 
            // menuItem7
            // 
            this.menuItem7.Index = 1;
            this.menuItem7.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItem8});
            this.menuItem7.Text = "Ajustes";
            // 
            // menuItem8
            // 
            this.menuItem8.Index = 0;
            this.menuItem8.Text = "Tama�o de tablero";
            this.menuItem8.Click += new System.EventHandler(this.menuItem8_Click);
            // 
            // pbPiezaSiguiente
            // 
            this.pbPiezaSiguiente.BackColor = System.Drawing.Color.Black;
            this.pbPiezaSiguiente.Location = new System.Drawing.Point(16, 40);
            this.pbPiezaSiguiente.Name = "pbPiezaSiguiente";
            this.pbPiezaSiguiente.Size = new System.Drawing.Size(80, 80);
            this.pbPiezaSiguiente.TabIndex = 7;
            this.pbPiezaSiguiente.TabStop = false;
            this.ttConsejos.SetToolTip(this.pbPiezaSiguiente, "Pr�xima pieza a colocar");
            this.pbPiezaSiguiente.Click += new System.EventHandler(this.pbPiezaSiguiente_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(16, 24);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(89, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Pr�xima pieza:";
            // 
            // pbPause
            // 
            this.pbPause.BackColor = System.Drawing.SystemColors.Control;
            this.pbPause.Image = ((System.Drawing.Image)(resources.GetObject("pbPause.Image")));
            this.pbPause.Location = new System.Drawing.Point(2, 24);
            this.pbPause.Name = "pbPause";
            this.pbPause.Size = new System.Drawing.Size(128, 128);
            this.pbPause.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pbPause.TabIndex = 9;
            this.pbPause.TabStop = false;
            this.pbPause.Visible = false;
            // 
            // frmGUI
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(416, 452);
            this.Controls.Add(this.pbPause);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.pbPiezaSiguiente);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblNivel);
            this.Controls.Add(this.lblNumLineas);
            this.Controls.Add(this.pbPantallaJuego);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Menu = this.mMenu;
            this.Name = "frmGUI";
            this.Text = "eTetris";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmGUI_FormClosing);
            this.Load += new System.EventHandler(this.frmGUI_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmGUI_KeyDown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.frmGUI_KeyUp);
            ((System.ComponentModel.ISupportInitialize)(this.pbPantallaJuego)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbPiezaSiguiente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbPause)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion

		/// <summary>
		/// Punto de entrada principal de la aplicaci�n.
		/// </summary>
		[STAThread]
        //static void Main() 
        //{
        //    Application.Run(new frmGUI());
        //}

		private void frmGUI_Load(object sender, System.EventArgs e)
		{
            //Tama�o por defecto del tablero
            _numColumnas = Constantes.COLUMNAS_PANTALLA;
            _numFilas = Constantes.FILAS_PANTALLA;

            pbPantallaJuego.Width = _numColumnas * Constantes.ANCHO_CELDA;
            pbPantallaJuego.Height = _numFilas * Constantes.ALTO_CELDA;
            t = new Tetris(Convert.ToByte(_numFilas), Convert.ToByte(_numColumnas));

            RegistryReminder rReminder = new RegistryReminder();

            Point pt;
            if ((pt = rReminder.LoadFormPosition())!= Point.Empty){
                this.Location = pt;
            }
            else
            {
                this.CenterToScreen();
            }
		}

		private void frmGUI_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			string strKeyPress = null;
			strKeyPress = e.KeyCode.ToString();
			if (!t.juegoTerminado)
			{
				switch(strKeyPress.ToUpper())
				{
					case "LEFT":
						haciaIzquierda = true;
						//t.muevePiezaIzquierda();
						//pintaPantalla(t.matrizPantalla);
						break;
					case "RIGHT":
						haciaDerecha = true;
						//t.muevePiezaDerecha();
						//pintaPantalla(t.matrizPantalla);
						break;
					case "UP":
						rotaDerecha = true;
						//t.rotaPiezaDerecha();
						//pintaPantalla(t.matrizPantalla);
						break;
					case "A":
						rotaIzquierda = true;
						//t.rotaPiezaIzquierda();
						//pintaPantalla(t.matrizPantalla);
						break;
					case "DOWN":
						haciaAbajo = true;
						//t.muevePiezaAbajo();
						//pintaPantalla(t.matrizPantalla);
						break;
					case "P":
						pausaJuego();
						break;
					default:
						//MessageBox.Show(strKeyPress.ToUpper());
						break;
				}
			}
			else
			{
				switch(strKeyPress.ToUpper())
				{
					case "ENTER":
						break;
					default:
						break;
				}
			}
		}

		private void frmGUI_KeyUp(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			string strKeyPress = null;
			strKeyPress = e.KeyCode.ToString();
			if (!t.juegoTerminado)
			{
				switch(strKeyPress.ToUpper())
				{
					case "LEFT":
						haciaIzquierda = false;
						break;
					case "RIGHT":
						haciaDerecha = false;
						break;
					case "DOWN":
						haciaAbajo = false;
						break;
					case "UP":
						rotaDerecha = false;
						break;
					case "A":
						rotaIzquierda = false;
						break;
					default:
						//MessageBox.Show(strKeyPress.ToUpper());
						break;
				}
			}
		}

		public void pintaPantalla(int [, ] matrizPantalla)
		{
			Bitmap B = new Bitmap(pbPantallaJuego.Width, pbPantallaJuego.Height, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
            //Bitmap B = new Bitmap(pbPiezaSiguiente.Width, pbPiezaSiguiente.Height, System.Drawing.Imaging.PixelFormat.Format16bppRgb555);
            Graphics G = Graphics.FromImage(B);
			G.Clear(Color.Black);
			for (int x = 0; x < _numColumnas; x++)
			{
				for (int y = 0; y < _numFilas; y++)
				{
					int elemento = matrizPantalla[y, x];
					if (elemento != 0)
					{
						dibujaCuadro(G, y, x, Constantes.COLORES(elemento - 1));
					}
				}
			}
			pbPantallaJuego.Image = B;
		}
		
		public void pintaPiezaSiguiente(Pieza p)
		{
            Bitmap B = new Bitmap(pbPiezaSiguiente.Width, pbPiezaSiguiente.Height, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
            //Bitmap B = new Bitmap(pbPiezaSiguiente.Width, pbPiezaSiguiente.Height, System.Drawing.Imaging.PixelFormat.Format16bppRgb555);
            Graphics G = Graphics.FromImage(B);
			G.Clear(Color.Black);
			for (int x = 0; x < Constantes.COLUMNAS_PIEZAS; x++)
			{
				for (int y = 0; y < Constantes.FILAS_PIEZAS; y++)
				{
					int elemento = t.piezaSiguiente[y, x];
					if (elemento != 0)
					{
						dibujaCuadro(G, y, x, Constantes.COLORES(t.piezaSiguiente.color - 1));
					}
				}
			}
			pbPiezaSiguiente.Image = B;
		}

		private void dibujaCuadro(Graphics G, int Y,int X, Color C)
		{
			int x = (X * Constantes.ANCHO_CELDA) + 1;
			int y = (Y * Constantes.ALTO_CELDA) + 1;
			SolidBrush Br = new SolidBrush(C);
			G.FillRectangle(Br, x, y, Constantes.ANCHO_CELDA - 2, Constantes.ALTO_CELDA - 2);
		}

		private void timer1_Tick(object sender, System.EventArgs e)
		{
			if (t.numLineas < Constantes.NUM_LINEAS_POR_NIVEL)
			{
				lblNumLineas.Text = t.numLineas.ToString();
				if (haciaAbajo)
					actualizaPantalla();
				else
				{
					haciaAbajo = true;
					actualizaPantalla();
					haciaAbajo = false;
				}
				pintaPiezaSiguiente(t.piezaSiguiente);
				if (t.juegoTerminado)
				{
					tmrMuevePiezas.Stop();
					MessageBox.Show("Se termin� el juego");
				}
			}
		}

		private void timer2_Tick(object sender, System.EventArgs e)
		{
			if (t.numLineas < Constantes.NUM_LINEAS_POR_NIVEL)
			{
				actualizaPantalla();
			}
			else
			{
				timer2.Stop();
				tmrMuevePiezas.Stop();
				lblNumLineas.Text = t.numLineas.ToString();
				inicializaTeclas();
				t.nuevoNivel();
				MessageBox.Show("��Enhorabuena!! Has pasado al nivel " + (t.nivel + 1));
				lblNivel.Text = ((int)t.nivel + 1).ToString();
				tmrMuevePiezas.Interval = Constantes.NIVELES(t.nivel);
				timer2.Start();
				tmrMuevePiezas.Start();		
			}
			if (t.juegoTerminado)
			{
				timer2.Stop();
			}
		}

		private void actualizaPantalla()
		{
			if (haciaAbajo)
			{
				t.muevePiezaAbajo();
			}
			if (haciaDerecha)
			{
				t.muevePiezaDerecha();
			}
			if (haciaIzquierda)
			{
				t.muevePiezaIzquierda();
			}
			if (rotaDerecha)
			{
				t.rotaPiezaDerecha();
				rotaDerecha = false;
			}	
			if (rotaIzquierda)
			{
				t.rotaPiezaIzquierda();
				rotaIzquierda = false;
			}
			pintaPantalla(t.matrizPantalla);		
		}

		private void inicializaTeclas()
		{
			haciaDerecha = false;
			haciaIzquierda = false;
			rotaDerecha = false;
			rotaIzquierda = false;
			haciaAbajo = false;
			enPausa = false;
		}

		private void nuevoJuego()
		{
            inicializaTeclas();
			t.nuevoJuego();
			tmrMuevePiezas.Interval = Constantes.NIVELES(t.nivel);
			pintaPantalla(t.matrizPantalla);
			tmrMuevePiezas.Start();
			timer2.Start();

            juegoIniciado = true;
            menuItem6.Text = "Pausa";
            pbPause.Visible = false;
        }

		private void pausaJuego()
		{
            if (juegoIniciado)
            {
                if (!enPausa)
                {
                    tmrMuevePiezas.Stop();
                    timer2.Stop();
                    menuItem6.Text = "Reanudar";
                    pbPause.Visible = true;
                }
                else
                {
                    tmrMuevePiezas.Start();
                    timer2.Start();
                    menuItem6.Text = "Pausa";
                    pbPause.Visible = false;

                }
                enPausa = !enPausa;
            }
		}

		private void menuItem4_Click(object sender, System.EventArgs e)
		{
			this.Close();
		}

		private void menuItem3_Click(object sender, System.EventArgs e)
		{
			nuevoJuego();
		}

		private void menuItem5_Click(object sender, System.EventArgs e)
		{
			enPausa = false;
			pausaJuego();
			AcerdaDe a = new AcerdaDe();
			a.ShowDialog();
			a.Dispose();
			enPausa = true;
			pausaJuego();
		}

		private void menuItem6_Click(object sender, System.EventArgs e)
		{
			pausaJuego();
		}

        private void pbPiezaSiguiente_Click(object sender, EventArgs e)
        {

        }

        private void frmGUI_FormClosing(object sender, FormClosingEventArgs e)
        {
            new RegistryReminder().SaveFormPosition(this);
        }

        private void menuItem8_Click(object sender, EventArgs e)
        {
            enPausa = false;
            pausaJuego();

            frmBoardSize fBoard = new frmBoardSize(_numFilas, _numColumnas);
            fBoard.ShowDialog();
            fBoard.Dispose();

            if (fBoard.HasChanged)
            {
                if(_numFilas != fBoard.NumFilas || _numColumnas != fBoard.NumColumnas)
                {
                    _numFilas = fBoard.NumFilas;
                    _numColumnas = fBoard.NumColumnas;

                    t = new Tetris(fBoard.NumFilas, fBoard.NumColumnas);
                    pbPantallaJuego.Width = fBoard.NumColumnas * Constantes.ANCHO_CELDA;
                    pbPantallaJuego.Height = fBoard.NumFilas * Constantes.ALTO_CELDA;
                    pintaPantalla(t.matrizPantalla);

                    menuItem6.Text = "Pausa";
                    pbPause.Visible = false;

                    MessageBox.Show(
                        this,
                        "Inicie un juego nuevo con el tama�o establecido",
                        "Juego finalizado",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show(
                        this,
                        "Reanude el juego desde el menu principal",
                        "Juego pausado",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Information);
                }
            }
        }
    }
}
