﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Tetris
{
    public partial class SplashScreen : Form
    {
        Timer tmr = new Timer();
        public SplashScreen()
        {
            InitializeComponent();
        }

        private void SplashScreen_Load(object sender, EventArgs e)
        {
            tmr.Interval = 3000;
            tmr.Tick += LaunchForm;
            tmr.Start();

        }

        private void LaunchForm(object sender, EventArgs e)
        {
            tmr.Stop();
            this.Hide();
            frmGUI mform = new frmGUI();
            mform.ShowDialog();
            mform.Dispose();
        }
    }
}
