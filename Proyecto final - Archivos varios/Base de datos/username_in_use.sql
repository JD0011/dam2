CREATE DEFINER=`root`@`localhost` PROCEDURE `username_in_use`(IN username VARCHAR(45), OUT in_use BOOLEAN)
BEGIN
	DECLARE match_found INT;
	SELECT COUNT(username) INTO match_found FROM users u WHERE u.username = username;
    
    IF(match_found > 0 ) THEN 
		SET in_use = TRUE;
	ELSE 
		SET in_use = FALSE;
	END IF ;
END