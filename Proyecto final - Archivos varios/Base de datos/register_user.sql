CREATE DEFINER=`root`@`localhost` PROCEDURE `register_user`(IN user VARCHAR(45), IN pwd VARCHAR(45), IN email VARCHAR(45), IN pnumber INT, OUT is_registered BOOLEAN)
BEGIN
	DECLARE registered_user VARCHAR(45);
	
    INSERT INTO users (username, password, mail, phonenumber) VALUES (user, pwd, email, pnumber);

	SELECT 
		username
	INTO registered_user FROM
		users u
	WHERE
		u.username = user;

	IF(registered_user IS NOT NULL) THEN
		SET is_registered = TRUE;
	ELSE
		SET is_registered = FALSE;
	END IF;
END