CREATE DEFINER=`root`@`localhost` PROCEDURE `user_password_ok`(IN username VARCHAR(45), IN pwd VARCHAR(45), OUT canlogin BOOLEAN)
BEGIN

	DECLARE stored_password VARCHAR(45);
	SELECT password INTO stored_password FROM users u WHERE u.username = username;
    
    IF(stored_password = pwd) THEN 
		SET canlogin = TRUE;
    ELSE 
		SET canlogin = FALSE;
    END IF;
    
END